import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
import aiohttp
from asyncio.queues import Queue
from statsmodels.stats.weightstats import DescrStatsW

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class PriceDistCahce():
    def __init__(self,cache_size=50) -> None:
        self.ts = np.array([]) 
        self.price = np.array([]) 
        self.qty_with_price = np.array([]) 
        self.count = 0
        self.qty = np.empty((0, 0))
        self.cache_size = cache_size # in milisecond

    def feed_trade(self,trade_data):
        row, column = self.qty.shape
        ts_index = np.where(self.ts==trade_data["timestamp"])[0]
        price_index = np.where(self.price==trade_data["price"])[0]
        self.count += 1
        if ts_index.size and price_index.size:
            self.qty[price_index[0]][ts_index[0]] += trade_data["qty"]
            self.qty_with_price[price_index[0]] += trade_data["qty"]
        elif ts_index.size and not price_index.size:
            new = np.zeros((1,column))
            new[0][ts_index[0]] += trade_data["qty"]
            self.qty = np.concatenate((self.qty, new), axis=0)
            self.price = np.append(self.price, trade_data["price"])
            self.qty_with_price = np.append(self.qty_with_price,trade_data["qty"])
        elif price_index.size and not ts_index.size:
            new = np.zeros((row,1))
            new[price_index[0]][0] += trade_data["qty"]
            self.qty = np.concatenate((self.qty,new), axis=1)
            self.ts = np.append(self.ts,trade_data["timestamp"])
            self.qty_with_price[price_index[0]] += trade_data["qty"]
        else:
            new_column = np.zeros((row,1))
            new_row = np.zeros((1,column+1))
            self.qty = np.concatenate((self.qty, new_column), axis=1)
            self.qty = np.concatenate((self.qty, new_row), axis=0)
            self.qty[-1][-1] += trade_data["qty"]
            
            self.price = np.append(self.price,trade_data["price"])
            self.ts = np.append(self.ts,trade_data["timestamp"])
            self.qty_with_price = np.append(self.qty_with_price,trade_data["qty"])
            
    def remove_expired_ts(self,ts):
        ts = ts - self.cache_size
        while self.ts.size>0 and self.ts[0] < ts:
            col = self.qty[:,0]
            ind =np.where(col>0)[0]
            if ind.size>0:
                self.qty_with_price[ind] -= col[ind]
            
            self.ts = self.ts[1:]
            self.qty = self.qty[:,1:]
            self.count -= ind.size
            
    def remove_expired_price(self):
        ind = np.where(self.qty_with_price>1e-10)[0]
        self.qty_with_price = self.qty_with_price[ind]
        self.price = self.price[ind]
        self.qty = self.qty[ind]
        
    def get_ask_price_quantile(self, quantile=0.5):
        return DescrStatsW(self.price, weights=self.qty_with_price).quantile(quantile).iloc[0] if np.sum(self.qty_with_price) else None
    
    def get_bid_price_quantile(self, quantile=0.5):
        return DescrStatsW(self.price, weights=self.qty_with_price).quantile(1-quantile).iloc[0] if np.sum(self.qty_with_price) else None
    
    def get_filtered_price(self,qty_filter):
        tmp = self.price[np.where(self.qty_with_price*self.price > qty_filter)[0]]
        return np.min(tmp) if tmp.size else None
    
    def get_trade_count(self):
        return self.count
    
    def get_total_qty(self):
        return np.sum(self.qty_with_price)

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)