import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue
from typing import AnyStr, List, Union
import ujson as json
import math

from atom.helpers import ClientIDGenerator, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_executor.order_executor import OrderExecutorTest2, TransOrder
from xex_mm.utils.base import Depth, Trade, Direction, ReferencePriceCalculator
from xex_mm.utils.configs import price_precision, qty_precision

from atom.model.depth import Depth as AtomDepth
import traceback



ART = 100
FREQUENCY = 500
FLOOR = 0.001
BETA_BASE = 1e-2
T = 1000
T_F = 500

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class OrderBalancer():
    def __init__(self, key_list: list) -> None:
        self._key_list = key_list.copy()
            
    def add_count(self, key):
        self._key_list.append(self._key_list.pop(self._key_list.index(key)))
                        
    def get_sort_list(self):
        return self._key_list

        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_usdt.margin.fix"
        self.contract = "btc_usdt"
        self.ex = "binance"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_order_queue = Queue()
        self.cancel_order_queue = Queue()
        self.bbo_ts = None
        
        self.prc_per_diff = 0.00005
        self.ref_prc_cal = ReferencePriceCalculator()
        self.order_balancer = OrderBalancer(self.account_names)

        self.cancel_ts = 0
        self.send = True
        self.sending = False
        self.canceling = False
        
        self.ap = None
        
        self.last_bbo = 0
        
        self.executor = OrderExecutorTest2(
            iids=["binance.btc_usdt"], 
            contract=self.contract, 
            art=ART, 
            floor=FLOOR,
            onboard=True)
        
        self.traders = [
            "hf_trader1",
            "hf_trader2",
            "hf_trader3",
            "hf_trader4",
            "hf_trader5",
            "hf_trader6",
        ]
    
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def internal_fail(self,client_id,acc,err=None):
        try:
            self.logger.critical(f"failed order: {err}, acc:{acc}, client_id={client_id}")
            if self.order_cache.get(client_id) == None:
                return
            o = self.order_cache[client_id]
            self.logger.warning(f"order: {o}")
            self.logger.warning(f"acc available={self.acc_available[acc]['usdt'] if o.side == OrderSide.Buy else self.acc_available[acc]['btc']}")
            self.logger.warning(f"acc available={(await self.check_account(acc))['btc_usdt']['usdt']['available']} by query")
            o.xchg_status = OrderStatus.Failed  
            await self.on_order(o)
        except:
            pass
    
    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
    
    async def check_account(self, acc):
        api = self.get_exchange_api_by_account(acc)
        result = dict()
        response = await api.make_request("margin", "GET", "/sapi/v1/margin/isolated/account", query=dict(), need_sign=True)
        for item in response.json["assets"]:
            pair = f"""{item["baseAsset"]["asset"]}_{item["quoteAsset"]["asset"]}""".lower()
            info = dict()
            for k in ("baseAsset", "quoteAsset"):
                info[item[k]["asset"].lower()] = dict(
                    all=safe_decimal(item[k]["totalAsset"]),
                    available=safe_decimal(item[k]["free"]),
                    borrowed=safe_decimal(item[k]["borrowed"]),
                    net=safe_decimal(item[k]["netAsset"]),
                    hold=0,
                    frozen=safe_decimal(item[k]["locked"])
                )
            result[pair] = info
            
        return result

    async def before_strategy_start(self):
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
        self.direct_subscribe_orderbook(symbol_name="binance.btc_usdt.spot.na", is_incr_depth=True)
        
        ch1 = "mq:hf_signal:order_response"
        ch2 = "mq:hf_signal:cancel_response"
        self.loop.create_task(self.subscribe_to_channel([ch1],self.process_order_response))
        self.loop.create_task(self.subscribe_to_channel([ch2],self.process_cancel_response))
    
        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        self.half_step_tick = qty_tick/Decimal("2")
        self.half_price_tick = price_tick/Decimal("2")
        self.price_tick = price_tick
        
        self.api = dict()
        self.uid_load = dict()
        self.ip_load = dict()
        self.acc_available = dict()
        self.acc_net = dict()
        self.logger.setLevel("WARNING")
        
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
            except:
                pass
            cur_balance = (await self.check_account(acc))["btc_usdt"]
            print(cur_balance,acc)
            self.api[acc] = api
            self.uid_load[acc] = 0
            
            self.acc_available[acc] = dict()
            self.acc_available[acc]["btc"] = cur_balance["btc"]["available"]
            self.acc_available[acc]["usdt"] = cur_balance["usdt"]["available"]
            
            self.acc_net[acc] = dict()
            self.acc_net[acc]["btc"] = cur_balance["btc"]["net"]
            self.acc_net[acc]["usdt"] = cur_balance["usdt"]["net"]
        
        for strategy_name in self.traders:
            self.ip_load[strategy_name] = 0
            
        c = 0
        while True:
            await asyncio.sleep(1)
            c += 1
            if self.ap:
                for acc in self.account_names:
                    if self.acc_net[acc]["btc"] > 0:
                        qty = round_decimals_down(self.acc_net[acc]["btc"], qty_precision[f"{self.ex}.{self.contract}"])
                        self.logger.warning(f"selling {qty} for {acc}")
                        await self.send_order(
                            price=self.ap*Decimal(1-0.03),
                            qty=Decimal(qty),
                            side=OrderSide.Sell,
                            account_name=acc, 
                            strategy_name=self.traders[0],
                            label=1
                            )
                    else:
                        qty = round_decimals_up(-self.acc_net[acc]["btc"], qty_precision[f"{self.ex}.{self.contract}"])
                        self.logger.warning(f"buying {qty} for {acc}")
                        await self.send_order(
                            price=self.ap*Decimal(1+0.03),
                            qty=Decimal(qty),
                            side=OrderSide.Buy,
                            account_name=acc, 
                            strategy_name=self.traders[0],
                            label=1
                            )
                    await asyncio.sleep(2)
                    cur_balance = (await self.check_account(acc))["btc_usdt"]
                    btc_borrow_qty = cur_balance["usdt"]["net"] / self.ap - cur_balance["btc"]["borrowed"]
                    api = self.get_exchange_api_by_account(acc)
                    self.logger.warning(f"need to borrow {btc_borrow_qty} for {acc}")
                    if btc_borrow_qty > 0:
                        try:
                            borrow = round_decimals_up(btc_borrow_qty, qty_precision[f"{self.ex}.{self.contract}"])
                            new_volume = str(int(Decimal(borrow) / qty_tick) * qty_tick)
                            res = await api.margin_borrow(
                                symbol=self.get_symbol_config(self.symbol),
                                coin="btc",
                                amount=new_volume,
                                )
                            self.logger.warning(f"borrow result for {acc}: {res.data}")
                        except Exception as err:
                            self.logger.warning(f"borrow err: {err}")
                    elif btc_borrow_qty < 0:
                        repay = round_decimals_down(-btc_borrow_qty, qty_precision[f"{self.ex}.{self.contract}"])
                        new_volume = str(int(Decimal(repay) / qty_tick) * qty_tick)
                        try:
                            res = await api.margin_repay(
                                symbol=self.get_symbol_config(self.symbol),
                                coin="btc",
                                amount=new_volume,
                                )
                            self.logger.warning(f"repay result for {acc}: {res.data}")
                        except Exception as err:
                            self.logger.warning(f"repay err: {err}")
                break
            if c > 1000:
                self.logger.critical("no price coming")
                exit()
            
            
        await asyncio.sleep(5)
        self.starting_cash = 0
        for acc in self.account_names:
            cur_balance = (await self.check_account(acc))["btc_usdt"]
            self.starting_cash += cur_balance["usdt"]["all"]
            self.acc_available[acc]["btc"] = cur_balance["btc"]["available"]
            self.acc_available[acc]["usdt"] = cur_balance["usdt"]["available"]
            print(cur_balance,acc)

        self.logger.warning(f"starting cash: {self.starting_cash}")
        
    async def balance_account(self):
        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        while True:
            await asyncio.sleep(3600)
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                cur_balance = (await self.check_account(acc))["btc_usdt"]
                borrowed_btc = cur_balance["btc"]["borrowed"] * self.ap
                all = cur_balance["usdt"]["all"] + cur_balance["btc"]["all"] * self.ap
                if borrowed_btc / all > 0.55:
                    try:
                        repay = (borrowed_btc - all /2) / self.ap
                        repay = round_decimals_down(repay, qty_precision[f"{self.ex}.{self.contract}"])
                        new_volume = str(int(Decimal(repay) / qty_tick) * qty_tick)
                        res = await api.margin_repay(
                                symbol=self.get_symbol_config(self.symbol),
                                coin="btc",
                                amount=new_volume,
                                )
                        self.logger.warning(f"repay result for {acc}: {res.data}")
                    except Exception as err:
                        self.logger.warning(f"repay err: {err}")
                elif borrowed_btc / all < 0.45:
                    try:
                        borrow = (all / 2 - borrowed_btc) / self.ap
                        borrow = round_decimals_up(borrow, qty_precision[f"{self.ex}.{self.contract}"])
                        new_volume = str(int(Decimal(borrow) / qty_tick) * qty_tick)
                        res = await api.margin_borrow(
                                symbol=self.get_symbol_config(self.symbol),
                                coin="btc",
                                amount=new_volume,
                                )
                        self.logger.warning(f"borrow result for {acc}: {res.data}")
                    except Exception as err:
                        self.logger.warning(f"borrow err: {err}")
                    
                    
    async def on_orderbook(self, symbol, orderbook):
        try:
            self.ap = Decimal(orderbook["asks"][0][0])
            new_depth = Depth.from_dict(depth=orderbook)
            self.executor.update_depth(symbol, new_depth)

            ref_prc = self.ref_prc_cal.calc(self.executor.get_depth(symbol))
            if self.send_order_queue.empty() and not self.sending and self.send:
                self.send_order_queue.put_nowait(ref_prc)
                
            if self.cancel_order_queue.empty() and not self.canceling:
                self.cancel_order_queue.put_nowait(1)
        
        except:
            traceback.print_exc()

    async def on_order(self, order):
        if self.order_cache.get(order.client_id) is None:
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            o = self.order_cache.get(order.client_id)
            try:
                if order.filled_amount > 0 and o.message["label"] == 0:
                    amt_chg = order.filled_amount if o.side == OrderSide.Buy else - order.filled_amount
                    symbol_config = self.get_symbol_config(o.symbol_id)
                    self.executor.update_finished_order(
                        ".".join((symbol_config["exchange_name"],symbol_config["pair"])), 
                        float(amt_chg)
                    )
                    
                side = o.side
                acc = o.account_name
                if side == OrderSide.Buy:
                    self.acc_available[acc]["usdt"] += (o.requested_amount * o.requested_price - order.filled_amount * order.avg_filled_price)
                    self.acc_available[acc]["btc"] += order.filled_amount
                else:
                    self.acc_available[acc]["btc"] += (o.requested_amount - order.filled_amount)
                    self.acc_available[acc]["usdt"] += order.filled_amount * order.avg_filled_price
                        
                self.order_cache.pop(order.client_id)
            
            except Exception as err:
                traceback.print_exc()
                self.logger.critical(f"handle order err: {err}")
            
    
    def handle_limit_sapi(self, ip1m, uid1m, acc, sn):
        self.ip_load[sn] = ip1m
        self.uid_load[acc] = uid1m
    
    async def reduce_count(self):
        while True:
            await asyncio.sleep(1)
            trader_list = sorted(self.ip_load, key=self.ip_load.get)
            self.ip_load[trader_list[-1]] -= 1
    
    async def get_balance(self):
        while True:
            await asyncio.sleep(30)
            btc_amount = 0
            usdt_amount = 0
            btc_borrowed = 0
            for acc in self.account_names:
                try:
                    cur_balance = (await self.check_account(acc))["btc_usdt"]
                    btc_amount += cur_balance["btc"]["all"]
                    usdt_amount += cur_balance["usdt"]["all"]
                    btc_borrowed += cur_balance["btc"]["borrowed"]
                except Exception as err:
                    self.logger.warning(f"get balance err, {err}")
                    
            usdt_balance = float(btc_amount*self.ap + usdt_amount - btc_borrowed*self.ap)
            if usdt_balance < 0:
                self.logger.warning(f"btc_amount={btc_amount}, usdt_amount={usdt_amount}, ap={self.ap}, starting cash={self.starting_btc}")
            self.loop.create_task(
                self.push_influx_data(
                    measurement="tt", 
                    tag={"sn":"btc_margin"}, 
                    fields={
                        "balance":float(btc_amount*self.ap + usdt_amount),
                        "usdt_balance":usdt_balance
                        }
                    )
                )
    
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(5)
            self.loop.create_task(
                self.push_influx_data(
                    measurement="tt", 
                    tag={"sn":"btc_margin"}, 
                    fields={
                        "inventory":float(self.executor.get_inventory("binance.btc_usdt"))
                        }
                    )
                )
            for trader in self.traders:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":"btc_margin"}, 
                        fields={
                            trader:float(self.ip_load[trader])
                            }
                        )
                    )
                
            for acc in self.account_names:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":"btc_margin"}, 
                        fields={
                            acc:float(self.uid_load[acc])
                            }
                        )
                    )
    
    # def generate_orders(self, ref_prc):
    #     return self.executor.get_orders(
    #         contract=self.contract,
    #         ref_prc=ref_prc, 
    #         beta_base=BETA_BASE,
    #         t=0,
    #         tau=FREQUENCY,
    #         T=T
    #         )
    
    def generate_orders(self,ref_prc):
        try:
            return self.executor.get_orders(
                contract=self.contract,
                ref_prc=ref_prc, 
                beta=BETA_BASE,
                t=0,
                tau=FREQUENCY,
                T_F=T_F,  
                floors=[0.003, 0.004, 0.005, 0.006, 0.007],
                step=0.001
                )
        except:
            return []
        
    def direction_map(self, side):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
        
    async def send_order_action(self):
        async def send_order_helper(order, acc_list, strategy_name):
            send_block = False
            for oid, o in self.order_cache.items():
                if o.side == self.direction_map(order.side) and not self.order_cache[oid].message["cancel"] and o.message["floor"] == order.floor:
                    if abs(float(o.requested_price) - order.price) / float(o.requested_price) > self.prc_per_diff:
                        self.loop.create_task(self.handle_cancel_order(oid))
                    else:
                        self.order_cache[oid].create_ms = int(time.time()*1e3)
                    send_block = True
            if send_block:
                return
            if order.side == Direction.short:
                price = round_decimals_up(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
                for acc in acc_list:
                    qty_temp = round_decimals_down(float(self.acc_available[acc]["btc"])*0.99, qty_precision[order.iid])
                    if qty_temp > qty:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty),
                                side=OrderSide.Sell,
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                            )
                        qty = 0
                        break
                    else:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty_temp),
                                side=OrderSide.Sell,  
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                            )
                        qty -= qty_temp
                if qty > 0:
                    self.logger.warning(f"sell order unsent with {qty}")
                    available_btc = 0
                    for acc in acc_list:
                        available_btc += self.acc_available[acc]["btc"]
                    self.logger.warning(f"available_btc={available_btc}, at level={order.floor}")
                    for _, o in self.order_cache.items():
                        if o.side == OrderSide.Sell:
                            self.logger.warning(f"p={o.requested_price}, q={o.requested_amount}, side={o.side}, tag={o.message['floor']}, cancel={o.message['cancel']}, sn={strategy_name}, id={o.client_id}")
                    self.logger.warning(f".....................")
                    
            elif order.side == Direction.long:
                price = round_decimals_down(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
                for acc in acc_list:
                    qty_temp = round_decimals_down(float(self.acc_available[acc]["usdt"])/price*0.99, qty_precision[order.iid])
                    if qty_temp > qty:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty),
                                side=OrderSide.Buy,
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                                )
                        qty = 0
                        break
                    else:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty_temp),
                                side=OrderSide.Buy,
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                                )
                        qty -= qty_temp
                if qty > 0:
                    self.logger.warning(f"buy order unsent with {qty}, amount of {qty*price}, at level={order.floor}")
                    available_usdt = 0
                    for acc in acc_list:
                        available_usdt += self.acc_available[acc]["usdt"]
                    self.logger.warning(f"available_usdt={available_usdt}")
                    for _, o in self.order_cache.items():
                        if o.side == OrderSide.Buy:
                            self.logger.warning(f"p={o.requested_price}, q={o.requested_amount}, side={o.side}, tag={o.message['floor']}, cancel={o.message['cancel']}, sn={strategy_name}, id={o.client_id}")
                    self.logger.warning(f".....................")
                            
        while True:
            ref_prc = await self.send_order_queue.get()
            self.sending = True
            orders = self.generate_orders(ref_prc)
            acc_list = self.order_balancer.get_sort_list()
            trader_list = sorted(self.ip_load, key=self.ip_load.get)
            for order in orders:
                self.loop.create_task(send_order_helper(order,acc_list, trader_list[0]))  
            self.sending = False
    
    async def handle_cancel_order(self,oid):
        try:
            if self.order_cache.get(oid):
                if oid not in self.canceling_id:
                    trader_list = sorted(self.ip_load, key=self.ip_load.get)
                    self.canceling_id.add(oid)
                    self.order_cache[oid].message["cancel"] = True
                    await self.publish_to_channel(
                        "mq:hf_signal:cancel_request",
                        {
                            "strategy_name":trader_list[0],
                            "order": self.order_cache[oid].to_json(),
                            "main_sn":self.strategy_name
                        }
                    )

        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
                
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["main_sn"] != self.strategy_name:
            return
        if self.order_cache.get(payload["client_id"]) and payload["res"] == False:
            self.order_cache[payload["client_id"]].message["cancel"] = False
        self.canceling_id.remove(payload["client_id"])
        
    async def send_order(self,price,qty,side,strategy_name,account_name=None,floor=None,order_type=OrderType.Limit,label=0):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            order_type=order_type,
            position_side=OrderPositionSide.Open,
            account_name=account_name
        )
        extra_info = dict()
        extra_info["cancel"] = False
        extra_info["query"] = 0
        extra_info["life"] = 100
        extra_info["label"] = label
        extra_info["floor"] = floor
        extra_info["sn"] = strategy_name
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        
        if side == OrderSide.Buy:
            self.acc_available[account_name]["usdt"] -= price * qty
        else:
            self.acc_available[account_name]["btc"] -= qty
            
        try:
            self.order_balancer.add_count(account_name)
            await self.publish_to_channel(
                "mq:hf_signal:order_request",
                {
                    "strategy_name":strategy_name,
                    "symbol":self.symbol,
                    "price":price,
                    "qty":qty,
                    "side":1 if side == OrderSide.Buy else -1,
                    "client_id": client_id,
                    "acc": account_name,
                    "main_sn":self.strategy_name
                }
            )
        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"{err}")
            await self.internal_fail(client_id,err)
            
    async def process_order_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["main_sn"] != self.strategy_name:
            return
        if payload["uid1m"] == None:
            await self.internal_fail(
                client_id=payload["client_id"],
                err=payload["err"],
                acc=payload["account_name"]
                )
            return 
        
        self.handle_limit_sapi(
            ip1m=payload["ip1m"],
            uid1m=payload["uid1m"],
            sn=payload["startegy_name"],
            acc=payload["account_name"],
            )
            
    async def cancel_order_action(self):
        while True:
            await self.cancel_order_queue.get()
            self.canceling = True
            for oid, order in self.order_cache.items():
                if not order.message["cancel"] and order.create_ms < time.time()*1e3 - order.message["life"]:
                    self.loop.create_task(self.handle_cancel_order(oid))
                elif order.message["cancel"] and order.create_ms < time.time()*1e3 - 2000:
                    self.loop.create_task(self.handle_cancel_order(oid))
            self.canceling = False
    
    async def retreat(self):
            # rebalance to 0
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
            except Exception as err:
                self.logger.critical(f"retreat cancel err:{err}")
            cur_balance = (await self.check_account(acc))["btc_usdt"]
            if cur_balance["btc"]["net"] < 0:
                price = self.ap * Decimal(1.04)
                await self.send_order(
                    price=Decimal(price),
                    qty=Decimal(
                        round_decimals_up(-cur_balance["btc"]["net"], qty_precision[f"{self.ex}.{self.contract}"])
                    ),
                    side=OrderSide.Buy,
                    strategy_name=self.traders[0],
                    account_name=acc,
                )
                await asyncio.sleep(10)
            elif cur_balance["btc"]["net"] > 0:
                price = self.ap * Decimal(0.96)
                await self.send_order(
                    price=Decimal(price),
                    qty=Decimal(
                        round_decimals_down(cur_balance["btc"]["net"], qty_precision[f"{self.ex}.{self.contract}"])
                    ),
                    side=OrderSide.Sell,
                    strategy_name=self.traders[0],
                    account_name=acc,
                )
            else:
                await asyncio.sleep(1)
                continue

        # repay
        end_amount = 0
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            cur_balance = (await self.check_account(acc))["btc_usdt"]
            end_amount += cur_balance["usdt"]["net"]
            if cur_balance['btc']['borrowed'] <= 0:
                continue
            symbol_cfg = self.get_symbol_config(self.symbol)
            try:
                self.logger.warning(f"repay for {acc}, quantity={cur_balance['btc']['borrowed']}")
                res = await api.margin_repay(
                    symbol=symbol_cfg,
                    coin="btc",
                    amount=str(cur_balance["btc"]["borrowed"]) if cur_balance["btc"]["net"] >=0 else str(cur_balance["btc"]["all"]),
                    )
                self.logger.warning(f"end of strategy repay result for {acc}: {res.data}")
            except Exception as err:
                self.logger.warning(f"end of strategy repay err: {err}")
        self.logger.warning(f"end amount={end_amount}")
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    try:
                        await self.retreat()
                    except:
                        traceback.print_exc()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if oid in self.canceling_id:
                return
            if time.time()*1e3 - order.create_ms > 60000:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["query"] += 1
                        if self.order_cache[oid].message["query"]>3:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                        else:
                            self.loop.create_task(self.handle_cancel_order(oid))

                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(30)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )
            
    async def debug(self):
        while True:
            await asyncio.sleep(10)
            for oid, o in self.order_cache.items():
                if o.message["cancel"] is True and time.time()*1e3 - o.create_ms> 1000:
                    self.logger.warning(f"expired order: p={o.requested_price}, q={o.requested_amount}, side={o.side}, acc={o.account_name}, sn={o.message['sn']}, id={oid}, time={time.time()*1e3 - o.create_ms}")
            self.logger.warning(".................")
            self.logger.warning(f"{self.ip_load},{self.uid_load}")
                    
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.update_redis_cache(),
            self.reduce_count(),
            self.get_balance(),
            self.get_local_stats(),
            self.balance_account(),
            # self.debug(),
            )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
        