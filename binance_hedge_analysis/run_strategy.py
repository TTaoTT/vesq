from strategy_base.base import CommonStrategy
from atom.model import PartialOrder, OrderStatus

import asyncio
from decimal import Decimal
from collections import deque
import numpy as np
import time

import ujson as json
import traceback

ACC_STRAT_MAP = {
    "binance.mm12":"hard press",
    "binance.mm15":"with stop"
}

class Cache:
    def __init__(self) -> None:
        self._cache = {
            "stop_pnl":deque(maxlen=1008),
            "press_pure_pnl":deque(maxlen=1008),
            "press_commission_pnl":deque(maxlen=1008),
            "limit_pnl":deque(maxlen=1008),
            "stop_amt":deque(maxlen=1008),
            "press_amt":deque(maxlen=1008),
            "limit_amt":deque(maxlen=1008),
        }
        self.stop_pnl = Decimal("0")
        self.press_pure_pnl = Decimal("0")
        self.press_commission_pnl = Decimal("0")
        self.limit_pnl = Decimal("0")
        self.stop_amt = Decimal("0")
        self.press_amt = Decimal("0")
        self.limit_amt = Decimal("0")
    
    def gather(self):
        self._cache["stop_pnl"].append(self.stop_pnl)
        self._cache["press_pure_pnl"].append(self.press_pure_pnl)
        self._cache["press_commission_pnl"].append(self.press_commission_pnl)
        self._cache["limit_pnl"].append(self.limit_pnl)
        self._cache["stop_amt"].append(self.stop_amt)
        self._cache["press_amt"].append(self.press_amt)
        self._cache["limit_amt"].append(self.limit_amt)
        
        self.stop_pnl = Decimal("0")
        self.press_pure_pnl = Decimal("0")
        self.press_commission_pnl = Decimal("0")
        self.limit_pnl = Decimal("0")
        self.stop_amt = Decimal("0")
        self.press_amt = Decimal("0")
        self.limit_amt = Decimal("0")
        
    def get_stats(self):
        self.gather()
        res = dict()
        res["curr_stop_pnl_perU"] = float(self._cache["stop_pnl"][-1] / self._cache["stop_amt"][-1]) if self._cache["stop_amt"][-1] > 0 else float(0)
        res["tot_stop_pnl_perU"] = float(np.sum(self._cache["stop_pnl"]) / np.sum(self._cache["stop_amt"])) if np.sum(self._cache["stop_amt"]) > 0 else float(0)
        
        res["curr_limit_pnl_perU"] = float(self._cache["limit_pnl"][-1] / self._cache["limit_amt"][-1]) if self._cache["limit_amt"][-1] > 0 else float(0)
        res["tot_limit_pnl_perU"] = float(np.sum(self._cache["limit_pnl"]) / np.sum(self._cache["limit_amt"])) if np.sum(self._cache["limit_amt"]) > 0 else float(0)
        
        res["curr_press_ppnl_perU"] = float(self._cache["press_pure_pnl"][-1] / self._cache["press_amt"][-1]) if self._cache["press_amt"][-1] > 0 else float(0)
        res["curr_press_cpnl_perU"] = float(self._cache["press_commission_pnl"][-1] / self._cache["press_amt"][-1]) if self._cache["press_amt"][-1] > 0 else float(0)
        res["curr_press_perU"] = res["curr_press_ppnl_perU"] + res["curr_press_cpnl_perU"]
        
        res["tot_press_ppnl_perU"] = float(np.sum(self._cache["press_pure_pnl"]) / np.sum(self._cache["press_amt"])) if np.sum(self._cache["press_amt"]) > 0 else float(0)
        res["tot_press_cpnl_perU"] = float(np.sum(self._cache["press_commission_pnl"]) / np.sum(self._cache["press_amt"])) if np.sum(self._cache["press_amt"]) > 0 else float(0)
        res["tot_press_perU"] = res["tot_press_ppnl_perU"] + res["tot_press_cpnl_perU"]
        
        total_amt = np.sum(self._cache["press_amt"]) + np.sum(self._cache["stop_amt"]) + np.sum(self._cache["limit_amt"])
        res["stop_per"] = float(np.sum(self._cache["stop_amt"]) / total_amt) if total_amt > 0 else float(0)
        res["press_per"] = float(np.sum(self._cache["press_amt"]) / total_amt) if total_amt > 0 else float(0)
        res["limit_per"] = float(np.sum(self._cache["limit_amt"]) / total_amt) if total_amt > 0 else float(0)
        
        return res
        
    def update(self, order: PartialOrder):
        raw_order = order.raw_data.get("o")
        if not raw_order:
            return
        if raw_order.get("n"):
            pure_pnl = Decimal(raw_order["rp"])
            commission_pnl = - Decimal(raw_order["n"])
            pnl = pure_pnl + commission_pnl
        else:
            pnl = Decimal(raw_order["rp"])
            pure_pnl = pnl
            commission_pnl = Decimal("0")
        
        if raw_order.get("R") == True and raw_order.get("ot") == "STOP":
            self.stop_pnl += pnl
            if order.xchg_status in OrderStatus.fin_status():
                self.stop_amt += abs(order.filled_amount) * order.avg_filled_price
        elif raw_order.get("R") == True and raw_order.get("ot") == "LIMIT" and raw_order.get("f") == "GTX":
            self.limit_pnl += pnl
            if order.xchg_status in OrderStatus.fin_status():
                self.limit_amt += abs(order.filled_amount) * order.avg_filled_price
        elif raw_order.get("R") == True and raw_order.get("ot") == "LIMIT" and raw_order.get("f") == "GTC":
            self.press_pure_pnl += pure_pnl
            self.press_commission_pnl += commission_pnl
            if order.xchg_status in OrderStatus.fin_status():
                self.press_amt += abs(order.filled_amount) * order.avg_filled_price
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.cache = Cache()
        self.last_ts = 0
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        
        for acc in self.account_names:
            self.direct_subscribe_order_update(
                        symbol_name="binance.btc_busd_swap.usdt_contract.na", account_name=acc)
        
    async def push_influx_data(self, measurement, tag, fields):
        try:
            dt = {
                "timestamp": int(time.time() * 1e3),
                "measurement": measurement,
                "tag": tag,
                "fields": fields
            }
            await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        except:
            traceback.print_exc()
            
    async def on_order(self, order: PartialOrder):
        self.cache.update(order)
    
    async def strategy_core(self):
        while True:
            await asyncio.sleep(0.1)
            curr_ts = time.time() % 600
            if curr_ts < self.last_ts:
                try:
                    res = self.cache.get_stats()
                    self.logger.info(res)
                    for k,v in res.items():
                        self.logger.info(f"{k},{v}")
                        self.loop.create_task(
                                self.push_influx_data(
                                    measurement="analysis",
                                    tag={"sn": "hard press"},
                                    fields={k:float(v)}
                                )
                            )
                except:
                    traceback.print_exc()
            self.last_ts = curr_ts
            
            
if __name__ == '__main__':
    # logging.getLogger().setLevel("WARNING")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
