

from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "preprod")

import json
from collections import defaultdict
from typing import Dict

from xex_mm.dmm_strategy.hedge_inven_manager import HedgeInvenManager
from xex_mm.utils.base import DefaultDict, Order, TransOrder

from xex_mm.utils.enums import OrderStatus as XexOrderStatus

from strategy_base.base import CommonStrategy
import asyncio
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2
from atom.model import BBODepth, OrderSide, OrderPositionSide, OrderType, PartialOrder, OrderStatus
import traceback
from decimal import Decimal
import simplejson

from xex_mm.dmm_strategy.executor import InventoryHedger, InventoryHedger2
import time

from atom.helpers import send_ding_talk
from atom.helpers import ClientIDGenerator

INITIAL_BALANCE = 4600000

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbols = [
            # "binance.btc_usdt_swap.usdt_contract.na",
            # "binance.eth_usdt_swap.usdt_contract.na",
            # "binance.ape_usdt_swap.usdt_contract.na",
            # "binance.gmt_usdt_swap.usdt_contract.na",
            # "binance.xrp_usdt_swap.usdt_contract.na",
            # "binance.matic_usdt_swap.usdt_contract.na",
            # "binance.ada_usdt_swap.usdt_contract.na",
            # "binance.sol_usdt_swap.usdt_contract.na",
            # "binance.gala_usdt_swap.usdt_contract.na",
            # "binance.etc_usdt_swap.usdt_contract.na",
            # "binance.axs_usdt_swap.usdt_contract.na",
            # "binance.doge_usdt_swap.usdt_contract.na",
            # "binance.atom_usdt_swap.usdt_contract.na",
            # "binance.near_usdt_swap.usdt_contract.na",
            # "binance.sand_usdt_swap.usdt_contract.na",
            # "binance.apt_usdt_swap.usdt_contract.na",
            "binance.bnb_usdt_swap.usdt_contract.na",
        ]
        self.hedge_ex = "binance"
        self.mp = dict()

        # NEW HEDGER SUIT
        self.inven_manager = HedgeInvenManager(ex=self.hedge_ex, logger=self.logger)
        self.executor2: Dict[str, InventoryHedger2] = dict()
        
        self.max_lose_ccy2 = float(self.config['strategy']['params']['max_lose_ccy2'])

        self.uid_inven_ccy2: Dict[str, float] = defaultdict(lambda: 0)

        self.hedge_account = "binance.mm19"
        self.hedge_account_balance = None
        self.max_binance_leverage = 1
        
        self.orderId_cache = dict()
        self.quering_id = set()
        
        self.error_counter = 0
        
        self.send = False

    def get_vol_profile_key(self, contract: str):
        return f"cache:lbank_hedge:{contract}_vol_profile"

    async def before_strategy_start(self):
        self.direct_subscribe_order_update(symbol_name=self.symbols[0])
        for symbol in self.symbols:
            self.direct_subscribe_bbo(symbol_name=symbol)
            ex, contract, _, _ = symbol.split(".")

            self.executor2[contract] = InventoryHedger2(contract=contract, ex=ex, max_lose_ccy2=self.max_lose_ccy2,
                                                      VaR_quantile=0.99, short_halflife=600, long_halflife=86400,
                                                      hegde_increment_ccy2=10, min_sharpe=1, logger=self.logger)
            # init orderId cache 
            self.orderId_cache[contract] = 0
            # load cached vol profile
            vol_profile_data, status = await self.redis_get_data(redis_key=self.get_vol_profile_key(contract=contract))
            if not status:
                vol_profile_data = {}

            self.logger.info(f"contract={contract}, vol_profile_data={vol_profile_data}")

            # set cached vol profile
            if vol_profile_data:
                short_ret_var = vol_profile_data.get("short_ret_var")
                if short_ret_var:
                    short_ret_var = float(short_ret_var)
                short_dt = vol_profile_data.get("short_dt")
                if short_dt:
                    short_dt = float(short_dt)
                long_ret_var = vol_profile_data.get("long_ret_var")
                if long_ret_var:
                    long_ret_var = float(long_ret_var)
                long_dt = vol_profile_data.get("long_dt")
                if long_dt:
                    long_dt = float(long_dt)
                self.executor2[contract].on_init(short_ret_var=short_ret_var, short_dt=short_dt,
                                                long_ret_var=long_ret_var, long_dt=long_dt)
                
            cache_data, status = await self.redis_get_data(f"cache:hf_lbank:hedge_inventory_{contract}")
            if not status:
                await self.cache_redis.handler.set(
                    f"cache:hf_lbank:hedge_inventory_{contract}",
                    json.dumps(
                        {
                            "blacklist": 0, 
                            "cta": 0, 
                            "var": 0, 
                            "orderId": 0, 
                            "start_ts": int(time.time()*1e3)}
                        )
                    )
            else:
                im = self.inven_manager.contract_hedge_inven_managers[contract]
                im.blacklist_hedge_inven_manager.inven_ccy1 = float(cache_data["blacklist"])
                im.cta_hedge_inven_manager.inven_ccy1 = float(cache_data["cta"])
                im.VaR_hedge_inven_manager.inven_ccy1 = float(cache_data["var"])
                
                self.orderId_cache[contract] = cache_data["orderId"]
                
        self.send = True
    
    async def redis_get_data(self, redis_key):
        r = await self.cache_redis.handler.get(redis_key)
        if r:
            return simplejson.loads(r, use_decimal=True), True
        else:
            self.logger.warning(f"get data from {redis_key} fail")
            return dict(), False

    async def on_order(self, order: PartialOrder):
        if order.exchange_pair == "SANDUSDT":
            self.logger.info(f"partial order:{order}")
        try:
            todr = self.inven_manager.pending_finish_transorders.get(order.client_id)
            if not todr:
                return
            self.logger.info(f"trans order:{todr.__dict__}")
            ex, contract = todr.iid.split(".")
            symbol = f"binance.{contract}.usdt_contract.na"
            cv = self.get_symbol_config(symbol)["contract_value"]
            
            odr_msg = Order.from_dict(porder=order, torder=todr, contract_value=cv)
            if odr_msg.status == OrderStatus.PartiallyFilled:
                odr_msg.status = XexOrderStatus.PartialFilled
            elif odr_msg.status == OrderStatus.Canceled:
                odr_msg.status = XexOrderStatus.Canceled
            elif odr_msg.status == OrderStatus.Filled:
                odr_msg.status = XexOrderStatus.Filled
            else:
                if odr_msg.status in OrderStatus.fin_status():
                    odr_msg.status = XexOrderStatus.Expired
                else:
                    odr_msg.status = XexOrderStatus.New
            self.inven_manager.on_odr_msg(odr_msg=odr_msg)
        except:
            traceback.print_exc()
    
    async def on_bbo(self, symbol, bbo: BBODepth):
        ex, contract = symbol.split(".")
        try:
            ap = float(bbo.ask[0])
            bp = float(bbo.bid[0])
            self.mp[contract] = (ap + bp) / 2
        except:
            self.logger.critical(traceback.format_exc())
        
    async def query_position(self):
        if not self.hedge_account_balance:
            return
        
        lbank_pos = dict()
        binance_pos = dict()
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            if acc.startswith("binance"):
                cur_position = (await api.contract_position_all(market="usdt_contract", sub_market="na"))
                for contract in cur_position.data:
                    spos = float(cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"])
                    if not self.mp.get(contract):
                        continue
                    self.logger.info(f"{acc}.{contract} position (ccy2): {spos * self.mp.get(contract)}")
                    if self.mp.get(contract) != None:
                        p = self.mp.get(contract)
                        if binance_pos.get(contract) != None:
                            binance_pos[contract] += spos * p
                        else:
                            binance_pos[contract] = spos * p
                    else:
                        self.logger.info(f"{contract} no price")
            else:
                cur_position = (await api.contract_position_all(market="swap", sub_market="na"))
            # self.logger.info(f"position info:{cur_position}") 
                for contract in cur_position.data:
                    spos = float(cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"])
                    p = self.mp.get(contract)
                    self.logger.info(f"{acc}.{contract} position (ccy2): {spos * p if p else None}")
                    if self.mp.get(contract) != None:
                        p = self.mp.get(contract)
                        if lbank_pos.get(contract) != None:
                            lbank_pos[contract] += spos * p
                        else:
                            lbank_pos[contract] = spos * p
                    else:
                        self.logger.info(f"{contract} no price")

        max_hedge_amt = self.hedge_account_balance * self.max_binance_leverage
        self.logger.info(f"max_hedge_amt:{max_hedge_amt}")
        
        total_binance_pos = 0
        for contract, pos in binance_pos.items():
            total_binance_pos += abs(pos)  
        
        for contract in lbank_pos:
            
            mp = self.mp.get(contract)
            if not mp:
                continue
            
            lbank_spos = lbank_pos[contract]

            # new hedger
            exec = self.executor2.get(contract)
            if not exec:
                continue
            inven_ccy2 = lbank_spos
            contract_hedge_inven_manager = self.inven_manager.contract_hedge_inven_managers[contract]
            blacklist_hedge_amt = contract_hedge_inven_manager.blacklist_hedge_inven_manager.inven_ccy1 * exec.blacklist_hedger.prc
            signal_hedge_amt = contract_hedge_inven_manager.cta_hedge_inven_manager.inven_ccy1 * exec.cta_hedger.prc
            VaR_hedge_amt = contract_hedge_inven_manager.VaR_hedge_inven_manager.inven_ccy1 * exec.VaR_hedger.prc
            exec.on_inven_ccy2(inven_ccy2)
            exec.blacklist_hedger.on_blacklist_hedge_amt(blacklist_hedge_amt=blacklist_hedge_amt)
            exec.cta_hedger.on_signal_hedge_amt(signal_hedge_amt=signal_hedge_amt)
            exec.VaR_hedger.on_VaR_hedge_amt(VaR_hedge_amt=VaR_hedge_amt)
            todrs = exec.get_hedge_transorders()
            self.logger.critical(f"contract={contract}")
            for todr in todrs:
                self.logger.critical(f"todr={todr}")
                
                target = total_binance_pos + todr.price * todr.quantity
                if target < max_hedge_amt:
                    await self.base_send_order(todr=todr)
                    total_binance_pos += todr.price * todr.quantity


    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True
    
    async def internal_fail(self, todr: TransOrder, error_code: int):
        self.logger.info(f"internal failed {error_code}")
        try:
            order = PartialOrder(
                account_name=todr.account_name,
                xchg_id=None,
                exchange_pair="",
                client_id=todr.client_id,
                xchg_status=OrderStatus.Failed,
                filled_amount=0,
                avg_filled_price=0,
                commission_fee=0,
                local_ms=int(time.time() * 1e3),
                server_ms=int(time.time() * 1e3),
                raw_data={},
            )
            await self.on_order(order)
        except:
            traceback.print_exc()
    
    async def base_send_order(self, todr: TransOrder):
        try:
            if not self.send:
                return
            ex, contract = todr.iid.split(".")
            mp = self.mp.get(contract)
            if not mp:
                return
            price = mp * (1 + 0.001) if todr.side == 1 else mp * (1 - 0.001)
            volume = todr.quantity
            symbol = f"binance.{contract}.usdt_contract.na"
            if not self.volume_notional_check(symbol=symbol, price=price, qty=volume):
                return
            
            
            cid = ClientIDGenerator.gen_client_id(exchange_name=ex, market="usdt_contract")
            tag = todr.tag.lower()
            
            symbol_cfg = self.get_symbol_config(symbol)
            qt = symbol_cfg["qty_tick_size"]
            
            todr.client_id = tag + cid[len(tag):]
            todr.sent_ts = int(time.time()*1e3)
            self.inven_manager.on_transorder(todr=todr)
        except:
            traceback.print_exc()
        try:
            order = await self.make_order(
                account_name=self.hedge_account,
                symbol_name=symbol,
                price = Decimal(price),
                volume=Decimal(volume) + qt/2,
                side=OrderSide.Buy if todr.side == 1 else OrderSide.Sell,
                position_side=OrderPositionSide.Open,
                order_type=OrderType.IOC,
                client_id=todr.client_id
            )
            self.orderId_cache[contract] = max(self.orderId_cache[contract], int(order.xchg_id))
        except Exception as err:
            self.logger.critical(f"send order err:{err}")
            self.loop.create_task(self.internal_fail(todr=todr, error_code=0))


    async def feed_price(self):
        while True:
            await asyncio.sleep(1)
            try:
                for contract in self.mp:
                    self.executor2[contract].on_prc(prc=self.mp[contract], ts=int(time.time() * 1e3))
            except:
                traceback.print_exc()
                
    async def main_logic(self):
        while True:
            await asyncio.sleep(10)
            try:
                await self.query_position()
            except:
                self.logger.critical(f"error: {traceback.format_exc()}")

    async def update_wyf_signal(self):
        sharpe_data, status = await self.redis_get_data(f"cache:strategy:wangyife_mm_signal_sharpe_ratio")
        if not status:
            sharpe_data = {}

        for symbol in self.symbols:
            ex, contract, _, _ = symbol.split(".")
            ccy1 = contract.split("_")[0]

            uid = "wyf"
            # sharpe
            pair = f"{ccy1}_usdt"
            sharpe = sharpe_data.get(pair)
            if sharpe is not None:
                sharpe = float(sharpe)
                self.logger.info(f"{pair} sharpe: {sharpe}")
                self.executor2[contract].cta_hedger.on_cta_sharpe(name=uid, sharpe=sharpe)
            else:
                self.logger.critical(f"{uid} missing sharpe for {contract}")
            # pos
            signal_data, stauts = await self.redis_get_data(f"cache:strategy:mm_signal_b_{ccy1}usdt")
            self.logger.info(f"{symbol} raw data: {signal_data}")
            for key in signal_data:
                pos = float(signal_data[key]["long_qty"] - signal_data[key]["short_qty"])
                self.logger.info(f"{key} pos: {pos}")
                self.executor2[contract].cta_hedger.on_cta_signal(name=uid, pos=pos)

    async def update_max_drawdown(self):
        while True:
            await asyncio.sleep(10)
            try:

                data, status = await self.redis_get_data(f"cache:hf_lbank:max_drawdown")
                if not status:
                    data = {}

                mdd = data.get("max_drawdown")
                if mdd is not None:
                    mdd = float(mdd)
                    self.logger.info(f"MaxDrawDown: {mdd}")
                    for symbol in self.symbols:
                        ex, contract, _, _ = symbol.split(".")
                        self.executor2[contract].VaR_hedger.on_max_drawdown(max_drawdown=mdd)
                else:
                    self.logger.critical(f"MaxDrawDown missing")
            except:
                traceback.print_exc()

    async def update_blacklist_inven(self):
        while True:
            await asyncio.sleep(10)
            try:
                data, status = await self.redis_get_data(f"cache:hf_lbank:blacklist_position")
                if not status:
                    data = {}

                for symbol in self.symbols:
                    ex, contract, _, _ = symbol.split(".")
                    blacklist_inven_ccy1 = data.get(contract)
                    if blacklist_inven_ccy1 is not None:
                        blacklist_inven_ccy1 = float(blacklist_inven_ccy1)
                        self.executor2[contract].blacklist_hedger.on_blacklist_inven_ccy1(blacklist_inven_ccy1=blacklist_inven_ccy1)
                        self.logger.critical(f"UpdateBlackListInven: contract={contract},blacklist_inven_ccy1={blacklist_inven_ccy1}")
                    else:
                        self.logger.critical(f"UpdateBlackListInven: contract={contract},blacklist_inven_ccy1 missing.")
            except:
                traceback.print_exc()
                
    async def update_cl_signal(self):
        sharpe_data, status = await self.redis_get_data(f"cache:strategy:save_strategy_sharpe")
        if not status:
            sharpe_data = {}

        self.logger.info(f"sharpe_data={sharpe_data}")

        for symbol in self.symbols:
            ex, contract, _, _ = symbol.split(".")
            ccy1 = contract.split("_")[0]

            uid = "cl"
            # sharpe
            key = f"hedge_volumeProfileStdStrategy_{ccy1}"
            sharpe = sharpe_data.get(key)
            if sharpe is not None:
                sharpe = float(sharpe)
                self.logger.info(f"uid={uid}, key={key}, sharpe={sharpe}")
                self.executor2[contract].cta_hedger.on_cta_sharpe(name=uid, sharpe=sharpe)
            else:
                self.logger.critical(f"{uid} missing sharpe for {contract}")
            # pos
            signal_data, stauts = await self.redis_get_data(f"cache:strategy:{key}")
            for key in signal_data:
                pos = float(signal_data[key]["long_qty"] - signal_data[key]["short_qty"])
                self.logger.info(f"uid={uid}, key={key}, pos={pos}")
                self.executor2[contract].cta_hedger.on_cta_signal(name=uid, pos=pos)

    async def update_signal(self):
        while True:
            await asyncio.sleep(10)
            try:
                await self.update_wyf_signal()
                await self.update_cl_signal()
            except:
                traceback.print_exc()
                
    async def update_binance_balance(self):
        while True:
            try:
                api = self.get_exchange_api_by_account(self.hedge_account)
                curr_balance = (await api.account_balance_all(market="usdt_contract", sub_market="na")).data["usdt"]["all"]
                self.hedge_account_balance = curr_balance
            except:
                traceback.print_exc()
            await asyncio.sleep(10)

    async def write_vol_profile(self):
        while True:
            await asyncio.sleep(10)
            try:
                for symbol in self.symbols:
                    ex, contract, _, _ = symbol.split(".")
                    executor = self.executor2.get(contract)
                    if not executor:
                        continue
                    VaR_exec = executor.VaR_hedger
                    vol_profile_data = dict(short_ret_var=VaR_exec.short_daily_ret_quantile_cal.ema_ret_var.get_avg(),
                                            short_dt=VaR_exec.short_daily_ret_quantile_cal.ema_dt.get_avg(),
                                            long_ret_var=VaR_exec.long_daily_ret_quantile_cal.ema_ret_var.get_avg(),
                                            long_dt=VaR_exec.long_daily_ret_quantile_cal.ema_dt.get_avg())
                    await self.cache_redis.handler.set(self.get_vol_profile_key(contract=contract), json.dumps(vol_profile_data))
                    self.logger.info(f"Wrote vol profile to redis for {contract}: {vol_profile_data}")
            except:
                traceback.print_exc()
                
    async def query_binance_orders_with_id(self, api, symbol, orderId):
        res = await api.make_request(
            market="usdt_contract",
            method="GET",
            endpoint="/fapi/v1/allOrders",
            query=dict(
                symbol=symbol,
                orderId=str(orderId),
                limit=str(1000),
                timestamp=str(int(time.time()*1e3))
                ),
            need_sign=True
            
        )
        return res.json
    
    async def query_binance_orders_with_start_ts(self, api, symbol, start_ts):
        res = await api.make_request(
            market="usdt_contract",
            method="GET",
            endpoint="/fapi/v1/allOrders",
            query=dict(
                symbol=symbol,
                startTime=str(start_ts),
                limit=str(1000),
                timestamp=str(int(time.time()*1e3))
                ),
            need_sign=True
        )
        return res.json
                
    async def calculate_position_changed(self, contract, orderId, start_ts):
        pos_chg = {"blacklist":0, "cta":0, "var":0}
        api = self.get_exchange_api(exchange_name="binance")[0]
        ccy1, ccy2, _ = contract.split("_")
        symbol = (f"{ccy1}{ccy2}").upper()
        orders = []
        if orderId:
            while True:
                res = await self.query_binance_orders_with_id(api=api, symbol=symbol, orderId=orderId)
                orders.extend(res[1:])
                orderId = res[-1]["orderId"]
                if len(res) <= 1:
                    break
                await asyncio.sleep(1) 
                
        else:
            res = await self.query_binance_orders_with_start_ts(api=api, symbol=symbol, start_ts=start_ts)
            orders.extend(res)
            if not res:
                return pos_chg, 0
            orderId = res[-1]["orderId"]
            while len(res) > 1:
                res = await self.query_binance_orders_with_id(api=api, symbol=symbol, orderId=orderId)
                orders.extend(res[1:])
                orderId = res[-1]["orderId"]
                await asyncio.sleep(1)
                
        for order in orders:
            for k in pos_chg:
                if order["clientOrderId"].startswith(k):
                    pos_chg[k] += float(order["executedQty"]) if order["side"] == "BUY" else -float(order["executedQty"])
            
        if orders:
            last_orderId = orders[-1]["orderId"]
        else:
            last_orderId = orderId
        
        return pos_chg, last_orderId
    
    
    async def postion_tracking_error_state(self, contract):
        # clear positon and start cache from empty
        im = self.inven_manager.contract_hedge_inven_managers[contract]
        api = self.get_exchange_api(exchange_name="binance")[0]
        symbol = f"binance.{contract}.usdt_contract.na"
        symbol_cfg = self.get_symbol_config(symbol)
        price = self.mp.get(contract)
        if not price:
            self.logger.critical("err but no price info to close position")
            return
        counter = 0
        while True:
            try:
                cur_pos_data = (await api.contract_position(symbol_cfg)).data
                cur_pos = cur_pos_data["long_qty"] - cur_pos_data["short_qty"]
                if cur_pos < Decimal("1e-10"):
                    break
                await self.make_order(
                    account_name=self.hedge_account,
                    symbol_name=symbol,
                    price = Decimal(price * 1.001) if cur_pos < 0 else Decimal(price * 0.999),
                    volume=abs(cur_pos),
                    side=OrderSide.Buy if cur_pos < 0 else OrderSide.Sell,
                    position_side=OrderPositionSide.Close,
                    order_type=OrderType.IOC,
                )
            except:  
                traceback.print_exc()
            counter += 1
            if counter > 100:
                self.logger.critical(f"stuck in position closing, current position for {contract}")
            await asyncio.sleep(0.1)
        
        await self.cache_redis.handler.set(
            f"cache:hf_lbank:hedge_inventory_{contract}",
            json.dumps(
                {
                    "blacklist": 0, 
                    "cta": 0, 
                    "var": 0, 
                    "orderId": 0, 
                    "start_ts": int(time.time()*1e3)}
                )
            )
        im.blacklist_hedge_inven_manager.inven_ccy1 = 0
        im.cta_hedge_inven_manager.inven_ccy1 = 0
        im.VaR_hedge_inven_manager.inven_ccy1 = 0
        
        
    async def inventory_set_redis(self):
        while True:
            await asyncio.sleep(30)
            # wait till no outstanding orders 
            self.send = False
            
            try:
                binance_pos = dict()
                for symbol in self.symbols:
                    _, contract, _, _ = symbol.split(".")
                    binance_pos[contract] = 0
                
                c = 0
                while True:
                    await asyncio.sleep(0.1)
                    all_orders_clear = True
                    for contract in binance_pos:
                        im = self.inven_manager.contract_hedge_inven_managers[contract]
                        all_orders_clear &= (len(im.pending_finish_transorders) == 0)
                    if all_orders_clear:
                        break
                    c += 1
                    if c > 10:
                        self.logger.critical(f"order for {contract} has pending order for too long")
                
                
                for acc in self.account_names:
                    api = self.get_exchange_api_by_account(acc)
                    if acc.startswith("binance"):
                        cur_position = (await api.contract_position_all(market="usdt_contract", sub_market="na"))
                        for contract in cur_position.data:
                            if not self.mp.get(contract):
                                continue
                            spos = float(cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"])
                            binance_pos[contract] += spos

                # compare cache position and query position
                for contract in binance_pos:
                    orderId = self.orderId_cache[contract]
                    im = self.inven_manager.contract_hedge_inven_managers[contract]

                    bl_tokens = im.blacklist_hedge_inven_manager.inven_ccy1
                    cta_tokens = im.cta_hedge_inven_manager.inven_ccy1
                    var_tokens = im.VaR_hedge_inven_manager.inven_ccy1
                    tot = bl_tokens + cta_tokens + var_tokens
                    self.logger.info(f"In strategy cache, bl:{bl_tokens}, cta:{cta_tokens}, var:{var_tokens}, total:{tot}")
                    self.logger.info(f"In strategy query, total:{binance_pos[contract]}")
                    
                    data = {f"binance.{contract}.usdt_contract.na": tot}
                    
                    await self.publish_to_channel(f"""publish_target_position:{self.strategy_name}""", data)
                    
                    await self.cache_redis.handler.set(
                            f"cache:strategy_position:{self.strategy_name}",
                            simplejson.dumps(data, encoding="utf-8")
                        )
                    
                    # no mattter what, calculate using cache in redis to build the position
                    key = f"cache:hf_lbank:hedge_inventory_{contract}"
                    cache_data, status = await self.redis_get_data(key)
                    await self.redis_set_cache(cache_data)
                    if not status:
                        continue
                    # cache_data should look like: {"blacklist": 0, "cta": 0, "var": 0, "orderId": 0, "start_ts": 0}
                    pos_chg, last_orderId = await self.calculate_position_changed(
                        contract=contract, 
                        orderId=int(cache_data["orderId"]), 
                        start_ts=int(cache_data["start_ts"])
                    )
                    cal_bl_tokens = float(cache_data["blacklist"]) + pos_chg["blacklist"]
                    cal_cta_tokens = float(cache_data["cta"]) + pos_chg["cta"]
                    cal_var_tokens = float(cache_data["var"]) + pos_chg["var"]
                    cache_tot = cal_bl_tokens + cal_cta_tokens + cal_var_tokens
                    self.logger.info(f"calculate using redis cache, bl:{cal_bl_tokens}, cta:{cal_cta_tokens}, var:{cal_var_tokens}, total:{cache_tot}")
                    
                    qt = float(self.get_symbol_config(f"binance.{contract}.usdt_contract.na")["qty_tick_size"])
                    if abs(tot - binance_pos[contract]) > qt:
                        if abs(cache_tot - binance_pos[contract]) <= qt:
                            # update cache manager inventory.
                            im.blacklist_hedge_inven_manager.inven_ccy1 = cal_bl_tokens
                            im.cta_hedge_inven_manager.inven_ccy1 = cal_cta_tokens
                            im.VaR_hedge_inven_manager.inven_ccy1 = cal_var_tokens
                            
                            await self.cache_redis.handler.set(
                                f"cache:hf_lbank:hedge_inventory_{contract}",
                                json.dumps(
                                    {
                                        "blacklist": cal_bl_tokens, 
                                        "cta": cal_cta_tokens, 
                                        "var": cal_var_tokens, 
                                        "orderId": last_orderId, 
                                        "start_ts": int(time.time()*1e3)}
                                    )
                                )
                            self.error_counter = 0
                        else:
                            self.error_counter += 1
                            if self.error_counter > 5:
                                await self.postion_tracking_error_state(contract=contract)
                    else:
                        await self.cache_redis.handler.set(
                            f"cache:hf_lbank:hedge_inventory_{contract}",
                            json.dumps(
                                {
                                    "blacklist": bl_tokens, 
                                    "cta": cta_tokens, 
                                    "var": var_tokens, 
                                    "orderId": orderId, 
                                    "start_ts": int(time.time()*1e3)}
                                )
                            )
                        self.error_counter = 0
            except:
                self.logger.critical(traceback.format_exc())
                
            self.send = True
            
    async def handle_query_order(self, oid, order: TransOrder):
        
        async def unit_query_order(order: TransOrder):
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=order.client_id,
                    )
                new_order = res.data
                self.logger.info(f"query order: {new_order.__dict__}")
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
                # else:
                #     await self.handle_cancel_order(oid=oid, order=order)
            except Exception as err:
                self.logger.critical(f"direct check order error: {err}, iid={order.iid}, oid={order.xchg_id}, cid={order.client_id}")
        
        window = 1000
        
        if time.time()*1e3 - order.sent_ts < window or oid in self.quering_id:
            return
        
        self.quering_id.add(oid)
        
        counter = 0
        ex, contract = order.iid.split(".")
        while True:
            if self.inven_manager.contract_hedge_inven_managers[contract].pending_finish_transorders.get(order.client_id) != None:
                if time.time()*1e3 - order.sent_ts < 200:
                    self.quering_id.remove(oid)
                    return
            else:
                self.quering_id.remove(oid)
                return
            counter += 1
            self.loop.create_task(unit_query_order(order))
            await asyncio.sleep(10)
            if counter > 100:
                self.loop.create_task(
                    send_ding_talk(title="LBANK", 
                                    message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: query order too many times({counter}), cid:{order.client_id}, strategy:{self.strategy_name}",
                                    token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                    secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                )
            
    async def check_orders(self):
        while True:
            await asyncio.sleep(60)
            try:
                for symbol in self.symbols:
                    _, contract, _, _ = symbol.split(".")
                    for _, tordr in self.inven_manager.contract_hedge_inven_managers[contract].pending_finish_transorders.items():
                        self.loop.create_task(self.handle_query_order(oid=tordr.client_id, order=tordr))
            except:
                traceback.print_exc()
                

    async def strategy_core(self):
        await asyncio.gather(
            self.feed_price(),
            self.main_logic(),
            self.update_signal(),
            self.update_binance_balance(),
            self.update_max_drawdown(),
            self.update_blacklist_inven(),
            self.write_vol_profile(),
            self.inventory_set_redis(),
            self.check_orders(),
        )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
