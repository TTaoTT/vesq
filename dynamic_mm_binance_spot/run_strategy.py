from collections import deque
from symbol import except_clause
from unittest.mock import AsyncMagicMixin
from xml.etree.ElementTree import TreeBuilder
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2
import logging
import asyncio
from decimal import Decimal
from inspect import trace
import time
import pandas as pd
from asyncio.queues import Queue
import os
import numpy as np
import traceback
import math
from typing import Set, AnyStr, Union, Dict, List
import ujson as json
from orderedset import OrderedSet
import uuid
from asyncio.queues import Queue, LifoQueue
from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from strategy_base.base import CommonStrategy

from xex_mm.strategy_configs.xex_mm_config import PredRefPrcMode, Config, QuoteLevelConfig
from xex_mm.xex_executor.params import ModelParams
from xex_mm.utils.base import Depth, BBO, Direction, Order, TransOrder, OrderManager, MakerTaker, Trade, XexInventoryManager, ReferencePriceCalculator
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig, HeartBeatTolerance
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_inventory_executor import XexMmDeltaNeutralInventoryExecutor
from xex_mm.strategy_configs.obligatory_mm_config import ObligatoryMmConfig

from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard

from xex_mm.utils.parallel import FunctorDistributionEngine
import math

from xex_mm.xex_depth.xex_depth import XExDepth, XExBBO
from xex_mm.xex_executor.order_executor2 import OrderExecutorTest2
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor

from xex_mm.signals.LocalCalculator import LocalCalculatorBase
from xex_mm.signals.SignalCalculator import SignalCalculatorBase

from xex_mm.utils.contract_mapping import ContractMapper

ACC_LOAD_KEY = "x-mbx-order-count-10s"
IP_LOAD_KEY = "x-mbx-used-weight-1m"
ACC_LOAD_1D_KEY = "x-mbx-order-count-1d"

EMAIL_CONFIG = {
    "binance.mm01": "mktbinance01@dcmammoth.com",
    "binance.mm02": "mktbinance02_virtual@wj9320ymnoemail.com",
    "binance.mm03": "mktbinance03_virtual@1phc7nwfnoemail.com",
    "binance.mm04": "mktbinance04_virtual@dh6dnun2noemail.com",
    "binance.mm05": "mktbinance05_virtual@otyjv1utnoemail.com",
    "binance.mm06": "mktbinance06_virtual@pnfu0clonoemail.com",
    "binance.mm07": "mktbinance07_virtual@3uyf9e8fnoemail.com",
    "binance.mm08": "mktbinance08_virtual@1qadxsienoemail.com",
    "binance.mm09": "mktbinance09_virtual@0tk8l3zinoemail.com",
    "binance.mm10": "mktbinance10_virtual@o68aw3sxnoemail.com",
    "binance.mm11": "mktbinance11_virtual@8fge8pjhnoemail.com",
    "binance.mm12": "mktbinance12_virtual@3vvwmr2wnoemail.com",
    "binance.mm13": "mktbinance13_virtual@bkjutzssnoemail.com",
    "binance.mm14": "mktbinance14_virtual@ad0jt3glnoemail.com",
    "binance.mm15": "mktbinance15_virtual@v0baiy1vnoemail.com",
    "binance.mm16": "mktbinance16_virtual@cv36czudnoemail.com",
    "binance.mm17": "mktbinance17_virtual@bahn1s0onoemail.com",
    "binance.mm18": "mktbinance18_virtual@5prx4hlunoemail.com",
    "binance.mm19": "mktbinance19_virtual@2idrq2vknoemail.com",
    "binance.mm20": "mktbinance20_virtual@vg7ms7emnoemail.com",
    "binance.mm21": "binancemkt21_virtual@esqcjbc6noemail.com",
    "binance.mm22": "binancemkt22_virtual@za0x39o9noemail.com",
    "binance.mm23": "binancemkt23_virtual@kkt6zfxinoemail.com",
    "binance.mm24": "binancemkt24_virtual@tdcnfg63noemail.com",
    "binance.mm25": "binancemkt25_virtual@spthu4c8noemail.com",
    "binance.mm26": "binancemkt26_virtual@mxgrc823noemail.com",
    "binance.mm27": "binancemkt27_virtual@mfsaiz9hnoemail.com",
    "binance.mm28": "binancemkt28_virtual@q9n2vs15noemail.com",
    "binance.mm29": "binancemkt29_virtual@9hz280ccnoemail.com",
    "binance.mm30": "binancemkt30_virtual@ado6vnldnoemail.com",
    "binance.mm31": "binancemkt31_virtual@g33wjb2onoemail.com",
    "binance.mm32": "binancemkt32_virtual@ekpuqbb5noemail.com",
    "binance.mm33": "binancemkt33_virtual@j6e1qytonoemail.com",
    "binance.mm34": "binancemkt34_virtual@fbkqiawwnoemail.com",
    "binance.mm35": "binancemkt35_virtual@cau2msmsnoemail.com",
    "binance.mm36": "binancemkt36_virtual@kj15h46rnoemail.com",
    "binance.mm37": "binancemkt37_virtual@bslz6f0vnoemail.com",
    "binance.mm38": "binancemkt38_virtual@4p5mohbdnoemail.com",
    "binance.mm39": "binancemkt39_virtual@8gjhtnnjnoemail.com",
    "binance.mm40": "binancemkt40_virtual@4tsbgki3noemail.com",
    "binance.mm41": "binancemkt41_virtual@nledklnjnoemail.com",
    "binance.mm42": "binancemkt42_virtual@unmzd6kpnoemail.com",
    "binance.mm43": "binancemkt43_virtual@buvijbennoemail.com",
    "binance.mm44": "binancemkt44_virtual@cdep3qtanoemail.com",
    "binance.mm45": "binancemkt45_virtual@r0kmn72xnoemail.com",
    "binance.mm46": "binancemkt46_virtual@v60ymgpinoemail.com",
    "binance.mm47": "binancemkt47_virtual@mnyywze1noemail.com",
    "binance.mm48": "binancemkt48_virtual@giqgfjx7noemail.com",
    "binance.mm49": "binancemkt49_virtual@j4bkc34dnoemail.com",
    "binance.mm50": "binancemkt50_virtual@xk76kbernoemail.com",
    "binance.mm51": "binancemkt51_virtual@yc7mikeinoemail.com",
    "binance.mm52": "binancemkt52_virtual@mvawsq50noemail.com",
    "binance.mm53": "binancemkt53_virtual@96ythoy7noemail.com",
    "binance.mm54": "binancemkt54_virtual@67n5zn1fnoemail.com",
    "binance.mm55": "binancemkt55_virtual@u41wvjdynoemail.com",
    "binance.mm56": "binancemkt56_virtual@e8j7a1i2noemail.com",
    "binance.mm57": "binancemkt57_virtual@3mmrz2aynoemail.com",
    "binance.mm58": "binancemkt58_virtual@3l3hg2yynoemail.com",
    "binance.mm59": "binancemkt59_virtual@tjt4h2afnoemail.com",
    "binance.mm60": "binancemkt60_virtual@wbtky4fgnoemail.com"
}

ACC_TRANSFER_MAP = {
    "binance.mm04":"binance.mm14",
    "binance.mm05":"binance.mm15",
    "binance.mm06":"binance.mm16",
    "binance.mm07":"binance.mm17",
    "binance.mm08":"binance.mm18",
    "binance.mm09":"binance.mm19",
    "binance.mm10":"binance.mm20",
    "binance.mm14":"binance.mm04",
    "binance.mm15":"binance.mm05",
    "binance.mm16":"binance.mm06",
    "binance.mm17":"binance.mm07",
    "binance.mm18":"binance.mm08",
    "binance.mm19":"binance.mm09",
    "binance.mm20":"binance.mm10",
}

ACC_MAX_MAP = {
    "binance.mm04":200,
    "binance.mm05":50,
    "binance.mm06":50,
    "binance.mm07":50,
    "binance.mm08":50,
    "binance.mm09":50,
    "binance.mm10":50,
    "binance.mm11":50,
    "binance.mm12":50,
    "binance.mm13":50,
    "binance.mm14":50,
    "binance.mm15":50,
    "binance.mm16":50,
    "binance.mm17":50,
    "binance.mm18":50,
    "binance.mm19":50,
    "binance.mm20":50,
}

ACC_MAX_1D_MAP = {
    "binance.mm04":200000,
    "binance.mm05":160000,
    "binance.mm06":160000,
    "binance.mm07":160000,
    "binance.mm08":160000,
    "binance.mm09":160000,
    "binance.mm10":160000,
    "binance.mm11":160000,
    "binance.mm12":160000,
    "binance.mm13":160000,
    "binance.mm14":160000,
    "binance.mm15":160000,
    "binance.mm16":160000,
    "binance.mm17":160000,
    "binance.mm18":160000,
    "binance.mm19":160000,
    "binance.mm20":160000,
}

IP_MAX_MAP = {
    "trader01":2400,
    "trader02":2400,
    "trader03":2400,
    "trader04":2400,
    "trader05":2400,
    "trader06":2400,
    "trader07":2400,
    "trader08":2400,
}


def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class LoadBalancer:
    def __init__(self, ccy1: str, ccy2: str, logger) -> None:
        self._position_map = dict()
        self._balance_map = dict()
        self._account_load_map = dict()
        self._ip_load_map = dict()
        self._trader_map = dict()
        self._available_map = dict()
        
        self.ccy1 = ccy1
        self.ccy2 = ccy2
        
        self.send_counter = 0
        self.cancel_counter = 0
        self.query_counter = 0
        
        self.ban_name = set()
        self.ban_trader_name = set()
        
        self.logger = logger
        
        self.account_count = 0
        self.trader_count = 0
        
        self.max_task = 0
                
    def add_send(self):
        self.send_counter += 1
        
    def sub_send(self):
        self.send_counter -= 1
        
    def add_cancel(self):
        self.cancel_counter += 1
        
    def add_query(self):
        self.query_counter += 1
        
    def ban_account(self, account_name):
        if account_name:
            self.ban_name.add(account_name)
            
    def ban_trader(self, trader):
        if trader:
            self.ban_trader_name.add(trader)
    
    def free_account(self, account_name):
        if account_name in self.ban_name:
            self.ban_name.remove(account_name)
            
    def free_trader(self, trader):
        if trader in self.ban_trader_name:
            self.logger.info(f"free trader:{trader}")
            self.ban_trader_name.remove(trader)
            
    def init_account(self, account_name):
        self._position_map[account_name] = 0
        self._balance_map[account_name] = 0
        self._account_load_map[account_name] = 0
        self._available_map[account_name] = dict()
        self._available_map[account_name][self.ccy1] = 0
        self._available_map[account_name][self.ccy2] = 0
        
    def setup(self, acc_list: list, trader_list: list):
        for acc in acc_list:
            self.init_account(account_name=acc)
            self.account_count += 1
        
        for trader in trader_list:
            self._ip_load_map[trader] = 0
            self._trader_map[trader] = 0
            self.trader_count += 1
            
    def switch_account(self, working_acc, backup_acc):
        if working_acc in self._account_load_map:
            self._account_load_map.pop(working_acc)
            self._position_map.pop(working_acc)
            self._balance_map.pop(working_acc)
            self._available_map.pop(working_acc)
            
            self.init_account(account_name=backup_acc)
            self.ban_account(backup_acc)
            self.free_account(working_acc)
        else:
            self.logger.warning(f"account not working:{working_acc}")
            
    def get_acc_list(self):
        return sorted(self._account_load_map, key=self._account_load_map.get)
        
    def get_trader_list(self):
        return sorted(self._trader_map, key=self._trader_map.get)
    
    def get_trader(self):
        for trader in self.get_trader_list():
            if trader not in self.ban_trader_name and self._ip_load_map[trader] < 80:
                return trader
            
    def add_trader_count(self, trader):
        self._trader_map[trader] += 1
        cur_max = max(self._trader_map.values())
        if cur_max > self.max_task:
            self.max_task = cur_max
            self.logger.info(f"current max is {cur_max}")
        
    def sub_trader_count(self, trader):
        self._trader_map[trader] -= 1
        self.free_trader(trader)
     
    def update_available_balance(self, acc: str, available_balance: float, ccy: str):
        if self._available_map.get(acc):
            self._available_map[acc][ccy] = available_balance
        
    def split_order(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            if acc in self.ban_name:
                continue
            new_order = order.clone()
            if order.side == Direction.long:
                temp_qty = self._available_map[acc][ccy2] - 100
                if temp_qty < 0:
                    continue
                if total_qty*order.price > temp_qty:
                    new_order.quantity = temp_qty / order.price
                    new_order.account_name = acc
                    total_qty -= temp_qty / order.price
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break
            elif order.side == Direction.short:
                temp_qty = self._available_map[acc][ccy1] - 0.005
                if temp_qty < 0:
                    continue
                if total_qty > temp_qty:
                    new_order.quantity = temp_qty
                    new_order.account_name = acc
                    total_qty -= temp_qty
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break

        return splited_orders
        
    def send_order(self, order: TransOrder):
        if not order.account_name:
            raise RuntimeError("order with no account name")
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if order.side == Direction.long:
            self._available_map[order.account_name][ccy2] -= order.price * order.quantity
        elif order.side == Direction.short:
            self._available_map[order.account_name][ccy1] -= order.quantity
        else:
            raise NotImplementedError()
            
        if self._available_map[order.account_name][ccy1] < 0 or self._available_map[order.account_name][ccy2] < 0:
            raise RuntimeError("order routing wrong") 
               
    def finished_order(self, torder: TransOrder, order_obj: Order):
        _, contract = torder.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if torder.side == Direction.long:
            self._available_map[torder.account_name][ccy2] += torder.price * torder.quantity
            self._available_map[torder.account_name][ccy2] -= order_obj.avg_fill_prc * order_obj.filled_qty
            self._available_map[torder.account_name][ccy1] += order_obj.filled_qty
        elif torder.side == Direction.short:
            self._available_map[torder.account_name][ccy1] += torder.quantity
            self._available_map[torder.account_name][ccy1] -= order_obj.filled_qty
            self._available_map[torder.account_name][ccy2] += order_obj.avg_fill_prc * order_obj.filled_qty
        else:
            raise NotImplementedError()
        
    def update_balance(self, acc:str, balance:float):
        self._balance_map[acc] = balance
    
    def update_position(self, acc: str, chg_qty: float):
        self._position_map[acc] += chg_qty
    
    def update_limit(self, acc: str, trader_name: str, acc_load: Union[str, None], ip_load: Union[str, None]):
        if acc_load != None:
            self._account_load_map[acc] = int(int((acc_load)) / ACC_MAX_MAP[acc] * 100)
        if ip_load != None:
            self._ip_load_map[trader_name] = int(int(ip_load) / IP_MAX_MAP[trader_name] * 100)

    def reduce_count(self, reduce: float):
        self._account_load_map[self.get_acc_list()[-1]] -= reduce
        self._ip_load_map[sorted(self._ip_load_map, key=self._ip_load_map.get)[-1]] -= reduce
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.send_time = deque(maxlen=1000)
        self.start_ts = time.time()
        self.oid_to_cid = dict()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.bnd_amt_in_coin = Decimal(self.config['strategy']['params']['bnd_amt_in_coin'])
        self.unknown_order_dict = dict()
        self.cfg: Config = ...
        self._contract_mapper = ContractMapper()
        self._dynamic_mm_order_manager = OrderManager()
        self._snap_hedge_order_manager = OrderManager()
        self._rebalance_order_manager = OrderManager()
        self.ws_queue = {
            "trade": Queue(),
            "order": Queue(),
            "orderbook": LifoQueue(),
            "bbo": LifoQueue(),
        }
        
        self.traders = [
                "trader01",
                "trader02",
                "trader03",
                "trader04",
                "trader05",
                "trader06",
                "trader07",
                "trader08",
            ]

        
        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()
        
        self.transfer_out_time = dict()
    
    # preparation
    def init_before_strategy_starts(self):
        self._prev_bbo: BBO = ...
        self.dyn_mm_iids: List[str] = ...

        self._prev_ref_prc: Dict = dict()
        self._ref_prc_map: Dict = dict()
        self._xex_ref_prc_map: Dict = dict()
        self._ret_map: Dict = dict()

        self._dynamic_mm_exchanges = set()
        self._snap_hedge_exchanges = set()
        self._exchanges = set()
        self._xex = "xex"
        self.cfg = Config(
            CONTRACT=self.contract,
            DYNAMIC_MM_EXCHANGES=[
                "binance_spot",
            ],
            SNAP_HEDGE_EXCHANGES=[
            ],
            ART=1e3,
            SIGMAT=1e4,
            HST=1e4,
            RETT=1e3,
            REFPRCT=1e5,
            INVENT=1e5,
            DMT=1e3,
            TRL_COUNT=1000,
            REF_PRC_RESET_SIGMA_MULTIPLIER=1e10,
            TAU_LB=100,
            LATENCYT=100,
            EVENT_COUNT_HALFLIFE=100,
            BBO_SWITCH=True,
            FOLDER_NAME="",
            TRADE_DEFENSE_CANCEL_AR_MULTIPLIER=3,
            DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER=3,
            DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER=3,
            BETA=1e3,
            T=10000,
            PRED_ON=False,
            PRED_REF_PRC_MODE=PredRefPrcMode.xex,
            PROFIT_MARGIN_RETURN=1e-5,
            MDL_PARAMS=ModelParams(
                pred_on=False,
                trade_symbol_to_related_symbols_map={}, 
                trade_symbol_to_model_path_map={},
            ),
            MSG="",
            QUOTE_LVL_CONFIGS={"binance_spot": [
                # QuoteLevelConfig(TARGET_FILL_PROB=0.00000001, QTY_UB=60, CANCEL_TOL=1),
                # QuoteLevelConfig(TARGET_FILL_PROB=0.0000001, QTY_UB=2000, CANCEL_TOL=1),
                # QuoteLevelConfig(TARGET_FILL_PROB=0.000001, QTY_UB=2000, CANCEL_TOL=1),
                # QuoteLevelConfig(TARGET_FILL_PROB=0.00001, QTY_UB=2000, CANCEL_TOL=1),
                
                QuoteLevelConfig(TARGET_FILL_PROB=0.1, QTY_UB=1000, CANCEL_TOL=10),
                QuoteLevelConfig(TARGET_FILL_PROB=0.01, QTY_UB=1000, CANCEL_TOL=20),
                # QuoteLevelConfig(TARGET_FILL_PROB=0.001, QTY_UB=1000, CANCEL_TOL=30),
                # QuoteLevelConfig(TARGET_FILL_PROB=0.0001, QTY_UB=1000, CANCEL_TOL=40),
            ]},
            QUOTE_FILL_PROB_CUTOFFS={"binance_spot": 0},
            QUOTE_DEFENSE_T=1000,
        )
        self._ref_prc_xex_bbo = XExBBO(contract=self.contract)
        self._post_cost_ref_prc_xex_bbo = XExBBO(contract=self.contract)
        self._snap_hedge_xex_depth = XExDepth(contract=self.contract)
        self._depth_map = dict()

        self.signal_calculator_map = dict()
        mdl_params = self.cfg.MDL_PARAMS
        for trd_sym, rel_syms in mdl_params.trade_symbol_to_related_symbols_map.items():
            self.signal_calculator_map[trd_sym] = SignalCalculatorBase(pred_on=mdl_params.pred_on,
                                                                       trade_symbol=trd_sym,
                                                                       related_symbols=rel_syms,
                                                                       params=...)
        self.local_calculator = LocalCalculatorBase(
            art=self.cfg.ART,
            sigmat=self.cfg.SIGMAT,
            hst=self.cfg.HST,
            rett=self.cfg.RETT,
            refprct=self.cfg.REFPRCT,
            latencyt=self.cfg.LATENCYT,
            invent=self.cfg.INVENT,
            dmt=self.cfg.DMT,
            event_count_halflife=self.cfg.EVENT_COUNT_HALFLIFE,
            trl_count_halflife=self.cfg.TRL_COUNT,
            quote_defense_t=self.cfg.QUOTE_DEFENSE_T,
            trade_stability_buffer_window=1
        )

        self.executor = OrderExecutorTest2(
            contract=self.contract,
            amt_bnd=0,  # This is handled when building depth
            pred_on=self.cfg.PRED_ON,
            pred_ref_prc_mode=self.cfg.PRED_REF_PRC_MODE,
            beta=self.cfg.BETA,
            ref_prc_reset_sigma_multiplier=self.cfg.REF_PRC_RESET_SIGMA_MULTIPLIER,
            tau_lb=self.cfg.TAU_LB,
            T=self.cfg.T,
            mdl_params=self.cfg.MDL_PARAMS,
            local_cal=self.local_calculator,
            quote_lvl_cfgs=self.cfg.QUOTE_LVL_CONFIGS,
            quote_fill_prob_cfgs=self.cfg.QUOTE_FILL_PROB_CUTOFFS
        )
        if self.cfg.PRED_ON:
            for trd_sym, model_path in self.cfg.MDL_PARAMS.trade_symbol_to_model_path_map.items():
                self.signal_calculator_map[trd_sym].load_ref_prc_model(pickle_address=model_path)
                ex, contract = trd_sym.split(".")
                self.executor.update_signal_cal(signal_cal=self.signal_calculator_map[trd_sym], ex=ex)

        """ Snap Hedge Executor """
        self._delta_neutral_executor = XexMmDeltaNeutralExecutor(contract=self.cfg.CONTRACT,
                                                                 profit_margin_return=self.cfg.PROFIT_MARGIN_RETURN)

        self._dynamic_mm_exchanges = set(self.cfg.DYNAMIC_MM_EXCHANGES)
        self._snap_hedge_exchanges = set(self.cfg.SNAP_HEDGE_EXCHANGES)
        self._exchanges = self._dynamic_mm_exchanges | self._snap_hedge_exchanges

        for ex in self.cfg.SNAP_HEDGE_EXCHANGES + self.cfg.DYNAMIC_MM_EXCHANGES:
            makers_fee, takers_fee = FeeConfig().get_fee(ex=ex, contract=self.cfg.CONTRACT)
            self._delta_neutral_executor.update_fee_rate(contract=self.cfg.CONTRACT, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)

        self.iids = set()
        for ex in self.cfg.DYNAMIC_MM_EXCHANGES:
            self.iids.add(f"{ex}.{self.cfg.CONTRACT}")
        for ex in self.cfg.SNAP_HEDGE_EXCHANGES:
            self.iids.add(f"{ex}.{self.cfg.CONTRACT}")
        self._prc_precision = dict()
        for iid in self.iids:
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            self._prc_precision[iid] = price_precision[pp_iid]

        self._inventory_manager = XexInventoryManager(contract=self.cfg.CONTRACT)
        
        self.initial_account = [
                "binance.mm04",
                "binance.mm05",
                "binance.mm06",
                "binance.mm07",
                "binance.mm08",
                "binance.mm09",
                "binance.mm10",
                ]
        ccy = self.cfg.CONTRACT.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.load_balancer = LoadBalancer(ccy1=ccy1, ccy2=ccy2,logger=self.logger)
        self.load_balancer.setup(
            acc_list=self.initial_account,
            trader_list=self.traders
        )
        
        self.canceling_id = set()
        self.quering_id = set()
        
        self.send = False
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        self.init_before_strategy_starts()
        for acc in self.account_names:    
            ex, _ = acc.split(".")
            ccy = self.cfg.CONTRACT.split("_")
            if self.market == "spot":
                contract = f"{ccy[0]}_{ccy[1]}"
            else:
                contract = self.cfg.CONTRACT
            symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"
            self.direct_subscribe_order_update(symbol_name=symbol,account_name=acc)
            await asyncio.sleep(1)
        
        for iid in self.iids:

            symbol = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            symbol = f"{symbol}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            bnd = self.bnd_amt_in_coin / symbol_cfg["contract_value"]
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_agg_trade(symbol_name=symbol)
            self.direct_subscribe_bbo(symbol_name=symbol)
        
        self.loop.create_task(self.handle_ws_update())

        self.loop.create_task(
            self.subscribe_to_channel(
                [f"mq:hf_signal:send_response:{self.strategy_name}"],
                self.process_send_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                [f"mq:hf_signal:cancel_response:{self.strategy_name}"],
                self.process_cancel_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                [f"mq:hf_signal:query_response:{self.strategy_name}"],
                self.process_query_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                [f"mq:hf_signal:pong:{self.strategy_name}"],
                self.process_pong
                )
            )
        
        self.send_response = dict()
        self.cancel_response = dict()
        self.query_response = dict()
        
        counter = 0
        while True:
            await asyncio.sleep(1)
            counter += 1
            exs = []
            for ex in self.cfg.DYNAMIC_MM_EXCHANGES:
                exs.append(ex)
            for ex in self.cfg.SNAP_HEDGE_EXCHANGES:
                exs.append(ex)
            
            has_price = True
            for ex in exs:
                if self._ref_prc_map.get((self.cfg.CONTRACT, ex)) == None:
                    has_price = False
                    
            if has_price:
                await self.account_preparation()
                break
    
            if counter > 100:
                self.logger.critical("no price info coming")
                exit()
        
        self.logger.critical("start......")
        
        self.send = True
    
    async def account_preparation(self):
        ccy = self.cfg.CONTRACT.split("_")
        ccy1 = 0
        ccy2 = 0
        
        for dymm_ex in self.cfg.DYNAMIC_MM_EXCHANGES:
            ref_prc = self._ref_prc_map.get((self.cfg.CONTRACT, dymm_ex))
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                ex, _ = acc.split(".")
                if dymm_ex.endswith("_spot"):
                    contract = f"{ccy[0]}_{ccy[1]}"
                else:
                    contract = self.cfg.CONTRACT
                symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"
                try:
                    await api.flash_cancel_orders(self.get_symbol_config(symbol))
                except:
                    pass
                cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                if ACC_TRANSFER_MAP[acc] in self.account_names and acc in self.initial_account:
                    backup_api = self.get_exchange_api_by_account(ACC_TRANSFER_MAP[acc])
                    backup_balance = (await backup_api.account_balance(self.get_symbol_config(symbol))).data
                    
                    cur_amt = float(cur_balance[ccy[0]]["available"]) * ref_prc + float(cur_balance[ccy[1]]["available"])
                    backup_amt = float(backup_balance[ccy[0]]["available"]) * ref_prc + float(backup_balance[ccy[1]]["available"])
                    if cur_amt < backup_amt:
                        self.loop.create_task(self.switch_account(account_name=ACC_TRANSFER_MAP[acc]))
                        cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                
                self.load_balancer.update_available_balance(acc=acc, available_balance=float(cur_balance[ccy[0]]["available"]), ccy=ccy[0])
                self.load_balancer.update_available_balance(acc=acc, available_balance=float(cur_balance[ccy[1]]["available"]), ccy=ccy[1])
                
                ccy1 += float(cur_balance[ccy[0]]["available"])
                ccy2 += float(cur_balance[ccy[1]]["available"])
                await asyncio.sleep(0.5)
            
            ccy1 *= ref_prc
            
            if ccy1 / (ccy1 + ccy2) < 0.5:
                side = Direction.long
                qty = ((ccy1 + ccy2) / 2 - ccy1) / ref_prc
                price = ref_prc * (1 + 0.03)
            else:
                side = Direction.short
                qty = (ccy1 - (ccy1 + ccy2) / 2) / ref_prc
                price = ref_prc * (1 - 0.03)
                
            order = TransOrder(
                side=side,
                p=price,
                q=qty,
                iid=f"{dymm_ex}.{self.cfg.CONTRACT}",
                floor=0,
                maker_taker=MakerTaker.taker,
            )
            order.generate_symbol(self._contract_mapper)
            self.logger.info(f"big order: {order.__dict__}")
            orders = self.load_balancer.split_order(order=order)
            for order in orders:
                self.logger.info(f"order: {order.__dict__}")
                self.load_balancer.send_order(order=order)
                cid = self.trim_order_before_sending(order)
                self._rebalance_order_manager.add_trans_order(order=order, oid=cid)
                self.loop.create_task(self.base_send_order(order=order, order_manager=self._rebalance_order_manager))
                    
            await asyncio.sleep(3)
        
        self.ccy1_in_coin = 0
        self.ccy2_amt = 0
        for dymm_ex in self.cfg.DYNAMIC_MM_EXCHANGES:
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                ex, _ = acc.split(".")
                if dymm_ex.endswith("_spot"):
                    contract = f"{ccy[0]}_{ccy[1]}"
                else:
                    contract = self.cfg.CONTRACT
                symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"

                cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                
                self.ccy1_in_coin += float(cur_balance[ccy[0]]["all"])
                self.ccy2_amt += float(cur_balance[ccy[1]]["all"])

                self.logger.info(f"account :{acc} starts with {float(cur_balance[ccy[0]]['all'])} {ccy[0]}, {float(cur_balance[ccy[1]]['all'])} {ccy[1]}")
        
        self.logger.info(f"starts with totoal {self.ccy1_in_coin} {ccy[0]}, {self.ccy2_amt} {ccy[1]}")
        
    
    # async def subscribe_to_channel(self, channel_names, call_back):
    #     while True:
    #         dt = await self.cache_redis.handler.brpop(channel_names[0])
    #         await call_back(*dt)

    # async def publish_to_channel(self, channel_name, payload):
    #     if not isinstance(payload, str):
    #         payload = json.dumps(payload)
    #     await self.cache_redis.handler.lpush(channel_name, payload)
    
    async def publish_to_channel(self, channel_name, payload):
        if not isinstance(payload, str):
            payload = json.dumps(payload)
        await self.oms_redis_agent.handler.publish(channel_name, payload)

    async def subscribe_to_channel(self, channel_names, call_back):
        """
        call_back:

        async def process_xxx(self, channel_name, msg):
            ...
        """
        p = self.oms_redis_agent.handler.pubsub()
        if not isinstance(channel_names, (list, tuple)):
            channel_names = [channel_names]
        await p.subscribe(*channel_names)
        async for msg in p.listen():
            if msg["type"] != "message":
                continue
            await call_back(msg["channel"], msg["data"])
    
        
    # websocket handling
    async def on_order(self, order: PartialOrder):
        try:
            await self.ws_queue["order"].put(order)
        except:
            traceback.print_exc()
    
    async def on_agg_trade(self, symbol, trade: PublicTrade):
        try:
            await self.ws_queue["trade"].put((symbol, trade))
        except:
            traceback.print_exc()
        
    async def on_bbo(self, symbol, bbo: BBODepth):
        try:
            await self.ws_queue["bbo"].put((symbol, bbo))
        except:
            traceback.print_exc()
    
    async def on_orderbook(self, symbol, orderbook):
        try:
            await self.ws_queue["orderbook"].put((symbol, orderbook))
        except:
            traceback.print_exc()
        
    def handle_order(self, order: PartialOrder):
        cid = order.client_id
        
        """ Rebalance """
        if self._rebalance_order_manager.contains_trans_order(oid=cid):
            torder = self._rebalance_order_manager.get_trans_order(oid=cid)
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=torder.iid)
            order_obj: Order = Order.from_dict(
                porder=order,
                torder=torder,
                contract_value=self.get_symbol_config(f"{pp_iid}.{self.market}.{self.sub_market}")["contract_value"]
                )
            """ Clean up """
            if order_obj.status in OrderStatus.fin_status():
                self.load_balancer.finished_order(torder=torder, order_obj=order_obj)
                self._rebalance_order_manager.pop_trans_order(oid=cid)
                self._rebalance_order_manager.pop_order(oid=cid)

        """ Dynamic MM  """
        if self._dynamic_mm_order_manager.contains_trans_order(oid=cid):
            torder = self._dynamic_mm_order_manager.get_trans_order(oid=cid)
            iid=torder.iid
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            order_obj: Order = Order.from_dict(
                porder=order, 
                torder=torder,
                contract_value=self.get_symbol_config(f"{pp_iid}.{self.market}.{self.sub_market}")["contract_value"]
                )
            """ Update Inventory """
            hedge_trans_orders = []
            if order_obj.filled_qty > 0:
                qty_chg = self._dynamic_mm_order_manager.get_order_inc_fill_qty_in_tokens(order=order_obj)
                if qty_chg > 0:
                    self.update_inventory(iid=iid, chg_qty=order_obj.side * qty_chg, ts=order_obj.local_time)
                    xex_iid = f"xex.{order_obj.contract}"
                    self.update_inventory(iid=xex_iid, chg_qty=order_obj.side * qty_chg, ts=order_obj.local_time)

                    """ Hedge Order """
                    takers_fee, makers_fee = self._delta_neutral_executor.get_fee_rate(contract=order_obj.contract, ex=order_obj.ex)
                    _, hedge_trans_orders = self._delta_neutral_executor.hedge_increment(
                        inc_fill_qty_in_tokens=qty_chg, prc=order_obj.avg_fill_prc, maker_fee=makers_fee,
                        d=order_obj.side)

            self._dynamic_mm_order_manager.update_order(order=order_obj)

            """ Clean up """
            if order_obj.status in OrderStatus.fin_status():
                self.load_balancer.finished_order(torder=torder, order_obj=order_obj)
                self._dynamic_mm_order_manager.pop_trans_order(oid=cid)
                self._dynamic_mm_order_manager.pop_order(oid=cid)

            for o in hedge_trans_orders:
                o.generate_symbol(self._contract_mapper)
                self.send_snap_hedge_order(order=o)

            return

        """ Snap Hedge """
        if self._snap_hedge_order_manager.contains_trans_order(oid=cid):
            torder = self._snap_hedge_order_manager.get_trans_order(oid=cid)
            iid = torder.iid
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            order_obj: Order = Order.from_dict(
                porder=order, 
                torder=torder,
                contract_value=self.get_symbol_config(f"{pp_iid}.{self.market}.{self.sub_market}")["contract_value"]
                )

            """ Update inventory """
            if order_obj.filled_qty > 0:
                qty_chg = self._snap_hedge_order_manager.get_order_inc_fill_qty_in_tokens(order=order_obj)
                if qty_chg > 0:
                    self.update_inventory(iid=iid, chg_qty=order_obj.side * qty_chg, ts=order_obj.local_time)
                    xex_iid = f"xex.{order_obj.contract}"
                    self.update_inventory(iid=xex_iid, chg_qty=order_obj.side * qty_chg, ts=order_obj.local_time)
            self._snap_hedge_order_manager.update_order(order=order_obj)

            """ Clean up """
            if order_obj.status in OrderStatus.fin_status():
                self.load_balancer.finished_order(torder=torder, order_obj=order_obj)
                if self._snap_hedge_order_manager.contains_trans_order(oid=cid):
                    self._snap_hedge_order_manager.pop_trans_order(oid=cid)
                    self._snap_hedge_order_manager.pop_order(oid=cid)

            """ Snap cancel """
            if self._snap_hedge_order_manager.contains_trans_order(oid=cid):
                o = self._snap_hedge_order_manager.trans_order_at(oid=cid)
                self.loop.create_task(self.handle_cancel_order(order=o, order_manager=self._snap_hedge_order_manager))
            return

    
    def handle_agg_trade(self, symbol: str, trade: PublicTrade):
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        iid_tup = (contract, ex)
        trade_obj = Trade.from_dict_prod(ex=ex, contract=contract, trade=trade)
        
        """ Latency state """
        self.local_calculator.latency_cal_map[iid_tup].add_trade(trade=trade_obj)
        latency = self.local_calculator.latency_cal_map[iid_tup].get_factor(ts=trade_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        if self.cfg.MDL_PARAMS.is_related_symbol(iid=iid):
            trd_syms = self.cfg.MDL_PARAMS.related_symbol_to_trade_symbols_map[iid]
            for trd_sym in trd_syms:
                self.signal_calculator_map[trd_sym].update_trade(trade=trade_obj)
        elif self.cfg.MDL_PARAMS.is_trade_symbol(iid=iid):
            self.signal_calculator_map[iid].update_trade(trade=trade_obj)
        else:
            pass

        self.executor.update_trade(trade_obj)

        
    def handle_bbo(self, symbol :str, bbo: BBODepth):
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        
        bbo_obj = BBO.from_dict_prod(iid=iid, bbo_depth=bbo)

        if not bbo_obj.valid_bbo:
            return

        contract = bbo_obj.meta.contract
        ex = bbo_obj.meta.ex
        server_ts = bbo_obj.server_time
        local_ts = bbo_obj.local_time
        iid_tup = (contract, ex)
        

        ism = self.iid_state_manager
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Latency state """
        self.local_calculator.latency_cal_map[iid_tup].add_bbo(bbo=bbo_obj)
        latency = self.local_calculator.latency_cal_map[iid_tup].get_factor(ts=bbo_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        """ Server time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.is_active(iid=iid):
            return

        if self._prev_bbo is not Ellipsis:
            bid_prc_chged = abs(math.log(bbo_obj.best_bid_prc / self._prev_bbo.best_bid_prc)) > 1e-6
            ask_prc_chged = abs(math.log(bbo_obj.best_ask_prc / self._prev_bbo.best_ask_prc)) > 1e-6
            if not ask_prc_chged and not bid_prc_chged:
                return

        if ex not in self.executor.active_exs:
            return

        """ Record prev ref prc """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
            if not success:
                raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=self._xex, ref_prc=ref_prc)
            if not success:
                raise RuntimeError()

        """ Update depth metrics """
        if ex in self._dynamic_mm_exchanges:
            success = self._ref_prc_xex_bbo.update_bbo(bbo=bbo_obj)
            if not success:
                return
            success = self._ref_prc_xex_bbo.update_ref_prc(ex=ex)
            if not success:
                return
            ref_prc = self._ref_prc_xex_bbo.get_ref_prc(ex=ex)
            if ref_prc is None:
                return
            success = self._update_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
            if not success:
                return
            success = self._post_cost_ref_prc_xex_bbo.update_depth_for_ex(depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex).get_post_cost_view())
            if not success:
                return
            success = self._post_cost_ref_prc_xex_bbo.update_emavg_ref_prc_sprd(ex=ex)
            if not success:
                return
            ref_prc = self._post_cost_ref_prc_xex_bbo.get_shifted_xex_ref_prc(ex=ex)
            if ref_prc is None:
                return
            success = self._update_xex_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
            if not success:
                return
            ref_prc = self._post_cost_ref_prc_xex_bbo.get_xex_ref_prc()
            if ref_prc is Ellipsis:
                return
            success = self._update_xex_ref_prc(contract=contract, ex=self._xex, ref_prc=ref_prc)
            if not success:
                return
            success = self.executor.update_bbo(bbo=bbo_obj)
            if not success:
                return
            success = self.executor.add_active_ex(ex=ex)
            if not success:
                return
        elif ex in self._snap_hedge_exchanges:
            success = self._snap_hedge_xex_depth.update_depth_for_ex_with_bbo(bbo=bbo_obj)
            if not success:
                return
            success = self._delta_neutral_executor.update_xex_depth(xex_depth=self._snap_hedge_xex_depth)
            if not success:
                return
        else:
            raise RuntimeError(f"Unsupported ex: {ex}")

        """ Compute and record ref prcs and returns """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=ex)
            if prev_ref_prc is not None:
                ret = math.log(ref_prc / prev_ref_prc)
                success = self._update_ret(contract=contract, ex=ex, ret=ret)
                if not success:
                    raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=self._xex)
            if prev_ref_prc is not None:
                ret = math.log(ref_prc / prev_ref_prc)
                success = self._update_ret(contract=contract, ex=self._xex, ret=ret)
                if not success:
                    raise RuntimeError()

        """ Full recalc of all optimal quotes is slow. """
        # self.generate_orders(self._ref_prc)
        # self.cancel_orders_action()

        """ Defensive cancel is fast. """
        ex, contract = iid.split(".")
        # self.defensive_cancel_orders_on_bbo(contract=contract, ex=ex, bbo=bbo_obj)
        self._prev_bbo = bbo_obj
    
    def handle_orderbook(self, symbol: str, orderbook):
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        ccy = contract.split("_")
        depth_obj = Depth.from_dict(
            contract=contract, 
            ccy1=ccy[0], 
            ccy2=ccy[1],
            ex=ex,
            depth=orderbook
            )
        ex = depth_obj.meta_data.ex
        contract = depth_obj.meta_data.contract
        server_ts = depth_obj.server_ts
        local_ts = depth_obj.resp_ts

        """ Latency state """
        iid_tup = (contract, ex)
        self.local_calculator.latency_cal_map[iid_tup].add_depth(depth=depth_obj)
        latency = self.local_calculator.latency_cal_map[iid_tup].get_factor(ts=depth_obj.resp_ts)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=True)

        """ tx time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        ism = self.iid_state_manager
        """ Activate stale state """
        is_in_stale_state = ism.is_in_stale_state(iid=iid)
        if is_in_stale_state:
            ism.enter_nonstale_state(iid=iid)
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.active_iids:
            return

        if not ism.is_active(iid=iid):
            return

        """ Record prev ref prc """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=ex, ref_prc=self._get_ref_prc(contract=contract, ex=ex))
            if not success:
                raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=self._xex, ref_prc=self._get_ref_prc(contract=contract, ex=self._xex))
            if not success:
                raise RuntimeError()

        """ Update depth metrics """
        if self.cfg.MDL_PARAMS.is_related_symbol(iid=iid):
            trd_syms = self.cfg.MDL_PARAMS.related_symbol_to_trade_symbols_map[iid]
            for trd_sym in trd_syms:
                self.signal_calculator_map[trd_sym].update_depth(depth=depth_obj)
        elif self.cfg.MDL_PARAMS.is_trade_symbol(iid=iid):
            self.signal_calculator_map[iid].update_depth(depth=depth_obj)
        else:
            pass

        """ Update XExdepth """
        if ex in self._dynamic_mm_exchanges:
            success = self._ref_prc_xex_bbo.update_depth(depth=depth_obj)
            if not success:
                return
            success = self._ref_prc_xex_bbo.update_ref_prc(ex=ex)
            if not success:
                return
            ref_prc = self._ref_prc_xex_bbo.get_ref_prc(ex=ex)
            if ref_prc is None:
                return
            success = self._update_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
            if not success:
                return
            success = self._post_cost_ref_prc_xex_bbo.update_depth_for_ex(depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex).get_post_cost_view())
            if not success:
                return
            success = self._post_cost_ref_prc_xex_bbo.update_emavg_ref_prc_sprd(ex=ex)
            if not success:
                return
            ref_prc = self._post_cost_ref_prc_xex_bbo.get_shifted_xex_ref_prc(ex=ex)
            if ref_prc is None:
                return
            success = self._update_xex_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
            if not success:
                return
            ref_prc = self._post_cost_ref_prc_xex_bbo.get_xex_ref_prc()
            if ref_prc is Ellipsis:
                return
            success = self._update_xex_ref_prc(contract=contract, ex=self._xex, ref_prc=ref_prc)
            if not success:
                return
            success = self.executor.update_depth(depth=depth_obj)
            if not success:
                return
            success = self.executor.add_active_ex(ex=ex)
            if not success:
                return
        elif ex in self._snap_hedge_exchanges:
            success = self._snap_hedge_xex_depth.update_depth_for_ex(depth=depth_obj)
            if not success:
                return
            success = self._delta_neutral_executor.update_xex_depth(xex_depth=self._snap_hedge_xex_depth)
            if not success:
                return
        else:
            raise RuntimeError(f"Unsupported ex: {ex}")

        """ Compute and record ref prcs and returns """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=ex)
            if prev_ref_prc is not None:
                success = self._update_ret(contract=contract, ex=ex, ret=math.log(ref_prc / prev_ref_prc))
                if not success:
                    raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=self._xex)
            if prev_ref_prc is not None:
                success = self._update_ret(contract=contract, ex=self._xex, ret=math.log(ref_prc / prev_ref_prc))
                if not success:
                    raise RuntimeError()

        """ Here is the real deal """
        self.generate_orders()
        
    async def handle_ws_update(self):
        while True:
            try:
                if not self.ws_queue["order"].empty():
                    order = await self.ws_queue["order"].get()
                    self.handle_order(order)
                    continue
                elif not self.ws_queue["trade"].empty():
                    symbol, trade = await self.ws_queue["trade"].get()
                    self.handle_agg_trade(symbol, trade)
                    continue
                elif self.ws_queue["orderbook"].empty() and self.ws_queue["bbo"].empty():
                    await asyncio.sleep(0.001)
                    continue
                elif not self.ws_queue["orderbook"].empty():
                    symbol, orderbook = await self.ws_queue["orderbook"].get()
                    self.handle_orderbook(symbol, orderbook)
                    self.ws_queue["orderbook"] = LifoQueue()
                elif not self.ws_queue["bbo"].empty():
                    symbol, bbo = await self.ws_queue["bbo"].get()
                    self.handle_bbo(symbol, bbo)
                    self.ws_queue["bbo"] = LifoQueue()
                    
                await asyncio.sleep(0.0001)
            except:
                await asyncio.sleep(0.0001)
                traceback.print_exc()
                
    
    # base operation, send, cancel, query with load balancer
    async def base_send_order(self, order: TransOrder, order_manager: OrderManager) -> Union[AtomOrder, None]:
        if not self.volume_notional_check(symbol=order.symbol, price=order.price, qty=order.quantity):
            self.loop.create_task(self.internal_fail(client_id=order.client_id, acc=order.account_name, order_manager=order_manager))
            return
        order.maker_taker = MakerTaker.undefined
        new_order = order.to_dict()
        trader = self.load_balancer.get_trader()
        try:
            self.send_response[order.client_id] = {
                "future":asyncio.Future(), 
                "create_time":int(time.time()), 
                "trader":trader,
                "client_id":order.client_id,
                "order_manager":order_manager
                }
            self.load_balancer.add_send()
            s = time.time()*1e3
            self.load_balancer.add_trader_count(trader)
            await self.publish_to_channel(
                f"mq:hf_signal:send_request:{trader}", 
                {
                    "order": new_order, 
                    "strategy_name": self.strategy_name,
                    "trader": trader,
                    "timestamp":int(time.time()*1e3)
                    }
                )
            resp, headers = await self.send_response[order.client_id]["future"]
            self.send_time.append(time.time()*1e3-s)
            self.send_response.pop(order.client_id, None)
            if isinstance(headers, dict):
                self.load_balancer.update_limit(
                    acc=order.account_name,
                    trader_name=trader,
                    acc_load=headers.get(ACC_LOAD_KEY),
                    ip_load=headers.get(IP_LOAD_KEY)
                    )
                
                if headers.get(ACC_LOAD_KEY) and int(headers.get(ACC_LOAD_KEY)) / ACC_MAX_MAP[order.account_name] > 0.8:
                    self.loop.create_task(self.handle_rate_limit(msg=-1015, account_name=order.account_name))
                    
                if headers.get(ACC_LOAD_1D_KEY) and int(headers.get(ACC_LOAD_1D_KEY)) / ACC_MAX_1D_MAP[order.account_name] > 0.9:
                    self.loop.create_task(self.switch_account(account_name=order.account_name))
            if resp == True:
                self.logger.info("order enter limbo state")
            elif resp and resp.xchg_id:
                order.xchg_id = resp.xchg_id
            else:
                self.load_balancer.sub_send()
                self.loop.create_task(self.internal_fail(client_id=order.client_id, acc=order.account_name, order_manager=order_manager, err=headers))
                if isinstance(headers, str):
                    self.logger.info(f"info about this failed order: {headers}")
                # self.logger.info(f"available load balancer: {self.load_balancer._available_map[order.account_name]}")
                # api = self.get_exchange_api_by_account(account_name=order.account_name)
                # cur_balance = (await api.account_balance(self.get_symbol_config(order.symbol))).data
                # self.logger.info(f"""query balance btc:{float(cur_balance["btc"]["available"])}, usdt:{float(cur_balance["usdt"]["available"])}""")
                
        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"base send order err:{err}")
            self.load_balancer.sub_send()
            self.loop.create_task(self.internal_fail(client_id=order.client_id, acc=order.account_name, order_manager=order_manager, err=err))

    async def switch_account(self, account_name):
        async def transfer(account_name):
            api = self.get_exchange_api_by_account(account_name=account_name)
            ccy = self.cfg.CONTRACT.split("_")
            for dymm_ex in self.cfg.DYNAMIC_MM_EXCHANGES:
                ex, _ = account_name.split(".")
                if dymm_ex.endswith("_spot"):
                    contract = f"{ccy[0]}_{ccy[1]}"
                else:
                    contract = self.cfg.CONTRACT
                symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"
                try:
                    await api.flash_cancel_orders(symbol)
                except:
                    pass
                await asyncio.sleep(0.5)
                
            cur_balance = (await api.account_balance_all(market="spot", sub_market="na")).data
            for asset in cur_balance:
                self.logger.info(f"account:{account_name} with {asset}: {cur_balance[asset]['available']}")
                try:
                    await api.make_request(
                        market="spot",
                        method="POST",
                        endpoint="/sapi/v1/sub-account/transfer/subToSub",
                        query=dict(
                            toEmail=EMAIL_CONFIG[ACC_TRANSFER_MAP[account_name]],
                            asset=asset.upper(),
                            amount=cur_balance[asset]["available"],
                        ),
                        need_sign=True
                    )
                    self.logger.info("transfer successful")
                except:
                    traceback.print_exc()
        
        self.logger.info(f"switch account: {account_name}") 
        self.load_balancer.ban_account(account_name=account_name)
        if self.transfer_out_time.get(account_name):
            if time.time() - self.transfer_out_time[account_name] < 3600:
                self.logger.info("last transfer within 1 hour")
                return
             
        await transfer(account_name=account_name)
        self.transfer_out_time[account_name] = time.time()
        self.load_balancer.switch_account(working_acc=account_name, backup_acc=ACC_TRANSFER_MAP[account_name])
        
        ccy = self.cfg.CONTRACT.split("_")
        acc = ACC_TRANSFER_MAP[account_name]
        for dymm_ex in self.cfg.DYNAMIC_MM_EXCHANGES:
            api = self.get_exchange_api_by_account(acc)
            ex, _ = acc.split(".")
            if dymm_ex.endswith("_spot"):
                contract = f"{ccy[0]}_{ccy[1]}"
            else:
                contract = self.cfg.CONTRACT
            symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"

            cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
            self.load_balancer.update_available_balance(acc=acc, available_balance=float(cur_balance[ccy[0]]["available"]), ccy=ccy[0])
            self.load_balancer.update_available_balance(acc=acc, available_balance=float(cur_balance[ccy[1]]["available"]), ccy=ccy[1])
            await asyncio.sleep(0.5)
            self.logger.info(f"account :{acc} starts with {float(cur_balance[ccy[0]]['all'])} {ccy[0]}, {float(cur_balance[ccy[1]]['all'])} {ccy[1]}")
        
        self.load_balancer.free_account(account_name=acc)
        
    
    async def handle_cancel_order(self, order: TransOrder, order_manager: OrderManager):
        async def unit_cancel_order(order: TransOrder):
            trader = self.load_balancer.get_trader()
            symbol = self._contract_mapper.proxy_iid_to_iid(order.iid)
            ex, _ = symbol.split(".")
            cancel_id = ClientIDGenerator.gen_client_id(exchange_name=ex)
            order.cancel_id = cancel_id
            new_order = order.to_dict()
            try:
                self.cancel_response[cancel_id] = {
                    "future":asyncio.Future(), 
                    "create_time":int(time.time()), 
                    "trader":trader,
                    "client_id":order.client_id,
                    "order_manager":order_manager
                }
                self.load_balancer.add_cancel()
                self.load_balancer.add_trader_count(trader)
                
                await self.publish_to_channel(
                    f"mq:hf_signal:cancel_request:{trader}", 
                    {
                        "order": new_order, 
                        "strategy_name": self.strategy_name,
                        "trader": trader,
                        "timestamp":int(time.time()*1e3)
                        }
                    )
                res, headers = await self.cancel_response[cancel_id]["future"]
                self.cancel_response.pop(cancel_id, None)
                if res == True:
                    order.cancel = True
                if isinstance(headers, dict):
                    self.load_balancer.update_limit(
                        acc=order.account_name,
                        trader_name=trader,
                        acc_load=headers.get(ACC_LOAD_KEY),
                        ip_load=headers.get(IP_LOAD_KEY)
                    )
            except Exception as err:
                self.logger.critical(f'cancel order {order.xchg_id} err {err}')
        
        cid = order.client_id
        if cid in self.canceling_id:
            return
        
        self.canceling_id.add(cid)
    
        counter = 0
        while True:
            if order_manager.get_trans_order(oid=cid) != None:
                o = order_manager.trans_order_at(oid=cid)
                if o.cancel:
                    self.canceling_id.remove(cid)
                    return
            else:
                self.canceling_id.remove(cid)
                return
            counter += 1
            self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(1)
            
            if counter > 10:
                if order_manager.get_trans_order(oid=cid) != None:
                    o = order_manager.trans_order_at(oid=cid)
                    o.cancel = True
                    self.logger.info("try to cancel 10 times and see it as canceled")
    
    async def handle_query_order(self, order: TransOrder, order_manager: OrderManager):
        async def unit_query_order(order: TransOrder):
            trader = self.load_balancer.get_trader()
            symbol = self._contract_mapper.proxy_iid_to_iid(order.iid)
            ex, _ = symbol.split(".")
            query_id = ClientIDGenerator.gen_client_id(exchange_name=ex)
            order.query_id = query_id
            new_order = order.to_dict()
            try:
                self.query_response[query_id] = {
                    "future":asyncio.Future(), 
                    "create_time":int(time.time()), 
                    "trader":trader,
                    "client_id":order.client_id,
                    "order_manager":order_manager
                }
                self.load_balancer.add_query()
                self.load_balancer.add_trader_count(trader)
                await self.publish_to_channel(
                        f"mq:hf_signal:query_request:{trader}", 
                        {
                            "order": new_order, 
                            "strategy_name": self.strategy_name,
                            "trader": trader,
                            "timestamp":int(time.time()*1e3)
                            }
                        )
                resp, headers = await self.query_response[query_id]["future"]
                self.query_response.pop(query_id, None)
                if isinstance(headers, dict):
                    self.load_balancer.update_limit(
                        acc=order.account_name,
                        trader_name=trader,
                        acc_load=headers.get(ACC_LOAD_KEY),
                        ip_load=headers.get(IP_LOAD_KEY)
                    )
            except:
                return

            if not resp:
                return
            if resp.xchg_status in OrderStatus.fin_status():
                await self.on_order(resp)
            else:
                await self.handle_cancel_order(order=order, order_manager=order_manager)
        
        cid = order.client_id  
        if time.time()*1e3 - order.sent_ts < 1000 or cid in self.quering_id:
            return
        
        self.quering_id.add(cid)
        
        counter = 0
        while True:
            if order_manager.get_trans_order(oid=cid) != None:
                o =  order_manager.trans_order_at(oid=cid)
                if time.time()*1e3 - order.sent_ts < 1000:
                    self.quering_id.remove(cid)
                    return
            else:
                self.quering_id.remove(cid)
                return
            counter += 1
            self.loop.create_task(unit_query_order(order))
            await asyncio.sleep(10)
            
            if counter > 30:
                if order_manager.get_trans_order(oid=cid) != None:
                    o = order_manager.trans_order_at(oid=cid)
                    self.loop.create_task(
                        self.internal_fail(
                            client_id=o.client_id, 
                            acc=o.account_name,
                            order_manager=order_manager, 
                            err=None)
                        )
                    self.logger.info(f"order query too much time and see it as failed order")
                    
                
    async def handle_rate_limit(self, msg=None, account_name=None, trader_name=None):
        if msg == -1015:
            if account_name in self.load_balancer.ban_name or account_name == None:
                return
            # self.logger.info(f"reach account rate limit:{account_name}")
            self.load_balancer.ban_account(account_name)
            await asyncio.sleep(5)
            self.load_balancer.free_account(account_name)
            return
        elif msg == -1003:
            self.logger.info(f"reach ip rate limit:{trader_name}")
            if trader_name in self.load_balancer.ban_trader_name or trader_name == None:
                return
            self.load_balancer.ban_trader(trader=trader_name)
            if len(self.load_balancer.ban_trader_name) < self.load_balancer.trader_count / 2:
                self.send = False
            await asyncio.sleep(60)
            self.load_balancer.free_trader(trader=trader_name)
            self.send = True
        else:
            self.logger.info(f"rate limit type not catch")
    
    def set_result_to_future(self, fut:asyncio.Future, data):
        try:
            fut.set_result(data)
        except:
            pass
    
    async def process_pong(self, ch_name, response):
        payload = json.loads(response)
        self.load_balancer.free_trader(payload["trader"])
    
    async def process_send_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        if self.send_response.get(payload["client_id"]) == None:
            return
        self.load_balancer.sub_trader_count(payload["trader"])
        future: asyncio.Future = self.send_response[payload["client_id"]]["future"]
        if payload["status"] == False:
            if payload.get("rate_limit"):
                self.loop.create_task(
                    self.handle_rate_limit(
                        msg=int(payload["rate_limit"]), 
                        account_name=payload.get("account_name"), 
                        trader_name=payload.get("trader")
                        )
                    )
            self.set_result_to_future(future, (None, payload["data"]))
        else:
            self.set_result_to_future(future, (AtomOrder.from_dict(payload["data"]), payload["headers"]))
        
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        if self.cancel_response.get(payload["cancel_id"]) == None:
            return
        self.load_balancer.sub_trader_count(payload["trader"])
        if payload["status"] == False and payload.get("rate_limit"):
            self.loop.create_task(
                    self.handle_rate_limit(
                        msg=int(payload["rate_limit"]), 
                        account_name=payload.get("account_name"), 
                        trader_name=payload.get("trader")
                        )
                    )
        future: asyncio.Future = self.cancel_response[payload["cancel_id"]]["future"]
        self.set_result_to_future(future, (payload["status"],payload["headers"]))
        
        
    async def process_query_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        if self.query_response.get(payload["query_id"]) == None:
            return
        self.load_balancer.sub_trader_count(payload["trader"])
        future: asyncio.Future = self.query_response[payload["query_id"]]["future"]
        if payload["status"] == False:
            if payload.get("rate_limit"):
                self.loop.create_task(
                    self.handle_rate_limit(
                        msg=int(payload["rate_limit"]), 
                        account_name=payload.get("account_name"), 
                        trader_name=payload.get("trader")
                        )
                    )
            self.set_result_to_future(future, (None, payload["headers"]))
        else:
            self.set_result_to_future(future, (AtomOrder.from_dict(payload["data"]), payload["headers"]))
            
    async def pin_trader(self):
        while True:
            await asyncio.sleep(0.5)
            for trader in self.load_balancer._ip_load_map:
                await self.publish_to_channel(
                    f"mq:hf_signal:ping:{trader}", 
                    {
                        "strategy_name":self.strategy_name,
                        }
                    )
    
    async def check_process_timeout(self):
        
        def unit_check(data):
            cur_time = int(time.time())
            for key in data:
                create_time = data[key]["create_time"]
                om = data[key]["order_manager"]
                cid = data[key]["client_id"]
                
                if cur_time - create_time > 1:
                    if om.get_trans_order(oid=cid) == None:
                        self.set_result_to_future(data[key]["future"], (None, None))
                        continue
                    self.set_result_to_future(data[key]["future"], (None, data[key]['trader']))
                    self.load_balancer.ban_trader(data[key]["trader"])
                    self.logger.info(f"trader {data[key]['trader']} not working properly, cid={cid}")
        
        def send_order_unit_check(data):
            cur_time = int(time.time())
            for key in data:
                create_time = data[key]["create_time"]
                om = data[key]["order_manager"]
                cid = data[key]["client_id"]
                
                if cur_time - create_time > 1:
                    if om.get_trans_order(oid=cid) == None:
                        self.set_result_to_future(data[key]["future"], (None, None))
                        continue
                    self.set_result_to_future(data[key]["future"], (True, data[key]['trader']))
                    self.load_balancer.ban_trader(data[key]["trader"])
                    self.logger.info(f"trader {data[key]['trader']} not working properly, cid={cid}")
        
        
        while True:
            await asyncio.sleep(1)
            send_order_unit_check(self.send_response)
            unit_check(self.cancel_response)
            unit_check(self.query_response)
            
    
    # strategy base function override
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    # strategy utils only for prod
    async def internal_fail(self, client_id: str, acc: str, order_manager: OrderManager, err=None):
        try:
            # if err:
            #     self.logger.info(f"failed order: {err}, acc:{acc}, client_id={client_id}")
            if order_manager.contains_trans_order(oid=client_id):
                o = order_manager.trans_order_at(oid=client_id)
            else:
                return
            order = PartialOrder(
                account_name=o.account_name,
                xchg_id=None,
                exchange_pair="",
                client_id=client_id,
                xchg_status=OrderStatus.Failed,
                filled_amount=0,
                avg_filled_price=0,
                commission_fee=0,
                local_ms=int(time.time() * 1e3),
                server_ms=int(time.time() * 1e3),
                )
            await self.on_order(order)
        except:
            traceback.print_exc()
            
    def trim_order_before_sending(self, order: TransOrder):
        symbol = self._contract_mapper.proxy_iid_to_iid(order.iid)
        ex, _ = symbol.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        order.symbol_id = self.get_symbol_config(order.symbol)["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[symbol])
        else:
            order.price = round_decimals_up(order.price, price_precision[symbol])
        
        order.quantity = round(order.quantity, qty_precision[symbol])
        
        return cid
    
    def check_orders(self, order_manager: OrderManager):
        for oid in order_manager.keys():
            if order_manager.get_trans_order(oid):
                o = order_manager.trans_order_at(oid)
                self.loop.create_task(self.handle_query_order(order=o, order_manager=order_manager))
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
    
    # strategy utils for prod and backtest
    def _main_signal_iid(self):
        return list(self.cfg.MDL_PARAMS.trade_symbol_to_related_symbols_map.keys())[0]
    
    def update_inventory(self, iid: str, chg_qty: float, ts: int):
        ex, contract = iid.split(".")
        self._inventory_manager.increment_inventory(contract=contract, ex=ex, qty_chg=chg_qty)
        if contract == self.executor.contract:
            ex_inven = self._inventory_manager.get_inventory(contract=contract, ex=ex)
            self.executor.update_inventory(inventory=ex_inven, contract=contract, ex=ex, ts=ts)

    def defensive_cancel_orders_on_trade(self, contract: str, ex: str, trd: Trade):

        mm_mdl = self.executor.mm_mdl
        local_cal = self.local_calculator
        ts = trd.local_time

        iid_tup = (contract, ex)
        depth_freq, bbo_freq, trade_freq = local_cal.update_freq_cal_map[iid_tup].get_factor(ts=ts)
        if depth_freq is None:
            return

        tau = depth_freq  # order update frequency
        pred_las, pred_sas = local_cal.pred_trade_arrival_size(contract=contract, ex=ex, ts=ts)
        if (pred_las is None) or (pred_sas is None):
            return
        pred_lat, pred_sat = local_cal.pred_trade_arrival_time(contract=contract, ex=ex, ts=ts)
        if (pred_lat is None) or (pred_sat is None):
            return
        pred_lat = max(1, pred_lat)
        pred_sat = max(1, pred_sat)
        curr_las, curr_sas = local_cal.impulse_arrival_size_cal_map[iid_tup].get_factor(ts=ts)
        if (curr_las is None) or (curr_sas is None):
            return
        curr_lat, curr_sat = local_cal.impulse_arrival_time_cal_map[iid_tup].get_factor(ts=ts)
        if (curr_lat is None) or (curr_sat is None):
            return
        curr_lat = max(1, curr_lat)
        curr_sat = max(1, curr_sat)
        avg_las, avg_sas = local_cal.arrival_size_cal_map[iid_tup].get_factor(ts=ts)
        if (avg_las is None) or (avg_sas is None):
            return
        avg_lat, avg_sat = local_cal.arrival_time_cal_map[iid_tup].get_factor(ts=ts)
        if (avg_lat is None) or (avg_sat is None):
            return
        ref_prc = mm_mdl.get_ref_prc(contract=contract, ex=ex)

        ask_prc_touch_line = ref_prc
        bid_prc_touch_line = ref_prc

        if trd.side == Direction.long:
            frequent_event = max(1/pred_lat, 1/curr_lat) > avg_lat * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            large_quantity = max(pred_las, curr_las) > avg_las * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            if frequent_event or large_quantity:
                lar = max(pred_las, curr_las, pred_las * tau / pred_lat, curr_las * tau / curr_lat)
                a_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="a_s")
                if a_s is None:
                    return
                b_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="b_s")
                if b_s is None:
                    return
                s_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="s_s")
                if s_s is None:
                    return
                delta = (lar + self.executor.mm_mdl._epsilon - b_s) / max(1e-6, a_s)
                ask_prc_touch_line = max(ref_prc + s_s + delta, 0)
        elif trd.side == Direction.short:
            frequent_event = max(1/pred_sat, 1/curr_sat) > avg_sat * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            large_quantity = max(pred_sas, curr_sas) > avg_sas * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            if frequent_event or large_quantity:
                sar = max(pred_sas, curr_sas, pred_sas * tau / pred_sat, curr_sas * tau / curr_sat)
                a_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="a_l")
                if a_l is None:
                    return
                b_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="b_l")
                if b_l is None:
                    return
                s_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="s_l")
                if s_l is None:
                    return
                delta = (sar + self.executor.mm_mdl._epsilon - b_l) / max(1e-6, a_l)
                bid_prc_touch_line = max(ref_prc - s_l - delta, 0)
        else:
            raise NotImplementedError()

        I = self._inventory_manager.get_inventory(contract=contract, ex=self._xex)
        self.defensive_cancel_orders(ask_touch_line=ask_prc_touch_line, bid_touch_line=bid_prc_touch_line, I=I)
        self._n_def_cancel += 1

    def _update_latency(self, iid: str, latency: float, allow_rectivate: bool):
        ism = self.iid_state_manager
        is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
        is_active = ism.is_active(iid=iid)
        if not is_in_high_latency_state:
            if ism.is_high_latency(iid=iid, latency=latency):
                ism.enter_high_latency_state(iid=iid)
                if is_active:
                    self.deactivate(iid=iid)
        else:  # is_in_high_latency_state
            if not ism.is_high_latency(iid=iid, latency=latency):
                if allow_rectivate:
                    ism.enter_normal_latency_state(iid=iid)

    def defensive_cancel_orders_on_bbo(self, contract: str, ex: str, bbo: BBO):

        """ SEx or XEx? That is the question. """


        exec = self.executor
        mm_mdl = exec.mm_mdl
        sig_cal = self.signal_calculator_map[self._main_signal_iid()]
        local_cal = self.local_calculator
        ts = bbo.local_time

        if not mm_mdl.has_depth_metrics(contract=contract, ex=ex):
            return

        tau = exec.get_tau(ex=ex, ts=ts)
        if tau is None:
            return

        ref_prc = mm_mdl.get_ref_prc(contract=contract, ex=ex)
        if ref_prc is None:
            return

        if self.cfg.PRED_ON:
            pred_ret = sig_cal.predict_ref_prc_ret(ms=tau)
        else:
            pred_ret = local_cal.predict_ref_prc_ret(contract=contract, ex=ex, ts=ts)
        if pred_ret is None:
            pred_ret = 0
        pred_long_half_sprd = local_cal.predict_long_half_spread(contract=contract, ex=ex, ts=ts)
        pred_short_half_sprd = local_cal.predict_short_half_spread(contract=contract, ex=ex, ts=ts)
        pred_ref_prc = ref_prc * (1 + pred_ret * tau)
        pred_bid_touch_line = pred_ref_prc - pred_long_half_sprd
        pred_ask_touch_line = pred_ref_prc + pred_short_half_sprd

        curr_ret = local_cal.curr_impulse_ref_prc_ret(contract=contract, ex=ex, ts=ts)
        if curr_ret is None:
            curr_ret = 0
        curr_long_half_sprd = local_cal.curr_impulse_long_half_spread(contract=contract, ex=ex, ts=ts)
        curr_short_half_sprd = local_cal.curr_impulse_short_half_spread(contract=contract, ex=ex, ts=ts)
        curr_ref_prc = ref_prc * (1 + curr_ret * tau)
        curr_bid_touch_line = curr_ref_prc - curr_long_half_sprd
        curr_ask_touch_line = curr_ref_prc + curr_short_half_sprd

        abs_ret = max(abs(curr_ret), abs(pred_ret))
        half_sprd = max(pred_long_half_sprd, pred_short_half_sprd, curr_long_half_sprd, curr_short_half_sprd)
        bid_touch_line = min(pred_bid_touch_line, curr_bid_touch_line)
        ask_touch_line = max(pred_ask_touch_line, curr_ask_touch_line)

        avg_sigma = local_cal.curr_ema_sigma(contract=contract, ex=ex, ts=ts)
        large_abs_ret_bnd = avg_sigma * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER
        lhs = local_cal.curr_impulse_long_half_spread(contract=contract, ex=ex, ts=ts)
        shs = local_cal.curr_emavg_short_half_spread(contract=contract, ex=ex, ts=ts)
        avg_half_sprd = (lhs + shs) / 2
        large_hs_bnd = avg_half_sprd * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER

        large_ret = abs_ret > large_abs_ret_bnd
        large_sprd = half_sprd > large_hs_bnd

        if large_sprd or large_ret:
            I = self._inventory_manager.get_inventory(contract=contract, ex=self._xex)
            self.defensive_cancel_orders(ask_touch_line=ask_touch_line, bid_touch_line=bid_touch_line, I=I)
            self._n_def_cancel += 1
    
    def _update_prev_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        self._prev_ref_prc[(contract, ex)] = ref_prc
        return True

    def _get_prev_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._prev_ref_prc.get((contract, ex))

    def _remove_prev_ref_prc(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._prev_ref_prc:
            self._prev_ref_prc.pop((contract, ex))

    def _update_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        ce = (contract, ex)
        self._ref_prc_map[ce] = ref_prc
        self.local_calculator.trade_ret_lambda_cal_map[ce].update_ref_prc(ref_prc=ref_prc)
        if contract == self.executor.contract:
            self.executor.update_ref_prc(ex=ex, ref_prc=ref_prc)
        return True

    def _get_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._ref_prc_map.get((contract, ex))

    def _remove_ref_prc(self, contract: str, ex: str) -> bool:
        self._ref_prc_map.pop((contract, ex), None)
        return True

    def _update_xex_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        self._xex_ref_prc_map[(contract, ex)] = ref_prc
        if contract == self.executor.contract:
            self.executor.update_xex_ref_prc(ex=ex, ref_prc=ref_prc)
        return True

    def _get_xex_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._xex_ref_prc_map.get((contract, ex))

    def _remove_xex_ref_prc(self, contract: str, ex: str) -> bool:
        self._xex_ref_prc_map.pop((contract, ex), None)
        return True

    def _update_ret(self, contract: str, ex: str, ret: float) -> bool:
        self._ret_map[(contract, ex)] = ret
        return True

    def _get_ret(self, contract: str, ex: str) -> Union[float, None]:
        return self._ret_map.get((contract, ex))

    def _remove_ret(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._ret_map:
            self._ret_map.pop(key)

    def deactivate(self, iid: str):
        ex, contract = iid.split(".")
        xex = "xex"
        if ex in self._dynamic_mm_exchanges:
            if self._ref_prc_xex_bbo.contains(ex=ex):
                self._ref_prc_xex_bbo.remove_ex(ex=ex)
            if self._post_cost_ref_prc_xex_bbo.contains(ex=ex):
                self._post_cost_ref_prc_xex_bbo.remove_ex(ex=ex)
            if ex in self._depth_map:
                self._depth_map.pop(ex)
        if ex in self._snap_hedge_exchanges:
            if self._snap_hedge_xex_depth.contains(ex=ex):
                self._snap_hedge_xex_depth.remove_ex(ex=ex)
        ref_prc = self._post_cost_ref_prc_xex_bbo.get_xex_ref_prc()
        if ref_prc is not Ellipsis:
            self._update_xex_ref_prc(contract=contract, ex=xex, ref_prc=ref_prc)
        self.executor.remove_active_ex(ex=ex)
        self.cancel_all_orders_for_iid(iid=iid)

    def _cancel_orders_for_iid(self, iid: str, order_manager: OrderManager):
        for oid in order_manager.get_oids_for_iid(iid=iid):
            o = order_manager.trans_order_at(oid=oid)
            if not o.cancel:
                self.loop.create_task(self.handle_cancel_order(order=o, order_manager=order_manager))

    def cancel_dynamic_mm_orders_for_iid(self, iid: str):
        self._cancel_orders_for_iid(iid=iid, order_manager=self._dynamic_mm_order_manager)

    def cancel_snap_hedge_orders_for_iid(self, iid: str):
        self._cancel_orders_for_iid(iid=iid, order_manager=self._snap_hedge_order_manager)

    def cancel_all_orders_for_iid(self, iid: str):
        self.cancel_dynamic_mm_orders_for_iid(iid=iid)
        self.cancel_snap_hedge_orders_for_iid(iid=iid)
    
    def defensive_cancel_orders(self, ask_touch_line: float, bid_touch_line: float, I: float):
        if I >= 0:  # long
            for int_prc, oid_set in self._dynamic_mm_order_manager._bid_ladder.items():
                prc = int_prc / self._dynamic_mm_order_manager.price_multilier
                if prc > bid_touch_line:
                    for oid in oid_set:
                        order = self._dynamic_mm_order_manager.trans_order_at(oid)
                        if not order.cancel:
                            self.loop.create_task(self.handle_cancel_order(order=order, order_manager=self._dynamic_mm_order_manager))
                    continue
                break
        if I <= 0:  # short
            for int_prc, oid_set in self._dynamic_mm_order_manager._ask_ladder.items():
                prc = int_prc / self._dynamic_mm_order_manager.price_multilier
                if prc < ask_touch_line:
                    for oid in oid_set:
                        order = self._dynamic_mm_order_manager.trans_order_at(oid)
                        if not order.cancel:
                            self.loop.create_task(self.handle_cancel_order(order=order, order_manager=self._dynamic_mm_order_manager))
                    continue
                break

    # strategy main logic
    def generate_orders(self):
        orders, data = self.executor.get_orders(
            t=int(time.time()*1e3),
        )
        for order in orders:
            order.generate_symbol(self._contract_mapper)
            self.send_dynamic_mm_order(order)
        return data

    def send_dynamic_mm_order(self, order: TransOrder):
        
        inner_send = True

        for _, o in self._dynamic_mm_order_manager.trans_order_items():
            if o.side == order.side and o.iid == order.iid and o.floor == order.floor:
                quote_lvl = int(order.floor)
                cancel_tol = self.cfg.QUOTE_LVL_CONFIGS[order.iid.split(".")[0]][quote_lvl].CANCEL_TOL
                if abs(o.price - order.price) > 10 ** (-self._prc_precision[order.iid]) * cancel_tol:
                    if not o.cancel:
                        self.loop.create_task(self.handle_cancel_order(order=o, order_manager=self._dynamic_mm_order_manager))
                else:
                    # order.life = 100
                    o.sent_ts = int(time.time()*1e3)
                    inner_send = False
                
        if not inner_send:
            return
        
        if not self.send:
            return
        orders = self.load_balancer.split_order(order=order)
        for order in orders:
            self.load_balancer.send_order(order=order)
            cid = self.trim_order_before_sending(order)
            self._dynamic_mm_order_manager.add_trans_order(order=order, oid=cid)
            self.loop.create_task(self.base_send_order(order=order,order_manager=self._dynamic_mm_order_manager))

    def send_snap_hedge_order(self, order: TransOrder):
        raise RuntimeError("Should not be here")

    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(10)
            self.check_orders(order_manager=self._dynamic_mm_order_manager)
            self.check_orders(order_manager=self._snap_hedge_order_manager)
            
    
    async def load_balancer_reduce_count(self):
        while True:
            await asyncio.sleep(1)
            self.load_balancer.reduce_count(1)
            
    # stats
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(20)
            try:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":self.strategy_name}, 
                        fields={
                            "inventory":float(self._inventory_manager.get_inventory(contract=self.cfg.CONTRACT, ex=self.cfg.DYNAMIC_MM_EXCHANGES[0])),
                            }
                        )
                    )
                
                for oid, order in list(self._dynamic_mm_order_manager.trans_order_items()):
                    if order.side == Direction.short and order.floor == 0:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    "cache_sell_price":float(order.price)
                                    }
                                )
                            )
                    elif order.side == Direction.long and order.floor == 0:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    "cache_buy_price":float(order.price)
                                    }
                                )
                            )
                
                ccy = self.cfg.CONTRACT.split("_")
                
                
                ccy1_in_coin = 0
                ccy2_amt = 0

                for dymm_ex in self.cfg.DYNAMIC_MM_EXCHANGES:
                    ref_prc = self._ref_prc_map.get((self.cfg.CONTRACT, dymm_ex))
                    for acc in self.account_names:
                        api = self.get_exchange_api_by_account(acc)
                        ex, _ = acc.split(".")
                        if dymm_ex.endswith("_spot"):
                            contract = f"{ccy[0]}_{ccy[1]}"
                        else:
                            contract = self.cfg.CONTRACT
                        symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"
                        
                        await asyncio.sleep(0.2)
                        cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                        
                        ccy1_in_coin += float(cur_balance[ccy[0]]["all"])
                        ccy2_amt += float(cur_balance[ccy[1]]["all"])
                        if acc in self.load_balancer._account_load_map:
                            self.loop.create_task(
                                self.push_influx_data(
                                    measurement="tt", 
                                    tag={"sn":self.strategy_name}, 
                                    fields={
                                        f"{acc}_load":float(self.load_balancer._account_load_map[acc])
                                        }
                                    )
                                ) 
                            
                    pnl = float(ccy2_amt) + float(ccy1_in_coin) * float(ref_prc)
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt", 
                            tag={"sn":self.strategy_name}, 
                            fields={
                                "balance": float(ccy2_amt) + float(ccy1_in_coin) * float(ref_prc),
                                "pnl":float(ccy2_amt-self.ccy2_amt) + float(ccy1_in_coin-self.ccy1_in_coin) * float(ref_prc),
                                }
                            )
                        ) 
                
                for trader in list(self.load_balancer._ip_load_map.keys()):
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt", 
                            tag={"sn":self.strategy_name}, 
                            fields={
                                f"{trader}_load": float(self.load_balancer._ip_load_map[trader])
                            }
                        )
                    )
                    
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":self.strategy_name}, 
                        fields={
                            f"send per min": float(self.load_balancer.send_counter) / 20 * 60,
                            f"cancel per min": float(self.load_balancer.cancel_counter) / 20 * 60,
                            f"query per min": float(self.load_balancer.query_counter) / 20 * 60,
                        }
                    )
                )
                self.load_balancer.send_counter = 0
                self.load_balancer.cancel_counter = 0
                self.load_balancer.query_counter = 0
                
                # self.logger.info(f"send time :{np.mean(self.send_time)}, {np.max(self.send_time)}")
            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
            
    async def update_redis_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    ccy = self.cfg.CONTRACT.split("_")
                    for dymm_ex in self.cfg.DYNAMIC_MM_EXCHANGES:
                        for acc in self.account_names:
                            api = self.get_exchange_api_by_account(acc)
                            ex, _ = acc.split(".")
                            if dymm_ex.endswith("_spot"):
                                contract = f"{ccy[0]}_{ccy[1]}"
                            else:
                                contract = self.cfg.CONTRACT
                            symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"
                            try:
                                await api.flash_cancel_orders(self.get_symbol_config(symbol))
                            except:
                                pass
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache(
                    {
                        "exit":None,
                        "send time":f"mean: {np.mean(self.send_time)}, max: {np.max(self.send_time)}"
                        }
                    )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(2)
            await check_cache()
        
    # strategy core
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.reset_missing_order_action(),
                self.get_local_stats(),
                self.update_redis_cache(),
                self.load_balancer_reduce_count(),
                self.check_process_timeout(),
                self.pin_trader()
            )
            
if __name__ == '__main__':
    # logging.getLogger().setLevel("WARNING")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()