from dataclasses import dataclass
from typing import Dict

from xex_mm.utils.enums import Side


@dataclass
class ServerBoundaryMessage:
    symbol: str
    prc: float
    qty: float
    side: Side
    offsetFlag: int
    orderPriceType: int
    origType: int
    time: int

    @staticmethod
    def from_dict(data: Dict):
        return ServerBoundaryMessage(
            symbol=data["symbol"],
            prc=float(data["price"]),
            qty=float(data["volume"]),
            side=Side.long if data["side"] == "BUY" else Side.short,
            offsetFlag=int(data["offsetFlag"]),
            orderPriceType=int(data["orderPriceType"]),
            origType=int(data["origType"]),
            time=int(data["time"]),
        )
