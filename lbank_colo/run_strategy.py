import traceback
# import os
# os.environ["ATOM_PRINT_RESPONSE"] = '1'

from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "press3_prod")
import asyncio
import logging
import time
from concurrent.futures.thread import ThreadPoolExecutor
from typing import Dict, Union, List

import json
import aio_pika
from atom.model import OrderStatus, OrderType, PartialOrder, OrderPositionSide, OrderSide
from atom.model.order import Order as ProdOrder
from strategy_base.base import CommonStrategy, OrderNotFoundError
from xex_mm.utils.base import Order, Depth, MsgOrderManager
from xex_mm.utils.enums import Side

from data_structures import ServerBoundaryMessage


class LBankColoStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super(LBankColoStrategy, self).__init__(loop, **kwargs)
        self.ex = "lbank"
        self.contracts: List[str] = self.config['strategy']['params']['contracts'].split(",")
        self.symbol_cfg_map: Dict[str, Dict] = {}
        self.order_manager_map: Dict[str, MsgOrderManager] = {}
        self.depth_map: Dict[str, Union[Depth, None]] = {}
        self.ex_pair_to_contract_map: Dict[str, str] = {}
        self.contract_to_syn_name_map: Dict[str, str] = {}
        for contract in self.contracts:
            sym_name = f"{self.ex}.{contract}.swap.na"
            self.contract_to_syn_name_map[contract] = sym_name
            sym_cfg = self.get_symbol_config(sym_name)
            self.symbol_cfg_map[contract] = sym_cfg
            self.logger.info(f"contract={contract},sym_cfg={sym_cfg}")
            self.ex_pair_to_contract_map[sym_cfg["exchange_pair"]] = contract
            self.order_manager_map[contract] = MsgOrderManager(contract=contract, ex=self.ex)
            self.depth_map[contract] = None
        self.numeric_tol = 1e-10
        self.thread_pool = ThreadPoolExecutor(10)
        self.mq_task_id = 0
        self.only_log = False

        self.limit_order_types = {OrderType.Limit, OrderType.FOK, OrderType.IOC, OrderType.PostOnly}

    def update_config_before_init(self):
        self.use_colo_url = True

    async def sub_mq_msg(self):
        connection = await aio_pika.connect_robust(
            "amqp://app:K3IeeVeEFJHpOqY@172.31.4.70:50000/", loop=self.loop
        )
        self.logger.info("AMQP Connected...")
        async with connection:
            channel = await connection.channel()
            queue = await channel.declare_queue(exclusive=True)
            await queue.bind("send.order.exchange", "rk_sendOrder")
            async with queue.iterator() as queue_iter:
                async for message in queue_iter:
                    async with message.process():
                        await self.onServerBoundaryMessage(json.loads(message.body))

    async def before_strategy_start(self):
        try:
            mq_task = self.loop.create_task(self.sub_mq_msg())
            self.mq_task_id = self.task_manager.add_task_record(mq_task)
            for contract in self.contracts:
                sym_name = self.contract_to_syn_name_map[contract]
                self.direct_subscribe_orderbook(sym_name, is_testnet=False)
            self.direct_subscribe_order_update(self.contract_to_syn_name_map[self.contracts[0]], self.account_names[0])
        except:
            self.logger.critical(traceback.format_exc())

    def from_dict_order_msg(self, porder: PartialOrder):
        contract = self.ex_pair_to_contract_map[porder.exchange_pair]
        cv = self.symbol_cfg_map[contract]["contract_value"]
        return Order(
            contract=contract,
            ex=self.ex,
            prc=float(porder.extra["requested_price"]),
            qty=float(porder.extra["requested_amount"]),
            side=Side.long if porder.extra["side"] == OrderSide.Buy else Side.short,
            filled_qty=float(porder.filled_amount) * float(cv),
            avg_fill_prc=float(porder.avg_filled_price),
            order_id=porder.xchg_id,
            client_id=porder.client_id,
            local_time=porder.local_ms,
            tx_time=porder.server_ms,
            status=porder.xchg_status,
            account_name=porder.account_name,
            match_type=porder.extra["order_type"]
        )

    async def on_orderbook(self, symbol, orderbook):
        try:
            if not orderbook:
                self.logger.info(f"Got none orderbook.")
                return
            iid = symbol
            ex, contract = iid.split(".")
            parts = contract.split("_")
            ccy1 = parts[0]
            ccy2 = parts[1]
            depth_obj = Depth.from_dict(contract=contract, ccy1=ccy1, ccy2=ccy2, ex=ex, depth=orderbook)
            self.depth_map[contract] = depth_obj
            # self.logger.info(f"onDepth: contract={contract}, depth={depth_obj}")

        except:
            self.logger.critical(traceback.format_exc())

    async def on_order(self, order: PartialOrder):
        try:
            if not order:
                self.logger.info(f"Got none order.")
                return
            order_obj: Order = self.from_dict_order_msg(porder=order)
            order_status = order_obj.status
            if order_status not in OrderStatus.fin_status():
                self.order_manager_map[order_obj.contract].update_order(order=order_obj)
            else:
                self.order_manager_map[order_obj.contract].pop_order(oid=order_obj.order_id)
            self.logger.info(f"OnOrder: contract={order.exchange_pair},order{order_obj.__dict__} ")

        except:
            self.logger.critical(traceback.format_exc())

    def _order_to_prod_order(self, order: Order) -> ProdOrder:
        ts_now = int(time.time() * 1e3)
        sym_cfg = self.symbol_cfg_map[order.contract]
        return ProdOrder(
            symbol_id=sym_cfg["id"],
            account_name=order.account_name,
            tag="",
            xchg_id=order.order_id,
            client_id=order.client_id,
            avg_filled_price=order.avg_fill_prc,
            commission_fee=0,
            create_ms=ts_now,
            fee_currency="na",
            filled_amount=order.filled_qty,
            local_ms=order.local_time,
            position_side=OrderPositionSide.Open,
            requested_amount=order.qty,
            requested_price=order.prc,
            server_ms=ts_now,
            side=OrderSide.Buy if order.side == Side.long else OrderSide.Sell,
            type=OrderType.Limit,
            xchg_status=OrderStatus.New,
        )

    async def wrapped_cancel_order(self, o: Order):
        try:
            po = self._order_to_prod_order(order=o)
            try:
                await self.cancel_order(order=po)
                o.status = OrderStatus.Canceled
            except OrderNotFoundError:
                o.status = OrderStatus.Canceled
        except:
            self.logger.critical(traceback.format_exc())

    async def onServerBoundaryMessage(self, server_boundary_message: Dict):

        """
        Ack is the message we get when a client order hits jojo's server boundary. When receiving this message,
        we can identify which orders will hit our outstanding order, hence we can determine which orders are the ones
        we want to allow hit and which ones are not.

        Orders we allow hit: if an order (either a deep limit order or a large market order) is predicted to hit our ith
        order level, any order in-front of i should be canceled to allow max spread capture.

        message-template:
          {
              "offsetFlag": "0", // 0:开仓;1:平仓;5:全平;
              "orderPriceType": "0", // 0:限价;4:市价_十档价
              "origType": "0", // 0:普通订单;1:(FillAndKill/IOC)立即完成并且剩余撤销;2:(FillOrKill)立即完成全部或者最小订单要求否则全部撤销;3:需要进入订单簿被动成交,否则全部撤销(只做Maker/PostOnly);
              "price": 13.41, // 价格
              "side": "BUY", // buy|sell
              "symbol": "SOLUSDT", // 交易对
              "volume": 800 // 数量
              "time": int, ms
          }
        """
        try:
            if not server_boundary_message:
                self.logger.info(f"Got none server boundary message.")
                return
            if server_boundary_message["symbol"] not in self.ex_pair_to_contract_map:
                self.logger.info(f"time={server_boundary_message['time']}, symbol={server_boundary_message['symbol']}, exchange_pairs={self.ex_pair_to_contract_map}")
                return
            sbm = ServerBoundaryMessage.from_dict(data=server_boundary_message)
            if self.only_log:
                self.logger.info(f"recv new order: symbol={server_boundary_message['symbol']}, time={sbm.time}, offsetFlag={sbm.offsetFlag}, orderPriceType={sbm.orderPriceType}, origType={sbm.origType}, side={sbm.side}, prc={sbm.prc}, qty={sbm.qty}")
                return
            is_lmt = sbm.orderPriceType == 0
            is_mkt = sbm.orderPriceType == 4
            if is_lmt:
                tgt_prc = sbm.prc
                self.logger.info(f"Receive New Limit Order: symbol={server_boundary_message['symbol']}, time={sbm.time}, offsetFlag={sbm.offsetFlag}, orderPriceType={sbm.orderPriceType}, origType={sbm.origType}, side={sbm.side}, prc={sbm.prc}, qty={sbm.qty}")
            elif is_mkt:
                self.logger.info(f"Receive New Market Order: symbol={server_boundary_message['symbol']}, time={sbm.time}, offsetFlag={sbm.offsetFlag}, orderPriceType={sbm.orderPriceType}, origType={sbm.origType}, side={sbm.side}, prc={sbm.prc}, qty={sbm.qty}")
                contract = self.ex_pair_to_contract_map[sbm.symbol]
                depth = self.depth_map[contract]
                if sbm.side == Side.long:
                    if len(depth.asks) == 0:
                        return
                    cum_q = 0
                    lvl = depth.asks[0]
                    for lvl in depth.asks:
                        cum_q += lvl.qty
                        if cum_q >= sbm.qty:
                            break
                    tgt_prc = lvl.prc
                elif sbm.side == Side.short:
                    if len(depth.bids) == 0:
                        return
                    cum_q = 0
                    lvl = depth.bids[0]
                    for lvl in depth.bids:
                        cum_q += lvl.qty
                        if cum_q >= sbm.qty:
                            break
                    tgt_prc = lvl.prc
                else:
                    self.logger.error(f"unknown side: {sbm.__dict__}")
                    return
            else:
                self.logger.info(f"Unsupported orderPriceType: {sbm.orderPriceType}")
                return
            self.logger.info(f"Target price = {tgt_prc}")
            contract = self.ex_pair_to_contract_map[sbm.symbol]
            om = self.order_manager_map[contract]
            if sbm.side == Side.long:
                orders_to_cancel = []
                for int_prc, oids in om.ask_ladder.items():
                    prc = om.int_prc_to_prc(int_prc=int_prc)
                    if prc < tgt_prc + self.numeric_tol:
                        self.logger.info(f"Price level = {prc}")
                        for oid in oids:
                            o = om.order_at(oid)
                            assert isinstance(o, Order), f"order type: {type(o)} {id(o.__class__)} {id(Order)} {o.__module__} {Order.__module__}"
                            if o.status not in OrderStatus.fin_status():
                                orders_to_cancel.append(o)
                for o in orders_to_cancel:
                    try:
                        self.logger.info(f"Canceling order = {o.__dict__}")
                        await self.wrapped_cancel_order(o)
                        self.logger.info(f"Canceled order = {o.__dict__}")
                    except Exception as e:
                        self.logger.error(f"cancel fail: {e} {o.__dict__}")
            elif sbm.side == Side.short:
                orders_to_cancel = []
                for int_prc, oids in om.bid_ladder.items():
                    prc = om.int_prc_to_prc(int_prc=int_prc)
                    if prc > tgt_prc - self.numeric_tol:
                        self.logger.info(f"Price level = {prc}")
                        for oid in oids:
                            o = om.order_at(oid=oid)
                            assert isinstance(o, Order), f"order type: {type(o)} {id(o.__class__)} {id(Order)} {o.__module__} {Order.__module__}"
                            if o.status not in OrderStatus.fin_status():
                                orders_to_cancel.append(o)
                for o in orders_to_cancel:
                    try:
                        self.logger.info(f"Canceling order = {o.__dict__}")
                        await self.wrapped_cancel_order(o)
                        self.logger.info(f"Canceled order = {o.__dict__}")
                    except Exception as e:
                        self.logger.error(f"cancel fail: {e} {o.__dict__}")
            else:
                self.logger.error(f"unknown side: {sbm.__dict__}")
                return

        except:
            self.logger.critical(traceback.format_exc())

    async def strategy_core(self):
        while True:
            await asyncio.sleep(60)


if __name__ == '__main__':
    logging.getLogger().setLevel("CRITICAL")
    loop = asyncio.get_event_loop()
    s = LBankColoStrategy(loop)
    s.run_forever()
