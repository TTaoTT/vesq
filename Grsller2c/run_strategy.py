import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

# smooth the way of changing quantity.

class PriceDistributionCache():
    def __init__(self, cache_size=50, dist_cache_size=5*60*1000,recvWindow=1300,tick_range=4) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        self.mp = None
        self.dist = SortedDict()
        self.mp_ts = None
        self.dist_cache_size = dist_cache_size
        self.dist_cache = []
        self.recvWindow = recvWindow
        self.tick_range = tick_range
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
        
        if self.mp:
            if trade["t"] > self.mp_ts + self.recvWindow:
                self.mp = self.get_ask_price(0.5)
                self.mp_ts = trade["t"]
                
            diff = trade["p"] - self.mp
            if self.dist.__contains__(diff):
                self.dist[diff] += trade["q"]
            else:
                self.dist[diff] = trade["q"]
            
            tmp = {
            "t":trade["t"],
            "diff":diff, 
            "q":trade["q"]
            }
            self.dist_cache.append(tmp)
        else:
            self.mp = self.get_ask_price(0.5)
            self.mp_ts = trade["t"]
        
    def remove_expired(self, ts):
        if not self.cache and not self.dist_cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
            
        while self.dist_cache and self.dist_cache[0]["t"] < ts-self.dist_cache_size:
            old = self.dist_cache.pop(0)
            if self.dist[old["diff"]]<=old["q"]:
                self.dist.pop(old["diff"])
            else:
                self.dist[old["diff"]] -= old["q"]
            
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_qty(self):
        return self.q_sum
    
    def get_dist_stats(self):
        v0 = self.dist[0]
        ask_dist = SortedDict(filter(lambda x: (x[0] > 0) & (x[0] <= self.tick_range), self.dist.items()))
        bid_dist = SortedDict(filter(lambda x: (x[0] < 0) & (x[0] >= -self.tick_range), self.dist.items()))

        x_ask = np.array(ask_dist.keys())
        y_ask = np.array(np.log(ask_dist.values()))
        A_ask = np.vstack([x_ask, np.ones(len(x_ask))]).T
        ask_keppa, _ = np.linalg.lstsq(A_ask, y_ask, rcond=None)[0]

        x_bid = np.array(bid_dist.keys())
        y_bid = np.array(np.log(bid_dist.values()))
        A_bid = np.vstack([x_bid, np.ones(len(x_bid))]).T
        bid_keppa, _ = np.linalg.lstsq(A_bid, y_bid, rcond=None)[0]
        # ask = self.get_median("ask")
        # bid = self.get_median("bid")
        # self.mp = self.get_ask_price(0.5)
        # self.dist = SortedDict()
        return -ask_keppa, bid_keppa

class OrderCacheManager():
    def __init__(self) -> None:
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.exposed_pos = Decimal(0)
        
    def add_order(self,order):
        self.order_cache[order.client_id] = order
        self.exposed_pos = self.exposed_pos + order.requested_amount if order.side == OrderSide.Buy else self.exposed_pos - order.requested_amount

    def can_send_order(self):
        ask = 0
        bid = 0
        for cid in self.order_cache:
            if self.order_cache[cid].side == OrderSide.Buy:
                bid += 1
            elif self.order_cache[cid].side == OrderSide.Sell:
                ask += 1
        return ask, bid
    
    def new_agg_trade(self,trade):
        # {"t":timestamp,"p":price}
        for cid in self.order_cache:
            o = self.order_cache[cid]
            time_cond = trade["t"] > o.message["xchg_create_ms"] if o.message["xchg_create_ms"] else False
            order_type_cond = o.type != OrderType.IOC and o.type != OrderType.FOK
            cond1 = o.side == OrderSide.Buy and \
                not o.message["status"] and \
                time_cond and \
                o.requested_price > trade["p"] and \
                order_type_cond
            cond2 = o.side == OrderSide.Sell and \
                not o.message["status"] and \
                time_cond and \
                o.requested_price < trade["p"] and \
                order_type_cond
                
            if cond1 or cond2:
                self.order_cache[cid].message["status"] = 1
                self.order_cache[cid].message["filled"] = o.requested_amount 
            
    def after_cancel(self,cid,res):
        if self.order_cache.get(cid) == None:
            return
        o = self.order_cache[cid]
        if o.message["status"] == 0 and res == True and o.message["sent"]:
            self.order_cache[cid].message["status"] = 1
            self.order_cache[cid].message["filled"] = o.requested_amount
            self.pos = self.pos + o.requested_amount if o.side == OrderSide.Buy else self.pos - o.requested_amount
        elif type(res) == dict and res["status"] == "CANCELED" and o.message["status"] == 0:
            self.order_cache[cid].message["status"] = 1
            amount_changed = Decimal(res["executedQty"])
            self.order_cache[cid].message["filled"] = amount_changed
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            canceled = o.requested_amount - amount_changed
            self.exposed_pos = self.exposed_pos - canceled if o.side == OrderSide.Buy else self.exposed_pos + canceled
        else:
            self.order_cache[cid].message["cancel"] += 1
            
    def after_send_result(self,cid,res):
        # Don't need it right now since the response for make_order is "ACK"
        o = self.order_cache[cid]
        if res == False:
            self.order_cache[cid].message["status"] = 1
            self.exposed_pos = self.exposed_pos - o.requested_amount if o.side == OrderSide.Buy else self.exposed_pos + o.requested_amount
        elif res["status"] == "NEW":
            self.order_cache[cid].message["xchg_create_ms"] = res["updateTime"]
        elif res["status"] == "PARTIALLY_FILLED":
            if not o.message["xchg_create_ms"]:
                self.order_cache[cid].message["xchg_create_ms"] = res["updateTime"]
            amount_changed = Decimal(res["executedQty"]) - o.message["filled"]
            self.order_cache[cid].message["filled"] = Decimal(res["executedQty"])
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            # TODO:what will it be if ioc order is sent?
        elif res["status"] == "FILLED":
            self.order_cache[cid].message["status"] = 1
            amount_changed = Decimal(res["executedQty"]) - o.message["filled"]
            self.order_cache[cid].message["filled"] = Decimal(res["executedQty"])
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            
    def after_send_ack(self,cid):
        if self.order_cache.get(cid):
            self.order_cache[cid].message["sent"] = True
            
    def order_finish(self,cid,filled):
        # TODO make sure this is wright
        o = self.order_cache[cid]
        if o.message["status"] == 0:
            self.pos = self.pos + filled if o.side == OrderSide.Buy else self.pos - filled
            canceled = o.requested_amount - filled
            self.exposed_pos = self.exposed_pos - canceled if o.side == OrderSide.Buy else self.exposed_pos + canceled
        else:
            if o.message["filled"] != filled:
                amount_changed = filled - o.message["filled"]
                self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
                self.exposed_pos = self.exposed_pos + amount_changed if o.side == OrderSide.Buy else self.exposed_pos - amount_changed
        self.order_cache.pop(cid)
    
    def check_exposure(self):
        expose = Decimal(0)
        for cid in self.order_cache:
            o = self.order_cache[cid]
            if o.message["status"] == 0:
                expose = expose + o.requested_amount if o.side == OrderSide.Buy else expose - o.requested_amount
            
        expose += self.pos
        if expose != self.exposed_pos:
            return expose, False
        else:
            return expose, True

        
    def has_open_order(self):
        for cid in self.order_cache:
            if self.order_cache[cid].message["status"] == 0:
                return True
        return False

class QtyManager():
    def __init__(self,cache_size=60*1000*30) -> None:
        self.cache = []
        self.cache_size = cache_size
        self.buy_qty_sum = 0
        self.sell_qty_sum = 0
        self.buy_amount_sum = 0
        self.sell_amount_sum = 0
    
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
        if trade["s"] == "BUY":
            self.buy_qty_sum += trade["q"]
            self.buy_amount_sum += trade["q"] * trade["p"]
        else:
            self.sell_qty_sum += trade["q"]
            self.sell_amount_sum += trade["q"] * trade["p"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            trade = self.cache.pop(0)
            if trade["s"] == "BUY":
                self.buy_qty_sum -= trade["q"]
                self.buy_amount_sum -= trade["q"] * trade["p"]
            else:
                self.sell_qty_sum -= trade["q"]
                self.sell_amount_sum -= trade["q"] * trade["p"]
                
    def get_spread(self):
        if not self.sell_qty_sum  or not self.buy_qty_sum:
            return None
        if self.sell_qty_sum + self.buy_qty_sum < 100:
            return None
        sell_price = self.sell_amount_sum / self.sell_qty_sum
        buy_price = self.buy_amount_sum / self.buy_qty_sum 
        return (sell_price - buy_price) / (sell_price + buy_price) * 2
    
class SpreadCalculator():
    def __init__(self, alpha=1, t=10,lam=1) -> None:
        self.lam = lam
        self.alpha = alpha
        self.t = t
    
    def get_h(self,q,keppa):
        w1 = np.exp(-self.alpha*keppa*(q**2))
        w2 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.alpha*keppa*((q-1)**2))
        w3 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.alpha*keppa*((q+1)**2))
        return 1/keppa * np.log(w1 + w2 + w3)
    
    def get_ask(self,inven,keppa):
        return 1/keppa - self.get_h(inven-1,keppa) + self.get_h(inven,keppa)
    
    def get_bid(self,inven,keppa):
        return 1/keppa - self.get_h(inven+1,keppa) + self.get_h(inven,keppa)
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]

        self.min = self.params["min"]
        self.recvWindow = self.params["recvWindow"]
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.trade_cache = PriceDistributionCache(100,self.min*60*1000,self.recvWindow,self.params["tick_range"])
        self.sc = SpreadCalculator(alpha=self.params["alpha"])

        self.order_to_send = Queue()
        self.order_to_cancel = Queue()
        self.retreat_queue = Queue()
        self.bbo_ts = 0
        self.pos_limit = Decimal(5)

        self.order_cache = OrderCacheManager()
        self.qty_manager = QtyManager()
        self.ask_order_cache = dict()
        self.bid_order_cache = dict()

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False
        
        self.order_count = 0
        self.order_fail_count = 0

        self.delay = 0
        
        self.last_symbol = None
        self.ap = None

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order_inner(o)
        except:
            pass
        
    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.8*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.8*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.8*1200

        if cond1 or cond2 or cond3:
            self.logger.warning(f"{float(headers['X-MBX-USED-WEIGHT-1M'])},{float(headers['X-MBX-ORDER-COUNT-10S'])}, {float(headers['X-MBX-ORDER-COUNT-1M'])}")
            self.hold_back = True
            self.logger.warning("hold back")
            api = self.get_exchange_api("binance")[0]
            self.loop.create_task(api.flash_cancel_orders(self.get_symbol_config(self.symbol)))
            await asyncio.sleep(1)
            self.hold_back = False

    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.order_cache.pos = position.data["long_qty"] - position.data["short_qty"]
            self.order_cache.exposed_pos = position.data["long_qty"] - position.data["short_qty"]

            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol)
                self.price_tick = symbol_cfg['price_tick_size']
                self.qty_tick = symbol_cfg['qty_tick_size']
                self.half_step_tick = self.qty_tick/Decimal(2)
                self.half_price_tick = self.price_tick/Decimal(2)
                c = 0
                
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ap:
                        self.logger.warning(f"{self.ap}")
                        amount = min(balance,Decimal(self.params["amount"]))
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/self.qty_tick)*self.qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
                        break
                    if c > 1000:
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise

    async def update_amount(self):
        api = self.get_exchange_api("binance")[0]
        while True:
            await asyncio.sleep(15)
            self.logger.warning(f"cur_risk: {self.order_cache.pos/self.amount_decimal}")
            if abs(self.order_cache.pos/self.amount_decimal) >= 0.5:
                continue
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                self.qty_manager.remove_expired(time.time()*1e3)
                spread = self.qty_manager.get_spread()
                self.logger.warning(f"spread: {spread}, buy qty: {self.qty_manager.buy_qty_sum}, sell qty: {self.qty_manager.sell_qty_sum}, cur amount: {self.amount}")
                if spread != None and spread > 0.0001:
                    amount = Decimal(self.params["amount_extend"])
                elif spread != None and spread > -0.0001:
                    amount = Decimal(self.params["amount"])
                elif spread == None:
                    amount = Decimal(self.params["amount"])
                else:
                    amount = Decimal(self.params["amount_min"])
                
                if self.ap:
                    amount = amount / Decimal(self.ap)
                    
                    symbol_cfg = self.get_symbol_config(self.symbol)
                    qty_tick = symbol_cfg["qty_tick_size"]
                    amount = float(int(amount/qty_tick)*qty_tick)
                    
                    self.amount = amount
                    self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")
            
    async def on_order(self, o):
        # self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        client_id = order["c"]
        if client_id not in list(self.order_cache.order_cache):
            return
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        
        if status in OrderStatus.fin_status():
            filled = Decimal(order["z"])
            if Decimal(order["z"]) > Decimal(0):
                ts = time.time()*1e3
                tmp = {"t":ts,"s":order["S"],"p":float(order["ap"]),"q":float(order["z"])/self.amount}
                self.qty_manager.feed_trade(tmp)
                self.qty_manager.remove_expired(ts)
            self.order_cache.order_finish(client_id,filled)

    async def on_order_inner(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache.order_cache): 
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            filled = order.filled_amount
            if order.filled_amount > Decimal(0):
                ts = time.time()*1e3
                side = "BUY" if self.order_cache.order_cache[client_id].side==OrderSide.Buy else "SELL"
                tmp = {"t":ts,"s":side,"p":float(order.avg_filled_price),"q":float(order.filled_amount)/self.amount}
                self.qty_manager.feed_trade(tmp)
                self.qty_manager.remove_expired(ts)
            self.order_cache.order_finish(client_id,filled)

    async def on_bbo(self, symbol, bbo):
        if symbol != self.last_symbol:
            self.logger.warning(f"wrong symbol: {symbol}")
        self.last_symbol = symbol
        
        self.ap = float(bbo["data"]["a"])
        self.bp = float(bbo["data"]["b"])
        self.bbo_ts = bbo["data"]["E"]
        
        if self.order_cache.has_open_order() and self.order_to_cancel.empty():
            self.order_to_cancel.put_nowait(1)
    
    async def on_agg_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        tmp = {
            "t":trade["data"]["E"],
            "p":int(Decimal(trade["data"]["p"])/self.price_tick), 
            "q":int(Decimal(trade["data"]["q"])/self.qty_tick)
            }
        if trade["data"]["m"]:
            self.trade_cache.feed_trade(tmp)
        else:
            self.trade_cache.feed_trade(tmp)

        self.trade_cache.remove_expired(trade["data"]["E"])
        self.trade_cache.remove_expired(trade["data"]["E"])
    
    async def main_logic(self):
        while True:
            await asyncio.sleep(60)
            ask_keppa, bid_keppa = self.trade_cache.get_dist_stats()
            self.logger.warning(f"""
            ask_keppa:{ask_keppa} , bid_keppa:{bid_keppa}
            """)

    async def send_order_action(self):
        while True:
            await asyncio.sleep(0.02)
            if not self.send or self.rate_limit or self.hold_back:
                continue
            
            cur_risk = self.order_cache.pos/self.amount_decimal
            
            mp = self.trade_cache.get_ask_price(0.5)
            if not mp:
                mp = Decimal((self.ap + self.bp) / 2)
            else:
                mp = Decimal(mp * self.price_tick)
            
            ask_k , bid_k = self.trade_cache.get_dist_stats()
            
            ask = mp + Decimal(self.sc.get_ask(float(cur_risk),ask_k)) * self.price_tick + self.half_price_tick if ask_k > 0 else mp*Decimal(1+0.005)
            bid = mp - Decimal(self.sc.get_bid(float(cur_risk),bid_k)) * self.price_tick + self.half_price_tick if bid_k > 0 else mp*Decimal(1-0.005)

            if ask <= Decimal(self.bp):
                ask = Decimal(self.bp) + self.half_price_tick * Decimal(3)
            if bid >= Decimal(self.ap):
                bid = Decimal(self.ap) - self.half_price_tick

            a, b = self.order_cache.can_send_order()
            if a == 0:
                # self.logger.warning(f"ask_keppa: {ask_k}")
                if -5 < cur_risk:
                    if 0 < cur_risk < 1:
                        self.loop.create_task(self.send_order(ask,self.amount_decimal+self.order_cache.pos,OrderSide.Sell))
                    else:
                        self.loop.create_task(self.send_order(ask,self.amount_decimal,OrderSide.Sell))
            
            if b == 0:
                # self.logger.warning(f"bid_keppa: {bid_k}")
                if cur_risk < 5:
                    if -1 < cur_risk < 0:
                        self.loop.create_task(self.send_order(bid,self.amount_decimal-self.order_cache.pos,OrderSide.Buy))
                    else:
                        self.loop.create_task(self.send_order(bid,self.amount_decimal,OrderSide.Buy))

            
    async def rate_limit_check(self):
        while True:
            await self.retreat_queue.get()
            if self.rate_limit and self.send:
                self.send = False
                await self.retreat()
                self.send = True
            
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=None,timestamp=None):
        client_id = ClientIDGenerator.gen_client_id("binance")
        self.order_count += 1
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow
        
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = recvWindow/1000
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        extra_info["status"] =  0
        extra_info["filled"] =  Decimal(0)
        extra_info["sent"] = False
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache.add_order(order)
        if timestamp == None:
            timestamp = order.create_ms
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=timestamp,
                recvWindow=recvWindow,
                client_id=client_id
            )
            self.order_cache.after_send_ack(client_id)
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            self.order_fail_count += 1
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except ExchangeTemporaryError as err:
            await self.internal_fail(client_id, err)
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def cancel_order(self, order: Order, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.json
        except Exception as err:
            if not catch_error:
                raise
            if type(err) in (OrderAlreadyCompletedError, OrderNotFoundError):
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect True")
                return True
            elif type(err) == ExchangeTemporaryError:
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect False")
                return False
            elif isinstance(err, ExchangeConnectorException):
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, raise unknown api error")
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False

    async def batch_cancel(self, oid, order):
        try:
            if self.order_cache.order_cache.get(oid) == None:
                return
            if oid not in self.canceling_id and self.order_cache.order_cache[oid].message["cancel"] < 4:
                self.canceling_id.add(oid)
                res = await self.cancel_order(order)
                self.canceling_id.remove(oid)
                self.order_cache.after_cancel(oid, res)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
        while True:
            await asyncio.sleep(0.01)
            for oid, order in self.order_cache.order_cache.items():
                self.loop.create_task(batch_cancel(oid, order))
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if not order.message:
                # self.logger.warning(f"order has no message while try to check,{oid}")
                return
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if not order.message["sent"]:
                # self.logger.warning(f"order not sent yet while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*6 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["query"] += 1
                        if self.order_cache.order_cache[oid].message["query"]>10:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order_inner(order_new)
                        else:
                            self.loop.create_task(self.batch_cancel(oid,order_new))
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(Decimal(self.bp*0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(Decimal(self.ap*1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None,"fail rate":self.order_fail_count/self.order_count if self.order_count else 0})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(60)
        await asyncio.gather(
            self.update_redis_cache(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            self.update_amount(),
            self.rate_limit_check(),
            self.main_logic()
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    