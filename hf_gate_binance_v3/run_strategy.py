import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy
from decimal import ROUND_HALF_UP

MAX_AMOUNT = Decimal(200)

"""
class Order(object):
    internal_symbol = attr.ib()
    xchg_name = attr.ib()
    account_name = attr.ib()
    xchg_id = attr.ib(converter=str)
    xchg_symbol = attr.ib()

    side = attr.ib(validator=[attr.validators.instance_of(OrderSide)])
    type = attr.ib(validator=[attr.validators.instance_of(OrderType)])
    xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])

    time_in_force = attr.ib(validator=[attr.validators.instance_of(OrderTimeInForce)])
    requested_price = attr.ib(converter=num_to_decimal)
    requested_amount = attr.ib(converter=num_to_decimal)
    filled_amount = attr.ib(converter=num_to_decimal)
    avg_filled_price = attr.ib(converter=num_to_decimal)
    commission_fee = attr.ib(converter=num_to_decimal)

    is_finished = attr.ib()

    # Timestamp related
    local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
    server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message

    # Event type of this order object, will be used to trace user and xchg event
    evt_type = attr.ib(validator=[attr.validators.instance_of(OrderEventType)], default=OrderEventType.XchgUpdate)

    created_ts_ms = attr.ib(default=0, converter=int)  # Order created ts
    finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts

    # Update historical records
    update_historical_records = attr.ib(factory=list)

    # Trade records
    trade_records = attr.ib(factory=list)

    # Position side
    position_side = attr.ib(validator=[attr.validators.instance_of(OrderPositionSide)], default=OrderPositionSide.Open)
    strategy_name = attr.ib(default="UNSET")
    tag = attr.ib(default="PY-ATOM")
    extra_info = attr.ib(default="")

    raw_data = attr.ib(default=None)
"""




class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_binance = "binance.1000shib_usdt_swap.swap"
        self.symbol_gate = "gateio.shib_usdt_swap.swap"
        self.ap_binance = Decimal(0)
        self.ap_gate = Decimal(0)
        self.bp_binance = Decimal(0)
        self.bp_gate = Decimal(0)
        self.mp_binance = Decimal(0)
        self.mp_gate = Decimal(0)
        self.ob_update = False
        self.send_order = True
        self.pos = Decimal(0)
        self.long_pos = Decimal(0)
        self.short_pos = Decimal(0)
        self.short_close_lock = Decimal(0)
        self.long_close_lock = Decimal(0)
        self.pos_price = Decimal(0)
        self.binance_ob_ts = time.time()
        self._orders_waiting_to_send = Queue()
        self._orders_waiting_to_cancel = Queue()
        self._orders_waiting_to_tf = Queue()

        self.ap_diff = collections.deque(maxlen=1000)
        self.bp_diff = collections.deque(maxlen=1000)
        self.mp_diff = collections.deque(maxlen=1000)

        self.ap_diff.append(float(0))
        self.bp_diff.append(float(0))
        self.mp_diff.append(float(0))

        self.action_count = 0
        self.max_pos = Decimal(0)

        self.trade_amount = Decimal(0)

        self.ap_binance_cache = []
        self.bp_binance_cache = []

        self.hedge_order_to_post = []
        self.order_cache = dict()
        self.unknown_order_dict = dict()

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        self.heartbeat_order_enabled = False
        self.max_balance = Decimal(0)

    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        await self._get_symbol_config_from_remote([self.symbol_gate, self.symbol_binance])
        task = self.loop.create_task(self._subscribe_orderbook_direct_(self.symbol_gate))
        await self.subscribe_ticker_direct(self.symbol_binance)
        # self.subscribe_public_trade([self.symbol_data])
        self.direct_subscribe_order_update('gateio',symbols=[self.symbol_gate])
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()

    async def on_ticker(self, symbol, ticker):
        self.binance_ob_ts = time.time()

        if symbol not in self.symbol_binance:
            return
        if Decimal(ticker["a"][0]) == self.ap_binance or Decimal(ticker["b"][0]) == self.bp_binance:
            return
        
        self.ob_update = True

        self.ap_binance = Decimal(ticker["a"][0]) / Decimal(1000)
        self.bp_binance = Decimal(ticker["b"][0]) / Decimal(1000)
        self.mp_binance = (self.ap_binance + self.bp_binance) / Decimal(2)

        ask_order = None
        bid_order = None
        balance_order = None

        for oid in list(self.order_cache):
            if self.order_cache[oid].raw_data["source"] == 1:
                if self.order_cache[oid].side == OrderSide.Sell:
                    ask_order = self.order_cache[oid]
                    ask_id = oid
                else:
                    bid_order = self.order_cache[oid]
                    bid_id = oid
            if self.order_cache[oid].raw_data["source"] == -1:
                balance_order = self.order_cache[oid]
                balance_oid = oid
                if self.order_cache[oid].side == OrderSide.Sell:
                    cancel = self.order_cache[oid].requested_price < self.pos_price
                else:
                    cancel = self.order_cache[oid].requested_price > self.pos_price

        if balance_order:
            if abs(self.pos) < balance_order.requested_amount*Decimal(0.9999) or abs(self.pos) > balance_order.requested_amount*Decimal(1.0001) or cancel:
                self.order_cache[balance_oid].raw_data["stop_ts"] = 0
                self.order_cache[balance_oid].raw_data["cancel"] += 1
                await self._orders_waiting_to_cancel.put(self.order_cache[balance_oid])

        if ask_order:
            ap_ts = self.bp_binance
            bp_ts = self.ap_binance
            if (ask_order.requested_price < bp_ts) or (ask_order.requested_price > ap_ts+Decimal(np.std(self.ap_diff))):
                self.order_cache[ask_id].raw_data["stop_ts"] = 0
                self.order_cache[ask_id].raw_data["cancel"] += 1
                await self._orders_waiting_to_cancel.put(self.order_cache[ask_id])
        if bid_order:
            ap_ts = self.bp_binance
            bp_ts = self.ap_binance
            if (bid_order.requested_price > ap_ts) or (bid_order.requested_price < bp_ts-Decimal(np.std(self.bp_diff))):
                self.order_cache[bid_id].raw_data["stop_ts"] = 0
                self.order_cache[bid_id].raw_data["cancel"] += 1
                await self._orders_waiting_to_cancel.put(self.order_cache[bid_id])

        self.ap_binance_cache.append(self.ap_binance)
        self.bp_binance_cache.append(self.bp_binance)


    async def on_orderbook(self, symbol, orderbook):
        self.ob_update = True
        if symbol in self.symbol_gate:
            self.ap_gate = orderbook["asks"][0][0]
            self.bp_gate = orderbook["bids"][0][0]
            self.mp_gate = (self.ap_gate + self.bp_gate) / Decimal(2)

            if self._orders_waiting_to_send.empty() and time.time() - self.binance_ob_ts < 0.01:
                await self._orders_waiting_to_send.put(1)
            if self._orders_waiting_to_tf.empty():
                await self._orders_waiting_to_tf.put(1)

    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[xchg_id].filled_amount
        if amount_changed <= Decimal(0):
            return 
        if self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            self.pos_price = (self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (self.pos + amount_changed)
            self.pos += amount_changed
        elif self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            if self.pos < amount_changed:
                self.pos_price = p_order.avg_filled_price
            self.pos -= amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            if self.pos + amount_changed > Decimal(0):
                self.pos_price = p_order.avg_filled_price
            self.pos += amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            self.pos_price = (-self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (-self.pos + amount_changed)
            self.pos -= amount_changed
        self.trade_amount += amount_changed * p_order.avg_filled_price * 10000
        self.max_pos = max(abs(self.pos), self.max_pos)

        if (self.order_cache[xchg_id].side == OrderSide.Buy and self.order_cache[xchg_id].position_side == OrderPositionSide.Open) or \
                (self.order_cache[xchg_id].side == OrderSide.Sell and self.order_cache[xchg_id].position_side == OrderPositionSide.Close):
            self.long_pos += Decimal((self.order_cache[xchg_id].side == OrderSide.Buy)*2 -1) * amount_changed
        else:
            self.short_pos += Decimal((self.order_cache[xchg_id].side == OrderSide.Sell)*2 -1) * amount_changed

    def handle_lock(self, xchg_id):
        if self.order_cache[xchg_id].side == OrderSide.Buy and self.order_cache[xchg_id].position_side == OrderPositionSide.Close:
            self.short_close_lock -= self.order_cache[xchg_id].requested_amount
        elif self.order_cache[xchg_id].side == OrderSide.Sell and self.order_cache[xchg_id].position_side == OrderPositionSide.Close:
            self.long_close_lock -= self.order_cache[xchg_id].requested_amount

    async def on_order(self, xchg_id, order: PartialOrder):
        """
        class PartialOrder(object):
            # used to update redis order
            xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])
            filled_amount = attr.ib(converter=num_to_decimal)
            avg_filled_price = attr.ib(converter=num_to_decimal)
            commission_fee = attr.ib(converter=num_to_decimal)
            is_finished = attr.ib()  
            # Timestamp related
            local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
            server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message
            finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts
        """
        if xchg_id not in list(self.order_cache):
            # add unknow order dealing funciton, check_unknown_order.
            # before, will only check order after order expired. 
            self.unknown_order_dict[xchg_id] = order
            return
        symbol_cfg = self.get_symbol_config(self.symbol_gate)
        qt_tick = symbol_cfg['qty_tick_size']
        order.filled_amount = order.filled_amount.quantize(qt_tick, ROUND_HALF_UP)
        
        if order.filled_amount < self.order_cache[xchg_id].filled_amount:
            return
        

        self.handle_pos(xchg_id,order)
        self.order_cache[xchg_id].filled_amount = order.filled_amount
        self.order_cache[xchg_id].avg_filled_price = order.avg_filled_price
        self.order_cache[xchg_id].is_finished = order.is_finished
        self.order_cache[xchg_id].xchg_status = order.xchg_status

        if order.is_finished:
            self.handle_lock(xchg_id)
            self.order_cache.pop(xchg_id)
            if xchg_id in self.unknown_order_dict:
                self.unknown_order_dict.pop(xchg_id)

    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.01)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.order_cache:
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(xchg_id, partial_order) for xchg_id, partial_order in now_know_order.items()]
            )

    async def generate_orders(self):
        orders = []

        symbol_cfg = self.get_symbol_config(self.symbol_gate)
        price_tick = symbol_cfg['price_tick_size']

        mp_diff_mean = Decimal(np.mean(self.mp_diff))
        mp_diff_std = Decimal(np.std(self.mp_diff))

        max_amount = MAX_AMOUNT*self.params["base_amount"]

        ap_ts = self.bp_binance
        ap_gap = max(float((-self.pos/max_amount))*0.001, 0)
        ap_ts = ap_ts * Decimal(1 + ap_gap)
        
        bp_ts = self.ap_binance
        bp_gap = max(float((self.pos / max_amount)) * 0.001, 0)
        bp_ts = bp_ts * Decimal(1 - bp_gap)

        if self.pos < max_amount:
            if bp_ts < self.ap_gate:
                bid_p = bp_ts
            else:
                bid_p = self.ap_gate - price_tick
            if self.pos < Decimal(0) and self.pos_price >= bid_p:
                bid_q = -self.pos
                orders.append([self.symbol_gate, bid_p, bid_q, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,300,-1])
            elif abs(self.pos)<self.params["base_amount"]:
                bid_q = self.params["base_amount"]
                orders.append([self.symbol_gate, bid_p, bid_q, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,10,1])
            elif self.pos>=self.params["base_amount"] and self.pos_price > bp_ts:
                if self.pos_price*Decimal(1-0.005) > bp_ts:
                    bid_q = self.params["base_amount"] * Decimal(2)
                elif self.pos_price*Decimal(1-0.01) > bp_ts:
                    bid_q = self.params["base_amount"] * Decimal(4)
                else:
                    bid_q = self.params["base_amount"]

                orders.append([self.symbol_gate, bid_p, bid_q, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,10,1])

        if self.pos > -max_amount:
            if ap_ts > self.bp_gate:
                ask_p = ap_ts
            else:
                ask_p = self.bp_gate + price_tick
            if self.pos > Decimal(0) and self.pos_price <= ask_p:
                ask_q = self.pos
                orders.append([self.symbol_gate, ask_p, ask_q, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,300,-1])
            elif abs(self.pos)<self.params["base_amount"]:
                ask_q = self.params["base_amount"]
                orders.append([self.symbol_gate, ask_p, ask_q, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,10,1])
            elif self.pos<=-self.params["base_amount"] and self.pos_price < ap_ts:
                if self.pos_price*Decimal(1+0.005) < ap_ts: 
                    ask_q = self.params["base_amount"]*Decimal(2)
                elif self.pos_price*Decimal(1+0.01) < ap_ts:
                    ask_q = self.params["base_amount"]*Decimal(4)
                else:
                    ask_q = self.params["base_amount"]

                orders.append([self.symbol_gate, ask_p, ask_q, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,10,1])

        if (self.pos < -max_amount and self.ap_gate > self.pos_price*Decimal(1.01)) or (self.pos > max_amount and self.bp_gate <self.pos_price*Decimal(0.99)):
            self.send_order = False
            self.logger.warning("stop loss")
            for i in range(3):
                res = await self.get_current_position(self.symbol_gate)
                await self.close_position([self.symbol_gate, self.mp_gate*Decimal(1.01), abs(res["short_qty"]), OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel])
                await self.close_position([self.symbol_gate, self.mp_gate*Decimal(0.99), abs(res["long_qty"]), OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel])
                await asyncio.sleep(3)
            await asyncio.sleep(60)
            self.send_order = True
            
        buy_source = [self.order_cache[key].raw_data["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Buy]
        sell_source = [self.order_cache[key].raw_data["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Sell]
        
        filter_orders = []

        for per_req in orders:
            if per_req[3] == OrderSide.Buy:
                x_source = np.array(buy_source)
            else:
                x_source = np.array(sell_source)
            x_source = np.array(x_source)
            cur_pending_len = sum((x_source == per_req[7]) * 1)
            if cur_pending_len < 1:
                filter_orders.append(per_req)

        return self.build_oc(filter_orders)

    def build_oc(self, req_orders):
        orders = []
        short_close_lock_cache = Decimal(0)
        long_close_lock_cache = Decimal(0)
        for order in req_orders:
            if order[3] == OrderSide.Buy:
                if self.short_pos - self.short_close_lock - short_close_lock_cache> order[2]:
                    order[4] = OrderPositionSide.Close
                    orders.append(order)
                    short_close_lock_cache += order[2]
                else:
                    orders.append(order)
            else:
                if self.long_pos - self.long_close_lock - long_close_lock_cache> order[2]:
                    order[4] = OrderPositionSide.Close
                    orders.append(order)
                    long_close_lock_cache += order[2]
                else:
                    orders.append(order)
        return orders

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def send_order_action(self):
        async def batch_send_order(params):
            if not self.volume_notional_check(*params[:3]):
                return
            try:
                order =  await self.make_future_order(*params[:6])
                self.action_count += 1
                if order:
                    extra_info = dict()
                    extra_info["stop_ts"] = params[6]
                    extra_info["cancel"] = 0
                    extra_info["source"] =  params[7] # entry or balance
                    extra_info["query"] =  0
                    order.raw_data = extra_info
                    self.order_cache[order.xchg_id] = order
                    self.order_cache[order.xchg_id].filled_amount = Decimal(0)
                    if params[3] == OrderSide.Buy and params[4] == OrderPositionSide.Close:
                        self.short_close_lock += order.requested_amount
                    elif params[3] == OrderSide.Sell and params[4] == OrderPositionSide.Close:
                        self.long_close_lock += order.requested_amount

            except Exception as err:
                self.logger.warning(f"send order err, {err}")

        while True:
            await self._orders_waiting_to_send.get()
            if self.send_order and self.action_count < 100:
                orders = await self.generate_orders()
                if not orders:
                    continue
                try:
                    await asyncio.gather(*[batch_send_order(order_params) for order_params in orders])
                except Exception as err:
                    self.logger.warning(f"batch send order err, {err}")

    async def update_cancel_order(self):

        async def batch_cancel(oid, order):

            if time.time()*1e3 - order.created_ts_ms > order.raw_data["stop_ts"] * 1e3 + order.raw_data["cancel"]*1e3:
                try:
                    await self._orders_waiting_to_cancel.put(order)
                    if self.order_cache.get(oid):
                        self.order_cache[oid].raw_data["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f'cancel order update {oid} err {err}')
            
        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )
    
    async def cancel_order_action(self):
        while True:
            order = await self._orders_waiting_to_cancel.get()
            if not self.order_cache.get(order.xchg_id):
                continue
            try:
                res = await self.cancel_order(order)
                self.action_count += 1
            except Exception as err:
                self.logger.warning(f'cancel order {order.xchg_id} err {err}')

    async def close_position(self,params):
        if not self.volume_notional_check(*params[:3]):
            return
        try:
            order =  await self.make_future_order(*params)
            self.action_count += 1
            if order:
                extra_info = dict()
                extra_info["stop_ts"] = 1
                extra_info["cancel"] = 0
                extra_info["source"] = 0
                extra_info["query"] =  0
                order.raw_data = extra_info
                self.order_cache[order.xchg_id] = order
                self.order_cache[order.xchg_id].filled_amount = Decimal(0)
                if params[3] == OrderSide.Buy:
                    self.short_close_lock += order.requested_amount
                elif params[3] == OrderSide.Sell:
                    self.long_close_lock += order.requested_amount

        except Exception as err:
            self.logger.warning(f"close position err, {err}")

    async def direct_check_position(self):
        while True:
            await asyncio.sleep(1)
            try:
                res = await self.get_current_position(self.symbol_gate)
                
                if res["long_qty"] != self.long_pos or res["short_qty"] != self.short_pos:
                    self.logger.warning(
                        f"""
                        =========update wrong position==========
                        origin: long{self.long_pos}, short{self.short_pos}
                        update: long{res["long_qty"]}, short{res["short_qty"]}
                        """
                    )
                    self.long_pos = res["long_qty"]
                    self.short_pos = res["short_qty"]
                    self.pos = self.long_pos - self.short_pos
                self.short_close_lock = res["short_qty"] - res["short_available"]
                self.long_close_lock = res["long_qty"] - res["long_available"]
            except Exception as err:
                self.logger.warning(f'check position err {err}')
    
    async def update_redis_cache(self):

        async def batch_cancel(oid, order):
            try:
                await self.cancel_order(order)
                self.action_count += 1
            except Exception as err:
                self.logger.warning(f'cancel order {oid} err {err}')

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send_order = False
                    for i in range(3):
                        await asyncio.gather(
                            *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
                            )
                        res = await self.get_current_position(self.symbol_gate)
                        await self.close_position([self.symbol_gate, self.mp_gate*Decimal(1.01), abs(res["short_qty"]), OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel])
                        await self.close_position([self.symbol_gate, self.mp_gate*Decimal(0.99), abs(res["long_qty"]), OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel])
                        await asyncio.sleep(3)

                    await self.redis_set_cache(
                        {
                        "exit":None, 
                        }
                        )
                    
                    self.logger.warning(f"manually exiting")
                    exit()

                # if not self.qty_switching and data.get("base_amount") is not None:
                #     if abs(Decimal(data.get("base_amount")) - self.params["base_amount"]) > Decimal(0.05)*self.params["base_amount"]:
                #         self.params["base_amount"] = Decimal(data.get("base_amount"))
                #         self.base_amount = Decimal(data.get("base_amount"))
                        
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        async def set_cache():
            try:
                cur_balance = await self.get_balance(self.symbol_gate)
                equity = cur_balance.get("usdt").get("all")
                # self.max_balance = max(self.max_balance, equity)
                await self.redis_set_cache(
                    {
                    "exit":None, 
                    "mp_diff_mean":np.mean(self.mp_diff),
                    "mp_diff_std":np.std(self.mp_diff),
                    "gate_position":self.pos,
                    "gate_position_price":self.pos_price,
                    "trade_amount":self.trade_amount,
                    "max_pos":self.max_pos,
                    "cur_balance":equity,
                    # "max_balance":self.max_balance
                    }
                    )
            except Exception as err:
                self.logger.warning(f"set cache failed {err}")

        while True:
            await asyncio.sleep(5)
            await check_cache()
            await set_cache()

    async def get_order_status_direct(self,order):
        _xchg_name,_pair,_market = order.internal_symbol.split(".")
        api = self._check_api_(_xchg_name)
        try:
            self.logger.warning(f"{_pair} get order {order.xchg_id} from exchange http api")
            _order = await api.order_match_result(_pair,order.xchg_id, extra_opts = {"market":_market})
            
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if time.time()*1e3 - order.created_ts_ms > (order.raw_data["stop_ts"] + 1)*1e3 and order.raw_data["cancel"]>0:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].raw_data["query"] += 1
                    if order_new.is_finished:
                        await self.on_order(oid, order_new)
                    elif self.order_cache[oid].raw_data["query"]>10:
                        self.order_cache.pop(oid)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def get_stats(self):
        while True:
            await asyncio.sleep(0.001)
            if self.ob_update:
                self.bp_diff.append(float(self.bp_gate-self.ap_binance))
                self.ap_diff.append(float(self.bp_binance-self.ap_gate))
                self.mp_diff.append(float(self.mp_gate-self.mp_binance))
                self.ob_update = False
            self.ap_binance_cache = []
            self.bp_binance_cache = []
    
    async def action_restriction(self):
        while True:
            await asyncio.sleep(1)
            self.action_count = 0

    async def take_profit(self):
        while True:
            await self._orders_waiting_to_tf.get()
            try:
                if self.pos >= self.params["base_amount"]:
                    tf = Decimal(0.003) - (self.pos / self.params["base_amount"] / MAX_AMOUNT) *Decimal(0.001)
                    if tf < Decimal(0.001):
                        tf = Decimal(0.001)
                    if self.bp_gate >= self.pos_price * (Decimal(1)+tf):
                        await self.close_position([self.symbol_gate, self.bp_gate, self.pos, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.ImmediateOrCancel])
                        self.logger.warning("take profit")
                        await asyncio.sleep(1)
                        self.direct_check_position()
                elif self.pos <= -self.params["base_amount"]:
                    tf = Decimal(0.003) - (-self.pos / self.params["base_amount"] / MAX_AMOUNT) *Decimal(0.001)
                    if tf < Decimal(0.001):
                        tf = Decimal(0.001)
                    if self.ap_gate <= self.pos_price * (Decimal(1)-tf):
                        await self.close_position([self.symbol_gate, self.ap_gate, -self.pos, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.ImmediateOrCancel])
                        self.logger.warning("take profit")
                        await asyncio.sleep(1)
                        self.direct_check_position()
            except Exception as err:
                self.logger.error(f"take profit error: {err}")

    async def strategy_core(self):
        await asyncio.sleep(15)
        await asyncio.gather(
            self.send_order_action(),
            self.update_redis_cache(),
            self.update_cancel_order(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.get_stats(),
            self.action_restriction(),
            self.take_profit(),
            self.direct_check_position()
        )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()