import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy

"""
class Order(object):
    internal_symbol = attr.ib()
    xchg_name = attr.ib()
    account_name = attr.ib()
    xchg_id = attr.ib(converter=str)
    xchg_symbol = attr.ib()

    side = attr.ib(validator=[attr.validators.instance_of(OrderSide)])
    type = attr.ib(validator=[attr.validators.instance_of(OrderType)])
    xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])

    time_in_force = attr.ib(validator=[attr.validators.instance_of(OrderTimeInForce)])
    requested_price = attr.ib(converter=num_to_decimal)
    requested_amount = attr.ib(converter=num_to_decimal)
    filled_amount = attr.ib(converter=num_to_decimal)
    avg_filled_price = attr.ib(converter=num_to_decimal)
    commission_fee = attr.ib(converter=num_to_decimal)

    is_finished = attr.ib()

    # Timestamp related
    local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
    server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message

    # Event type of this order object, will be used to trace user and xchg event
    evt_type = attr.ib(validator=[attr.validators.instance_of(OrderEventType)], default=OrderEventType.XchgUpdate)

    created_ts_ms = attr.ib(default=0, converter=int)  # Order created ts
    finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts

    # Update historical records
    update_historical_records = attr.ib(factory=list)

    # Trade records
    trade_records = attr.ib(factory=list)

    # Position side
    position_side = attr.ib(validator=[attr.validators.instance_of(OrderPositionSide)], default=OrderPositionSide.Open)
    strategy_name = attr.ib(default="UNSET")
    tag = attr.ib(default="PY-ATOM")
    extra_info = attr.ib(default="")

    raw_data = attr.ib(default=None)
"""

class MpOutput:
    def __init__(self, base_line):
        self.base_std = base_line
        self.mp_queue = collections.deque(maxlen=200)
        self.qt_queue = collections.deque(maxlen=10000)

        self.pre_max_list = collections.deque(maxlen=200)
        self.pre_min_list = collections.deque(maxlen=200)

    def feed_mp(self, mp):
        self.mp_queue.append(float(mp))
        self.update_base_line()

    def get_x_cur_duration(self):
        if len(self.pre_min_list) == 200:
            mp_mean = np.mean(list(self.mp_queue)[-50:])
            if mp_mean > self.pre_max_list[0]:
                return 1
            if mp_mean < self.pre_min_list[0]:
                return -1
        return 0

    def feed_max_logic(self):
        p_max = np.max(list(self.mp_queue))
        p_min = np.min(list(self.mp_queue))
        self.pre_max_list.append(p_max)
        self.pre_min_list.append(p_min)

    def update_base_line(self):
        if len(self.mp_queue) == 200:
            cur_std = self.get_cur_std()
            self.qt_queue.append(cur_std)
            if len(self.qt_queue) == 10000:
                cur_82_std = np.quantile(list(self.qt_queue), 0.82)
                self.base_std = cur_82_std
                self.qt_queue.clear()
            self.feed_max_logic()

    def get_cur_std(self):
        return np.std(list(self.mp_queue)[-100:])

    def mp_bad(self):
        return self.get_cur_std() > self.base_std

    def get_mean_mp(self):
        return np.mean(list(self.mp_queue)[-50:])

    def get_cur_duration(self):
        return self.mp_queue[-1] - self.get_mean_mp()

class AdjustMovement:
    def __init__(self, mt_line):
        self.buy_ban_mem_ts = None
        self.sell_ban_mem_ts = None
        self.mt_line = mt_line

        self.mp_cache = collections.deque(maxlen=5)
        self.mp_dif = collections.deque(maxlen=5)

        self.sp_update_mem = collections.deque(maxlen=10000)

    def feed_mp(self, cur_mp, cur_dt_ts):
        if len(self.mp_cache) == 5:
            mp_mean = np.mean(self.mp_cache)
            self.mp_dif.append(mp_mean)
        self.mp_cache.append(cur_mp)
        self.count_ban_signal(cur_dt_ts)

    def update_dif(self):
        if len(self.sp_update_mem) == 10000:
            new_line = np.quantile(list(self.sp_update_mem), 0.98)
            # print(f"update mmt new line: {self.mt_line} -> {new_line}")
            self.mt_line = new_line
            self.sp_update_mem.clear()

    def count_ban_signal(self, dt_ts):
        if len(self.mp_dif) == 5:
            cur_dif = self.mp_dif[-1] / self.mp_dif[0] - 1
            self.sp_update_mem.append(float(abs(cur_dif)))
            if cur_dif > self.mt_line:  # 动量阈值的影响非常大
                self.buy_ban_mem_ts = dt_ts

            if cur_dif < -1 * self.mt_line:
                self.sell_ban_mem_ts = dt_ts

            if self.buy_ban_mem_ts is not None:
                if dt_ts - self.buy_ban_mem_ts > 15e3:  # 时间的影响不大
                    self.buy_ban_mem_ts = None

            if self.sell_ban_mem_ts is not None:
                if dt_ts - self.sell_ban_mem_ts > 15e3:
                    self.sell_ban_mem_ts = None

            self.update_dif()


class MyStrategy(CommonStrategy):
    """
    {
        "mt_line" : 0.0026075617120832725
        "std_line": 0.0006243116909091545
        "base_amount":20
        
    }
    """

    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # init params
        # use self.params
        self.trade_cache = list()
        self.missing_order_cache = dict()
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.long_pos = Decimal(0)
        self.short_pos = Decimal(0)
        self.movement_adjust = AdjustMovement(mt_line=0.002)
        self.mp_std_handle = MpOutput(base_line=0.0006)
        self.mp = None
        self.trade_buy = 0
        self.trade_sell = 0

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(v)
                if type(v) == dict:
                    _dict[k] = float2decimal(v)
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        self.movement_adjust = AdjustMovement(mt_line=self.params["mt_line"])
        self.mp_std_handle = MpOutput(base_line=float(self.params["std_line"]))
        self.symbol_1 = self.config['strategy']['symbol_1']

        self._params ={"up_c_coef":0.00040882205393068895,
        "up_c_int":-0.004074283828223228,
        "up_f_coef":0.00019419704389341273,
        "up_f_int":-0.0019405169575420652,
        "down_c_coef": 0.00039801079701286176,
        "down_c_int": -0.003953587443435914,
        "down_f_coef": 0.00018344274434608588,
        "down_f_int": -0.0018079680849657906
        }

    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        self.subscribe_orderbook(self._symbol_config_.keys())
        self.subscribe_public_trade(self._symbol_config_.keys())
        self.direct_subscribe_order_update('binance',symbols=[self.config['strategy']['symbol_1']])
        # await self.redis_set_cache("dict")
        # await self.redis_get_cache()

    async def on_orderbook(self, symbol, orderbook):
        """
        price, volume
        orderbook: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        self.mp = (orderbook["asks"][0][0] + orderbook["bids"][0][0])/Decimal(2)
        self.movement_adjust.feed_mp(cur_mp=float(self.mp), cur_dt_ts=time.time()*1e3)
        self.mp_std_handle.feed_mp(mp=self.mp)

    async def on_public_trade(self, symbol, trade: TradeData):
        """
        class TradeData(object):
            # Internal symbol
            symbol = attr.ib()
            # Source exchange name
            source = attr.ib()
            price = attr.ib(converter=num_to_decimal)
            quantity = attr.ib(converter=num_to_decimal)
            side = attr.ib(validator=[attr.validators.instance_of(TradeSide)])
            # Timestamp related
            req_ts = attr.ib(converter=int)
            server_ts = attr.ib(converter=int)

            trade_id = attr.ib(default='', converter=str)
            buy_order_id = attr.ib(default='', converter=str)
            sell_order_id = attr.ib(default='', converter=str)
            resp_ts = attr.ib(factory=time_ms_now, converter=int)
            transaction_ts = attr.ib(factory=time_ms_now, converter=int)
        """
        self.trade_cache.append({"ts":trade.server_ts, "q":trade.quantity, "s":trade.side})
        while self.trade_cache[0]['ts'] < trade.server_ts - 10e3:
            self.trade_cache.pop(0)
        # for idx in range(len(self.trade_cache) - 1):
        #     if self.trade_cache[idx]['ts'] < trade.server_ts - 10e3:
        #         self.trade_cache.pop(idx)
        #     else:
        #         break
        if self.trade_cache[-1]["ts"] < trade.server_ts - 30e3:
            raise RuntimeError("trade flow missing")
            
        self.trade_buy = abs(sum([item["q"] for item in self.trade_cache if item["s"]==TradeSide.Buy]))
        self.trade_sell = abs(sum([item["q"] for item in self.trade_cache if item["s"]==TradeSide.Sell]))


    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[]
        if (self.order_cache[xchg_id][0].side == OrderSide.Buy and self.order_cache[xchg_id][0].position_side == OrderPositionSide.Open) or \
                (self.order_cache[xchg_id][0].side == OrderSide.Sell and self.order_cache[xchg_id][0].position_side == OrderPositionSide.Close):
            self.long_pos += Decimal((self.order_cache[xchg_id][0].side == OrderSide.Buy)*2 -1) * p_order.filled_amount
        else:
            self.short_pos += Decimal((self.order_cache[xchg_id][0].side == OrderSide.Sell)*2 -1) * p_order.filled_amount
        self.pos = self.long_pos - self.short_pos

    async def on_order(self, xchg_id, order: PartialOrder):
        """
        class PartialOrder(object):
            # used to update redis order
            xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])
            filled_amount = attr.ib(converter=num_to_decimal)
            avg_filled_price = attr.ib(converter=num_to_decimal)
            commission_fee = attr.ib(converter=num_to_decimal)
            is_finished = attr.ib()
            # Timestamp related
            local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
            server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message
            finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts
        """
        # TODO
        # only update order cahce here.
        # if missed, trigger on_order in another place.
        #  
        self.logger.info(f"on_order: {xchg_id} - {order}")
        if xchg_id in self.order_cache:
            self.handle_pos(xchg_id,order)
            if abs(self.pos) > self.params["base_amount"] * Decimal(100):
                raise RuntimeError("reach max position limit")

        # for order_id, missing_order in copy.deepcopy(self.missing_order_cache).items():
        #     if missing_order.is_finished:
        #         self.handle_pos(order_id,missing_order)
        #         if abs(self.pos) > self.params["base_amount"] * Decimal(100):
        #             raise RuntimeError("reach max position limit")
        #         self.order_cache.pop(order_id)
        #         self.missing_order_cache.pop(order_id)
                
    async def strategy_core(self):
        await asyncio.sleep(30)

        await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
        )


    def get_place_amount(self,net_loc,base_amount):
        cur_risk = abs(self.pos)
        v_cum = 0
        i_net = 0
        while True:
            i_net += 1
            cur_net_volume = sum([i_net*base_amount + i*base_amount for i in range(4)])
            v_cum += cur_net_volume
            if v_cum > cur_risk:
                break
        if i_net >= 2:
            i_net = 2
        return [i_net*base_amount + i*base_amount for i in range(4)][net_loc-1]
        
    def generate_entry_orders(self):
        if not self.trade_buy  or not self.trade_sell or not self.mp:
            return []
        entry_orders = []
        mp = self.mp
        
        ask_ceiling = Decimal(round(self._params["up_c_coef"]*np.log(float(self.trade_buy)) + self._params["up_c_int"],4))
        ask_floor = Decimal(round(self._params["up_f_coef"]*np.log(float(self.trade_buy)) + self._params["up_f_int"],4))
        
        bid_ceiling = Decimal(round(self._params["down_c_coef"]*np.log(float(self.trade_sell)) + self._params["down_c_int"],4))
        bid_floor = Decimal(round(self._params["down_f_coef"]*np.log(float(self.trade_sell)) + self._params["down_f_int"],4))

        sell_spread_tick = round((ask_ceiling - ask_floor) / 3, 5)
        buy_spread_tick = round((bid_ceiling - bid_floor) / 3, 5)

        # print(ask_ceiling,ask_floor,bid_ceiling,bid_floor)

        if sell_spread_tick < 0:
            sell_spread_tick = Decimal(0)
        if buy_spread_tick < 0:
            buy_spread_tick = Decimal(0)

        for i in range(1, 5):
            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor

            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))

                elif cd < 0:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            s_amount = self.get_place_amount(net_loc=i, base_amount=self.params["base_amount"])
            b_amount = self.get_place_amount(net_loc=i, base_amount=self.params["base_amount"])

            b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,1]
            s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,1]

            if self.long_pos > self.params["base_amount"]*Decimal(i*30):
                s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,1]

            if self.short_pos > self.params["base_amount"]*Decimal(i*30):
                b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,1]
            
            
            
            entry_orders.append(b_io)
            entry_orders.append(s_io)

        return entry_orders

    def generate_balance_orders(self):
        cur_risk = self.pos
        if not self.trade_buy  or not self.trade_sell or not self.mp:
            return []

        balance_orders = []
        
        mp = self.mp
        total_amount = Decimal(abs(cur_risk))

        ask_ceiling = Decimal(round(self._params["up_c_coef"]*np.log(float(self.trade_buy)) + self._params["up_c_int"],4))
        ask_floor = Decimal(round(self._params["up_f_coef"]*np.log(float(self.trade_buy)) + self._params["up_f_int"],4))
        
        bid_ceiling = Decimal(round(self._params["down_c_coef"]*np.log(float(self.trade_sell)) + self._params["down_c_int"],4))
        bid_floor = Decimal(round(self._params["down_f_coef"]*np.log(float(self.trade_sell)) + self._params["down_f_int"],4))

        sell_spread_tick = round((ask_ceiling - ask_floor) / 3, 5)
        buy_spread_tick = round((bid_ceiling - bid_floor) / 3, 5)

        # print(ask_ceiling,ask_floor,bid_ceiling,bid_floor)

        if sell_spread_tick < 0:
            sell_spread_tick = Decimal(0)
        if buy_spread_tick < 0:
            buy_spread_tick = Decimal(0)

        for i in range(4, 0, -1):

            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor
        
            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))
                else:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            ex_amount = self.get_place_amount(net_loc=5-i,base_amount=self.params["base_amount"])
            if cur_risk > 0:

                if self.movement_adjust.sell_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                #
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, sell_p, ex_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, sell_p, total_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
            else:
                if self.movement_adjust.buy_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, buy_p, ex_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, buy_p, total_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
        return balance_orders

    def order_generate_logic(self):
        if self.trade_buy and self.trade_sell:
            req_orders = []

            entry_orders = self.generate_entry_orders()
            req_orders.extend(entry_orders)

            balance_orders = self.generate_balance_orders()
            req_orders.extend(balance_orders)

            filter_orders = self.request_orders_filter(req_orders)
            return filter_orders
        return []

    def request_orders_filter(self,req_orders):
        filter_orders = []
        buy_source = [self.order_cache[key][1]["source"] for key in list(self.order_cache) if self.order_cache[key][0].side == OrderSide.Buy]
        sell_source = [self.order_cache[key][1]["source"] for key in list(self.order_cache) if self.order_cache[key][0].side == OrderSide.Sell]

        for per_req in req_orders:
            if per_req[3] == OrderSide.Buy:
                x_source = np.array(buy_source)
            else:
                x_source = np.array(sell_source)
            x_source = np.array(x_source)
            cur_pending_len = sum((x_source == per_req[7]) * 1)
            if cur_pending_len < 1:
                filter_orders.append(per_req)
        return filter_orders

    async def send_order_action(self):
        # only create new order cache here.

        while True:
            await asyncio.sleep(0.5)
            
            if self.trade_cache[-1]["ts"] < int(time.time()*1e3) - 30e3:
                self.logger.info("trade flow missing")
            orders = self.order_generate_logic()
            
            try:
                for i in range(1,5):
                    for order_info in orders:
                        if order_info[6] == i:
                            order = await self.make_future_order(order_info[0],order_info[1],order_info[2],order_info[3],order_info[4],order_info[5])
                            extra_info = dict()
                            extra_info["stop_ts"] = order_info[6]
                            extra_info["source"] =  order_info[7] # entry or balance
                            self.order_cache[order.xchg_id] =[order,extra_info]
            except:
                pass

    async def cancel_order_action(self):
        
        async def batch_cancel(oid, order_extra):

            if time.time()*1e3 - order_extra[0].created_ts_ms > order_extra[1]["stop_ts"] * 1e3:
                try:
                    await self.cancel_order(order_extra[0])
                except Exception as err:
                    raise RuntimeError(f'cancel order {oid} err {err}')

        while True:
            await asyncio.sleep(0.2)
            await asyncio.gather(
                *[batch_cancel(oid, order_extra) for oid, order_extra in self.order_cache.items()]
            )

            try:
                for order_id in list(self.missing_order_cache):
                    missing_order = self.missing_order_cache[order_id]
                    if missing_order.is_finished:
                        self.handle_pos(order_id,missing_order)
                        if abs(self.pos) > self.params["base_amount"] * Decimal(100):
                            raise RuntimeError("reach max position limit")
                        self.order_cache.pop(order_id)
                        self.missing_order_cache.pop(order_id)
            except:
                pass
            
            try:
                for order_id in list(self.order_cache):
                    order_extra = self.order_cache[order_id]
                    if time.time()*1e3 - order_extra[0].created_ts_ms > order_extra[1]["stop_ts"] * 1e3 + 400:
                        self.order_cache.pop(order_id)
            except:
                pass
            # try:
            #     for order_id,order in copy.deepcopy(self.order_cache).items():
            #         if time.time()*1e3 - order.created_ts_ms >  order.raw_data*1e3:
            #             await self.cancel_order(order)
            # except:
            #     raise RuntimeError("order cancel error")


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
