# import os
# os.environ["ATOM_PRINT_RESPONSE"] = '1'

from strategy_base.base import CommonStrategy
import time
import asyncio
from asyncio.queues import Queue, LifoQueue
import traceback

from atom.model import OrderSide, PartialOrder, OrderStatus, BBODepth, OrderPositionSide, OrderType
from atom.model.depth import Depth as AtomDepth
from atom.model.order import Order as AtomOrder
from atom.exceptions import OrderNotFoundError
from typing import Set, AnyStr, Union, Dict, List, Tuple
from decimal import Decimal
import numpy as np

from collections import deque, defaultdict

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.send = False
        
        self.contract = self.config['strategy']['params']['contract']
        
        self.mp = dict()
        
        self.orderbook_queue = LifoQueue()
        
        self.symbol_config_by_exchange_pair = dict()
        self.all_pair = set()
        
    async def update_max_order_volume(self):
        while True:
            await asyncio.sleep(3600)
            try:
                
                symbol = f"lbank.{self.contract}.swap.na"
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                symbol_cfg["max_order_volume"] = await self.get_max_order_volume(exchange_pair=symbol_cfg["exchange_pair"])
            except:
                pass
    
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        all_config = self.all_symbol_config()

        self.direct_subscribe_orderbook(symbol_name=f"binance.{self.contract}.usdt_contract.na", is_incr_depth=True)
        
        symbol = f"lbank.{self.contract}.swap.na"
        all_config[symbol]["max_order_volume"] = await self.get_max_order_volume(exchange_pair=all_config[symbol]["exchange_pair"])
        
        self.symbol_config_by_exchange_pair[all_config[symbol]["exchange_pair"]] = all_config[symbol]
        self.all_pair.add(all_config[symbol]["exchange_pair"])
        
        self.loop.create_task(self.handle_ws_update())
        
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)

            symbol_cfg = self.get_symbol_config(symbol_identity=f"lbank.{self.contract}.swap.na")
            try:
                await api.flash_cancel_orders(symbol_cfg)
            except:
                self.logger.info("no open orders")
        
        # self.send = True
                    
    async def get_max_order_volume(self, exchange_pair):
        api = self.get_exchange_api(exchange_name="lbank")[0]
        response = await api.make_request("swap", "GET", "/cfd/openApi/v1/pub/instrument", query={"productGroup":"SwapU"}, need_sign=False)
        config_list = response.json["data"]
        for config in config_list:
            if config["symbol"] == exchange_pair:
                self.logger.info(config)
                return Decimal(config["maxOrderVolume"])
            
    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            await self.orderbook_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")
        
    def handle_orderbook(self, symbol: str, orderbook: Dict):
        try:
            ex, contract = symbol.split(".")
            ap = orderbook["asks"][0][0]
            bp = orderbook["bids"][0][0]
            
            curr_mp = (ap + bp) / 2
            self.mp[contract] = curr_mp
        except:
            traceback.print_exc()
    
    async def handle_ws_update(self):
        while True:
            try:
                if self.orderbook_queue.empty():
                    await asyncio.sleep(0.001)
                else:
                    symbol, orderbook = await self.orderbook_queue.get()
                    self.orderbook_queue = LifoQueue()
                    self.handle_orderbook(symbol=symbol, orderbook=orderbook)

                await asyncio.sleep(0.001)
            except:
                await asyncio.sleep(0.001)
                traceback.print_exc()
 
    async def clear_position(self):
        while True:
            await asyncio.sleep(1)
            for acc in self.account_names:
                if acc.startswith("binance"):
                    continue
                api = self.get_exchange_api_by_account(acc)
                cur_position = (await api.contract_position_all(market="swap", sub_market="na"))
                # self.logger.info(f"position info:{cur_position}") 
                for contract in cur_position.data:
                    price = self.mp.get(contract)
                    if not price:
                        continue
                    self.logger.critical(f"{contract} position:{cur_position.data[contract]}")
                    pos = cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"]
                    if pos == Decimal(0):
                        continue
                    if pos > 0:
                        price = price * Decimal(1 - 0.001)
                        side = OrderSide.Sell
                    else:
                        price = price * Decimal(1 + 0.001)
                        side = OrderSide.Buy
                    
                    self.send = False
                    symbol_cfg = self.get_symbol_config(symbol_identity=f"lbank.{contract}.swap.na")
                    try:
                        await api.flash_cancel_orders(symbol_cfg)
                    except:
                        self.logger.critical("no open orders")
                    # self.send = True
                    
                    # volume = abs(pos)
                    # max_order_vol = symbol_cfg.get("max_order_volume")
                    # if max_order_vol:
                    #     volume = min(max_order_vol, volume)

                    if abs(pos) > Decimal("10000") / price:
                        volume = Decimal("10000") / price
                    else:
                        volume = abs(pos)
                    try:
                        order: AtomOrder = await self.make_order(
                            symbol_name=f"lbank.{contract}.swap.na",
                            price=price,
                            volume=volume,
                            side=side,
                            position_side=OrderPositionSide.Close,
                            order_type=OrderType.IOC,
                        )
                    except Exception as err:
                        self.logger.critical(f"close position err for {contract}, {err}")
            
    async def strategy_core(self):
        await asyncio.gather(
            self.clear_position(),
            self.update_max_order_volume(),
        )
        
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    
                    
                