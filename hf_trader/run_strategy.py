import asyncio
from decimal import Decimal
from asyncio.queues import Queue
import ujson as json

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
    async def before_strategy_start(self):
        ch1 = "mq:hf_signal:order_request"
        ch2 = "mq:hf_signal:cancel_request"
        self.loop.create_task(self.subscribe_to_channel([ch1],self.process_order_request))
        self.loop.create_task(self.subscribe_to_channel([ch2],self.process_cancel_request))

    
    async def trader_cancel_order(self, symbol_id, account_name, client_id, main_sn,catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        symbol = self.get_symbol_config(symbol_id)
        api = self.get_exchange_api_by_account(account_name)
        try:
            r = await api.cancel_order(symbol=symbol, order_id=None, client_id=client_id)
            self.logger.info(f"[cancel order]: {client_id} {r.data}")
            await self.publish_to_channel(
                    "mq:hf_signal:cancel_response", 
                    {
                        "res":r.data,
                        "client_id":client_id,
                        "main_sn":main_sn
                        }
                    )
        except Exception as err:
            if not catch_error:
                raise
            if type(err) in (OrderAlreadyCompletedError, OrderNotFoundError):
                self.logger.error(f"[cancel order error] [{err}] {client_id}, redirect True")
                await self.publish_to_channel(
                    "mq:hf_signal:cancel_response", 
                    {
                        "res":True,
                        "client_id":client_id,
                        "main_sn":main_sn
                        }
                    )
            elif type(err) == ExchangeTemporaryError:
                self.logger.error(f"[cancel order error] [{err}] {client_id}, redirect False")
                await self.publish_to_channel(
                    "mq:hf_signal:cancel_response", 
                    {
                        "res":False,
                        "client_id":client_id,
                        "main_sn":main_sn
                        }
                    )
            elif isinstance(err, ExchangeConnectorException):
                self.logger.error(f"[cancel order error] {err} {client_id}, unknown api error, redirect False")
                await self.publish_to_channel(
                    "mq:hf_signal:cancel_response", 
                    {
                        "res":False,
                        "client_id":client_id,
                        "main_sn":main_sn
                        }
                    )
            else:
                self.logger.error(f"[cancel order error] {err} {client_id}, redirect False")
                await self.publish_to_channel(
                    "mq:hf_signal:cancel_response", 
                    {
                        "res":False,
                        "client_id":client_id,
                        "main_sn":main_sn
                        }
                    )
        
    async def process_order_request(self, ch_name, request):
        payload = json.loads(request)
        if payload["strategy_name"] == self.strategy_name:
            if payload["order_type"] == "Limit":
                order_type = OrderType.Limit
            elif payload["order_type"] == "IOC":
                order_type = OrderType.IOC
            elif payload["order_type"] == "PostOnly":
                order_type = OrderType.PostOnly
            self.loop.create_task(
                self.send_order(
                    symbol=payload["symbol"],
                    price=Decimal(payload["price"]),
                    qty=Decimal(payload["qty"]),
                    side=OrderSide.Buy if payload["side"] == 1 else OrderSide.Sell,
                    client_id=payload["client_id"],
                    account_name=payload["acc"],
                    order_type=order_type,
                    main_sn=payload.get("main_sn")
                    )
                )
    
    async def process_cancel_request(self, ch_name, request):
        payload = json.loads(request)
        if payload["strategy_name"] == self.strategy_name:
            self.loop.create_task(
                self.trader_cancel_order(
                    symbol_id=payload["symbol_id"],
                    account_name=payload["account_name"],
                    client_id=payload["client_id"],
                    main_sn=payload.get("main_sn")
                )
            )
    
    async def send_order(self, symbol, price, qty, side, client_id, main_sn, account_name=None, order_type=OrderType.Limit, recvWindow=3000):
        try:
            self._account_config_[account_name]["cfg_pos_mode"][symbol] = 1
            # qty += self.half_step_tick
            # price += self.half_price_tick
            res = await self.raw_make_order(
                symbol_name=symbol,
                price=price,
                volume=qty,
                side=side,
                order_type=order_type,
                client_id=client_id,
                account_name=account_name,
                position_side=OrderPositionSide.Open,
                recvWindow=recvWindow,
            )
            _, _, market, _ = symbol.split(".")
            if market == "spot":
                await self.publish_to_channel(
                    "mq:hf_signal:order_response", 
                    {
                        "uid10s":int(res.headers["x-mbx-order-count-10s"]), 
                        "uid1d":int(res.headers["x-mbx-order-count-1d"]), 
                        "ip1m":int(res.headers["x-mbx-used-weight-1m"]), 
                        "account_name":account_name,
                        "startegy_name":self.strategy_name,
                        "client_id":client_id,
                        "err":None,
                        "main_sn":main_sn
                        }
                    )
            elif market == "margin":
                await self.publish_to_channel(
                    "mq:hf_signal:order_response", 
                    {
                        "ip1m":int(res.headers["X-SAPI-USED-IP-WEIGHT-1M"]), 
                        "uid1m":int(res.headers["X-SAPI-USED-UID-WEIGHT-1M"]), 
                        "account_name":account_name,
                        "startegy_name":self.strategy_name,
                        "client_id":client_id,
                        "err":None,
                        "main_sn":main_sn
                        }
                    )
            elif market == "usdt_contract":
                await self.publish_to_channel(
                    "mq:hf_signal:order_response", 
                    {
                        "ip1m":int(res.headers["X-MBX-USED-WEIGHT-1M"]), 
                        "uid10s":int(res.headers["X-MBX-ORDER-COUNT-10S"]), 
                        "uid1m":int(res.headers["X-MBX-ORDER-COUNT-1M"]), 
                        "account_name":account_name,
                        "startegy_name":self.strategy_name,
                        "client_id":client_id,
                        "err":None,
                        "main_sn":main_sn
                        }
                    )
                
        except Exception as err:
            self.logger.critical(f"{err},side={side}, p={price}, q={qty}, account_name={account_name}")
            if str(err) == "Expected object or value":
                raise
            else:
                await self.publish_to_channel(
                    "mq:hf_signal:order_response", 
                    {
                        "uid10s":None, 
                        "uid1d ":None, 
                        "ip1m":None, 
                        "uid1m":None, 
                        "account_name":account_name,
                        "startegy_name":self.strategy_name,
                        "client_id":client_id,
                        "err":f"{err}",
                        "main_sn":main_sn
                        }
                    )
                
    async def strategy_core(self):
        await asyncio.sleep(10)
        while True:
            await asyncio.sleep(10)
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()