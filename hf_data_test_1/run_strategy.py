import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue

from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

# base on s32_1, automatic change base amount,delete some params

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_usdt_swap.usdt_contract.na"
        self._f = open(f"./data/{self.extract_data_symbol(self.symbol)}_{int(time.time())}_trade_indirect.csv", "w")
        
        
    async def before_strategy_start(self):
        self.subscribe_public_trade(symbol_list=self.symbol)
        self.logger.setLevel("WARNING")
        
    async def on_public_trade(self, symbol, trade: PublicTrade):
        msg = f"{trade.price},{trade.quantity},{trade.transaction_ms},{trade.server_ms},{trade.local_ms},{time.time()*1e3}\n"
        self._f.write(msg)
        self._f.flush()
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(30)
    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    
    