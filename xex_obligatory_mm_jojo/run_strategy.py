from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2
import logging
import asyncio
from decimal import Decimal
from inspect import trace
import time
import pandas as pd
from asyncio.queues import Queue
import os
import numpy as np
import traceback
import math
from typing import Set, AnyStr, Union, Dict, List
import ujson as json
from orderedset import OrderedSet
import uuid
from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from strategy_base.base import CommonStrategy

from xex_mm.utils.base import Depth, BBO, Direction, Order, TransOrder, OrderManager, MakerTaker
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig, HeartBeatTolerance
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_inventory_executor import XexMmDeltaNeutralInventoryExecutor
from xex_mm.strategy_configs.obligatory_mm_config import ObligatoryMmConfig

from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard

qty_precision = {
    'binance.btc_usdt': 5,
    'binance.btc_usdt_swap': 3,
    'binance.btc_busd_swap': 3,
    'binance.eth_usdt_swap': 3,
    'binance.eth_busd_swap': 3,
    'binance.axs_usdt_swap': 0,
    'binance.doge_usdt_swap': 1,
    'binance.1000lunc_busd_swap': 0,
    'binance.luna2_busd_swap': 0,
    'binance.luna2_usdt_swap': 0,
    'binance.etc_usdt_swap': 2,
    'binance.sol_usdt_swap': 0,
    'binance.reef_usdt_swap': 0,
    'binance.ape_usdt_swap':0,
    'binance.ape_usdt':2,
    'binance.chz_usdt_swap':0,
    'binance.chz_usdt':0,
    'binance.hnt_usdt_swap':0,

    'mexc.btc_usdt': 6,
    'mexc.eth_usdt_swap': 2,
    'mexc.axs_usdt_swap': 0,

    "gateio.btc_usdt_swap": 4,
    "gateio.btc_usdt": 6,

    "okex.btc_usdt": 8,
    "okex.btc_usdt_swap": 3,
    "okex.ape_usdt_swap": 3,
    "okex.chz_usdt": 1,
    "okex.chz_usdt_swap": 1,

    "woo.btc_usdt_swap": 3,

    "crypto.btc_usdt_swap": 6,
    
    "jojo.btc_usdt_swap": 6,
    "jojo.eth_usdt_swap": 6,
    "jojo18.btc_usdt_swap": 6,
    "jojo18.eth_usdt_swap": 6
}

price_precision = {
    'binance.btc_usdt': 2,
    'binance.btc_usdt_swap': 1,
    'binance.btc_busd_swap': 1,
    'binance.eth_usdt_swap': 2,
    'binance.eth_busd_swap': 2,
    'binance.axs_usdt_swap': 2,
    'binance.doge_usdt_swap': 5,
    'binance.1000lunc_busd_swap': 4,
    'binance.luna2_busd_swap': 4,
    'binance.luna2_usdt_swap': 4,
    'binance.etc_usdt_swap': 3,
    'binance.sol_usdt_swap': 2,
    'binance.reef_usdt_swap': 6,
    'binance.ape_usdt_swap':3,
    'binance.ape_usdt':3,
    'binance.chz_usdt_swap':5,
    'binance.chz_usdt':4,
    'binance.hnt_usdt_swap':3,

    'mexc.btc_usdt': 2,
    'mexc.eth_usdt_swap': 2,
    'mexc.axs_usdt_swap': 5,

    "gateio.btc_usdt_swap": 1,
    "gateio.btc_usdt": 2,

    "okex.btc_usdt": 1,
    "okex.btc_usdt_swap": 1,
    "okex.ape_usdt_swap": 4,
    "okex.chz_usdt": 5,
    "okex.chz_usdt_swap": 5,

    "woo.btc_usdt_swap": 1,

    "crypto.btc_usdt_swap": 2,
    
    "jojo.btc_usdt_swap": 2,
    "jojo.eth_usdt_swap": 2,
    "jojo18.btc_usdt_swap": 2,
    "jojo18.eth_usdt_swap": 2,
}
        
class ObligatoryExecutor(ObligatoryExecutor):
    def gen_quotes(self, ts: int) -> List[TransOrder]:
        self.on_wakeup(ts=ts)
        orders = []
        if not self._xex_depth.contains("binance"):
            return []
        ref_prc = self._ref_prc_calculator.calc_from_depth(depth=self._xex_depth.get_depth_for_ex("binance"), amt_bnd=0)
        for i, (ret, q_ccy2, _) in enumerate(self._get_requirements()):
            ask_prc = ref_prc * (1 + ret)
            bid_prc = ref_prc * (1 - ret)
            q_ccy1 = q_ccy2 / ask_prc
            # l_r = self._get_hit_anomaly_ratio(d=Direction.long, i=i, ts=ts)
            # s_r = self._get_hit_anomaly_ratio(d=Direction.short, i=i, ts=ts)
            # if math.isnan(l_r) or (l_r < 2):
            orders.append(TransOrder(
                side=Direction.short,
                p=ask_prc,
                q=q_ccy1,
                iid=self.iid,
                floor=i,
                delta=np.nan,
                life=1,
                maker_taker=MakerTaker.maker,
            ))
            # if math.isnan(s_r) or (s_r < 2):
            orders.append(TransOrder(
                side=Direction.long,
                p=bid_prc,
                q=q_ccy1,
                iid=self.iid,
                floor=i,
                delta=np.nan,
                life=1,
                maker_taker=MakerTaker.maker,
            ))
        return orders
    
    def remove_order(self, oid: str):
        if self._prev_order_map.get(oid) != None:
            self._prev_order_map.pop(oid)
            
class ClientIDGenerator(ClientIDGenerator):
    @classmethod
    def gen_jojo(cls, **kwargs):
        return uuid.uuid4().hex
    
    @classmethod
    def gen_jojo18(cls, **kwargs):
        return uuid.uuid4().hex
    
class FeeStructure:
    def __init__(self, maker: float, taker: float) -> None:
        self.maker = maker
        self.taker = taker

class FeeConfig:
    def __init__(self) -> None:
        self.config = {
            # BINANCE
            "binance.btc_usdt": FeeStructure(maker=0, taker=0),
            "binance.axs_usdt_swap":FeeStructure(maker=-0.00005, taker=0.0003),
            "binance.luna_usdt_swap":FeeStructure(maker=-0.00005, taker=0.0003),
            "binance.btc_usdt_swap":FeeStructure(maker=-0.00005, taker=0.0003),
            "binance.eth_usdt_swap":FeeStructure(maker=-0.00005, taker=0.0003),
            # MEXC
            "mexc.axs_usdt_swap":FeeStructure(maker=-0.0001, taker=0.0001),
            "mexc.luna_usdt_swap":FeeStructure(maker=-0.0001, taker=0.0001),
            "mexc.btc_usdt":FeeStructure(maker=-0.0001, taker=0.0005),
            # WOO
            "woo.btc_usdt_swap": FeeStructure(maker=0, taker=0),
            # GATEIO
            "gateio.btc_usdt_swap": FeeStructure(maker=-0.000125, taker=0.000225),  # VIP MM2
            # OKEX
            "okex.btc_usdt_swap": FeeStructure(maker=0, taker=0.0002),  # vip5 ???
            # CRYPTO, same for all token pairs
            "crypto.btc_usdt": FeeStructure(maker=-0.00003, taker=0.00025),
            "crypto.btc_usdt_swap": FeeStructure(maker=-0.00002, taker=0.00015),
            # JOJO
            "jojo.btc_usdt_swap": FeeStructure(maker=-0.0002, taker=0.0005),
            "jojo.eth_usdt_swap": FeeStructure(maker=-0.0002, taker=0.0005),
            "jojo18.btc_usdt_swap": FeeStructure(maker=-0.0002, taker=0.0005),
            "jojo18.eth_usdt_swap": FeeStructure(maker=-0.0002, taker=0.0005),

        }
    
    def get_fee(self, ex: str, pair: str):
        return self.config[f"{ex}.{pair}"].maker, self.config[f"{ex}.{pair}"].taker


def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.start_ts = time.time()
        self.oid_to_cid = dict()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.ex_maker = self.config['strategy']['params']['ex_maker']
        self.bnd_amt_in_coin = Decimal(self.config['strategy']['params']['bnd_amt_in_coin'])
        self.unknown_order_dict = dict()
        self.counter = 0
        self.filled_counter = 0
        
        self.cfg = ObligatoryMmConfig(
            version="",
            contract=self.contract,
            ex=self.ex_maker,
            hedge_exs=["binance"],
            profit_margin_return=-np.inf,
            cancel_tolerance=1e-5,
            requirements=[
                (0.003, 1e4, 1e-5),
                (0.01, 1e5-1e4, 1e-4),
                (0.04, 1e6-1e5-1e4, 1e-3),
                # (0.002, 2e4),
                # (0.004, 4e4),
                # (0.008, 8e4),
                # (0.016, 16e4),
                # (0.032, 32e4),
                # (0.064, 64e4),
            ],
        )

        self._obligatory_order_manager = OrderManager()
        self._hedge_order_manager = OrderManager()
        self._inventory_order_manager = OrderManager()
        self._rebalance_order_manager = OrderManager()
        self._xex_depth = XExDepth(contract=self.cfg.contract)
        
        self._heart_beat_tol_cfg = HeartBeatTolerance()
        self._last_update_time = {}
        self._state = {}
        
        self._numeric_tol = 1e-10
        
        self._obligatory_executor = self.build_obligatory_executor()
        self._delta_neutral_executor = XexMmDeltaNeutralExecutor(contract=self.cfg.contract,
                                                                 profit_margin_return=self.cfg.profit_margin_return)
        self._inventory_executor = XexMmDeltaNeutralInventoryExecutor(contract=self.cfg.contract)
        for ex in [self.cfg.ex] + self.cfg.hedge_exs:
            makers_fee, takers_fee = FeeConfig().get_fee(ex=ex, pair=self.cfg.contract)
            self._delta_neutral_executor.update_fee_rate(contract=self.cfg.contract, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)
            self._inventory_executor.update_fee_rate(contract=self.cfg.contract, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)
            
        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()

        self._hex_set = set(self.cfg.hedge_exs)
        self._inactive_hexs: Set = self._hex_set.copy()
        self._active_hexs: Set = set()
        self._inventory: float = 0
        self._obl_inventory: float = 0
        self._hedge_inventory: float = 0
        
        self._order_stats = dict()
        self._order_stats["buy_amt"] = 0
        self._order_stats["buy_qty"] = 0
        self._order_stats["sell_amt"] = 0
        self._order_stats["sell_qty"] = 0

        self.canceling_id = set()
        
        self.send = False
        
        self.trd_amt= 0
        
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        
    def is_oex(self, ex: str):
        return ex == self.cfg.ex

    def is_hex(self, ex: str):
        return ex in self._hex_set

    def activate_oex(self, depth: Depth):
        ex = depth.meta_data.ex
        assert self.is_oex(ex=ex)
        self._xex_depth.update_depth_for_ex(depth)
        self.logger.info(f"Activating obligatory ex={ex}")

    def deactivate_oex(self):
        ex = self.cfg.ex
        self._xex_depth.remove_ex(ex=ex)
        self.logger.info(f"Deactivating obligatory ex={ex}")

    def activate_hex(self, depth: Depth):
        ex = depth.meta_data.ex
        assert self.is_hex(ex=ex)
        self._xex_depth.update_depth_for_ex(depth)
        self._inactive_hexs.remove(ex)
        self._active_hexs.add(ex)
        self.logger.info(f"Activating hedge ex={ex}")

    def deactivate_hex(self, ex: str):
        assert ex in self._hex_set
        self._xex_depth.remove_ex(ex=ex)
        self._inactive_hexs.add(ex)
        self._active_hexs.remove(ex)
        self.logger.info(f"Deactivating hedge ex={ex}")

    def build_obligatory_executor(self):
        """ Override this for different exchange / contracts with different MM requirements. """

        oe = ObligatoryExecutor(ex=self.cfg.ex, contract=self.cfg.contract)
        oe.update_requirements(requirements=self.cfg.requirements)
        return oe
    
    def update_inventory(self, chg_qty: float, key: str, prc: float):
        self._inventory += chg_qty
        if key == "obl":
            self._obl_inventory += chg_qty
        elif key == "hedge":
            self._hedge_inventory += chg_qty
            
        self.trd_amt += abs(chg_qty) * prc
        
    def direction_map(self, side: Direction):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
    
    def cancel_obligatory_orders(self):
        for oid in self._obligatory_order_manager.keys():
            if self._obligatory_order_manager.get_trans_order(oid=oid):
                o = self._obligatory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    # if self.cancel_ts_dict.get(oid) != None:
                    #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                    self.loop.create_task(self.handle_cancel_order(oid, o))

    def cancel_all_hedge_orders(self):
        for oid in self._hedge_order_manager.keys():
            if self._hedge_order_manager.get_trans_order(oid=oid):
                o = self._hedge_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    # if self.cancel_ts_dict.get(oid) != None:
                    #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                    self.loop.create_task(self.handle_cancel_order(oid, o))

    def cancel_hedge_orders_for_ex(self, ex: str):
        iid = f"{ex}.{self.cfg.contract}"
        for oid in self._hedge_order_manager.get_oids_for_iid(iid=iid):
            o = self._hedge_order_manager.trans_order_at(oid=oid)
            if not o.cancel:
                # if self.cancel_ts_dict.get(oid) != None:
                #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                self.loop.create_task(self.handle_cancel_order(oid, o))

    def cancel_all_inventory_orders(self):
        for oid in self._inventory_order_manager.keys():
            if self._inventory_order_manager.get_trans_order(oid=oid):
                o = self._inventory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    # if self.cancel_ts_dict.get(oid) != None:
                    #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                    self.loop.create_task(self.handle_cancel_order(oid, o))
        
    def cancel_inventory_orders_for_ex(self, ex: str):
        iid = f"{ex}.{self.cfg.contract}"
        for oid in list(self._inventory_order_manager.get_oids_for_iid(iid=iid)):
            if self._inventory_order_manager.get_trans_order(oid=oid):
                o = self._inventory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    # if self.cancel_ts_dict.get(oid) != None:
                    #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                    self.loop.create_task(self.handle_cancel_order(oid, o))
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        maker_symbol = f"{self.cfg.ex}.{self.cfg.contract}.{self.market}.{self.sub_market}"
        if self.cfg.ex == "okex" and self.market == "usdt_contract":
            maker_symbol = f"{self.cfg.ex}.{self.cfg.contract}.swap.cross"
        if self.cfg.ex.startswith("jojo") and self.market == "usdt_contract":
            ccy1, _, _ = self.cfg.contract.split("_")
            maker_symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
        maker_config = self.get_symbol_config(symbol_identity=maker_symbol)
        self.maker_qty_tick = maker_config["qty_tick_size"]
        self.maker_contract_value = maker_config["contract_value"]
        bnd = self.bnd_amt_in_coin / maker_config["contract_value"]
        self.direct_subscribe_order_update(symbol_name=maker_symbol)
        self.direct_subscribe_orderbook(symbol_name=maker_symbol, is_incr_depth=True, depth_min_v=bnd, is_testnet=True)
        # self.direct_subscribe_bbo(symbol_name=maker_symbol)

        self.taker_config = dict()
        for ex in self.cfg.hedge_exs:
            symbol = f"{ex}.{self.cfg.contract}.{self.market}.{self.sub_market}"
            if ex == "okex" and self.market == "usdt_contract":
                symbol = f"{ex}.{self.cfg.contract}.swap.cross"
            if ex.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = self.cfg.contract.split("_")
                symbol = f"{ex}.{ccy1}_usdc_swap.swap.na"
            taker_config = self.get_symbol_config(symbol_identity=symbol)
            self.taker_config[ex] = taker_config
            bnd = self.bnd_amt_in_coin / taker_config["contract_value"]
            self.direct_subscribe_order_update(symbol_name=symbol)
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_bbo(symbol_name=symbol)
        
        counter = 0    
        while True:
            if counter > 1000:
                self.logger.critical("no orderbook comming")
                exit()
            counter += 1
            ref_prc = self._obligatory_executor._ref_prc
            if ref_prc is Ellipsis:
                await asyncio.sleep(1)
                continue
            else:
                await self.rebalance()
                break
        
        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        base_ccy2 = ccy[1]
        
        self.ccy2_amt = 0

        for acc in self.account_names:
            ex, _ = acc.split(".")
            api = self.get_exchange_api_by_account(acc)
            iid = f"{ex}.{self.cfg.contract}"
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            if iid.startswith("okex") and self.market == "usdt_contract":
                symbol = f"{iid}.swap.cross"
            if iid.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = self.cfg.contract.split("_")
                symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            cur_balance = (await api.account_balance(symbol_cfg)).data
            self.logger.warning(f"cur_balance={cur_balance}")
            if ex.startswith("jojo"):
                ccy2 = "usdc"
            else:
                ccy2 = base_ccy2
            self.ccy2_amt += cur_balance[ccy2]["all"]
        
            
        self.send = True
            
    async def on_order(self, order):
        try:
            s = time.time()
            oid = order.xchg_id

            if self.oid_to_cid.get(oid) != None:
                cid = self.oid_to_cid[oid]
                order.client_id = cid
                if order.filled_amount > 0:
                    self.logger.critical(f"obligatory contains: {self._obligatory_order_manager.contains_trans_order(oid=cid)}")
                    self.logger.critical(f"order filled: {order.__dict__}")
                    self.logger.critical(f"recieve message delay {int(time.time()*1e3-order.server_ms)}ms")
            else:
                self.unknown_order_dict[oid] = order
                return
                
            if not self._obligatory_order_manager.contains_trans_order(oid=cid) and \
                not self._inventory_order_manager.contains_trans_order(oid=cid) and \
                not self._hedge_order_manager.contains_trans_order(oid=cid) and \
                not self._rebalance_order_manager.contains_trans_order(oid=cid):
                return

            if self._rebalance_order_manager.contains_trans_order(oid=cid):
                order_obj: Order = Order.from_dict(
                    porder=order, 
                    torder=self._rebalance_order_manager.get_trans_order(oid=cid),
                    contract_value=self.maker_contract_value
                    )
                """ Just Clean It Up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                    if self.oid_to_cid.get(oid) != None:
                        self.oid_to_cid.pop(oid)
        
            if self._obligatory_order_manager.contains_trans_order(oid=cid):
                if order.filled_amount > 0:
                    self.logger.critical(f"oid={order.xchg_id}, filled_qty={order.filled_amount}, price={order.avg_filled_price}, status={order.xchg_status}")
                    self.filled_counter += 1
                    
                order_obj: Order = Order.from_dict(
                    porder=order, 
                    torder=self._obligatory_order_manager.get_trans_order(oid=cid),
                    contract_value=self.maker_contract_value
                    )
                lvl_idx = self._obligatory_order_manager.get_trans_order(oid=cid).floor
                order_obj.meta["lvl_idx"] = int(lvl_idx)
                self._obligatory_executor.on_order(order=order_obj)

                """ Update Inventory """
                if order_obj.filled_qty > 0:
                    qty_chg = self._delta_neutral_executor.get_inc_fill_qty_in_tokens(order=order_obj)
                    qty_chg = qty_chg if order_obj.side == Direction.long else -qty_chg
                    self.update_inventory(chg_qty=qty_chg, key="obl", prc=order_obj.prc)

                """ Hedge Order """
                resid_qty_in_tokens, hedge_trans_orders = self._delta_neutral_executor.update_order(order=order_obj)

                """ Clean up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    self._obligatory_order_manager.pop_trans_order(oid=cid)
                    self._obligatory_executor.remove_order(oid=cid)
                    self._delta_neutral_executor.remove_order(oid=cid)
                    if self.oid_to_cid.get(oid) != None:
                        self.oid_to_cid.pop(oid)
                    if order_obj.side == Direction.long:
                        self._order_stats["buy_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["buy_qty"] += order_obj.filled_qty
                    else:
                        self._order_stats["sell_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["sell_qty"] += order_obj.filled_qty
                    
                # if abs(resid_qty_in_tokens) > self._numeric_tol:
                #     if self._hedge_order_manager.is_empty() and self._inventory_order_manager.is_empty():
                #         """ Hedge Resid Qty """
                #         resid_inventory, inven_trans_orders = self._inventory_executor.hedge_inventory(inventory=resid_qty_in_tokens)
                #         for o in inven_trans_orders:
                            # self.hedge_inventory_preparation(order=o, order_manager=self._inventory_order_manager)
                #             self.loop.create_task(self.send_inventory_order(order=o))
                #     else:
                #         self.cancel_all_hedge_orders()
                #         self.cancel_all_inventory_orders()

                # for o in hedge_trans_orders:
                    # self.hedge_inventory_preparation(order=o, order_manager=self._hedge_order_manager)
                #     self.send_hedge_order(order=o)


            if self._hedge_order_manager.contains_trans_order(oid=cid) or self._inventory_order_manager.contains_trans_order(oid=cid):
                if self._hedge_order_manager.contains_trans_order(oid=cid):
                    order_obj: Order = Order.from_dict(
                        porder=order, 
                        torder=self._hedge_order_manager.get_trans_order(oid=cid),
                        contract_value=self.maker_contract_value
                        )
                else:
                    order_obj: Order = Order.from_dict(
                        porder=order, 
                        torder=self._inventory_order_manager.get_trans_order(oid=cid),
                        contract_value=self.maker_contract_value
                        )

                """ Update inventory """
                if order_obj.filled_qty > 0:
                    qty_chg = self._inventory_executor.get_inc_fill_qty_in_tokens(order=order_obj)
                    qty_chg = qty_chg if order_obj.side == Direction.long else -qty_chg
                    self.update_inventory(chg_qty=qty_chg,key="hedge", prc=order_obj.prc)

                self._inventory_executor.update_order(order=order_obj)

                """ Clean up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    if self._hedge_order_manager.contains_trans_order(oid=cid):
                        self._hedge_order_manager.pop_trans_order(oid=cid)
                        if self.oid_to_cid.get(oid) != None:
                            self.oid_to_cid.pop(oid)
                    elif self._inventory_order_manager.contains_trans_order(oid=cid):
                        self._inventory_order_manager.pop_trans_order(oid=cid)
                        if self.oid_to_cid.get(oid) != None:
                            self.oid_to_cid.pop(oid)
                    if order_obj.side == Direction.long:
                        self._order_stats["buy_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["buy_qty"] += order_obj.filled_qty
                    else:
                        self._order_stats["sell_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["sell_qty"] += order_obj.filled_qty

            """ Hedge Inventory """
            # if abs(self._inventory) > self._numeric_tol:
            #     if self._hedge_order_manager.is_trans_order_empty() and self._inventory_order_manager.is_trans_order_empty():
            #         resid_inventory, hedge_trans_orders = self._inventory_executor.hedge_inventory(inventory=self._inventory)
            #         for o in hedge_trans_orders:
                        # self.hedge_inventory_preparation(order=o, order_manager=self._inventory_order_manager)
            #             self.loop.create_task(self.send_inventory_order(order=o))
            #     else:
            #         self.cancel_all_hedge_orders()
            #         self.cancel_all_inventory_orders()

            """ Got an order that has already finished, this is due to receiving msgs in the wrong order. Skip """
            self.logger.info(f"Received msg for a finished order {order}, skip.")
            
            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(f"on order processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle on_order err: {err}")
            traceback.print_exc()
        
            
    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            if len(self.unknown_order_dict) > 100000:
                self.logger.critical(f"too many unknown orders: {len(self.unknown_order_dict)}")
            for oid, partial_order in self.unknown_order_dict.copy().items():
                if self.oid_to_cid.get(oid) != None:
                    now_know_order[oid] = partial_order
                    self.unknown_order_dict.pop(oid)
                elif time.time()*1e3 - partial_order.server_ms > 10000:
                    self.unknown_order_dict.pop(oid)
            await asyncio.gather(
                *[self.on_order(partial_order) for _, partial_order in now_know_order.items()]
            )
            
    async def on_bbo(self, symbol, bbo):
        try:
            s = time.time()
            bbo_obj = BBO.from_dict_prod(iid=symbol, bbo_depth=bbo)
            iid = symbol
            if symbol.startswith("jojo") and self.market == "usdt_contract":
                ex, contract = symbol.split(".")
                ccy1, _, _ = contract.split("_")
                contract = f"{ccy1}_usdt_swap"
                iid = f"{ex}.{contract}"
            server_ts = bbo_obj.server_time
            local_ts = bbo_obj.local_time
            # ism = self.iid_state_manager
            # ism.update_heart_beat_time(iid=iid, ts=int(time.time()*1e3))

            """ tx time guard """
            if self.server_time_guard.has_last_server_time(iid=iid):
                last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                if server_ts < last_server_time:
                    return
            self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

            """ Obligatory check stale """
            # oiid = f"{self.cfg.ex}.{self.cfg.contract}"
            # is_in_stale_state = ism.is_in_stale_state(iid=oiid)
            # is_active = ism.is_active(iid=oiid)
            # if not is_in_stale_state:
            #     if ism.is_stale(iid=oiid, ts=int(time.time()*1e3)):
            #         ism.enter_stale_state(iid=oiid)
            #         if is_active:
            #             self.deactivate_oex()
                        # self.loop.create_task(self.cancel_obligatory_orders())

            """ Hedge check stale """
            # for hex in list(self._active_hexs):
            #     hiid = f"{hex}.{self.cfg.contract}"
            #     is_in_stale_state = ism.is_in_stale_state(iid=hiid)
            #     is_active = ism.is_active(iid=hiid)
            #     if not is_in_stale_state:
            #         if ism.is_stale(iid=hiid, ts=int(time.time()*1e3)):
            #             ism.enter_stale_state(iid=hiid)
            #             if is_active:
            #                 self.deactivate_hex(ex=hex)
            #                 self.loop.create_task(self.cancel_hedge_orders_for_ex(ex=hex))
            #                 self.loop.create_task(self.cancel_inventory_orders_for_ex(ex=hex))

            # if not ism.is_active(iid=iid):
            #     return

            self._xex_depth.update_depth_for_ex_with_bbo(bbo=bbo_obj)
            self._obligatory_executor.update_xex_depth(xex_depth=self._xex_depth)
            self._delta_neutral_executor.update_xex_depth(xex_depth=self._xex_depth)
            self._inventory_executor.update_xex_depth(xex_depth=self._xex_depth)

            # if not ism.is_active(iid=oiid):
            #     return
            # if not self._active_hexs:
            #     return
            if not self.send:
                return

            trans_orders = self._obligatory_executor.gen_quotes(ts=bbo_obj.local_time)
            
            for trans_order in trans_orders:
                self.loop.create_task(self.send_obligatory_order(order=trans_order))
        except Exception as err:
            self.logger.critical(f"handle bbo err: {err}")
            traceback.print_exc()
            
    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            self.ap = orderbook["asks"][0][0]
            self.bp = orderbook["bids"][0][0]
        except:
            self.logger.critical(f"{orderbook}")
        try:
            if symbol.startswith("okex"):
                self.logger.info("has okex orderbook")
            
            s = time.time()
            ex, contract = symbol.split(".")
            if ex.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = contract.split("_")
                contract = f"{ccy1}_usdt_swap"
            ccy = contract.split("_")
            depth_obj:Depth = Depth.from_dict(
                depth=orderbook,
                ccy1=ccy[0],
                ccy2=ccy[1],
                ex=ex,
                contract=contract
                )
            ex = depth_obj.meta_data.ex
            server_ts = depth_obj.server_ts
            local_ts = depth_obj.resp_ts
            iid = f"{ex}.{contract}"
            
            """ Latency state """
            # ism = self.iid_state_manager
            # latency = int(time.time()*1e3) - depth_obj.server_ts
            # is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
            # is_active = ism.is_active(iid=iid)
            # if not is_in_high_latency_state:
            #     if ism.is_high_latency(iid=iid, latency=latency):
            #         ism.enter_high_latency_state(iid=iid)
            #         if is_active:
            #             if self.is_oex(ex=ex):
                            # self.deactivate_oex()
                            # self.loop.create_task(self.cancel_obligatory_orders())
            #             elif self.is_hex(ex=ex):
            #                 self.deactivate_hex(ex=ex)
            #                 self.loop.create_task(self.cancel_hedge_orders_for_ex(ex=ex))
            #                 self.loop.create_task(self.cancel_inventory_orders_for_ex(ex=ex))
            #             else:
            #                 raise NotImplementedError()
            # else:  # is_in_high_latency_state
            #     if not ism.is_high_latency(iid=iid, latency=latency):
            #         ism.enter_normal_latency_state(iid=iid)
            #         is_active_now = ism.is_active(iid=iid)
            #         if not is_active and is_active_now:
            #             if self.is_oex(ex=ex):
            #                 self.activate_oex(depth=depth_obj)
            #             elif self.is_hex(ex=ex):
            #                 self.activate_hex(depth=depth_obj)
            #             else:
            #                 raise NotImplementedError()

            """ tx time guard """
            if self.server_time_guard.has_last_server_time(iid=iid):
                last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                if server_ts < last_server_time:
                    return
            self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

            """ Activate stale state """
            # is_in_stale_state = ism.is_in_stale_state(iid=iid)
            # is_active = ism.is_active(iid=iid)
            # if is_in_stale_state:
            #     ism.enter_nonstale_state(iid=iid)
            #     is_active_now = ism.is_active(iid)
            #     if not is_active and is_active_now:
            #         if self.is_oex(ex=ex):
            #             self.activate_oex(depth=depth_obj)
            #         elif self.is_hex(ex=ex):
            #             self.activate_hex(depth=depth_obj)
            #         else:
            #             raise NotImplementedError()

            # ism.update_heart_beat_time(iid=iid, ts=int(time.time()*1e3))

            # oiid = f"{self.cfg.ex}.{self.cfg.contract}"
            # is_in_stale_state = ism.is_in_stale_state(iid=oiid)
            # is_active = ism.is_active(iid=oiid)
            # if not is_in_stale_state:
            #     if ism.is_stale(iid=oiid, ts=int(time.time()*1e3)):
            #         ism.enter_stale_state(iid=oiid)
            #         if is_active:
                        # self.deactivate_oex()
                        # self.loop.create_task(self.cancel_obligatory_orders())

            # for hex in list(self._active_hexs):
            #     hiid = f"{hex}.{self.cfg.contract}"
            #     is_in_stale_state = ism.is_in_stale_state(iid=hiid)
            #     is_active = ism.is_active(iid=hiid)
            #     if not is_in_stale_state:
            #         if ism.is_stale(iid=hiid, ts=int(time.time()*1e3)):
            #             ism.enter_stale_state(iid=hiid)
            #             if is_active:
            #                 self.deactivate_hex(ex=hex)
            #                 self.loop.create_task(self.cancel_hedge_orders_for_ex(ex=hex))
            #                 self.loop.create_task(self.cancel_inventory_orders_for_ex(ex=hex))

            self._xex_depth.update_depth_for_ex(depth=depth_obj)
            self._obligatory_executor.update_xex_depth(xex_depth=self._xex_depth)
            self._delta_neutral_executor.update_xex_depth(xex_depth=self._xex_depth)
            self._inventory_executor.update_xex_depth(xex_depth=self._xex_depth)
            
            # if not ism.is_active(iid=oiid):
            #     self.logger.info("ism")
            #     return
            # if not self._active_hexs:
            #     self.logger.info(f"active_hexs={self._active_hexs}")
            #     return
            if not self.send:
                return
            trans_orders = self._obligatory_executor.gen_quotes(ts=depth_obj.resp_ts)
            for trans_order in trans_orders:
                self.loop.create_task(self.send_obligatory_order(order=trans_order))
            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(f"orderbook processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle orderbook err: {err}")
            traceback.print_exc()
            
    async def base_send_order(self, order: TransOrder):
        if order.floor == 2:
            self.logger.info(f"send floor 2 order: {order.__dict__}")
        iid = order.iid
        side = self.direction_map(side=order.side)

        order_type = OrderType.Limit
        if order.maker_taker == MakerTaker.maker:
            order_type = OrderType.PostOnly
        if order.maker_taker == MakerTaker.taker:
            order_type = OrderType.IOC

        if order.quantity == 0:
            return None
        try:
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            if iid.startswith("okex") and self.market == "usdt_contract":
                symbol = f"{iid}.swap.cross"
            if iid.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = self.cfg.contract.split("_")
                symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
            symbol_cfg = self.get_symbol_config(symbol)
            resp:AtomOrder = await self.make_order(
                symbol_name=symbol,
                price=Decimal(order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                volume=Decimal(order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                side=side,
                order_type=order_type,
                position_side=OrderPositionSide.Open,
                client_id=order.client_id,
                expired_at=int(time.time()+86400*365*11)
            )
        except Exception as err:
            self.logger.critical(f"send order failed, {err}")
            return None
        
        self.logger.info(f"order resp:{resp}")
        order.update(resp)
        order.price = round(float(resp.requested_price), price_precision[iid])
        order.quantity = round(float(resp.requested_amount), qty_precision[iid])
        order.sent_ts = int(time.time()*1e3)
        return resp
    
    def trim_order_before_sending(self, order: TransOrder):
        ex, contract = order.iid.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        if ex.startswith("jojo") and self.market == "usdt_contract":
            ccy1, _, _ = contract.split("_")
            order.symbol_id = self.get_symbol_config(f"{ex}.{ccy1}_usdc_swap.swap.na")["id"]
        elif ex == "okex" and self.market == "usdt_contract":
            order.symbol_id = self.get_symbol_config(f"{order.iid}.swap.cross")["id"]
        else:
            order.symbol_id = self.get_symbol_config(f"{order.iid}.{self.market}.{self.sub_market}")["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[order.iid])
        else:
            order.price = round_decimals_up(order.price, price_precision[order.iid])
        
        order.quantity = round(order.quantity, qty_precision[order.iid])
        
        return cid
    
    async def send_obligatory_order(self, order: TransOrder):
        # self.logger.info("sending obligatory order")
        try:
            for oid in self._obligatory_order_manager.keys():
                if self._obligatory_order_manager.get_trans_order(oid):
                    o = self._obligatory_order_manager.trans_order_at(oid=oid)
                else:
                    continue
                if o.side == order.side and o.iid == order.iid and o.floor == order.floor:
                    rel_diff = np.log(order.price / o.price)
                    if abs(rel_diff) > self.cfg.requirements[o.floor][2]:
                        if not o.cancel:
                            self.logger.info("canceling order")
                            await self.handle_cancel_order(oid, o)
                    else:
                        o.sent_ts = int(time.time()*1e3)
                    return
            cid = self.trim_order_before_sending(order)
            self._obligatory_order_manager.add_trans_order(order=order, oid=cid)
            # self.logger.info("sending obligatory order2")
            resp = await self.base_send_order(order=order)
            if resp:
                self.oid_to_cid[resp.xchg_id] = cid
            else:
                self._obligatory_order_manager.pop_trans_order(oid=cid)
        except:
            traceback.print_exc()
            
    def hedge_inventory_preparation(self, order: TransOrder, order_manager: OrderManager):
        """
        do this before create task of send_hedge_order and send_inventory_order
        """
        cid = self.trim_order_before_sending(order)
        order_manager.add_trans_order(order=order, oid=cid)
            
    async def send_hedge_order(self, order: TransOrder):
        resp = await self.base_send_order(order=order)
        if resp:
            self.oid_to_cid[resp.xchg_id] = order.client_id
        else:
            self._hedge_order_manager.pop_trans_order(oid=order.client_id)

    async def send_inventory_order(self, order: TransOrder):
        resp = await self.base_send_order(order=order)
        if resp:
            self.oid_to_cid[resp.xchg_id] = order.client_id
        else:
            self._inventory_order_manager.pop_trans_order(oid=order.client_id)
    
    async def cancel_order(self, order: TransOrder, catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.data
        except Exception as err:
            if not catch_error:
                raise
            else:
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, redirect False")
                return False
    
    async def handle_cancel_order(self, oid, order: TransOrder):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                order.cancel = True
                res = await self.cancel_order(order)
                if res is False:
                    order.cancel = False
                self.canceling_id.remove(oid)
                
        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            order.cancel = False
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
                
    async def rebalance(self):
        # rebalance, use when initializing or emergency exiting.
        ref_prc = self._obligatory_executor._ref_prc
        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.logger.info(f"current reference price={ref_prc}")
        if self.market == "spot":
            for acc in self.account_names:
                self.logger.info(f"handle account rebalance: {acc}")
                ex, _ = acc.split(".")
                iid = f"{ex}.{self.cfg.contract}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                api = self.get_exchange_api_by_account(acc)
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    await api.flash_cancel_orders(symbol_cfg)
                except:
                    self.logger.info("no open orders")
                cur_balance = (await api.account_balance(symbol_cfg)).data
                ccy1_amt = float(cur_balance[ccy1]["all"]) * ref_prc
                ccy2_amt = float(cur_balance[ccy2]["all"])
                buy_amt = (ccy2_amt - ccy1_amt) / 2

                p, side = (ref_prc * (1 + 0.03), Direction.long) if buy_amt > 0 else (ref_prc * (1 - 0.03), Direction.short)
                qty = round_decimals_down(abs(buy_amt)/ref_prc, qty_precision[iid])
                
                order = TransOrder(
                    iid=iid,
                    side=side,
                    p=p,
                    q=qty,
                    floor=None,
                    maker_taker=MakerTaker.taker
                )
                self.logger.info(f"trans order:price={order.price}, quantity={order.quantity},side={order.side},iid={order.iid}")
                cid = self.trim_order_before_sending(order)
                self._rebalance_order_manager.add_trans_order(order=order, oid=cid)
                resp = await self.base_send_order(order=order)
                if resp:
                    self.oid_to_cid[resp.xchg_id] = cid
                else:
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                await asyncio.sleep(1)
                cur_balance = (await api.account_balance(symbol_cfg)).data
                self.logger.info(f"""{ccy1}={float(cur_balance[ccy1]["all"])}, {ccy2}={float(cur_balance[ccy2]["all"])} for {acc}""")
                await asyncio.sleep(1)
        elif self.market == "usdt_contract":
            for acc in self.account_names:
                ex, _ = acc.split(".")
                api = self.get_exchange_api_by_account(acc)
                iid = f"{ex}.{self.cfg.contract}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                if iid.startswith("okex"):
                    symbol = f"{iid}.swap.cross"
                if iid.startswith("jojo"):
                    ccy1, _, _ = self.cfg.contract.split("_")
                    symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    all_opening_orders = (await api.all_opening_orders(symbol_cfg)).data
                    for order in all_opening_orders:
                        await self.cancel_order(order)
                except:
                    traceback.print_exc()
                    self.logger.info("no open orders")
                cur_position = (await api.contract_position(symbol_cfg)).data
                self.logger.critical(f"current position: {cur_position}")
                pos = cur_position["long_qty"] - cur_position["short_qty"]

                p, side = (ref_prc * (1 - 0.001), Direction.short) if pos > 0 else (ref_prc * (1 + 0.001), Direction.long)
                qty = round_decimals_down(abs(pos), qty_precision[iid])
                
                order = TransOrder(
                    iid=iid,
                    side=side,
                    p=p,
                    q=qty,
                    floor=None,
                    maker_taker=MakerTaker.taker
                )
                cid = self.trim_order_before_sending(order)
                self._rebalance_order_manager.add_trans_order(order=order, oid=cid)
                resp = await self.base_send_order(order=order)
                if resp:
                    self.oid_to_cid[resp.xchg_id] = cid
                else:
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                resp = await self.base_send_order(order=order)
                await asyncio.sleep(1)
                cur_position = (await api.contract_position(symbol_cfg)).data
                pos = cur_position["long_qty"] - cur_position["short_qty"]
                self.logger.info(f"""current position={pos}""")
                await asyncio.sleep(1)

        
    async def get_order_status_direct(self, oid, order: TransOrder):
        if oid in self.canceling_id:
            return
        
        if time.time()*1e3 - order.sent_ts > 10000 + order.query * 10000:
            order.query += 1
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(self.get_symbol_config(order.symbol_id),order.xchg_id)
                new_order:AtomOrder = res.data
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
                else:
                    await self.handle_cancel_order(oid=oid,order=order)
                
            except Exception as err:
                self.logger.critical(f"direct check order error: {err}")
                if order.query > 3:
                    self.send = False
                    await asyncio.sleep(5)
                    await self.rebalance()
                    await self.redis_set_cache({})
                    self.logger.critical(f"check order too many times and exiting")
                    exit()
        
    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.check_obligatory_orders(),
                self.check_hedge_orders(),
                self.check_inventory_orders()
            )
            
    async def check_obligatory_orders(self):
        for oid in self._obligatory_order_manager.keys():
            if self._obligatory_order_manager.get_trans_order(oid):
                o = self._obligatory_order_manager.trans_order_at(oid)
            else:
                continue
            await self.get_order_status_direct(oid=oid, order=o)
                
    async def check_hedge_orders(self):
        for oid in self._hedge_order_manager.keys():
            if self._hedge_order_manager.get_trans_order(oid):
                o = self._hedge_order_manager.trans_order_at(oid)
            else:
                continue
            await self.get_order_status_direct(oid=oid, order=o)
            
    async def check_inventory_orders(self):
        for oid in self._inventory_order_manager.keys():
            if self._inventory_order_manager.get_trans_order(oid):
                o = self._inventory_order_manager.trans_order_at(oid)
            else:
                continue
            await self.get_order_status_direct(oid=oid, order=o)
            
    async def check_rebalance_orders(self):
        for oid in self._rebalance_order_manager.keys():
            if self._rebalance_order_manager.get_trans_order(oid):
                o = self._rebalance_order_manager.trans_order_at(oid)
            else:
                continue
            await self.get_order_status_direct(oid=oid, order=o)
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    await self.rebalance()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(5)
            try:
                avg_buy_price = float(self._order_stats["buy_amt"]/self._order_stats["buy_qty"]) if self._order_stats["buy_qty"] else 0
                avg_sell_price = float(self._order_stats["sell_amt"]/self._order_stats["sell_qty"]) if self._order_stats["sell_qty"] else 0
                spread = float((avg_sell_price - avg_buy_price) / (avg_sell_price + avg_buy_price) * 2) if avg_sell_price + avg_buy_price > 0 else float(0)
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="test", 
                        tag={"sn":"xex_obligatory_mm_jojo"}, 
                        fields={
                            "avg_buy_price":float(avg_buy_price),
                            "avg_sell_price":float(avg_sell_price),
                            "spread": float(spread),
                            "inventory":float(self._inventory),
                            "obligatory_inventory": float(self._obl_inventory),
                            "hedge_inventory": float(self._hedge_inventory),
                            }
                        )
                    )
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="test", 
                        tag={"sn":"xex_obligatory_mm_jojo"}, 
                        fields={
                            "trade_amount_per_second": float(self.trd_amt/(time.time() - self.start_ts)),
                            "pure_profit": float(self.trd_amt * spread / 2) 
                            }
                        )
                    )
                
                for oid, order in list(self._obligatory_order_manager.trans_order_items()):
                    if order.side == Direction.short:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="test", 
                                tag={"sn":"xex_obligatory_mm_jojo"}, 
                                fields={
                                    "cache_sell_price":float(order.price)
                                    }
                                )
                            )
                    else:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="test", 
                                tag={"sn":"xex_obligatory_mm_jojo"}, 
                                fields={
                                    "cache_buy_price":float(order.price)
                                    }
                                )
                            )
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="test", 
                        tag={"sn":"xex_obligatory_mm_jojo"}, 
                        fields={
                            "trade_amount_per_second": float(self.trd_amt/(time.time() - self.start_ts)),
                            "pure_profit": float(self.trd_amt * spread / 2),
                            }
                        )
                    )
            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
    
    async def debug(self):
        while True:
            await asyncio.sleep(30)
            for oid, o in list(self._obligatory_order_manager.trans_order_items()):
                if o.floor != 0:
                    continue
                self.logger.critical(f"order: p={o.price}, q={o.quantity}, side={o.side}, id={oid}, time={time.time()*1e3 - o.sent_ts}")
            self.logger.critical(".................")
            # self.logger.critical(f"attack filled rate={self.filled_counter/self.counter if self.counter else 0}, total attact={self.counter}")
            
    async def rolling_attack(self):
        while True:
            await asyncio.sleep(1)
            try:
                for oid in self._obligatory_order_manager.keys():
                    if self._obligatory_order_manager.get_trans_order(oid=oid):
                        order = self._obligatory_order_manager.trans_order_at(oid=oid)
                        if order.cancel == True:
                            continue
                        iid = order.iid
                        symbol = f"{iid}.{self.market}.{self.sub_market}"
                        if iid.startswith("okex") and self.market == "usdt_contract":
                            symbol = f"{iid}.swap.cross"
                        if iid.startswith("jojo") and self.market == "usdt_contract":
                            ccy1, _, _ = self.cfg.contract.split("_")
                            symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
                        symbol_cfg = self.get_symbol_config(symbol)
                        
                        if order.side == Direction.long and order.price >= self.bp:
                            self.logger.info(f"attacking, side=sell, price={order.price}")
                            self.counter += 1
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                                volume=Decimal(order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                                side=OrderSide.Sell,
                                order_type=OrderType.IOC,
                                position_side=OrderPositionSide.Open,
                            )
                        elif order.side == Direction.short and order.price <= self.ap:
                            self.logger.info(f"attacking, side=buy, price={order.price}")
                            self.counter += 1
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                                volume=Decimal(order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                                side=OrderSide.Buy,
                                order_type=OrderType.IOC,
                                position_side=OrderPositionSide.Open,
                            )
            except:
                traceback.print_exc()
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.check_unknown_order(),
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.debug(),
                # self.rolling_attack()
                # self.get_local_stats()
            )
        
            
if __name__ == '__main__':
    logging.getLogger().setLevel("CRITICAL")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()