import asyncio
from collections import deque
from decimal import Decimal
from asyncio.queues import Queue
import traceback
import ujson as json
import time
import numpy as np

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

from atom.model.order import Order as AtomOrder

from xex_mm.utils.base import TransOrder, Direction, MakerTaker
from atom.exchange_api.abc import ApiResponse
from xex_mm.utils.contract_mapping import ContractMapper

EXPIRED_WINDOW = 20

RECVWINDOW = 300

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.send_stats = dict()
        self.send_stats["recieve"] = deque(maxlen=100)
        self.send_stats["inner"] = deque(maxlen=100)
        self.send_stats["raw_recieve"] = deque(maxlen=100)
        
        self.max_process_time = 0
    
    # def update_config_before_init(self):
    #     self.use_colo_url = True
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        ch1 = f"mq:hf_signal:send_request:{self.strategy_name}"
        ch2 = f"mq:hf_signal:cancel_request:{self.strategy_name}"
        ch3 = f"mq:hf_signal:query_request:{self.strategy_name}"
        ch4 = f"mq:hf_signal:ping:{self.strategy_name}"
        self.loop.create_task(self.subscribe_to_channel([ch1],self.process_send_request))
        self.loop.create_task(self.subscribe_to_channel([ch2],self.process_cancel_request))
        self.loop.create_task(self.subscribe_to_channel([ch3],self.process_query_request))
        self.loop.create_task(self.subscribe_to_channel([ch4],self.process_ping))

    # async def subscribe_to_channel(self, channel_names, call_back):
    #     while True:
    #         dt = await self.cache_redis.handler.brpop(channel_names[0])
    #         await call_back(*dt)

    # async def publish_to_channel(self, channel_name, payload):
    #     if not isinstance(payload, str):
    #         payload = json.dumps(payload)
    #     await self.cache_redis.handler.lpush(channel_name, payload)
    
    async def publish_to_channel(self, channel_name, payload):
        if not isinstance(payload, str):
            payload = json.dumps(payload)
        await self.oms_redis_agent.handler.publish(channel_name, payload)

    async def subscribe_to_channel(self, channel_names, call_back):
        """
        call_back:

        async def process_xxx(self, channel_name, msg):
            ...
        """
        p = self.oms_redis_agent.handler.pubsub()
        if not isinstance(channel_names, (list, tuple)):
            channel_names = [channel_names]
        await p.subscribe(*channel_names)
        async for msg in p.listen():
            if msg["type"] != "message":
                continue
            await call_back(msg["channel"], msg["data"])
        
    async def trader_cancel_order(self, order: TransOrder, strategy_name: str,catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        try:
            self.logger.info(f"cancel, {order.__dict__}")
            symbol = self.get_symbol_config(order.symbol)
            api = self.get_exchange_api_by_account(order.account_name)
            r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            self.logger.info(f"[cancel order]: {order.xchg_id}/{order.client_id} {r.data}")
            self.logger.info(f"cancel header: {r.headers}")
            await self.publish_to_channel(
                    f"mq:hf_signal:cancel_response:{strategy_name}", 
                    {
                        "status":r.data,
                        "strategy_name":strategy_name,
                        "cancel_id":order.cancel_id,
                        "client_id":order.client_id,
                        "headers":dict(r.headers),
                        "trader":self.strategy_name,
                        }
                    )
        except RateLimitException as err:
            code = json.loads(err.err_message).get("code")
            await self.publish_to_channel(
                f"mq:hf_signal:cancel_response:{strategy_name}",
                {
                    "status":False,
                    "strategy_name":strategy_name,
                    "cancel_id":order.cancel_id,
                    "client_id":order.client_id,
                    "headers":None,
                    "trader":self.strategy_name,
                    "rate_limit": code,
                    "trader":self.strategy_name
                    }
            )
        except Exception as err:
            if not catch_error:
                raise
            self.logger.error(f"[cancel order error] [{err}] {order.xchg_id}/{order.client_id}, redirect True")
            await self.publish_to_channel(
                f"mq:hf_signal:cancel_response:{strategy_name}", 
                {
                    "status":False,
                    "strategy_name":strategy_name,
                    "cancel_id":order.cancel_id,
                    "client_id":order.client_id,
                    "headers":None,
                    "trader":self.strategy_name,
                    }
                )
    
    def direction_map(self, side: Direction):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True
        
    
    async def send_order(self, order: TransOrder, strategy_name: str):
        symbol = order.symbol
        try:
            self._account_config_[order.account_name]["cfg_pos_mode"][symbol] = 1
            side = self.direction_map(side=order.side)

            order_type = OrderType.Limit
            if order.maker_taker == MakerTaker.maker:
                order_type = OrderType.PostOnly
            if order.maker_taker == MakerTaker.taker:
                order_type = OrderType.IOC

            symbol_cfg = self.get_symbol_config(symbol)
            s = time.time()*1e3
            self.send_stats["recieve"].append(s-order.sent_ts)
            resp: ApiResponse = await self.raw_make_order(
                symbol_name=symbol,
                price=Decimal(order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                volume=Decimal(order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                side=side,
                order_type=order_type,
                position_side=OrderPositionSide.Open,
                client_id=order.client_id,
                account_name=order.account_name,
                recvWindow=RECVWINDOW
            )
            self.send_stats["inner"].append(time.time()*1e3 - s)
            self.logger.info(f"send header:{resp.headers}")
            self.logger.info(f"send header copy:{dict(resp.headers)}")
            await self.publish_to_channel(
                f"mq:hf_signal:send_response:{strategy_name}", 
                {
                    "status": True,
                    "strategy_name":strategy_name,
                    "client_id":order.client_id,
                    "data":resp.data.to_dict(),
                    "headers":dict(resp.headers),
                    "trader":self.strategy_name
                    }
            )
        except RateLimitException as err:
            code = json.loads(err.err_message).get("code")
            self.logger.info(f"rate limit exception code:{code}")
            await self.publish_to_channel(
                f"mq:hf_signal:send_response:{strategy_name}",
                {
                    "status": False,
                    "strategy_name":strategy_name,
                    "client_id":order.client_id,
                    "data":str(err),
                    "headers":None,
                    "rate_limit": code,
                    "account_name":order.account_name,
                    "trader":self.strategy_name
                    }
            )
        except Exception as err:
            self.logger.info(f"send order err:{err}")
            traceback.print_exc()
            await self.publish_to_channel(
                f"mq:hf_signal:send_response:{strategy_name}", 
                {
                    "status": False,
                    "strategy_name":strategy_name,
                    "client_id":order.client_id,
                    "data":str(err),
                    "headers":None,
                    "trader":self.strategy_name
                    }
            )

    async def query_order(self, order: TransOrder, strategy_name):
        try:
            self.logger.info(f"quering: {order.__dict__}")
            api = self.get_exchange_api_by_account(order.account_name) 
            resp: ApiResponse = await api.order_match_result(
                symbol=self.get_symbol_config(order.symbol),
                order_id=order.xchg_id,
                client_id=order.client_id,
                )
            self.logger.info(f"query result: {resp.data.__dict__}")
            self.logger.info(f"query header: {resp.headers}")
            await self.publish_to_channel(
                f"mq:hf_signal:query_response:{strategy_name}",
                {
                    "status": True,
                    "strategy_name":strategy_name,
                    "query_id":order.query_id,
                    "data":resp.data.to_dict(),
                    "headers":dict(resp.headers),
                    "trader":self.strategy_name
                    }
            )
        except RateLimitException as err:
            code = json.loads(err.err_message).get("code")
            await self.publish_to_channel(
                f"mq:hf_signal:query_response:{strategy_name}",
                {
                    "status": False,
                    "strategy_name":strategy_name,
                    "query_id":order.query_id,
                    "data":str(err),
                    "headers":None,
                    "rate_limit": code,
                    "trader":self.strategy_name
                    }
            )
        except Exception as err:
            await self.publish_to_channel(
                f"mq:hf_signal:query_response:{strategy_name}",
                {
                    "status": False,
                    "strategy_name":strategy_name,
                    "query_id":order.query_id,
                    "data":str(err),
                    "headers":None,
                    "trader":self.strategy_name
                    }
            )
        
    async def process_ping(self, ch_name, request):
        payload = json.loads(request)
        self.loop.create_task(
            self.publish_to_channel(
                f"mq:hf_signal:pong:{payload['strategy_name']}",
                {
                    "trader": self.strategy_name
                } 
            )
        )
            
    async def process_send_request(self, ch_name, request):
        s = time.time()*1e3
        payload = json.loads(request)
        if payload["trader"] != self.strategy_name:
            return
        recv = time.time()*1e3-int(payload["timestamp"])
        self.send_stats["raw_recieve"].append(recv)
        if recv > EXPIRED_WINDOW:
            self.loop.create_task(
                self.publish_to_channel(
                    f"mq:hf_signal:send_response:{payload['strategy_name']}", 
                    {
                        "status": False,
                        "strategy_name":payload["strategy_name"],
                        "client_id":payload["order"]["client_id"],
                        "data":f"send request expired:{recv}, trader:{self.strategy_name}, inner time:{time.time()*1e3 - s}, max process time:{self.max_process_time}",
                        "headers":None,
                        "trader":self.strategy_name,                    
                        }
                )
            )
            return
        order = TransOrder.from_dict(payload["order"])
        self.logger.info(f"trader sending order:{order.__dict__}")
        self.loop.create_task(
            self.send_order(
                order=order,
                strategy_name=payload["strategy_name"]
                )
            )
        elapsed = time.time()*1e3 - s
        if elapsed > self.max_process_time:
            self.max_process_time = elapsed
    
    async def process_cancel_request(self, ch_name, request):
        payload = json.loads(request)
        if payload["trader"] != self.strategy_name:
            return
        recv = time.time()*1e3-int(payload["timestamp"])
        order = TransOrder.from_dict(payload["order"])
        if recv > EXPIRED_WINDOW:
            self.loop.create_task(
                self.publish_to_channel(
                f"mq:hf_signal:cancel_response:{payload['strategy_name']}", 
                {
                    "status":False,
                    "strategy_name":payload['strategy_name'],
                    "cancel_id":order.cancel_id,
                    "client_id":order.client_id,
                    "headers":None,
                    "trader":self.strategy_name,
                    }
                )
            )
            return
        
        self.loop.create_task(
            self.trader_cancel_order(
                order=order,
                strategy_name=payload["strategy_name"],
            )
        )
            
    async def process_query_request(self, ch_name, request):
        payload = json.loads(request)
        if payload["trader"] != self.strategy_name:
            return
        recv = time.time()*1e3-int(payload["timestamp"])
        order = TransOrder.from_dict(payload["order"])
        if recv > EXPIRED_WINDOW:
            self.loop.create_task(
                self.publish_to_channel(
                f"mq:hf_signal:query_response:{payload['strategy_name']}",
                {
                    "status": False,
                    "strategy_name":payload["strategy_name"],
                    "query_id":order.query_id,
                    "data":"internal expired",
                    "headers":None,
                    "trader":self.strategy_name
                    }
                )
            )
            return
        
        self.loop.create_task(
            self.query_order(
                order=order,
                strategy_name=payload["strategy_name"]
            )
        )
            
                
    async def strategy_core(self):
        await asyncio.sleep(10)
        while True:
            await asyncio.sleep(10)
            if self.send_stats["recieve"] and self.send_stats["inner"] and self.send_stats["raw_recieve"]:
                await self.redis_set_cache(
                    {
                        "recieve mean":np.mean(self.send_stats["recieve"]),
                        "recieve 90":np.quantile(self.send_stats["recieve"], 0.9),
                        "inner mean":np.mean(self.send_stats["inner"]),
                        "inner 90":np.quantile(self.send_stats["inner"], 0.9),
                        "raw_recieve mean":np.mean(self.send_stats["raw_recieve"]),
                        "raw_recieve 90":np.quantile(self.send_stats["raw_recieve"], 0.9),
                    }
                )
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()