from lib2to3.pgen2.token import AWAIT
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

from decimal import Decimal
import collections

import asyncio
import time
import numpy as np

SYMBOLS = ["btc","eth","sol","etc","ada","ftm"
    ]

class OrderbookInfo():
    def __init__(self, symbol) -> None:
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.amount_cache = collections.deque(maxlen=10*60*30)
        self.trade_cache = collections.deque(maxlen=60*30)
        self.trade_max =0
        self.trade_min =0
    
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.use_raw_stream = True
        self.orderbook_info = dict()
        for key in SYMBOLS:
            self.orderbook_info[key] = OrderbookInfo(key)

    async def before_strategy_start(self):
        for key in SYMBOLS:
            self.subscribe_orderbook(self.orderbook_info[key].symbol)
            self.direct_subscribe_public_trade(self.orderbook_info[key].symbol)

    async def on_orderbook(self, symbol, orderbook):
        amount = 0
        best_ask = orderbook["asks"][0][0]
        best_bid = orderbook["bids"][0][0]
 
        for i in range(len(orderbook["asks"])):
            if (orderbook["asks"][i][0] - best_ask) / best_ask < Decimal(0.001):
                amount += orderbook["asks"][i][1] * orderbook["asks"][i][0] 
            if (best_bid - orderbook["bids"][i][0]) / best_bid < Decimal(0.001):
                amount += orderbook["bids"][i][1] * orderbook["bids"][i][0]
        symbol_key = symbol.split(".")[1]
        symbol_key = symbol_key.split("_")[0]
        self.orderbook_info[symbol_key].amount_cache.append(float(amount))
        
    async def on_public_trade(self, symbol, trade):
        p = float(trade["data"]["p"])
        symbol_key = symbol.split(".")[1]
        symbol_key = symbol_key.split("_")[0]
    
        p_min = self.orderbook_info[symbol_key].trade_min
        
        self.orderbook_info[symbol_key].trade_max = max(self.orderbook_info[symbol_key].trade_max,p)
        self.orderbook_info[symbol_key].trade_min = min(self.orderbook_info[symbol_key].trade_min,p) if p_min > 0 else p
        
    async def trim_trade(self):
        while True:
            await asyncio.sleep(1)
            for key in SYMBOLS:
                p_max = self.orderbook_info[key].trade_max
                p_min = self.orderbook_info[key].trade_min
                gap = (p_max - p_min) / (p_max + p_min) * 2 if p_max>0 and p_min>0 else 0
                self.orderbook_info[key].trade_cache.append(gap)
                self.orderbook_info[key].trade_max = 0
                self.orderbook_info[key].trade_min = 0
                
    async def set_cache(self):
        while True:
            await asyncio.sleep(3)
            # temp_cache = dict(sorted(self.orderbook_info.items(), key=lambda item: np.median(item[1].amount_cache),reverse=False))
            # temp_cache = dict(sorted(self.orderbook_info.items(), key=lambda item: item[1].res,reverse=True))
            temp_cache = dict(sorted(self.orderbook_info.items(), key=lambda item: np.quantile(item[1].trade_cache,0.8),reverse=True))
            cache = dict()
            for key in list(temp_cache):
                cache[key] = f"{round(np.median(self.orderbook_info[key].amount_cache) / 10000,2)},{round(np.quantile(self.orderbook_info[key].trade_cache,0.8)*1e3,3)}"
            await self.redis_set_cache(cache)
        
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache(),
            self.trim_trade()
        )
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()