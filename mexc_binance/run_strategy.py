import asyncio
from decimal import Decimal
import time
import pandas as pd
from asyncio.queues import Queue
import os
import numpy as np
import traceback
import math

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

from xex_mm.utils.base import Depth, Trade, Direction, ReferencePriceCalculator
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor
from xex_mm.utils.configs import price_precision, qty_precision

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.ex_maker = self.config['strategy']['params']['ex_maker']
        self.ex_taker = self.config['strategy']['params']['ex_taker']
        self.symbol_maker = f"{self.ex_maker}.{self.contract}.{self.market}.{self.sub_market}"
        self.symbol_taker = f"{self.ex_taker}.{self.contract}.{self.market}.{self.sub_market}"
        
        
        self.maker_order_cache = dict()
        self.taker_order_cache = dict()
        
        self.send_order_queue = Queue()
        
        self.hedge_queue = Queue()
        self.hedge = 0
        
        self.pos = 0
        
        self.unknown_order_dict = {}
        
        self.canceling_id= set()
        
        self.buy_amount = 0
        self.sell_amount = 0
        self.buy_qty = 0
        self.sell_qty = 0
        
        self.crypto_best_ask = None
        self.crypto_best_bid = None
        
        self.xex_depth = XExDepth()
        self.ref_prc_cal = ReferencePriceCalculator()
        self.hedge_executor = XexMmDeltaNeutralExecutor(
            contract=self.contract,
            ex=self.symbol_taker  , 
            hedge_exs=[self.symbol_taker], 
            profit_margin_return=-np.inf
            )
        
    def direction_map(self, side):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
        
    async def before_strategy_start(self):
        self.direct_subscribe_order_update(symbol_name=self.symbol_maker)
        self.direct_subscribe_order_update(symbol_name=self.symbol_taker)
        
        self.direct_subscribe_orderbook(symbol_name=self.symbol_maker, is_incr_depth=True)
        self.direct_subscribe_orderbook(symbol_name=self.symbol_taker, is_incr_depth=True)
        
        self.logger.setLevel("WARNING")
        
        maker_config = self.get_symbol_config(self.symbol_maker)
        self.maker_qty_tick = maker_config["qty_tick_size"]
        self.logger.warning(f"maker qty tick: {self.maker_qty_tick}, min_qty_value: {maker_config['min_quantity_val']}, min_notional_val: {maker_config['min_notional_val']}")
        
        taker_config = self.get_symbol_config(self.symbol_taker)
        self.taker_qty_tick = taker_config["qty_tick_size"]
        self.logger.warning(f"taker qty tick: {self.taker_qty_tick}, min_qty_value: {taker_config['min_quantity_val']}, min_notional_val: {taker_config['min_notional_val']}")
        
        if self.market in {"spot","margin"}:
            api = self.get_exchange_api(self.ex_maker)[0]
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol_maker))).data
            self.maker_btc_all = cur_balance["btc"]["all"]
            self.maker_btc_available = cur_balance["btc"]["available"]
            self.maker_usdt_all = cur_balance["usdt"]["all"]
            self.maker_usdt_available = cur_balance["usdt"]["available"]
            self.logger.warning(f"{self.ex_maker}: {cur_balance}")
            self.logger.warning(f"{self.maker_btc_all}_{self.maker_btc_available}_{self.maker_usdt_all}_{self.maker_usdt_available}")
        
    async def on_order(self, order):
        
        if order.xchg_id not in list(self.maker_order_cache) and order.xchg_id not in list(self.taker_order_cache):
            self.unknown_order_dict[order.xchg_id] = order
            self.logger.warning(f"find unknow order {order.xchg_id}")
            return
        
        if order.xchg_id in list(self.maker_order_cache):
            if order.filled_amount < self.maker_order_cache[order.xchg_id].filled_amount:
                return
            amount_changed = order.filled_amount - self.maker_order_cache[order.xchg_id].filled_amount
            self.maker_order_cache[order.xchg_id].filled_amount = order.filled_amount
            self.maker_order_cache[order.xchg_id].avg_filled_price = order.avg_filled_price
            self.maker_order_cache[order.xchg_id].xchg_status = order.xchg_status
            if amount_changed > self.maker_qty_tick:
                self.logger.warning(f"order has filled, {amount_changed}, {self.maker_order_cache[order.xchg_id].side}, {order.avg_filled_price}")
                hedge_orders = self.hedge_executor.update_order(oid=order.xchg_id, order=self.maker_order_cache[order.xchg_id])
                self.hedge_queue.put_nowait(hedge_orders)
                
            if order.xchg_status in OrderStatus.fin_status():
                o = self.maker_order_cache.get(order.xchg_id)
                if self.maker_order_cache[order.xchg_id].side == OrderSide.Buy:
                    self.pos += order.filled_amount
                    self.buy_amount += order.filled_amount * order.avg_filled_price
                    self.buy_qty += order.filled_amount
                    self.maker_usdt_available += (o.requested_amount * o.requested_price - order.filled_amount * order.avg_filled_price)
                    self.maker_btc_available += order.filled_amount
                else:
                    self.pos -= order.filled_amount
                    self.sell_amount += order.filled_amount * order.avg_filled_price
                    self.sell_qty += order.filled_amount
                    self.maker_usdt_available += order.filled_amount * order.avg_filled_price
                    self.maker_btc_available += (o.requested_amount - order.filled_amount)
                self.maker_order_cache.pop(order.xchg_id)
                if order.xchg_id in self.unknown_order_dict:
                    self.unknown_order_dict.pop(order.xchg_id)
                    
        elif order.xchg_id in list(self.taker_order_cache):
            if order.xchg_status in OrderStatus.fin_status():
                self.logger.warning(f"hedge at: {order.filled_amount}, {self.taker_order_cache[order.xchg_id].side}, {order.avg_filled_price}")
                if self.taker_order_cache[order.xchg_id].side == OrderSide.Buy:
                    self.pos += order.filled_amount
                    self.buy_amount += order.filled_amount * order.avg_filled_price
                    self.buy_qty += order.filled_amount
                else:
                    self.pos -= order.filled_amount
                    self.sell_amount += order.filled_amount * order.avg_filled_price
                    self.sell_qty += order.filled_amount
                self.taker_order_cache.pop(order.xchg_id)
                if order.xchg_id in self.unknown_order_dict:
                    self.unknown_order_dict.pop(order.xchg_id)
            
    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.maker_order_cache or xchg_id in self.taker_order_cache:
                    # self.logger.warning(f"unkonw {xchg_id} order rest back")
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(partial_order) for xchg_id, partial_order in now_know_order.items()]
            )
    
    async def on_orderbook(self, symbol, orderbook):
        try:
            ex, _ = symbol.split(".")
            new_depth = Depth.from_dict(depth=orderbook)
            self.xex_depth.update_depth_for_ex(ex_name=ex, depth=new_depth)
            self.hedge_executor.update_xex_depth(xex_depth=self.xex_depth)
            self.ref_prc = self.ref_prc_cal.calc_from_depth(self.xex_depth.get_xex_depth(symbol))
            if self.send_order_queue.empty() and not self.sending and len(self.xex_depth.get_ex_names()) == 2 and self.send:
                self.send_order_queue.put_nowait(1)
                            
        except Exception as err:
            self.logger.warning(f"handle orderbook err: {err}")
            
    async def send_maker_order_action(self):
        
        async def send_order_helper(order):
            send_block = False
            for oid, o in self.maker_order_cache.items():
                if o.side == self.direction_map(order.side) and not self.order_cache[oid].message["cancel"] and o.message["floor"] == order.floor:
                    if abs(np.log(float(o.requested_price)/ order.price)) > self.tolerance_diff:
                        self.loop.create_task(self.handle_cancel_order(oid))
                    else:
                        self.maker_order_cache[oid].create_ms = int(time.time()*1e3)
                        send_block = True
            if send_block:
                return 
            
            if order.side == Direction.short:
                price = round_decimals_up(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
            elif order.side == Direction.long:
                price = round_decimals_down(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
            else:
                raise NotImplementedError(f"trans order side is {order.side}")
            await self.send_order(
                symbol=self.symbol_maker,
                price=Decimal(price),
                qty=Decimal(qty),
                side=self.direction_map(order.side),
                floor=order.floor
            )
            
        while True:
            await self.send_order_queue.get()
            self.sending = True
            orders = self.generate_obligatory_orders()
            for order in orders:
                self.loop.create_task(send_order_helper(order))  
            self.sending = False
    
    def generate_obligatory_orders(self):
        ...
        
    async def hedge_order_action(self):
        while True:
            orders = await self.hedge_queue.get()
            self.logger.warning("hedging now")
            for order in orders:
                self.send_order_taker(order)
                    
    async def send_order_taker(self, order):
        symbol = ".".join([order.iid, self.market, self.sub_market])
        if order.side == Direction.long:
            price = round_decimals_down(order.price, price_precision[order.iid])
        elif order.side == Direction.short:
            price = round_decimals_up(order.price, price_precision[order.iid])
        side = self.direction_map(order.side)
        qty = round(order.quantity, qty_precision[order.iid])
        
        if not self.volume_notional_check(symbol, price, qty):
            self.hedge += qty if side == OrderSide.Buy else -qty
            return
        new_price, new_qty = self.format_price_volume(symbol, price+self.half_price_tick, qty+self.half_step_tick)
        try:
            order = await self.make_order(
                    symbol_name=symbol,
                    price=new_price,
                    volume=new_qty,
                    side=side,
                    order_type=OrderType.IOC,
                    position_side=OrderPositionSide.Open,
                )
            self.hedge += (qty - new_qty) if side == OrderSide.Buy else -(qty - new_qty)
            extra_info = dict()
            extra_info["query"] = 0 
            order.message = extra_info
            self.taker_order_cache[order.xchg_id] = order
        except:
            self.hedge_queue.put_nowait([order])
            
    async def send_order_maker(self,symbol,price,qty,side,floor=None,order_type=OrderType.PostOnly):
        if not self.volume_notional_check(symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(symbol, price, qty)
        
        if side == OrderSide.Buy:
            self.maker_usdt_available -= price * qty
        else:
            self.maker_btc_available -= qty
            
        try:
            order = await self.make_order(
                symbol_name=symbol,
                price=price,
                volume=qty,
                side=side,
                order_type=order_type,
                position_side=OrderPositionSide.Open,
            )
            extra_info = dict()
            extra_info["cancel"] = False
            extra_info["query"] = 0
            extra_info["floor"] = floor
            order.message = extra_info
            self.maker_order_cache[order.xchg_id] = order
        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"{err}")
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            res = await api.order_match_result(self.get_symbol_config(order.symbol_id),order.xchg_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order
                    
    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > 60000 + order.message["query"] * 10000:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.maker_order_cache.get(oid) is not None:
                        self.maker_order_cache[oid].message["query"] += 1
                        if self.maker_order_cache[oid].message["query"]>3:
                            self.send = False
                            await asyncio.sleep(5)
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        if not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                    if self.taker_order_cache.get(oid) is not None:
                        self.taker_order_cache[oid].message["query"] += 1
                        if self.taker_order_cache[oid].message["query"]>3:
                            self.send = False
                            await asyncio.sleep(5)
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        if not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                    
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.maker_order_cache.items()]
            )
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.taker_order_cache.items()]
            )
    
    async def set_cache(self):
        while True:
            await asyncio.sleep(30)
            await self.redis_set_cache(
                {}
                )
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
        
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.check_unknown_order(),
                self.send_maker_order_action(),
                self.hedge_order_action(),
                self.reset_missing_order_action(),
                self.update_redis_cache()
            )
        
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()