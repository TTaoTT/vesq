from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2

import re
MEXC_RE = re.compile(r"^[0-9]{1,20}$")

import collections
import logging
import asyncio
from decimal import Decimal
import time
import pandas as pd
import os
import numpy as np
import traceback
import math
from typing import Dict, Set, List, AnyStr, Union, Tuple
import ujson as json
from orderedset import OrderedSet
from asyncio.queues import Queue, LifoQueue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from strategy_base.base import CommonStrategy

from xex_mm.utils.base import Depth, BBO, Direction, TransOrder, OrderManager, MakerTaker, Order
from xex_mm.utils.base import OrderManager
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor
from xex_mm.utils.configs import price_precision, qty_precision, HeartBeatTolerance, FeeConfig, LatencyBoundConfig
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_inventory_executor import XexMmDeltaNeutralInventoryExecutor
from xex_mm.strategy_configs.obligatory_mm_config import ObligatoryMmConfig

from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard

from sortedcontainers import SortedDict
from _operator import neg

class OrderManager(OrderManager):
    def clear(self):
        self._trans_order_cache = {}
        self._ask_ladder = SortedDict()
        self._bid_ladder = SortedDict(neg)
        self._oid_to_iid_map: Dict[str, str] = {}
        self._iid_to_oids_map: Dict[str, OrderedSet] = {}
        self._order_cache = {}
        
class ObligatoryExecutor(ObligatoryExecutor):
    def gen_quotes(self, ts: int) -> List[TransOrder]:
        self.on_wakeup(ts=ts)
        orders = []
        if not self._xex_depth.contains("binance"):
            return []
        ref_prc = self._ref_prc_calculator.calc_from_depth(depth=self._xex_depth.get_depth_for_ex("binance"), amt_bnd=0)
        for i, (ret, q_ccy2) in enumerate(self._get_requirements()):
            ask_prc = ref_prc * (1 + ret)
            bid_prc = ref_prc * (1 - ret)
            q_ccy1 = q_ccy2 / ask_prc
            l_r = self._get_hit_anomaly_ratio(d=Direction.long, i=i, ts=ts)
            s_r = self._get_hit_anomaly_ratio(d=Direction.short, i=i, ts=ts)
            if math.isnan(l_r) or (l_r < 2):
                orders.append(TransOrder(
                    side=Direction.short,
                    p=ask_prc,
                    q=q_ccy1,
                    iid=self.iid,
                    floor=i,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.maker,
                ))
            if math.isnan(s_r) or (s_r < 2):
                orders.append(TransOrder(
                    side=Direction.long,
                    p=bid_prc,
                    q=q_ccy1,
                    iid=self.iid,
                    floor=i,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.maker,
                ))
        return orders
        
class LatencyBoundConfig1(LatencyBoundConfig):
    latency_bound_map = {
        "binance.btc_usdt": (100, 100),
        "mexc.btc_usdt": (1000, 1000)
    }
    
class IIdStateManager1(IIdStateManager):
    def __init__(self):
        self._heart_beat_tol_cfg = HeartBeatTolerance()
        self._last_update_time_map = dict()
        self.stale_iids = set()
        self.nonstale_iids = set()

        self._latency_bound_cfg = LatencyBoundConfig1()
        self._latency_map = dict()
        self.high_latency_iids = set()
        self.normal_latency_iids = set()

        self._active_state_tracker_map = dict()
        self.active_iids = set()
        self.inactive_iids = set()

        self._n_switches = 2
        
def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # self.ob_process_time = collections.deque(maxlen=1000000)
        # self.ob_process_time_update = collections.deque(maxlen=1000000)
        self.order_counter = 0
        self.cancel_counter = 0
        self.start_ts = time.time()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.ex_maker = self.config['strategy']['params']['ex_maker']
        self.bnd_amt_in_coin = Decimal(self.config['strategy']['params']['bnd_amt_in_coin'])
        self.max_inventory = float(self.config['strategy']['params']['max_inventory'])
        self.inner_order_trigger_inventory = self.max_inventory / 2
        self.unknown_order_dict = dict()
        self.oid_to_cid = dict()
        self.cancel_ts_dict = dict()
        self.cfg = ObligatoryMmConfig(
            version="",
            contract=self.contract,
            ex=self.ex_maker,
            hedge_exs=["binance"],
            profit_margin_return=-np.inf,
            cancel_tolerance=1e-7,
            requirements=[
                (-0.00003, 500),
                (0.0000, 2000),
                (0.00003, 2000),
                (0.00005, 2000),
                # (0.0005, 100),
                # (0.001, 80),
                # (0.008, 8e4),
                # (0.016, 16e4),
                # (0.032, 32e4),
                # (0.064, 64e4),
            ],
        )
        self.layer = len(self.cfg.requirements)

        self._obligatory_order_manager = OrderManager()
        self._hedge_order_manager = OrderManager()
        self._inventory_order_manager = OrderManager()
        self._rebalance_order_manager = OrderManager()
        self._xex_depth = XExDepth(contract=self.cfg.contract)
        
        self._heart_beat_tol_cfg = HeartBeatTolerance()
        self._last_update_time = {}
        self._state = {}
        
        self._numeric_tol = 1e-10
        self.min_notional = 15
        
        
        self._obligatory_executor = self.build_obligatory_executor()
        self._delta_neutral_executor = XexMmDeltaNeutralExecutor(contract=self.cfg.contract,
                                                                 profit_margin_return=self.cfg.profit_margin_return)
        self._inventory_executor = XexMmDeltaNeutralInventoryExecutor(contract=self.cfg.contract)
        for ex in [self.cfg.ex] + self.cfg.hedge_exs:
            makers_fee, takers_fee = FeeConfig().get_fee(ex=ex, contract=self.cfg.contract)
            self._delta_neutral_executor.update_fee_rate(contract=self.cfg.contract, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)
            self._inventory_executor.update_fee_rate(contract=self.cfg.contract, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)
            
        self.iid_state_manager = IIdStateManager1()
        self.server_time_guard = ServerTimeGuard()
        
        self.bob_queue = LifoQueue()
        self.mob_queue = LifoQueue()
        self.bbo_queue = LifoQueue()
        self.order_queue = Queue()

        self._hex_set = set(self.cfg.hedge_exs)
        self._inactive_hexs: Set = self._hex_set.copy()
        self._active_hexs: Set = set()
        self._inventory: float = 0
        self._obl_inventory: float = 0
        self._hedge_inventory: float = 0
        
        self._order_stats = dict()
        self._order_stats["buy_amt"] = 0
        self._order_stats["buy_qty"] = 0
        self._order_stats["sell_amt"] = 0
        self._order_stats["sell_qty"] = 0

        self.canceling_id = set()
        self.quering_id = set()
        
        self.send = False
        
        self.trd_amt= 0 
        
        self.handling_bbo = False
        self.handling_orderbook = False
    
    def update_config_before_init(self):
        self.use_colo_url = True
    
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        
    def is_oex(self, ex: str):
        return ex == self.cfg.ex

    def is_hex(self, ex: str):
        return ex in self._hex_set

    def activate_oex(self, depth: Depth):
        ex = depth.meta_data.ex
        assert self.is_oex(ex=ex)
        self._xex_depth.update_depth_for_ex(depth)
        self.logger.info(f"Activating obligatory ex={ex}")

    def deactivate_oex(self):
        ex = self.cfg.ex
        self._xex_depth.remove_ex(ex=ex)
        self.logger.info(f"Deactivating obligatory ex={ex}")

    def activate_hex(self, depth: Depth):
        ex = depth.meta_data.ex
        assert self.is_hex(ex=ex)
        self._xex_depth.update_depth_for_ex(depth)
        self._inactive_hexs.remove(ex)
        self._active_hexs.add(ex)
        self.logger.info(f"Activating hedge ex={ex}")

    def deactivate_hex(self, ex: str):
        assert ex in self._hex_set
        self._xex_depth.remove_ex(ex=ex)
        self._inactive_hexs.add(ex)
        self._active_hexs.remove(ex)
        self.logger.info(f"Deactivating hedge ex={ex}")

    def build_obligatory_executor(self):
        """ Override this for different exchange / contracts with different MM requirements. """

        oe = ObligatoryExecutor(ex=self.cfg.ex, contract=self.cfg.contract)
        oe.update_requirements(requirements=self.cfg.requirements)
        return oe
    
    def update_inventory(self, chg_qty: float, key: str, prc: float):
        self._inventory += chg_qty
        if key == "obl":
            self._obl_inventory += chg_qty
        elif key == "hedge":
            self._hedge_inventory += chg_qty
            
        self.trd_amt += abs(chg_qty) * prc
        
    def direction_map(self, side: Direction):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
    
    def cancel_obligatory_orders(self):
        for oid in self._obligatory_order_manager.keys():
            if self._obligatory_order_manager.get_trans_order(oid=oid):
                o = self._obligatory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(oid, o))

    def cancel_all_hedge_orders(self):
        for oid in self._hedge_order_manager.keys():
            if self._hedge_order_manager.get_trans_order(oid=oid):
                o = self._hedge_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(oid, o))

    def cancel_hedge_orders_for_ex(self, ex: str):
        iid = f"{ex}.{self.cfg.contract}"
        for oid in self._hedge_order_manager.get_oids_for_iid(iid=iid):
            o = self._hedge_order_manager.trans_order_at(oid=oid)
            if not o.cancel:
                # if self.cancel_ts_dict.get(oid) != None:
                #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                self.loop.create_task(self.handle_cancel_order(oid, o))

    def cancel_all_inventory_orders(self):
        for oid in self._inventory_order_manager.keys():
            if self._inventory_order_manager.get_trans_order(oid=oid):
                o = self._inventory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    # if self.cancel_ts_dict.get(oid) != None:
                    #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                    self.loop.create_task(self.handle_cancel_order(oid, o))
        
    def cancel_inventory_orders_for_ex(self, ex: str):
        iid = f"{ex}.{self.cfg.contract}"
        for oid in list(self._inventory_order_manager.get_oids_for_iid(iid=iid)):
            if self._inventory_order_manager.get_trans_order(oid=oid):
                o = self._inventory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    # if self.cancel_ts_dict.get(oid) != None:
                    #     self.cancel_ts_dict[oid][1] = int(time.time()*1e3)
                    self.loop.create_task(self.handle_cancel_order(oid, o))
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        maker_symbol = f"{self.cfg.ex}.{self.cfg.contract}.{self.market}.{self.sub_market}"
        maker_config = self.get_symbol_config(symbol_identity=maker_symbol)
        self.maker_qty_tick = maker_config["qty_tick_size"]
        self.maker_contract_value = maker_config["contract_value"]
        bnd = self.bnd_amt_in_coin / maker_config["contract_value"]
        self.direct_subscribe_order_update(symbol_name=maker_symbol)
        self.direct_subscribe_orderbook(symbol_name=maker_symbol, is_incr_depth=True)
        # self.direct_subscribe_bbo(symbol_name=maker_symbol)

        self.taker_config = dict()
        for ex in self.cfg.hedge_exs:
            symbol = f"{ex}.{self.cfg.contract}.{self.market}.{self.sub_market}"
            taker_config = self.get_symbol_config(symbol_identity=symbol)
            self.taker_config[ex] = taker_config
            bnd = self.bnd_amt_in_coin / taker_config["contract_value"]
            self.direct_subscribe_order_update(symbol_name=symbol)
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_bbo(symbol_name=symbol)
        
        self.loop.create_task(self.handle_ws_update())
        
        counter = 0    
        while True:
            if counter > 1000:
                self.logger.critical("no orderbook comming")
                exit()
            counter += 1
            ref_prc = self._obligatory_executor._ref_prc
            if ref_prc is Ellipsis:
                await asyncio.sleep(1)
                continue
            else:
                await self.rebalance()
                self.logger.critical("finish rebalance")
                break
        
        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        
        self.ccy1_in_coin = 0
        self.ccy2_amt = 0
        
        self.ccy1_in_coin_ex = dict()

        for acc in self.account_names:
            ex, _ = acc.split(".")
            api = self.get_exchange_api_by_account(acc)
            iid = f"{ex}.{self.cfg.contract}"
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            cur_balance = (await api.account_balance(symbol_cfg)).data
            self.ccy1_in_coin += cur_balance[ccy1]["all"]
            self.ccy2_amt += cur_balance[ccy2]["all"]
            
            self.ccy1_in_coin_ex[ex] = cur_balance[ccy1]["all"]
        
            
        self.send = True
        self.logger.critical("start.....")
    
    def handle_on_order(self, order: PartialOrder):
        try:
            s = time.time()
            oid = order.xchg_id
            cid = order.client_id
            
            # if self.cancel_ts_dict.get(cid) != None and order.xchg_status in OrderStatus.fin_status():
            #     tlist = self.cancel_ts_dict[cid]
            #     send_ts, cancel_task_create_ts, cancel_action_ts, cancel_finish_ts = tlist
            #     ts_gap2 = cancel_action_ts - cancel_task_create_ts
            #     ts_gap3 = cancel_finish_ts - cancel_action_ts
            #     if (ts_gap3 > 20 or ts_gap2 > 2) and 0 not in {cancel_task_create_ts,cancel_action_ts,cancel_finish_ts}:
            #         self.logger.critical(f"cancel takes {ts_gap3}ms, trigger cancel_takes {ts_gap2}ms, sent_ts={send_ts}, cancel_task_create_ts={cancel_task_create_ts}, cancel_action_ts={cancel_action_ts}, cancel_finish_ts={cancel_finish_ts}, finished_time={int(time.time()*1e3)}, status={order.xchg_status}")
                
            #     self.cancel_ts_dict.pop(cid)
            # self.logger.info(f"status={order.xchg_status},oid={oid}")
            
            if self.oid_to_cid.get(oid) != None:
                cid = self.oid_to_cid[oid]
                order.client_id = cid
                if cid == None:
                    self.logger.critical(f"no cid")
            # else:
            #     self.unknown_order_dict[oid] = order
                
            
            if not self._obligatory_order_manager.contains_trans_order(oid=cid) and \
                not self._inventory_order_manager.contains_trans_order(oid=cid) and \
                not self._hedge_order_manager.contains_trans_order(oid=cid) and \
                not self._rebalance_order_manager.contains_trans_order(oid=cid):
                # self.logger.warning(f"find unknow order {oid}")
                return
            
            if self._rebalance_order_manager.contains_trans_order(oid=cid):
                order_obj: Order = Order.from_dict(
                    porder=order, 
                    torder=self._rebalance_order_manager.get_trans_order(oid=cid),
                    contract_value=self.maker_contract_value
                    )
                """ Just Clean It Up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                    if self.oid_to_cid.get(oid) != None:
                        self.oid_to_cid.pop(oid)
            
            if self._obligatory_order_manager.contains_trans_order(oid=cid):
                torder = self._obligatory_order_manager.get_trans_order(oid=cid)
                order_obj: Order = Order.from_dict(
                    porder=order, 
                    torder=torder,
                    contract_value=self.maker_contract_value
                    )
                lvl_idx = self._obligatory_order_manager.get_trans_order(oid=cid).floor
                order_obj.meta["lvl_idx"] = int(lvl_idx)
                self._obligatory_executor.on_order(order=order_obj)
                
                """ Update Inventory """
                if order_obj.filled_qty > 0:
                    qty_chg_abs = self._delta_neutral_executor.get_inc_fill_qty_in_tokens(order=order_obj)
                    qty_chg = qty_chg_abs if order_obj.side == Direction.long else -qty_chg_abs
                    if qty_chg_abs > 0:
                        self.update_inventory(chg_qty=qty_chg, key="obl", prc=order_obj.prc)
                        self.logger.critical(f"position created {torder.iid}, send at time {torder.sent_ts}, side {torder.side}, fill time {order.server_ms}, at price {order_obj.avg_fill_prc}, with quantity {qty_chg_abs}, recieve message delay {int(time.time()*1e3-order.server_ms)}ms")

                """ Hedge Order """
                resid_qty_in_tokens, hedge_trans_orders = self._delta_neutral_executor.update_order(order=order_obj)
                
                """ Clean up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    self._obligatory_order_manager.pop_trans_order(oid=cid)
                    self._obligatory_executor.remove_order(oid=cid)
                    self._delta_neutral_executor.remove_order(oid=cid)
                    if self.oid_to_cid.get(oid) != None:
                        self.oid_to_cid.pop(oid)
                    if order_obj.side == Direction.long:
                        self._order_stats["buy_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["buy_qty"] += order_obj.filled_qty
                    else:
                        self._order_stats["sell_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["sell_qty"] += order_obj.filled_qty
                    
                if abs(resid_qty_in_tokens) > self._numeric_tol:
                    if self._hedge_order_manager.is_trans_order_empty() and self._inventory_order_manager.is_trans_order_empty():
                        """ Hedge Resid Qty """
                        if self._active_hexs:
                            resid_inventory, inven_trans_orders = self._inventory_executor.hedge_inventory(inventory=resid_qty_in_tokens)
                            for o in inven_trans_orders:
                                self.hedge_inventory_preparation(order=o, order_manager=self._inventory_order_manager)
                                self.logger.critical(f"residue inventory order {o.iid}, send at time {o.sent_ts}, side {o.side}, at price {o.price}, with quantity {o.quantity}")
                                self.loop.create_task(self.send_inventory_order(order=o))
                    else:
                        self.cancel_all_hedge_orders()
                        self.cancel_all_inventory_orders()

                for o in hedge_trans_orders:
                    self.logger.critical(f"hedge order {o.iid}, send at time {o.sent_ts}, side {o.side}, at price {o.price}, with quantity {o.quantity}")
                    self.hedge_inventory_preparation(order=o, order_manager=self._hedge_order_manager)
                    self.loop.create_task(self.send_hedge_order(order=o))

            if self._hedge_order_manager.contains_trans_order(oid=cid) or self._inventory_order_manager.contains_trans_order(oid=cid):
                if self._hedge_order_manager.contains_trans_order(oid=cid):
                    torder = self._hedge_order_manager.get_trans_order(oid=cid)
                    order_obj: Order = Order.from_dict(
                        porder=order, 
                        torder=torder,
                        contract_value=self.maker_contract_value
                        )
                else:
                    torder = self._inventory_order_manager.get_trans_order(oid=cid)
                    order_obj: Order = Order.from_dict(
                        porder=order, 
                        torder=torder,
                        contract_value=self.maker_contract_value
                        )

                """ Update inventory """
                if order_obj.filled_qty > 0:
                    qty_chg_abs = self._inventory_executor.get_inc_fill_qty_in_tokens(order=order_obj)
                    qty_chg = qty_chg_abs if order_obj.side == Direction.long else -qty_chg_abs
                    if qty_chg_abs > 0:
                        self.update_inventory(chg_qty=qty_chg,key="hedge", prc=order_obj.prc)
                        self.logger.critical(f"close position {torder.iid} at time {torder.sent_ts}, side {torder.side}, fill time {order.server_ms}, at price {order_obj.avg_fill_prc}, with quantity {qty_chg_abs}, recieve message at {int(time.time()*1e3)}")

                self._inventory_executor.update_order(order=order_obj)

                """ Clean up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    if self._hedge_order_manager.contains_trans_order(oid=cid):
                        self._hedge_order_manager.pop_trans_order(oid=cid)
                        if self.oid_to_cid.get(oid) != None:
                            self.oid_to_cid.pop(oid)
                    elif self._inventory_order_manager.contains_trans_order(oid=cid):
                        self._inventory_order_manager.pop_trans_order(oid=cid)
                        if self.oid_to_cid.get(oid) != None:
                            self.oid_to_cid.pop(oid)
                    if order_obj.side == Direction.long:
                        self._order_stats["buy_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["buy_qty"] += order_obj.filled_qty
                    else:
                        self._order_stats["sell_amt"] += order_obj.avg_fill_prc * order_obj.filled_qty
                        self._order_stats["sell_qty"] += order_obj.filled_qty

            """ Hedge Inventory """
            if abs(self._inventory) > self._numeric_tol and (abs(self._inventory) * self._obligatory_executor._ref_prc > self.min_notional):
                if self._hedge_order_manager.is_trans_order_empty() and self._inventory_order_manager.is_trans_order_empty():
                    if self._active_hexs:
                        resid_inventory, hedge_trans_orders = self._inventory_executor.hedge_inventory(inventory=self._inventory)
                        for o in hedge_trans_orders:
                            self.logger.critical(f"inventory order {o.iid}, send at time {o.sent_ts}, side {o.side}, at price {o.price}, with quantity {o.quantity}")
                            self.hedge_inventory_preparation(order=o, order_manager=self._inventory_order_manager)
                            self.loop.create_task(self.send_inventory_order(order=o))
                else:
                    self.cancel_all_hedge_orders()
                    self.cancel_all_inventory_orders()
            
            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(f"on order processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle on_order err: {err}")
            traceback.print_exc()
            
    async def on_order(self, order):
        try:
            await self.order_queue.put(order)
        except:
            traceback.print_exc()
            
    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            if len(self.unknown_order_dict) > 1000:
                self.logger.critical(f"too many unknown orders: {len(self.unknown_order_dict)}")
            for oid, partial_order in self.unknown_order_dict.copy().items():
                if self.oid_to_cid.get(oid) != None:
                    now_know_order[oid] = partial_order
                    self.unknown_order_dict.pop(oid)
            await asyncio.gather(
                *[self.on_order(partial_order) for _, partial_order in now_know_order.items()]
            )
            
            
    async def on_bbo(self, symbol, bbo):
        try:
            await self.bbo_queue.put((symbol, bbo))
        except:
            traceback.print_exc()
            
    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            if symbol.startswith("binance"):
                await self.bob_queue.put((symbol, orderbook))
            elif symbol.startswith("mexc"):
                await self.mob_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")
            
    async def handle_ws_update(self):
        while True:
            try:
                if not self.order_queue.empty():
                    order = await self.order_queue.get()
                    self.handle_on_order(order)
                    continue
                elif self.bbo_queue.empty() and self.mob_queue.empty() and self.bob_queue.empty():
                    await asyncio.sleep(0.001)
                    continue
                elif not self.bob_queue.empty():
                    symbol, orderbook = await self.bob_queue.get()
                    self.bob_queue = LifoQueue()
                    message_type = "ob"
                elif not self.bbo_queue.empty():
                    symbol, bbo = await self.bbo_queue.get()
                    self.bbo_queue = LifoQueue()
                    message_type = "bbo"
                else:
                    symbol, orderbook = await self.mob_queue.get()
                    self.mob_queue = LifoQueue()
                    message_type = "ob"
                
                await asyncio.sleep(0.0001)

                if message_type == "ob":
                    s = time.time()*1e3
                    ex, contract = symbol.split(".")
                    ccy = contract.split("_")
                    depth_obj:Depth = Depth.from_dict(
                        depth=orderbook,
                        ccy1=ccy[0],
                        ccy2=ccy[1],
                        ex=ex,
                        contract=contract
                        )
                    ex = depth_obj.meta_data.ex
                    server_ts = depth_obj.server_ts
                    local_ts = depth_obj.resp_ts
                    iid = symbol
                    
                    """ Latency state """
                    ism = self.iid_state_manager
                    latency = depth_obj.resp_ts - depth_obj.server_ts
                    is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
                    is_active = ism.is_active(iid=iid)
                    if not is_in_high_latency_state:
                        if ism.is_high_latency(iid=iid, latency=latency):
                            ism.enter_high_latency_state(iid=iid)
                            if is_active:
                                if self.is_oex(ex=ex):
                                    self.deactivate_oex()
                                    self.cancel_obligatory_orders()
                                elif self.is_hex(ex=ex):
                                    self.deactivate_hex(ex=ex)
                                    self.cancel_hedge_orders_for_ex(ex=ex)
                                    self.cancel_inventory_orders_for_ex(ex=ex)
                                else:
                                    raise NotImplementedError()
                    else:  # is_in_high_latency_state
                        if not ism.is_high_latency(iid=iid, latency=latency):
                            ism.enter_normal_latency_state(iid=iid)
                            is_active_now = ism.is_active(iid=iid)
                            if not is_active and is_active_now:
                                if self.is_oex(ex=ex):
                                    self.activate_oex(depth=depth_obj)
                                elif self.is_hex(ex=ex):
                                    self.activate_hex(depth=depth_obj)
                                else:
                                    raise NotImplementedError()

                    """ tx time guard """
                    if self.server_time_guard.has_last_server_time(iid=iid):
                        last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                        if server_ts < last_server_time:
                            continue
                    self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

                    """ Activate stale state """
                    is_in_stale_state = ism.is_in_stale_state(iid=iid)
                    is_active = ism.is_active(iid=iid)
                    if is_in_stale_state:
                        ism.enter_nonstale_state(iid=iid)
                        is_active_now = ism.is_active(iid)
                        if not is_active and is_active_now:
                            if self.is_oex(ex=ex):
                                self.activate_oex(depth=depth_obj)
                            elif self.is_hex(ex=ex):
                                self.activate_hex(depth=depth_obj)
                            else:
                                raise NotImplementedError()

                    ism.update_heart_beat_time(iid=iid, ts=local_ts)

                    oiid = f"{self.cfg.ex}.{self.cfg.contract}"
                    is_in_stale_state = ism.is_in_stale_state(iid=oiid)
                    is_active = ism.is_active(iid=oiid)
                    if not is_in_stale_state:
                        if ism.is_stale(iid=oiid, ts=int(time.time()*1e3)):
                            ism.enter_stale_state(iid=oiid)
                            if is_active:
                                self.deactivate_oex()
                                self.cancel_obligatory_orders()

                    for hex in list(self._active_hexs):
                        hiid = f"{hex}.{self.cfg.contract}"
                        is_in_stale_state = ism.is_in_stale_state(iid=hiid)
                        is_active = ism.is_active(iid=hiid)
                        if not is_in_stale_state:
                            if ism.is_stale(iid=hiid, ts=int(time.time()*1e3)):
                                ism.enter_stale_state(iid=hiid)
                                if is_active:
                                    self.deactivate_hex(ex=hex)
                                    self.cancel_hedge_orders_for_ex(ex=hex)
                                    self.cancel_inventory_orders_for_ex(ex=hex)

                    s1 = time.time()*1e3
                    self._xex_depth.update_depth_for_ex(depth=depth_obj)
                    self._obligatory_executor.update_xex_depth(xex_depth=self._xex_depth)
                    self._delta_neutral_executor.update_xex_depth(xex_depth=self._xex_depth)
                    self._inventory_executor.update_xex_depth(xex_depth=self._xex_depth)
                    # self.ob_process_time_update.append(time.time()*1e3-s1)
                    
                    if not ism.is_active(iid=oiid):
                        self.cancel_obligatory_orders()
                        continue
                    if not self._active_hexs:
                        self.cancel_obligatory_orders()
                        continue
                    if not self.send:
                        self.cancel_obligatory_orders()
                        continue

                    trans_orders = self._obligatory_executor.gen_quotes(ts=depth_obj.resp_ts)
                    if not trans_orders:
                        self.cancel_obligatory_orders()
                        continue
                    buy_send = True
                    sell_send = True
                    if self._obl_inventory > self.max_inventory:
                        buy_send = False
                    if self._obl_inventory < -self.max_inventory:
                        sell_send = False
                        
                    for trans_order in trans_orders:
                        ex, _ = trans_order.iid.split(".")
                        
                        d = self._xex_depth.get_depth_for_ex(ex=ex)
                        if trans_order.side == Direction.long:
                            if not buy_send:
                                trans_order.quantity = 0
                            target = d.asks[0].prc - 10**(-price_precision[trans_order.iid])
                            if trans_order.price < target:
                                trans_order.quantity = 0
                            else:
                                trans_order.price = target
                            
                            # trans_order.price = min(d.asks[0].prc - 10**(-price_precision[trans_order.iid]), trans_order.price)
                            if self._obl_inventory < 0 and trans_order.floor == 0:
                                trans_order.quantity = min(trans_order.quantity - self._obl_inventory, trans_order.quantity * 5)
                            elif trans_order.floor == 0:
                                trans_order.quantity = 0
                        else:
                            if not sell_send:
                                trans_order.quantity = 0
                            target = d.bids[0].prc + 10**(-price_precision[trans_order.iid])
                            if trans_order.price > target:
                                trans_order.quantity = 0
                            else:
                                trans_order.price = target
                            # trans_order.price = max(d.bids[0].prc +  10**(-price_precision[trans_order.iid]), trans_order.price)
                            if self._obl_inventory > 0 and trans_order.floor == 0:
                                trans_order.quantity = min(trans_order.quantity + self._obl_inventory, trans_order.quantity * 5)
                            elif trans_order.floor == 0:
                                trans_order.quantity = 0
                                
                        self.loop.create_task(self.send_obligatory_order(order=trans_order))
                    
                    # self.ob_process_time.append(time.time()*1e3-s)
                elif message_type == "bbo":
                    bbo_obj = BBO.from_dict_prod(iid=symbol, bbo_depth=bbo)
                    iid = symbol
                    server_ts = bbo_obj.server_time
                    local_ts = bbo_obj.local_time
                    ism = self.iid_state_manager
                    ism.update_heart_beat_time(iid=iid, ts=local_ts)

                    """ tx time guard """
                    if self.server_time_guard.has_last_server_time(iid=iid):
                        last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                        if server_ts < last_server_time:
                            continue
                    self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

                    """ Obligatory check stale """
                    oiid = f"{self.cfg.ex}.{self.cfg.contract}"
                    is_in_stale_state = ism.is_in_stale_state(iid=oiid)
                    is_active = ism.is_active(iid=oiid)
                    if not is_in_stale_state:
                        if ism.is_stale(iid=oiid, ts=int(time.time()*1e3)):
                            ism.enter_stale_state(iid=oiid)
                            if is_active:
                                self.deactivate_oex()
                                self.cancel_obligatory_orders()

                    """ Hedge check stale """
                    for hex in list(self._active_hexs):
                        hiid = f"{hex}.{self.cfg.contract}"
                        is_in_stale_state = ism.is_in_stale_state(iid=hiid)
                        is_active = ism.is_active(iid=hiid)
                        if not is_in_stale_state:
                            if ism.is_stale(iid=hiid, ts=int(time.time()*1e3)):
                                ism.enter_stale_state(iid=hiid)
                                if is_active:
                                    self.deactivate_hex(ex=hex)
                                    self.cancel_hedge_orders_for_ex(ex=hex)
                                    self.cancel_inventory_orders_for_ex(ex=hex)

                    if not ism.is_active(iid=iid):
                        self.cancel_obligatory_orders()
                        continue

                    self._xex_depth.update_depth_for_ex_with_bbo(bbo=bbo_obj)
                    self._obligatory_executor.update_xex_depth(xex_depth=self._xex_depth)
                    self._delta_neutral_executor.update_xex_depth(xex_depth=self._xex_depth)
                    self._inventory_executor.update_xex_depth(xex_depth=self._xex_depth)

                    if not ism.is_active(iid=oiid):
                        self.cancel_obligatory_orders()
                        continue
                    if not self._active_hexs:
                        self.cancel_obligatory_orders()
                        continue
                    if not self.send:
                        self.cancel_obligatory_orders()
                        continue

                    trans_orders = self._obligatory_executor.gen_quotes(ts=bbo_obj.local_time)
                    if not trans_orders:
                        self.cancel_obligatory_orders()
                        continue
                    
                    buy_send = True
                    sell_send = True
                    if self._obl_inventory > self.max_inventory:
                        buy_send = False
                    if self._obl_inventory < -self.max_inventory:
                        sell_send = False
                    for trans_order in trans_orders:
                        ex, _ = trans_order.iid.split(".")
                        
                        d = self._xex_depth.get_depth_for_ex(ex=ex)
                        if trans_order.side == Direction.long:
                            if not buy_send:
                                trans_order.quantity = 0
                            target = d.asks[0].prc - 10**(-price_precision[trans_order.iid])
                            if trans_order.price < target:
                                trans_order.quantity = 0
                            else:
                                trans_order.price = target
                            
                            # trans_order.price = min(d.asks[0].prc - 10**(-price_precision[trans_order.iid]), trans_order.price)
                            if self._obl_inventory < 0 and trans_order.floor == 0:
                                trans_order.quantity = min(-self._obl_inventory, trans_order.quantity * 5)
                            elif trans_order.floor == 0:
                                trans_order.quantity = 0
                        else:
                            if not sell_send:
                                trans_order.quantity = 0
                            target = d.bids[0].prc + 10**(-price_precision[trans_order.iid])
                            if trans_order.price > target:
                                trans_order.quantity = 0
                            else:
                                trans_order.price = target
                            # trans_order.price = max(d.bids[0].prc +  10**(-price_precision[trans_order.iid]), trans_order.price)
                            if self._obl_inventory > 0 and trans_order.floor == 0:
                                trans_order.quantity = min(self._obl_inventory, trans_order.quantity * 5)
                            elif trans_order.floor == 0:
                                trans_order.quantity = 0
                                
                        self.loop.create_task(self.send_obligatory_order(order=trans_order))
                
            except:
                traceback.print_exc()
 
            
    async def base_send_order(self, order: TransOrder):
        iid = order.iid
        side = self.direction_map(side=order.side)

        order_type = OrderType.Limit
        if order.maker_taker == MakerTaker.maker:
            order_type = OrderType.PostOnly
        if order.maker_taker == MakerTaker.taker:
            order_type = OrderType.IOC

        if order.quantity == 0:
            return None
        symbol = f"{iid}.{self.market}.{self.sub_market}"
        symbol_cfg = self.get_symbol_config(symbol)
        if not self.volume_notional_check(symbol=symbol, price=order.price, qty=order.quantity):
            return None
        try:
            resp:AtomOrder = await self.make_order(
                symbol_name=symbol,
                price=Decimal(order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                volume=Decimal(order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                side=side,
                order_type=order_type,
                position_side=OrderPositionSide.Open,
                client_id=order.client_id
            )
        except Exception as err:
            # msg = json.loads(err.err_message).get("msg")
            # if not iid.startswith("mexc") or msg not in {"Insufficient position", "Oversold","冻结用户资产失败"}:
            self.logger.critical(f"send order failed, {err}, client_id:{order.client_id}")
            if err.metadata["code"] == 429:
                self.loop.create_task(self.hold_for_rate_limit())
            return None
        
        self.logger.info(f"order sent: {resp.__dict__}, client id:{order.client_id}")
        order.update(resp)
        order.price = round(float(resp.requested_price), price_precision[iid])
        order.quantity = round(float(resp.requested_amount), qty_precision[iid])
        order.sent_ts = int(time.time()*1e3)
        # self.cancel_ts_dict[order.client_id] = [0 for _ in range(4)]
        # self.cancel_ts_dict[order.client_id][0] = order.sent_ts
        return resp

    async def hold_for_rate_limit(self):
        self.send = False
        await asyncio.sleep(5)
        self.send = True
        
    def trim_order_before_sending(self, order: TransOrder):
        ex, _ = order.iid.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        order.symbol_id = self.get_symbol_config(f"{order.iid}.{self.market}.{self.sub_market}")["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[order.iid])
        else:
            order.price = round_decimals_up(order.price, price_precision[order.iid])
        
        order.quantity = round(order.quantity, qty_precision[order.iid])
        
        return cid
    
    async def send_obligatory_order(self, order: TransOrder):
        for oid in self._obligatory_order_manager.keys():
            if self._obligatory_order_manager.get_trans_order(oid):
                o = self._obligatory_order_manager.trans_order_at(oid=oid)
            else:
                continue
            if o.side == order.side and o.iid == order.iid and o.floor == order.floor:
                rel_diff = np.log(order.price / o.price)
                if abs(rel_diff) > self.cfg.cancel_tolerance:
                    if not o.cancel:
                        self.loop.create_task(self.handle_cancel_order(oid, o))
                else:
                    o.sent_ts = int(time.time()*1e3)
                    return
        cid = self.trim_order_before_sending(order)
        self._obligatory_order_manager.add_trans_order(order=order, oid=cid)
        resp = await self.base_send_order(order=order)
        if resp != None:
            if resp.xchg_id and self._obligatory_order_manager.get_trans_order(oid=cid) != None:
                self.oid_to_cid[resp.xchg_id] = order.client_id
        else:
            try:
                self._obligatory_order_manager.pop_trans_order(oid=cid)
            except:
                traceback.print_exc()
                
    def hedge_inventory_preparation(self, order: TransOrder, order_manager: OrderManager):
        """
        do this before create task of send_hedge_order and send_inventory_order
        """
        order.price = order.price * (1+0.0002) if order.side == Direction.long else order.price * (1-0.0002)
        cid = self.trim_order_before_sending(order)
        order_manager.add_trans_order(order=order, oid=cid)
            
    async def send_hedge_order(self, order: TransOrder):
        resp = await self.base_send_order(order=order)
        if resp != None:
            if resp.xchg_id and self._hedge_order_manager.get_trans_order(oid=order.client_id) != None:
                self.oid_to_cid[resp.xchg_id] = order.client_id
        else:
            try:
                self._hedge_order_manager.pop_trans_order(oid=order.client_id)
            except:
                traceback.print_exc()
                
    async def send_inventory_order(self, order: TransOrder):
        resp = await self.base_send_order(order=order)
        if resp != None:
            if resp.xchg_id and self._inventory_order_manager.get_trans_order(oid=order.client_id) != None:
                self.oid_to_cid[resp.xchg_id] = order.client_id
        else:
            try:
                self._inventory_order_manager.pop_trans_order(oid=order.client_id)
            except:
                traceback.print_exc()
                
    async def cancel_order(self, order: TransOrder, catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}, status code: {r.status}, raw content: {r.json}")
            return r.data
        except Exception as err:
            if not catch_error:
                raise
            else:
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, redirect False")
                return False
    
    async def handle_cancel_order(self, oid, order: TransOrder):
        async def unit_cancel_order(order: TransOrder):
            try:
                res = await self.cancel_order(order)
                if res is True:
                    order.cancel = True
            except Exception as err:
                self.logger.critical(f'cancel order {oid} err {err}')
                
        if oid in self.canceling_id:
            return
        
        self.canceling_id.add(oid)
    
        counter = 0
        while True:
            if self._obligatory_order_manager.get_trans_order(oid=oid) != None:
                o = self._obligatory_order_manager.trans_order_at(oid=oid)
                if o.cancel:
                    self.canceling_id.remove(oid)
                    return
            else:
                self.canceling_id.remove(oid)
                return
            counter += 1
            self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(0.01)
            
            if counter > 1000:
                if self._obligatory_order_manager.get_trans_order(oid=oid) != None:
                    o = self._obligatory_order_manager.trans_order_at(oid=oid)
                    self.logger.critical(f"too many cancel orders, cache order={o.__dict__}")
                self.logger.critical(f"too many cancel orders, order={order.__dict__}")
                exit()
                
    async def reset_rebalance(self):
        # rebalance, use when emergency exiting.
        ref_prc = self._obligatory_executor._ref_prc
        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.logger.info(f"current reference price={ref_prc}")   
        self._inventory = 0
        self._obl_inventory = 0
        self._hedge_inventory = 0
        total_ccy1_in_coin = 0
        for acc in self.account_names:
            self.logger.info(f"handle account rebalance: {acc}")
            ex, _ = acc.split(".")
            api = self.get_exchange_api_by_account(acc)
            iid = f"{ex}.{self.cfg.contract}"
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            try:
                await api.flash_cancel_orders(symbol_cfg)
            except:
                self.logger.info("no open orders")
            cur_balance = (await api.account_balance(symbol_cfg)).data
            ccy1_in_coin = float(cur_balance[ccy1]["all"])
            total_ccy1_in_coin += ccy1_in_coin
            pos = ccy1_in_coin - float(self.ccy1_in_coin_ex[ex])
            if ex == self.cfg.ex:
                self._obl_inventory = pos
                self._inventory += pos
            elif ex in self.cfg.hedge_exs:
                self._hedge_inventory = pos
                self._inventory += pos
            await asyncio.sleep(1)
            
        buy_amt = self.ccy1_in_coin - total_ccy1_in_coin

        p, side = (ref_prc * (1 + 0.03), Direction.long) if buy_amt > 0 else (ref_prc * (1 - 0.03), Direction.short)
        qty = round_decimals_down(abs(buy_amt)/ref_prc, qty_precision[iid])
        iid = f"binance.{self.cfg.contract}"
        order = TransOrder(
            iid=iid,
            side=side,
            p=p,
            q=qty,
            floor=None,
            maker_taker=MakerTaker.taker
        )
        self.logger.info(f"trans order:price={order.price}, quantity={order.quantity},side={order.side},iid={order.iid}")
        cid = self.trim_order_before_sending(order)
        self._rebalance_order_manager.add_trans_order(order=order, oid=cid)
        resp = await self.base_send_order(order=order)
        if resp != None:
            if resp.xchg_id:
                self.oid_to_cid[resp.xchg_id] = order.client_id
            else:
                self.logger.critical(f"order recieved with no xchg id:{resp.to_dict()}")
        else:
            try:
                self._rebalance_order_manager.pop_trans_order(oid=cid)
            except:
                traceback.print_exc()
                
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            iid = f"{ex}.{self.cfg.contract}"
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            cur_balance = (await api.account_balance(symbol_cfg)).data
            self.logger.critical(f"""{ccy1}={float(cur_balance[ccy1]["all"])}, {ccy2}={float(cur_balance[ccy2]["all"])} for {acc}""")
            
    async def rebalance(self):
        # rebalance, use when initializing or emergency exiting.
        ref_prc = self._obligatory_executor._ref_prc
        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.logger.info(f"current reference price={ref_prc}")
        self.available = dict()
        for acc in self.account_names:
            self.logger.info(f"handle account rebalance: {acc}")
            ex, _ = acc.split(".")
            api = self.get_exchange_api_by_account(acc)
            iid = f"{ex}.{self.cfg.contract}"
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            try:
                await api.flash_cancel_orders(symbol_cfg)
            except:
                self.logger.info("no open orders")
            cur_balance = (await api.account_balance(symbol_cfg)).data
            ccy1_amt = float(cur_balance[ccy1]["all"]) * ref_prc
            ccy2_amt = float(cur_balance[ccy2]["all"])
            buy_amt = (ccy2_amt - ccy1_amt) / 2

            p, side = (ref_prc * (1 + 0.03), Direction.long) if buy_amt > 0 else (ref_prc * (1 - 0.03), Direction.short)
            qty = round_decimals_down(abs(buy_amt)/ref_prc, qty_precision[iid])
            
            order = TransOrder(
                iid=iid,
                side=side,
                p=p,
                q=qty,
                floor=None,
                maker_taker=MakerTaker.taker
            )
            self.logger.info(f"trans order:price={order.price}, quantity={order.quantity},side={order.side},iid={order.iid}")
            cid = self.trim_order_before_sending(order)
            self._rebalance_order_manager.add_trans_order(order=order, oid=cid)
            resp = await self.base_send_order(order=order)
            if resp != None:
                if resp.xchg_id and self._rebalance_order_manager.get_trans_order(oid=cid) != None:
                    self.oid_to_cid[resp.xchg_id] = order.client_id
            else:
                try:
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                except:
                    traceback.print_exc()
            await asyncio.sleep(1)
            cur_balance = (await api.account_balance(symbol_cfg)).data
            self.available[acc] = dict()
            self.available[acc][ccy1] = cur_balance[ccy1]["all"]
            self.available[acc][ccy2] = cur_balance[ccy2]["all"]
            self.logger.info(f"""{ccy1}={float(cur_balance[ccy1]["all"])}, {ccy2}={float(cur_balance[ccy2]["all"])} for {acc}""")

    async def get_order_status_direct(self, oid, order: TransOrder, order_manager: OrderManager):
        
        async def unit_query_order(order: TransOrder):
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=order.client_id,
                    )
                new_order:AtomOrder = res.data
                self.logger.info(f"query order: {new_order.__dict__}")
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
                # else:
                #     await self.handle_cancel_order(oid=oid, order=order)
            except Exception as err:
                self.logger.critical(f"direct check order error: {err}, iid={order.iid}, oid={order.xchg_id}, cid={order.client_id}")
                
        if time.time()*1e3 - order.sent_ts < 200 or oid in self.quering_id:
            return
        
        self.quering_id.add(oid)
        
        counter = 0
        while True:
            if order_manager.get_trans_order(oid=oid) != None:
                o =  order_manager.trans_order_at(oid=oid)
                if time.time()*1e3 - order.sent_ts < 200:
                    self.quering_id.remove(oid)
                    return
            else:
                self.quering_id.remove(oid)
                return
            counter += 1
            self.loop.create_task(unit_query_order(order))
            await asyncio.sleep(1)
            
            if counter > 1000:
                if order_manager.get_trans_order(oid=oid) != None:
                    o = order_manager.trans_order_at(oid=oid)
                    self.logger.critical(f"too many query orders, cache order={o.__dict__}")
                self.logger.critical(f"too many query orders, order={order.__dict__}")
                exit()
        
    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(0.1)
            self.check_orders(order_manager=self._obligatory_order_manager)
            self.check_orders(order_manager=self._hedge_order_manager)
            self.check_orders(order_manager=self._inventory_order_manager)
            self.check_orders(order_manager=self._rebalance_order_manager)
    
    def check_orders(self, order_manager: OrderManager):
        for oid in order_manager.keys():
            if order_manager.get_trans_order(oid):
                o = order_manager.trans_order_at(oid)
                self.loop.create_task(self.get_order_status_direct(oid=oid, order=o, order_manager=order_manager))
    
    async def check_obligatory_orders(self):
        tasks = []
        for oid in self._obligatory_order_manager.keys():
            if self._obligatory_order_manager.get_trans_order(oid):
                o = self._obligatory_order_manager.trans_order_at(oid)
                tasks.append((oid, o))
            else:
                continue
            # await self.get_order_status_direct(oid=oid, order=o)
        await asyncio.gather(
                *[self.get_order_status_direct(oid=oid, order=o) for oid, o in tasks]
            )
                
    async def check_hedge_orders(self):
        tasks = []
        for oid in self._hedge_order_manager.keys():
            if self._hedge_order_manager.get_trans_order(oid):
                o = self._hedge_order_manager.trans_order_at(oid)
                tasks.append((oid, o))
            else:
                continue
        await asyncio.gather(
                *[self.get_order_status_direct(oid=oid, order=o) for oid, o in tasks]
            )
            
    async def check_inventory_orders(self):
        tasks = []
        for oid in self._inventory_order_manager.keys():
            if self._inventory_order_manager.get_trans_order(oid):
                o = self._inventory_order_manager.trans_order_at(oid)
                tasks.append((oid, o))
            else:
                continue
        await asyncio.gather(
                *[self.get_order_status_direct(oid=oid, order=o) for oid, o in tasks]
            )
            
    async def check_rebalance_orders(self):
        for oid in self._rebalance_order_manager.keys():
            if self._rebalance_order_manager.get_trans_order(oid):
                o = self._rebalance_order_manager.trans_order_at(oid)
            else:
                continue
            await self.get_order_status_direct(oid=oid, order=o)
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    await self.rebalance()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache(
                    {
                        "exit":None, 
                        # "ob process time min":np.min(self.ob_process_time),
                        # "ob process time 25%":np.quantile(self.ob_process_time,0.25),
                        # "ob process time 50%":np.quantile(self.ob_process_time,0.5),
                        # "ob process time 75%":np.quantile(self.ob_process_time,0.75),
                        # "ob process time max":np.max(self.ob_process_time),
                        # "ob process time update min":np.min(self.ob_process_time_update),
                        # "ob process time update 25%":np.quantile(self.ob_process_time_update,0.25),
                        # "ob process time update 50%":np.quantile(self.ob_process_time_update,0.5),
                        # "ob process time update 75%":np.quantile(self.ob_process_time_update,0.75),
                        # "ob process time update max":np.max(self.ob_process_time_update),
                        }
                    )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(5)
            try:
                avg_buy_price = float(self._order_stats["buy_amt"]/self._order_stats["buy_qty"]) if self._order_stats["buy_qty"] else 0
                avg_sell_price = float(self._order_stats["sell_amt"]/self._order_stats["sell_qty"]) if self._order_stats["sell_qty"] else 0
                spread = float((avg_sell_price - avg_buy_price) / (avg_sell_price + avg_buy_price) * 2) if avg_sell_price + avg_buy_price > 0 else float(0)
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":self.strategy_name}, 
                        fields={
                            "avg_buy_price":float(avg_buy_price),
                            "avg_sell_price":float(avg_sell_price),
                            "spread": float(spread),
                            "inventory":float(self._inventory),
                            "obligatory_inventory": float(self._obl_inventory),
                            "hedge_inventory": float(self._hedge_inventory),
                            }
                        )
                    )
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="trade amount", 
                        tag={"sn":self.strategy_name}, 
                        fields={
                            "trade_amount_per_second": float(self.trd_amt/(time.time() - self.start_ts)),
                            "pure_profit": float(self.trd_amt * spread / 2) 
                            }
                        )
                    )
                
                for oid, order in list(self._obligatory_order_manager.trans_order_items()):
                    if order.side == Direction.short:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    "cache_sell_price":float(order.price)
                                    }
                                )
                            )
                    else:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    "cache_buy_price":float(order.price)
                                    }
                                )
                            )
                
                ccy = self.cfg.contract.split("_")
                ccy1 = ccy[0]
                ccy2 = ccy[1]
                
                ccy1_in_coin = 0
                ccy2_amt = 0

                for acc in self.account_names:
                    ex, _ = acc.split(".")
                    api = self.get_exchange_api_by_account(acc)
                    iid = f"{ex}.{self.cfg.contract}"
                    symbol = f"{iid}.{self.market}.{self.sub_market}"
                    symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                    cur_balance = (await api.account_balance(symbol_cfg)).data
                    ccy1_in_coin += cur_balance[ccy1]["all"]
                    ccy2_amt += cur_balance[ccy2]["all"]
                    
                pnl = float(ccy2_amt - self.ccy2_amt) + float(ccy1_in_coin - self.ccy1_in_coin) * self._obligatory_executor._ref_prc
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="trade amount", 
                        tag={"sn":self.strategy_name }, 
                        fields={
                            "trade_amount_per_second": float(self.trd_amt/(time.time() - self.start_ts)),
                            "pure_profit": float(self.trd_amt * spread / 2),
                            "profit": float(pnl)
                            }
                        )
                    ) 
            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
                
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                # self.check_unknown_order(),
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.get_local_stats(),
                # self.reset_counter()
            )
        
            
if __name__ == '__main__':
    logging.getLogger().setLevel("INFO")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()