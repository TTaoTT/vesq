import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.tot = 0
        self.expired = 0
        
    def update_config_before_init(self):
        self.use_colo_url = True
        
    async def pinging(self):
        api = self.get_exchange_api("binance")[0]
        while True:
            await asyncio.sleep(1)
            s = time.time()*1e3
            await api.make_request("usdt_contract","GET","/fapi/v1/ping")
            t = time.time()*1e3 - s
            self.tot += 1
            if t > 100:
                self.expired += 1
                self.logger.warning(f"ping takes time:{t}")
                
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            await self.redis_set_cache({"fail rate":self.expired/self.tot if self.tot else 0})
    
    async def strategy_core(self):
        await asyncio.gather(
            self.pinging(),
            self.set_cache()
        )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()