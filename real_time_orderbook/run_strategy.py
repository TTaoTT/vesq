from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy

import asyncio
import time

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_1 = "binance.luna_usdt_swap.swap"

    async def before_strategy_start(self):
        await self._get_symbol_config_from_remote([self.symbol_1])
        await self.subscribe_ticker_direct(self.symbol_1)

    async def on_ticker(self,symbol,ticker):
        cms = int(time.time()*1e3)
        gap = cms - ticker['sms']
        self.logger.info(f"symbol:{symbol}, cms:{cms}, gapms:{gap}, ticker:{ticker}")
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(60)
    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()