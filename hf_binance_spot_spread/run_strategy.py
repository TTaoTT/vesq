
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.helpers import send_ding_talk
import collections

from decimal import Decimal 
import asyncio
import time
import numpy as np

SYMBOLS = ["lrc","mana","shib","ltc","avax","trx","iotx","sol","waxp","win","doge","sand","chr","dot","chz","ankr","lto","fil","nu","vet","ada",
"omg","keep","link","idex","ftm","fet","algo","luna","icp","mbox","near","axs","btt","qtum","matic","eos","atom","slp","ens","hot","gala","dent",
"enj","alice","etc","dar","bch","ftt","nbs","sun","zec","sxp","kp3r","atom","mith","flow","porto","iota"]

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # def float2decimal(_dict):
        #     for k, v in _dict.copy().items():
        #         if type(v) == float:
        #             _dict[k] = Decimal(str(v))
        #     return _dict

        # self.params = float2decimal(self.config['strategy']['params'])

        self.symbol_binance = dict()
        self.spread_cache = dict()
        self.trade_vol = dict()
        self.pts = dict()
        self.spread_per_cache = dict()
        self.price_cache = dict()
        self.price = dict()

        self.best_symbol = None

        for key in SYMBOLS:
            self.symbol_binance[key] = f"binance.{key}_usdt.spot.na"
            symbol_cfg = self.get_symbol_config(self.symbol_binance[key])
            self.pts[key] = symbol_cfg['price_tick_size']
            self.spread_cache[key] = []
            self.spread_per_cache[key] = []
            self.trade_vol[key] = 0
            
    async def before_strategy_start(self):
        self.logger.setLevel("WARNING")
        for key in list(self.symbol_binance):
            self.direct_subscribe_orderbook(symbol_name=self.symbol_binance[key])
            self.direct_subscribe_public_trade(symbol_name=self.symbol_binance[key])

    async def on_orderbook(self, symbol, orderbook):
        s = symbol.split(".")[1].split("_")[0]
        spread = float(orderbook["asks"][0][0] - orderbook["bids"][0][0])
        mp = float(orderbook["asks"][0][0] + orderbook["bids"][0][0]) / 2
        pts = float(self.pts[s])
        self.spread_cache[s].append(spread/pts)
        self.spread_per_cache[s].append(spread/mp)
        self.price[s] = mp
        if not self.price_cache[s]:
            self.price_cache[s] = mp

    async def on_public_trade(self, symbol, trade: PublicTrade):
        s = symbol.split(".")[1].split("_")[0]
        self.trade_vol[s] += float(trade.quantity*trade.price)

    async def cal(self):
        while True:
            await asyncio.sleep(180)
            temp_dict = dict(sorted(self.trade_vol.items(), key=lambda item: item[1]))
            n = []
            s_median = []
            s_mean = []
            s_max = []
            s_per = []
            v = []
            temp_key = collections.deque(maxlen=5)
            for key in list(temp_dict):
                if np.median(self.spread_cache[key]) >= 5 or key == self.best_symbol and self.price[key] > self.price_cache[key]*0.995:
                    temp_key.append(key)
            for key in temp_key:
                n.append(key)
                s_median.append(np.median(self.spread_cache[key]))
                s_mean.append(np.mean(self.spread_cache[key]))
                s_max.append(np.max(self.spread_cache[key]))
                s_per.append(np.median(self.spread_per_cache[key]))
                v.append(self.trade_vol[key]/10000)
            try:
                self.logger.warning(
                f"""
                ~~~ {n[0]} ====== {n[1]} ===== {n[2]} ==== {n[3]} ==== {n[4]}
                ~~ {round(s_median[0],2)} ~~ {round(s_median[1],2)} ~~ {round(s_median[2],2)} ~~ {round(s_median[3],2)} ~~ {round(s_median[4],2)}
                ~~ {round(s_mean[0],2)} ~~ {round(s_mean[1],2)} ~~ {round(s_mean[2],2)} ~~ {round(s_mean[3],2)} ~~ {round(s_mean[4],2)}
                ~~ {round(s_max[0],2)} ~~ {round(s_max[1],2)} ~~ {round(s_max[2],2)} ~~ {round(s_max[3],2)} ~~ {round(s_max[4],2)}
                ~~ {round(s_per[0],5)} ~~ {round(s_per[1],5)} ~~ {round(s_per[2],5)} ~~ {round(s_per[3],5)} ~~ {round(s_per[4],5)}
                ~~ {round(v[0],2)} ~~ {round(v[1],2)} ~~ {round(v[2],2)} ~~ {round(v[3],2)} ~~ {round(v[4],2)}
                """
                )
            except:
                self.logger.warning("not enough candidate")

            if len(n) > 2:
                if not self.best_symbol or self.best_symbol not in n or v[-1] > v[-2]*2:
                    if n[-1] != self.best_symbol:
                        self.best_symbol = n[-1]
                        await send_ding_talk(title="INFO USDT SPOT", message="USDT SPOT:" + self.best_symbol,
                                             token="d9f8947090ad3b1ecc36c537490f9ecfb4995d908e15de5eb527676e349ddeeb")
                        await self.redis_set_cache({"best symbol": self.best_symbol})



            for key in SYMBOLS:
                self.spread_cache[key] = []
                self.trade_vol[key] = 0
                self.price_cache[key] = None

    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.cal()
        )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()

   