import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
import aiohttp
from asyncio.queues import Queue
from statsmodels.stats.weightstats import DescrStatsW

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class PriceDistCahce():
    def __init__(self,cache_size=50) -> None:
        self.ts = np.array([]) 
        self.price = np.array([]) 
        self.qty_with_price = np.array([]) 
        self.qty = np.empty((0, 0))
        self.cache_size = cache_size
        self.count = 0
        
        self.feed_trade_time1 = collections.deque(maxlen=10000)
        self.feed_trade_time2 = collections.deque(maxlen=10000)
        self.feed_trade_time3 = collections.deque(maxlen=10000)
        self.feed_trade_time4 = collections.deque(maxlen=10000)
        self.feed_trade_time5 = collections.deque(maxlen=10000)
        
    def feed_trade(self,trade_data):
        s= time.time()
        
        row, column = self.qty.shape
        ts_index = np.where(self.ts==trade_data.transaction_ms)[0]
        price_index = np.where(self.price==trade_data.price)[0]
        self.count += 1
        self.feed_trade_time1.append((time.time()-s)*1e3)
       
        if ts_index.size and price_index.size:
            s = time.time()
            self.qty[price_index[0]][ts_index[0]] += trade_data.quantity
            self.qty_with_price[price_index[0]] += trade_data.quantity
            self.feed_trade_time2.append((time.time()-s)*1e3)
        elif ts_index.size and not price_index.size:
            s = time.time()
            new = np.zeros((1,column))
            new[0][ts_index[0]] += trade_data.quantity
            self.qty = np.concatenate((self.qty, new), axis=0)
            self.price = np.append(self.price,trade_data.price)
            self.qty_with_price = np.append(self.qty_with_price,trade_data.quantity)
            self.feed_trade_time3.append((time.time()-s)*1e3)
        elif price_index.size and not ts_index.size:
            s = time.time()
            new = np.zeros((row,1))
            new[price_index[0]][0] += trade_data.quantity
            self.qty = np.concatenate((self.qty,new), axis=1)
            self.ts = np.append(self.ts,trade_data.transaction_ms)
            self.qty_with_price[price_index[0]] += trade_data.quantity
            self.feed_trade_time4.append((time.time()-s)*1e3)
        else:
            s = time.time()
            new_column = np.zeros((row,1))
            new_row = np.zeros((1,column+1))
            self.qty = np.concatenate((self.qty, new_column), axis=1)
            self.qty = np.concatenate((self.qty, new_row), axis=0)
            self.qty[-1][-1] += trade_data.quantity
            
            self.price = np.append(self.price,trade_data.price)
            self.ts = np.append(self.ts,trade_data.transaction_ms)
            self.qty_with_price = np.append(self.qty_with_price,trade_data.quantity)
            
            self.feed_trade_time5.append((time.time()-s)*1e3)
            
    def remove_expired_ts(self,ts):
        ts = ts - self.cache_size
        while self.ts.size>0 and self.ts[0] < ts:
            col = self.qty[:,0]
            ind =np.where(col>0)[0]
            if ind.size>0:
                self.qty_with_price[ind] -= col[ind]
            
            self.ts = self.ts[1:]
            self.qty = self.qty[:,1:]
            self.count -= ind.size
            
    def remove_expired_price(self):
        ind = np.where(self.qty_with_price>1e-10)[0]
        self.qty_with_price = self.qty_with_price[ind]
        self.price = self.price[ind]
        self.qty = self.qty[ind]
        
    def get_ask_price_quantile(self, quantile=0.5):
        return DescrStatsW(self.price, weights=self.qty_with_price).quantile(quantile).iloc[0] if self.qty_with_price.size>1 else None

    def get_bid_price_quantile(self, quantile=0.5):
        return DescrStatsW(self.price, weights=self.qty_with_price).quantile(1-quantile).iloc[0] if np.sum(self.qty_with_price) else None
    
    def get_filtered_price(self,qty_filter):
        tmp = self.price[np.where(self.qty_with_price*self.price > qty_filter)[0]]
        return np.min(tmp) if tmp.size else None
    
    def get_trade_count(self):
        return self.count
    
    def get_total_qty(self):
        return np.sum(self.qty_with_price)
    
    def get_shape(self):
        return self.qty.shape
            
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_data = f"binance.btc_usdt_swap.usdt_contract.na"
        self.trade_cache = PriceDistCahce()
        
        self.feed_trade_time = collections.deque(maxlen=10000)
        
        self.trim_time = collections.deque(maxlen=10000)
        self.get_price_time = collections.deque(maxlen=10000)
        
        self.max_price_point = 0
        self.max_ts_point = 0
        
    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol_data)
        
    async def on_public_trade(self, symbol, trade: PublicTrade):
        trade.price = float(trade.price)
        trade.quantity = float(trade.quantity)
        s = time.time()*1e3
        
        # self.logger.info(self.trade_cache.ts.shape)
        # self.logger.info(self.trade_cache.price.shape)
        # self.logger.info(self.trade_cache.qty_with_price.shape)
        # self.logger.info(self.trade_cache.qty.shape)
        
        self.trade_cache.feed_trade(trade)
        self.feed_trade_time.append(time.time()*1e3-s)
        
    async def trim_cache(self):
        while True:
            await asyncio.sleep(0.005)
            s = time.time()*1e3
            self.trade_cache.remove_expired_ts(int(time.time()*1e3))
            self.trim_time.append(time.time()*1e3-s)
            
            self.trade_cache.remove_expired_price()
            
            r,c = self.trade_cache.qty.shape
            self.max_price_point = max(self.max_price_point,r)
            self.max_ts_point = max(self.max_ts_point,c)
            
    async def get_price(self):
        while True:
            await asyncio.sleep(1)
            s = time.time()*1e3
            self.trade_cache.get_ask_price_quantile()
            self.get_price_time.append(time.time()*1e3-s)
            
    async def set_cache(self):
        while True:
            await asyncio.sleep(1)
            await self.redis_set_cache(
                {
                "feed trade time":np.mean(self.feed_trade_time) if self.feed_trade_time else 0,
                "max feed trade time":np.max(self.feed_trade_time) if self.feed_trade_time else 0,
                "feed trade time1":np.mean(self.trade_cache.feed_trade_time1) if self.trade_cache.feed_trade_time1 else 0,
                "max feed trade time1":np.max(self.trade_cache.feed_trade_time1) if self.trade_cache.feed_trade_time1 else 0,
                "feed trade time2":np.mean(self.trade_cache.feed_trade_time2) if self.trade_cache.feed_trade_time2 else 0,
                "max feed trade time2":np.max(self.trade_cache.feed_trade_time2) if self.trade_cache.feed_trade_time2 else 0,
                "feed trade time3":np.mean(self.trade_cache.feed_trade_time3) if self.trade_cache.feed_trade_time3 else 0,
                "max feed trade time3":np.max(self.trade_cache.feed_trade_time3) if self.trade_cache.feed_trade_time3 else 0,
                "feed trade time4":np.mean(self.trade_cache.feed_trade_time4) if self.trade_cache.feed_trade_time4 else 0,
                "max feed trade time4":np.max(self.trade_cache.feed_trade_time4) if self.trade_cache.feed_trade_time4 else 0,
                "feed trade time5":np.mean(self.trade_cache.feed_trade_time5) if self.trade_cache.feed_trade_time5 else 0,
                "max feed trade time5":np.max(self.trade_cache.feed_trade_time5) if self.trade_cache.feed_trade_time5 else 0,
                "trim time":np.mean(self.trim_time) if self.trim_time else 0,
                "max trim time":np.max(self.trim_time) if self.trim_time else 0,
                "max get price time":np.max(self.get_price_time) if self.get_price_time else 0,
                "max price point":self.max_price_point,
                "curr price point":self.trade_cache.get_shape()[0],
                "max ts point":self.max_ts_point,
                "curr ts point":self.trade_cache.get_shape()[1],
                }
            )
            
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache(),
            self.trim_cache(),
            self.get_price()
            )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    