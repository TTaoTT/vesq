import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict


class SpreadCalculator():
    def __init__(self, compound_alpha) -> None:
        self.lam = 1
        self.compound_alpha = compound_alpha
        self.t = 10
    
    def get_h(self,q,keppa):
        w1 = np.exp(-self.compound_alpha*(q**2))
        w2 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.compound_alpha*((q-1)**2))
        w3 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.compound_alpha*((q+1)**2))
        return 1/keppa * np.log(w1 + w2 + w3)
    
    def get_ask(self,inven,keppa):
        return 1/keppa - self.get_h(inven-1,keppa) + self.get_h(inven,keppa)
    
    def get_bid(self,inven,keppa):
        return 1/keppa - self.get_h(inven+1,keppa) + self.get_h(inven,keppa)

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
    
    def get_median_price(self):
        return self.get_ask_price(0.5)

class VolumeCache():
    def __init__(self,cache_size=30*1e3) -> None:
        # No need to convert to int for this cache
        self._cache = []
        self._cache_size = cache_size
        self._amount_sum = Decimal(0)
        
    def feed_trade(self, trade: dict):
        if not self._cache or trade["t"]>= self._cache[-1]["t"]:
            self._cache.append(trade)
        elif trade["t"]<=self._cache[0]["t"]:
            self._cache = [trade] + self._cache
        else:
            i = len(self._cache) - 1
            while self._cache[i]["t"]>trade["t"]:
                i -= 1
            self._cache = self._cache[:i+1] + [trade] + self._cache[i+1:]
            
        self._amount_sum += trade["q"] * trade["p"]
            
    def remove_expired(self, ts):
        if not self._cache:
            return
        elif self._cache[-1]["t"] > ts:
            ts = self._cache[-1]["t"]
            
        while self._cache and self._cache[0]["t"] < ts-self._cache_size:
            old_trade = self._cache.pop(0)
            self._amount_sum -= old_trade["q"] * old_trade["p"]
    
    def get_amount(self):
        return self._amount_sum
    
class OrderCacheManager():
    def __init__(self) -> None:
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.exposed_pos = Decimal(0)
        
    def add_order(self,order):
        self.order_cache[order.client_id] = order
        self.exposed_pos = self.exposed_pos + order.requested_amount if order.side == OrderSide.Buy else self.exposed_pos - order.requested_amount

    def can_send_order(self):
        ask = 0
        bid = 0
        for cid in self.order_cache:
            if self.order_cache[cid].side == OrderSide.Buy:
                bid += 1
            elif self.order_cache[cid].side == OrderSide.Sell:
                ask += 1
        return ask, bid
    
    def after_cancel(self,cid,res):
        if self.order_cache.get(cid) == None:
            return
        o = self.order_cache[cid]
        if o.message["status"] == 0 and res == True and o.message["sent"]:
            self.order_cache[cid].message["status"] = 1
            self.order_cache[cid].message["filled"] = o.requested_amount
            self.pos = self.pos + o.requested_amount if o.side == OrderSide.Buy else self.pos - o.requested_amount
        elif type(res) == dict and res["status"] == "CANCELED" and o.message["status"] == 0:
            self.order_cache[cid].message["status"] = 1
            amount_changed = Decimal(res["executedQty"])
            self.order_cache[cid].message["filled"] = amount_changed
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            canceled = o.requested_amount - amount_changed
            self.exposed_pos = self.exposed_pos - canceled if o.side == OrderSide.Buy else self.exposed_pos + canceled
        else:
            self.order_cache[cid].message["cancel"] += 1
            
    def after_send_ack(self,cid):
        if self.order_cache.get(cid):
            self.order_cache[cid].message["sent"] = True
            
    def order_finish(self,cid,filled):
        # TODO make sure this is wright
        o = self.order_cache[cid]
        if o.message["status"] == 0:
            self.pos = self.pos + filled if o.side == OrderSide.Buy else self.pos - filled
            canceled = o.requested_amount - filled
            self.exposed_pos = self.exposed_pos - canceled if o.side == OrderSide.Buy else self.exposed_pos + canceled
        else:
            if o.message["filled"] != filled:
                amount_changed = filled - o.message["filled"]
                self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
                self.exposed_pos = self.exposed_pos + amount_changed if o.side == OrderSide.Buy else self.exposed_pos - amount_changed
        self.order_cache.pop(cid)

    
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.trade_cache = PriceDistributionCache(100)
        self.sc = SpreadCalculator(self.params["compound_alpha"])
        self.vol_cache = VolumeCache()
        self.a = Decimal(self.params["a"])
        self.b = Decimal(self.params["b"])

        self.recvWindow = 1000
        self.stop_ts = self.params["stop_ts"]
        self.time_gap = self.params["time_gap"]

        self.send_order_queue = Queue()
        self.retreat_queue = Queue()
        self.trade_ts = 0
        self.bbo_ts = 0
        self.pos_limit = Decimal(10)

        self.order_cache = OrderCacheManager()
        self.ask_order_cache = dict()
        self.bid_order_cache = dict()

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False
        
        self.order_count = 0
        self.order_fail_count = 0

        self.delay = 0
        
        self.last_symbol = None
        self.ap = None
        
        self.sending = False

        self.trigger_ts = None
        
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order_inner(o)
        except:
            pass
        
    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.8*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.8*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.8*1200

        if cond1 or cond2 or cond3:
            self.logger.warning(f"{float(headers['X-MBX-USED-WEIGHT-1M'])},{float(headers['X-MBX-ORDER-COUNT-10S'])}, {float(headers['X-MBX-ORDER-COUNT-1M'])}")
            self.hold_back = True
            self.logger.warning("hold back")
            api = self.get_exchange_api("binance")[0]
            self.loop.create_task(api.flash_cancel_orders(self.get_symbol_config(self.symbol)))
            await asyncio.sleep(1)
            self.hold_back = False

    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        # self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})
        
        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.order_cache.pos = position.data["long_qty"] - position.data["short_qty"]
            self.order_cache.exposed_pos = position.data["long_qty"] - position.data["short_qty"]

            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol)
                self.price_tick = symbol_cfg['price_tick_size']
                self.qty_tick = symbol_cfg['qty_tick_size']
                self.half_step_tick = self.qty_tick/Decimal(2)
                self.half_price_tick = self.price_tick/Decimal(2)
                
                c = 0
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ap:
                        self.logger.warning(f"{self.ap}")
                        amount = min(balance,Decimal(self.params["amount"]))
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/self.qty_tick)*self.qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
                        break
                    if c > 1000:
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise

    async def update_amount(self):
        while True:
            await asyncio.sleep(60*60*24)
            try:
                if self.ap:
                    amount = Decimal(self.params["amount"]) / Decimal(self.ap)
                    
                    symbol_cfg = self.get_symbol_config(self.symbol)
                    qty_tick = symbol_cfg["qty_tick_size"]
                    amount = float(int(amount/qty_tick)*qty_tick)
                    
                    self.amount = amount
                    self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")
            
    async def on_order(self, o):
        # self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        client_id = order["c"]
        if client_id not in list(self.order_cache.order_cache):
            return
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        
        if status in OrderStatus.fin_status():
            filled = Decimal(order["z"])
            # if Decimal(order["z"]) > Decimal(0):
                # self.logger.warning(f"order filled at {Decimal(order['ap'])},side:{self.order_cache.order_cache[client_id].side}, filled: {order['z']}")
            self.order_cache.order_finish(client_id,filled)

    async def on_order_inner(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache.order_cache): 
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            filled = order.filled_amount
            # if order.filled_amount > Decimal(0):
                # self.logger.warning(f"order filled at {order.avg_filled_price},side:{self.order_cache.order_cache[client_id].side}, filled: {order.filled_amount}")
            self.order_cache.order_finish(client_id,filled)

    async def on_bbo(self, symbol, bbo):
        if symbol != self.last_symbol:
            self.logger.warning(f"wrong symbol: {symbol}")
        self.last_symbol = symbol
        
        self.ap = Decimal(bbo["data"]["a"])
        self.bp = Decimal(bbo["data"]["b"])
        self.bbo_ts = bbo["data"]["E"]
        
        if self.trigger_ts and (bbo["data"]["E"]+self.time_gap) % (self.stop_ts*1e3) < self.trigger_ts:
            self.send_order_queue.put_nowait(1)
        self.trigger_ts = (bbo["data"]["E"]+self.time_gap) % (self.stop_ts*1e3)
    
    async def on_agg_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        self.trade_ts = max(self.trade_ts, trade["data"]["E"])
        tmp = {
            "t":trade["data"]["E"],
            "p":int(Decimal(trade["data"]["p"])/self.price_tick), 
            "q":int(Decimal(trade["data"]["q"])/self.qty_tick)
            }

        self.trade_cache.feed_trade(tmp)
        self.trade_cache.remove_expired(self.trade_ts)
        self.vol_cache.remove_expired(self.trade_ts)
        self.vol_cache.feed_trade(
            {
            "t":trade["data"]["E"],
            "p":Decimal(trade["data"]["p"]), 
            "q":Decimal(trade["data"]["q"])
            }
        )

    async def send_order_action(self):
        while True:
            await self.send_order_queue.get()
            if not self.send or self.rate_limit:
                continue
            
            spread = float(self.a * np.sqrt(self.vol_cache.get_amount()) + self.b)
            spread = max(spread,0.0002)
            
            cur_risk = self.order_cache.pos/self.amount_decimal
            
            mp = self.trade_cache.get_median_price()
            if not mp:
                mp = (self.ap + self.bp) / 2
            else:
                mp = mp * self.price_tick
                
            ask = mp*(1+Decimal(self.sc.get_ask(float(cur_risk),1/spread)))
            bid = mp*(1-Decimal(self.sc.get_bid(float(cur_risk),1/spread)))
            
            if not self.hold_back:
                self.send_entry_order(ask,bid,self.amount_decimal,cur_risk)
                
    def send_entry_order(self,ask,bid,qty,cur_risk):
        if cur_risk > -self.pos_limit:
            if abs(cur_risk) < 0.999 and cur_risk > 0:
                self.loop.create_task(self.send_order(ask,qty+self.order_cache.pos,OrderSide.Sell))
            else:
                self.loop.create_task(self.send_order(ask,qty,OrderSide.Sell))
        if cur_risk < self.pos_limit:
            if abs(cur_risk) < 0.999 and cur_risk < 0:
                self.loop.create_task(self.send_order(bid,qty-self.order_cache.pos,OrderSide.Buy))
            else:
                self.loop.create_task(self.send_order(bid,qty,OrderSide.Buy))

    async def rate_limit_check(self):
        while True:
            await self.retreat_queue.get()
            if self.rate_limit and self.send:
                self.send = False
                await self.retreat()
                self.send = True
            
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.Limit,recvWindow=None,timestamp=None):
        client_id = ClientIDGenerator.gen_client_id("binance")
        self.order_count += 1
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow
        
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = self.stop_ts
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        extra_info["status"] =  0
        extra_info["filled"] =  Decimal(0)
        extra_info["sent"] = False
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache.add_order(order)
        if timestamp == None:
            timestamp = order.create_ms
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=timestamp,
                recvWindow=recvWindow,
                client_id=client_id
            )
            self.order_cache.after_send_ack(client_id)
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except ExchangeTemporaryError as err:
            await self.internal_fail(client_id, err)
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def cancel_order(self, order: Order, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.json
        except Exception as err:
            if not catch_error:
                raise
            if type(err) in (OrderAlreadyCompletedError, OrderNotFoundError):
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect True")
                return True
            elif type(err) == ExchangeTemporaryError:
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect False")
                return False
            elif isinstance(err, ExchangeConnectorException):
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, raise unknown api error")
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False

    async def batch_cancel(self, oid, order):
        try:
            if self.order_cache.order_cache.get(oid) == None:
                return
            if oid not in self.canceling_id and self.order_cache.order_cache[oid].message["cancel"] < 4:
                self.canceling_id.add(oid)
                res = await self.cancel_order(order)
                self.canceling_id.remove(oid)
                self.order_cache.after_cancel(oid, res)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3*0.2:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
        while True:
            await asyncio.sleep(0.01)
            for oid, order in self.order_cache.order_cache.items():
                self.loop.create_task(batch_cancel(oid, order))
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if not order.message:
                # self.logger.warning(f"order has no message while try to check,{oid}")
                return
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if not order.message["sent"]:
                # self.logger.warning(f"order not sent yet while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*6 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["query"] += 1
                        if self.order_cache.order_cache[oid].message["query"]>10:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order_inner(order_new)
                        else:
                            self.loop.create_task(self.batch_cancel(oid,order_new))
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(self.bp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(self.ap*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None,"fail rate":self.order_fail_count/self.order_count if self.order_count else 0})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(30)
        await asyncio.gather(
            self.update_redis_cache(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            self.update_amount(),
            self.rate_limit_check()
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    