
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.helpers import send_ding_talk
import collections

from decimal import Decimal 
import asyncio
import time
import numpy as np

SYMBOLS = ["eth","sand","mana","1000shib","sol","bnb","xrp","ltc","avax","gala","dot","ada","lrc","doge","ftm","sxp","link","iotx","chr","axs","trx","bat","alice","sfp","crv",
           "enj","matic","fil","zec","chz","storj","luna","egld","qtum","theta","dydx","sushi","bch","eos","atom","vet","lpt","icp","rune","1inch","algo","celr","one",
           "mask","ogn","iota","ankr","ctsi","zen","grt","xlm","aave","skl","near","keep","hot","xtz","btt"]

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # def float2decimal(_dict):
        #     for k, v in _dict.copy().items():
        #         if type(v) == float:
        #             _dict[k] = Decimal(str(v))
        #     return _dict

        # self.params = float2decimal(self.config['strategy']['params'])

        self.symbol_binance = dict()
        self.spread_cache = dict()
        self.trade_vol = dict()
        self.pts = dict()
        self.spread_per_cache = dict()
        self.price_cache = dict()
        self.price = dict()
        self.best_qty = dict()

        self.best_symbol = None
        self.symbol_counter = collections.Counter()

        for key in SYMBOLS:
            self.symbol_binance[key] = f"binance.{key}_usdt_swap.usdt_contract.na"
            symbol_cfg = self.get_symbol_config(self.symbol_binance[key])
            self.pts[key] = symbol_cfg['price_tick_size']
            self.spread_cache[key] = []
            self.spread_per_cache[key] = []
            self.trade_vol[key] = 0
            self.best_qty[key] = []
            
    async def before_strategy_start(self):
        self.logger.setLevel("WARNING")
        for key in list(self.symbol_binance):
            self.direct_subscribe_orderbook(symbol_name=self.symbol_binance[key])
            self.direct_subscribe_public_trade(symbol_name=self.symbol_binance[key])

    async def on_orderbook(self, symbol, orderbook):
        s = symbol.split(".")[1].split("_")[0]
        spread = float(orderbook["asks"][0][0] - orderbook["bids"][0][0])
        mp = float(orderbook["asks"][0][0] + orderbook["bids"][0][0]) / 2
        pts = float(self.pts[s])
        self.spread_cache[s].append(spread/pts)
        self.spread_per_cache[s].append(spread/mp)
        self.price[s] = mp
        if not self.price_cache[s]:
            self.price_cache[s] = mp
        self.best_qty[s].append(float(orderbook["asks"][0][1] + orderbook["bids"][0][1]) * mp)

    async def on_public_trade(self, symbol, trade: PublicTrade):
        s = symbol.split(".")[1].split("_")[0]
        self.trade_vol[s] += float(trade.quantity*trade.price)

    async def cal(self):
        while True:
            await asyncio.sleep(180)
            temp_dict = dict(sorted(self.trade_vol.items(), key=lambda item: item[1]))
            n = []
            s_median = []
            s_mean = []
            s_max = []
            s_per = []
            v = []
            v_w = []
            temp_key = collections.deque(maxlen=5)
            for key in list(temp_dict):
                if np.median(self.spread_cache[key]) >= 2 or key == self.best_symbol and self.price[key] > self.price_cache[key]:
                    temp_key.append(key)
            for key in temp_key:
                n.append(key)
                s_median.append(np.median(self.spread_cache[key]))
                s_mean.append(np.mean(self.spread_cache[key]))
                s_max.append(np.max(self.spread_cache[key]))
                s_per.append(np.median(self.spread_per_cache[key]))
                v.append(self.trade_vol[key]/10000)
                v_w.append(self.trade_vol[key]/np.mean(self.best_qty[key]))
                if self.trade_vol[key]/10000 > 2000:
                    self.symbol_counter[key] += 3
                elif self.trade_vol[key]/10000 > 1000:
                    self.symbol_counter[key] += 2
                else:
                    self.symbol_counter[key] += 1
                
            try:
                self.logger.warning(
                f"""
                ~~~ {n[0]} ====== {n[1]} ===== {n[2]} ==== {n[3]} ==== {n[4]}
                ~~ {round(s_median[0],2)} ~~ {round(s_median[1],2)} ~~ {round(s_median[2],2)} ~~ {round(s_median[3],2)} ~~ {round(s_median[4],2)}
                ~~ {round(s_mean[0],2)} ~~ {round(s_mean[1],2)} ~~ {round(s_mean[2],2)} ~~ {round(s_mean[3],2)} ~~ {round(s_mean[4],2)}
                ~~ {round(s_max[0],2)} ~~ {round(s_max[1],2)} ~~ {round(s_max[2],2)} ~~ {round(s_max[3],2)} ~~ {round(s_max[4],2)}
                ~~ {round(s_per[0],5)} ~~ {round(s_per[1],5)} ~~ {round(s_per[2],5)} ~~ {round(s_per[3],5)} ~~ {round(s_per[4],5)}
                ~~ {round(v[0],2)} ~~ {round(v[1],2)} ~~ {round(v[2],2)} ~~ {round(v[3],2)} ~~ {round(v[4],2)}
                ~~ {round(v_w[0],2)} ~~ {round(v_w[1],2)} ~~ {round(v_w[2],2)} ~~ {round(v_w[3],2)} ~~ {round(v_w[4],2)}
                {self.symbol_counter.most_common(3)}
                """
                )
            except:
                self.logger.warning("not enough candidate")

            if len(n) > 2:
                if not self.best_symbol or self.best_symbol not in n or v[-1] > v[-2]*2:
                    if n[-1] != self.best_symbol:
                        self.best_symbol = n[-1]
                        await send_ding_talk(title="INFO",message="USDT PERP:" + self.best_symbol,
                                             token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3")
                        await self.redis_set_cache({"best symbol": self.best_symbol})

            for key in SYMBOLS:
                self.spread_cache[key] = []
                self.trade_vol[key] = 0
                self.price_cache[key] = None
                self.best_qty[key] = []

    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.cal()
        )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()

   