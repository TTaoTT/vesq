import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self, cache_size=50, dist_cache_size=90,recvWindow=1300,tick_range=1000) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        self.mp = None
        self.dist = SortedDict()
        self.dist_sum_ask = 0
        self.dist_sum_bid = 0
        self.mp_ts = None
        self.dist_cache_size = dist_cache_size*60*1000
        self.dist_cache = []
        self.recvWindow = recvWindow
        self.tick_range = tick_range
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
        
        if self.mp:
            if trade["t"] > self.mp_ts + self.recvWindow:
                self.mp = self.get_ask_price(0.5)
                self.mp_ts = trade["t"]
                if not self.mp:
                    return
                
            diff = trade["p"] - self.mp
            if self.dist.__contains__(diff):
                self.dist[diff] += trade["q"]
            else:
                self.dist[diff] = trade["q"]
            
            if diff> 0:
                self.dist_sum_ask += trade["q"]
            elif diff<0:
                self.dist_sum_bid += trade["q"]
            tmp = {
            "t":trade["t"],
            "diff":diff, 
            "q":trade["q"]
            }
            self.dist_cache.append(tmp)
        else:
            self.mp = self.get_ask_price(0.5)
            self.mp_ts = trade["t"]
        
    def remove_expired(self, ts):
        if not self.cache and not self.dist_cache:
            return
        elif self.cache and self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
            
        while self.dist_cache and self.dist_cache[0]["t"] < ts-self.dist_cache_size:
            old = self.dist_cache.pop(0)
            if self.dist[old["diff"]]<=old["q"]:
                self.dist.pop(old["diff"])
            else:
                self.dist[old["diff"]] -= old["q"]
            
            if old["diff"] > 0:
                self.dist_sum_ask -= old["q"]
            elif old["diff"] < 0:
                self.dist_sum_bid -= old["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_median(self, side):
        q_tmp = 0
        if side == "ask":
            for diff in self.dist:
                if diff <= 0:
                    continue
                q_tmp += self.dist[diff]
                if q_tmp >= self.dist_sum_ask/2:
                    return diff
        elif side == "bid":
            for diff in self.dist.__reversed__():
                if diff >= 0:
                    continue
                q_tmp += self.dist[diff]
                if q_tmp >= self.dist_sum_bid/2:
                    return diff
                
    def get_qty(self):
        return self.q_sum
    
    def get_dist_stats(self):
        s = time.time()
        print(self.dist)
        v0 = self.dist[0]
        ask_dist = SortedDict(filter(lambda x: (x[0] >= 0) & (x[0] < self.tick_range), self.dist.items()))
        bid_dist = SortedDict(filter(lambda x: (x[0] <= 0) & (x[0] > -self.tick_range), self.dist.items()))
        # f_dist = SortedDict(filter(lambda x: x[1]>v0/1000, self.dist.items()))

        x_ask = np.array(ask_dist.keys())
        y_ask = np.array(np.log(ask_dist.values()))
        A_ask = np.vstack([x_ask, np.ones(len(x_ask))]).T
        ask_keppa, _ = np.linalg.lstsq(A_ask, y_ask, rcond=None)[0]

        x_bid = np.array(bid_dist.keys())
        y_bid = np.array(np.log(bid_dist.values()))
        A_bid = np.vstack([x_bid, np.ones(len(x_bid))]).T
        bid_keppa, _ = np.linalg.lstsq(A_bid, y_bid, rcond=None)[0]
        
        # x = np.array(f_dist.keys())
        # y = np.array(np.log(f_dist.values()))
        # A = np.vstack([x, np.ones(len(x))]).T
        # keppa, lnlambda = np.linalg.lstsq(A, y, rcond=None)[0]
        return ask_keppa, bid_keppa, time.time()-s
    
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.use_raw_stream = True
        
        self.price_multi = 1e7
        self.qty_multi = 1e7
        
        self.params = self.config['strategy']['params']
        self.trade_buy = PriceDistributionCache(
            cache_size=self.params["cache_size"],
            dist_cache_size=self.params["min"],
            recvWindow=self.params["recvWindow"],
            tick_range=self.params["tick_range"]
            )
        self.trade_sell = PriceDistributionCache(
            cache_size=self.params["cache_size"],
            dist_cache_size=self.params["min"],
            recvWindow=self.params["recvWindow"],
            tick_range=self.params["tick_range"]
            )
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        
    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        # self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        
        symbol_cfg = self.get_symbol_config(self.symbol)
        self.price_tick = symbol_cfg['price_tick_size']
        self.qty_tick = symbol_cfg['qty_tick_size']
        
    async def on_agg_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        tmp = {
            "t":trade["data"]["E"],
            "p":int(Decimal(trade["data"]["p"])/self.price_tick), 
            "q":int(Decimal(trade["data"]["q"])/self.qty_tick)
            }
        if trade["data"]["m"]:
            self.trade_sell.feed_trade(tmp)
        else:
            self.trade_buy.feed_trade(tmp)
            
        self.trade_sell.remove_expired(trade["data"]["E"])
        self.trade_buy.remove_expired(trade["data"]["E"])
        
    async def main_logic(self):
        while True:
            await asyncio.sleep(60)
            _, bid_keppa,t1 = self.trade_sell.get_dist_stats()
            ask_keppa, _,t2 = self.trade_buy.get_dist_stats()
            self.logger.warning(f"""
            ask_keppa:{ask_keppa} , bid_keppa:{bid_keppa}
            calculation takes: {t1+t2}s
            """)

    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(1)
        await asyncio.gather(
            self.main_logic()
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
