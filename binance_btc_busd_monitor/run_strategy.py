import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections


from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False
        self.symbol = "binance.btc_busd.spot.na"
        self.use_raw_stream = True
        self.buy_amount = 0
        self.sell_amount = 0
        self.buy_qty = 0
        self.sell_qty = 0

    async def before_strategy_start(self):
        self.logger.setLevel("WARNING")
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
    async def on_order(self, order):
        if order["e"] != "executionReport":
            return
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        if status in OrderStatus.fin_status():
            if order["S"] == "BUY":
                self.buy_amount += float(order["Z"])
                self.buy_qty += float(order["z"])
            else:
                self.sell_amount += float(order["Z"])
                self.sell_qty += float(order["z"])
                
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            try:
                data = dict()
                data["buy_price"] = self.buy_amount / self.buy_qty
                data["sell_price"] = self.sell_amount / self.sell_qty
                data["buy_amount"] = self.buy_amount
                data["sell_amount"] = self.sell_amount
                    
                await self.redis_set_cache(data)
                    
            except Exception as err:
                self.logger.warning(f"set cahce err:{err}")
    
            
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache(),
            )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    