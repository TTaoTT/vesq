from dataclasses import dataclass
import logging
import asyncio
from decimal import Decimal
import time
import trace
import pandas as pd
from asyncio.queues import Queue, LifoQueue
import os
import numpy as np
import traceback
import math
from typing import Dict, Set, List, AnyStr, Union, Tuple
import ujson as json
from orderedset import OrderedSet

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.order import PartialOrder
from atom.model.depth import Depth as AtomDepth
from strategy_base.base import CommonStrategy

from xex_mm.utils.base import Trade, Depth, BBO, Direction, TransOrder, OrderManager, MakerTaker, ReferencePriceCalculator
# from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig


from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard
from xex_mm.strategy_configs.xex_mm_config import PredRefPrcMode
from xex_mm.xex_executor.order_executor import OrderExecutorTest2

from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model_no_partial_fill import SingleArrivalRateXExMmModelNoPartialFill

price_precision = {
    'binance.btc_usdt': 2,
    'binance.btc_usdt_swap': 1,
    'binance.eth_usdt_swap': 2,
    'binance.axs_usdt_swap': 2,
    'binance.doge_usdt_swap': 5,
    
    "binance.eth_usdt_swap":2,
    "binance.luna2_usdt_swap":4,
    "binance.etc_usdt_swap":3,
    "binance.sol_usdt_swap":2,
    "binance.reef_usdt_swap":6,
    "binance.rvn_usdt_swap":5,
    
    "binance.btc_busd_swap":1,
    "binance.eth_busd_swap":2,
    "binance.luna2_busd_swap":4,
    "binance.1000lunc_busd_swap":4,

    'mexc.btc_usdt': 2,
    'mexc.eth_usdt_swap': 2,
    'mexc.axs_usdt_swap': 5,

    "gateio.btc_usdt_swap": 1,

    "okex.btc_usdt": 1,
    "okex.btc_usdt_swap": 1,

    "woo.btc_usdt_swap": 1,

    "crypto.btc_usdt_swap": 2,
    
    "jojo.btc_usdt_swap": 2
}

class FeeStructure:
    def __init__(self, maker: float, taker: float) -> None:
        self.maker = maker
        self.taker = taker
        
class FeeConfig:
    def __init__(self) -> None:
        self.config = {
            # BINANCE
            "binance.btc_usdt": FeeStructure(maker=0, taker=0),
            "binance.axs_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.luna_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.btc_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.eth_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.luna2_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.etc_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.sol_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.reef_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.rvn_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            
            "binance.btc_busd_swap":FeeStructure(maker=-0.00014, taker=0.0003),
            "binance.eth_busd_swap":FeeStructure(maker=-0.00014, taker=0.0003),
            "binance.luna2_busd_swap":FeeStructure(maker=-0.00014, taker=0.0003),
            "binance.1000lunc_busd_swap":FeeStructure(maker=-0.00014, taker=0.0003),
            
            # MEXC
            "mexc.axs_usdt_swap":FeeStructure(maker=-0.0001, taker=0.0001),
            "mexc.luna_usdt_swap":FeeStructure(maker=-0.0001, taker=0.0001),
            "mexc.btc_usdt":FeeStructure(maker=-0.0001, taker=0.0005),
            # WOO
            "woo.btc_usdt_swap": FeeStructure(maker=0, taker=0),
            # GATEIO
            "gateio.btc_usdt_swap": FeeStructure(maker=-0.000125, taker=0.000225),  # VIP MM2
            # OKEX
            "okex.btc_usdt": FeeStructure(maker=0, taker=0.0002), # TODO maker sure this is right
            "okex.btc_usdt_swap": FeeStructure(maker=0, taker=0.0002),  # vip5 ???
            # CRYPTO, same for all token pairs
            "crypto.btc_usdt": FeeStructure(maker=-0.00003, taker=0.00025),
            "crypto.btc_usdt_swap": FeeStructure(maker=-0.00002, taker=0.00015),
            # JOJO
            "jojo.btc_usdt_swap": FeeStructure(maker=0.0002, taker=0),

        }
    
    def get_fee(self, ex: str, pair: str):
        return self.config[f"{ex}.{pair}"].maker, self.config[f"{ex}.{pair}"].taker
    
qty_precision = {
    'binance.btc_usdt': 5,
    'binance.btc_usdt_swap': 3,
    'binance.eth_usdt_swap': 3,
    'binance.axs_usdt_swap': 0,
    'binance.doge_usdt_swap': 1,
    
    "binance.eth_usdt_swap":3,
    "binance.luna2_usdt_swap":0,
    "binance.etc_usdt_swap":2,
    "binance.sol_usdt_swap":3,
    "binance.reef_usdt_swap":1,
    "binance.rvn_usdt_swap":0,
    
    "binance.btc_busd_swap":3,
    "binance.eth_busd_swap":3,
    "binance.luna2_busd_swap":0,
    "binance.1000lunc_busd_swap":0,

    'mexc.btc_usdt': 6,
    'mexc.eth_usdt_swap': 2,
    'mexc.axs_usdt_swap': 0,

    "gateio.btc_usdt_swap": 4,

    "okex.btc_usdt": 8,
    "okex.btc_usdt_swap": 3,

    "woo.btc_usdt_swap": 3,

    "crypto.btc_usdt_swap": 6,
    
    "jojo.btc_usdt_swap": 6
}

@dataclass
class Config:
    SYMBOL: str
    EXCHANGE: str
    ART: float
    SIGMAT: float
    HST: float
    RETT: float
    REFPRCT: float
    RESET_RET: float
    LATENCYT: float
    TAU: float
    T_F: float
    BETA: float
    BBO_SWITCH: bool
    FOLDER_NAME: str
    GRANUALITY: float
    TOLERANCE_ARR: List
    U_LB_MULTIPLIERS: List[float]
    TRADE_DEFENSIVE_CANCEL_AR_MULTIPLIER: float
    DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER: float
    DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER: float
    QTY_UB_ARR: List
    MODEL_FILEPATH: str
    PRED_ON: bool
    PRED_REF_PRC_MODE: PredRefPrcMode
    MSG: str

class OrderExecutorTest2(OrderExecutorTest2):
    def _setup_mm_model(self,contract):
        self.mm_mdl = SingleArrivalRateXExMmModelNoPartialFill(
            fee_rate_map=dict(
                {contract:dict(
                    binance={
                        MakerTaker.maker: FeeConfig().config[f"binance.{contract}"].maker,
                        MakerTaker.taker: FeeConfig().config[f"binance.{contract}"].taker,
                    },
                )}
            ),
            q_grp_size_ccy2_map=self.q_grp_size_map_ccy2
        )
    
    def _update_prediction(self, contract: str, ref_prc: float, ts: int, tau: float):

        if self.pred_on:
            pred_lar = self.signal_calculator.predict_long_arrival_rate(ts=ts)
            pred_sar = self.signal_calculator.predict_short_arrival_rate(ts=ts)
            pred_lhs = self.signal_calculator.predict_long_half_spread(ts=ts)
            pred_shs = self.signal_calculator.predict_short_half_spread(ts=ts)
            pred_sigma = self.signal_calculator.predict_sigma(ts=ts)
            if not self.signal_calculator.ref_prc_ret_mdl_ready():
                # logging.warning(f" Reference price model not ready!")
                return
            pred_ref_prc_ret = self.signal_calculator.predict_ref_prc_ret(ts=ts)
            pred_ref_prc = ref_prc*(1+pred_ref_prc_ret * tau)
            pred_latency = self.signal_calculator.pred_latency(ts=ts)
        else:
            xex = "xex"
            if not self.mm_mdl.has_depth_metrics(contract=contract, ex=xex):
                return

            pred_sigma = self.signal_calculator.curr_ema_sigma(ts=ts)
            if pred_sigma is None:
                return
            pred_sigma = max(pred_sigma, 1e-10)

            b_s = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_s")
            lambda_l_lb = b_s * self.mm_mdl._get_q_grp_size_ccy1(contract=contract) / tau
            pred_lar = self.signal_calculator.curr_emavg_long_arrival_rate(ts=ts)
            if pred_lar is None:
                return
            pred_lar = max(lambda_l_lb, pred_lar)
            # pred_lar = self.signal_calculator.curr_emavg_long_arrival_rate(ts=ts)

            b_l = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_l")
            lambda_s_lb = b_l * self.mm_mdl._get_q_grp_size_ccy1(contract=contract) / tau 
            pred_sar = self.signal_calculator.curr_emavg_short_arrival_rate(ts=ts) 
            if pred_sar is None:
                return
            pred_sar = max(lambda_s_lb, pred_sar)
            # pred_sar = self.signal_calculator.curr_emavg_short_arrival_rate(ts=ts)  # num tokens

            pred_lhs = self.signal_calculator.curr_emavg_long_half_spread(ts=ts)
            if pred_lhs is None:
                return
            pred_shs = self.signal_calculator.curr_emavg_short_half_spread(ts=ts)
            if pred_shs is None:
                return
            # pred_ref_prc_ret = 0
            # pred_ref_prc = ref_prc
            pred_ref_prc_ret = self.signal_calculator.curr_emavg_ref_prc_ret(ts=ts)
            if pred_ref_prc_ret is None:
                pred_ref_prc_ret = 0
            if self._pred_ref_prc_mode == PredRefPrcMode.neutral:
                pred_ref_prc = ref_prc
            elif self._pred_ref_prc_mode == PredRefPrcMode.ret:
                pred_ref_prc = ref_prc * (1 + pred_ref_prc_ret * tau)
            elif self._pred_ref_prc_mode == PredRefPrcMode.prc:
                pred_ref_prc = self.signal_calculator.curr_emavg_ref_prc(ts=ts)
            else:
                raise NotImplementedError()
            pred_latency = self.signal_calculator.curr_emavg_latency(ts=ts)

        self.mm_mdl.update_pred_arrival_rate(
            contract=contract, ex="xex", d=Direction.long,
            pred=pred_lar)
        self.mm_mdl.update_pred_arrival_rate(
            contract=contract, ex="xex", d=Direction.short,
            pred=pred_sar)
        self.mm_mdl.update_pred_sigma(
            contract=contract, ex="xex",
            pred=pred_sigma)
        self.mm_mdl.update_pred_half_spread(
            contract=contract, ex="xex", d=Direction.long,
            pred=pred_lhs)
        self.mm_mdl.update_pred_half_spread(
            contract=contract, ex="xex", d=Direction.short,
            pred=pred_shs)
        self.mm_mdl.update_pred_ref_prc(
            contract=contract, ex="xex",
            pred=pred_ref_prc)
        self.mm_mdl.update_pred_latency(
            contract=contract, ex="xex",
            pred_latency=pred_latency
        )

        return None not in {pred_lar, pred_sar, pred_lhs, pred_shs, pred_sigma, pred_ref_prc_ret}
    
def setup_q(ret_quantile_arr):
    q_arr = []
    for i, quantile in enumerate(ret_quantile_arr):
        q_arr.append(1)
    return q_arr

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class LoadBalancer:
    def __init__(self, max_leverage: float) -> None:
        self._position_map = dict()
        self._balance_map = dict()
        self._account_load_map = dict()
        self._ip_load_map = dict()
        
        self.max_leverage = max_leverage
    
    def setup(self, acc_list: list, trader_list: list):
        acc_list = acc_list.copy()
        trader_list = trader_list.copy()
        for acc in acc_list:
            self._position_map[acc] = 0
            self._balance_map[acc] = 0
            self._account_load_map[acc] = 0
        
        for trader in trader_list:
            self._ip_load_map[trader] = 0
        
    
    def get_acc_list(self):
        return sorted(self._account_load_map, key=self._account_load_map.get)
        
    def get_trader_list(self):
        return sorted(self._ip_load_map, key=self._ip_load_map.get)
    
    def get_trader(self):
        return self.get_trader_list()[0]
    
    def update_balance(self, acc:str, balance:float):
        self._balance_map[acc] = balance
    
    def update_position(self, acc: str, chg_qty: float):
        self._position_map[acc] += chg_qty
    
    def volume_allowed(self, acc: str, side: Direction, prc: float):
        if side == Direction.long:
            return self._balance_map[acc] * self.max_leverage / prc - self._position_map[acc]
        else:
            return self._balance_map[acc] * self.max_leverage / prc + self._position_map[acc]
    
    def update_limit(self, acc: str, trader_name: str, acc_load: float, ip_load: float):
        self._account_load_map[acc] = acc_load
        self._ip_load_map[trader_name] = ip_load
    
    def reduce_count(self, reduce: float):
        self._account_load_map[self.get_acc_list()[-1]] -= reduce
        self._ip_load_map[self.get_trader_list()[-1]] -= reduce
        

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.start_ts = time.time()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.stats = int(self.config['strategy']['params']['stats'])
        self.layer = int(self.config['strategy']['params']['layer'])
        self.beta = int(self.config['strategy']['params']['beta'])
        self.sigma_base = int(self.config['strategy']['params']['sigma_base'])
        self.multi = int(self.config['strategy']['params']['multi'])
        self.quantity_base = int(self.config['strategy']['params']['quantity_base'])
        max_leverage = int(self.config['strategy']['params']['max_leverage'])
        self.bnd_amt_in_coin = Decimal(self.config['strategy']['params']['bnd_amt_in_coin'])
        
        self.genearte_lt = 0
        self._order_manager = OrderManager()
        self.load_balancer = LoadBalancer(max_leverage=max_leverage)
        # self.prc_per_diff = 0.00005 / 5
        self._ref_prc_cal = ReferencePriceCalculator()
        self.send = True
        self.bbo_index =0
        self._n_def_cancel = 0
        self._prev_bbo: BBO = ...

        self.pair: str = ...
        self.symbols: List[str] = ...

        self._prev_depth_metrics: Dict = dict()
        self._ref_prc_map: Dict = dict()
        self._ret_map: Dict = dict()
        
        self._order_stats = dict()
        self._order_stats["buy_amt"] = 0
        self._order_stats["buy_qty"] = 0
        self._order_stats["sell_amt"] = 0
        self._order_stats["sell_qty"] = 0
        
        self.canceling_id = set()
        
        self.send = False
        
        self.trd_amt= 0 
        
        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()
        
        self.ob_queue = LifoQueue()
        self.bbo_queue = LifoQueue()
        
        self.cfg: Config = Config(
            SYMBOL=self.contract,
            EXCHANGE="binance",
            ART=100,
            SIGMAT=1000,
            HST=1000,
            RETT=1000,
            REFPRCT=1e3,
            RESET_RET=1e-4,
            LATENCYT=1000,
            TAU=500,
            T_F=500,
            BETA=self.beta,
            BBO_SWITCH=True,
            FOLDER_NAME="",
            GRANUALITY=1000,
            TRADE_DEFENSIVE_CANCEL_AR_MULTIPLIER=self.multi,
            DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER=self.multi,
            DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER=self.multi,
            U_LB_MULTIPLIERS=[self.sigma_base + i for i in range(self.layer)],
            TOLERANCE_ARR=[1 for _ in range(self.layer)],
            QTY_UB_ARR=[self.quantity_base for _ in range(self.layer)],
            MODEL_FILEPATH="",
            PRED_ON=False,
            PRED_REF_PRC_MODE=PredRefPrcMode.neutral,
            MSG="",
        )

        symbols = [f"{self.cfg.EXCHANGE}.{self.cfg.SYMBOL}"]
        q_grp_size_map = {self.cfg.SYMBOL: self.cfg.GRANUALITY}
        self.executor = OrderExecutorTest2(
            iids=symbols,
            contract=self.cfg.SYMBOL,
            q_grp_size_map_ccy2=q_grp_size_map,
            amt_bnd=0,  # This is handled when building depth
            pred_on=self.cfg.PRED_ON,
            art=self.cfg.ART,
            sigmat=self.cfg.SIGMAT,
            hst=self.cfg.HST,
            rett=self.cfg.RETT,
            refprct=self.cfg.REFPRCT,
            reset_ret=self.cfg.RESET_RET,
            latencyt=self.cfg.LATENCYT,
            q_ubs=self.cfg.QTY_UB_ARR,
            pred_ref_prc_mode=self.cfg.PRED_REF_PRC_MODE,
            U_lb_multipliers=self.cfg.U_LB_MULTIPLIERS,
        )

        self.precision = price_precision[symbols[0]]
    
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        
    async def before_strategy_start(self):
        symbol = f"{self.cfg.EXCHANGE}.{self.cfg.SYMBOL}.{self.market}.{self.sub_market}"
        symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
        bnd = self.bnd_amt_in_coin / symbol_cfg["contract_value"]
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=symbol,account_name=acc)
            await asyncio.sleep(1)
            
        self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
        self.direct_subscribe_agg_trade(symbol_name=symbol)
        self.direct_subscribe_bbo(symbol_name=symbol)
        
        self.loop.create_task(self.handle_ob_update())
        
        ch1 = "mq:hf_signal:order_response"
        ch2 = "mq:hf_signal:cancel_response"
        self.loop.create_task(self.subscribe_to_channel([ch1],self.process_order_response))
        self.loop.create_task(self.subscribe_to_channel([ch2],self.process_cancel_response))
        
        for acc in self.account_names:
            self._account_config_[acc]["cfg_pos_mode"][symbol] = 1
            
        self.load_balancer.setup(
            acc_list=self.account_names,
            trader_list=["hf1_binance_swap", "hf2_binance_swap","hf3_binance_swap","hf4_binance_swap"]
        )
        
        counter = 0
        while True:
            await asyncio.sleep(1)
            counter += 1
            if self._ref_prc_map.get((self.cfg.SYMBOL, self.cfg.EXCHANGE)):
                await self.rebalance()
                break
            if counter > 100:
                self.logger.critical("no price info coming")
                exit()
        
        
        self.logger.critical("start......")
        
        self.send = True
            
    async def on_order(self, order: PartialOrder):
        if not self._order_manager.get(order.client_id):
            return
        try:
            order_status = order.xchg_status
            oid = order.client_id
            o: TransOrder = self._order_manager.get(oid)
            if not o:
                return
            if order_status in OrderStatus.fin_status():
                if order.filled_amount > 0:
                    _, contract = o.iid.split(".")
                    chg_qty = float(order.filled_amount) if o.side == Direction.long else -float(order.filled_amount)
                    self.executor.update_finished_order(
                        iid=o.iid, 
                        chg_qty=chg_qty
                        )
                    self.executor.update_finished_order(
                        iid=f"xex.{contract}", 
                        chg_qty=chg_qty
                        )
                    self.load_balancer.update_position(acc=order.account_name, chg_qty=chg_qty)
                    if o.side == Direction.long:
                        self._order_stats["buy_amt"] += order.avg_filled_price * order.filled_amount
                        self._order_stats["buy_qty"] += order.filled_amount
                    else:
                        self._order_stats["sell_amt"] += order.avg_filled_price * order.filled_amount
                        self._order_stats["sell_qty"] += order.filled_amount
                    self.trd_amt += abs(order.filled_amount) * order.avg_filled_price
                self._order_manager.pop(oid=oid)
        except Exception as err:
            self.logger.critical(f"handle on_order err: {err}")
            traceback.print_exc()
    
    async def on_agg_trade(self, symbol, trade: PublicTrade):
        try:
            iid = symbol
            ex, contract = iid.split(".")
            t = Trade(
                ex=ex,
                contract=contract,
                price=float(trade.price),
                quantity=float(trade.quantity),
                tx_time=trade.transaction_ms,
                local_time=trade.local_ms,
                side=1 if trade.side == OrderSide.Buy else -1,
                mexc_side=None,
                uid=-1
            )

            """ Latency state """
            latency = int(time.time()*1e3) - t.tx_time
            self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

            self.executor.update_trade(t)

            self.defensive_cancel_orders_on_trade(contract=contract, ex=ex, trd=t)
        except Exception as err:
            traceback.print_exc()
            
    async def internal_fail(self,client_id,acc,err=None):
        try:
            self.logger.critical(f"failed order: {err}, acc:{acc}, client_id={client_id}")
            if not self._order_manager.get(client_id):
                return
            o = self._order_manager.at(client_id)
            order = PartialOrder(
                account_name=o.account_name,
                xchg_id=None,
                exchange_pair="",
                client_id=client_id,
                xchg_status=OrderStatus.Failed,
                filled_amount=0,
                avg_filled_price=0,
                commission_fee=0,
                local_ms=int(time.time() * 1e3),
                server_ms=int(time.time() * 1e3),
                )
            await self.on_order(order)
        except:
            pass
            
    def defensive_cancel_orders_on_trade(self, contract: str, ex: str, trd: Trade):

        """ SEx or XEx? That is the question. """

        xex = "xex"

        mm_mdl = self.executor.mm_mdl
        sig_cal = self.executor.signal_calculator
        if mm_mdl.has_depth_metrics(contract=contract, ex=xex):
            if trd.side == Direction.long:
                pred_ar = sig_cal.predict_long_arrival_rate(ts=trd.local_time)
                curr_ar = sig_cal.curr_impulse_long_arrival_rate(ts=trd.local_time)
                avg_ar = sig_cal.curr_emavg_long_arrival_rate(ts=trd.local_time)
            else:
                pred_ar = sig_cal.predict_short_arrival_rate(ts=trd.local_time)
                curr_ar = sig_cal.curr_impulse_short_arrival_rate(ts=trd.local_time)
                avg_ar = sig_cal.curr_emavg_short_arrival_rate(ts=trd.local_time)
            ar = max(pred_ar, curr_ar)
            high_ar_bnd = avg_ar  + math.sqrt(abs(avg_ar)) * self.cfg.TRADE_DEFENSIVE_CANCEL_AR_MULTIPLIER
            if ar > high_ar_bnd:
                tilde_p = mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
                ask_prc_touch_line = tilde_p
                bid_prc_touch_line = tilde_p
                tau = self.cfg.TAU

                if trd.side == Direction.short:
                    ar *= tau
                    a_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_l")
                    b_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_l")
                    delta_l_touch_line = (ar + self.executor.mm_mdl._epsilon - b_l) / max(1e-6, a_l)
                    s_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_l")
                    bid_prc_touch_line = max(tilde_p - s_l - delta_l_touch_line, 0)

                if trd.side == Direction.long:
                    ar *= tau
                    a_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_s")
                    b_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_s")
                    s_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_s")
                    delta_s_touch_line = (ar + self.executor.mm_mdl._epsilon - b_s) / max(1e-6, a_s)
                    ask_prc_touch_line = tilde_p + s_s + delta_s_touch_line

                I = 0
                self.defensive_cancel_orders(ask_touch_line=ask_prc_touch_line, bid_touch_line=bid_prc_touch_line, I=I)
                self._n_def_cancel += 1
                
    def _update_latency(self, iid: str, latency: float, allow_rectivate: bool):
        ism = self.iid_state_manager
        is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
        is_active = ism.is_active(iid=iid)
        if not is_in_high_latency_state:
            if ism.is_high_latency(iid=iid, latency=latency):
                ism.enter_high_latency_state(iid=iid)
                if is_active:
                    self.deactivate(iid=iid)
        else:  # is_in_high_latency_state
            if not ism.is_high_latency(iid=iid, latency=latency):
                if allow_rectivate:
                    ism.enter_normal_latency_state(iid=iid)
    
    async def on_bbo(self, symbol, bbo):
        try:
            await self.bbo_queue.put((symbol, bbo))
        except:
            traceback.print_exc()
            
    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            await self.ob_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")
            
    async def handle_ob_update(self):
        while True:
            try:
                if self.bbo_queue.empty() and self.ob_queue.empty():
                    await asyncio.sleep(0.001)
                    continue
                elif not self.ob_queue.empty():
                    symbol, orderbook = await self.ob_queue.get()
                    self.ob_queue = LifoQueue()
                    message_type = "ob"
                else:
                    symbol, bbo = await self.bbo_queue.get()
                    self.bbo_queue = LifoQueue()
                    message_type = "bbo"
                    
                if message_type == "ob":
                    s = time.time()*1e3
                    iid = symbol
                    ex, contract = symbol.split(".")
                    ccy = contract.split("_")
                    depth_obj:Depth = Depth.from_dict(
                        depth=orderbook,
                        ccy1=ccy[0],
                        ccy2=ccy[1],
                        ex=ex,
                        contract=contract
                        )
                    ex = depth_obj.meta_data.ex
                    contract = depth_obj.meta_data.contract
                    server_ts = depth_obj.server_ts
                    local_ts = depth_obj.resp_ts

                    """ Latency state """
                    latency = int(time.time()*1e3) - depth_obj.server_ts
                    self._update_latency(iid=iid, latency=latency, allow_rectivate=True)

                    """ tx time guard """
                    if self.server_time_guard.has_last_server_time(iid=iid):
                        last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                        if server_ts < last_server_time:
                            continue
                    self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

                    ism = self.iid_state_manager
                    """ Activate stale state """
                    is_in_stale_state = ism.is_in_stale_state(iid=iid)
                    if is_in_stale_state:
                        ism.enter_nonstale_state(iid=iid)
                    ism.update_heart_beat_time(iid=iid, ts=int(time.time()*1e3))

                    """ Check stale """
                    for nsiids in ism.nonstale_iids:
                        is_active = ism.is_active(iid=nsiids)
                        if ism.is_stale(iid=nsiids, ts=int(time.time()*1e3)):
                            ism.enter_stale_state(iid=nsiids)
                            if is_active:
                                self.deactivate(iid=nsiids)

                    if not ism.active_iids:
                        continue

                    """ Record prev depth metrics """
                    xex = "xex"
                    ex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    xex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=xex)
                    if ex_has_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=ex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=ex))
                    if xex_has_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=xex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=xex))

                    """ Update depth metrics """
                    self.executor.update_depth(iid, depth_obj)

                    """ Compute and record ref prcs and returns """
                    self._update_ref_prc(contract=contract, ex=ex,
                                        ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="tilde_p"))
                    self._update_ref_prc(contract=contract, ex=xex,
                                        ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p"))
                    if ex_has_depth_metrics:
                        _prev_ex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=ex, metric_name="tilde_p")
                        ex_ref_prc = self._get_ref_prc(contract=contract, ex=ex)
                        self._update_ret(contract=contract, ex=ex,
                                        ret=math.log(ex_ref_prc / _prev_ex_ref_prc))
                    if xex_has_depth_metrics:
                        _prev_xex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=xex, metric_name="tilde_p")
                        xex_ref_prc = self._get_ref_prc(contract=contract, ex=xex)
                        self._update_ret(contract=contract, ex=xex,
                                        ret=math.log(xex_ref_prc / _prev_xex_ref_prc))
                    
                    self.generate_orders(ref_prc=self._get_ref_prc(contract=contract, ex=xex))
                    # self.cancel_orders_action()
                    if time.time()*1e3 - s > 100:
                        self.logger.critical(f"orderbook handling time: {time.time()*1e3 - s}")
                    
                elif message_type == "bbo":
                    s = time.time()*1e3
                    iid = symbol
                    bbo_obj = BBO.from_dict_prod(iid=symbol, bbo_depth=bbo)

                    if not bbo_obj.valid_bbo:
                        continue

                    contract = bbo_obj.meta.contract
                    ex = bbo_obj.meta.ex
                    server_ts = bbo_obj.server_time
                    local_ts = bbo_obj.local_time

                    ism = self.iid_state_manager
                    ism.update_heart_beat_time(iid=iid, ts=local_ts)

                    """ Latency state """
                    latency = int(time.time()*1e3) - bbo_obj.server_time
                    self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

                    """ Server time guard """
                    if self.server_time_guard.has_last_server_time(iid=iid):
                        last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                        if server_ts < last_server_time:
                            continue
                    self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

                    """ Check stale """
                    for nsiids in ism.nonstale_iids:
                        is_active = ism.is_active(iid=nsiids)
                        if ism.is_stale(iid=nsiids, ts=int(time.time()*1e3)):
                            ism.enter_stale_state(iid=nsiids)
                            if is_active:
                                self.deactivate(iid=nsiids)

                    if not ism.is_active(iid=iid):
                        continue

                    if self._prev_bbo is not Ellipsis:
                        bid_prc_chged = abs(math.log(bbo_obj.best_bid_prc / self._prev_bbo.best_bid_prc)) > 1e-6
                        ask_prc_chged = abs(math.log(bbo_obj.best_ask_prc / self._prev_bbo.best_ask_prc)) > 1e-6
                        if not ask_prc_chged and not bid_prc_chged:
                            continue

                    if not self.executor._xex_depth_map[contract].contains(ex=ex):
                        continue

                    """ Record prev depth metrics """
                    xex = "xex"
                    ex_had_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    xex_had_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=xex)
                    if ex_had_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=ex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=ex))

                    if xex_had_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=xex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=xex))

                    """ Update depth metrics """
                    self.executor.update_bbo(bbo_obj)

                    """ Compute and record ref prcs and returns """
                    ex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    if ex_has_depth_metrics:
                        self._update_ref_prc(contract=contract, ex=ex,
                                            ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="tilde_p"))
                    xex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    if xex_has_depth_metrics:
                        self._update_ref_prc(contract=contract, ex=xex,
                                            ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p"))
                    if ex_had_depth_metrics and ex_has_depth_metrics:
                        _prev_ex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=ex, metric_name="tilde_p")
                        self._update_ret(contract=contract, ex=ex,
                                        ret=math.log(self._get_ref_prc(contract=contract, ex=ex) / _prev_ex_ref_prc))
                    if xex_had_depth_metrics and xex_has_depth_metrics:
                        _prev_xex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=xex, metric_name="tilde_p")
                        self._update_ret(contract=contract, ex=xex,
                                        ret=math.log(self._get_ref_prc(contract=contract, ex=xex) / _prev_xex_ref_prc))

                    """ Full recalc of all optimal quotes is slow. """
                    # self.generate_orders(self._ref_prc)
                    # self.cancel_orders_action()

                    """ Defensive cancel is fast. """
                    ex, contract = iid.split(".")
                    self.defensive_cancel_orders_on_bbo(contract=contract, ex=ex, bbo=bbo_obj)
                    self._prev_bbo = bbo_obj
                    if time.time()*1e3 - s > 100:
                        self.logger.critical(f"bbo handling time: {time.time()*1e3 - s}")
            except Exception as err:
                traceback.print_exc()
        
                
    def defensive_cancel_orders_on_bbo(self, contract: str, ex: str, bbo: BBO):

        """ SEx or XEx? That is the question. """

        xex = "xex"

        mm_mdl = self.executor.mm_mdl
        sig_cal = self.executor.signal_calculator
        ts = bbo.local_time

        if mm_mdl.has_depth_metrics(contract=contract, ex=ex):

            tilde_p = mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")

            pred_ret = sig_cal.predict_ref_prc_ret(ts=ts)
            if pred_ret is None:
                pred_ret = 0
            pred_long_half_sprd = sig_cal.predict_long_half_spread(ts=ts)
            pred_short_half_sprd = sig_cal.predict_short_half_spread(ts=ts)
            pred_ref_prc = tilde_p * (1 + pred_ret * self.cfg.TAU)
            pred_bid_touch_line = pred_ref_prc - pred_long_half_sprd
            pred_ask_touch_line = pred_ref_prc + pred_short_half_sprd

            curr_ret = sig_cal.curr_impulse_ref_prc_ret(ts=ts)
            if curr_ret is None:
                curr_ret = 0
            curr_long_half_sprd = sig_cal.curr_impulse_long_half_spread(ts=ts)
            curr_short_half_sprd = sig_cal.curr_impulse_short_half_spread(ts=ts)
            curr_ref_prc = tilde_p * (1 + curr_ret * self.cfg.TAU)
            curr_bid_touch_line = curr_ref_prc - curr_long_half_sprd
            curr_ask_touch_line = curr_ref_prc + curr_short_half_sprd

            abs_ret = max(abs(curr_ret), abs(pred_ret))
            half_sprd = max(pred_long_half_sprd, pred_short_half_sprd, curr_long_half_sprd, curr_short_half_sprd)
            bid_touch_line = min(pred_bid_touch_line, curr_bid_touch_line)
            ask_touch_line = max(pred_ask_touch_line, curr_ask_touch_line)
            
            avg_sigma = sig_cal.curr_ema_sigma(ts=ts)
            large_abs_ret_bnd = avg_sigma * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER
            lhs = sig_cal.curr_impulse_long_half_spread(ts=ts)
            shs = sig_cal.curr_emavg_short_half_spread(ts=ts)
            avg_half_sprd = (lhs + shs) / 2
            large_hs_bnd = avg_half_sprd * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER
            
            large_ret = abs_ret > large_abs_ret_bnd
            large_sprd = half_sprd > large_hs_bnd

            if large_sprd or large_ret:
                I = 0
                self.defensive_cancel_orders(ask_touch_line=ask_touch_line, bid_touch_line=bid_touch_line, I=I)
                self._n_def_cancel += 1
    
    def _update_prev_depth_metrics(self, contract: str, ex: str, metrics_dict: Dict):
        self._prev_depth_metrics[(contract, ex)] = metrics_dict

    def _get_prev_depth_metrics(self, contract: str, ex: str, metric_name: str) -> float:
        return self._prev_depth_metrics[(contract, ex)][metric_name]

    def _remove_prev_depth_metrics(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._prev_depth_metrics:
            self._prev_depth_metrics.pop((contract, ex))

    def _update_ref_prc(self, contract: str, ex: str, ref_prc: float):
        self._ref_prc_map[(contract, ex)] = ref_prc

    def _get_ref_prc(self, contract: str, ex: str) -> float:
        return self._ref_prc_map[(contract, ex)]

    def _remove_ref_prc(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._ref_prc_map:
            self._ref_prc_map.pop(key)

    def _update_ret(self, contract: str, ex: str, ret: float):
        self._ret_map[(contract, ex)] = ret

    def _get_ret(self, contract: str, ex: str) -> float:
        return self._ret_map[(contract, ex)]

    def _remove_ret(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._ret_map:
            self._ret_map.pop(key)

    def _has_ret(self, contract: str, ex: str) -> bool:
        return (contract, ex) in self._ret_map

    def deactivate(self, iid: str):
        ex, contract = iid.split(".")
        self.executor.remove_depth(iid=iid)
        self.cancel_orders_for_iid(iid=iid)
        self._remove_prev_depth_metrics(contract=contract, ex=ex)
        # self._remove_ref_prc(contract=contract, ex=ex)
        self._remove_ret(contract=contract, ex=ex)
    
    def generate_orders(self, ref_prc):
        orders = self.executor.get_orders(
            contract=self.cfg.SYMBOL,
            ref_prc=ref_prc,
            beta=self.cfg.BETA,
            t=int(time.time()*1e3),
            tau=self.cfg.TAU,
            T_F=self.cfg.T_F,
        )
        for order in orders:
            self.loop.create_task(self.send_order_action(order))
            
    def cancel_orders_action(self):
        for oid in self._order_manager.keys():
            if self._order_manager.get(oid):
                o = self._order_manager.at(oid=oid)
                if o.sent_ts < time.time()*1e3 - o.life and not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(oid=oid, order=o))
                    
    def defensive_cancel_orders(self, ask_touch_line: float, bid_touch_line: float, I: float):
        if I >= 0:  # long
            for int_prc in list(self._order_manager._bid_ladder.keys()):
                if self._order_manager._bid_ladder.get(int_prc):
                    oid_set = self._order_manager._bid_ladder[int_prc]
                    prc =  int_prc / self._order_manager.price_multilier
                    if prc > bid_touch_line:
                        for oid in oid_set:
                            order = self._order_manager.at(oid)
                            if not order.cancel:
                                self.loop.create_task(self.handle_cancel_order(oid=oid, order=order))
                        continue
                    break
        if I <= 0:  # short
            for int_prc in list(self._order_manager._bid_ladder.keys()):
                if self._order_manager._ask_ladder.get(int_prc):
                    oid_set = self._order_manager._ask_ladder[int_prc]
                    prc = int_prc / self._order_manager.price_multilier
                    if prc < ask_touch_line:
                        for oid in oid_set:
                            order = self._order_manager.at(oid)
                            if not order.cancel:
                                self.loop.create_task(self.handle_cancel_order(oid=oid, order=order))
                        continue
                    break

    def cancel_orders_for_iid(self, iid: str):
        for oid in list(self._order_manager.get_oids_for_iid(iid=iid)):
            o = self._order_manager.at(oid=oid)
            if not o.cancel:
                self.loop.create_task(self.handle_cancel_order(oid=oid, order=o))
                
    def get_volume_locked(self, side: Direction, acc: str):
        vol = 0
        for _, o in list(self._order_manager.items()):
            if o.side == side and o.account_name == acc:
                vol += o.quantity
        return vol
                
    async def send_order_action(self, order: TransOrder):
        if not self.send:
            return
        block = False
        for oid in self._order_manager.keys():
            if self._order_manager.get(oid):
                o = self._order_manager.at(oid=oid)
            else:
                continue
            if o.side == order.side and o.iid == order.iid and o.floor == order.floor:
                if abs(o.price - order.price) > 10**(-self.precision) * self.cfg.TOLERANCE_ARR[order.floor]:
                    if not o.cancel:
                        await self.handle_cancel_order(oid=oid, order=o)
                else:
                    order.sent_ts = int(time.time()*1e3)
                    block = True
                    continue
        
        if block:
            return
                
        iid = order.iid
        ex, _ = iid.split(".")
        
        order.symbol_id = self.get_symbol_config(f"{order.iid}.{self.market}.{self.sub_market}")["id"]
        
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[order.iid])
        else:
            order.price = round_decimals_up(order.price, price_precision[order.iid])
        
        order.quantity = round(order.quantity, qty_precision[order.iid])
        
        order_type = "PostOnly"
        # if order.maker_taker == MakerTaker.maker:
        #     order_type = "PostOnly"
        # if order.maker_taker == MakerTaker.taker:
        #     order_type = "IOC"
        
        if order.quantity == 0:
            return
        symbol = f"{iid}.{self.market}.{self.sub_market}"
        symbol_cfg = self.get_symbol_config(symbol)
        

        total_volume = order.quantity
        trader = self.load_balancer.get_trader()
        for acc in list(self.load_balancer.get_acc_list()):
            volume_allowed = self.load_balancer.volume_allowed(acc=acc, side=order.side, prc=order.price)
            volume_locked = self.get_volume_locked(side=order.side, acc=acc)
            volume_allowed -= volume_locked
            if volume_allowed > total_volume:
                if not self.volume_notional_check(symbol=symbol, price=order.price, qty=order.quantity):
                    continue
                cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
                new_order = TransOrder(
                    p=round(float(order.price), price_precision[iid]),
                    q=round(float(order.quantity), qty_precision[iid]),
                    iid=order.iid,
                    floor=order.floor,
                    maker_taker=order.maker_taker,
                    side=order.side
                )
                new_order.sent_ts = int(time.time()*1e3)
                new_order.account_name = acc
                new_order.symbol_id = order.symbol_id
                new_order.client_id = cid
                
                self._order_manager.add_order(order=new_order, oid=cid)
                
                price = Decimal(new_order.price) + symbol_cfg["price_tick_size"]/Decimal("2")
                volume = Decimal(new_order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2")
                
                price, volume = self.format_price_volume(symbol_name=symbol, price=price, volume=volume)
                try:
                    await self.publish_to_channel(
                            "mq:hf_signal:order_request",
                            {
                                "strategy_name":trader,
                                "symbol":symbol,
                                "price":str(price),
                                "qty":str(volume),
                                "side":1 if new_order.side == Direction.long else -1,
                                "client_id": cid,
                                "acc": acc,
                                "order_type":order_type,
                                "main_sn":self.strategy_name,
                            }
                        )
                    break
                except Exception as err:
                    self.logger.critical(f"{err}")
                    await self.internal_fail(cid,err)
                    
            else:
                if not self.volume_notional_check(symbol=symbol, price=order.price, qty=volume_allowed):
                    continue
                cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
                new_order = TransOrder(
                    p=round(float(order.price), price_precision[iid]),
                    q=round(float(volume_allowed), qty_precision[iid]),
                    iid=order.iid,
                    floor=order.floor,
                    maker_taker=order.maker_taker,
                    side=order.side
                )
                new_order.sent_ts = int(time.time()*1e3)
                new_order.account_name = acc
                new_order.symbol_id = order.symbol_id
                new_order.client_id = cid
                
                self._order_manager.add_order(order=new_order, oid=cid)
                
                price = Decimal(new_order.price) + symbol_cfg["price_tick_size"]/Decimal("2")
                volume = Decimal(new_order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2")
                
                price, volume = self.format_price_volume(symbol_name=symbol, price=price, volume=volume)
                try:
                    await self.publish_to_channel(
                            "mq:hf_signal:order_request",
                            {
                                "strategy_name":trader,
                                "symbol":symbol,
                                "price":str(price),
                                "qty":str(volume),
                                "side":1 if new_order.side == Direction.long else -1,
                                "client_id": cid,
                                "acc": acc,
                                "order_type":order_type,
                                "main_sn":self.strategy_name,
                            }
                        )
                    total_volume -= new_order.quantity
                except Exception as err:
                    self.logger.critical(f"{err}")
                    await self.internal_fail(cid,err)
                
        
    async def process_order_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["main_sn"] != self.strategy_name:
            return
        if payload["err"] != None:
            await self.internal_fail(
                client_id=payload["client_id"],
                err=payload["err"],
                acc=payload["account_name"]
                )
            return 
        self.load_balancer.update_limit(
            acc=payload["account_name"],
            trader_name=payload["startegy_name"],
            acc_load=payload["uid10s"],
            ip_load=payload["ip1m"])
        
        if payload["uid10s"] > 300 * 0.8 or payload["ip1m"] > 2400 * 0.8:
            self.send = False
            await asyncio.sleep(1)
            self.send = True
        
    
    async def handle_cancel_order(self, oid, order: TransOrder):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                trader = self.load_balancer.get_trader()
                order.cancel = True
                await self.publish_to_channel(
                        "mq:hf_signal:cancel_request",
                        {
                            "strategy_name":trader,
                            "symbol_id": order.symbol_id,
                            "account_name": order.account_name,
                            "client_id": order.client_id,
                            "main_sn":self.strategy_name,
                        }
                    )
                
        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
        
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["main_sn"] != self.strategy_name:
            return
        if self._order_manager.get(payload["client_id"]) and payload["res"] == False:
            self._order_manager.at([payload["client_id"]]).cancel = False
        self.canceling_id.remove(payload["client_id"])
                
    async def rebalance(self):
        # rebalance, use when initializing or emergency exiting.
        try:
            ref_prc = self._get_ref_prc(contract=self.cfg.SYMBOL, ex=self.cfg.EXCHANGE)
            ccy = self.cfg.SYMBOL.split("_")
            ccy2 = ccy[1]
            self.logger.info(f"current reference price={ref_prc}")
            for acc in self.account_names:
                self.logger.info(f"handle account rebalance: {acc}")
                ex, _ = acc.split(".")
                api = self.get_exchange_api_by_account(acc)
                iid = f"{ex}.{self.cfg.SYMBOL}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    await api.flash_cancel_orders(symbol_cfg)
                except:
                    self.logger.info("no open orders")
                
                for _ in range(3):
                    position_data = (await api.contract_position(symbol_cfg)).data
                    pos = position_data["long_qty"] - position_data["short_qty"]
                    self.logger.info(f"position={pos}")
                    if pos > 0:
                        try:
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(ref_prc*0.99), 
                                volume=pos,
                                side=OrderSide.Sell, 
                                position_side=OrderPositionSide.Close, 
                                order_type=OrderType.IOC,
                                account_name=acc,
                                recvWindow=3000)
                        except:
                            self.logger.warning(f"clean position err")
                            traceback.print_exc()
                    elif pos < 0:
                        try:
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(ref_prc*1.01), 
                                volume=-pos, 
                                side=OrderSide.Buy, 
                                position_side=OrderPositionSide.Close, 
                                order_type=OrderType.IOC,
                                account_name=acc,
                                recvWindow=3000)
                        except:
                            self.logger.warning(f"clean position err")
                            traceback.print_exc()
                    await asyncio.sleep(0.5)
                
                await asyncio.sleep(0.5)
                
                cur_balance = (await api.account_balance(symbol_cfg)).data
                self.load_balancer.update_balance(acc=acc, balance=float(cur_balance[ccy2]["all"]))
                
                position_data = (await api.contract_position(symbol_cfg)).data
                pos = position_data["long_qty"] - position_data["short_qty"]
                self.logger.info(f"""pos={pos} for {acc}""")

                self.logger.info(f"""{ccy2}={float(cur_balance[ccy2]["all"])} for {acc}""")
        except Exception as err:
            traceback.print_exc()
            
    async def rolling_cancel_order(self):
        while True:
            await asyncio.sleep(0.01)
            self.cancel_orders_action()

    async def get_order_status_direct(self, oid, order: TransOrder):
        
        if time.time()*1e3 - order.sent_ts > 5000 + order.query * 5000:
            order.query += 1
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=oid)
                new_order:AtomOrder = res.data
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
                else:
                    await self.handle_cancel_order(oid=oid, order=order)
                
            except Exception as err:
                self.logger.critical(f"direct check order error: {err}")
                
            if order.query > 10:
                self.send = False
                await asyncio.sleep(1)
                await self.rebalance()
                await asyncio.sleep(1)
                self.logger.critical(f"check order too many times and reset")
                self._order_manager = OrderManager()
                
                self.send = True
        
    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(10)
            await self.check_orders()

            
    async def check_orders(self):
        for oid in self._order_manager.keys():
            if self._order_manager.get(oid):
                o = self._order_manager.at(oid)
            else:
                continue
            await self.get_order_status_direct(oid=oid, order=o)
                
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    await self.rebalance()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(5)
            try:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="perp",
                        tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                        fields={
                            "inventory":float(self.executor.get_inventory(f"{self.cfg.EXCHANGE}.{self.cfg.SYMBOL}")),
                            "ref_prc":float(self._get_ref_prc(contract=self.cfg.SYMBOL, ex=self.cfg.EXCHANGE)),
                            }
                        )
                    )
                ccy = self.cfg.SYMBOL.split("_")
                ccy2 = ccy[1]
                tot_balance = 0
                for acc in self.account_names:
                    ex, _ = acc.split(".")
                    api = self.get_exchange_api_by_account(acc)
                    iid = f"{ex}.{self.cfg.SYMBOL}"
                    symbol = f"{iid}.{self.market}.{self.sub_market}"
                    symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                    cur_balance = (await api.account_balance(symbol_cfg)).data
                    balance=float(cur_balance[ccy2]["all"])
                    self.load_balancer.update_balance(acc=acc, balance=balance)
                    await asyncio.sleep(0.2)
                    tot_balance += balance
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="perp", 
                            tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                            fields={
                                f"{acc}_load": float(self.load_balancer._account_load_map[acc])
                            }
                        )
                    )
                
                for trader in list(self.load_balancer._ip_load_map.keys()):
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="perp", 
                            tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                            fields={
                                f"{trader}_load": float(self.load_balancer._ip_load_map[trader])
                            }
                        )
                    )
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="perp", 
                        tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                        fields={
                            "balance": float(tot_balance)
                            }
                        )
                    )
                
                for _, order in list(self._order_manager.items()):
                    if order.side == Direction.short:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="perp", 
                                tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                                fields={
                                    "cache_sell_price":float(order.price)
                                    }
                                )
                            )
                    else:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="perp", 
                                tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                                fields={
                                    "cache_buy_price":float(order.price)
                                    }
                                )
                            )
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="perp", 
                        tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                        fields={
                            "trade_amount_per_second": float(float(self.trd_amt)/(time.time() - self.start_ts)),
                            }
                        )
                    )
                
                
                if self.stats:
                    stats = self.executor.get_stats(contract=self.cfg.SYMBOL, ts=int(time.time()*1e3))
                    for k in stats.keys():
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="stats", 
                                tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.SYMBOL}"}, 
                                fields={
                                    k: float(stats[k]),
                                    }
                                )
                            )

            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
                
    async def debug(self):
        while True:
            await asyncio.sleep(10)
            for oid, o in list(self._order_manager.items()):
                if o.cancel is True and time.time()*1e3 - o.sent_ts> 1000:
                    self.logger.warning(f"expired order: p={o.price}, q={o.quantity}, side={o.side}, acc={o.account_name}, id={oid}, time={time.time()*1e3 - o.sent_ts}")
            self.logger.warning(".................")
            
            tot_pos = 0
            for acc in self.account_names:
                self.logger.critical(f"{acc} load={self.load_balancer._account_load_map[acc]}")
                self.logger.critical(f"{acc} pos={self.load_balancer._position_map[acc]}")
                tot_pos += self.load_balancer._position_map[acc]
            self.logger.critical(f"total pos={tot_pos}")
            
            for trader in list(self.load_balancer._ip_load_map.keys()):
                self.logger.critical(f"{trader} load={self.load_balancer._ip_load_map[trader]}")
    
    async def reduce_count(self):
        while True:
            await asyncio.sleep(1)
            self.load_balancer.reduce_count(1)
        
    async def strategy_core(self):
        
        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.get_local_stats(),
                self.reduce_count(),
                # self.debug()
            )
        
            
if __name__ == '__main__':
    logging.getLogger().setLevel("CRITICAL")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()