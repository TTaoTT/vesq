import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy



class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.price = None
        self.symbol = f"binance.btc_usdt_swap.usdt_contract.na"
        self.use_raw_stream = True
    
    def update_config_before_init(self):
        self.use_colo_url = True
        
    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        for acc in self.account_names:
            self._account_config_[acc]["cfg_pos_mode"][self.symbol] = 1
        
    async def on_order(self, o):
        self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        if order["s"] != "BTCUSDT":
            return
        self.logger.warning(order)
        
    async def on_public_trade(self,symbol,trade):
        self.price = Decimal(trade["data"]["p"])
        
    async def send_order(self):
        while True:
            await asyncio.sleep(10)
            if not self.price:
                continue
            try:
                await self.make_order(
                    self.symbol,
                    self.price*Decimal(1.01),
                    Decimal("0.005"),
                    OrderSide.Buy,
                    OrderPositionSide.Open
                    )
            except:
                raise
            
            await asyncio.sleep(0.5)
            
            try:
                p = self.price*Decimal(1.002)
                await self.make_order(
                    self.symbol,
                    p,
                    Decimal("0.005"),
                    OrderSide.Sell,
                    OrderPositionSide.Close,
                    OrderType.PostOnly
                    ) 
            except:
                raise
            await asyncio.sleep(0.5)
            
            try:
                await self.make_order(
                    self.symbol,
                    self.price*Decimal(1.003),
                    Decimal("0.005"),
                    OrderSide.Sell,
                    OrderPositionSide.Open,
                    OrderType.PostOnly,
                    ) 
            except:
                raise
            await asyncio.sleep(0.5)
            
            # try:
            #     await self.make_order(
            #         self.symbol,
            #         p - Decimal("0.1"),
            #         Decimal("0.006"),
            #         OrderSide.Sell,
            #         OrderPositionSide.Close,
            #         OrderType.PostOnly
            #         ) 
            # except:
            #     raise
            
            try:
                await self.make_order(
                    self.symbol,
                    self.price*Decimal(0.998),
                    Decimal("0.005"),
                    OrderSide.Sell,
                    OrderPositionSide.Close,
                    OrderType.IOC
                    ) 
            except:
                raise
            
            break
                
    
    async def strategy_core(self):
        await asyncio.sleep(1)
        # while True:
        #     await asyncio.sleep(10)
        #     self.logger.info(".....")
            
        await self.send_order()  
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()