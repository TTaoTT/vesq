
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections
import pickle

from decimal import Decimal 
import asyncio
import time
import numpy as np
import hashlib
import hmac
import aiohttp

url = "http://35.73.223.244:5555/public/tool/hf_model"

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)

    async def strategy_core(self):
        while True:
            await asyncio.sleep(1)
            try:
                await self.get_data_async()
            except:
                pass
    
    def sign(self):
        key = 'a0744ec83963312fsdf642e38471367c3fasf2f2sdfa'
        timestamp = int(time.time())
        signature = hmac.new(key.encode(), str(timestamp).encode(), hashlib.sha256).hexdigest()
        return dict(timestamp=timestamp.__str__(), signature=signature)
    
    async def get_data_async(self):
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=dict(filename="binance.eth_usdt_swap.pkl"), headers=self.sign()) as conn:
                data = await conn.content.readany()
                model = pickle.loads(data)
                temp = model["bid_model"].coef_
                self.logger.info(f"{temp}")
    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()


