import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
import aiohttp
from asyncio.queues import Queue
from statsmodels.stats.weightstats import DescrStatsW

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0

        self.feed_trade_time1 = collections.deque(maxlen=10000)
        self.feed_trade_time2 = collections.deque(maxlen=10000)
        
    def feed_trade(self, trade: dict):
        s = time.time()*1e3
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cahce[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
        self.feed_trade_time1.append(time.time()*1e3-s)
            
        s = time.time()*1e3
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
        self.feed_trade_time2.append(time.time()*1e3-s)
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        
                
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_data = f"binance.btc_usdt_swap.usdt_contract.na"
        self.trade_cache = PriceDistributionCache()
        
        self.feed_trade_time = collections.deque(maxlen=10000)
        
        self.get_price_time = collections.deque(maxlen=10000)
        self.trim = collections.deque(maxlen=10000)
        self.trade_cache_len = 0
        
        self.max_price_point = 0
        self.max_ts_point = 0
        
    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol_data)
        
    async def on_public_trade(self, symbol, trade: PublicTrade):
        tmp = {"t":trade.server_ms,"p":int(trade.price*Decimal(1e5)), "q":int(trade.quantity*Decimal(1e3))}
        s = time.time()*1e3
        
        self.trade_cache.feed_trade(tmp)
        self.feed_trade_time.append(time.time()*1e3-s)

        s = time.time()*1e3
        self.trade_cache.remove_expired(trade.server_ms)
        self.trim.append(time.time()*1e3-s)

        self.trade_cache_len = max(self.trade_cache_len,len(self.trade_cache.cache))
            
    async def get_price(self):
        while True:
            await asyncio.sleep(1)
            s = time.time()*1e3
            self.trade_cache.get_ask_price(0.95)
            self.get_price_time.append(time.time()*1e3-s)
            
    async def set_cache(self):
        while True:
            await asyncio.sleep(1)
            await self.redis_set_cache(
                {
                "feed trade time1":np.mean(self.trade_cache.feed_trade_time1) if self.trade_cache.feed_trade_time1 else 0,
                "max feed trade time1":np.max(self.trade_cache.feed_trade_time1) if self.trade_cache.feed_trade_time1 else 0,
                "feed trade time2":np.mean(self.trade_cache.feed_trade_time2) if self.trade_cache.feed_trade_time2 else 0,
                "max feed trade time2":np.max(self.trade_cache.feed_trade_time2) if self.trade_cache.feed_trade_time2 else 0,
                "mean get price time":np.mean(self.get_price_time) if self.get_price_time else 0,
                "max get price time":np.max(self.get_price_time) if self.get_price_time else 0,
                "mean trim time":np.mean(self.trim) if self.trim else 0,
                "max trim time":np.max(self.trim) if self.trim else 0,
                "max cache length":self.trade_cache_len

                }
            )
            
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache(),
            self.get_price()
            )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    