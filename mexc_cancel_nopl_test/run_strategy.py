import asyncio
from decimal import Decimal
import time
import traceback
from strategy_base.base import CommonStrategy
from xex_mm.utils.base import OrderManager, TransOrder

from atom.exceptions import *
from atom.model import *
from atom.helpers import ClientIDGenerator



class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.cancel_ts_dict = dict()
        self.order_manager = dict()
        self.symbol = "mexc.btc_usdt.spot.na"
        self.prc = ...
    
        
    async def before_strategy_start(self):
        self.logger.setLevel("CRITICAL")
        self.direct_subscribe_orderbook(symbol_name=self.symbol, is_incr_depth=True)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
    
    async def on_orderbook(self, symbol, orderbook):
        try:
            self.prc = orderbook["asks"][0][0]
        except:
            traceback.print_exc()
            
    async def on_order(self, order: PartialOrder):
        try:
            oid = order.xchg_id
            cid = order.client_id
            
            if self.cancel_ts_dict.get(oid) != None and order.xchg_status in OrderStatus.fin_status():
                tlist = self.cancel_ts_dict[oid]
                cancel_action_ts, cancel_finish_ts = tlist
                ts_gap = cancel_finish_ts - cancel_action_ts
                if ts_gap > 50:
                    self.logger.critical(f"cancel takes {ts_gap}ms, finished_time={int(time.time()*1e3)}")
                
                self.cancel_ts_dict.pop(oid)
                
            if not self.order_manager.get(oid) == None:
                return
                
            if order.xchg_status in OrderStatus.fin_status() and self.order_manager.get(oid) != None:
                self.order_manager.pop(oid)
        except:
            traceback.print_exc()
            
    async def main_logic(self):
        while True:
            await asyncio.sleep(0.4)
            if self.prc is Ellipsis:
                continue
            try:
                s = int(time.time()*1e3)
                resp:Order = await self.make_order(
                    symbol_name=self.symbol,
                    price=self.prc * Decimal(0.997),
                    volume=Decimal(0.001),
                    side=OrderSide.Buy,
                    order_type=OrderType.PostOnly,
                    position_side=OrderPositionSide.Open,
                )
                dur = int(time.time()*1e3) - s
                # if dur > 20:
                #     self.logger.critical(f"send order takes {dur}ms") 
                self.order_manager[resp.xchg_id] = resp
                self.cancel_ts_dict[resp.xchg_id] = [0,0]
                await asyncio.sleep(0.2)
                if self.cancel_ts_dict.get(resp.xchg_id) != None:
                    self.cancel_ts_dict[resp.xchg_id][0] = int(time.time()*1e3)
                await self.cancel_order(order=resp, catch_error=False)
                if self.cancel_ts_dict.get(resp.xchg_id) != None:
                    self.cancel_ts_dict[resp.xchg_id][1] = int(time.time()*1e3)
            except:
                traceback.print_exc()
    
    async def strategy_core(self):
        await self.main_logic()
        

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()