import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue

from atom.helpers import json, safe_decimal, ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

SYMBOLS = ["eth","doge","sand","mana","gala"]

MAX_CASH = Decimal(200)

class BboData():
    def __init__(self) -> None:
        self.spot_ask = None
        self.spot_bid = None
        self.spot_ask_qty = None
        self.spot_bid_qty = None
        self.spot_ts = None
        self.perp_ask = None
        self.perp_bid = None
        self.perp_ask_qty = None
        self.perp_bid_qty = None
        self.perp_ts = None
        self.sb_pa = collections.deque(maxlen=10000)
        self.pb_sa = collections.deque(maxlen=10000)
        
        self.pos = dict()
        
        self.pos["spot"] = {"pos":Decimal(0), "pos_price":Decimal(0)}
        self.pos["perp"] = {"pos":Decimal(0), "pos_price":Decimal(0)}
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict
        self.params = float2decimal(self.config['strategy']['params'])
        # symbol = self.params["symbol"]
        # self.symbol_spot = f"binance.{symbol}_busd.spot.na" 
        # self.symbol_perp = f"binance.{symbol}_usdt_swap.usdt_contract.na" 
        self._order_update = Queue()
        self._order_to_close = Queue()
        self._order_to_balance = Queue()
        self._order_to_store = Queue()
        self.opp = 0
        self.bbo_data = dict()
        for symbol in SYMBOLS:
            self.bbo_data[symbol] = BboData()
        self.freeze = None
        self.order_cache = dict()
        
    async def before_strategy_start(self):
        self.logger.setLevel("WARNING")
        self.direct_subscribe_order_update(symbol_name="binance.btc_busd.spot.na")
        self.direct_subscribe_order_update(symbol_name="binance.btc_usdt_swap.usdt_contract.na")
        self.session = aiohttp.ClientSession()
        self.loop.create_task(self.store_order())
        
        for symbol in SYMBOLS:
            symbol_spot = f"binance.{symbol}_busd.spot.na"
            symbol_perp = f"binance.{symbol}_usdt_swap.usdt_contract.na" 
            await self.direct_subscribe_bbo(symbol_name=symbol_spot)
            await self.direct_subscribe_bbo(symbol_name=symbol_perp)
            
    async def on_order(self, order: PartialOrder):
        if order.xchg_status in OrderStatus.fin_status():
            self.logger.warning(f"order:{order}")
            symbol, market, side, direction = order.client_id.split("_")[-4:]
            pos = self.bbo_data[symbol].pos[market]["pos"]
            pos_price = self.bbo_data[symbol].pos[market]["pos_price"]
            if order.filled_amount > Decimal(0):
                if pos <= Decimal(0) and side=="1":
                    if order.filled_amount > abs(pos):
                        self.bbo_data[symbol].pos[market]["pos_price"] = order.avg_filled_price
                    self.bbo_data[symbol].pos[market]["pos"] = order.filled_amount + pos
                elif pos <= Decimal(0) and side=="2":
                    self.bbo_data[symbol].pos[market]["pos_price"] = (order.avg_filled_price*order.filled_amount + abs(pos)*pos_price) / (order.filled_amount + abs(pos))
                    self.bbo_data[symbol].pos[market]["pos"] = pos - order.filled_amount
                elif pos > Decimal(0) and side=="1":
                    self.bbo_data[symbol].pos[market]["pos_price"] = (order.avg_filled_price*order.filled_amount + pos*pos_price) / (order.filled_amount+pos)
                    self.bbo_data[symbol].pos[market]["pos"] = pos + order.filled_amount
                elif pos > Decimal(0) and side=="2":
                    if order.filled_amount > abs(pos):
                        self.bbo_data[symbol].pos[market]["pos_price"] = order.avg_filled_price
                    self.self.bbo_data[symbol].pos[market]["pos"] = pos - order.filled_amount
                
            q_1 = self.bbo_data[symbol].pos["spot"]["pos"]
            q_2 = self.bbo_data[symbol].pos["perp"]["pos"]
            s_id = self.order_cache[order.client_id].symbol_id
            
            self.order_cache[order.client_id].filled_amount = order.filled_amount
            self.order_cache[order.client_id].xchg_status = order.xchg_status
            self.order_cache[order.client_id].xchg_id = order.xchg_id
            self._order_to_store.put_nowait(self.order_cache[order.client_id])
            self.order_cache.pop(order.client_id)
            
            if direction == "close" and \
                self.position_residue_check(s_id, order.avg_filled_price, q_1+q_2) and \
                self.position_residue_check(s_id, order.avg_filled_price, q_1) and \
                self.position_residue_check(s_id, order.avg_filled_price, q_2):
                self.freeze =  None
            elif direction == "open" and self._order_to_balance.empty():
                self._order_to_balance.put_nowait(symbol)
                
    async def store_order(self):
        while True:
            order = await self._order_to_store.get()
            await self.post_order_to_system(order, self.session)
            
    async def on_bbo(self, symbol, bbo):
        symbol = symbol.split(".")[1]
        symbol_key = symbol.split("_")[0]
        type = symbol.split("_")[1]
        
        if type == "busd":
            self.bbo_data[symbol_key].spot_ask = Decimal(bbo.ask[0])
            self.bbo_data[symbol_key].spot_bid = Decimal(bbo.bid[0])
            self.bbo_data[symbol_key].spot_ask_qty = Decimal(bbo.ask[1])
            self.bbo_data[symbol_key].spot_bid_qty = Decimal(bbo.bid[1])
            self.bbo_data[symbol_key].spot_ts = bbo.server_ms
        elif type == "usdt":
            self.bbo_data[symbol_key].perp_ask = Decimal(bbo.ask[0])
            self.bbo_data[symbol_key].perp_bid = Decimal(bbo.bid[0])
            self.bbo_data[symbol_key].perp_ask_qty = Decimal(bbo.ask[1])
            self.bbo_data[symbol_key].perp_bid_qty = Decimal(bbo.bid[1])
            self.bbo_data[symbol_key].perp_ts = bbo.server_ms
        
        if self.bbo_data[symbol_key].spot_bid and self.bbo_data[symbol_key].perp_bid:
            self.bbo_data[symbol_key].sb_pa.append(float((self.bbo_data[symbol_key].spot_bid - self.bbo_data[symbol_key].perp_ask) / self.bbo_data[symbol_key].perp_ask))
            self.bbo_data[symbol_key].pb_sa.append(float((self.bbo_data[symbol_key].perp_bid - self.bbo_data[symbol_key].spot_ask) / self.bbo_data[symbol_key].spot_ask))
        
        s_pos = self.bbo_data[symbol_key].pos["spot"]["pos"]
        p_pos = self.bbo_data[symbol_key].pos["perp"]["pos"]
        s_pos_price = self.bbo_data[symbol_key].pos["spot"]["pos_price"]
        p_pos_price = self.bbo_data[symbol_key].pos["perp"]["pos_price"]
        
        if self._order_update.empty() and self._order_to_close.empty() and self._order_to_balance.empty() and self.freeze == symbol_key and not self.order_cache:
            if self.position_residue_check(f"binance.{symbol_key}_busd.spot.na", s_pos_price, s_pos+p_pos) or \
                self.position_residue_check(f"binance.{symbol_key}_usdt_swap.usdt_contract.na" , s_pos_price, s_pos+p_pos):
                if s_pos > Decimal(0):
                    price_diff = p_pos_price - s_pos_price
                    curr_price_diff = self.bbo_data[symbol_key].spot_bid - self.bbo_data[symbol_key].perp_ask
                    if price_diff + curr_price_diff > self.bbo_data[symbol_key].perp_ask * Decimal(0.00091) and \
                        s_pos > self.bbo_data[symbol_key].perp_ask_qty and \
                        s_pos > self.bbo_data[symbol_key].spot_bid_qty:
                        self._order_to_close.put_nowait([symbol_key,s_pos,p_pos, s_pos_price])
                elif s_pos < Decimal(0):
                    price_diff = s_pos_price - p_pos_price
                    curr_price_diff = self.bbo_data[symbol_key].perp_bid - self.bbo_data[symbol_key].spot_ask
                    if price_diff + curr_price_diff > self.bbo_data[symbol_key].perp_ask * Decimal(0.00091) and \
                        -s_pos > self.bbo_data[symbol_key].perp_bid_qty and \
                        -s_pos > self.bbo_data[symbol_key].spot_ask_qty:
                        self._order_to_close.put_nowait([symbol_key,s_pos,p_pos,s_pos_price])
                else:
                    self.freeze = None
                    
        if self._order_update.empty() and \
            self._order_to_close.empty() and \
            self._order_to_balance.empty() and \
            not self.order_cache and \
            s_pos*s_pos_price < MAX_CASH :
            self._order_update.put_nowait(symbol_key)
            
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    def position_residue_check(self, symbol_id, price, qty):
        config = self.get_symbol_config(symbol_id)
        if qty < config["min_quantity_val"] * Decimal(1) or price * qty < config["min_notional_val"] * Decimal(1):
            return True
        return False
    
    def execute_order(self, symbol_name,price,volume,side,position_side,order_type,clabel):
        client_id = str(int(time.time()*1e3)) + "_" + clabel
        self.logger.warning(client_id)
        if not self.volume_notional_check(symbol_name, price, volume):
            return
        self.order_cache[client_id] = self.gen_async_order(
            client_id=client_id,
            symbol_name=symbol_name,
            price=price,
            volume=volume,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        self.loop.create_task(
            self.make_order(
                symbol_name=symbol_name,
                price=price,
                volume=volume,
                side=side,
                position_side=position_side,
                order_type=order_type,
                client_id= client_id  
            )
        )
    
    async def open_position(self):
        await asyncio.sleep(60)
        while True:
            symbol = await self._order_update.get()
            bbo_data = self.bbo_data[symbol]
            if bbo_data.perp_ts and bbo_data.spot_ts and len(bbo_data.pb_sa)>1000 and not self.freeze: 
                if time.time()*1e3 - bbo_data.perp_ts < 50 and time.time()*1e3 - bbo_data.spot_ts < 50:
                    # self.logger.warning(f"pb:{bbo_data.perp_bid}, sa:{bbo_data.spot_ask}")
                    if bbo_data.perp_bid - bbo_data.spot_ask + Decimal(np.median(bbo_data.sb_pa))> bbo_data.spot_ask * Decimal(0.00091):
                        try:
                            amount = np.min(
                                [
                                    bbo_data.perp_bid_qty,
                                    bbo_data.spot_ask_qty, 
                                    self.params["base_amount"]/bbo_data.spot_ask, 
                                    (MAX_CASH - abs(bbo_data.pos["perp"]["pos"]*bbo_data.pos["perp"]["pos_price"]))/bbo_data.spot_ask
                                ]
                            )
                            self.freeze = symbol
                            self.execute_order(
                                symbol_name=f"binance.{symbol}_busd.spot.na",
                                price=bbo_data.spot_ask,
                                volume=amount,
                                side=OrderSide.Buy,
                                position_side=OrderPositionSide.Both,
                                order_type=OrderType.IOC,
                                clabel=f"{symbol}_spot_1_open"
                            )
                            self.execute_order(
                                symbol_name=f"binance.{symbol}_usdt_swap.usdt_contract.na" ,
                                price=bbo_data.perp_bid,
                                volume=amount,
                                side=OrderSide.Sell,
                                position_side=OrderPositionSide.Both,
                                order_type=OrderType.IOC,
                                clabel=f"{symbol}_perp_2_open"
                            )
                            self.logger.warning(f"symbol:{symbol}, perp_bid:{bbo_data.perp_bid} and spot_ask:{bbo_data.spot_ask}")
                        except Exception as err:
                            self.logger.warning(f"open position error: {err}")
                        
                    elif bbo_data.spot_bid - bbo_data.perp_ask + Decimal(np.median(bbo_data.pb_sa))> bbo_data.perp_ask * Decimal(0.00091):
                        try:
                            amount = np.min(
                                [
                                    bbo_data.spot_bid_qty, 
                                    bbo_data.perp_ask_qty, 
                                    self.params["base_amount"]/bbo_data.spot_ask, 
                                    (MAX_CASH - abs(bbo_data.pos["perp"]["pos"]*bbo_data.pos["perp"]["pos_price"]))/bbo_data.spot_ask
                                ]
                            )
                            self.freeze = symbol
                            self.execute_order(
                                symbol_name=f"binance.{symbol}_busd.spot.na",
                                price=bbo_data.spot_bid,
                                volume=amount,
                                side=OrderSide.Sell,
                                position_side=OrderPositionSide.Both,
                                order_type=OrderType.IOC,
                                clabel=f"{symbol}_spot_2_open"
                            )
                            self.execute_order(
                                symbol_name=f"binance.{symbol}_usdt_swap.usdt_contract.na" ,
                                price=bbo_data.perp_ask,
                                volume=amount,
                                side=OrderSide.Buy,
                                position_side=OrderPositionSide.Both,
                                order_type=OrderType.IOC,
                                clabel=f"{symbol}_perp_1_open"
                            )
                            self.logger.warning(f"symbol:{symbol}, spot_bid:{bbo_data.spot_bid} and perp_ask:{bbo_data.perp_ask}")
                        except Exception as err:
                            self.logger.warning(f"open position error: {err}")
                        
    async def close_position(self):
        while True:
            symbol, spot_pos, perp_pos, price = await self._order_to_close.get()
            try:
                if spot_pos > Decimal(0):
                    self.execute_order(
                        symbol_name=f"binance.{symbol}_usdt_swap.usdt_contract.na",
                        price=price*Decimal(1.01),
                        volume=abs(perp_pos),
                        side=OrderSide.Buy,
                        position_side=OrderPositionSide.Both,
                        order_type=OrderType.IOC,
                        clabel=f"{symbol}_perp_1_close"
                    )
                    self.execute_order(
                        symbol_name=f"binance.{symbol}_busd.spot.na",
                        price=price*Decimal(0.99),
                        volume=abs(spot_pos),
                        side=OrderSide.Sell,
                        position_side=OrderPositionSide.Both,
                        order_type=OrderType.IOC,
                        clabel=f"{symbol}_spot_2_close"
                    )
                else:
                    self.execute_order(
                        symbol_name=f"binance.{symbol}_usdt_swap.usdt_contract.na",
                        price=price*Decimal(0.99),
                        volume=abs(perp_pos),
                        side=OrderSide.Sell,
                        position_side=OrderPositionSide.Both,
                        order_type=OrderType.IOC,
                        clabel=f"{symbol}_perp_2_close"
                    )
                    self.execute_order(
                        symbol_name=f"binance.{symbol}_busd.spot.na",
                        price=price*Decimal(1.01),
                        volume=abs(spot_pos),
                        side=OrderSide.Buy,
                        position_side=OrderPositionSide.Both,
                        order_type=OrderType.IOC,
                        clabel=f"{symbol}_spot_1_close"
                    )
            except Exception as err:
                self.logger.warning(f"close position error: {err}")
                
    async def pos_balance(self):
        while True:
            symbol = await self._order_to_balance.get()
            try:
                if self.freeze==symbol and not self.order_cache:
                    pos_diff = self.bbo_data[self.freeze].pos["spot"]["pos"]+self.bbo_data[self.freeze].pos["perp"]["pos"]
                    if not self.position_residue_check(f"binance.{self.freeze}_busd.spot.na", self.bbo_data[self.freeze].pos["spot"]["pos_price"], abs(pos_diff)) and \
                    not self.position_residue_check(f"binance.{self.freeze}_usdt_swap.usdt_contract.na", self.bbo_data[self.freeze].pos["perp"]["pos_price"], abs(pos_diff)):
                        if pos_diff > Decimal(0):
                            self.execute_order(
                                symbol_name=f"binance.{symbol}_usdt_swap.usdt_contract.na",
                                price=price*Decimal(0.99),
                                volume=abs(pos_diff),
                                side=OrderSide.Sell,
                                position_side=OrderPositionSide.Both,
                                order_type=OrderType.IOC,
                                clabel=f"{symbol}_perp_2_balance"
                            )
                        else:
                            self.execute_order(
                                symbol_name=f"binance.{symbol}_usdt_swap.usdt_contract.na",
                                price=price*Decimal(1.01),
                                volume=abs(pos_diff),
                                side=OrderSide.Buy,
                                position_side=OrderPositionSide.Both,
                                order_type=OrderType.IOC,
                                clabel=f"{symbol}_perp_1_balance"
                            )
            except Exception as err:
                self.logger.warning(f"balance position error: {err}")
                
    async def check_cache(self):
        while True:
            await asyncio.sleep(1)
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    if self.freeze:
                        symbol = self.freeze
                        self.freeze = True
                    else:
                        exit()
                    for i in range(10):
                        ...
                    
                    self.logger.warning(f"manually exiting")
                    exit()
         
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")
                    
    async def strategy_core(self):
        await asyncio.sleep(60)
        await asyncio.gather(
            self.open_position(),
            self.close_position(),
            self.pos_balance()
        )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    
    
                
            