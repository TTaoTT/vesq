from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "press3_prod")

from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from atom.helpers import ClientIDGenerator
from atom.exchange_api.abc import ApiResponse
from atom.exceptions import *

from xex_mm.at_touch_algo_order_strategy.base import StopPack, StopPackCID
from xex_mm.at_touch_algo_order_strategy.binance.enum import \
    OrderType as BinanceOrderType
from xex_mm.utils.enums import Side
from xex_mm.utils.contract_mapping import ContractMapper
from xex_mm.utils.configs import price_precision, qty_precision
from xex_mm.utils.base import Direction, Order, TransOrder, XexInventoryManager, Trade, BBO, Depth, \
    ReferencePriceCalculator, Level, DefaultDict
from xex_mm.strategy_configs.at_touch_algo_order_config import Config, QuoteLevelConfig, PressMode
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard
from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.calculators.price_stability import CalPriceStability, PriceStabilityState
from xex_mm.calculators.latency import CalEMAvgLatency
from xex_mm.at_touch_algo_order_strategy.order_manager import AtTouchBinanceOrderRequestManager, AtTouchHedgeBinanceOrderRequestManager, AtTouchBinanceOrderRequestManagerMap
from xex_mm.at_touch_algo_order_strategy.executor import AtTouchAlgoOrderExecutor
from xex_mm.at_touch_algo_order_strategy.binance.requests import BinanceLimitOrder, BinanceOrderRequest, BinanceStopOrder
from xex_mm.at_touch_algo_order_strategy.binance.depth_manager import XExDepthManager
from xex_mm.at_touch_algo_order_strategy.binance.calculators import CalEMAvgTradeSize, CalBBOPressure, CalInvenRet, \
    CalMomentumLength

from strategy_base.base import CommonStrategy

import ujson as json
import pandas as pd
import numpy as np
from typing import AnyStr, Deque, Dict, Union, List
from decimal import Decimal
from collections import defaultdict, deque
from asyncio.queues import LifoQueue, Queue
import traceback
import time
import math
import asyncio
from atom.model.depth import DepthConfig
from atom.logger import BasicLogger

DepthConfig.MAX_DEPTH = 20

class AtTouchBinanceOrderRequestManagerMap(AtTouchBinanceOrderRequestManagerMap):
    def get_om(self, cid: str) -> AtTouchBinanceOrderRequestManager:
        if not self.cid_to_ex_map.get(cid):
            return None
        return self.om_map[self.cid_to_ex_map[cid]]

    def get_order(self, cid: str):
        if self.get_om(cid=cid):
            return self.get_om(cid=cid).get_order(cid=cid)
        return None

ACC_LOAD_KEY = "X-MBX-ORDER-COUNT-10S"
IP_LOAD_KEY = "X-MBX-USED-WEIGHT-1M"

class Order(Order):
    @classmethod
    def from_dict(cls, porder: Union[PartialOrder,AtomOrder], local_order: Union[TransOrder,BinanceOrderRequest], contract_value: Decimal):
        xchg_status = porder.xchg_status
        if isinstance(porder, cls):
            return porder
        
        if isinstance(local_order, TransOrder):
            ex, contract = local_order.iid.split(".")
        elif isinstance(local_order, BinanceOrderRequest):
            ex, contract = local_order.piid.split(".")
            
        if isinstance(porder, PartialOrder):
            match_type=porder.extra.get("order_type")
            if porder.raw_data and porder.raw_data.get("o"):
                request_qty = float(porder.raw_data["o"]["q"])
                if porder.raw_data["o"].get("X") == "EXPIRED":
                    xchg_status = OrderStatus.Expired
            else:
                request_qty = local_order.quantity
        elif isinstance(porder, AtomOrder):
            match_type=local_order.order_type_atom
            if porder.raw_data:
                request_qty = float(porder.raw_data["origQty"])
                if porder.raw_data.get("status")== "EXPIRED":
                    xchg_status = OrderStatus.Expired
        return cls(
            contract=contract,
            ex=ex,
            prc=local_order.price,
            qty=request_qty,
            side=local_order.side,
            filled_qty=float(porder.filled_amount * contract_value),
            avg_fill_prc=float(porder.avg_filled_price),
            order_id=porder.xchg_id,
            client_id=local_order.new_client_order_id,
            local_time=porder.local_ms,
            tx_time=porder.server_ms,
            status=xchg_status,
            account_name=porder.account_name,
            match_type=match_type
        )
        
class RateLimitHandler:
    def __init__(self,executor: AtTouchAlgoOrderExecutor, logger: BasicLogger, loop=asyncio.get_event_loop()) -> None:
        self.hedge_order_ban = False
        self.execution_ban = False
        self.stop_ban = False
        
        self.hedge_free_ts = 0
        self.mm_free_ts = 0
        self.execution_free_ts = 0
        self.logger = logger
        self.executor = executor
        loop.create_task(self.free())
    
    async def free(self):
        while True:
            await asyncio.sleep(1)
            curr_time = time.time()
            if self.hedge_order_ban == True and self.hedge_free_ts <= curr_time:
                self.hedge_order_ban = False
            if self.executor.no_new_mm == True and self.mm_free_ts <= curr_time:
                self.executor.no_new_mm = False
            if self.execution_ban == True and self.execution_free_ts <= curr_time:
                self.execution_ban = False
    
    def hedge_is_ban(self):
        if self.execution_ban or self.hedge_order_ban:
            return True
        return False
    
    def stop_is_ban(self):
        if self.execution_ban or self.stop_ban:
            return True
        return False
    
    def execution_is_ban(self):
        return self.execution_ban
    
    def ban_mm_order(self, sec: int):
        free_time = int(time.time()) + sec
        if self.executor.no_new_mm == False:
            self.logger.critical(f"ban mm order in handler, free at {free_time}")
        self.executor.no_new_mm = True
        self.mm_free_ts = free_time
        
    def ban_stop_order(self):
        if self.stop_ban == False:
            self.logger.critical(f"ban stop order in handler")
        self.stop_ban = True
    
    def free_stop_order(self):
        if self.stop_ban == True:
            self.logger.critical(f"free stop order in handler")
        self.stop_ban = False
        
    def ban_hedge_order(self, sec: int):
        self.ban_mm_order(sec=sec)
        free_time = int(time.time()) + sec
        if self.hedge_order_ban == False:
            self.logger.critical(f"ban hedge order in handler, free at {free_time}")
        self.hedge_order_ban = True
        self.hedge_free_ts = free_time
        
    def ban_execution(self, sec: int):
        self.ban_mm_order(sec=sec)
        free_time = int(time.time()) + sec
        self.logger.critical(f"ban execution in handler, free at {free_time}")
        self.execution_ban = True
        self.execution_free_ts = free_time
        

class HoldingPeriodTracker():
    def __init__(self) -> None:
        self.discret_pos = []
        self.pos = 0

        self.time_qty_sum = 0
        self.qty_sum = 0
        self.last_inven = 0
        self.count = 0
        self.hit_count = deque(maxlen=10000)

    def on_position(self, pos_chg: dict):
        temp = pos_chg["qty"]
        if pos_chg["side"] * self.pos < 0:
            self.pos += temp * pos_chg["side"]
            while self.discret_pos:
                pos = self.discret_pos[0]
                if pos_chg["qty"] > pos["qty"]:
                    self.time_qty_sum += (time.time()-pos["ts"]) * pos["qty"]
                    self.qty_sum += pos["qty"]
                    self.discret_pos.pop(0)
                    pos_chg["qty"] -= pos["qty"]
                elif pos_chg["qty"] < pos["qty"]:
                    self.time_qty_sum += (time.time() -
                                          pos["ts"]) * pos_chg["qty"]
                    self.qty_sum += pos_chg["qty"]
                    pos["qty"] -= pos_chg["qty"]
                    return
                else:
                    self.time_qty_sum += (time.time()-pos["ts"]) * pos["qty"]
                    self.qty_sum += pos["qty"]
                    self.discret_pos.pop(0)
                    return

            self.discret_pos.append(pos_chg)
        else:
            self.pos += temp * pos_chg["side"]
            self.discret_pos.append(pos_chg)
            
    def on_inven_change(self, cur_inven:float, tick:float):
        if (abs(cur_inven) < tick and abs(self.last_inven) > tick) or (cur_inven * self.last_inven < 0 and abs(self.last_inven)> tick):
            res = self.count
            self.count = 0
            self.hit_count.append(res)
        self.last_inven = cur_inven
    
    def mm_order_count(self):
        self.count += 1
        
    def log_out(self):
        if not self.qty_sum:
            return 0
        res = self.time_qty_sum / self.qty_sum
        self.time_qty_sum = 0
        self.qty_sum = 0
        return res


class StateTracker():
    def __init__(self, logger, save_log, file_name) -> None:
        self.cache = dict()
        self.ref_update = []
        self.send_request = []
        self.cancel_request = []
        self.send_execution = []
        self.cancel_execution = []
        self.inventory_update = []

        self.logger = logger
        self.start_ts = int(time.time()*1e3)
        
        self.delay_time = deque(maxlen=10000)
        
        self.send_hedge_order = []
        self.on_order_cache = []
        self.trade_cache = []
        self.bbo_cache = []
        
        self.save_log = save_log
        self.file_name = file_name
        
    def add_on_order(self, order: PartialOrder):
        if not order.raw_data:
            return
        if not order.raw_data.get("o"):
            return
        rorder = order.raw_data["o"]
        if rorder:
            self.on_order_cache.append(rorder)
            
    def add_public_trade(self, trade: PublicTrade):
        self.trade_cache.append(trade.__dict__)
        
    def add_bbo(self, bbo: BBODepth):
        b = bbo.__dict__
        b["ask_price"] = Decimal(bbo.ask[0])
        b["ask_qty"] = Decimal(bbo.ask[1])
        b["bid_price"] = Decimal(bbo.bid[0])
        b["bid_qty"] = Decimal(bbo.bid[1])
        b["tx_time"] = bbo.meta["tx"]
        self.bbo_cache.append(b)
        
    def add_source(self, time_data: list, cid: str):
        self.cache[cid] = time_data.copy()

    def before_send(self, cid: str, curr_time: int):
        res = self.cache.get(cid)
        if res != None:
            res.append(curr_time)
            self.delay_time.append(res[-1] - res[1])
    
    def after_send(self, order: BinanceOrderRequest, curr_time: int, error_code=None):
        res = self.cache.get(order.new_client_order_id)
        if res != None:
            res.append(curr_time)
            d = order.__dict__
            if len(res) != 6:
                self.logger.critical(f"wrong time len:{len(res)}, cid:{order.new_client_order_id}")
                return
            d["tx_time"] = res[0]
            d["in_queue_time"] = res[1]
            d["out_queue_time"] = res[2]
            d["create_task_time"] = res[3]
            d["execution_time"] = res[4]
            d["finish_time"] = res[5]
            d["error_code"] = error_code
            self.send_hedge_order.append(d)
    
    def clear_cache(self, cid):
        self.cache.pop(cid, None)
            
    def gather_data(self, curr_time: int):
        if self.save_log:
            data = pd.DataFrame(self.send_hedge_order)
            data.to_csv(f"./data/{self.file_name}_send_hedge_order_{curr_time}.csv")
            
            data = pd.DataFrame(self.on_order_cache)
            data.to_csv(f"./data/{self.file_name}_order_message_{curr_time}.csv")
            
            data = pd.DataFrame(self.trade_cache)
            data.to_csv(f"./data/{self.file_name}_trade_{curr_time}.csv")
            
            data = pd.DataFrame(self.bbo_cache)
            data.to_csv(f"./data/{self.file_name}_bbo_{curr_time}.csv")
        
        self.send_hedge_order = []
        self.on_order_cache = []
        self.trade_cache = []
        self.bbo_cache = []
        
    def get_inner_delay_stats(self):
        return self.delay_time if self.delay_time else [0,0,0]

    def add_ref_update(self, best_ask_prc, best_ask_qty, best_bid_prc,
                       best_bid_qty, source, tx_time, timestamp, localtime,
                       in_queue_time, out_queue_time, update_time
                       ):
        self.ref_update.append(
            dict(
                best_ask_prc=best_ask_prc,
                best_ask_qty=best_ask_qty,
                best_bid_prc=best_bid_prc,
                best_bid_qty=best_bid_qty,
                source=source,
                tx_time=tx_time,
                timestamp=timestamp,
                localtime=localtime,
                in_queue_time=in_queue_time,
                out_queue_time=out_queue_time,
                update_time=update_time
            )
        )

    def add_send_request(self, source, decision_time, create_task_time, client_id):
        self.send_request.append(
            dict(
                source=source,
                decision_time=decision_time,
                create_task_time=create_task_time,
                client_id=client_id
            )
        )
        self.logger.info(f"save log send request")

    def add_cancel_request(self, decision_time, create_task_time, client_id):
        self.cancel_request.append(
            dict(
                decision_time=decision_time,
                create_task_time=create_task_time,
                client_id=client_id
            )
        )

    def add_send_execution(self, send_time, finish_time, client_id):
        self.send_execution.append(
            dict(
                send_time=send_time,
                finish_time=finish_time,
                client_id=client_id
            )
        )

    def add_cancel_execution(self, cancel_time, finish_time, client_id):
        self.cancel_execution.append(
            dict(
                cancel_time=cancel_time,
                finish_time=finish_time,
                client_id=client_id
            )
        )

    def add_inventory_update(self, timestamp, localtime, in_queue_time, out_queue_time, client_id, inventory):
        self.inventory_update.append(
            dict(
                timestamp=timestamp,
                localtime=localtime,
                in_queue_time=in_queue_time,
                out_queue_time=out_queue_time,
                client_id=client_id,
                inventory=inventory
            )
        )

    def save_data(self):
        data = pd.DataFrame(self.ref_update)
        data.to_csv(f"./data/ref_update_{self.start_ts}.csv")

        data = pd.DataFrame(self.send_request)
        data.to_csv(f"./data/send_request_{self.start_ts}.csv")

        data = pd.DataFrame(self.cancel_request)
        data.to_csv(f"./data/cancel_request_{self.start_ts}.csv")

        data = pd.DataFrame(self.send_execution)
        data.to_csv(f"./data/send_execution_{self.start_ts}.csv")

        data = pd.DataFrame(self.cancel_execution)
        data.to_csv(f"./data/cancel_execution_{self.start_ts}.csv")

        data = pd.DataFrame(self.inventory_update)
        data.to_csv(f"./data/inventory_update_{self.start_ts}.csv")

        self.ref_update = []
        self.send_request = []
        self.cancel_request = []
        self.send_execution = []
        self.cancel_execution = []
        self.inventory_update = []

        self.start_ts = int(time.time()*1e3)


class LoadBalancer:
    def __init__(self, ccy1: str, ccy2: str, logger) -> None:
        self._position_map = dict()
        self._balance_map = dict()
        self._account_load_map = dict()
        self._ip_load_map = dict()
        self._available_map = dict()
        self._locked_map = dict()

        self.ccy1 = ccy1
        self.ccy2 = ccy2

        self.send_counter = 0
        self.cancel_counter = 0
        self.query_counter = 0

        self.max_leverage = 10
        self.ban_name = set()

        self.logger = logger

    def add_send(self):
        self.send_counter += 1

    def sub_send(self):
        self.send_counter -= 1

    def add_cancel(self):
        self.cancel_counter += 1

    def add_query(self):
        self.query_counter += 1

    def ban_account(self, account_name):
        if account_name:
            self.ban_name.add(account_name)

    def ban_trader(self, trader):
        if trader in self._ip_load_map:
            self._ip_load_map.pop(trader)

    def free_account(self, account_name):
        if account_name in self.ban_name:
            self.ban_name.remove(account_name)

    def setup(self, acc_list: list, trader_list: list):
        acc_list = acc_list.copy()
        trader_list = trader_list.copy()
        for acc in acc_list:
            self._position_map[acc] = 0
            self._balance_map[acc] = 0
            self._account_load_map[acc] = 0
            self._available_map[acc] = dict()
            self._available_map[acc][self.ccy1] = 0
            self._available_map[acc][self.ccy2] = 0
            self._locked_map[acc] = 0

        for trader in trader_list:
            self._ip_load_map[trader] = 0

    def get_acc_list(self):
        return sorted(self._account_load_map, key=self._account_load_map.get)

    def get_trader_list(self):
        return sorted(self._ip_load_map, key=self._ip_load_map.get)

    def get_trader(self):
        return self.get_trader_list()[0]

    def update_available_balance(self, acc: str, available_balance: float, ccy: str):
        self._available_map[acc][ccy] = available_balance

    def split_order(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            new_order = order.clone()
            if order.side == Direction.long:
                temp_qty = self._available_map[acc][ccy2] * 0.99
                if total_qty*order.price > temp_qty:
                    new_order.quantity = temp_qty / order.price
                    new_order.account_name = acc
                    total_qty -= temp_qty / order.price
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break
            elif order.side == Direction.short:
                temp_qty = self._available_map[acc][ccy1] * 0.99
                if total_qty > temp_qty:
                    new_order.quantity = temp_qty
                    new_order.account_name = acc
                    total_qty -= temp_qty
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break

        return splited_orders

    def split_orders_perp(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            new_order = order.clone()
            allowed_qty = self.volume_allowed(
                acc=acc, side=order.side, prc=order.price)
            self.logger.info(
                f"!!!!!!!allowed quantity={allowed_qty}, side={order.side}")
            allowed_qty = 0.1
            if allowed_qty < 0:
                continue
            if total_qty > allowed_qty:
                new_order.quantity = allowed_qty
                new_order.account_name = acc
                splited_orders.append(new_order)
                total_qty -= allowed_qty
            else:
                new_order.quantity = total_qty
                new_order.account_name = acc
                splited_orders.append(new_order)
                break
        return splited_orders

    def send_order_perp(self, order: TransOrder):
        self.logger.info(f"send order:{order.__dict__}")
        if order.side == Direction.long:
            self._locked_map[order.account_name] += order.quantity
        elif order.side == Direction.short:
            self._locked_map[order.account_name] -= order.quantity
        self.logger.info(
            f"locked value:{self._locked_map[order.account_name]}")

    def finished_order_perp(self, torder: TransOrder, order_obj: Order):
        self.logger.info(f"finish torder:{torder.__dict__}")
        self.logger.info(f"finish order_obj:{order_obj.__dict__}")
        if torder.side == Direction.long:
            self._locked_map[torder.account_name] -= torder.quantity
            self._locked_map[torder.account_name] += order_obj.filled_qty
        elif torder.side == Direction.short:
            self._locked_map[torder.account_name] += torder.quantity
            self._locked_map[torder.account_name] -= order_obj.filled_qty
        else:
            raise NotImplementedError()
        self.logger.info(
            f"locked value:{self._locked_map[torder.account_name]}")

    def volume_allowed(self, acc: str, side: Direction, prc: float):
        if side == Direction.long:
            return self._balance_map[acc] * self.max_leverage - self._locked_map[acc] * prc
        else:
            return self._balance_map[acc] * self.max_leverage + self._locked_map[acc] * prc

    def send_order(self, order: TransOrder):
        if not order.account_name:
            raise RuntimeError("order with no account name")
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if order.side == Direction.long:
            self._available_map[order.account_name][ccy2] -= order.price * \
                order.quantity
        elif order.side == Direction.short:
            self._available_map[order.account_name][ccy1] -= order.quantity
        else:
            raise NotImplementedError()

        if self._available_map[order.account_name][ccy1] < 0 or self._available_map[order.account_name][ccy2] < 0:
            raise RuntimeError("order routing wrong")

    def finished_order(self, torder: TransOrder, order_obj: Order):
        _, contract = torder.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if torder.side == Direction.long:
            self._available_map[torder.account_name][ccy2] += torder.price * \
                torder.quantity
            self._available_map[torder.account_name][ccy2] -= order_obj.avg_fill_prc * \
                order_obj.filled_qty
            self._available_map[torder.account_name][ccy1] += order_obj.filled_qty
        elif torder.side == Direction.short:
            self._available_map[torder.account_name][ccy1] += torder.quantity
            self._available_map[torder.account_name][ccy1] -= order_obj.filled_qty
            self._available_map[torder.account_name][ccy2] += order_obj.avg_fill_prc * \
                order_obj.filled_qty
        else:
            raise NotImplementedError()

    def update_balance(self, acc: str, balance: float):
        self._balance_map[acc] = balance

    def update_position(self, acc: str, chg_qty: float):
        self._position_map[acc] += chg_qty

    def update_limit(self, acc: str, trader_name: str, acc_load: Union[str, None], ip_load: Union[str, None]):
        if acc_load != None:
            self._account_load_map[acc] = int(acc_load)
        if ip_load != None:
            self._ip_load_map[trader_name] = int(ip_load)

    def reduce_count(self, reduce: float):
        self._account_load_map[self.get_acc_list()[-1]] -= reduce
        self._ip_load_map[self.get_trader_list()[-1]] -= reduce


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.stop_close_amt = Decimal("0")
        self.limit_close_amt = Decimal("0")
        self.press_close_amt = Decimal("0")
        self.stop_pnl = Decimal("0")
        self.limit_pnl = Decimal("0")
        self.press_pnl = Decimal("0")
        self.save_log = StateTracker(logger=self.logger,save_log=self.config['strategy']['params']['save_log'], file_name=self.strategy_name)
        self.time_period = HoldingPeriodTracker()
        
        self.recvWindow = 100
        self.start_ts = time.time()
        self.trd_amt = 0
        self.oid_to_cid = dict()
        self.contract = self.config['strategy']['params']['contract']
        self.ex = self.config['strategy']['params']['ex']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.bnd_amt_in_coin = Decimal(
            self.config['strategy']['params']['bnd_amt_in_coin'])
        self.acc_limit = float(
            self.config['strategy']['params']['acc_limit'])
        self.lock_max = float(
            self.config['strategy']['params']['lock_max'])


        # self.prc_per_diff = 0.00005 / 5
        self._contract_mapper = ContractMapper()

        self.cfg: Config = ...

        self._xex = "xex"
        self._numeric_tol = 1e-10

        # TODO
        self._allow_inventory_mismatch_count = 0
        self._on_wakeup_count = 0
        self._last_scheduled_wakeup_ts = ...
        self.ws_queue: Dict[str, Queue] = {
            "trade": Queue(),
            "order": Queue(),
            "orderbook": Queue(),
            "bbo": LifoQueue(),
        }

        self.traders = [
            self.strategy_name,
        ]

        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()
        
        self.lastest_trade = Decimal("0")
        
        self.limbo_orders = deque(maxlen=100)

    def update_config_before_init(self):
        self.use_colo_url = True
        self.config['system']['logger']["max_size"] = 1024 * 1024 * 20   # 每个文件最大1M
        self.config['system']['logger']["backup_count"] = 10 # 1个backup，总计两个文件

    def build_momentum_length_cal(self, ex: str, halflife: float):
        return CalMomentumLength(halflife=halflife)

    def build_inven_ret_cal(self, ex: str, halflife: float):
        return CalInvenRet(contract=self.contract, halflife=halflife, logger=self.logger)

    def init_before_strategy_starts(self):
        self.cfg = Config(
            CONTRACT=self.contract,
            EXCHANGES=[
                self.ex,
            ],
            PRICE_STABILITY_BUFFER_WINDOW=2,
            MM_PRICE_STABILITY_BUFFER_WINDOW=100,
            FOLDER_NAME=f"",
            QUOTE_LVL_CONFIGS=dict(
                binance_busd=QuoteLevelConfig(
                    STOP_N_TICKS=10, QUOTE_AMT=float(self.config['strategy']['params']['quote_amt']), RECV_WINDOW=100),
                binance=QuoteLevelConfig(
                    STOP_N_TICKS=10, QUOTE_AMT=float(self.config['strategy']['params']['quote_amt']), RECV_WINDOW=100),
            ),
            LATENCY_T=100,
            AMOUNT_BOUND=10,
            PRESS_MODE=PressMode.all,
            PRESS_HEDGE_RATIO=0.02,
            PRESS_WAIT=0,
            PRESS_RECV_WINDOW=100,
            MIN_PRESS_AMT=100,
            LOCK_RATIO=0.1,
            MAX_LEVERAGE=50,
            MOMENTUM_LENGTH_HALFLIFE=10,
            INVEN_RET_HALFLIFE=10,
            TRADE_SIZE_CAL_HALFLIFE=100,
            BBO_PRESSURE_CAL_HALFLIFE=3,
            PROB_MASS_TOL=0.01,
            MIN_PROB_MASS=0.0,
            BBO_PRESSURE_TOL=5,
            MIN_BBO_PRESSURE_AMT=100000,  # U
            MAX_LEVERAGE_WINDOW=600000, 
            N_MM=1,
            MSG="",
            MAX_LOCK_INVEN=self.lock_max
        )
        self.contract = self.cfg.CONTRACT
        self.depth_manager = XExDepthManager(contract=self.contract)
        self._order_manager_map: AtTouchBinanceOrderRequestManagerMap = AtTouchBinanceOrderRequestManagerMap(contract=self.contract, logger=self.logger)
        
        self._inventory_manager = XexInventoryManager(contract=self.cfg.CONTRACT, logger=self.logger)
        self._inventory_manager.link_order_manamger_map(order_manamger_map=self._order_manager_map)
        self.latency_cal_map = defaultdict(lambda: CalEMAvgLatency(halflife=self.cfg.LATENCY_T))
        self.price_stability_cal_map = defaultdict(lambda: CalPriceStability())
        self.trade_size_cal_map = defaultdict(lambda: CalEMAvgTradeSize(halflife=self.cfg.TRADE_SIZE_CAL_HALFLIFE))
        self.bbo_pressure_cal_map = defaultdict(lambda: CalBBOPressure(halflife=self.cfg.BBO_PRESSURE_CAL_HALFLIFE))
        self.momentum_length_cal_map = DefaultDict(factory=lambda ex: self.build_momentum_length_cal(ex=ex, halflife=self.cfg.MOMENTUM_LENGTH_HALFLIFE))
        self.inven_ret_cal_map: Dict[str, CalInvenRet] = DefaultDict(factory=lambda ex: self.build_inven_ret_cal(ex=ex, halflife=self.cfg.INVEN_RET_HALFLIFE))

        self.executor = AtTouchAlgoOrderExecutor(
            contract=self.contract,
            exs=self.cfg.EXCHANGES,
            quote_level_configs=self.cfg.QUOTE_LVL_CONFIGS,
            amount_bound=self.cfg.AMOUNT_BOUND,
            press_ratio=self.cfg.PRESS_HEDGE_RATIO,
            press_wait=self.cfg.PRESS_WAIT,
            press_recv_window=self.cfg.PRESS_RECV_WINDOW,
            min_press_amt=self.cfg.MIN_PRESS_AMT,
            lock_ratio=self.cfg.LOCK_RATIO,
            min_quote_notional=5,
            max_leverage=self.cfg.MAX_LEVERAGE,
            prob_mass_tol=self.cfg.PROB_MASS_TOL,
            min_prob_mass=self.cfg.MIN_PROB_MASS,
            logger=self.logger,
            bbo_pressure_tol=self.cfg.BBO_PRESSURE_TOL,
            min_bbo_pressure_amt=self.cfg.MIN_BBO_PRESSURE_AMT,
            max_leverage_window=self.cfg.MAX_LEVERAGE_WINDOW,
            n_mm=self.cfg.N_MM,
            prc_stability_buffer_window=self.cfg.PRICE_STABILITY_BUFFER_WINDOW,
            mm_prc_stability_buffer_window=self.cfg.MM_PRICE_STABILITY_BUFFER_WINDOW,
            max_lock_inven=self.cfg.MAX_LOCK_INVEN


        )
        self.executor.link_inventory_manager(inventory_manager=self._inventory_manager)
        self.executor.link_order_manager_map(order_manager_map=self._order_manager_map)
        self.executor.link_depth_manager(depth_manager=self.depth_manager)
        self.executor.link_prc_stability_cal_map(prc_stability_cal_map=self.price_stability_cal_map)
        self.executor.link_trade_size_cal_map(trade_size_cal_map=self.trade_size_cal_map)
        self.executor.link_bbo_pressure_cal_map(bbo_pressure_cal_map=self.bbo_pressure_cal_map)
        self.executor.link_momentum_length_cal_map(momentum_length_cal_map=self.momentum_length_cal_map)
        self.executor.link_inven_ret_cal_map(inven_ret_cal_map=self.inven_ret_cal_map)

        for ex in self.cfg.EXCHANGES:
            self.executor.update_margin_balance(ex=ex, margin_balance=100)
        
        self.rate_limit_handler = RateLimitHandler(loop=self.loop, logger=self.logger, executor=self.executor)

        self._iids = set()
        for ex in self.cfg.EXCHANGES:
            self._iids.add(f"{ex}.{self.cfg.CONTRACT}")

        self._prc_precision = dict()
        for iid in self._iids:
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            self._prc_precision[iid] = price_precision[pp_iid]

        ccy = self.cfg.CONTRACT.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.load_balancer = LoadBalancer(
            ccy1=ccy1, ccy2=ccy2, logger=self.logger)
        self.load_balancer.setup(
            acc_list=self.account_names,
            trader_list=self.traders
        )

        self.canceling_id = set()
        self.quering_id = set()
        self.quering_triggerd_stop_id = set()

        self.send = False

    def iid_to_binance_symbol(self, iid: str):
        _, contract = iid.split(".")
        ccy = contract.split("_")
        if ccy[-1] == "swap" and ccy[1] in {"usdt", "busd"}:
            symbol = f"{iid}.usdt_contract.na"
        else:
            symbol = f"{iid}.spot.na"
        return symbol

    async def before_strategy_start(self):
        self.logger.setLevel(self.config['strategy']['params']['log_level'])
        self.init_before_strategy_starts()

        await self.account_preparation()

        for acc in self.account_names:
            ex, _ = acc.split(".")
            for piid in self._iids:
                iid: str = self._contract_mapper.proxy_iid_to_iid(
                    proxy_iid=piid)
                if ex != "binance" or not iid.startswith("binance"):
                    raise NotImplementedError(
                        f"{ex},{iid} piid to symbol mapping is absent")
                symbol = self.iid_to_binance_symbol(iid=iid)
                self.direct_subscribe_order_update(
                    symbol_name=symbol, account_name=acc)
                await asyncio.sleep(1)

        for piid in self._iids:
            iid: str = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
            if not iid.startswith("binance"):
                raise NotImplementedError(
                    f"{iid} piid to symbol mapping is absent")
            symbol = self.iid_to_binance_symbol(iid=iid)

            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            bnd = self.bnd_amt_in_coin / symbol_cfg["contract_value"]
            self.direct_subscribe_orderbook(
                symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_public_trade(symbol_name=symbol)
            self.direct_subscribe_bbo(symbol_name=symbol)

        self.loop.create_task(self.handle_ws_update())

        self.send = True

    async def account_preparation(self):
        # TODO
        for ex in self.cfg.EXCHANGES:
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                ex, _ = acc.split(".")
                for piid in self._iids:
                    iid: str = self._contract_mapper.proxy_iid_to_iid(
                        proxy_iid=piid)
                    if ex != "binance" or not iid.startswith("binance"):
                        raise NotImplementedError(
                            f"{ex},{iid} piid to symbol mapping is absent")
                    symbol = self.iid_to_binance_symbol(iid=iid)
                    try:
                        await api.flash_cancel_orders(self.get_symbol_config(symbol))
                    except:
                        pass
                    
                    
                    if self.ex == "binance":
                        ccy = "usdt"
                    elif self.ex == "binance_busd":
                        ccy = "busd"
                    cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                    self.load_balancer.update_balance(
                        acc=acc, balance=float(cur_balance[ccy]["all"]))
                    
                    self.executor.update_margin_balance(ex=self.ex, margin_balance=float(cur_balance[ccy]["all"]))

                    await asyncio.sleep(0.2)
                    cur_position = (await api.contract_position(self.get_symbol_config(symbol))).data
                    self.logger.critical(f"current position: {cur_position}")
                    pos = cur_position["long_qty"] - cur_position["short_qty"]

                    self._inventory_manager.set_inven(ex=self.ex, inven=float(pos))
                    self.logger.critical(f"{self._inventory_manager.get_inventory(ex=self.ex)} for {self.ex}")

                    await asyncio.sleep(3)

    # websocket handling
    async def on_order(self, order: PartialOrder):
        self.save_log.add_on_order(order=order)
        try:
            await self.ws_queue["order"].put((order, int(time.time()*1e3)))
        except:
            traceback.print_exc()

    async def on_public_trade(self, symbol, trade: PublicTrade):
        self.save_log.add_public_trade(trade=trade)
        try:
            await self.ws_queue["trade"].put((symbol, trade, [trade.transaction_ms, self.current_time()]))
        except:
            traceback.print_exc()

    async def on_bbo(self, symbol, bbo: BBODepth):
        self.save_log.add_bbo(bbo=bbo)
        try:
            await self.ws_queue["bbo"].put((symbol, bbo, [bbo.meta["tx"], self.current_time()]))
        except:
            traceback.print_exc()

    async def on_orderbook(self, symbol, orderbook):
        try:
            await self.ws_queue["orderbook"].put((symbol, orderbook, [orderbook["resp_ts"], self.current_time()]))
        except:
            traceback.print_exc()

    def handle_order(self, order: Union[PartialOrder, AtomOrder], in_queue_time: int):
        try:
            # self.logger.critical(f"on order:{order.__dict__}")
            om = self._order_manager_map
            cid = order.client_id
            request_order = om.get_order(cid=cid)
            if not request_order:
                # if isinstance(order, PartialOrder) and \
                #     order.extra.get("order_type") == OrderType.Limit and \
                #         order.xchg_status in OrderStatus.fin_status():
                self.logger.critical(f"order has been poped wrong, cid={cid}")
                return
            
            if order.xchg_status in OrderStatus.fin_status() and order.filled_amount > 0:
                self.trd_amt += abs(order.filled_amount) * order.avg_filled_price
            
            if order.xchg_status in OrderStatus.fin_status() and order.filled_amount>0 and request_order.tag == "mm":
                self.time_period.mm_order_count()
            
            if isinstance(order, PartialOrder):
                raw_order = order.raw_data.get("o") if order.raw_data else None
                
                if raw_order:
                    if raw_order.get("n"):
                        pnl = Decimal(raw_order["rp"]) - Decimal(raw_order["n"])
                    else:
                        pnl = Decimal(raw_order["rp"])
                else:
                    pnl = Decimal("0")
                if request_order.tag in {"stop", "stop_limit"}:
                    self.stop_pnl += pnl
                    if order.xchg_status in OrderStatus.fin_status():
                        self.stop_close_amt += abs(order.filled_amount) * order.avg_filled_price
                elif request_order.tag in {"hedge"}:
                    self.limit_pnl += pnl
                    if order.xchg_status in OrderStatus.fin_status():
                        self.limit_close_amt += abs(order.filled_amount) * order.avg_filled_price
                elif request_order.tag in {"ioc","press"}:
                    self.press_pnl += pnl
                    if order.xchg_status in OrderStatus.fin_status():
                        self.press_close_amt += abs(order.filled_amount) * order.avg_filled_price
                # elif request_order.tag not in {"mm","lock","ign"}:
                #     self.logger.critical(f"tag wrong order:{request_order.__dict__}")
            elif isinstance(order, AtomOrder) and order.xchg_status in OrderStatus.fin_status():
                if request_order.tag in {"stop", "stop_limit"}:
                    self.stop_close_amt += abs(order.filled_amount) * order.avg_filled_price
                elif request_order.tag in {"hedge"}:
                    self.limit_close_amt += abs(order.filled_amount) * order.avg_filled_price
                elif request_order.tag in {"ioc","press"}:
                    self.press_close_amt += abs(order.filled_amount) * order.avg_filled_price
                
            
            # if order.xchg_status in OrderStatus.fin_status() and order.filled_amount > 0:
            #     self.logger.info(f"order filled:{order.filled_amount}, cid:{order.client_id}, side:{order.extra.get('side')}")
            
        
            # self.logger.info(f"{order.__dict__}")
            order_msg: Order = Order.from_dict(
                porder=order,
                local_order=request_order,
                contract_value=self.get_symbol_config(
                    request_order.symbol)["contract_value"]
            )
            iid = request_order.piid
            ex, contract = iid.split(".")
            o = om.get_order(cid=cid)
            # if o:
            #     if o.tag == "close":
            #         self.logger.critical(f"close_order: cid={o.new_client_order_id},prc={o.price},qty={o.quantity},side={o.side},timeInForce={o.time_in_force},reduceOnly={o.reduce_only},status={order_msg.status}")
            # else:
            #     self.logger.critical(f"can't find order: cid={o.new_client_order_id},prc={o.price},qty={o.quantity},side={o.side},timeInForce={o.time_in_force},reduceOnly={o.reduce_only},status={order_msg.status}")
            
            if cid in om:
                """ Handle Reduce Only Hedge Limits """
                if om.is_hedge_limit(cid=cid):
                    o: BinanceLimitOrder = om.get_order(cid=cid)
                    # if o.tag == "hedge":
                    #     self.logger.critical(f"on order:cid={cid},price={order_msg.prc},qty={order_msg.qty},filled_qty={order_msg.filled_qty},status={order_msg.status}")
                    assert o.reduce_only, cid
                    o.quantity = order_msg.qty
                    o.unfilled_quantity = order_msg.qty - order_msg.filled_qty

                """ Handle Hedge Stop Orders """
                if om.is_hedge_stop(cid=cid) and order_msg.is_limit:
                    o: BinanceStopOrder = om.pop_stop_order(cid=cid)
                    if o.new_client_order_id in self.quering_triggerd_stop_id:
                        self.quering_triggerd_stop_id.remove(o.new_client_order_id)
                    o.quantity = order_msg.qty
                    o.unfilled_quantity = order_msg.qty - order_msg.filled_qty
                    lo = o.to_limit_order()
                    lo.tag = "stop_limit"
                    om.add_hedge_order(order=lo)
                    o.cancel = True
                    
                """ Update Inventory """
                if order_msg.filled_qty > self._numeric_tol:
                    self.logger.critical(f"order_msg: side={order_msg.side}, filled_qty={order_msg.filled_qty}, cid:{order_msg.client_id}")
                    if self._inventory_manager.inven_chged(order_msg=order_msg):
                        self._inventory_manager.update_inventory(order_msg=order_msg)
                        avg_fill_prc = self._inventory_manager.avg_fill_prc_map[ex]
                        self.inven_ret_cal_map[ex].on_inven_update(ex=ex,
                                                                   inven=self._inventory_manager.get_inventory(ex=ex),
                                                                   avg_filled_prc=avg_fill_prc,
                                                                   t=self.current_time())
                        ex_inven = self._inventory_manager.get_inventory(ex=ex)
                        # total_inven = self._inventory_manager.total_inventory
                        self.logger.critical(f"current inventory:{ex_inven}, cid:{order.client_id} for {ex}")
                        self.time_period.on_inven_change(cur_inven=ex_inven, tick=float(self.get_symbol_config(request_order.symbol)["qty_tick_size"]))

                        qty_chg = om.get_order_inc_fill_qty_in_tokens(order_msg=order_msg)
                        pos_chg = dict(
                            side=order_msg.side,
                            qty=qty_chg,
                            ts=time.time()
                        )
                        self.time_period.on_position(pos_chg=pos_chg)

                if order_msg.is_limit:
                    om.update_order_msg(order_msg=order_msg)
                    if order_msg.filled_qty > self._numeric_tol:
                        # self.logger.critical(f"TRACK_ORDER:cid={order_msg.client_id},oid={order_msg.order_id},filled_qty={order_msg.filled_qty}")
                        tracked_order = om.om_map[ex].get_order_msg(cid=order_msg.client_id)
                        # if tracked_order:
                        #     self.logger.critical(f"TRACKED_ORDER:cid={tracked_order.client_id},oid={tracked_order.order_id},filled_qty={tracked_order.filled_qty}")

                """ Clean up """
                order_status = order_msg.status
                if order_status in OrderStatus.fin_status():
                    if om.is_hedge(cid=cid):
                        if om.is_hedge_stop(cid=cid):
                            if order_msg.is_stop:
                                self.cancel_hedge_ignition_order(cid=om.stop_to_ign_cid(stop_cid=cid))
                                if order_status == OrderStatus.Expired:
                                    om.trigger_stop(ts=self.current_time(), cid=cid)
                                    request_order.canceled = True
                                    # logging.debug(f"ALGO_ORDER {order_status}: ts={ts},oid={oid}")
                                else:
                                    om.pop_stop_order(cid=cid)
                                    # logging.debug(f"Popped Stop Order {order_status}: ts={ts},oid={oid}")
                                
                        else:
                            if order_msg.is_stop:
                                if order_status == OrderStatus.Expired:
                                    pass
                                else:
                                    raise NotImplementedError()
                            else:
                                # if order_status == OrderStatus.Rejected:
                                #     o = om.get_order(oid=oid)
                                #     logging.debug(f"HEDGE_LIMIT_REJECTED: ts={ts},oid={oid},side={o.side},prc={o.price}")
                                is_ign = om.is_hedge_ignition(cid=cid)
                                if is_ign:
                                    is_filled = order_msg.status in {OrderStatus.Filled}
                                    is_failed_or_canceled = not is_filled
                                    if is_failed_or_canceled:
                                        stop_cid = om.ign_to_stop_cid(ign_cid=cid)
                                        # logging.debug(f"Cancel stop: cid={stop_cid}")
                                        self.cancel_hedge_stop_order(cid=stop_cid)
                                    else:
                                        pass
                                    om.pop_ignition_order(cid=cid)
                                else:
                                    om.pop_order(cid=cid)

                    else:
                        om.pop_order(cid=cid)
                        # logging.debug(f"Pop Order {order_status}: ts={ts},oid={oid}")

                return
            else:
                self.logger.critical(f"cid not in om: cid={cid},o={order_msg.__dict__}")
                
        except:
            self.logger.critical(traceback.format_exc())

    def handle_trade(self, symbol: str, trade: PublicTrade, time_data: list):
        self.lastest_trade = trade.price
        time_data.append(self.current_time())

        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        trade_obj = Trade.from_dict_prod(
            ex=ex,
            contract=contract,
            trade=trade
        )

        if not trade_obj.is_valid:
            return

        """ Latency state """
        self.latency_cal_map[ex].add_trade(trade=trade_obj)
        latency = self.latency_cal_map[ex].get_factor(ts=trade_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        self.depth_manager.on_trade(trade=trade_obj)

        price_stability_cal = self.price_stability_cal_map[ex]
        price_stability_cal.add_trade(trade=trade_obj)
        depth = self.depth_manager.depths_map.get(trade_obj.ex)
        if depth is None:
            return
        price_stability_cal.add_bbo(bbo=depth.to_bbo())

        self.trade_size_cal_map[ex].on_trade(trade=trade_obj)
        self.momentum_length_cal_map[ex].on_trade(trade=trade_obj)

        ts_gap = self.price_stability_cal_map[ex].get_factor(ts=trade_obj.tx_time)
        if not ts_gap:
            return
        wait_ts_gap = self.cfg.PRICE_STABILITY_BUFFER_WINDOW - ts_gap
        mm_wait_ts_gap = self.cfg.MM_PRICE_STABILITY_BUFFER_WINDOW - ts_gap
        next_wake_up_ts_gap = min(wait_ts_gap, mm_wait_ts_gap)
        if next_wake_up_ts_gap <= 0:
            press_on = False
            if self.cfg.PRESS_MODE in {PressMode.all}:
                press_on = True
            self.update_orders(press_on=press_on, time_data=time_data)
        else:
            wakeup_ts = trade_obj.local_time + next_wake_up_ts_gap
            if (self._last_scheduled_wakeup_ts is not Ellipsis) and (wakeup_ts <= self._last_scheduled_wakeup_ts):
                return
            self.loop.create_task(
                self.insertWakeup(data=dict(
                timestamp=wakeup_ts,
                contract=trade_obj.contract,
                ex=trade_obj.ex
            ), time_data=time_data))
            self._last_scheduled_wakeup_ts = wakeup_ts

    async def insertWakeup(self, data, time_data):
        await asyncio.sleep(self.cfg.PRICE_STABILITY_BUFFER_WINDOW)
        self.onWakeup(data, time_data=time_data)

    def handle_bbo(self, symbol: str, bbo: BBODepth, time_data: list):
        time_data.append(self.current_time())
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        bbo_obj = BBO.from_dict_prod(iid=iid, bbo_depth=bbo)

        if not bbo_obj.valid_bbo:
            return

        contract = bbo_obj.meta.contract
        ex = bbo_obj.meta.ex
        server_ts = bbo_obj.server_time
        local_ts = bbo_obj.local_time

        ism = self.iid_state_manager
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Latency state """
        self.latency_cal_map[ex].add_bbo(bbo=bbo_obj)
        latency = self.latency_cal_map[ex].get_factor(ts=bbo_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        """ Server time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(
                iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.is_active(iid=iid):
            return

        self.depth_manager.on_bbo(bbo=bbo_obj)
        self.bbo_pressure_cal_map[ex].on_bbo(bbo=bbo_obj)

        price_stability_cal = self.price_stability_cal_map[ex]
        price_stability_cal.add_bbo(bbo=bbo_obj)
        ts_gap = self.price_stability_cal_map[ex].get_factor(ts=bbo_obj.server_time)
        if not ts_gap:
            return
        wait_ts_gap = self.cfg.PRICE_STABILITY_BUFFER_WINDOW - ts_gap
        mm_wait_ts_gap = self.cfg.MM_PRICE_STABILITY_BUFFER_WINDOW - ts_gap
        next_wake_up_ts_gap = min(wait_ts_gap, mm_wait_ts_gap)
        if next_wake_up_ts_gap <= 0:
            press_on = False
            if self.cfg.PRESS_MODE in {PressMode.all}:
                press_on = True
            self.update_orders(press_on=press_on, time_data=time_data)
        else:
            wakeup_ts = bbo_obj.local_time + next_wake_up_ts_gap
            if (self._last_scheduled_wakeup_ts is not Ellipsis) and (wakeup_ts <= self._last_scheduled_wakeup_ts):
                return
            self.loop.create_task(
                self.insertWakeup(data=dict(
                timestamp=wakeup_ts,
                ccontract=bbo_obj.meta.contract,
                ex=bbo_obj.meta.ex,
            ), time_data=time_data))
            self._last_scheduled_wakeup_ts = wakeup_ts



    def handle_orderbook(self, symbol: str, orderbook, time_data: list):
        time_data.append(self.current_time())
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        ccy = contract.split("_")
        depth_obj = Depth.from_dict(
            contract=contract,
            ccy1=ccy[0],
            ccy2=ccy[1],
            ex=ex,
            depth=orderbook
        )
        ex = depth_obj.meta_data.ex
        contract = depth_obj.meta_data.contract
        server_ts = depth_obj.server_ts
        local_ts = depth_obj.resp_ts

        """ Latency state """
        self.latency_cal_map[ex].add_depth(depth=depth_obj)
        latency = self.latency_cal_map[ex].get_factor(ts=depth_obj.resp_ts)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=True)

        """ tx time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(
                iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        ism = self.iid_state_manager
        """ Activate stale state """
        is_in_stale_state = ism.is_in_stale_state(iid=iid)
        if is_in_stale_state:
            ism.enter_nonstale_state(iid=iid)
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.active_iids:
            return

        if not ism.is_active(iid=iid):
            return

        self.depth_manager.on_depth(depth=depth_obj)
        self.bbo_pressure_cal_map[ex].on_depth(depth=depth_obj)

        price_stability_cal = self.price_stability_cal_map[ex]
        price_stability_cal.add_bbo(bbo=depth_obj.to_bbo())

        press_on = False
        if self.cfg.PRESS_MODE in {PressMode.all, PressMode.depth}:
            press_on = True
        self.update_orders(press_on=press_on, time_data=time_data)


    async def handle_ws_update(self):
        while True:
            try:
                if not self.ws_queue["order"].empty():
                    order, in_queue_time = await self.ws_queue["order"].get()
                    self.handle_order(order, in_queue_time)
                    continue
                elif not self.ws_queue["orderbook"].empty():
                    symbol, orderbook, time_data = await self.ws_queue["orderbook"].get()
                    self.handle_orderbook(symbol, orderbook, time_data)
                    continue
                elif not self.ws_queue["trade"].empty():
                    symbol, trade, time_data = await self.ws_queue["trade"].get()
                    self.handle_trade(symbol, trade, time_data)
                    continue
                elif not self.ws_queue["bbo"].empty():
                    symbol, bbo, time_data = await self.ws_queue["bbo"].get()
                    self.ws_queue["bbo"] = LifoQueue()
                    self.handle_bbo(symbol, bbo, time_data)
                await asyncio.sleep(0.0001)
            except:
                await asyncio.sleep(0.0001)
                traceback.print_exc()

    # base operation, send, cancel, query with load balancer
    def sendBinanceOrder(self, order: BinanceOrderRequest, time_data = None):
        self.trim_order_before_sending(order)
        if time_data:
            time_data.append(self.current_time())
            self.save_log.add_source(time_data, order.new_client_order_id)
        self.loop.create_task(self.base_send_order(order=order))
        
    def sendBinanceOrderDouble(self, orders: List[BinanceOrderRequest], time_data: list):
        for order in orders:
            self.trim_order_before_sending(order)
        self.loop.create_task(self.base_send_order_double(orders=orders, time_data=time_data.copy()))
        
    async def base_send_order_double(self, orders: List[BinanceOrderRequest], time_data: list):
        tdata = time_data.copy()
        for order in orders:
            if tdata:
                t = tdata.copy()
                t.append(self.current_time())
                self.save_log.add_source(time_data=t, cid=order.new_client_order_id)
            self.loop.create_task(self.base_send_order(order=order))
            await asyncio.sleep(0.0005)
        
    async def base_send_order(self, order: BinanceOrderRequest):
        self.save_log.before_send(cid=order.new_client_order_id,curr_time=self.current_time())
        error_code=0
        if not order.reduce_only and not self.volume_notional_check(symbol=order.symbol, price=order.price, qty=order.quantity):
            self.loop.create_task(self.internal_fail(order=order, error_code=error_code))
            return
        try:
            origin_resp = await self.send_order(order=order)
            if isinstance(origin_resp, ApiResponse):
                resp = origin_resp.data
                headers = origin_resp.headers
            else:
                resp = None
                headers = None
                error_code = origin_resp

            if headers:
                self.load_balancer.update_limit(
                    acc=order.account_name,
                    trader_name=self.strategy_name,
                    acc_load=headers.get(ACC_LOAD_KEY),
                    ip_load=headers.get(IP_LOAD_KEY)
                )
                self.logger.info(f"order load:{int(headers.get(ACC_LOAD_KEY))}, ip load:{headers.get(IP_LOAD_KEY)}")
                if headers.get(ACC_LOAD_KEY) and int(headers.get(ACC_LOAD_KEY)) > self.acc_limit / 3:
                    self.handle_rate_limit(msg=0)
                    if int(headers.get(ACC_LOAD_KEY)) > self.acc_limit * 0.8:
                        self.handle_rate_limit(msg=-1013)
                    if int(headers.get(ACC_LOAD_KEY)) > self.acc_limit * 0.95:
                        self.handle_rate_limit(msg=-1012)
                elif headers.get(ACC_LOAD_KEY) and int(headers.get(ACC_LOAD_KEY)) <= self.acc_limit / 3:
                    self.rate_limit_handler.free_stop_order()
            if resp:
                order.update(order=resp)
                self.save_log.after_send(order=order, curr_time=self.current_time())
            else:
                self.loop.create_task(self.internal_fail(order=order, error_code=error_code))
                self.save_log.after_send(order=order, curr_time=self.current_time(), error_code=error_code)

            self.save_log.clear_cache(cid=order.new_client_order_id)
        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"base send order err:{err}")
            self.loop.create_task(self.internal_fail(order=order, error_code=error_code))
            self.save_log.clear_cache(cid=order.new_client_order_id)
            
    async def send_order(self, order: BinanceOrderRequest):
        symbol = order.symbol
        try:
            self._account_config_[
                order.account_name]["cfg_pos_mode"][symbol] = 1
            side = self.direction_map(side=order.side)
            symbol_cfg = self.get_symbol_config(symbol)
            pt = symbol_cfg['price_tick_size']
            if order.stop_price > 0:
                new_stop_price = Decimal(f"{int((Decimal(order.stop_price) + pt/Decimal('2')) / pt) * pt:f}")
                # here for preventing trigger immediately
                if order.side == Side.long and self.lastest_trade >= new_stop_price:
                    return -2021
                elif order.side == Side.short and self.lastest_trade <= new_stop_price:
                    return -2021
                resp: ApiResponse = await self.raw_make_order(
                    symbol_name=symbol,
                    price=Decimal(order.price) +
                    symbol_cfg["price_tick_size"]/Decimal("2"),
                    volume=Decimal(order.quantity) +
                    symbol_cfg["qty_tick_size"]/Decimal("2"),
                    side=side,
                    order_type=order.order_type_atom,
                    position_side=order.position_side_atom,
                    client_id=order.new_client_order_id,
                    account_name=order.account_name,
                    recvWindow=self.recvWindow,
                    stopPrice=new_stop_price,
                    reduce_only=order.reduce_only
                )
            else:
                resp: ApiResponse = await self.raw_make_order(
                    symbol_name=symbol,
                    price=Decimal(order.price) +
                    symbol_cfg["price_tick_size"]/Decimal("2"),
                    volume=Decimal(order.quantity) +
                    symbol_cfg["qty_tick_size"]/Decimal("2"),
                    side=side,
                    order_type=order.order_type_atom,
                    position_side=order.position_side_atom,
                    client_id=order.new_client_order_id,
                    account_name=order.account_name,
                    recvWindow=self.recvWindow,
                    reduce_only=order.reduce_only
                )
            # self.save_log.add_send_execution(send_time=send_time, finish_time=int(time.time()*1e3), client_id=order.new_client_order_id)
            return resp

        except RateLimitException as err:
            code = int(json.loads(err.err_message).get("code"))
            self.logger.critical(f"rate limit exception code:{code}")
            self.handle_rate_limit(msg=code)
            return code 
        except Exception as err:
            code = int(json.loads(err.err_message).get("code"))
            if code == -2022:
                ex,_ = order.piid.split(".")
                self.logger.info(f"current inventory={self._inventory_manager.get_inventory(ex=ex)}")
                om = self.get_hedge_order_manager(ex)
                self.logger.info(f"HEDGE LIMIT OUTSTANDING_ORDERS:")
                for oid, o in om.outstanding_orders.items():
                    if isinstance(o, BinanceLimitOrder):
                        self.logger.info(f"oid={oid},o={o}")
            return code
    
    
    def send_cancel_order(self, order: BinanceOrderRequest):
        self.loop.create_task(self.handle_cancel_order(order=order))

    async def handle_cancel_order(self, order: BinanceOrderRequest):
        async def unit_cancel_order(order: BinanceOrderRequest):
            try:
                cancel_time = int(time.time()*1e3)
                origin_resp: ApiResponse = await self.cancel_order(order=order)
                if origin_resp:
                    resp = origin_resp.data
                    headers = origin_resp.headers
                else:
                    resp = False
                    headers = None

                if resp == True:
                    order.canceled = True
                if headers:
                    self.load_balancer.update_limit(
                        acc=order.account_name,
                        trader_name=self.strategy_name,
                        acc_load=headers.get(ACC_LOAD_KEY),
                        ip_load=headers.get(IP_LOAD_KEY)
                    )
                    if headers.get(IP_LOAD_KEY) and int(headers.get(IP_LOAD_KEY)) > 2000:
                        self.handle_rate_limit(msg=-1012)
            except Exception as err:
                self.logger.info(f'cancel order {order.xchg_id} err {err}')
        
        cid = order.new_client_order_id
        if cid in self.canceling_id:
            return

        self.canceling_id.add(cid)

        counter = 0
        while True:
            o: BinanceOrderRequest = self._order_manager_map.get_order(cid=cid)
            if o:
                if o.canceled:
                    self.canceling_id.remove(cid)
                    return
            else:
                self.canceling_id.remove(cid)
                return
            if not self.rate_limit_handler.execution_is_ban():
                counter += 1
                self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(0.2)

            if counter > 30:
                o = self._order_manager_map.get_order(cid=cid)
                self.logger.critical(f"cancel order for too many times, cid={o.new_client_order_id}")
                if o:
                    o.canceled = True

    async def cancel_order(self, order: BinanceOrderRequest, catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        try:
            symbol = self.get_symbol_config(order.symbol)
            api = self.get_exchange_api_by_account(order.account_name)
            r = await api.cancel_order(symbol=symbol, order_id=order.order_id, client_id=order.new_client_order_id)
            self.logger.info(
                f"[cancel order]: {order.order_id}/{order.new_client_order_id} {r.data}")
            return r
        except RateLimitException as err:
            self.logger.critical(f"cancel order failed with rate limit: {err}")
            self.handle_rate_limit(msg=-1003)
            return None
        except Exception as err:
            if not catch_error:
                raise
            self.logger.info(f"cancel order failed: {err}, cid={order.new_client_order_id}")
            return None
        
    async def unit_query_order(self, order: BinanceOrderRequest):
        try:
            origin_resp = await self.query_order(order=order)
            if origin_resp:
                resp = origin_resp.data
                headers = origin_resp.headers
            else:
                resp = None
                headers = None

            if headers:
                self.load_balancer.update_limit(
                    acc=order.account_name,
                    trader_name=self.strategy_name,
                    acc_load=headers.get(ACC_LOAD_KEY),
                    ip_load=headers.get(IP_LOAD_KEY)
                )
                if headers.get(IP_LOAD_KEY) and int(headers.get(IP_LOAD_KEY)) > 2000:
                    self.handle_rate_limit(msg=-1012)
        except:
            return
        
        if not resp:
            return
        if resp.xchg_status in OrderStatus.fin_status():
            await self.on_order(resp)
        else:
            self.loop.create_task(self.handle_cancel_order(order))

    async def handle_query_order(self, order: BinanceOrderRequest):
        cid = order.new_client_order_id
        if cid in self.quering_id:
            return
        
        if not order.canceled:
            return
        
        if time.time()*1e3 - order.sent_ts < 10*60*1000:
            return

        self.quering_id.add(cid)

        counter = 0
        while True:
            o = self._order_manager_map.get_order(cid=cid)
            if o:
                if time.time()*1e3 - order.sent_ts < 10*60*1000:
                    self.quering_id.remove(cid)
                    return
            else:
                self.quering_id.remove(cid)
                return

            if not self.rate_limit_handler.execution_is_ban():
                counter += 1
                self.loop.create_task(self.unit_query_order(order))
            await asyncio.sleep(60)

            if counter > 100:
                o = self._order_manager_map.get_order(cid=cid)
                if o:
                    # self.loop.create_task(self.internal_fail(order=o, error_code=0))
                    self.logger.critical(f"query order for too many times, cid={o.new_client_order_id}, times:{counter}")
                    self.limbo_orders.append(o.new_client_order_id)
                    return
                
    async def handle_query_expired_stop_order(self, order: BinanceOrderRequest):
        if self.rate_limit_handler.execution_is_ban():
            return
        if self.current_time() - order.timestamp < 10000:
            return
        
        cid = order.new_client_order_id
        if cid in self.quering_triggerd_stop_id:
            return 
        
        self.quering_triggerd_stop_id.add(cid)
        
        
        res: ApiResponse = await self.query_order(order=order)
        if not res:
            return
        
        res_order: AtomOrder = res.data
        
        # pop pending trigger only if the type is still stop and status is expired
        if res_order.raw_data:
            # self.logger.critical(f"stop order expired check:{res_order.raw_data}")
            if res_order.raw_data.get("status") == "EXPIRED" and res_order.raw_data.get("type") == "STOP" and res_order.raw_data.get("updateTime"):
                if self.current_time() - res_order.raw_data.get("updateTime") > 10000:
                    self._order_manager_map.pop_pending_trigger_stop(cid=order.new_client_order_id)
                    # self.logger.critical(f"stop order actually expired, cid:{order.new_client_order_id}")
        
        if cid in self.quering_triggerd_stop_id:
            self.quering_triggerd_stop_id.remove(cid)
            
        if res.headers:
            self.load_balancer.update_limit(
                acc=order.account_name,
                trader_name=self.strategy_name,
                acc_load=res.headers.get(ACC_LOAD_KEY),
                ip_load=res.headers.get(IP_LOAD_KEY)
            )
            if res.headers.get(IP_LOAD_KEY) and int(res.headers.get(IP_LOAD_KEY)) > 2000:
                self.handle_rate_limit(msg=-1012)
        
    async def query_order(self, order: BinanceOrderRequest):
        try:
            acc = order.account_name
            if not acc:
                acc = self.get_exchange_api_by_account(self.account_names[0])
            api = self.get_exchange_api_by_account(order.account_name)
            resp: ApiResponse = await api.order_match_result(
                symbol=self.get_symbol_config(order.symbol),
                order_id=order.order_id,
                client_id=order.new_client_order_id,
            )
            return resp
        except RateLimitException as err:
            self.logger.critical(f"query order failed with rate limit: {err}")
            self.handle_rate_limit(msg=-1003)
            return None
        except Exception as err:
            self.logger.critical(f"query order failed: {err}, cid={order.new_client_order_id}")
            return None

    def handle_rate_limit(self,msg: int):
        if msg == -1003:
            self.logger.info(f"ban execution:{msg}")
            self.rate_limit_handler.ban_execution(sec=30)
        elif msg == -1013:
            self.rate_limit_handler.ban_stop_order()
        elif msg == -1012:
            self.rate_limit_handler.ban_hedge_order(sec=10)
            for ex in self.executor.exs:
                oco = self._order_manager_map.om_map[ex].h_orm.outstanding_close_order
                if oco:
                    continue
                o = self.executor.get_inven_flatten_order(ex=ex, ratio=1, prc_buffer=0.001)
                self.send_hedge_close_order(order=o, time_data=[])
                self.logger.critical(f"msg == -1012, oco={oco}")
        elif msg == -1015:
            # self.logger.info(f"ban stop hedge orders:{msg}")
            self.rate_limit_handler.ban_hedge_order(sec=10)
        elif msg == 0:
            # self.logger.info(f"ban mm orders:{msg}")
            self.rate_limit_handler.ban_mm_order(sec=10)
        
    # strategy base function override
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob

    # strategy utils only for prod
    def direction_map(self, side: Union[Direction, Side]):
        if side == Direction.long or side == Side.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell

    def current_time(self):
        return int(time.time()*1e3)

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"] - Decimal(self._numeric_tol):
            return False
        if Decimal(price * qty) < config["min_notional_val"] - Decimal(self._numeric_tol):
            return False
        return True

    async def internal_fail(self, order: BinanceOrderRequest, error_code: int):
        self.logger.info(f"internal failed {error_code}, order={order}")
        try:
            order = PartialOrder(
                account_name=order.account_name,
                xchg_id=None,
                exchange_pair="",
                client_id=order.new_client_order_id,
                xchg_status=OrderStatus.Failed,
                filled_amount=0,
                avg_filled_price=0,
                commission_fee=0,
                local_ms=int(time.time() * 1e3),
                server_ms=int(time.time() * 1e3),
                raw_data={},
                extra=dict(
                    order_type=order.order_type_atom,
                    price=order.price,
                    quantity=order.quantity
                )
            )
            await self.on_order(order)
        except:
            traceback.print_exc()

    def trim_order_before_sending(self, order: BinanceOrderRequest):
        iid = self._contract_mapper.proxy_iid_to_iid(order.piid)
        ex, _ = iid.split(".")
        order.generate_symbol(self._contract_mapper)
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        order.symbol_id = self.get_symbol_config(order.symbol)["id"]
        order.new_client_order_id = cid
        order.sent_ts = int(time.time()*1e3)
        order.account_name = self.account_names[0]

        order.quantity = round(order.quantity, qty_precision[iid])
        
        self._order_manager_map.cid_to_ex_map[cid] = order.piid.split(".")[0]

        return cid

    def check_orders(self, order_manager: AtTouchBinanceOrderRequestManager):
        for oid, order in order_manager.get_outstanding_orders().items():
            self.loop.create_task(self.handle_query_order(order=order))
            
        for oid, order in order_manager.mm_orm.pending_cancel_orders.items():
            self.loop.create_task(self.handle_query_order(order=order))
            
        for oid, order in order_manager.h_orm.pending_cancel_orders.items():
            self.loop.create_task(self.handle_query_order(order=order))
            
    def check_expired_stop_orders(self, orders_list: Deque[BinanceStopOrder]):
        for order in orders_list:
            self.loop.create_task(self.handle_query_expired_stop_order(order=order))

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))

    def deactivate(self, iid: str):
        ex, contract = iid.split(".")
        if ex in self.cfg.EXCHANGES:
            if ex in self.depth_manager:
                self.depth_manager.pop(ex=ex)

        self.cancel_all_orders(ex=ex)

    def _update_latency(self, iid: str, latency: float, allow_rectivate: bool):
        ism = self.iid_state_manager
        is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
        is_active = ism.is_active(iid=iid)
        if not is_in_high_latency_state:
            if ism.is_high_latency(iid=iid, latency=latency):
                ism.enter_high_latency_state(iid=iid)
                if is_active:
                    self.deactivate(iid=iid)
        else:  # is_in_high_latency_state
            if not ism.is_high_latency(iid=iid, latency=latency):
                if allow_rectivate:
                    ism.enter_normal_latency_state(iid=iid)

    def _update_ref_prc_suit(self, contract: str, ex: str):
        success = self._ref_prc_xex_bbo.update_ref_prc(ex=ex)
        if not success:
            return False
        return True

    def onWakeup(self, data, time_data: list):
        self._on_wakeup_count += 1
        ts = data["timestamp"]
        ex = data["ex"]
        price_stability_cal = self.price_stability_cal_map[ex]
        price_stable = price_stability_cal.get_factor(ts=ts)
        if price_stable == PriceStabilityState.stable:
            time_data.append(self.current_time())
            press_on = False
            if self.cfg.PRESS_MODE in {PressMode.all}:
                press_on = True
            self.update_orders(press_on=press_on, time_data=time_data)
            
    def update_orders(self, press_on: bool, time_data: list):
        ts = self.current_time()
        tdm = self.executor.get_orders(t=ts, press_on=press_on)

        for ex, (mm_tdc, hlmt_tdc, hstop_tdc) in tdm.items():
            ### hedge
            for cid in hlmt_tdc.limit_orders_to_cancel:
                self.cancel_hedge_limit_order(cid)
            for o in hlmt_tdc.close_order_to_send:
                self.send_hedge_close_order(order=o, time_data=time_data)
            for o in hlmt_tdc.ioc_orders_to_send:
                self.send_hedge_ioc_orders(order=o, time_data=time_data)
            for o in hlmt_tdc.limit_orders_to_send:
                self.send_hedge_limit_order(order=o, time_data=time_data)
            # stop
            for spcid in hstop_tdc.stop_orders_to_cancel:
                assert isinstance(spcid, StopPackCID)
                self.cancel_hedge_ignition_order(cid=spcid.ign)
                # cancel stop after ign is successfully canceled
            for int_prc, sp_dll in hstop_tdc.stop_orders_to_send.items():
                for sp in sp_dll:
                    self.send_hedge_stop_pack(stop_pack=sp)
            ### MM
            for cid in mm_tdc.lock_orders_to_cancel:
                self.cancel_lock_order(cid=cid)
            for o in mm_tdc.lock_orders_to_send:
                self.send_lock_order(order=o, time_data=time_data)
            for cid in mm_tdc.limit_orders_to_cancel:
                self.cancel_mm_limit_order(cid=cid)
            for o in mm_tdc.limit_orders_to_send:
                self.send_mm_limit_order(order=o)
                    
        return


    def get_hedge_order_manager(self, ex: str) -> AtTouchHedgeBinanceOrderRequestManager:
        return self._order_manager_map.om_map[ex].h_orm

    def send_mm_limit_order(self, order: BinanceLimitOrder) -> bool:
        iid = order.piid
        assert iid in self._iids
        order.timestamp = self.current_time()
        if not self.send:
            return
        ex, _ = iid.split(".")
        # self.logger.info(f"before send {order}")
        order.reduce_only = False
        self.sendBinanceOrder(order=order)
        self._order_manager_map.add_mm_order(order=order)
        # self.logger.info(
        #     f"""
        #     bid outstanding cid={self._order_manager_map.om_map[ex].mm_orm.outstanding_bid_oid}, 
        #     ask outstanding cid={self._order_manager_map.om_map[ex].mm_orm.outstanding_ask_oid}
        #     """
        #     )
    
    def send_hedge_ioc_orders(self, order: BinanceLimitOrder, time_data:list):
        self.send_hedge_limit_order(order=order, time_data=time_data)
        
    def send_hedge_limit_order(self, order: BinanceLimitOrder, time_data: list) -> bool:
        # self.logger.critical("sending hedge limit")
        order.timestamp = self.current_time()
        
        if not self.send:
            return
        if self.rate_limit_handler.hedge_is_ban():
            return
        order.reduce_only = True
        if order.tag in {"press"}:
            orders = [order]
            for _ in range(20):
                orders.append(order.clone())
            self.sendBinanceOrderDouble(orders=orders, time_data=time_data.copy())
            for trimed_order in orders:
                self._order_manager_map.add_hedge_order(order=trimed_order)
        else:
            self.sendBinanceOrder(order=order, time_data=time_data.copy())
            self._order_manager_map.add_hedge_order(order=order)
            
    def send_hedge_close_order(self, order: BinanceLimitOrder, time_data: list) -> bool:
        order.timestamp = self.current_time()
        self.logger.info(f"sending close order:{order}")
        if not self.send:
            return
        if self.rate_limit_handler.hedge_is_ban():
            return
        self.sendBinanceOrder(order=order, time_data=time_data.copy())
        self._order_manager_map.add_close_order(order=order)

    def send_lock_order(self, order: BinanceLimitOrder, time_data: list) -> bool:
        order.timestamp = self.current_time()
        if not self.send:
            return
        if self.rate_limit_handler.hedge_is_ban():
            return
        
        self.sendBinanceOrder(order=order, time_data=time_data.copy())
        # logging.debug(f"Sent HEDGE.{order.__class__.__name__} order {resp.order_id}: ts={self.current_time()},ex={ex},d={order.side},p={order.price},q={order.quantity},tp={order.stop_price}")
        self._order_manager_map.add_lock_order(order=order)

    def send_hedge_stop_pack(self, stop_pack: StopPack) -> bool:
        if not self.send:
            return
        if self.rate_limit_handler.stop_is_ban():
            return
        t = self.current_time()
        stop_pack.stop.timestamp = t
        stop_pack.ign.timestamp = t
        
        stop_pack.stop.generate_symbol(self._contract_mapper)
        symbol_cfg = self.get_symbol_config(stop_pack.stop.symbol)
        pt = symbol_cfg['price_tick_size']
        new_stop_price = Decimal(f"{int((Decimal(stop_pack.stop.stop_price) + pt/Decimal('2')) / pt) * pt:f}")
        # here for preventing trigger immediately
        if stop_pack.stop.side == Side.long and self.lastest_trade >= new_stop_price:
            return
        elif stop_pack.stop.side == Side.short and self.lastest_trade <= new_stop_price:
            return
        
        self.sendBinanceOrder(order=stop_pack.stop)
        self.sendBinanceOrder(order=stop_pack.ign)

        # self.logger.info(f"send hedge stop order:{order}")
        self._order_manager_map.add_stop_pack(stop_pack=stop_pack)

    def cancel_mm_limit_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_order(cid=cid)
        if o is None:
            return
        if not o.cancel:
            self.send_cancel_order(order=o)
            self._order_manager_map.cancel_mm_order(cid=cid)

    def cancel_hedge_limit_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_order(cid=cid)
        if not o:
            return
        if not o.cancel:
            self.send_cancel_order(order=o)
            self._order_manager_map.cancel_hedge_order(cid=cid)
    
    def cancel_lock_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_order(cid=cid)
        if not o:
            return
        if not o.cancel:
            self.send_cancel_order(order=o)
            self._order_manager_map.cancel_lock_order(cid=cid)
    
    def cancel_hedge_ignition_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_order(cid=cid)
        if not o:
            return
        if not o.cancel:
            self.send_cancel_order(order=o)
            self._order_manager_map.cancel_ignition_order(cid=cid)

    def cancel_hedge_stop_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_stop_order(cid=cid)
        if o is None:
            return
        if not o.cancel:
            self.send_cancel_order(order=o)
            self._order_manager_map.cancel_stop_order(cid=cid)

    def cancel_all_orders(self, ex: str) -> bool:
        om = self._order_manager_map.om_map[ex]
        for oid, o in om.get_outstanding_orders().items():
            self.send_cancel_order(order=o)
        om.cancel_all()

    # strategy main logic
    async def reset_pending_cancel_and_hedge_order(self):
        while True:
            await asyncio.sleep(1)
            for _, om in self._order_manager_map.om_map.items():
                self.check_orders(order_manager=om)
                
    async def reset_triggered_stop_order(self):
        while True:
            await asyncio.sleep(1)
            for ex, _ in self._order_manager_map.om_map.items():
                orders_list = self._order_manager_map.get_pending_trigger_orders(ex=ex)
                if orders_list:
                    self.loop.create_task(self.handle_query_expired_stop_order(order=orders_list[0]))
                
    async def load_balancer_reduce_count(self):
        while True:
            await asyncio.sleep(1)
            self.load_balancer.reduce_count(1)

    # stats
    async def get_holding_period(self):
        while True:
            await asyncio.sleep(300)
            try:
                self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                f"holding period": float(self.time_period.log_out()),
                            }
                        )
                    )
            except Exception as err:
                self.logger.critical(f"sending holding period info to grafana err: {err}")
                traceback.print_exc()
                
    async def get_inventory(self):
        while True:
            await asyncio.sleep(5)
            try:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt",
                        tag={"sn": self.strategy_name},
                        fields={
                            "inventory": float(self._inventory_manager.get_inventory(ex=self.cfg.EXCHANGES[0])),
                        }
                    )
                )
            except Exception as err:
                self.logger.critical(f"sending inventory info to grafana err: {err}")
                traceback.print_exc()
                
    
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(30)
            if not self.send:
                continue
            if self.rate_limit_handler.execution_is_ban():
                continue
            try:
                base_amt = 0

                for dymm_ex in self.cfg.EXCHANGES:
                    for acc in self.account_names:
                        api = self.get_exchange_api_by_account(acc)
                        piid = f"{dymm_ex}.{self.cfg.CONTRACT}"
                        iid = self._contract_mapper.proxy_iid_to_iid(
                            proxy_iid=piid)

                        symbol = f"{iid}.{self.market}.{self.sub_market}"

                        await asyncio.sleep(0.2)
                        cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data

                        if self.ex == "binance":
                            ccy = "usdt"
                        elif self.ex == "binance_busd":
                            ccy = "busd"
                        base_amt += float(cur_balance[ccy]["all"])
                        self.load_balancer.update_balance(
                            acc=acc, balance=float(cur_balance[ccy]["all"]))
                        self.executor.update_margin_balance(ex=dymm_ex, margin_balance=float(cur_balance[ccy]["all"]))
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt",
                                tag={"sn": self.strategy_name},
                                fields={
                                    f"{acc}_acc": float(self.load_balancer._account_load_map[acc])
                                }
                            )
                        )

                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                "balance": float(base_amt),
                            }
                        )
                    )

                for trader in list(self.load_balancer._ip_load_map.keys()):
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                f"{trader}_trader": float(self.load_balancer._ip_load_map[trader])
                            }
                        )
                    )

                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt",
                        tag={"sn": self.strategy_name},
                        fields={
                            "trade_amount_per_second": float(float(self.trd_amt)/(time.time() - self.start_ts)),
                        }
                    )
                )


            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
                
    async def close_all_position(self, symbol):
        try:
            for _ in range(2):
                for acc in self.account_names:
                    api = self.get_exchange_api_by_account(acc)
                    cur_pos = (await api.contract_position(self.get_symbol_config(symbol))).data
                    pos = cur_pos["long_qty"] - cur_pos["short_qty"]
                    if pos == Decimal(0):
                        continue
                    await self.make_order(
                        symbol_name=symbol,
                        price=Decimal(0),
                        volume=abs(pos),
                        side = OrderSide.Buy if pos < 0 else OrderSide.Sell,
                        position_side=OrderPositionSide.Close,
                        order_type= OrderType.Market,
                        account_name=acc,
                    )
                    await asyncio.sleep(0.5)
        except:
            traceback.print_exc()

    async def update_redis_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    self.rate_limit_handler.ban_execution(sec = 100)
                    await asyncio.sleep(5)
                    
                    for ex in self.cfg.EXCHANGES:
                        for acc in self.account_names:
                            api = self.get_exchange_api_by_account(acc)
                            ex, _ = acc.split(".")
                            for piid in self._iids:
                                iid: str = self._contract_mapper.proxy_iid_to_iid(
                                    proxy_iid=piid)
                                if ex != "binance" or not iid.startswith("binance"):
                                    raise NotImplementedError(
                                        f"{ex},{iid} piid to symbol mapping is absent")
                                symbol = self.iid_to_binance_symbol(iid=iid)
                                try:
                                    await api.flash_cancel_orders(self.get_symbol_config(symbol))
                                except:
                                    pass

                                await asyncio.sleep(0.2)
                                await self.close_all_position(symbol)
                                
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                total_close = self.stop_close_amt + self.limit_close_amt + self.press_close_amt
                await self.redis_set_cache(
                    {
                        "exit": None,
                        "hit before closing mean": f"{np.mean(self.time_period.hit_count)}"  if self.time_period.hit_count else 0,
                        "hit before closing max": f"{np.max(self.time_period.hit_count)}"  if self.time_period.hit_count else 0,
                        "stop close percentage": self.stop_close_amt / total_close if total_close > 0 else 0,
                        "limit close percentage": self.limit_close_amt / total_close if total_close > 0 else 0,
                        "press close percentage": self.press_close_amt / total_close if total_close > 0 else 0,
                        "stop pnl per u":self.stop_pnl / self.stop_close_amt if self.stop_close_amt else 0,
                        "limit pnl per u":self.limit_pnl/ self.limit_close_amt if self.limit_close_amt else 0,
                        "press pnl per u":self.press_pnl/ self.press_close_amt if self.press_close_amt else 0,
                        "hedge delay max":f"{np.max(self.save_log.get_inner_delay_stats())}",
                        "hedge delay min":f"{np.min(self.save_log.get_inner_delay_stats())}",
                        "hedge delay median":f"{np.median(self.save_log.get_inner_delay_stats())}",
                        "hedge delay mean":f"{np.mean(self.save_log.get_inner_delay_stats())}",
                        "hedge delay 90":f"{np.quantile(self.save_log.get_inner_delay_stats(), 0.9)}",
                        "limbo order len":len(self.limbo_orders)
                    }
                )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def log_save_to_local(self):
        self.last_ts = 0
        while True:
            await asyncio.sleep(0.1)
            curr_ts = time.time() % (60*15)
            if curr_ts < self.last_ts:
                self.save_log.gather_data(curr_time=int(time.time() // (60*15) * (60*15) * 1e3))
            self.last_ts = curr_ts

    # strategy core
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.reset_pending_cancel_and_hedge_order(),
                self.reset_triggered_stop_order(),
                self.get_local_stats(),
                self.get_holding_period(),
                self.get_inventory(),
                self.update_redis_cache(),
                self.load_balancer_reduce_count(),
                self.log_save_to_local()
            )


if __name__ == '__main__':
    # logging.getLogger().setLevel("WARNING")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
