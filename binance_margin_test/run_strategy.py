import asyncio
from decimal import Decimal
from atom.helpers import safe_decimal

from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_usdt.margin.fix"

    async def main_logic(self):
        # res = await self.raw_make_order(
        #         symbol_name=self.symbol,
        #         price=Decimal(23000),
        #         volume=Decimal("0.0005"),
        #         side=OrderSide.Buy,
        #         position_side=OrderPositionSide.Open
        #     )
        # self.logger.info(res.headers)
        api = self.get_exchange_api_by_account(self.account_names[0])
        result = dict()
        response = await api.make_request("margin", "GET", "/sapi/v1/margin/isolated/account", query=dict(), need_sign=True)
        for item in response.json["assets"]:
            pair = f"""{item["baseAsset"]["asset"]}_{item["quoteAsset"]["asset"]}""".lower()
            info = dict()
            for k in ("baseAsset", "quoteAsset"):
                info[item[k]["asset"].lower()] = dict(
                    all=safe_decimal(item[k]["totalAsset"]),
                    available=safe_decimal(item[k]["free"]),
                    borrowed=safe_decimal(item[k]["borrowed"]),
                    net=safe_decimal(item[k]["netAsset"]),
                    hold=0,
                    frozen=safe_decimal(item[k]["locked"])
                )
            print(info)
        # cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
        # print(cur_balance)
        exit()
        
    async def strategy_core(self):
        await self.main_logic()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()