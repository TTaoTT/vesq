import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class OrderCache():
    def __init__(self) -> None:
        self.lcache = dict()
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.label_name = ["ap1","ap2","bp1","bp2"]
        for label in self.label_name:
            self.lcache[label] = None
        
    def remove_order(self,client_id):
        if self.order_cache.get(client_id):
            o = self.order_cache.pop(client_id)
            label = o.message["label"]
            if label not in self.label_name:
                return
            self.lcache[label] = None
    
    def new_order(self,order):
        self.order_cache[order.client_id] = order
        label = order.message["label"]
        if label not in self.label_name:
            return
        self.lcache[label] = order
        
    def handle_pos_inner(self,client_id,order):
        if client_id not in list(self.order_cache): 
            return
        
        if order.filled_amount < self.order_cache[client_id].filled_amount:
            return

        self.order_cache[client_id].avg_filled_price = order.avg_filled_price
        self.order_cache[client_id].xchg_status = order.xchg_status
        amount_changed = order.filled_amount - self.order_cache[client_id].filled_amount
        if not amount_changed:
            return

        if self.order_cache[client_id].side == OrderSide.Buy:
            self.pos += amount_changed
        else:
            self.pos -= amount_changed

        self.order_cache[client_id].filled_amount = order.filled_amount
            
    def handle_pos(self,client_id,order):
        if client_id not in list(self.order_cache):
            return
        
        if Decimal(order["z"]) < self.order_cache[client_id].filled_amount:
            return
        
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        self.order_cache[client_id].avg_filled_price = Decimal(order["ap"])
        self.order_cache[client_id].xchg_status = status
        
        amount_changed = Decimal(order["z"]) - self.order_cache[client_id].filled_amount
        if not amount_changed:
            return

        if self.order_cache[client_id].side == OrderSide.Buy:
            self.pos += amount_changed
        else:
            self.pos -= amount_changed

        self.order_cache[client_id].filled_amount = Decimal(order["z"])
        
            
    def get_order_status(self, client_id):
        return self.order_cache[client_id].xchg_status

    def has_pos(self):
        if abs(self.pos) < Decimal(1e-10):
            return False
        return True
    
class PriceCache():
    def __init__(self,price_multi,cache_size=100) -> None:
        self.high = None
        self.low = None
        self.cache = []
        self.cache_size = cache_size
        self.price_count = SortedDict()
        self.price_multi = price_multi
        
    def new_price(self,data):
        if not self.cache or data["t"]>= self.cache[-1]["t"]:
            self.cache.append(data)
        elif data["t"]<=self.cache[0]["t"]:
            self.cache = [data] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>data["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [data] + self.cache[i+1:]
            
        if self.price_count.__contains__(data["p"]):
            self.price_count[data["p"]] +=1
        else:
            self.price_count[data["p"]] =1
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_data = self.cache.pop(0)
            if self.price_count[old_data["p"]] == 1:
                self.price_count.pop(old_data["p"])
            else:
                self.price_count[old_data["p"]] -= 1
    
    def get_high_price(self):
        return self.price_count.keys()[-1]/self.price_multi if self.price_count else None
    
    def get_low_price(self):
        return self.price_count.keys()[0]/self.price_multi if self.price_count else None

    def cancel_check(self):
        if self.get_high_price() and self.high:
            if self.get_high_price()>= self.high:
                return True
        
        if self.get_low_price() and self.low:
            if self.get_low_price() < self.low:
                return True

        return False
    
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"

        self.recvWindow = self.params["recvWindow"]
        self.unit_qty = self.params["unit_qty"]
        self.spread = self.params["spread"]
        self.danger_spread = self.params["danger_spread"]

        self.price_multi = 1e7

        self.order_to_cancel = Queue()

        self.order_cache = OrderCache()
        self.price_cache = PriceCache(price_multi=self.price_multi)

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.hold_back = False
        self.retreating = False

        self.delay = 0

        self.working_counter = 0
        self.total_counter = 0
        
    def remove_order(self, client_id):
        return self.order_cache.remove_order(client_id)

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        o = self.order_cache.order_cache[client_id]
        o.xchg_status = OrderStatus.Failed
        await self.on_order_inner(o)

    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.9*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.9*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.9*1200

        if cond1 or cond2 or cond3:
            self.hold_back = True
            self.logger.warning("holding back")
            await asyncio.sleep(0.5)
            self.hold_back = False
    
    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        self.half_step_tick = qty_tick/Decimal(2)
        
        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.pos = position.data["long_qty"] - position.data["short_qty"]
        except Exception as err:
            self.logger.warning(f"init position error: {err}")
            
    async def on_order(self, o):
        # self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        
        order = o["o"]
        client_id = order["c"]
        
        if client_id not in list(self.order_cache.order_cache):
            return
        self.order_cache.handle_pos(client_id,order)
        status = self.order_cache.get_order_status(client_id)
        
        if status in OrderStatus.fin_status():
            self.remove_order(client_id)
            if Decimal(order["z"]) > Decimal(0):
                self.logger.warning(f"filled order:{order}")

    async def on_order_inner(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache.order_cache):
            return
        self.order_cache.handle_pos_inner(client_id,order)
        if order.xchg_status in OrderStatus.fin_status():
            self.remove_order(client_id)
            if order.filled_amount > Decimal(0):
                self.logger.warning(f"filled order:{order}")

    async def on_bbo(self, symbol, bbo):
        self.delay = time.time()*1e3 - bbo["data"]["E"]
        
        self.ap = float(bbo["data"]["a"])
        self.bp = float(bbo["data"]["b"])
        self.mp = (self.ap + self.bp) / 2

        self.price_cache.new_price({"p":int(self.ap*self.price_multi),"t":bbo["data"]["E"]})
        self.price_cache.new_price({"p":int(self.bp*self.price_multi),"t":bbo["data"]["E"]})

        self.price_cache.remove_expired(bbo["data"]["E"])

        if self.order_cache.order_cache and self.order_to_cancel.empty():
            self.order_to_cancel.put_nowait(1)
    
    async def on_public_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        tmp = {
            "t":trade["data"]["E"],
            "p":int(float(trade["data"]["p"])*self.price_multi)
            }
        self.price_cache.new_price(tmp)
        self.price_cache.remove_expired(trade["data"]["E"])

    
    async def main_logic(self):
        while True:
            await asyncio.sleep(0.001)
            if self.order_cache.order_cache and self.price_cache.cancel_check() and not self.retreating:
                res = await asyncio.gather(
                    *[self.batch_cancel(oid, order) for oid, order in self.order_cache.order_cache.items()]
                )
                if False not in res:
                    self.price_cache.high = None
                    self.price_cache.low = None
            elif not self.order_cache.order_cache and self.delay<30 and not self.retreating and not self.hold_back and not self.rate_limit:
                order = []
                self.price_cache.high = self.mp*(1+self.danger_spread)
                self.price_cache.low = self.mp*(1-self.danger_spread)
                ap1 = Decimal(self.mp*(1+self.spread/2-0.00005))
                ap2 = Decimal(self.mp*(1+self.spread-0.00005))
                bp1 = Decimal(self.mp*(1-self.spread/2+0.00005))
                bp2 = Decimal(self.mp*(1-self.spread+0.00005))
                if not self.order_cache.lcache["ap1"]:
                    order.append([ap1,self.unit_qty,OrderSide.Sell,"ap1"])
                if not self.order_cache.lcache["ap2"]:
                    order.append([ap2,self.unit_qty,OrderSide.Sell,"ap2"])
                if not self.order_cache.lcache["bp1"]:
                    order.append([bp1,self.unit_qty,OrderSide.Buy,"bp1"])
                if not self.order_cache.lcache["bp2"]:
                    order.append([bp2,self.unit_qty,OrderSide.Buy,"bp2"])
                await asyncio.gather(
                        *[self.send_order(*param) for param in order]
                    )

    async def clear_position(self):
        while True:
            await asyncio.sleep(0.1)
            if self.order_cache.has_pos() and not self.retreating:
                self.logger.warning("well we have a position")
                self.retreating = True
                await self.retreat()
                self.retreating = False
            elif self.rate_limit and not self.retreating:
                self.logger.warning("well we have a rate limit")
                self.retreating = True
                await self.retreat()
                self.retreating = False

    async def counter(self):
        while True:
            await asyncio.sleep(0.1)
            if len(self.order_cache.order_cache) == 4:
                self.working_counter += 1
            self.total_counter += 1


    async def send_order(self,price,qty,side,label,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=None):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow

        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = 20
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        extra_info["label"] =  label
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache.new_order(order)
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=order.create_ms,
                recvWindow=recvWindow,
                client_id=client_id
            )
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
            self.retreating = True
            await self.retreat()
            self.logger.warning("exit because insufficient balance")
            exit()
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except  ExceededMaxPositionError as err:
            await self.internal_fail(client_id, err)
            self.retreating = True
            await self.retreat()
            self.logger.warning("exit because exceeding max position")
            exit()
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def batch_cancel(self, oid, order):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                res = await self.cancel_order(order)
                self.canceling_id.remove(oid)
                return res
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
            return False

    async def cancel_order_action(self):

        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
                
        while True:
            await self.order_to_cancel.get()
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )

    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] +order.message["query"]*6 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order_inner(order_new)
                    elif self.order_cache.order_cache[oid].message["query"]>10:
                        self.retreating = True
                        await self.retreat()
                        await self.redis_set_cache({})
                        self.logger.warning(f"check order too many times and exiting")
                        exit()
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )

    async def check_position(self):
        while True:
            await asyncio.sleep(60)
            try:
                if not self.rate_limit:
                    api = self.get_exchange_api("binance")[0]
                    position = await api.contract_position(self.get_symbol_config(self.symbol))
                    pos = position.data["long_qty"] - position.data["short_qty"]
                    if self.order_cache.pos != pos:
                        self.logger.warning(
                            f"""
                            =========update wrong position==========
                            origin: {self.order_cache.pos}
                            update: {pos}
                            """
                        )
                        self.order_cache.pos = pos
                    
            except Exception as err:
                self.logger.warning(f'check position err {err}')

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(Decimal(self.bp*0.99), abs(pos), OrderSide.Sell, "close", OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(Decimal(self.ap*1.01), abs(pos), OrderSide.Buy, "close", OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)
        

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.retreating = True
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None,"fail rate":self.working_counter/self.total_counter if self.total_counter else 0})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(1)
        await asyncio.gather(
            self.update_redis_cache(),
            self.check_position(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.main_logic(),
            self.clear_position(),
            self.check_position(),
            self.counter()
            # self.debug_funtion(),
            # self.time_control_function()
            )

    # async def debug_funtion(self):
    #     while True:
    #         await asyncio.sleep(1)
    #         if self.order_cache:
    #             self.logger.warning(f"all order:{self.order_cache}")
    #             self.logger.warning(f"ask order:{self.ask_order_cache}")
    #             self.logger.warning(f"bid order:{self.bid_order_cache}")
    
    # async def time_control_function(self):
    #     await asyncio.sleep(60*30)
    #     await self.retreat()
    #     self.logger.warning("testing over")
    #     exit()
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    