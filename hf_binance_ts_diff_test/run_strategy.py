import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy
from asyncio.queues import Queue




class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)

        self.symbol_1 = f"binance.1000shib_usdt_swap.swap"
        self.depth_ts_cache = collections.deque(maxlen=10000)
        self.trade_ts_cache = collections.deque(maxlen=10000)


    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        await self._get_symbol_config_from_remote([self.symbol_1])
        self.subscribe_orderbook([self.symbol_1])
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()
        self.subscribe_public_trade([self.symbol_1])

        
    async def on_orderbook(self, symbol, orderbook):
        """
        price, volume
        orderbook: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        lag = time.time()*1e3-int(orderbook["server_ts"])
        self.depth_ts_cache.append(lag)
        
    async def on_public_trade(self, symbol, trade: TradeData):
        """
        class TradeData(object):
            # Internal symbol
            symbol = attr.ib()
            # Source exchange name
            source = attr.ib()
            price = attr.ib(converter=num_to_decimal)
            quantity = attr.ib(converter=num_to_decimal)
            side = attr.ib(validator=[attr.validators.instance_of(TradeSide)])
            # Timestamp related
            req_ts = attr.ib(converter=int)
            server_ts = attr.ib(converter=int)

            trade_id = attr.ib(default='', converter=str)
            buy_order_id = attr.ib(default='', converter=str)
            sell_order_id = attr.ib(default='', converter=str)
            resp_ts = attr.ib(factory=time_ms_now, converter=int)
            transaction_ts = attr.ib(factory=time_ms_now, converter=int)
        """
        lag = time.time()*1e3 - int(trade.server_ts)
        self.trade_ts_cache.append(lag)
        
        
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            await self.redis_set_cache({"depth median": np.median(self.depth_ts_cache), "trade mdeian":np.median(self.trade_ts_cache)})

    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache()
            # self.check_timeout()
        )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()