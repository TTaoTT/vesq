import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy

"""
class Order(object):
    internal_symbol = attr.ib()
    xchg_name = attr.ib()
    account_name = attr.ib()
    xchg_id = attr.ib(converter=str)
    xchg_symbol = attr.ib()

    side = attr.ib(validator=[attr.validators.instance_of(OrderSide)])
    type = attr.ib(validator=[attr.validators.instance_of(OrderType)])
    xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])

    time_in_force = attr.ib(validator=[attr.validators.instance_of(OrderTimeInForce)])
    requested_price = attr.ib(converter=num_to_decimal)
    requested_amount = attr.ib(converter=num_to_decimal)
    filled_amount = attr.ib(converter=num_to_decimal)
    avg_filled_price = attr.ib(converter=num_to_decimal)
    commission_fee = attr.ib(converter=num_to_decimal)

    is_finished = attr.ib()

    # Timestamp related
    local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
    server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message

    # Event type of this order object, will be used to trace user and xchg event
    evt_type = attr.ib(validator=[attr.validators.instance_of(OrderEventType)], default=OrderEventType.XchgUpdate)

    created_ts_ms = attr.ib(default=0, converter=int)  # Order created ts
    finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts

    # Update historical records
    update_historical_records = attr.ib(factory=list)

    # Trade records
    trade_records = attr.ib(factory=list)

    # Position side
    position_side = attr.ib(validator=[attr.validators.instance_of(OrderPositionSide)], default=OrderPositionSide.Open)
    strategy_name = attr.ib(default="UNSET")
    tag = attr.ib(default="PY-ATOM")
    extra_info = attr.ib(default="")

    raw_data = attr.ib(default=None)
"""

class MpOutput:
    def __init__(self, base_line):
        self.base_std = base_line
        self.mp_queue = collections.deque(maxlen=200)
        self.qt_queue = collections.deque(maxlen=10000)

        self.pre_max_list = collections.deque(maxlen=200)
        self.pre_min_list = collections.deque(maxlen=200)

    def feed_mp(self, mp):
        self.mp_queue.append(float(mp))
        self.update_base_line()

    def get_x_cur_duration(self):
        if len(self.pre_min_list) == 200:
            mp_mean = np.mean(list(self.mp_queue)[-50:])
            if mp_mean > self.pre_max_list[0]:
                return 1
            if mp_mean < self.pre_min_list[0]:
                return -1
        return 0

    def feed_max_logic(self):
        p_max = np.max(list(self.mp_queue))
        p_min = np.min(list(self.mp_queue))
        self.pre_max_list.append(p_max)
        self.pre_min_list.append(p_min)

    def update_base_line(self):
        if len(self.mp_queue) == 200:
            cur_std = self.get_cur_std()
            self.qt_queue.append(cur_std)
            if len(self.qt_queue) == 10000:
                cur_82_std = np.quantile(list(self.qt_queue), 0.82)
                self.base_std = cur_82_std
                self.qt_queue.clear()
            self.feed_max_logic()

    def get_cur_std(self):
        return np.std(list(self.mp_queue)[-100:])

    def mp_bad(self):
        return self.get_cur_std() > self.base_std

    def get_mean_mp(self):
        return np.mean(list(self.mp_queue)[-50:])

    def get_cur_duration(self):
        return self.mp_queue[-1] - self.get_mean_mp()

class AdjustMovement:
    def __init__(self, mt_line):
        self.buy_ban_mem_ts = None
        self.sell_ban_mem_ts = None
        self.mt_line = mt_line

        self.mp_cache = collections.deque(maxlen=5)
        self.mp_dif = collections.deque(maxlen=5)

        self.sp_update_mem = collections.deque(maxlen=10000)

    def feed_mp(self, cur_mp, cur_dt_ts):
        if len(self.mp_cache) == 5:
            mp_mean = np.mean(self.mp_cache)
            self.mp_dif.append(mp_mean)
        self.mp_cache.append(cur_mp)
        self.count_ban_signal(cur_dt_ts)

    def update_dif(self):
        if len(self.sp_update_mem) == 10000:
            new_line = np.quantile(list(self.sp_update_mem), 0.98)
            # print(f"update mmt new line: {self.mt_line} -> {new_line}")
            self.mt_line = new_line
            self.sp_update_mem.clear()

    def count_ban_signal(self, dt_ts):
        if len(self.mp_dif) == 5:
            cur_dif = self.mp_dif[-1] / self.mp_dif[0] - 1
            self.sp_update_mem.append(float(abs(cur_dif)))
            if cur_dif > self.mt_line:  # 动量阈值的影响非常大
                self.buy_ban_mem_ts = dt_ts

            if cur_dif < -1 * self.mt_line:
                self.sell_ban_mem_ts = dt_ts

            if self.buy_ban_mem_ts is not None:
                if dt_ts - self.buy_ban_mem_ts > 15e3:  # 时间的影响不大
                    self.buy_ban_mem_ts = None

            if self.sell_ban_mem_ts is not None:
                if dt_ts - self.sell_ban_mem_ts > 15e3:
                    self.sell_ban_mem_ts = None

            self.update_dif()


class MyStrategy(CommonStrategy):
    """
    1000shib
    {
        "mt_line" : 0.0008701675430150109,
        "std_line": 5.645012072205979e-06,
        "base_amount":750,
        "model_params": {
            "up_c_coef": 7.597565484127043e-08,
            "up_c_int": 0.0011419423437169223,
            "up_f_coef": 6.832285985465625e-07,
            "up_f_int": -0.0004008578623042261,
            "down_c_coef": 7.409106415997389e-08,
            "down_c_int": 0.001142924323096629,
            "down_f_coef": 5.849067660863639e-07,
            "down_f_int": -0.0003493628558276356
    }
    }

    doge
    {
        "mt_line" : 0.0008070218261430952,
        "std_line": 0.001756994568023799,
        "base_amount":20,
        "model_params": {
            "up_c_coef": 1.1248517599236778e-06,
            "up_c_int": 5.306240356248127e-05,
            "up_f_coef": 4.995125935201646e-07,
            "up_f_int": 1.7425333182578644e-05,
            "down_c_coef": 1.0043788661679417e-06,
            "down_c_int": 0.00012143100047970435,
            "down_f_coef": 4.5502511061070124e-07,
            "down_f_int": 4.5965380177283126e-05
    }
        
    }
    
    eth
    {
    "mt_line": 0.0009841303912762056,
    "std_line": 1.324417139087233,
    "base_amount": 0.005,
    "model_params": {
        "up_c_coef": 0.0000222710353,
        "up_c_int": 0.0003944,
        "up_f_coef": 0.0000122213423,
        "up_f_int": 0.00015009,
        "down_c_coef": 0.0000211126932,
        "down_c_int": 0.00040205,
        "down_f_coef": 0.0000143259034,
        "down_f_int": 0.00011922
    }
}
    """
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # init params
        # use self.params
        self.trade_cache = list()
        self.missing_order_cache = list()
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.long_pos = Decimal(0)
        self.short_pos = Decimal(0)
        self.mp = None
        self.trade_buy = 0
        self.trade_sell = 0
        self.heartbeat_order_enabled = False
        self.unknown_order_dict = {}
        self.order_storage_enabled = False
        self.max_balance = 0
        self.pos_cache = [] # store unclosed orders.

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(v)
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        self.movement_adjust = AdjustMovement(mt_line=self.params["mt_line"])
        self.mp_std_handle = MpOutput(base_line=float(self.params["std_line"]))
        self.symbol_1 = self.config['strategy']['symbol_1']

        self._params =self.params["model_params"]

    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        self.subscribe_orderbook(self._symbol_config_.keys())
        self.subscribe_public_trade(self._symbol_config_.keys())
        self.direct_subscribe_order_update('binance',symbols=[self.config['strategy']['symbol_1']])
        self.logger.setLevel("WARNING")

        # await self.redis_set_cache("dict")
        # await self.redis_get_cache()

    async def on_orderbook(self, symbol, orderbook):
        """
        price, volume
        orderbook: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        self.mp = (orderbook["asks"][0][0] + orderbook["bids"][0][0])/Decimal(2)
        self.movement_adjust.feed_mp(cur_mp=float(self.mp), cur_dt_ts=time.time()*1e3)
        self.mp_std_handle.feed_mp(mp=self.mp)

    async def on_public_trade(self, symbol, trade: TradeData):
        """
        class TradeData(object):
            # Internal symbol
            symbol = attr.ib()
            # Source exchange name
            source = attr.ib()
            price = attr.ib(converter=num_to_decimal)
            quantity = attr.ib(converter=num_to_decimal)
            side = attr.ib(validator=[attr.validators.instance_of(TradeSide)])
            # Timestamp related
            req_ts = attr.ib(converter=int)
            server_ts = attr.ib(converter=int)

            trade_id = attr.ib(default='', converter=str)
            buy_order_id = attr.ib(default='', converter=str)
            sell_order_id = attr.ib(default='', converter=str)
            resp_ts = attr.ib(factory=time_ms_now, converter=int)
            transaction_ts = attr.ib(factory=time_ms_now, converter=int)
        """
        self.trade_cache.append({"ts":trade.server_ts, "q":trade.quantity, "s":trade.side})
        
        
    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[xchg_id].filled_amount
        if not amount_changed:
            return

        if (self.order_cache[xchg_id].side == OrderSide.Buy and self.order_cache[xchg_id].position_side == OrderPositionSide.Open) or \
                (self.order_cache[xchg_id].side == OrderSide.Sell and self.order_cache[xchg_id].position_side == OrderPositionSide.Close):
            self.long_pos += Decimal((self.order_cache[xchg_id].side == OrderSide.Buy)*2 -1) * amount_changed
        else:
            self.short_pos += Decimal((self.order_cache[xchg_id].side == OrderSide.Sell)*2 -1) * amount_changed

        if (self.pos >= 0 and self.order_cache[xchg_id].side == OrderSide.Buy) or (self.pos < 0 and self.order_cache[xchg_id].side == OrderSide.Sell):
            self.pos_cache.append([amount_changed,p_order.server_evt_ts_ms, p_order.avg_filled_price])
        elif (self.pos>=0 and self.order_cache[xchg_id].side == OrderSide.Sell) or (self.pos < 0 and self.order_cache[xchg_id].side == OrderSide.Buy):
            if amount_changed < abs(self.pos):
                while self.pos_cache:
                    if amount_changed < self.pos_cache[0][0]:
                        self.pos_cache[0][0] -= amount_changed
                        break
                    else:
                        amount_changed -= self.pos_cache[0][0]
                        self.pos_cache.pop(0)
            elif amount_changed > abs(self.pos):
                self.pos_cache = [[amount_changed - abs(self.pos),p_order.server_evt_ts_ms,p_order.avg_filled_price]]
            elif amount_changed == abs(self.pos):
                self.pos_cache = []

        self.pos = self.long_pos - self.short_pos
        self.logger.info(f"current pos: {self.pos}, long pos: {self.long_pos}, short pos: {self.short_pos}")

    async def on_order(self, xchg_id, order: PartialOrder):
        """
        class PartialOrder(object):
            # used to update redis order
            xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])
            filled_amount = attr.ib(converter=num_to_decimal)
            avg_filled_price = attr.ib(converter=num_to_decimal)
            commission_fee = attr.ib(converter=num_to_decimal)
            is_finished = attr.ib()
            # Timestamp related
            local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
            server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message
            finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts
        """
        if xchg_id not in list(self.order_cache):
            # add unknow order dealing funciton, check_unknown_order.
            # before, will only check order after order expired. 
            self.unknown_order_dict[xchg_id] = order
            self.logger.warning(f"find unknow order {xchg_id}")
            return
        
        if order.filled_amount < self.order_cache[xchg_id].filled_amount:
            return

        self.handle_pos(xchg_id,order)
        self.order_cache[xchg_id].filled_amount = order.filled_amount
        self.order_cache[xchg_id].is_finished = order.is_finished
        self.order_cache[xchg_id].xchg_status = order.xchg_status

        if order.is_finished:
            if order.filled_amount > 0:
                await self._order_store_queue_.put(self.order_cache[xchg_id])
            self.order_cache.pop(xchg_id)
            if xchg_id in self.unknown_order_dict:
                self.unknown_order_dict.pop(xchg_id)

    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.01)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.order_cache:
                    self.logger.warning(f"unkonw {xchg_id} order rest back")
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(xchg_id, partial_order) for xchg_id, partial_order in now_know_order.items()]
            )

    async def strategy_core(self):
        await asyncio.sleep(30)
        await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.update_redis_cache(),
            self.check_trade_flow(),
            self.check_unknown_order(),
            self.check_drawback(),
            self.check_timeout()
        )

    def get_place_amount(self,net_loc,base_amount,level=5):
        cur_risk = abs(self.pos)
        v_cum = 0
        i_net = 0
        while True:
            i_net += 1
            cur_net_volume = sum([i_net*base_amount + i*base_amount for i in range(4)])
            v_cum += cur_net_volume
            if v_cum > cur_risk:
                break
        if i_net >= level:
            i_net = level
        return [i_net*base_amount + i*base_amount for i in range(4)][net_loc-1]
    
    def get_order_position(self,balance=False):
        
        ask_ceiling = Decimal(round(self._params["up_c_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up_c_int"],4))
        ask_floor = Decimal(round(self._params["up_f_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up_f_int"],4))
        
        bid_ceiling = Decimal(round(self._params["down_c_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down_c_int"],4))
        bid_floor = Decimal(round(self._params["down_f_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down_f_int"],4))
        
        if balance:
            ask_floor = Decimal(round(self._params["up_f_coef_b"]*np.sqrt(float(self.trade_buy)) + self._params["up_f_int_b"],4))
            bid_floor = Decimal(round(self._params["down_f_coef_b"]*np.sqrt(float(self.trade_sell)) + self._params["down_f_int_b"],4))

        sell_spread_tick = round((ask_ceiling - ask_floor) / 3, 5)
        buy_spread_tick = round((bid_ceiling - bid_floor) / 3, 5)

        if sell_spread_tick < 0:
            sell_spread_tick = Decimal(0)
        if buy_spread_tick < 0:
            buy_spread_tick = Decimal(0)

        return ask_floor, bid_floor, sell_spread_tick, buy_spread_tick

    def generate_entry_orders(self):
        if not self.trade_buy or not self.trade_sell or not self.mp or abs(self.pos)>Decimal(100)*self.params["base_amount"]:
            return []
        entry_orders = []
        mp = self.mp
        
        ask_floor, bid_floor, sell_spread_tick, buy_spread_tick = self.get_order_position()

        for i in range(1, 5):
            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor

            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))

                elif cd < 0:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            s_amount = self.get_place_amount(net_loc=i, base_amount=self.params["base_amount"],level=2)
            b_amount = self.get_place_amount(net_loc=i, base_amount=self.params["base_amount"],level=2)

            b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,1]
            s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,1]

            if self.pos > 0 and (self.long_pos-self.pos) > self.params["base_amount"]*Decimal(i/2+i**2/2):
                s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,1]
            elif self.pos < 0 and self.long_pos > self.params["base_amount"]*Decimal(i/2+i**2/2):
                s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,1]

            if self.pos < 0 and (self.short_pos+self.pos) > self.params["base_amount"]*Decimal(i/2+i**2/2):
                b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,1]
            elif self.pos > 0 and self.short_pos > self.params["base_amount"] * Decimal(i/2+i**2/2):
                b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i, 1]

            entry_orders.append(b_io)
            entry_orders.append(s_io)

        return entry_orders

    def generate_balance_orders(self):
        cur_risk = self.pos
        if not self.trade_buy  or not self.trade_sell or not self.mp:
            return []

        balance_orders = []
        mp = self.mp
        total_amount = Decimal(abs(cur_risk))
        
        ask_floor, bid_floor, sell_spread_tick, buy_spread_tick = self.get_order_position()

        for i in range(4, 0, -1):

            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor
        
            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))
                else:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            ex_amount = self.get_place_amount(net_loc=5-i,base_amount=self.params["base_amount"])
            if cur_risk > 0:

                if self.movement_adjust.sell_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                #
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, sell_p, ex_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, sell_p, total_amount, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
            else:
                if self.movement_adjust.buy_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, buy_p, ex_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, buy_p, total_amount, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
        return balance_orders

    def order_generate_logic(self):
        if self.trade_buy and self.trade_sell:
            req_orders = []

            entry_orders = self.generate_entry_orders()
            req_orders.extend(entry_orders)

            balance_orders = self.generate_balance_orders()
            req_orders.extend(balance_orders)

            filter_orders = self.request_orders_filter(req_orders)
            return filter_orders
        return []

    def request_orders_filter(self,req_orders):
        filter_orders = []
        buy_source = [self.order_cache[key].raw_data["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Buy]
        sell_source = [self.order_cache[key].raw_data["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Sell]

        for per_req in req_orders:
            if per_req[3] == OrderSide.Buy:
                x_source = np.array(buy_source)
            else:
                x_source = np.array(sell_source)
            x_source = np.array(x_source)
            cur_pending_len = sum((x_source == per_req[7]) * 1)
            if cur_pending_len < 1:
                filter_orders.append(per_req)
        return filter_orders

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def send_order_action(self):
        # only create order cache here
        async def batch_send_order(params):
            if not self.volume_notional_check(*params[:3]):
                return
            try:
                order =  await self.make_future_order(*params[:6])
                extra_info = dict()
                extra_info["stop_ts"] = params[6]
                extra_info["source"] =  params[7] # entry or balance
                order.raw_data = extra_info
                self.order_cache[order.xchg_id] = order
                
            except Exception as err:
                self.logger.warning(f"send order err, {err}")

        while True:
            await asyncio.sleep(0.2)
            
            orders = self.order_generate_logic()
            try:
                await asyncio.gather(*[batch_send_order(order_params) for order_params in orders])
            except:
                pass

    async def cancel_order_action(self):
        
        async def batch_cancel(oid, order):

            if time.time()*1e3 - order.created_ts_ms > order.raw_data["stop_ts"] * 1e3:
                try:
                    await self.cancel_order(order)
                except Exception as err:
                    self.logger.warning(f'cancel order {oid} err {err}')

        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

    async def get_order_status_direct(self,order):
        _xchg_name,_pair,_market = order.internal_symbol.split(".")
        api = self._check_api_(_xchg_name)
        try:
            self.logger.warning(f"{_pair} get order {order.xchg_id} from exchange http api")
            _order = await api.order_match_result(_pair,order.xchg_id, extra_opts = {"market":_market})
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if time.time()*1e3 - order.created_ts_ms > order.raw_data["stop_ts"] * 1e3 + 1e3:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.logger.warning(f"check order{order_new.xchg_id}, order status:{order_new.xchg_status}, is_finished:{order_new.is_finished}")
                    if not order_new:
                        self.order_cache.pop(oid)
                    if order_new.is_finished:
                        self.handle_pos(oid,order_new)
                        self.order_cache.pop(oid)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}")

        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def update_redis_cache(self):

        async def batch_cancel(oid, order):
            try:
                await self.cancel_order(order)
            except Exception as err:
                self.logger.warning(f'cancel order {oid} err {err}')

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    await asyncio.gather(
                        *[batch_cancel(oid, order) for oid, order in self.order_cache.items()],
                        self.make_future_order(self.symbol_1, self.mp*Decimal(1.1), abs(self.short_pos), OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel),
                        self.make_future_order(self.symbol_1, self.mp*Decimal(0.9), abs(self.long_pos), OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel),
                        self.redis_set_cache({"exit":None, "current position/base amount":0, "max balance": 0,"current position": 0, "long position": 0, "short position": 0, "base amount":self.params["base_amount"]})
                    )
                    exit()
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        async def update_cache():
            _dict = dict()
            _dict = {
                "exit": None,
                "current position/base amount": self.pos/self.params["base_amount"],
                "max balance": self.max_balance,
                "current position": self.pos,
                "long position": self.long_pos,
                "short position": self.short_pos,
                "base amount": self.params["base_amount"]
            }
            try:
                await self.redis_set_cache(_dict)
            except Exception as err:
                self.logger.warning(f"set redis cache failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            await update_cache()
    
    async def check_trade_flow(self):

        async def batch_cancel(oid, order):
            try:
                await self.cancel_order(order)
            except Exception as err:
                self.logger.warning(f'cancel order {oid} err {err}')

        async def close_position(params):
            try:
                order =  await self.make_future_order(*params)
                extra_info = dict()
                extra_info["stop_ts"] = 1
                extra_info["source"] =  0 # entry or balance, 0 for close
                order.raw_data = extra_info
                self.order_cache[order.xchg_id] = order
            except Exception as err:
                self.logger.warning(f"close position err, {err}")

        while True:
            await asyncio.sleep(0.5)
            
            while self.trade_cache and self.trade_cache[0]['ts'] < time.time()*1e3 - 10e3:
                self.trade_cache.pop(0)

            if not self.trade_cache:
                self.logger.warning("trade data flow missing and lock")
                await asyncio.gather(
                        *[batch_cancel(oid, order) for oid, order in self.order_cache.items()],
                        close_position([self.symbol_1, self.mp*Decimal(1.1), abs(self.short_pos), OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel]),
                        close_position([self.symbol_1, self.mp*Decimal(0.9), abs(self.long_pos), OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel]),
                    )
                continue

            if self.trade_cache[0]['ts'] < time.time()*1e3 - 8e3:
                self.trade_buy = abs(sum([item["q"] for item in self.trade_cache if item["s"]==TradeSide.Buy]))
                self.trade_sell = abs(sum([item["q"] for item in self.trade_cache if item["s"]==TradeSide.Sell]))
            else:
                self.trade_buy = 0
                self.trade_sell = 0

    async def check_drawback(self):
        async def batch_cancel(oid, order):
            try:
                await self.cancel_order(order)
            except Exception as err:
                self.logger.warning(f'cancel order {oid} err {err}')
        
        while True:
            await asyncio.sleep(5)
            try:
                cur_balance = await self.get_balance(self.symbol_1)
                equity = cur_balance.get("usdt")
                self.max_balance = max(self.max_balance, equity["all"])
                if equity["all"] < self.max_balance*Decimal(0.99):
                    await asyncio.gather(
                        *[batch_cancel(oid, order) for oid, order in self.order_cache.items()],
                        self.make_future_order(self.symbol_1, self.mp*Decimal(1.1), abs(self.short_pos), OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel),
                        self.make_future_order(self.symbol_1, self.mp*Decimal(0.9), abs(self.long_pos), OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel),
                    )
                    exit()
            except Exception as err:
                self.logger.warning(f"check balance err {err}")

    async def check_timeout(self):
        if not self.mp:
            return

        def get_timeout_vol():
            vol = 0
            pos_cache = self.pos_cache.copy()
            for pos in pos_cache:
                if pos[1] < time.time()*1e3 - 540*1e3:
                    if self.pos >= 0:
                        if self.mp < pos[2]:
                            vol += pos[0]
                    else:
                        if self.mp > pos[2]:
                            vol += pos[0]
                else:
                    break
            return vol

        while True:
            await asyncio.sleep(1)
            vol = get_timeout_vol()
            if vol:
                try:
                    if self.pos >= 0:
                        order =  await self.make_future_order(self.symbol_1, self.mp*Decimal(0.9), vol, OrderSide.Sell, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel)
                    else:
                        order =  await self.make_future_order(self.symbol_1, self.mp*Decimal(1.1), vol, OrderSide.Buy, OrderPositionSide.Close, OrderTimeInForce.GoodTillCancel)
                    extra_info = dict()
                    extra_info["stop_ts"] = 1
                    extra_info["source"] =  0 # entry or balance, 0 for close
                    order.raw_data = extra_info
                    self.order_cache[order.xchg_id] = order
                except Exception as err:
                    self.logger.warning(f"close position err, {err}")

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
