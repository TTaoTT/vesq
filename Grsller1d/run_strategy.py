import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

# QtyManager maker sure update amount when position is even.

class SpreadCalculator():
    def __init__(self,lam, keppa, alpha, t) -> None:
        self.lam = lam
        self.keppa = keppa
        self.alpha = alpha
        self.t = t
    
    def get_h(self,q):
        w1 = np.exp(-self.alpha*self.keppa*(q**2))
        w2 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.alpha*self.keppa*((q-1)**2))
        w3 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.alpha*self.keppa*((q+1)**2))
        return 1/self.keppa * np.log(w1 + w2 + w3)
    
    def get_ask(self,inven):
        return 1/self.keppa - self.get_h(inven-1) + self.get_h(inven)
    
    def get_bid(self,inven):
        return 1/self.keppa - self.get_h(inven+1) + self.get_h(inven)
    
    def update_keppa(self, keppa):
        self.keppa = keppa

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
    
    def get_median_price(self):
        return self.get_ask_price(0.5)
    
class OrderCacheManager():
    def __init__(self) -> None:
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.exposed_pos = Decimal(0)
        
    def add_order(self,order):
        self.order_cache[order.client_id] = order
        self.exposed_pos = self.exposed_pos + order.requested_amount if order.side == OrderSide.Buy else self.exposed_pos - order.requested_amount

    def can_send_order(self):
        ask = 0
        bid = 0
        for cid in self.order_cache:
            if self.order_cache[cid].side == OrderSide.Buy:
                bid += 1
            elif self.order_cache[cid].side == OrderSide.Sell:
                ask += 1
        return ask, bid
    
    def new_agg_trade(self,trade):
        # {"t":timestamp,"p":price}
        for cid in self.order_cache:
            o = self.order_cache[cid]
            time_cond = trade["t"] > o.message["xchg_create_ms"] if o.message["xchg_create_ms"] else False
            order_type_cond = o.type != OrderType.IOC and o.type != OrderType.FOK
            cond1 = o.side == OrderSide.Buy and \
                not o.message["status"] and \
                time_cond and \
                o.requested_price > trade["p"] and \
                order_type_cond
            cond2 = o.side == OrderSide.Sell and \
                not o.message["status"] and \
                time_cond and \
                o.requested_price < trade["p"] and \
                order_type_cond
                
            if cond1 or cond2:
                self.order_cache[cid].message["status"] = 1
                self.order_cache[cid].message["filled"] = o.requested_amount 
            
    def after_cancel(self,cid,res):
        if self.order_cache.get(cid) == None:
            return
        o = self.order_cache[cid]
        if o.message["status"] == 0 and res == True and o.message["sent"]:
            self.order_cache[cid].message["status"] = 1
            self.order_cache[cid].message["filled"] = o.requested_amount
            self.pos = self.pos + o.requested_amount if o.side == OrderSide.Buy else self.pos - o.requested_amount
        elif type(res) == dict and res["status"] == "CANCELED" and o.message["status"] == 0:
            self.order_cache[cid].message["status"] = 1
            amount_changed = Decimal(res["executedQty"])
            self.order_cache[cid].message["filled"] = amount_changed
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            canceled = o.requested_amount - amount_changed
            self.exposed_pos = self.exposed_pos - canceled if o.side == OrderSide.Buy else self.exposed_pos + canceled
        else:
            self.order_cache[cid].message["cancel"] += 1
            
    def after_send_result(self,cid,res):
        # Don't need it right now since the response for make_order is "ACK"
        o = self.order_cache[cid]
        if res == False:
            self.order_cache[cid].message["status"] = 1
            self.exposed_pos = self.exposed_pos - o.requested_amount if o.side == OrderSide.Buy else self.exposed_pos + o.requested_amount
        elif res["status"] == "NEW":
            self.order_cache[cid].message["xchg_create_ms"] = res["updateTime"]
        elif res["status"] == "PARTIALLY_FILLED":
            if not o.message["xchg_create_ms"]:
                self.order_cache[cid].message["xchg_create_ms"] = res["updateTime"]
            amount_changed = Decimal(res["executedQty"]) - o.message["filled"]
            self.order_cache[cid].message["filled"] = Decimal(res["executedQty"])
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            # TODO:what will it be if ioc order is sent?
        elif res["status"] == "FILLED":
            self.order_cache[cid].message["status"] = 1
            amount_changed = Decimal(res["executedQty"]) - o.message["filled"]
            self.order_cache[cid].message["filled"] = Decimal(res["executedQty"])
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
            
    def after_send_ack(self,cid):
        if self.order_cache.get(cid):
            self.order_cache[cid].message["sent"] = True
            
    def order_finish(self,cid,filled):
        # TODO make sure this is wright
        o = self.order_cache[cid]
        if o.message["status"] == 0:
            self.pos = self.pos + filled if o.side == OrderSide.Buy else self.pos - filled
            canceled = o.requested_amount - filled
            self.exposed_pos = self.exposed_pos - canceled if o.side == OrderSide.Buy else self.exposed_pos + canceled
        else:
            if o.message["filled"] != filled:
                amount_changed = filled - o.message["filled"]
                self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
                self.exposed_pos = self.exposed_pos + amount_changed if o.side == OrderSide.Buy else self.exposed_pos - amount_changed
        self.order_cache.pop(cid)
    
    def check_exposure(self):
        expose = Decimal(0)
        for cid in self.order_cache:
            o = self.order_cache[cid]
            if o.message["status"] == 0:
                expose = expose + o.requested_amount if o.side == OrderSide.Buy else expose - o.requested_amount
            
        expose += self.pos
        if expose != self.exposed_pos:
            return expose, False
        else:
            return expose, True

        
    def has_open_order(self):
        for cid in self.order_cache:
            if self.order_cache[cid].message["status"] == 0:
                return True
        return False

class QtyManager():
    def __init__(self) -> None:
        self.buy_qty_sum = 0
        self.sell_qty_sum = 0
        self.buy_amount_sum = 0
        self.sell_amount_sum = 0
        
        self.spread = None
        self.spread_ts = time.time()
        
        self.trigger = 0
        
    def check_equal(self):
        if abs(self.sell_qty_sum - self.buy_qty_sum) / (self.sell_qty_sum + self.buy_qty_sum) * 2 < 0.0001:
            return True
        return False
    
    def feed_trade(self, trade: dict):
        if trade["s"] == "BUY":
            self.buy_qty_sum += trade["q"]
            self.buy_amount_sum += trade["q"] * trade["p"]
        else:
            self.sell_qty_sum += trade["q"]
            self.sell_amount_sum += trade["q"] * trade["p"]
        
        if self.check_equal() and self.sell_qty_sum + self.buy_qty_sum > 50 and self.trigger == 0:
            sell_price = self.sell_amount_sum / self.sell_qty_sum
            buy_price = self.buy_amount_sum / self.buy_qty_sum 
            self.spread = (sell_price - buy_price) / (sell_price + buy_price) * 2
            if time.time() - self.spread_ts > 60*15:
                self.spread = None
                self.trigger = 1
            elif self.spread > 0.0001:
                self.trigger = 2
            elif self.spread < -0.0001:
                self.trigger = 3
            else:
                self.trigger = 4
            self.spread_ts = time.time()
            
            self.buy_qty_sum = 0
            self.sell_qty_sum = 0
            self.buy_amount_sum = 0
            self.sell_amount_sum = 0
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
        self.freq = self.params["freq"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.trade_cache = PriceDistributionCache(100)
        self.keppa = self.params["keppa"]
        self.keppa_step = self.params["keppa_step"]
        alpha = self.params["alpha"]
        self.sc = SpreadCalculator(1, self.keppa, alpha,10)

        self.price_muli = 1e7
        self.qty_muli = 1e7
        self.recvWindow = self.params["recvWindow"]

        self.order_to_send = Queue()
        self.retreat_queue = Queue()
        self.trade_ts = 0
        self.bbo_ts = 0
        self.pos_limit = Decimal(5)

        self.order_cache = OrderCacheManager()
        self.qty_manager = QtyManager()
        self.ask_order_cache = dict()
        self.bid_order_cache = dict()

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False
        
        self.order_count = 0
        self.order_fail_count = 0

        self.delay = 0
        
        self.last_symbol = None
        self.ap = None

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order_inner(o)
        except:
            pass
        
    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.8*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.8*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.8*1200

        if cond1 or cond2 or cond3:
            self.logger.warning(f"{float(headers['X-MBX-USED-WEIGHT-1M'])},{float(headers['X-MBX-ORDER-COUNT-10S'])}, {float(headers['X-MBX-ORDER-COUNT-1M'])}")
            self.hold_back = True
            self.logger.warning("hold back")
            api = self.get_exchange_api("binance")[0]
            self.loop.create_task(api.flash_cancel_orders(self.get_symbol_config(self.symbol)))
            await asyncio.sleep(1)
            self.hold_back = False

    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.order_cache.pos = position.data["long_qty"] - position.data["short_qty"]
            self.order_cache.exposed_pos = position.data["long_qty"] - position.data["short_qty"]

            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol)
                qty_tick = symbol_cfg["qty_tick_size"]
                price_tick = symbol_cfg['price_tick_size']
                self.half_step_tick = qty_tick/Decimal(2)
                self.half_price_tick = price_tick/Decimal(2)
                self.price_tick = price_tick
                c = 0
                if Decimal(self.price_muli)*price_tick < Decimal(0.5) or Decimal(self.qty_muli)*qty_tick < Decimal(0.5):
                    self.logger.warning("price or qty multi not good enough")
                    exit()
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ap:
                        self.logger.warning(f"{self.ap}")
                        amount = min(balance,Decimal(self.params["amount"]))
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/qty_tick)*qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
                        break
                    if c > 1000:
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise
            
    async def update_amount(self):
        while True:
            await asyncio.sleep(0.1)
            if not self.qty_manager.trigger:
                continue
            try:
                t = self.qty_manager.trigger
                self.logger.warning(f"spread: {self.qty_manager.spread}, cur amount: {self.amount}, cur keppa: {self.keppa}")
                if t == 1:
                    self.keppa += self.keppa_step
                    self.sc.update_keppa(self.keppa)
                    amount = Decimal(self.params["amount"])
                elif t == 2:
                    amount = Decimal(self.params["amount_extend"])
                elif t == 3:
                    self.keppa -= self.keppa_step
                    amount = Decimal(self.params["amount_min"])
                elif t ==4:
                    amount = Decimal(self.params["amount"])
                self.qty_manager.trigger = 0
            
                if self.ap:
                    amount = amount / Decimal(self.ap)
                    
                    symbol_cfg = self.get_symbol_config(self.symbol)
                    qty_tick = symbol_cfg["qty_tick_size"]
                    amount = float(int(amount/qty_tick)*qty_tick)
                    
                    self.amount = amount
                    self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")
            
    async def on_order(self, o):
        # self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        client_id = order["c"]
        if client_id not in list(self.order_cache.order_cache):
            return
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        
        if status in OrderStatus.fin_status():
            filled = Decimal(order["z"])
            if Decimal(order["z"]) > Decimal(0):
                ts = time.time()*1e3
                tmp = {"t":ts,"s":order["S"],"p":float(order["ap"]),"q":float(order["z"])/self.amount}
                self.qty_manager.feed_trade(tmp)
                # self.logger.warning(f"order filled at {Decimal(order['ap'])},side:{self.order_cache.order_cache[client_id].side}, filled: {order['z']}")
            self.order_cache.order_finish(client_id,filled)

    async def on_order_inner(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache.order_cache): 
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            filled = order.filled_amount
            if order.filled_amount > Decimal(0):
                ts = time.time()*1e3
                side = "BUY" if self.order_cache.order_cache[client_id].side==OrderSide.Buy else "SELL"
                tmp = {"t":ts,"s":side,"p":float(order.avg_filled_price),"q":float(order.filled_amount)/self.amount}
                self.qty_manager.feed_trade(tmp)
                # self.logger.warning(f"order filled at {order.avg_filled_price},side:{self.order_cache.order_cache[client_id].side}, filled: {order.filled_amount}")
            self.order_cache.order_finish(client_id,filled)

    async def on_bbo(self, symbol, bbo):
        if symbol != self.last_symbol:
            self.logger.warning(f"wrong symbol: {symbol}")
        self.last_symbol = symbol
        
        self.ap = float(bbo["data"]["a"])
        self.bp = float(bbo["data"]["b"])
        self.bbo_ts = bbo["data"]["E"]
        
    
    async def on_public_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        self.trade_ts = max(self.trade_ts, trade["data"]["E"])
        tmp = {
            "t":trade["data"]["E"],
            "p":int(float(trade["data"]["p"])*self.price_muli), 
            "q":int(float(trade["data"]["q"])*self.qty_muli)
            }

        self.trade_cache.feed_trade(tmp)
        self.trade_cache.remove_expired(self.trade_ts)

    async def send_order_action(self):
        while True:
            await asyncio.sleep(self.freq/1000)
            if not self.send or self.rate_limit:
                continue
            
            cur_risk = self.order_cache.pos/self.amount_decimal
            
            mp = self.trade_cache.get_median_price()
            if not mp:
                mp = Decimal((self.ap + self.bp) / 2)
            else:
                mp = Decimal(mp / self.price_muli)
                
            ask = mp + Decimal(self.sc.get_ask(float(cur_risk))) * self.price_tick
            bid = mp - Decimal(self.sc.get_bid(float(cur_risk))) * self.price_tick

            if ask < Decimal(self.ap):
                ask = Decimal(self.ap)
            if bid > Decimal(self.bp):
                bid = Decimal(self.bp)
            
            if not self.hold_back:
                self.send_entry_order(ask,bid,self.amount_decimal)
                
    def send_entry_order(self,ask,bid,qty):
        a, b = self.order_cache.can_send_order()
        cur_risk = self.order_cache.pos/self.amount_decimal
        if not a and cur_risk > -self.pos_limit:
            if abs(cur_risk) < 1 and cur_risk > 0:
                self.loop.create_task(self.send_order(ask,qty+self.order_cache.pos,OrderSide.Sell))
            else:
                self.loop.create_task(self.send_order(ask,qty,OrderSide.Sell))
        if not b and cur_risk < self.pos_limit:
            if abs(cur_risk) < 1 and cur_risk < 0:
                self.loop.create_task(self.send_order(bid,qty-self.order_cache.pos,OrderSide.Buy))
            else:
                self.loop.create_task(self.send_order(bid,qty,OrderSide.Buy))
            
    async def rate_limit_check(self):
        while True:
            await self.retreat_queue.get()
            if self.rate_limit and self.send:
                self.send = False
                await self.retreat()
                self.send = True
            
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=None,timestamp=None):
        client_id = ClientIDGenerator.gen_client_id("binance")
        self.order_count += 1
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow
        
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = recvWindow/1000
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        extra_info["status"] =  0
        extra_info["filled"] =  Decimal(0)
        extra_info["sent"] = False
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache.add_order(order)
        if timestamp == None:
            timestamp = order.create_ms
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=timestamp,
                recvWindow=recvWindow,
                client_id=client_id
            )
            self.order_cache.after_send_ack(client_id)
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            self.order_fail_count += 1
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except ExchangeTemporaryError as err:
            await self.internal_fail(client_id, err)
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def cancel_order(self, order: Order, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.json
        except Exception as err:
            if not catch_error:
                raise
            if type(err) in (OrderAlreadyCompletedError, OrderNotFoundError):
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect True")
                return True
            elif type(err) == ExchangeTemporaryError:
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect False")
                return False
            elif isinstance(err, ExchangeConnectorException):
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, raise unknown api error")
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False

    async def batch_cancel(self, oid, order):
        try:
            if self.order_cache.order_cache.get(oid) == None:
                return
            if oid not in self.canceling_id and self.order_cache.order_cache[oid].message["cancel"] < 4:
                self.canceling_id.add(oid)
                res = await self.cancel_order(order)
                self.canceling_id.remove(oid)
                self.order_cache.after_cancel(oid, res)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
        while True:
            await asyncio.sleep(0.01)
            for oid, order in self.order_cache.order_cache.items():
                self.loop.create_task(batch_cancel(oid, order))
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if not order.message:
                # self.logger.warning(f"order has no message while try to check,{oid}")
                return
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if not order.message["sent"]:
                # self.logger.warning(f"order not sent yet while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*6 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["query"] += 1
                        if self.order_cache.order_cache[oid].message["query"]>10:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order_inner(order_new)
                        else:
                            self.loop.create_task(self.batch_cancel(oid,order_new))
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(Decimal(self.bp*0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(Decimal(self.ap*1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None,"fail rate":self.order_fail_count/self.order_count if self.order_count else 0})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(1)
        await asyncio.gather(
            self.update_redis_cache(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            self.update_amount(),
            self.rate_limit_check(),
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    