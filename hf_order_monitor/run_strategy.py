from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.helpers import send_ding_talk
import collections

from decimal import Decimal 
import asyncio
from asyncio.queues import Queue
import time
import numpy as np
import simplejson




class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_binance = []
        
        self.symbol_binance.append(f"binance.btc_usdt_swap.usdt_contract.na")

        self.last_5_min = Decimal(0)
        self.last_1_hour = Decimal(0)
        self.last_6_hour = Decimal(0)
        self.last_24_hour = Decimal(0)
        self._dict = dict()

    async def before_strategy_start(self):
        for symbol in self.symbol_binance:
            self.direct_subscribe_order_update(symbol_name=symbol)
        self.logger.setLevel("WARNING")

    async def _on_order_inner_(self, order: PartialOrder):

        if order.xchg_status not in OrderStatus.fin_status():
            return

        if order.filled_amount == Decimal(0):
            return

        # if order.raw_data:
        #     order_info = order.raw_data["o"]
        #     side = order_info.raw_data["S"]
        #     price = order_info.raw_data["p"]
        #     qty = order_info.raw_data["z"]
        #     pair = order_info.raw_data["s"]
        #     i = order_info.raw_data["i"]
        #     self.logger.warning(f"pair:{pair}, price:{price}, qty:{qty}, side:{side},{i}")

        self.last_5_min += order.avg_filled_price * order.filled_amount
        self.last_1_hour += order.avg_filled_price * order.filled_amount
        self.last_6_hour += order.avg_filled_price * order.filled_amount
        self.last_24_hour += order.avg_filled_price * order.filled_amount

    async def five_min(self):
        # goal for 60,000 
        while True:
            await asyncio.sleep(60*5)
            self._dict["5 min vol"] = self.last_5_min
            await self.redis_set_cache(self._dict)
            self.last_5_min = Decimal(0)

    async def one_hour(self):
        # goal for 800,000 
        while True:
            await asyncio.sleep(60*60)
            self._dict["1 hour vol"] = self.last_1_hour
            await self.redis_set_cache(self._dict)
            self.last_1_hour = Decimal(0)

    async def six_hour(self):
        # goal for 5,000,000 
        while True:
            await asyncio.sleep(60*60*6)
            self._dict["6 hour vol"] = self.last_6_hour
            await send_ding_talk(title="INFO 6 hour trading volume", message="6hr vol: " + str(self.last_6_hour),
                                             token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3")
            await self.redis_set_cache(self._dict)
            self.last_6_hour = Decimal(0)
            
    async def twenty_four_hour(self):
        while True:
            await asyncio.sleep(60*60*24)
            self._dict["24 hour vol"] = self.last_24_hour
            await self.redis_set_cache(self._dict)
            self.last_24_hour = Decimal(0)
            

    async def strategy_core(self):
        await asyncio.sleep(10)
        while True:
             await asyncio.gather(
                 self.five_min(),
                 self.one_hour(),
                 self.six_hour(),
                 self.twenty_four_hour(),
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()