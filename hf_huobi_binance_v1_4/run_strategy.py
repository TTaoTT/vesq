
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections

from decimal import Decimal 
import asyncio
from asyncio.queues import Queue
import time
import numpy as np

# v1_1: Improved the way of closing the position.
# v1_2: trigger closing position at binance bbo
# v1_3: improve cancel huobi order

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.order_cache = dict()
        self.unknown_order_dict = dict()
        self.pos = Decimal(0)
        self.pos_huobi = Decimal(0)
        self.pos_huobi_short = Decimal(0)
        self.pos_huobi_long = Decimal(0)

        self.ap_huobi = Decimal(0)
        self.bp_huobi = Decimal(0)

        self._orders_waiting_to_send = Queue()
        self._orders_waiting_to_cancel = Queue()
        self._close_position = Queue()

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        self.base_amount = self.params["base_amount"]
        self.spread_min = self.params["spread_min"]

        symbol = self.params["symbol"]

        self.symbol_binance = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.symbol_huobi = f"huobi.{symbol}_usdt_swap.usdt_swap.cross"

        self.action_count = 0
        self.buy_count = 0
        self.sell_count = 0

        self.buy_count_ts = []
        self.sell_count_ts = []

        self.ap_binance = Decimal(0)
        self.bp_binance = Decimal(0)

        self.close_in_action = False

        self.cancel_time = dict()
        self.cancel_trigger_time = dict()

    async def before_strategy_start(self):
        self.direct_subscribe_orderbook(symbol_name=self.symbol_huobi)
        self.direct_subscribe_order_update(symbol_name=self.symbol_huobi)
        self.direct_subscribe_order_update(symbol_name=self.symbol_binance)
        self.direct_subscribe_public_trade(symbol_name=self.symbol_binance)
        await self.direct_subscribe_bbo(symbol_name=self.symbol_binance)
        self.logger.setLevel("WARNING")

        self.symbol_binance_id = self.get_symbol_config(self.symbol_binance)["id"]
        self.symbol_huobi_id = self.get_symbol_config(self.symbol_huobi)["id"]

        symbol_cfg = self.get_symbol_config(self.symbol_huobi)
        self.cv = symbol_cfg['contract_value']

        await self.direct_check_position()

    async def on_bbo(self, symbol, bbo):

        if not self.close_in_action and self._close_position.empty():
            self._close_position.put_nowait(1)

        if Decimal(bbo.ask[0]) == self.ap_binance or Decimal(bbo.bid[0]) == self.bp_binance:
            return

        self.ap_binance = Decimal(bbo.ask[0])
        self.bp_binance = Decimal(bbo.bid[0])

        ask_order = None
        bid_order = None

        for oid in list(self.order_cache):
            if self.order_cache[oid].raw_data["source"] == 1:
                if self.order_cache[oid].side == OrderSide.Sell:
                    ask_order = self.order_cache[oid]
                    ask_id = oid
                else:
                    bid_order = self.order_cache[oid]
                    bid_id = oid
        
        if ask_order:
            ask_spread = self.ap_huobi - self.ap_binance
            if ask_spread < self.spread_min*self.ap_huobi:
                if self.cancel_trigger_time.get(ask_id) is None:
                    self.cancel_trigger_time[ask_id] = time.time()
                self.order_cache[ask_id].raw_data["stop_ts"] = 0
                self.order_cache[ask_id].raw_data["cancel"] += 1
                await self._orders_waiting_to_cancel.put(self.order_cache[ask_id])
        if bid_order:
            bid_spread = self.bp_binance - self.bp_huobi
            if bid_spread < self.spread_min*self.bp_huobi:
                if self.cancel_trigger_time.get(bid_id) is None:
                    self.cancel_trigger_time[bid_id] = time.time()
                self.order_cache[bid_id].raw_data["stop_ts"] = 0
                self.order_cache[bid_id].raw_data["cancel"] += 1
                await self._orders_waiting_to_cancel.put(self.order_cache[bid_id])

        
    async def on_public_trade(self, symbol, trade: PublicTrade):
        if trade.side == OrderSide.Buy:
            self.buy_count += 1
            self.buy_count_ts.append(trade.server_ms)
        else:
            self.sell_count += 1
            self.sell_count_ts.append(trade.server_ms)

    async def on_orderbook(self, symbol, orderbook):
        if symbol in self.symbol_huobi:
            self.ap_huobi = orderbook["asks"][0][0]
            self.bp_huobi = orderbook["bids"][0][0]
            
            if self._orders_waiting_to_send.empty():
                self._orders_waiting_to_send.put_nowait(1)
            
    async def _on_order_inner_(self, order: PartialOrder):
        xchg_id = order.xchg_id

        if xchg_id not in list(self.order_cache):
            self.unknown_order_dict[xchg_id] = order
            return

        if order.filled_amount < self.order_cache[xchg_id].filled_amount:
            return

        self.handle_pos(xchg_id, order)

        self.order_cache[xchg_id].avg_filled_price = order.avg_filled_price
        self.order_cache[xchg_id].filled_amount = order.filled_amount
        self.order_cache[xchg_id].xchg_status = order.xchg_status

        if order.xchg_status in OrderStatus.fin_status():
            self.order_cache.pop(xchg_id)
            try:
                self.cancel_time.pop(xchg_id)
                self.cancel_trigger_time.pop(xchg_id)
            except:
                pass
            if xchg_id in self.unknown_order_dict:
                self.unknown_order_dict.pop(xchg_id)
            
    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[xchg_id].filled_amount
        if amount_changed > Decimal(0):
            if self.order_cache[xchg_id].symbol_id == self.symbol_binance_id:
                ratio = Decimal(1)
                huobi_pos = False
            else:
                ratio = self.cv
                huobi_pos = True
                if not self.close_in_action and self._close_position.empty():
                    self._close_position.put_nowait(1)
            if self.cancel_time.get(xchg_id):
                gap = time.time() - self.cancel_time[xchg_id]
            else:
                gap = None
            self.logger.warning(f"filled amount:{p_order.filled_amount}, price{p_order.avg_filled_price}, side:{self.order_cache[xchg_id].side},id:{xchg_id}, cancel time:{gap}")
            if self.order_cache[xchg_id].side == OrderSide.Buy:
                self.pos += (amount_changed * ratio)
                self.pos_huobi += (amount_changed * huobi_pos)
                if self.order_cache[xchg_id].position_side == OrderPositionSide.Open:
                    self.pos_huobi_long += (amount_changed * huobi_pos)
                elif self.order_cache[xchg_id].position_side == OrderPositionSide.Close:
                    self.pos_huobi_short -= (amount_changed * huobi_pos)
            else:
                self.pos -= (amount_changed * ratio)
                self.pos_huobi -= (amount_changed * huobi_pos)
                if self.order_cache[xchg_id].position_side == OrderPositionSide.Open:
                    self.pos_huobi_short += (amount_changed * huobi_pos)
                elif self.order_cache[xchg_id].position_side == OrderPositionSide.Close:
                    self.pos_huobi_long -= (amount_changed * huobi_pos)
            if self.order_cache[p_order.xchg_id].raw_data["source"] == 1:
                self.open_price = p_order.avg_filled_price
            self.logger.warning(f"pos:{self.pos}, pos_huobi:{self.pos_huobi}")
        
    def generate_orders(self):
        config_binance = self.get_symbol_config(self.symbol_binance)

        if self.order_cache or not (abs(self.pos) < config_binance["min_quantity_val"] or abs(self.pos)*self.ap_binance < config_binance["min_notional_val"]):
        
            return []

        orders = []
        symbol_cfg = self.get_symbol_config(self.symbol_huobi)
        price_tick = symbol_cfg['price_tick_size']

        bid_spread = self.bp_binance - self.bp_huobi
        ask_spread = self.ap_huobi - self.ap_binance
   
       
        if bid_spread > self.spread_min*self.bp_huobi and self.pos_huobi <= Decimal(0):
            if self.base_amount <= self.pos_huobi_short:
                orders.append([self.symbol_huobi, self.bp_huobi+price_tick, self.base_amount, OrderSide.Buy, OrderPositionSide.Close, OrderType.PostOnly,0.2,1])
            else:
                orders.append([self.symbol_huobi, self.bp_huobi+price_tick, self.base_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,0.2,1])
        elif ask_spread > self.spread_min*self.ap_huobi and self.pos_huobi >= Decimal(0):
            if self.base_amount <= self.pos_huobi_long:
                orders.append([self.symbol_huobi, self.ap_huobi-price_tick, self.base_amount, OrderSide.Sell, OrderPositionSide.Close, OrderType.PostOnly,0.2,1])
            else:
                orders.append([self.symbol_huobi, self.ap_huobi-price_tick, self.base_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,0.2,1])
        return orders

    async def close_position(self):

        async def send_order(params):
            if not self.volume_notional_check(*params[:3]):
                return
            try:
                order =  await self.make_order(*params[:6])
                
                if order:
                    extra_info = dict()
                    extra_info["stop_ts"] = params[6]
                    extra_info["cancel"] = 0
                    extra_info["source"] =  params[7] # entry or balance
                    extra_info["query"] =  0
                    order.raw_data = extra_info
                    self.order_cache[order.xchg_id] = order
                    self.order_cache[order.xchg_id].filled_amount = Decimal(0)
                    if order.xchg_id in self.unknown_order_dict:
                        await self._on_order_inner_(self.unknown_order_dict[order.xchg_id])

            except Exception as err:
                self.logger.warning(f"close order err, {err}")

        symbol_cfg = self.get_symbol_config(self.symbol_binance)
        price_tick = symbol_cfg['price_tick_size']

        while self.is_running():
            await self._close_position.get()
            self.close_in_action = True
            try:
                if abs(self.pos) > self.base_amount*Decimal(0.01)*self.cv:
                    if self.pos<0:
                        buy_source = [key for key in list(self.order_cache) if (self.order_cache[key].side == OrderSide.Buy and self.order_cache[key].symbol_id == self.symbol_binance_id)]
                        if len(buy_source) == 1:
                            if self.order_cache[buy_source[0]].requested_price < self.bp_binance:
                                for _ in range(2):
                                    await self.cancel_order(self.order_cache[buy_source[0]])
                                    await asyncio.sleep(0.1)                                  
                                self.action_count += 2
                        elif len(buy_source) == 0:
                            if self.open_price > self.ap_binance * Decimal(1.00025):
                                self.logger.warning(f"taker buy close {self.pos}, price:{self.ap_binance}")
                                await send_order([self.symbol_binance, self.ap_binance, -self.pos, OrderSide.Buy, OrderPositionSide.Both, OrderType.IOC,0.2,-1])
                                self.action_count += 1
                            else:
                                bid_p = min(self.ap_binance-price_tick, self.bp_binance+price_tick)
                                self.logger.warning(f"maker buy close {self.pos}, price:{bid_p}")
                                await send_order([self.symbol_binance, bid_p, -self.pos, OrderSide.Buy, OrderPositionSide.Both, OrderType.PostOnly,0.2,-1])
                                self.action_count += 1
                    else:
                        sell_source = [key for key in list(self.order_cache) if (self.order_cache[key].side == OrderSide.Sell and self.order_cache[key].symbol_id == self.symbol_binance_id)]
                        if len(sell_source) == 1:
                            if self.order_cache[sell_source[0]].requested_price > self.ap_binance:
                                for _ in range(2):
                                    await self.cancel_order(self.order_cache[sell_source[0]])
                                    await asyncio.sleep(0.1) 
                                self.action_count += 2
                        elif len(sell_source) == 0:
                            if self.open_price < self.bp_binance * Decimal(1-0.00025):
                                self.logger.warning(f"taker sell close {self.pos}, price:{self.bp_binance}")
                                await send_order([self.symbol_binance,  self.bp_binance, self.pos, OrderSide.Sell, OrderPositionSide.Both, OrderType.IOC,0.2,-1])
                                self.action_count += 1
                            else:
                                ask_p = max(self.ap_binance-price_tick, self.bp_binance+price_tick)
                                self.logger.warning(f"maker sell close {self.pos}, price:{ask_p}")
                                await send_order([self.symbol_binance, ask_p, self.pos, OrderSide.Sell, OrderPositionSide.Both, OrderType.PostOnly,0.2,-1])
                                self.action_count += 1
            except:
                pass
            self.close_in_action = False

    async def _make_order_(self, symbol_config, price: str,
                           volume: str,
                           side: OrderSide,
                           position_side: OrderPositionSide,
                           order_type: OrderType,
                           tag, extra_info, account_name, **kwargs):
        if account_name:
            api = self.get_exchange_api_by_account(account_name)
        else:
            api = self.get_exchange_api(symbol_config['exchange_name'])[0]
        response = await api.make_order(symbol=symbol_config, price=price, quantity=volume, side=side, position_side=position_side, order_type=order_type, **kwargs)
        response.data.strategy_name = self.config['strategy']['name']
        response.data.tag = tag
        response.data.extra_info = extra_info
        return response

    async def cancel_order(self, order: Order):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.xchg_id} {r.data}")
            return r.data
        except (OrderAlreadyCompletedError, OrderNotFoundError) as e:
            return True
        except ExchangeTemporaryError as e:
            self.logger.error(f"[cancel order error] [{e}] {order.xchg_id}, redirect False")
            return False
        except ExchangeConnectorException as _err:
            self.logger.error(f"[cancel order error] {_err} {order.xchg_id}, raise unknown api error")
            raise
        except Exception as err:
            self.logger.error(f"[cancel order error] {err!r} {order.xchg_id}, redirect False")
            return False

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def send_order_action(self):
        async def batch_send_order(params):
            if not self.volume_notional_check(*params[:3]):
                return
            try:
                order =  await self.make_order(*params[:6])
                self.action_count += 1
                if order:
                    extra_info = dict()
                    extra_info["stop_ts"] = params[6]
                    extra_info["cancel"] = 0
                    extra_info["source"] =  params[7] # entry or balance
                    extra_info["query"] =  0
                    order.raw_data = extra_info
                    self.order_cache[order.xchg_id] = order
                    self.order_cache[order.xchg_id].filled_amount = Decimal(0)
                    if order.xchg_id in self.unknown_order_dict:
                        await self._on_order_inner_(self.unknown_order_dict[order.xchg_id])

            except Exception as err:
                self.logger.warning(f"send order err, {err}")

        while self.is_running():
            await self._orders_waiting_to_send.get()
            if self.action_count < 50:
                orders = self.generate_orders()
                if orders:
                    try:
                        await asyncio.gather(*[batch_send_order(order_params) for order_params in orders])
                    except Exception as err:
                        self.logger.warning(f"batch send order err, {err}")

    async def update_cancel_order(self):

        async def batch_cancel(oid, order):

            if time.time()*1e3 - order.create_ms > order.raw_data["stop_ts"] * 1e3 + order.raw_data["cancel"]*1e3:
                try:
                    await self._orders_waiting_to_cancel.put(order)
                    if self.order_cache.get(oid):
                        self.order_cache[oid].raw_data["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f'cancel order update {oid} err {err}')
            
        while True:
            await asyncio.sleep(0.001)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

    async def cancel_order_action(self):

        while self.is_running():
            order = await self._orders_waiting_to_cancel.get()
            if self.order_cache.get(order.xchg_id):
                try:
                    if self.cancel_time.get(order.xchg_id) is None:
                        self.cancel_time[order.xchg_id] = time.time()
                    await self.cancel_order(order)
                    self.action_count += 1
                except Exception as err:
                    self.logger.warning(f'cancel order {order.xchg_id} err {err}')

    async def direct_check_position(self):
        api = self.get_exchange_api("huobi")[0]
        position = await api.contract_position(self.get_symbol_config(self.symbol_huobi))
        if position.data.get("long_qty") is not None:
            self.pos_huobi_long = position.data["long_qty"]
            self.pos_huobi_short = position.data["short_qty"]
            self.pos_huobi = self.pos_huobi_long - self.pos_huobi_short

        api = self.get_exchange_api("binance")[0]
        position = await api.contract_position(self.get_symbol_config(self.symbol_binance))
        if position.data.get("amount") is not None:
            pos = position.data["amount"]
            self.pos = pos + self.pos_huobi * self.cv

    async def match_position(self):
        while True:
            await asyncio.sleep(120)  
            try:
                await self.direct_check_position()
                
            except Exception as err:
                self.logger.warning(f"match position err, {err}")

    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            self.action_count += 1
            res = await api.order_match_result(self.get_symbol_config(order.symbol_id),order.xchg_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if time.time()*1e3 - order.create_ms > (order.raw_data["stop_ts"] + 1)*1e3 and order.raw_data["cancel"]>0:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].raw_data["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self._on_order_inner_(order_new)
                    elif self.order_cache[oid].raw_data["query"]>10:
                        self.order_cache.pop(oid)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def action_restriction(self):
        while True:
            await asyncio.sleep(1)
            self.action_count = 0

            
    async def strategy_core(self):
        await asyncio.sleep(15)
        await asyncio.gather(
            self.send_order_action(),
            self.update_cancel_order(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.match_position(),
            self.action_restriction(),
            self.close_position(),
        )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()