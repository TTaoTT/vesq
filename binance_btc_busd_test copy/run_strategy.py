import asyncio
from decimal import Decimal
from re import L
from selectors import EpollSelector
from this import d
import time 
import numpy as np
import collections
from asyncio.queues import Queue
import pandas as pd

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class SpreadCalculatorLite():
    def __init__(self, compound_alpha) -> None:
        self.lam = 1
        self.compound_alpha = compound_alpha
        self.t = 10
    
    def get_h(self,q, keppa):
        w1 = np.exp(-self.compound_alpha*(q**2))
        w2 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.compound_alpha*((q-1)**2))
        w3 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.compound_alpha*((q+1)**2))
        if w1 + w2 + w3 ==0 :
            return -0.01
        return 1/keppa * np.log(w1 + w2 + w3)
    
    def get_ask(self,inven, keppa):
        return 1/keppa - self.get_h(inven-1, keppa) + self.get_h(inven, keppa)
    
    def get_bid(self,inven, keppa):
        return 1/keppa - self.get_h(inven+1, keppa) + self.get_h(inven, keppa)
        
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_busd.spot.na"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_order_queue = Queue()
        self.cancel_order_queue = Queue()
        self.hedge_queue = Queue()
        self.bbo_ts = None
        
        self.qty = Decimal("0.001")
        self.gap = 40
        self.gap_replace = 1

        self.cancel_ts = 0
        self.canceling_id = set()
        self.pos = 0
        self.send = True
        self.sending = False
        
        self.ap = None
        self.sc = SpreadCalculatorLite(0.5)
        
        self.last_bbo = 0
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order(o)
        except:
            pass

    async def before_strategy_start(self):
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
        await asyncio.sleep(5)
        self.use_raw_stream = True
        self.direct_subscribe_bbo(symbol_name=self.symbol)
    
        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        self.half_step_tick = qty_tick/Decimal(2)
        self.half_price_tick = price_tick/Decimal(2)
        self.price_tick = price_tick
        
        self.api = dict()
        self.order_load_10s = dict()
        self.order_load_daily = dict()
        self.acc_available = dict()
        self.logger.setLevel("WARNING")
        
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
            except:
                pass
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            print(cur_balance,acc)
            self.api[acc] = api
            self.order_load_10s[acc] = 0
            self.order_load_daily[acc] = 0
            self.acc_available[acc] = dict()
            self.acc_available[acc]["btc"] = cur_balance["btc"]["available"]
            self.acc_available[acc]["busd"] = cur_balance["busd"]["available"]
            
        c = 0
        while True:
            await asyncio.sleep(1)
            c += 1
            if self.ap:
                l = self.acc_available.values()
                btc = sum(item['btc'] for item in l)*self.ap
                busd = sum(item['busd'] for item in l)
                self.starting_cash = btc + busd
                self.logger.warning(f"starting cash: {self.starting_cash}")
                
                if btc / (btc + busd) < 0.4:
                    amount = ((btc + busd) / 2 -btc)
                    qty = amount / self.ap
                    
                    for acc in self.account_names:
                        print(amount)
                        amout_temp = self.acc_available[acc]["busd"]
                        if amout_temp >= amount:
                            await self.send_order(self.ap*Decimal(1.03),qty,OrderSide.Buy,account_name=acc)
                            break
                        else:
                            await self.send_order(self.ap*Decimal(1.03),self.acc_available[acc]["busd"]/self.ap/Decimal(1.04),OrderSide.Buy,account_name=acc)
                            amount -= amout_temp
                            qty = amount / self.ap
                elif btc / (btc + busd) > 0.6:
                    qty = (btc - (btc + busd) / 2) / self.ap
                    for acc in self.account_names:
                        print(qty)
                        qty_temp = self.acc_available[acc]["btc"]
                        if qty_temp >= qty:
                            await self.send_order(self.ap*Decimal(1 - 0.03),qty,OrderSide.Sell,account_name=acc)
                            break
                        else:
                            await self.send_order(self.ap*Decimal(1 - 0.03),self.acc_available[acc]["btc"],OrderSide.Sell,account_name=acc)
                            qty -= qty_temp
                break
            if c > 1000:
                break
            
        for acc, api in self.api.items():
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data    
            print(cur_balance,acc)
            
        
    async def on_bbo(self,symbol, bbo):
        self.ap = Decimal(bbo["data"]["a"])
        self.bp = Decimal(bbo["data"]["b"])
        t = int(time.time()*1e3)
        if self.send and not self.sending and self.send_order_queue.empty() and t - self.last_bbo >40:
            self.send_order_queue.put_nowait(1)
            self.last_bbo = t

    async def on_order(self, order):
        if order.xchg_status in OrderStatus.fin_status():
            try:
                side = self.order_cache[order.client_id].side
                acc = self.order_cache[order.client_id].account_name
                residue = self.order_cache[order.client_id].requested_amount - order.filled_amount
                if side == OrderSide.Buy:
                    self.acc_available[acc]["busd"] += residue * self.order_cache[order.client_id].requested_price
                    self.acc_available[acc]["btc"] += order.filled_amount
                    self.pos += order.filled_amount
                else:
                    self.acc_available[acc]["btc"] += residue
                    self.acc_available[acc]["busd"] += order.filled_amount * order.avg_filled_price
                    self.pos -= order.filled_amount
            except Exception as err:
                self.logger.warning(f"hanle order err: {err}")
            self.order_cache.pop(order.client_id)
    
    async def handle_limit(self,headers,acc):
        # self.logger.warning(f"{headers}")
        self.order_load_10s[acc] = int(headers["x-mbx-order-count-10s"])
        self.order_load_daily[acc] = int(headers["x-mbx-order-count-1d"])
        
        self.ip_rate_limit = int(headers["x-mbx-used-weight-1m"])
        self.gap_replace = max(Decimal(self.ip_rate_limit/1200) * self.gap, Decimal(self.gap/4) )
        if self.ip_rate_limit > 0.9 * 1200:
            self.logger.warning("ip limit")
            self.send = False
            await asyncio.sleep(10)
            self.send = True
    
    async def reduce_count(self):
        while True:
            await asyncio.sleep(0.5)
            acc_list = sorted(self.order_load_10s, key=self.order_load_10s.get)
            self.order_load_10s[acc_list[-1]] -= 1
            
            
    async def send_order_action(self):
        while True:
            await self.send_order_queue.get()
            self.sending = True
            mp = (self.ap+self.bp) / 2
            cur_risk = self.pos/self.qty
            cur_risk = max(cur_risk, -5)
            cur_risk = min(cur_risk, 5)
            ask = mp + Decimal(self.sc.get_ask(float(cur_risk),1/self.gap)) * self.price_tick
            bid = mp - Decimal(self.sc.get_bid(float(cur_risk),1/self.gap)) * self.price_tick
            
            for oid, order in self.order_cache.items():
                if order.side == OrderSide.Buy and abs(order.requested_price - bid) > self.price_tick * self.gap_replace or\
                    order.side == OrderSide.Sell and abs(order.requested_price - ask) > self.price_tick * self.gap_replace:
                        if order.message["cancel"] is False:
                            self.loop.create_task(self.handle_cancel_order(oid))
            
            ask_count, bid_count = self.open_orders()
            acc_list = sorted(self.order_load_10s, key=self.order_load_10s.get)
            
            if not ask_count:
                for acc in acc_list:
                    if self.acc_available[acc]["btc"] > self.qty:
                        self.loop.create_task(self.send_order(ask,self.qty,OrderSide.Sell,account_name=acc))
                        break
            if not bid_count:
                for acc in acc_list:
                    if self.acc_available[acc]["busd"] > self.qty * bid:
                        self.loop.create_task(self.send_order(bid,self.qty,OrderSide.Buy,account_name=acc))
                        break
            self.sending = False
            
    def open_orders(self):
        ask = 0
        bid = 0
        for oid in list(self.order_cache.keys()):
            if self.order_cache.get(oid) is not None and self.order_cache[oid].message["cancel"] is False:
                side = self.order_cache[oid].side
                if side == OrderSide.Buy:
                    bid += 1
                else:
                    ask += 1
        return ask, bid
    
    async def handle_cancel_order(self,oid):
        try:
            if self.order_cache.get(oid):
                if oid not in self.canceling_id:
                    self.canceling_id.add(oid)
                    self.order_cache[oid].message["cancel"] = True
                    res = await self.cancel_order(self.order_cache[oid])

                    if self.order_cache.get(oid) and res is False:
                        self.order_cache[oid].message["cancel"] = False
                    self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
            
    async def send_order(self,price,qty,side,account_name=None,order_type=OrderType.Limit,recvWindow=100):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            order_type=order_type,
            position_side=OrderPositionSide.Open,
            account_name=account_name
        )
        extra_info = dict()
        extra_info["cancel"] = False
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        
        if side == OrderSide.Buy:
            self.acc_available[account_name]["busd"] -= price * qty
        else:
            self.acc_available[account_name]["btc"] -= qty
            
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                order_type=order_type,
                client_id=client_id,
                account_name=account_name,
                position_side=OrderPositionSide.Open,
                recvWindow=recvWindow,
            )
            await self.handle_limit(res.headers,account_name)
        except RateLimitException as err:
            self.logger.warning(self.acc_available)
            await self.internal_fail(client_id, err)
        except Exception as err:
            self.logger.warning(f"{err}")
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)
        
    
    async def batch_cancel(self, oid, order):
        try:
            if self.order_cache.get(oid) == None:
                return
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                await self.cancel_order(order)
                self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if order.message["cancel"] and time.time()*1e3 - order.create_ms > 5000:
                try:
                    await self.batch_cancel(oid, order)
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
        while True:
            await asyncio.sleep(2)
            for oid, order in self.order_cache.items():
                self.loop.create_task(batch_cancel(oid, order))
    
    async def retreat(self):
        for _ in range(3):
            for api in self.api.values():
                try:
                    await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                except Exception as err:
                    self.logger.warning(f"retreat err:{err}")
            await asyncio.sleep(3)
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    cash = 0
                    for acc in self.account_names:
                        cur_balance = (await self.api[acc].account_balance(self.get_symbol_config(self.symbol))).data
                        cash += cur_balance["btc"]["all"] * self.ap + cur_balance["busd"]["all"]
                    self.logger.warning(f"ending cash: {cash}")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
            # self.cancel_order_action(),
            self.send_order_action(),
            self.update_redis_cache(),
            self.reduce_count()
            )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
        