import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue
from typing import AnyStr, List, Union
import ujson as json
import math

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_executor.order_executor import OrderExecutorTest2, TransOrder
from xex_mm.utils.base import Depth, Trade, Direction, ReferencePriceCalculator
from xex_mm.utils.configs import price_precision, qty_precision

from atom.model.depth import Depth as AtomDepth
import traceback


ART = 100
FREQUENCY = 500
FLOOR = 0.001
BETA_BASE = 1e-2
T = 1000
T_F = 500

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.symbol = "binance.btc_usdt.spot.na"
        self.contract = "btc_usdt"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_order_queue = Queue()
        self.cancel_order_queue = Queue()
        self.bbo_ts = None
        
        self.prc_per_diff = 0.00005
        self.ref_prc_cal = ReferencePriceCalculator()

        self.cancel_ts = 0
        self.send = True
        self.sending = False
        self.canceling = False
        
        self.ap = None
        
        self.last_bbo = 0
        
        self.executor = OrderExecutorTest2(
            iids=["binance.btc_usdt"], 
            contract=self.contract, 
            art=ART, 
            floor=FLOOR,
            onboard=True)
        
        self.traders = [
            "hf_trader1",
            "hf_trader2",
            "hf_trader3",
            "hf_trader4",
            "hf_trader5",
            "hf_trader6",
        ]
    
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def internal_fail(self,client_id,acc,err=None):
        try:
            self.logger.critical(f"failed order: {err}, acc:{acc}, client_id={client_id}")
            if self.order_cache.get(client_id) == None:
                return
            o = self.order_cache[client_id]
            self.logger.warning(f"order: {o}")
            self.logger.warning(f"acc available={self.acc_available[acc]['usdt'] if o.side == OrderSide.Buy else self.acc_available[acc]['btc']}")
            o.xchg_status = OrderStatus.Failed  
            await self.on_order(o)
        except:
            pass
    
    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))

    async def before_strategy_start(self):
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
        self.direct_subscribe_orderbook(symbol_name=self.symbol, is_incr_depth=True)
        
        ch1 = "mq:hf_signal:order_response"
        ch2 = "mq:hf_signal:cancel_response"
        self.loop.create_task(self.subscribe_to_channel([ch1],self.process_order_response))
        self.loop.create_task(self.subscribe_to_channel([ch2],self.process_cancel_response))
    
        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        self.half_step_tick = qty_tick/Decimal(2)
        self.half_price_tick = price_tick/Decimal(2)
        self.price_tick = price_tick
        
        self.api = dict()
        self.order_load_10s = dict()
        self.order_load_daily = dict()
        self.ip_load = dict()
        self.acc_available = dict()
        self.logger.setLevel("WARNING")
        
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
            except:
                pass
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            print(cur_balance,acc)
            self.api[acc] = api
            self.order_load_10s[acc] = 0
            self.order_load_daily[acc] = 0
            self.acc_available[acc] = dict()
            self.acc_available[acc]["btc"] = cur_balance["btc"]["available"]
            self.acc_available[acc]["usdt"] = cur_balance["usdt"]["available"]
        
        for strategy_name in self.traders:
            self.ip_load[strategy_name] = 0
            
        c = 0
        while True:
            await asyncio.sleep(1)
            c += 1
            if self.ap:
                l = self.acc_available.values()
                btc = sum(item['btc'] for item in l)*self.ap
                usdt = sum(item['usdt'] for item in l)
                self.starting_cash = btc + usdt
                self.logger.critical(f"starting total: {self.starting_cash}")
                self.logger.critical(f"starting cash: {usdt}")
                self.logger.critical(f"starting btc: {btc}")
                self.logger.critical(f"current price: {self.ap}")
                
                if btc / (btc + usdt) < 0.5:
                    amount = ((btc + usdt) / 2 -btc)
                    qty = amount / self.ap
                    
                    for acc in self.account_names:
                        print(amount)
                        amout_temp = self.acc_available[acc]["usdt"]
                        if amout_temp >= amount:
                            await self.send_order(
                                price=self.ap*Decimal(1.03),
                                qty=qty,
                                side=OrderSide.Buy,
                                account_name=acc, 
                                strategy_name=self.traders[0],
                                label=1
                                )
                            break
                        else:
                            await self.send_order(
                                price=self.ap*Decimal(1.03),
                                qty=amout_temp*Decimal(0.9)/self.ap/Decimal(1.03),
                                side=OrderSide.Buy,
                                account_name=acc,
                                strategy_name=self.traders[0],
                                label=1
                                )
                            amount -= amout_temp*Decimal(0.9)
                            qty = amount / self.ap
                elif btc / (btc + usdt) > 0.5:
                    qty = (btc - (btc + usdt) / 2) / self.ap
                    for acc in self.account_names:
                        print(qty)
                        qty_temp = self.acc_available[acc]["btc"]
                        print(acc,qty_temp)
                        if qty_temp >= qty:
                            await self.send_order(
                                price=self.ap*Decimal(1-0.03),
                                qty=qty,
                                side=OrderSide.Sell,
                                account_name=acc,
                                strategy_name=self.traders[0],
                                label=1
                                )
                            break
                        else:
                            await self.send_order(
                                price=self.ap*Decimal(1-0.03),
                                qty=qty_temp*Decimal(0.9),
                                side=OrderSide.Sell,
                                account_name=acc,
                                strategy_name=self.traders[0],
                                label=1
                                )
                            qty -= qty_temp*Decimal(0.9)
                break
            if c > 1000:
                break
            
        await asyncio.sleep(10)
        self.starting_btc = 0
        for acc, api in self.api.items():
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data 
            self.starting_btc +=  cur_balance["btc"]["all"]
            print(cur_balance,acc)
        
        self.logger.warning(f"starting btc: {self.starting_btc}")
            
    async def on_orderbook(self, symbol, orderbook):
        try:
            self.ap = Decimal(orderbook["asks"][0][0])
            new_depth = Depth.from_dict(depth=orderbook)
            self.executor.update_depth(symbol, new_depth)

            ref_prc = self.ref_prc_cal.calc(self.executor.get_depth(symbol))
            if self.send_order_queue.empty() and not self.sending and self.send:
                self.send_order_queue.put_nowait(ref_prc)
                
            if self.cancel_order_queue.empty() and not self.canceling:
                self.cancel_order_queue.put_nowait(1)
        
        except:
            traceback.print_exc()

    async def on_order(self, order):
        if self.order_cache.get(order.client_id) is None:
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            o = self.order_cache.get(order.client_id)
            try:
                if order.filled_amount > 0 and o.message["label"] == 0:
                    amt_chg = order.filled_amount if o.side == OrderSide.Buy else - order.filled_amount
                    symbol_config = self.get_symbol_config(o.symbol_id)
                    self.executor.update_finished_order(
                        ".".join((symbol_config["exchange_name"],symbol_config["pair"])), 
                        float(amt_chg)
                    )
                    
                side = o.side
                acc = o.account_name
                if side == OrderSide.Buy:
                    self.acc_available[acc]["usdt"] += (o.requested_amount * o.requested_price - order.filled_amount * order.avg_filled_price)
                    self.acc_available[acc]["btc"] += order.filled_amount
                else:
                    self.acc_available[acc]["btc"] += (o.requested_amount - order.filled_amount)
                    self.acc_available[acc]["usdt"] += order.filled_amount * order.avg_filled_price
                        
                self.order_cache.pop(order.client_id)
            
            except Exception as err:
                traceback.print_exc()
                self.logger.critical(f"hanle order err: {err}")
            
    
    def handle_limit(self,order10s,order1d,ip1m,acc,sn):
        self.order_load_10s[acc] = order10s
        self.order_load_daily[acc] = order1d
        self.ip_load[sn] = ip1m
    
    async def reduce_count(self):
        while True:
            await asyncio.sleep(0.5)
            acc_list = sorted(self.order_load_10s, key=self.order_load_10s.get)
            trader_list = sorted(self.ip_load, key=self.ip_load.get)
            self.order_load_10s[acc_list[-1]] -= 1
            self.ip_load[trader_list[-1]] -= 1
    
    async def get_balance(self):
        while True:
            await asyncio.sleep(30)
            btc_amount = 0
            usdt_amount = 0
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                try:
                    cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
                    btc_amount += cur_balance["btc"]["all"]
                    usdt_amount += cur_balance["usdt"]["all"]
                except Exception as err:
                    self.logger.warning(f"get balance err, {err}")
                    
            usdt_balance = float(btc_amount*self.ap + usdt_amount - self.starting_btc*self.ap)
            if usdt_balance < 0:
                self.logger.warning(f"btc_amount={btc_amount}, usdt_amount={usdt_amount}, ap={self.ap}, starting cash={self.starting_btc}")
            self.loop.create_task(
                self.push_influx_data(
                    measurement="tt", 
                    tag={"sn":"btc_spot"}, 
                    fields={
                        "balance":float(btc_amount*self.ap + usdt_amount),
                        "usdt_balance":usdt_balance
                        }
                    )
                )
    
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(5)
            self.loop.create_task(
                self.push_influx_data(
                    measurement="tt", 
                    tag={"sn":"btc_spot"}, 
                    fields={
                        "inventory":float(self.executor.get_inventory("binance.btc_usdt"))
                        }
                    )
                )
            for trader in self.traders:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":"btc_spot"}, 
                        fields={
                            trader:float(self.ip_load[trader])
                            }
                        )
                    )
    
    # def generate_orders(self, ref_prc):
    #     return self.executor.get_orders(
    #         contract=self.contract,
    #         ref_prc=ref_prc, 
    #         beta_base=BETA_BASE,
    #         t=0,
    #         tau=FREQUENCY,
    #         T=T
    #         )
    
    def generate_orders(self,ref_prc):
        return self.executor.get_orders(
            contract=self.contract,
            ref_prc=ref_prc, 
            beta=BETA_BASE,
            t=0,
            tau=FREQUENCY,
            T_F=T_F,
            floors=[0.001, 0.002, 0.003, 0.004],
            step=0.001
            )
        
    def direction_map(self, side):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
        
    async def send_order_action(self):
        async def send_order_helper(order, acc_list, strategy_name):
            send_block = False
            for oid, o in self.order_cache.items():
                if o.side == self.direction_map(order.side) and not self.order_cache[oid].message["cancel"] and o.message["floor"] == order.floor:
                    if abs(float(o.requested_price) - order.price) / float(o.requested_price) > self.prc_per_diff:
                        self.loop.create_task(self.handle_cancel_order(oid))
                    else:
                        self.order_cache[oid].create_ms = int(time.time()*1e3)
                    send_block = True
            if send_block:
                return
            if order.side == Direction.short:
                price = round_decimals_up(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
                for acc in acc_list:
                    qty_temp = round_decimals_down(float(self.acc_available[acc]["btc"])*0.99, qty_precision[order.iid])
                    if qty_temp > qty:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty),
                                side=OrderSide.Sell,
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                            )
                        qty = 0
                        break
                    else:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty_temp),
                                side=OrderSide.Sell,  
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                            )
                        qty -= qty_temp
                if qty > 0:
                    self.logger.warning(f"sell order unsent with {qty}")
                    available_btc = 0
                    for acc in acc_list:
                        available_btc += self.acc_available[acc]["btc"]
                    self.logger.warning(f"available_btc={available_btc}, at level={order.floor}")
                    for _, o in self.order_cache.items():
                        if o.side == OrderSide.Sell:
                            self.logger.warning(f"p={o.requested_price}, q={o.requested_amount}, side={o.side}, tag={o.message['floor']}, cancel={o.message['cancel']}, sn={strategy_name}, id={o.client_id}")
                    self.logger.warning(f".....................")
                    
            elif order.side == Direction.long:
                price = round_decimals_down(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
                for acc in acc_list:
                    qty_temp = round_decimals_down(float(self.acc_available[acc]["usdt"])/price*0.99, qty_precision[order.iid])
                    if qty_temp > qty:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty),
                                side=OrderSide.Buy,
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                                )
                        qty = 0
                        break
                    else:
                        await self.send_order(
                                price=Decimal(price),
                                qty=Decimal(qty_temp),
                                side=OrderSide.Buy,
                                strategy_name=strategy_name,
                                account_name=acc,
                                floor=order.floor
                                )
                        qty -= qty_temp
                if qty > 0:
                    self.logger.warning(f"buy order unsent with {qty}, amount of {qty*price}, at level={order.floor}")
                    available_usdt = 0
                    for acc in acc_list:
                        available_usdt += self.acc_available[acc]["usdt"]
                    self.logger.warning(f"available_usdt={available_usdt}")
                    for _, o in self.order_cache.items():
                        if o.side == OrderSide.Buy:
                            self.logger.warning(f"p={o.requested_price}, q={o.requested_amount}, side={o.side}, tag={o.message['floor']}, cancel={o.message['cancel']}, sn={strategy_name}, id={o.client_id}")
                    self.logger.warning(f".....................")
                            
        while True:
            ref_prc = await self.send_order_queue.get()
            self.sending = True
            orders = self.generate_orders(ref_prc)
            acc_list = sorted(self.order_load_10s, key=self.order_load_10s.get)
            trader_list = sorted(self.ip_load, key=self.ip_load.get)
            for order in orders:
                self.loop.create_task(send_order_helper(order,acc_list, trader_list[0]))  
            self.sending = False
    
    async def handle_cancel_order(self,oid):
        try:
            if self.order_cache.get(oid):
                if oid not in self.canceling_id:
                    trader_list = sorted(self.ip_load, key=self.ip_load.get)
                    self.canceling_id.add(oid)
                    self.order_cache[oid].message["cancel"] = True
                    await self.publish_to_channel(
                        "mq:hf_signal:cancel_request",
                        {
                            "strategy_name":trader_list[0],
                            "order": self.order_cache[oid].to_json()
                        }
                    )

        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
                
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if self.order_cache.get(payload["client_id"]) and payload["res"] == False:
            self.order_cache[payload["client_id"]].message["cancel"] = False
        self.canceling_id.remove(payload["client_id"])
        
    async def send_order(self,price,qty,side,strategy_name,account_name=None,floor=None,order_type=OrderType.Limit,label=0):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            order_type=order_type,
            position_side=OrderPositionSide.Open,
            account_name=account_name
        )
        extra_info = dict()
        extra_info["cancel"] = False
        extra_info["query"] = 0
        extra_info["life"] = 100
        extra_info["label"] = label
        extra_info["floor"] = floor
        extra_info["sn"] = strategy_name
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        
        if side == OrderSide.Buy:
            self.acc_available[account_name]["usdt"] -= price * qty
        else:
            self.acc_available[account_name]["btc"] -= qty
            
        try:
            await self.publish_to_channel(
                "mq:hf_signal:order_request",
                {
                    "strategy_name":strategy_name,
                    "symbol":self.symbol,
                    "price":price,
                    "qty":qty,
                    "side":1 if side == OrderSide.Buy else -1,
                    "client_id": client_id,
                    "acc": account_name
                }
            )
        except Exception as err:
            self.logger.critical(f"{err}")
            await self.internal_fail(client_id,err)
            
    async def process_order_response(self, ch_name, response):
        payload = json.loads(response)
        if not payload["order10s"]:
            await self.internal_fail(
                client_id=payload["client_id"],
                err=payload["err"],
                acc=payload["account_name"]
                )
            return 
        self.handle_limit(
            order10s=payload["order10s"],
            order1d=payload["order1d"],
            ip1m=payload["ip1m"],
            sn=payload["startegy_name"],
            acc=payload["account_name"],
            )
            
    async def cancel_order_action(self):
        while True:
            await self.cancel_order_queue.get()
            self.canceling = True
            for oid, order in self.order_cache.items():
                if not order.message["cancel"] and order.create_ms < time.time()*1e3 - order.message["life"]:
                    self.loop.create_task(self.handle_cancel_order(oid))
                elif order.message["cancel"] and order.create_ms < time.time()*1e3 - 2000:
                    self.loop.create_task(self.handle_cancel_order(oid))
            self.canceling = False
    
    async def retreat(self):
        for _ in range(3):
            for api in self.api.values():
                try:
                    await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                except Exception as err:
                    self.logger.critical(f"retreat err:{err}")
            await asyncio.sleep(3)
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    cash = 0
                    for acc in self.account_names:
                        cur_balance = (await self.api[acc].account_balance(self.get_symbol_config(self.symbol))).data
                        cash += cur_balance["btc"]["all"] * self.ap + cur_balance["usdt"]["all"]
                    self.logger.critical(f"ending cash: {cash}")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if oid in self.canceling_id:
                return
            if time.time()*1e3 - order.create_ms > 60000:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["query"] += 1
                        if self.order_cache[oid].message["query"]>10:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                        else:
                            self.loop.create_task(self.handle_cancel_order(oid))

                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(30)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )
            
    async def debug(self):
        while True:
            await asyncio.sleep(10)
            for oid, o in self.order_cache.items():
                if o.message["cancel"] is True and time.time()*1e3 - o.create_ms> 1000:
                    self.logger.warning(f"expired order: p={o.requested_price}, q={o.requested_amount}, side={o.side}, acc={o.account_name}, sn={o.message['sn']}, id={oid}, time={time.time()*1e3 - o.create_ms}")
            self.logger.warning(".................")
                    
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.update_redis_cache(),
            self.reduce_count(),
            self.get_balance(),
            self.get_local_stats(),
            self.debug(),
            )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
        