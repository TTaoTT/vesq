import asyncio
from decimal import Decimal
import time 
import pandas as pd
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import pandas as pd
from typing import List, Union
from datetime import datetime, timedelta, timezone, tzinfo
import os


class DataSource(object):

    def iter_data(self, begin: int, end: int):
        raise NotImplementedError()

    def prepare(self, begin: int, end: int, group_size: int):
        pass


class SingleCSVDataSource(DataSource):

    def __init__(self, path: str, tzinfo: timezone=timezone(timedelta(0))) -> None:
        super().__init__()
        self.path = path
        self.tzinfo = tzinfo
        self.data: pd.DataFrame = None

    def iter_data(self, begin: int, end: int):
        data = self.data.loc[begin:end]
        data["timestamp"] = data.index
        return data.to_dict("record")
    
    def prepare(self, begin: int, end: int, group_size: int):
        
        self.data = pd.read_csv(
            self.path, index_col="ts"
        ).rename(lambda t: datetime.fromisoformat(t).replace(tzinfo=self.tzinfo).timestamp()*1000).loc[begin:end]


class HDFDataSource(DataSource):

    def __init__(self, path: str, keys: List[str], columns: List[str]=None, lable: str="default") -> None:
        super().__init__()
        self.path = path
        self.table = pd.HDFStore(path, mode="r")
        self.keys = keys
        self.lable = lable
        self.columns = columns
    
    def close(self):
        self.table.close()

    @staticmethod
    def bi_search_index(table, value, side="right"):
        begin = 0
        end = table.nrows
        while(begin+1<end):
            mid = int((begin+end)/2)
            _v = table[mid][0]
            if _v < value:
                begin = mid
            elif _v > value:
                end = mid
            else:
                return mid
        if side == "left":
            
            return begin
        else:
            return end

    @staticmethod
    def search_sorted(store:pd.HDFStore, key: str, columns: Union[List[str], None], start, end):
        table = store.get_storer(key).table
        s_index = HDFDataSource.bi_search_index(table, start, 'right')
        e_index = HDFDataSource.bi_search_index(table, end, 'right')
        return store.select(key, start=s_index, stop=e_index, columns=columns)

    def iter_data(self, begin: int, end: int):
        for key in self.keys:
            data = self.search_sorted(self.table, key, self.columns, begin, end)
            data["timestamp"] = data.index
            data['key'] = key
            data["lable"] = self.lable
            yield from data.to_dict("record")

    def prepare(self, begin: int, end: int, group_size: int):
        pass


class TimeSliceHDFDataSource(DataSource):

    DAY_MIL_SECS = 24*3600*1000

    def __init__(self, root: str, keys: List[str], columns: List[str]=None, lable: str="default") -> None:
        super().__init__()
        self.root = root 
        self.keys = keys
        self.lable = lable
        self.columns = columns
        self.current_tag = ""
        self.ds: Union[HDFDataSource, None] = None
    
    def iter_data(self, begin: int, end: int):
        next_begin = begin - begin % self.DAY_MIL_SECS + self.DAY_MIL_SECS
        while end > next_begin:
            tag = self.tag_name(begin - begin % self.DAY_MIL_SECS)
            self.switch_file(tag)
            yield from self.ds.iter_data(begin, next_begin)
            begin = next_begin
            next_begin = begin - begin % self.DAY_MIL_SECS + self.DAY_MIL_SECS

        tag = self.tag_name(begin - begin % self.DAY_MIL_SECS)
        self.switch_file(tag)
        yield from self.ds.iter_data(begin, end)

    def prepare(self, begin: int, end: int, group_size: int):
        tag = self.tag_name(begin - begin % self.DAY_MIL_SECS)
        self.switch_file(tag)

    def switch_file(self, tag: str):
        if tag != self.current_tag:
            if isinstance(self.ds, HDFDataSource):
                self.ds.close()
            filename = os.path.join(self.root, f"{tag}.hdf")
            self.ds = HDFDataSource(
                filename,
                self.keys,
                self.columns,
                self.lable
            )

    @staticmethod
    def tag_name(ts: int):
        return datetime.fromtimestamp(ts/1000, timezone(timedelta(hours=0))).strftime("%Y%m%d")
    
    def write(self, key: str, data: pd.DataFrame):
        if not os.path.isdir(self.root):
            os.makedirs(self.root, exist_ok=True)
        for tag, frame in data.groupby(self.tag_name):
            filename = os.path.join(self.root, f"{tag}.hdf")
            store = pd.HDFStore(filename, mode="a")
            store.append(key, frame)
            store.close()

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.eth_usdt_swap.usdt_contract.na"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_ordr_queue = Queue()
        self.cancel_order_queue = Queue()
        self.bbo_ts = None
        self.cancel_trigger_ts = 0
        
        self.orders = []
    
    async def before_strategy_start(self):
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        await asyncio.sleep(5)
        # self.direct_subscribe_orderbook(symbol_name=self.symbol)
        self.use_raw_stream = True
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        
        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        self.half_step_tick = qty_tick/Decimal(2)
        self.half_price_tick = price_tick/Decimal(2)
        
    # async def on_orderbook(self, symbol, orderbook):
    #     ap = orderbook["asks"][0][0]
    #     bp = orderbook["bids"][0][0]
    #     self.mp = (ap + bp) / Decimal(2)
    #     if self.bbo_ts and orderbook["server_ts"] % 10000 < self.bbo_ts:
    #         self.send_ordr_queue.put_nowait(orderbook["server_ts"])
    #     self.bbo_ts = orderbook["server_ts"] % 10000
    
    async def on_bbo(self, symbol, bbo):
        ap = Decimal(bbo["data"]["a"])
        bp = Decimal(bbo["data"]["b"])
        self.mp = (ap + bp) / Decimal(2)
        if self.bbo_ts and bbo["data"]["T"] % 1000 < self.bbo_ts:
            self.send_ordr_queue.put_nowait([bbo["data"]["T"],bbo["data"]["E"],ap,bp])
        self.bbo_ts = bbo["data"]["T"] % 1000
        
        if bbo["data"]["T"] - self.cancel_trigger_ts > 10:
            self.cancel_order_queue.put_nowait(1)
            self.cancel_trigger_ts = bbo["data"]["T"]

    async def on_order(self, order):
        if order.xchg_status == OrderStatus.New:
            
            self.orders.append(
                {
                    "ts":self.order_cache[order.client_id].create_ms,
                    "delay":order.raw_data["T"] - self.order_cache[order.client_id].create_ms,
                    "action":"send"
                }
            )
        if order.xchg_status in OrderStatus.fin_status():
            if order.xchg_status == OrderStatus.Canceled and order.raw_data:
                self.orders.append(
                {
                    "ts":self.order_cache[order.client_id].message["cancel_trigger_time"],
                    "delay":order.raw_data["T"] - self.order_cache[order.client_id].message["cancel_trigger_time"],
                    "action":"cancel"
                }
            )
            self.order_cache.pop(order.client_id)

    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.get(client_id) == None:
                return
            o = self.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order(o)
        except:
            pass
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=3000):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
    
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = 1
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        extra_info["cancel_trigger_time"] = int(time.time()*1e3)
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        try:
            await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                recvWindow=recvWindow,
                client_id=client_id
            )
        except Exception as err:
            await self.internal_fail(client_id,err)
        
    async def send_order_action(self):
        while True:
            await self.send_ordr_queue.get()
            self.loop.create_task(self.send_order(self.mp*Decimal(1.002),Decimal(0.005),OrderSide.Sell))
    
    async def batch_cancel(self, oid, order):
        try:
            if self.order_cache.get(oid) == None:
                return
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                if self.order_cache[oid].message["cancel"] == 0:
                    self.order_cache[oid].message["cancel_trigger_time"] = int(time.time()*1e3)
                await self.cancel_order(order)
                self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):
        async def batch_cancel(oid, order):
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3*0.3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
        while True:
            await self.cancel_order_queue.get()
            for oid, order in self.order_cache.items():
                self.loop.create_task(batch_cancel(oid, order))
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if not order.message:
                # self.logger.warning(f"order has no message while try to check,{oid}")
                return
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*6 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["query"] += 1
                        if self.order_cache[oid].message["query"]>10:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                        else:
                            self.loop.create_task(self.batch_cancel(oid,order_new))
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )
    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(self.mp*Decimal(0.99), abs(pos),OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)
        
    async def store_data(self):
        self.store_ts = 0
        while True:
            await asyncio.sleep(1)
            t = time.time()*1e3
            if t % (24*60*60*1000) < self.store_ts:
                data = pd.DataFrame(self.orders)
                # ds = TimeSliceHDFDataSource("./data", keys="dummy")
                # ds.write("dummy", data.set_index("localtime"))
                data.to_csv(f"./data/{self.symbol}_{int(t)}_end_point_delay.csv")
                self.orders = []
                self.logger.warning(">>>>>>")
            self.store_ts = t % (24*60*60*1000)
            
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

                
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.gather(
                self.send_order_action(),
                self.cancel_order_action(),
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.store_data()
                )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    
            
            