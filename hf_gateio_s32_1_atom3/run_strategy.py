import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue

from atom.helpers import json, safe_decimal, ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy


class MpOutput:
    def __init__(self, base_line):
        self.base_std = base_line
        self.mp_queue = collections.deque(maxlen=200)
        self.qt_queue = collections.deque(maxlen=10000)

        self.pre_max_list = collections.deque(maxlen=200)
        self.pre_min_list = collections.deque(maxlen=200)

    def feed_mp(self, mp):
        self.mp_queue.append(float(mp))
        self.update_base_line()

    def get_x_cur_duration(self):
        if len(self.pre_min_list) == 200:
            mp_mean = np.mean(list(self.mp_queue)[-50:])
            if mp_mean > self.pre_max_list[0]:
                return 1
            if mp_mean < self.pre_min_list[0]:
                return -1
        return 0

    def feed_max_logic(self):
        p_max = np.max(list(self.mp_queue))
        p_min = np.min(list(self.mp_queue))
        self.pre_max_list.append(p_max)
        self.pre_min_list.append(p_min)

    def update_base_line(self):
        if len(self.mp_queue) == 200:
            cur_std = self.get_cur_std()
            self.qt_queue.append(cur_std)
            if len(self.qt_queue) == 10000:
                cur_82_std = np.quantile(list(self.qt_queue), 0.82)
                self.base_std = cur_82_std
                self.qt_queue.clear()
            self.feed_max_logic()

    def get_cur_std(self):
        return np.std(list(self.mp_queue)[-100:])

    def mp_bad(self):
        return self.get_cur_std() > self.base_std

    def get_mean_mp(self):
        return np.mean(list(self.mp_queue)[-50:])

    def get_cur_duration(self):
        return self.mp_queue[-1] - self.get_mean_mp()

class AdjustMovement:
    def __init__(self, mt_line):
        self.buy_ban_mem_ts = None
        self.sell_ban_mem_ts = None
        self.mt_line = mt_line

        self.mp_cache = collections.deque(maxlen=5)
        self.mp_dif = collections.deque(maxlen=5)

        self.sp_update_mem = collections.deque(maxlen=10000)

    def feed_mp(self, cur_mp, cur_dt_ts):
        if len(self.mp_cache) == 5:
            mp_mean = np.mean(self.mp_cache)
            self.mp_dif.append(mp_mean)
        self.mp_cache.append(cur_mp)
        self.count_ban_signal(cur_dt_ts)

    def update_dif(self):
        if len(self.sp_update_mem) == 10000:
            new_line = np.quantile(list(self.sp_update_mem), 0.98)
            # print(f"update mmt new line: {self.mt_line} -> {new_line}")
            self.mt_line = new_line
            self.sp_update_mem.clear()

    def count_ban_signal(self, dt_ts):
        if len(self.mp_dif) == 5:
            cur_dif = self.mp_dif[-1] / self.mp_dif[0] - 1
            self.sp_update_mem.append(float(abs(cur_dif)))
            if cur_dif > self.mt_line:  # 动量阈值的影响非常大
                self.buy_ban_mem_ts = dt_ts

            if cur_dif < -1 * self.mt_line:
                self.sell_ban_mem_ts = dt_ts

            if self.buy_ban_mem_ts is not None:
                if dt_ts - self.buy_ban_mem_ts > 15e3:  # 时间的影响不大
                    self.buy_ban_mem_ts = None

            if self.sell_ban_mem_ts is not None:
                if dt_ts - self.sell_ban_mem_ts > 15e3:
                    self.sell_ban_mem_ts = None

            self.update_dif()

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # init params
        # use self.params
        self.trade_cache = list()
        self.trade_cache_buy_4s = list()
        self.trade_cache_sell_4s = list()
        self.trade_cache_buy_4s_large = list()
        self.trade_cache_sell_4s_large = list()

        self.trade_cache_buy_1s = list()
        self.trade_cache_sell_1s = list()
        self.trade_cache_buy_1s_large = list()
        self.trade_cache_sell_1s_large = list()
        self.missing_order_cache = list()
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.long_pos = Decimal(0)
        self.short_pos = Decimal(0)
        self.mp = None
        self.spread = Decimal(0)
        self.ap = Decimal(0)
        self.bp = Decimal(0)
        self.trade_buy = 0
        self.trade_sell = 0
        self.heartbeat_order_enabled = False
        self.unknown_order_dict = {}
        self.max_balance = 0
        self.last_trade_ts = 0
        self.ob_update = False
        self.send_order = True
        self.order_storage_enabled = False
        self.last_tick = 0
        self.d_count = 0
        self.long_close_lock = Decimal(0)
        self.short_close_lock = Decimal(0)
        self.qty_switching = False
        self.recent_balance = collections.deque(maxlen=10)
        self.pos_price = Decimal(0)

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        self.movement_adjust = AdjustMovement(mt_line=self.params["mt_line"])
        self.mp_std_handle = MpOutput(base_line=float(self.params["std_line"]))
        # self.symbol_1 = self.config['strategy']['symbol_1']
        symbol = self.params["symbol"]
        self.symbol_1 = f"gateio.{symbol}_usdt_swap.usdt_swap.na"

        self._params =self.params["model_params"]
        self.base_amount = self.params["base_amount"]

        self.symbol_trade = f"gateio.{symbol}_usdt_swap.usdt_swap.na"
        self.symbol_data = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        
        if symbol == "shib":
            self.symbol_data = "binance.1000shib_usdt_swap.usdt_contract.na"

        self.t_1 = Decimal(0)
        self.t_4 = Decimal(0)

        self.filled_order = {
            "buy_tot":0,
            "buy_amount":0,
            "sell_tot":0,
            "sell_amount":0,
            "amount_usdt":collections.deque(maxlen=24),
            "amount_unit":collections.deque(maxlen=24),
            "buy_sell_diff":collections.deque(maxlen=24),
            "indicator":collections.deque(maxlen=24)
        }

        self.strategy_start_time = int(time.time()*1e3)
        
        self._order_to_store = Queue()
        # self.check_failed_order = Queue()

        self.tf = False
        self.tf_id = None
        
        
    async def calculate_stats(self):
        config = self.get_symbol_config(self.symbol_1)
        ts = float(config["price_tick_size"]*2)
        i = 0
        while ts % 10<1:
            ts = ts *10
            i += 1

        while True:
            await asyncio.sleep(1)
            if not self.qty_switching:
                await asyncio.sleep(1800)
                try:
                    buy_avg_price = self.filled_order["buy_tot"]/self.filled_order["buy_amount"] if self.filled_order["buy_amount"] else None
                    sell_avg_price = self.filled_order["sell_tot"] / self.filled_order["sell_amount"] if self.filled_order["sell_amount"] else None
                    price_gap = round(
                            (sell_avg_price-buy_avg_price)/(sell_avg_price+buy_avg_price)*2,5
                        ) if buy_avg_price and sell_avg_price else None
                    amount_usdt = round((self.filled_order["buy_tot"]+self.filled_order["sell_tot"])*float(self.cv),2)
                    amount_unit = round((self.filled_order["buy_amount"]+self.filled_order["sell_amount"])/float(self.params["base_amount"]),2)

                    self.filled_order["amount_usdt"].append(amount_usdt)
                    self.filled_order["amount_unit"].append(amount_unit)
                    self.filled_order["buy_sell_diff"].append(price_gap)
                    self.filled_order["indicator"].append(round(price_gap*amount_unit,5) if price_gap else None)
                    self.logger.warning(f"""
                    ===================================================             
                    amount_usdt,{[x for x in self.filled_order["amount_usdt"]]}
                    amount_unit, {[x for x in self.filled_order["amount_unit"]]}
                    sell buy gap, {[x for x in self.filled_order["buy_sell_diff"]]}
                    ===================================================
                    """)
                    self.filled_order["buy_tot"] = 0
                    self.filled_order["sell_tot"] = 0
                    self.filled_order["buy_amount"] = 0
                    self.filled_order["sell_amount"] = 0

                    if price_gap is not None:
                        if amount_unit > float(self.params["amount_min"]) and price_gap>float(self.params["price_gap_min"]):
                            if self.params["base_amount"] == self.base_amount:
                                self.qty_switching = True
                        else:
                            if self.params["base_amount"] > self.base_amount:
                                self.qty_switching = True
                    elif self.params["base_amount"] > self.base_amount:
                        self.qty_switching = True

                except Exception as err:
                    self.logger.warning(f"stats calculation error, {err}")

    async def qty_switching_process(self):
        while True:
            await asyncio.sleep(0.01)
            try:
                if self.qty_switching:
                    if (abs(self.pos)<self.params["base_amount"]*Decimal(10) or self.params["base_amount"] == self.base_amount) and len(self.filled_order["amount_unit"]):
                        self.logger.warning(f"switching qty now")
                        if not self.filled_order["amount_unit"][-1] or not self.filled_order["buy_sell_diff"][-1]:
                            self.params["base_amount"] = self.base_amount
                        elif self.filled_order["amount_unit"][-1]> float(self.params["amount_min"]) and self.filled_order["buy_sell_diff"][-1]>float(self.params["price_gap_min"]):
                            self.params["base_amount"] = self.base_amount * self.params["qty_amp"]
                        else:
                            self.params["base_amount"] = self.base_amount
                        await self.redis_set_cache({"base_amount": self.params["base_amount"],})
                        self.qty_switching = False
                        temp = self.params["base_amount"]
                        self.logger.warning(f"base amount:{temp}")
            except Exception as err:
                self.logger.warning(f"qty switching error, {err}")

    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        self.direct_subscribe_orderbook(symbol_name=self.symbol_1)
        self.direct_subscribe_public_trade(symbol_name=self.symbol_data)
        self.direct_subscribe_order_update(symbol_name=self.symbol_trade)
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()
        self.loop.create_task(self.store_order())
        
        api = self.get_exchange_api("gateio")[0]
        position = await api.contract_position(self.get_symbol_config(self.symbol_1))
        res = position.data
        self.long_pos = res["long_qty"]
        self.short_pos = res["short_qty"]
        self.pos = self.long_pos - self.short_pos
        
        symbol_cfg = self.get_symbol_config(self.symbol_1)
        self.cv = symbol_cfg['contract_value']
        
    async def excute_order(self,params):
        client_id = ClientIDGenerator.gen_client_id("gateio")
        if not self.volume_notional_check(*params[:3]):
            return
        self.order_cache[client_id] = self.gen_async_order(
            client_id=client_id,
            symbol_name=params[0],
            price=params[1],
            volume=params[2],
            side=params[3],
            position_side=params[4],
            order_type=params[5]
        )
        extra_info = dict()
        extra_info["stop_ts"] = params[6]
        extra_info["cancel"] = 0
        extra_info["source"] =  params[7] # entry or balance, 0 for close
        extra_info["query"] =  0
        self.order_cache[client_id].message = extra_info
        
        if params[3] == OrderSide.Buy and params[4] == OrderPositionSide.Close:
            self.short_close_lock += params[2]
        elif params[3] == OrderSide.Sell and params[4] == OrderPositionSide.Close:
            self.long_close_lock += params[2]

        try:
            await self.make_order(
                    symbol_name=params[0],
                    price=params[1],
                    volume=params[2],
                    side=params[3],
                    position_side=params[4],
                    order_type=params[5],
                    client_id=client_id
                )
        except PostOnlyOrderFail as err:
            if self.order_cache.get(client_id) is not None:
                o = self.order_cache[client_id]
                o.xchg_status = OrderStatus.Failed
                await self.on_order(o)
        except Exception as err:
            self.logger.warning(f"send order error:{err}")
            
    # async def check_failed(self):
    #     while True:
    #         order = await self.check_failed_order.get()
    #         try:
    #             api = self.get_exchange_api("gateio")[0]
    #             await api.order_match_result(self.get_symbol_config(order.symbol_id),order.client_id)
    #         except OrderNotFoundError:
    #             if self.order_cache.get(oid) is not None:
    #                 o = self.order_cache[oid]
    #                 o.xchg_status = OrderStatus.Failed
    #                 await self.on_order(o)
    #         except Exception as err:
    #             self.logger.error(f"direct check order error: {err}")
                   
    async def on_orderbook(self, symbol, orderbook):
        self.ap = orderbook["asks"][0][0]
        self.bp = orderbook["bids"][0][0]
        self.mp = (self.ap + self.bp)/Decimal(2)
        self.spread = self.ap - self.bp
        self.movement_adjust.feed_mp(cur_mp=float(self.mp), cur_dt_ts=time.time()*1e3)
        self.mp_std_handle.feed_mp(mp=self.mp)
        self.ob_update = True

    async def on_public_trade(self, symbol, trade: PublicTrade):
        self.trade_cache.append({"ts":trade.server_ms, "q":trade.quantity, "s":trade.side})
        self.last_trade_ts = trade.server_ms
        if trade.side == OrderSide.Buy:
            self.trade_cache_buy_4s.append(float(trade.price))
            self.trade_cache_buy_1s.append(float(trade.price))
        else:
            self.trade_cache_sell_4s.append(float(trade.price))
            self.trade_cache_sell_1s.append(float(trade.price))
    
    async def calculate_t_4(self):
        while True:
            await asyncio.sleep(4)

            if self.trade_cache_buy_4s and self.trade_cache_buy_4s[0]!=0:
                buy_imp = (max(self.trade_cache_buy_4s) - self.trade_cache_buy_4s[0]) / self.trade_cache_buy_4s[0]
            else:
                buy_imp = 0

            if self.trade_cache_sell_4s and self.trade_cache_sell_4s[0]!=0:
                sell_imp = (self.trade_cache_sell_4s[0] - min(self.trade_cache_sell_4s)) / self.trade_cache_sell_4s[0]
            else:
                sell_imp = 0

            self.trade_cache_buy_4s = []
            self.trade_cache_sell_4s = []

            self.trade_cache_buy_4s_large.append(buy_imp)
            self.trade_cache_sell_4s_large.append(sell_imp)
            if len(self.trade_cache_buy_4s_large) >= 201:
                self.trade_cache_buy_4s_large.pop(0)
            if len(self.trade_cache_sell_4s_large) >= 201:
                self.trade_cache_sell_4s_large.pop(0)

            t_buy = np.quantile(self.trade_cache_buy_4s_large, 0.98)
            t_sell = np.quantile(self.trade_cache_sell_4s_large, 0.98)

            self.t_4 = Decimal(max(t_buy,t_sell))

    async def calculate_t_1(self):
        while True:
            await asyncio.sleep(1)

            if self.trade_cache_buy_1s and self.trade_cache_buy_1s[0]!=0:
                buy_imp = (max(self.trade_cache_buy_1s) - self.trade_cache_buy_1s[0]) / self.trade_cache_buy_1s[0]
            else:
                buy_imp = 0

            if self.trade_cache_sell_1s and self.trade_cache_sell_1s[0]!=0:
                sell_imp = (self.trade_cache_sell_1s[0] - min(self.trade_cache_sell_1s)) / self.trade_cache_sell_1s[0]
            else:
                sell_imp = 0

            self.trade_cache_buy_1s = []
            self.trade_cache_sell_1s = []

            self.trade_cache_buy_1s_large.append(buy_imp)
            self.trade_cache_sell_1s_large.append(sell_imp)
            if len(self.trade_cache_buy_1s_large) >= 201:
                self.trade_cache_buy_1s_large.pop(0)
            if len(self.trade_cache_sell_1s_large) >= 201:
                self.trade_cache_sell_1s_large.pop(0)

            t_buy = np.quantile(self.trade_cache_buy_1s_large, 0.95)
            t_sell = np.quantile(self.trade_cache_sell_1s_large, 0.95)

            self.t_1 = Decimal(max(t_buy,t_sell))
        
        
    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[xchg_id].filled_amount
        if not amount_changed:
            return
        
        if not self.qty_switching:
            if self.order_cache[xchg_id].side == OrderSide.Buy:
                self.filled_order["buy_tot"] += float(amount_changed * p_order.avg_filled_price)
                self.filled_order["buy_amount"] += float(amount_changed)
            
            if self.order_cache[xchg_id].side == OrderSide.Sell:
                self.filled_order["sell_tot"] += float(amount_changed * p_order.avg_filled_price)
                self.filled_order["sell_amount"] += float(amount_changed)
        
        if (self.order_cache[xchg_id].side == OrderSide.Buy and self.order_cache[
            xchg_id].position_side == OrderPositionSide.Open) or \
                (self.order_cache[xchg_id].side == OrderSide.Sell and self.order_cache[
                    xchg_id].position_side == OrderPositionSide.Close):
            self.long_pos += Decimal((self.order_cache[xchg_id].side == OrderSide.Buy) * 2 - 1) * amount_changed
        else:
            self.short_pos += Decimal((self.order_cache[xchg_id].side == OrderSide.Sell) * 2 - 1) * amount_changed

        if self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            self.pos_price = (self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (self.pos + amount_changed)
            self.pos += amount_changed
        elif self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            if self.pos < amount_changed:
                self.pos_price = p_order.avg_filled_price
            self.pos -= amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            if self.pos + amount_changed > Decimal(0):
                self.pos_price = p_order.avg_filled_price
            self.pos += amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            self.pos_price = (-self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (-self.pos + amount_changed)
            self.pos -= amount_changed
            
    def handle_lock(self, xchg_id):
        if self.order_cache[xchg_id].side == OrderSide.Buy and self.order_cache[
            xchg_id].position_side == OrderPositionSide.Close:
            self.short_close_lock -= self.order_cache[xchg_id].requested_amount
        elif self.order_cache[xchg_id].side == OrderSide.Sell and self.order_cache[
            xchg_id].position_side == OrderPositionSide.Close:
            self.long_close_lock -= self.order_cache[xchg_id].requested_amount

    async def on_order(self, order: PartialOrder):
        oid = order.client_id
        if oid not in list(self.order_cache): 
            return
        
        if order.filled_amount < self.order_cache[oid].filled_amount:
            return

        self.handle_pos(oid,order)
        self.order_cache[oid].filled_amount = order.filled_amount
        self.order_cache[oid].avg_filled_price = order.avg_filled_price
        self.order_cache[oid].xchg_id = order.xchg_id
        self.order_cache[oid].xchg_status = order.xchg_status
        extra_info = self.order_cache[oid].message
        if extra_info['source'] == -1:
            preflix = "balance"
        elif extra_info['source'] == 1:
            preflix = "entry"
        else:
            preflix = "exit"
        self.order_cache[oid].extra_info = f"{preflix}_{extra_info['stop_ts']}"
        tag_info = int(self.params["base_amount"]/self.base_amount)
        self.order_cache[oid].tag = f"{tag_info}"

        if order.xchg_status in OrderStatus.fin_status():
            self.handle_lock(oid)
            if order.filled_amount > 0:
                o = self.order_cache[oid]
                o.message = ""
                self._order_to_store.put_nowait(o)
            self.order_cache.pop(oid)
            if self.tf_id == oid:
                self.tf = False
            
    async def store_order(self):
        while True:
            order = await self._order_to_store.get()
            await self.post_order_to_system(order, self.session)

    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.update_redis_cache(),
            self.check_trade_flow(),
            self.check_drawback(),
            self.calculate_t_1(),
            self.calculate_t_4(),
            self.calculate_stats(),
            self.qty_switching_process(),
            self.check_position(),
            self.exit_action()
        )

    def get_place_amount(self, net_loc, base_amount, level=5):
        cur_risk = abs(self.pos)
        v_cum = 0
        i_net = 0
        while True:
            i_net += 1
            cur_net_volume = sum([i_net * base_amount * (i+1) for i in range(4)])
            v_cum += cur_net_volume
            if v_cum > cur_risk:
                break
        if i_net >= level:
            i_net = level
        return [i_net * base_amount * (i+1) for i in range(4)][net_loc - 1]

    def get_entry_amount(self, net_loc, base_amount,side,level=2):
        if self.pos > Decimal(10)*self.params["base_amount"] and self.pos_price > self.mp*(self.t_4+Decimal(1)) and side == 1:
            return [level * base_amount * (i+1) for i in range(4)][net_loc - 1]
        elif self.pos < -Decimal(10)*self.params["base_amount"] and self.pos_price < self.mp*(Decimal(1)-self.t_4) and side == 2:
            return [level * base_amount * (i+1) for i in range(4)][net_loc - 1]
        else:
            return [base_amount * (i+1) for i in range(4)][net_loc - 1]

    def get_order_position(self,balance=False):
        
        ask_ceiling = Decimal(round(self._params["up_c_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up_c_int"],4))
        ask_floor = Decimal(round(self._params["up_f_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up_f_int"],4))
        
        bid_ceiling = Decimal(round(self._params["down_c_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down_c_int"],4))
        bid_floor = Decimal(round(self._params["down_f_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down_f_int"],4))
        
        if balance:
            ask_floor = Decimal(0)
            bid_floor = Decimal(0)
        else:
            if ask_floor < self.t_1:
                ask_floor = self.t_1
            if bid_floor < self.t_1:
                bid_floor = self.t_1
            if self.t_4 < self.t_1*Decimal(4):
                self.t_4 = self.t_1*Decimal(4)
            if ask_ceiling < self.t_4:
                ask_ceiling = self.t_4
            if bid_ceiling < self.t_4:
                bid_ceiling = self.t_4
            

        sell_spread_tick = round((ask_ceiling - ask_floor) / 3, 5)
        buy_spread_tick = round((bid_ceiling - bid_floor) / 3, 5)

        if sell_spread_tick < 0:
            sell_spread_tick = Decimal(0)
        if buy_spread_tick < 0:
            buy_spread_tick = Decimal(0)

        return ask_floor, bid_floor, sell_spread_tick, buy_spread_tick

    def generate_entry_orders(self):
        if abs(self.pos)>Decimal(100)*self.params["base_amount"] or not self.mp or not self.ob_update or self.qty_switching or self.spread/self.mp>self.params["spread_min"]:
            return []
        
        entry_orders = []
        mp = self.mp
        
        ask_floor, bid_floor, sell_spread_tick, buy_spread_tick = self.get_order_position()

        for i in range(1, 5):
            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor

            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))

                elif cd < 0:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            if self.spread/self.mp > self.t_1:
                sell_p = self.ap * (Decimal(1) + Decimal(sell_spread))
                buy_p = self.bp * (Decimal(1) - Decimal(buy_spread))

            s_amount = self.get_entry_amount(net_loc=i, base_amount=self.params["base_amount"],side=2, level=2)
            b_amount = self.get_entry_amount(net_loc=i, base_amount=self.params["base_amount"],side=1, level=2)

            b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,i,1]
            s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,i,1]

            entry_orders.append(b_io)
            entry_orders.append(s_io)

        self.ob_update = False

        return entry_orders

    def generate_balance_orders(self):
        if not self.mp:
            return []
        cur_risk = self.pos

        balance_orders = []
        mp = self.mp
        total_amount = Decimal(abs(cur_risk))
        
        ask_floor, bid_floor, sell_spread_tick, buy_spread_tick = self.get_order_position(balance=True)

        for i in range(4, 0, -1):

            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor
        
            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))
                else:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            ex_amount = self.get_place_amount(net_loc=5-i,base_amount=self.params["base_amount"])
            if cur_risk > 0:

                if self.movement_adjust.sell_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                if sell_p < self.pos_price:
                    ex_amount = self.params["base_amount"]
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, sell_p, ex_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, sell_p, total_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
            else:
                if self.movement_adjust.buy_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                if buy_p > self.pos_price:
                    ex_amount = self.params["base_amount"]
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, buy_p, ex_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, buy_p, total_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
        return balance_orders
        
    def order_generate_logic(self):
        
        req_orders = []

        entry_orders = self.generate_entry_orders()
        req_orders.extend(entry_orders)

        balance_orders = self.generate_balance_orders()
        req_orders.extend(balance_orders)

        filter_orders = self.request_orders_filter(req_orders)
        built_orders = self.build_oc(filter_orders)

        return built_orders
    
    def build_oc(self, req_orders):
        orders = []
        short_close_lock_cache = Decimal(0)
        long_close_lock_cache = Decimal(0)
        for order in req_orders:
            if order[3] == OrderSide.Buy:
                if self.short_pos - self.short_close_lock - short_close_lock_cache > order[2]:
                    order[4] = OrderPositionSide.Close
                    orders.append(order)
                    short_close_lock_cache += order[2]
                else:
                    orders.append(order)
            else:
                if self.long_pos - self.long_close_lock - long_close_lock_cache > order[2]:
                    order[4] = OrderPositionSide.Close
                    orders.append(order)
                    long_close_lock_cache += order[2]
                else:
                    orders.append(order)
        return orders

    def request_orders_filter(self,req_orders):
        filter_orders = []
        buy_source = [self.order_cache[key].message["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Buy and self.order_cache[key].message]
        sell_source = [self.order_cache[key].message["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Sell and self.order_cache[key].message]

        for per_req in req_orders:
            if per_req[3] == OrderSide.Buy:
                x_source = np.array(buy_source)
            else:
                x_source = np.array(sell_source)
            x_source = np.array(x_source)
            cur_pending_len = sum((x_source == per_req[7]) * 1)
            if cur_pending_len < 1:
                filter_orders.append(per_req)
        return filter_orders

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def send_order_action(self):
        while True:
            await asyncio.sleep(0.005)
            if self.send_order:
                orders = self.order_generate_logic()
                asyncio.gather(*[self.excute_order(params) for params in orders])

    async def check_position(self):
        while True:
            await asyncio.sleep(1)
            try:
                api = self.get_exchange_api("gateio")[0]
                position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                res = position.data
                if res is not None:
                    if res["long_qty"] != self.long_pos or\
                    res["short_qty"] != self.short_pos or\
                    res["long_qty"]-res["short_qty"] != self.pos:
                        self.logger.warning(
                        f"""
                        =========update wrong position==========
                        origin: long{self.long_pos}, short{self.short_pos}
                        update: long{res["long_qty"]}, short{res["short_qty"]}
                        """
                    )
                    self.long_pos = res["long_qty"]
                    self.short_pos = res["short_qty"]
                    self.pos = self.long_pos - self.short_pos
                
            except Exception as err:
                self.logger.warning(f'check position err {err}')
    
      
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(order.symbol_id),order.xchg_id,client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*0.5 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].message["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order(order_new)
                    elif self.order_cache[oid].message["query"]>10:
                        o = self.order_cache[oid]
                        o.xchg_status = OrderStatus.Failed
                        await self.on_order(o)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send_order = False
                    for i in range(3):
                        api = self.get_exchange_api("gateio")[0]
                        await api.flash_cancel_orders(self.get_symbol_config(self.symbol_1))
                        position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                        res = position.data
                        if res is not None:
                            pos = res["long_qty"] - res["short_qty"]
                            if pos > 0:
                                await self.excute_order([self.symbol_1, self.mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,1,0])
                            else:
                                await self.excute_order([self.symbol_1, self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,1,0])
                        
                        await asyncio.sleep(3)
                    await self.redis_set_cache(
                        {
                        "exit":None, 
                        "current position/base_amount":0, 
                        "max balance": 0,
                        "current position": 0, 
                        "long position": 0, 
                        "short position": 0, 
                        }
                        )
                    
                    self.logger.warning(f"manually exiting")
                    exit()
                        
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        async def update_cache():
            _dict = dict()
            _dict = {
                "exit": None,
                "current position/base_amount": self.pos/self.params["base_amount"],
                "max balance": self.max_balance,
                "current position": self.pos,
                "long position": self.long_pos,
                "short position": self.short_pos,
                "long_lock": self.long_close_lock,
                "short_lock": self.short_close_lock,
                "t_1":self.t_1,
                "t_4":self.t_4,
                "12h_vol_usdt":sum(self.filled_order["amount_usdt"]),
                "base_amount": self.params["base_amount"],
                "mt":self.movement_adjust.mt_line,
                "std":self.mp_std_handle.base_std,
                "pos_price":self.pos_price
            }
            try:
                await self.redis_set_cache(_dict)
            except Exception as err:
                self.logger.warning(f"set redis cache failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            await update_cache()
    
    async def check_trade_flow(self):

        while True:
            await asyncio.sleep(0.1)
            
            while self.trade_cache and self.trade_cache[0]['ts'] < time.time()*1e3 - 10e3:
                self.trade_cache.pop(0)

            if not self.trade_cache and self.last_trade_ts < time.time()*1e3 - 120e3:
                self.logger.warning("trade data flow missing and lock")
                for i in range(3):
                    api = self.get_exchange_api("gateio")[0]
                    await api.flash_cancel_orders(self.get_symbol_config(self.symbol_1))
                    position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                    res = position.data
                    if res is not None:
                        pos = res["long_qty"] - res["short_qty"]
                        if pos > 0:
                            await self.excute_order([self.symbol_1, self.mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,1,0])
                        else:
                            await self.excute_order([self.symbol_1, self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,1,0])
                    await asyncio.sleep(3)
                await asyncio.sleep(30)

            elif not self.trade_cache:
                self.trade_buy = Decimal(0)
                self.trade_sell = Decimal(0)
            else:
                self.trade_buy = abs(sum([item["q"] for item in self.trade_cache if item["s"]==OrderSide.Buy]))
                self.trade_sell = abs(sum([item["q"] for item in self.trade_cache if item["s"]==OrderSide.Sell]))

    async def check_drawback(self):

        async def clear_position():
            for i in range(3):
                api = self.get_exchange_api("gateio")[0]
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol_1))
                position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                res = position.data
                if res is not None:
                    pos = res["long_qty"] - res["short_qty"]
                    if pos > 0:
                        await self.excute_order([self.symbol_1, self.mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,1,0])
                    else:
                        await self.excute_order([self.symbol_1, self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,1,0])
                await asyncio.sleep(3)
                
        while True:
            await asyncio.sleep(30)
            try:
                api = self.get_exchange_api("gateio")[0]
                cur_balance = (await api.account_balance (self.get_symbol_config(self.symbol_1))).data
                equity = cur_balance.get('usdt')
                
                self.recent_balance.append(equity["all"])
                self.max_balance = max(self.max_balance, np.median(self.recent_balance))
                if np.median(self.recent_balance) < self.max_balance*Decimal(0.9):
                    t = time.time() - self.last_tick
                    self.send_order = False

                    await clear_position()
                    await self.redis_set_cache(
                    {
                    "exit":None, 
                    "current position/base_amount":0, 
                    "max balance": 0,
                    "current position": 0, 
                    "long position": 0, 
                    "short position": 0, 
                    "base_amount":self.params["base_amount"]
                    }
                    )
                    self.max_balance = 0
                    self.d_count += 1
                    if self.d_count >= 1:
                        self.logger.warning(f"1 drawdown")
                        exit()
                    self.logger.warning(f"reach max drawback, try to lock for 300s now")
                    await asyncio.sleep(300)
                    if t < 120:
                        await asyncio.sleep(900)
                    self.last_tick = time.time()
                    self.send_order = True
                elif abs(self.pos)>Decimal(120)*self.params["base_amount"]:
                    self.logger.warning(f"reach 120 inven")
                    self.send_order = False
                    await clear_position()
                    await asyncio.sleep(90)
                    self.send_order = True

            except Exception as err:
                self.logger.warning(f"check balance err {err}")

    async def cancel_order_action(self):
        
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3*0.2:
                try:
                    await self.cancel_order(order, catch_error=False)
                except OrderNotFoundError as err:
                    if self.order_cache.get(oid) is not None:
                        o = self.order_cache[oid]
                        o.xchg_status = OrderStatus.Failed
                        await self.on_order(o)
                except Exception as err:
                    self.logger.warning(f'cancel order {oid} err {err}')
                if self.order_cache.get(oid) is not None:
                    self.order_cache[oid].message["cancel"] += 1
                    self.order_cache[oid].message["query"] += 1
                    if self.order_cache[oid].message["query"] > 10:
                        o = self.order_cache[oid]
                        o.xchg_status = OrderStatus.Failed
                        await self.on_order(o)

        while True:
            await asyncio.sleep(0.001)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )
            
    async def exit_action(self):
        async def exit_position(params):
            if not self.volume_notional_check(*params[:3]):
                return
            client_id = ClientIDGenerator.gen_client_id("gateio")
        
            self.order_cache[client_id] = self.gen_async_order(
                client_id=client_id,
                symbol_name=params[0],
                price=params[1],
                volume=params[2],
                side=params[3],
                position_side=params[4],
                order_type=params[5]
            )
            extra_info = dict()
            extra_info["stop_ts"] = 1
            extra_info["cancel"] = 0
            extra_info["source"] =  0 # entry or balance, 0 for close
            extra_info["query"] =  0
            self.order_cache[client_id].message = extra_info
            try:
                self.tf = True
                self.tf_id = client_id
                await self.make_order(
                    symbol_name=params[0],
                    price=params[1],
                    volume=params[2],
                    side=params[3],
                    position_side=params[4],
                    order_type=params[5],
                    client_id=client_id
                )
                if params[3] == OrderSide.Buy and params[4] == OrderPositionSide.Close:
                    self.short_close_lock += params[2]
                elif params[3] == OrderSide.Sell and params[4] == OrderPositionSide.Close:
                    self.long_close_lock += params[2]
            except Exception as err:
                self.logger.warning(f"take profit err: {err}")
                # o = self.order_cache[client_id]
                # o.xchg_status = OrderStatus.Failed
                # await self.on_order(o)
        
        while True:
            await asyncio.sleep(1)
            close_amount = self.params["base_amount"]*Decimal(5)
            if self.tf == False:
                if self.params["base_amount"]*Decimal(5) <abs(self.pos) <= self.params["base_amount"]*Decimal(15):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1 + 0.002):
                        await exit_position([self.symbol_1, self.bp, abs(self.pos), OrderSide.Sell, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1 - 0.002):
                        await exit_position([self.symbol_1, self.ap, abs(self.pos), OrderSide.Buy, OrderPositionSide.Open, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(15) <abs(self.pos) <= self.params["base_amount"]*Decimal(30):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1 + 0.0005):
                        await exit_position([self.symbol_1, self.bp, close_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1 - 0.0005):
                        await exit_position([self.symbol_1, self.ap, close_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(30) <abs(self.pos) <= self.params["base_amount"]*Decimal(50):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1):
                        await exit_position([self.symbol_1, self.bp, close_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1):
                        await exit_position([self.symbol_1, self.ap, close_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(50) <abs(self.pos):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1 - 0.0005):
                        await exit_position([self.symbol_1, self.bp, close_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1 + 0.0005):
                        await exit_position([self.symbol_1, self.ap, close_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos > 0 and self.bp < self.pos_price * Decimal(1 - 0.002):
                        await exit_position([self.symbol_1, self.mp*Decimal(0.99), close_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos < 0 and self.ap > self.pos_price * Decimal(1 + 0.002):
                        await exit_position([self.symbol_1, self.mp*Decimal(1.01), close_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(100) <abs(self.pos):
                    if self.pos > 0:
                        await exit_position([self.symbol_1, self.mp*Decimal(0.99), close_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.IOC])
                    if self.pos < 0:
                        await exit_position([self.symbol_1, self.mp*Decimal(1.01), close_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.IOC])
                    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()