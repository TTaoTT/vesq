
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections

from decimal import Decimal 
import asyncio
from asyncio.queues import Queue
import time
import numpy as np

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_binance = self.symbol_list[1]
        self.symbol_bit = self.symbol_list[0]
        self.ap_binance = Decimal(0)
        self.ap_bit = Decimal(0)
        self.bp_binance = Decimal(0)
        self.bp_bit = Decimal(0)
        self.mp_binance = Decimal(0)
        self.mp_bit = Decimal(0)
        self.ob_update = False
        self.send_order = True
        self.pos = Decimal(0)
        self.pos_price = Decimal(0)
        self.binance_ob_ts = time.time()
        self._orders_waiting_to_send = Queue()
        self._orders_waiting_to_cancel = Queue()

        self.ap_diff = collections.deque(maxlen=1000)
        self.bp_diff = collections.deque(maxlen=1000)
        self.mp_diff = collections.deque(maxlen=1000)

        self.ap_diff.append(float(0))
        self.bp_diff.append(float(0))
        self.mp_diff.append(float(0))

        self.action_count = 0


        self.ap_binance_cache = []
        self.bp_binance_cache = []

        self.hedge_order_to_post = []
        self.order_cache = dict()
        self.unknown_order_dict = dict()

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])

    async def before_strategy_start(self):
        self.direct_subscribe_orderbook(symbol_name=self.symbol_bit)
        self.direct_subscribe_orderbook(symbol_name=self.symbol_binance)
        self.direct_subscribe_order_update(symbol_name=self.symbol_bit)
        self.direct_subscribe_order_update(symbol_name=self.symbol_binance)
        self.logger.setLevel("WARNING")

    async def on_orderbook(self, symbol, orderbook):
        self.ob_update = True
        if symbol in self.symbol_bit:
            self.ap_bit = orderbook["asks"][0][0]
            self.bp_bit = orderbook["bids"][0][0]
            self.mp_bit = (self.ap_bit + self.bp_bit) / Decimal(2)

            if self._orders_waiting_to_send.empty() and time.time() - self.binance_ob_ts < 0.01:
                await self._orders_waiting_to_send.put(1)
            
        elif symbol in self.symbol_binance:
            self.ap_binance = orderbook["asks"][0][0]
            self.bp_binance = orderbook["bids"][0][0]
            self.mp_binance = (self.ap_binance + self.bp_binance) / Decimal(2)
            
            self.binance_ob_ts = time.time()

            ask_order = None
            bid_order = None

            for oid in list(self.order_cache):
                if self.order_cache[oid].side == OrderSide.Sell:
                    ask_order = self.order_cache[oid]
                    ask_id = oid
                else:
                    bid_order = self.order_cache[oid]
                    bid_id = oid

            symbol_cfg = self.get_symbol_config(self.symbol_bit)
            price_tick = symbol_cfg['price_tick_size']

            mp_diff_mean = Decimal(np.mean(self.mp_diff))
            mp_diff_std = Decimal(np.std(self.mp_diff))

            ap_ts = self.bp_binance - Decimal(np.median(self.ap_diff))
            bp_ts = self.ap_binance + Decimal(np.median(self.bp_diff))

            if ask_order:
                if (ask_order.requested_price < ap_ts) or (ask_order.requested_price > self.ap_bit):
                    self.order_cache[ask_id].raw_data["stop_ts"] = 0
                    self.order_cache[ask_id].raw_data["cancel"] += 1
                    self.logger.warning(f"trigger cancel: {ask_id}")
                    await self._orders_waiting_to_cancel.put(self.order_cache[ask_id])
            if bid_order:
                if (bid_order.requested_price > bp_ts) or (bid_order.requested_price < self.bp_bit):
                    self.order_cache[bid_id].raw_data["stop_ts"] = 0
                    self.order_cache[bid_id].raw_data["cancel"] += 1
                    self.logger.warning(f"trigger cancel: {bid_id}")
                    await self._orders_waiting_to_cancel.put(self.order_cache[bid_id])

            self.ap_binance_cache.append(self.ap_binance)
            self.bp_binance_cache.append(self.bp_binance)

    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[xchg_id].filled_amount
        if amount_changed <= Decimal(0):
            return 
        if self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            self.pos_price = (self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (self.pos + amount_changed)
            self.pos += amount_changed
        elif self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            if self.pos < amount_changed:
                self.pos_price = p_order.avg_filled_price
            self.pos -= amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            if self.pos + amount_changed > Decimal(0):
                self.pos_price = p_order.avg_filled_price
            self.pos += amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            self.pos_price = (-self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (-self.pos + amount_changed)
            self.pos -= amount_changed

            
    async def on_order(self, xchg_id, order: PartialOrder):
        self.logger.warning(f"{xchg_id}_{order.xchg_status}")
        if order.account_name != "bit.test":
            return

        if xchg_id not in list(self.order_cache) and order.account_name == "bit.test":
            # add unknow order dealing funciton, check_unknown_order.
            # before, will only check order after order expired. 
            self.unknown_order_dict[xchg_id] = order
            # self.logger.warning(f"find unknow order {xchg_id}")
            return
        
        if order.filled_amount < self.order_cache[xchg_id].filled_amount:
            return

        if order.filled_amount > Decimal(0):
            self.logger.warning(f"order is filled {order.filled_amount},{self.order_cache[xchg_id].filled_amount}")

        self.handle_pos(xchg_id, order)
        
        # if self.order_cache[xchg_id].side == OrderSide.Buy:
        #     side = OrderSide.Sell
        #     price = self.bp_binance * Decimal(0.99)
        # else:
        #     side = OrderSide.Buy
        #     price = self.ap_binance * Decimal(1.01)
        self.logger.warning(f"handle position done, pos:{self.pos}, price:{self.pos_price}")
        self.order_cache[xchg_id].avg_filled_price = order.avg_filled_price
        self.order_cache[xchg_id].filled_amount = order.filled_amount
        self.order_cache[xchg_id].xchg_status = order.xchg_status

        if order.xchg_status in OrderStatus.fin_status():
            self.logger.warning(f"pop cache: {xchg_id}")
            self.order_cache.pop(xchg_id)
            if xchg_id in self.unknown_order_dict:
                self.unknown_order_dict.pop(xchg_id)

        # if amount > 0 or amount_changed>Decimal(0) or order.filled_amount > Decimal(0):
        #     self.logger.warning(f"{order.filled_amount},{amount},{amount_changed}")
        #     params = [self.symbol_binance, price, amount, side, OrderPositionSide.Both, OrderType.Limit, 1, -1]
        #     await self.make_order(*params[:6])

    # async def hedge_order(self):

    #     async def batch_send_order(params):
    #         if not self.volume_notional_check(*params[:3]):
    #             return
    #         try:
    #             order = await self.make_order(*params[:6])
    #             if order:
    #                 extra_info = dict()
    #                 extra_info["stop_ts"] = params[6]
    #                 extra_info["cancel"] = 0
    #                 extra_info["source"] = params[7]
    #                 extra_info["query"] =  0
    #                 order.raw_data = extra_info
    #                 self.hedge_cache[order.xchg_id] = order
    #         except Exception as err:
    #             self.logger.warning(f"send order err, {err}")

    #     while True:
    #         await asyncio.sleep(0.001)
    #         try:
    #             if self.hedge_order_to_post:
    #                 self.logger.warning(self.hedge_order_to_post)
    #             await asyncio.gather(*[batch_send_order(param) for param in self.hedge_order_to_post])
    #             self.hedge_order_to_post = []
    #         except Exception as err:
    #             self.logger.warning(f"batch hedge err, {err}")

    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.order_cache:
                    self.logger.warning(f"unkonw {xchg_id} order rest back")
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(xchg_id, partial_order) for xchg_id, partial_order in now_know_order.items()]
            )

    def generate_orders(self):
        orders = []

        symbol_cfg = self.get_symbol_config(self.symbol_bit)
        price_tick = symbol_cfg['price_tick_size']

        ask_p = max((self.ap_bit - price_tick), (self.bp_bit + price_tick))
        bid_p = min((self.ap_bit - price_tick), (self.bp_bit + price_tick))

        mp_diff_mean = Decimal(np.mean(self.mp_diff))
        mp_diff_std = Decimal(np.std(self.mp_diff))

        max_amount = Decimal(8)*self.params["base_amount"]

        ask_q = self.params["base_amount"]
        bid_q = self.params["base_amount"]

        if self.pos>=self.params["base_amount"] and self.pos_price > self.bp_bit:
            bid_q = self.pos * Decimal(2)
        elif self.pos<=-self.params["base_amount"] and self.pos_price < self.ap_bit:
            ask_q = -self.pos * Decimal(2)

        ap_ts = self.bp_binance - Decimal(np.median(self.ap_diff))
        bp_ts = self.ap_binance + Decimal(np.median(self.bp_diff))

        self.logger.warning(f"ap_ts:{ap_ts}, bp_ts{bp_ts}, ap_bit{self.ap_bit}, bp_bit{self.bp_bit}")
        
        if (self.bp_bit < bp_ts or bp_ts < self.ap_bit) and self.pos < max_amount:
            if self.bp_bit < bp_ts:
                bid_p = self.bp_bit
            elif bp_ts <self.ap_bit:
                bid_p = bp_ts

            if self.pos < Decimal(0) and self.pos_price >= bid_p:
                bid_q = -self.pos
            orders.append([self.symbol_bit, bid_p, bid_q, OrderSide.Buy, OrderPositionSide.Both, OrderType.PostOnly,10,1])

        if (self.ap_bit > ap_ts or ap_ts > self.bp_bit) and self.pos > -max_amount:
            if self.ap_bit > ap_ts:
                ask_p = self.ap_bit
            elif ap_ts > self.bp_bit:
                ask_p = ap_ts
            
            if self.pos > Decimal(0) and self.pos_price <= ask_p:
                ask_q = self.pos
            orders.append([self.symbol_bit, ask_p, ask_q, OrderSide.Sell, OrderPositionSide.Both, OrderType.PostOnly,10,1])
        
        buy_source = [self.order_cache[key].raw_data["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Buy]
        sell_source = [self.order_cache[key].raw_data["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Sell]
        
        filter_orders = []

        for per_req in orders:
            if per_req[3] == OrderSide.Buy:
                x_source = np.array(buy_source)
            else:
                x_source = np.array(sell_source)
            x_source = np.array(x_source)
            cur_pending_len = sum((x_source == per_req[7]) * 1)
            if cur_pending_len < 1:
                filter_orders.append(per_req)

        return filter_orders

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def send_order_action(self):
        async def batch_send_order(params):
            if not self.volume_notional_check(*params[:3]):
                return
            try:
                order =  await self.make_order(*params[:6])
                self.action_count += 1
                if order:
                    extra_info = dict()
                    extra_info["stop_ts"] = params[6]
                    extra_info["cancel"] = 0
                    extra_info["source"] =  params[7] # entry or balance
                    extra_info["query"] =  0
                    order.raw_data = extra_info
                    self.order_cache[order.xchg_id] = order
                    self.order_cache[order.xchg_id].filled_amount = Decimal(0)

            except Exception as err:
                self.logger.warning(f"send order err, {err}")

        while self.is_running():
            await self._orders_waiting_to_send.get()
            if self.send_order and self.action_count < 5:
                orders = self.generate_orders()
                if not orders:
                    continue
                try:
                    await asyncio.gather(*[batch_send_order(order_params) for order_params in orders])
                except Exception as err:
                    self.logger.warning(f"batch send order err, {err}")
    
    async def update_cancel_order(self):

        async def batch_cancel(oid, order):

            if time.time()*1e3 - order.create_ms > order.raw_data["stop_ts"] * 1e3 + order.raw_data["cancel"]*1e3:
                try:
                    await self._orders_waiting_to_cancel.put(order)
                    if self.order_cache.get(oid):
                        self.order_cache[oid].raw_data["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f'cancel order update {oid} err {err}')
            
        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )
    
    async def cancel_order_action(self):

        while self.is_running():
            order = await self._orders_waiting_to_cancel.get()
            self.logger.warning(f"canceling order: {order.xchg_id}")
            try:
                await self.cancel_order(order)
                self.action_count += 1
                if self.order_cache.get(order.xchg_id) is not None:
                    self.order_cache[order.xchg_id].raw_data["cancel"] += 1
            except Exception as err:
                self.logger.warning(f'cancel order {order.xchg_id} err {err}')


    async def close_position(self,params):
        if not self.volume_notional_check(*params[:3]):
            return
        try:
            order =  await self.make_order(*params)
            self.action_count += 1
            if order:
                extra_info = dict()
                extra_info["stop_ts"] = 1
                extra_info["cancel"] = 0
                extra_info["source"] = 0
                extra_info["query"] =  0
                order.raw_data = extra_info
                self.order_cache[order.xchg_id] = order

        except Exception as err:
            self.logger.warning(f"close position err, {err}")

    async def match_position(self):
        while True:
            await asyncio.sleep(300)
            self.send_order = False
            await asyncio.sleep(5)
            try:
                api_bit = self.get_exchange_api("bit")[0]
                api_binance = self.get_exchange_api("binance")[0]
                bit_position = ((await api_bit.contract_position(self.get_symbol_config(self.symbol_bit))).data)["amount"]
                binance_position = ((await api_binance.contract_position(self.get_symbol_config(self.symbol_binance))).data).get("amount")
                if binance_position is None:
                    self.send_order = True
                    return
                gap = bit_position + binance_position * Decimal(100)
                self.logger.warning(gap)
                if gap > Decimal(100):
                    q = int(gap/100)
                    await self.make_order(self.symbol_binance, self.bp*Decimal(0.99), q, OrderSide.Sell, OrderPositionSide.Both, OrderType.Limit)
                if gap < -Decimal(100):
                    q = -int(gap/100)
                    await self.make_order(self.symbol_binance, self.ap*Decimal(1.01), q, OrderSide.Buy, OrderPositionSide.Both, OrderType.Limit)
            except Exception as err:
                self.logger.warning(f"match position err, {err}")
            self.send_order = True


    async def update_redis_cache(self):

        async def batch_cancel(oid, order):
            try:
                await self.cancel_order(order)
                self.action_count += 1
            except Exception as err:
                self.logger.warning(f'cancel order {oid} err {err}')

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send_order = False
                    for i in range(3):
                        await asyncio.gather(
                            *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
                            )

                        api_bit = self.get_exchange_api("bit")[0]
                        api_binance = self.get_exchange_api("binance")[0]
                        bit_position = ((await api_bit.contract_position(self.get_symbol_config(self.symbol_bit))).data)["amount"]
                        self.action_count += 1
                        binance_position = ((await api_binance.contract_position(self.get_symbol_config(self.symbol_binance))).data).get("amount")

                        if bit_position is not None:
                            bit_side = OrderSide.Buy if bit_position<Decimal(0) else OrderSide.Sell
                            bit_q = -bit_position if bit_position<Decimal(0) else bit_position
                            bit_p = self.ap_bit*Decimal(1.01) if bit_position<Decimal(0) else self.bp_bit*Decimal(0.99)
                            await self.close_position([self.symbol_bit, bit_p, bit_q, bit_side, OrderPositionSide.Both, OrderType.Limit])

                        if binance_position is not None:
                            binance_side = OrderSide.Buy if binance_position<Decimal(0) else OrderSide.Sell
                            binance_q = -binance_position if binance_position<Decimal(0) else binance_position
                            binance_p = self.ap_binance*Decimal(1.01) if binance_position<Decimal(0) else self.bp_binance*Decimal(0.99)
                            await self.close_position([self.symbol_binance, binance_p, binance_q, binance_side, OrderPositionSide.Both, OrderType.Limit])
                        await asyncio.sleep(3)

                    await self.redis_set_cache(
                        {
                        "exit":None, 
                        }
                        )
                    
                    self.logger.warning(f"manually exiting")
                    exit()

                # if not self.qty_switching and data.get("base_amount") is not None:
                #     if abs(Decimal(data.get("base_amount")) - self.params["base_amount"]) > Decimal(0.05)*self.params["base_amount"]:
                #         self.params["base_amount"] = Decimal(data.get("base_amount"))
                #         self.base_amount = Decimal(data.get("base_amount"))
                        
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        async def set_cache():
            try:
                api = self.get_exchange_api("bit")[0]
                api_binance = self.get_exchange_api("binance")[0]
                balance = (await api.account_balance(self.get_symbol_config(self.symbol_bit))).data
                self.action_count += 1
                equity = balance.get('btc')

                position = (await api.contract_position (self.get_symbol_config(self.symbol_bit))).data
                self.action_count += 1
                self.pos = position["amount"]
                position_binance = (await api_binance.contract_position (self.get_symbol_config(self.symbol_binance))).data
                await self.redis_set_cache(
                    {
                    "exit":None, 
                    "mp_diff_mean":np.mean(self.mp_diff),
                    "bit_position":self.pos,
                    "bit_position_price":self.pos_price
                    
                    }
                    )
            except Exception as err:
                self.logger.warning(f"set cache failed {err}")

        while True:
            await asyncio.sleep(5)
            await check_cache()
            await set_cache()

    async def get_order_status_direct(self,order):
        
        try:
            api = self.get_exchange_api("bit")[0]
            self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            self.action_count += 1
            res = await api.order_match_result(self.get_symbol_config(self.symbol_bit),order.xchg_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if time.time()*1e3 - order.create_ms > (order.raw_data["stop_ts"] + 1)*1e3 and order.raw_data["cancel"]>0:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].raw_data["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order(oid, order_new)
                    elif self.order_cache[oid].raw_data["query"]>10:
                        self.order_cache.pop(oid)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )
     
    async def get_stats(self):
        while True:
            await asyncio.sleep(0.001)
            if self.ob_update:
                self.bp_diff.append(float(self.bp_bit-self.ap_binance))
                self.ap_diff.append(float(self.bp_binance-self.ap_bit))
                self.mp_diff.append(float(self.mp_bit-self.mp_binance))
                self.ob_update = False
            self.ap_binance_cache = []
            self.bp_binance_cache = []

    async def action_restriction(self):
        while True:
            await asyncio.sleep(1)
            self.action_count = 0

    async def strategy_core(self):
        await asyncio.sleep(15)
        await asyncio.gather(
            self.send_order_action(),
            self.update_redis_cache(),
            self.update_cancel_order(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.get_stats(),
            self.action_restriction()
        )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()