import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections


from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

class PairData():
    def __init__(self) -> None:
        self.pnl_all_day = collections.deque(maxlen=24)
        self.vol_all_day = collections.deque(maxlen=24)
        
        self.pnl = Decimal(0)
        self.vol = Decimal(0)
        
    def new_order(self, pnl, vol):
        self.pnl += pnl
        self.vol += vol
    
    def clear_and_store(self):
        self.pnl_all_day.append(self.pnl)
        self.vol_all_day.append(self.vol)
        self.pnl = Decimal(0)
        self.vol = Decimal(0)
        
    def get_stats(self,hours):
        if not self.pnl_all_day:
            return self.pnl,self.vol
        pnl = Decimal(0)
        vol = Decimal(0)
        if len(self.pnl_all_day) > hours:
            for i in range(hours):
                pnl += self.pnl_all_day[-i-1]
        else:
            pnl = sum(self.pnl_all_day)
        
        if len(self.vol_all_day) > hours:
            for i in range(hours):
                vol += self.vol_all_day[-i-1]
        else:
            vol = sum(self.vol_all_day)
            
        return round(pnl,2), round(vol/Decimal(10000), 2)
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False
        self.symbol = "binance.gala_usdt_swap.usdt_contract.na"
        self.cache = dict()
        self.use_raw_stream = True

    async def before_strategy_start(self):
        self.logger.setLevel("WARNING")
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
    async def on_order(self, o):
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        if order["s"] not in list(self.cache):
            self.cache[order["s"]] =  PairData()
        if order.get("n"):
            pnl = Decimal(order["rp"]) - Decimal(order["n"])
        else:
            pnl = Decimal(order["rp"])
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        if status in OrderStatus.fin_status():
            vol = Decimal(order["z"]) * Decimal(order["ap"])
        else:
            vol = Decimal(0)
            
        self.cache[order["s"]].new_order(pnl,vol)
                
    async def trim_and_set_cache(self):
        while True:
            await asyncio.sleep(60*60)
            try:
                data = dict()
                data["overall"] = dict()
                o_pnl1 = Decimal(0)
                o_pnl6 = Decimal(0)
                o_pnl24 = Decimal(0)
                o_vol1 = Decimal(0)
                o_vol6 = Decimal(0)
                o_vol24 = Decimal(0)
                for key in list(self.cache):
                    self.cache[key].clear_and_store()
                    data[key] = dict()
                    pnl1, vol1 = self.cache[key].get_stats(1)
                    pnl6, vol6 = self.cache[key].get_stats(6)
                    pnl24, vol24 = self.cache[key].get_stats(24)
                    
                    data[key]["pnl"] = f"{pnl1}__{pnl6}__{pnl24}"
                    data[key]["vol"] = f"{vol1}__{vol6}__{vol24}"
                    
                    o_pnl1 += pnl1
                    o_pnl6 += pnl6
                    o_pnl24 += pnl24
                    
                    o_vol1 += vol1
                    o_vol6 += vol6
                    o_vol24 += vol24
                    
                    if vol24 == Decimal(0):
                        self.cache.pop(key)
                        
                data["overall"]["pnl"] = f"{o_pnl1}__{o_pnl6}__{o_pnl24}"
                data["overall"]["vol"] = f"{o_vol1}__{o_vol6}__{o_vol24}"
                    
                await self.redis_set_cache(data)
                    
            except Exception as err:
                self.logger.warning(f"trim and set cahce err:{err}")
    
    async def debug_function(self):
        while True:
            await asyncio.sleep(10)
            for key in self.cache:
                self.logger.warning(f"{key},pnl: {self.cache[key].pnl}, vol: {self.cache[key].vol}")
            
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.trim_and_set_cache(),
            # self.debug_function()
            )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    