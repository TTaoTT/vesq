import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0

        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cahce[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        
                
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.buy_trade_cache = PriceDistributionCache()
        self.sell_trade_cache = PriceDistributionCache()

        self.quantile = self.params["quantile"]
        self.weight = self.params["weight"]

        self.price_muli = 1e7
        self.qty_muli = 1e7
        self.recvWindow = self.params["recvWindow"]

        self.order_to_send = Queue()
        self.order_to_cancel = Queue()
        self.trade_ts = 0
        self.bbo_ts = 0
        self.pos_limit = Decimal(5)

        self.order_cache = dict()
        self.ask_order_cache = None
        self.bid_order_cache = None

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False
        self.sending = False
        
        self.order_count = 0
        self.order_fail_count = 0

    def remove_order(self,cid):
        order = self.order_cache.pop(cid)
        if order.side == OrderSide.Buy and self.bid_order_cache and self.bid_order_cache.client_id==cid:
            self.bid_order_cache = None
        elif order.side == OrderSide.Sell and self.ask_order_cache and self.ask_order_cache.client_id==cid:
            self.ask_order_cache = None

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def internal_fail(self,client_id,err=None):
        self.logger.warning(f"failed order: {err}")
        o = self.order_cache[client_id]
        o.xchg_status = OrderStatus.Failed
        await self.on_order(o)

    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.9*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.9*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.9*1200

        if cond1 or cond2 or cond3:
            self.hold_back = True
            await asyncio.sleep(5)
            self.logger.warning("rest for 5s done")
            self.hold_back = False
    
    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.pos = position.data["long_qty"] - position.data["short_qty"]
                
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol)
                qty_tick = symbol_cfg["qty_tick_size"]
                price_tick = symbol_cfg['price_tick_size']
                self.half_step_tick = qty_tick/Decimal(2)
                c = 0
                if Decimal(self.price_muli)*price_tick < Decimal(0.5) or Decimal(self.qty_muli)*qty_tick < Decimal(0.5):
                    self.logger.warning("price or qty multi not good enough")
                    exit()
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ap:
                        self.logger.warning(f"{self.ap}")
                        amount = min(balance,Decimal(5000)) * Decimal(0.3)
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/qty_tick)*qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
                        break
                    if c > 1000:
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise

    async def update_amount(self):
        while True:
            await asyncio.sleep(60*60*24)
            try:
                api = self.get_exchange_api("binance")[0]  
                cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
                equity = cur_balance.get('usdt')
                if equity:
                    balance = equity["all"]
                    symbol_cfg = self.get_symbol_config(self.symbol)
                    qty_tick = symbol_cfg["qty_tick_size"]
                    if self.ap:
                        amount = min(balance,Decimal(5000)) * Decimal(0.3)
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/qty_tick)*qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")

    def handle_pos(self,client_id,p_order):

        if self.order_cache[client_id].side == OrderSide.Buy:
            self.pos = self.pos - self.order_cache[client_id].requested_amount + p_order.filled_amount
        else:
            self.pos = self.pos + self.order_cache[client_id].requested_amount - p_order.filled_amount

    async def on_order(self, order: PartialOrder):
        client_id = order.client_id
        if client_id not in list(self.order_cache): 
            return

        if order.xchg_status in OrderStatus.fin_status():
            self.handle_pos(client_id,order)
            self.remove_order(client_id)
            if self.order_to_send.empty() and not self.sending:
                self.order_to_send.put_nowait(1)
            
    async def on_bbo(self, symbol, bbo):
        t_s = bbo["data"]["E"] - self.bbo_ts
        
        self.ap = float(bbo["data"]["a"])
        self.bp = float(bbo["data"]["b"])

        if self.order_to_send.empty() and t_s > 80 and not self.sending:
            self.bbo_ts = bbo["data"]["E"]
            self.s = time.time()
            self.order_to_send.put_nowait(1)
        if self.order_cache and self.order_to_cancel.empty():
            self.order_to_cancel.put_nowait(1)
    
    async def on_public_trade(self, symbol, trade):
        self.trade_ts = max(self.trade_ts, trade["data"]["E"])
        tmp = {
            "t":trade["data"]["E"],
            "p":int(float(trade["data"]["p"])*self.price_muli), 
            "q":int(float(trade["data"]["q"])*self.qty_muli)
            }
        if trade["data"]["m"]:
            self.sell_trade_cache.feed_trade(tmp)
        else:
            self.buy_trade_cache.feed_trade(tmp)

        self.buy_trade_cache.remove_expired(self.trade_ts)
        self.sell_trade_cache.remove_expired(self.trade_ts)

    async def send_order_action(self):
        async def replace_order(order,price,qty,side):
            res = await self.batch_cancel(order.client_id,order)
            if res:
                await self.send_order(price,qty,side)
            
        while True:
            await self.order_to_send.get()
            self.sending = True
            ask = self.buy_trade_cache.get_ask_price(self.quantile)
            bid = self.sell_trade_cache.get_bid_price(self.quantile)
            if not ask:
                ask = self.ap
            else:
                ask = ask/self.price_muli
            if not bid:
                bid = self.bp
            else:
                bid = bid/self.price_muli
            weight = ask - bid
            weight = weight / (ask + bid) * 2

            cur_risk = self.pos/self.amount_decimal
            
            
            if self.rate_limit:
                await self.retreat()
            elif abs(cur_risk) < Decimal(0.01):
                if not self.ask_order_cache and not self.bid_order_cache:
                    if not self.rate_limit and self.send and not self.hold_back:
                        self.logger.warning(f"price gap: {weight}")
                        weight = weight/0.001
                        weight = min(weight, 1.5)
                        qty = self.amount * weight
                        await asyncio.gather(
                            self.send_order(Decimal(ask),qty,OrderSide.Sell),
                            self.send_order(Decimal(bid),qty,OrderSide.Buy)
                        )
                elif self.ask_order_cache and not self.bid_order_cache:
                    if Decimal(ask) < self.ask_order_cache.requested_price:
                        qty = self.ask_order_cache.requested_amount
                        res = await self.batch_cancel(self.ask_order_cache.client_id,self.ask_order_cache)
                        if res:
                            await self.send_order(Decimal(ask),qty,OrderSide.Sell)
                elif not self.ask_order_cache and self.bid_order_cache:
                    if Decimal(bid) > self.bid_order_cache.requested_price:
                        qty = self.bid_order_cache.requested_amount
                        res = await self.batch_cancel(self.bid_order_cache.client_id,self.bid_order_cache)
                        if res:
                            await self.send_order(Decimal(bid),qty,OrderSide.Buy)
                else:
                    if weight < self.weight:
                        await asyncio.gather(
                            self.batch_cancel(self.bid_order_cache.client_id,self.bid_order_cache),
                            self.batch_cancel(self.ask_order_cache.client_id,self.ask_order_cache)
                        )
                    elif Decimal(ask) < self.ask_order_cache.requested_price:
                        qty = self.ask_order_cache.requested_amount
                        await replace_order(self.ask_order_cache,Decimal(ask),qty,OrderSide.Sell)
                    elif Decimal(bid) > self.bid_order_cache.requested_price:
                        qty = self.bid_order_cache.requested_amount
                        await replace_order(self.bid_order_cache,Decimal(bid),qty,OrderSide.Buy)
                        
            elif cur_risk > Decimal(0.01):
                if self.ask_order_cache:
                    qty = self.ask_order_cache.requested_amount + abs(self.pos)
                    await replace_order(self.ask_order_cache,Decimal(ask),qty,OrderSide.Sell)
                else:
                    qty = abs(self.pos)
                    await self.send_order(Decimal(ask),qty,OrderSide.Sell)
                    
            elif cur_risk < Decimal(0.01):
                if self.bid_order_cache:
                    qty = self.bid_order_cache.requested_amount + abs(self.pos)
                    await replace_order(self.bid_order_cache,Decimal(bid),qty,OrderSide.Buy)
                else:
                    qty = abs(self.pos)
                    await self.send_order(Decimal(bid),qty,OrderSide.Buy)
            
            self.sending = False
                    
    
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=None,timestamp=None):
        client_id = ClientIDGenerator.gen_client_id("binance")
        self.order_count += 1
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow
        if timestamp == None:
            timestamp = self.trade_ts

        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = 1
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        if side == OrderSide.Sell:
            self.ask_order_cache = order
            self.pos -= qty
        else:
            self.bid_order_cache = order
            self.pos += qty
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty+self.half_step_tick,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=order.create_ms,
                recvWindow=recvWindow,
                client_id=client_id

            )
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            self.order_fail_count += 1
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def batch_cancel(self, oid, order):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                res = await self.cancel_order(order)
                self.canceling_id.remove(oid)
                if res:
                    return True
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):

        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
                
        while True:
            await self.order_to_cancel.get()
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*6)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].message["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order(order_new)
                    elif self.order_cache[oid].message["query"]>10:
                        self.send = False
                        await self.retreat()
                        await self.redis_set_cache({})
                        self.logger.warning(f"check order too many times and exiting")
                        exit()
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def check_position(self):
        while True:
            await asyncio.sleep(60)
            try:
                if not self.rate_limit:
                    api = self.get_exchange_api("binance")[0]
                    position = await api.contract_position(self.get_symbol_config(self.symbol))
                    pos = position.data["long_qty"] - position.data["short_qty"]
                    if abs(pos - self.pos)>self.amount_decimal*self.pos_limit:
                        self.logger.warning(
                        f"""
                        =========update wrong position==========
                        origin: {self.pos}
                        update: {pos}
                        """
                    )
                    self.pos = pos
                    
            except Exception as err:
                self.logger.warning(f'check position err {err}')

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(Decimal(self.bp*0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(Decimal(self.ap*1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None,"fail rate":self.order_fail_count/self.order_count if self.order_count else 0})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(1)
        await asyncio.gather(
            self.update_redis_cache(),
            self.check_position(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            self.update_amount()
            # self.debug_funtion(),
            # self.time_control_function()
            )

    # async def debug_funtion(self):
    #     while True:
    #         await asyncio.sleep(1)
    #         if self.order_cache:
    #             self.logger.warning(f"all order:{self.order_cache}")
    #             self.logger.warning(f"ask order:{self.ask_order_cache}")
    #             self.logger.warning(f"bid order:{self.bid_order_cache}")
    
    # async def time_control_function(self):
    #     await asyncio.sleep(60*30)
    #     await self.retreat()
    #     self.logger.warning("testing over")
    #     exit()
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    