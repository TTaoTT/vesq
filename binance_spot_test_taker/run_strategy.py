import asyncio
from decimal import Decimal
from this import d
import time 
import numpy as np
import collections
from asyncio.queues import Queue
import pandas as pd

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class TradeImbalance():
    def __init__(self,cache_size=3000) -> None:
        self.cache = []
        self.ob_cache = []
        self.cache_size = cache_size
        self.buy_qty = 0
        self.sell_qty = 0


    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
        if trade["s"] == 1:
            self.buy_qty += trade["q"]
        else:
            self.sell_qty += trade["q"]
            
    def remove_expired(self,ts):
        self.remove_expired_trade(ts)

    def remove_expired_trade(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            trade = self.cache.pop(0)
            if trade["s"] == 1:
                self.buy_qty -= trade["q"]
            else:
                self.sell_qty -= trade["q"]

    def get_result(self):
        return  0 if self.buy_qty + self.sell_qty == 0 else (self.buy_qty - self.sell_qty) / (self.buy_qty + self.sell_qty)


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_usdt.spot.na"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_order_queue = Queue()
        self.cancel_order_queue = Queue()
        self.hedge_queue = Queue()
        self.bbo_ts = None
        
        self.orders = []
        self.balance_qty = 0.002
        self.qty = Decimal("0.001")
        self.gap = 3

        self.cancel_ts = 0
        self.canceling_id = set()
        self.pos = 0
        self.trade_cache = TradeImbalance()

        self.buy_amount = 0
        self.sell_amount = 0
        self.buy_qty = 0
        self.sell_qty = 0
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order(o)
        except:
            pass

    async def before_strategy_start(self):
        
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        await asyncio.sleep(5)
        self.use_raw_stream = True
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)


        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        self.half_step_tick = qty_tick/Decimal(2)
        self.half_price_tick = price_tick/Decimal(2)
        self.price_tick = price_tick

        self.logger.setLevel("WARNING")
        
    async def on_bbo(self,symbol, bbo):
        self.ap = Decimal(bbo["data"]["a"])
        self.bp = Decimal(bbo["data"]["b"])

        t = int(time.time()*1e3)

        if self.bbo_ts and t % 100 < self.bbo_ts:
            self.send_order_queue.put_nowait(1)
        self.bbo_ts = t % 100
    
    async def on_agg_trade(self, symbol, trade):
        tmp = {
            "t":trade["data"]["T"],
            "q":Decimal(trade["data"]["q"]), 
            "s":-1 if trade["data"]["m"] else 1
            }

        self.trade_cache.feed_trade(tmp)
        self.trade_cache.remove_expired(trade["data"]["T"])

    async def on_order(self, order):
        if order.xchg_status in OrderStatus.fin_status():
            self.pos += order.filled_amount if self.order_cache[order.client_id].side == OrderSide.Buy else -order.filled_amount
            if self.order_cache[order.client_id].side == OrderSide.Buy:
                self.buy_amount += order.filled_amount*order.avg_filled_price
                self.buy_qty += order.filled_amount
            else:
                self.sell_amount += order.filled_amount*order.avg_filled_price
                self.sell_qty += order.filled_amount
            self.order_cache.pop(order.client_id)
    

    async def send_order_action(self):
        while True:
            await self.send_order_queue.get()
            res = self.trade_cache.get_result()
            if res > 0.25:
                qty = self.qty - self.pos
                if qty > 0:
                    self.loop.create_task(self.send_order(self.ap,qty,OrderSide.Buy))
            if res < -0.25:
                qty = self.qty + self.pos
                if qty > 0:
                    self.loop.create_task(self.send_order(self.bp,qty,OrderSide.Sell))
        
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.IOC):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)

        
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        self.order_cache[client_id] = order

        try:
            await self.make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                client_id=client_id
            )

        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)


    async def log_data(self):
        while True:
            await asyncio.sleep(60)
            self.logger.warning(f"{self.sell_amount/self.sell_qty - self.buy_amount/self.buy_qty}")


    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
            self.send_order_action(),
            self.log_data()
            )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
        