import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue
from typing import AnyStr, List, Union
import ujson as json
import math

from atom.helpers import ClientIDGenerator, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_executor.order_executor import OrderExecutorTest2, TransOrder
from xex_mm.utils.base import Depth, Trade, Direction, ReferencePriceCalculator
from xex_mm.utils.configs import price_precision, qty_precision

from atom.model.depth import Depth as AtomDepth
import traceback

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.contract = "btc_usdt"
        self.symbol = f"jojo.{self.contract}.usdt_contract.na"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_order_queue = Queue()
        self.cancel_order_queue = Queue()
        self.bbo_ts = None
        
        self.ref_prc_cal = ReferencePriceCalculator()

        self.send = True
        self.sending = False
        self.canceling = False
        
        self.ap = None
    
    async def before_strategy_start(self):
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
        self.direct_subscribe_orderbook(symbol_name=self.symbol, is_incr_depth=True)
        
    async def on_order(self, order):
        if self.order_cache.get(order.client_id) is None:
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            o = self.order_cache.get(order.client_id)
            try:
                if order.filled_amount > 0 and o.message["label"] == 0:
                    amt_chg = order.filled_amount if o.side == OrderSide.Buy else - order.filled_amount
                    symbol_config = self.get_symbol_config(o.symbol_id)
                    self.executor.update_finished_order(
                        ".".join((symbol_config["exchange_name"],symbol_config["pair"])), 
                        float(amt_chg)
                    )
                    
                side = o.side
                acc = o.account_name
                if side == OrderSide.Buy:
                    self.acc_available[acc]["usdt"] += (o.requested_amount * o.requested_price - order.filled_amount * order.avg_filled_price)
                    self.acc_available[acc]["btc"] += order.filled_amount
                else:
                    self.acc_available[acc]["btc"] += (o.requested_amount - order.filled_amount)
                    self.acc_available[acc]["usdt"] += order.filled_amount * order.avg_filled_price
                        
                self.order_cache.pop(order.client_id)
            
            except Exception as err:
                traceback.print_exc()
                self.logger.critical(f"handle order err: {err}")
    
    async def on_orderbook(self, symbol, orderbook):
        try:
            self.ap = Decimal(orderbook["asks"][0][0])
            new_depth = Depth.from_dict(depth=orderbook)
            self.executor.update_depth(symbol, new_depth)

            ref_prc = self.ref_prc_cal.calc(self.executor.get_depth(symbol))
            if self.send_order_queue.empty() and not self.sending and self.send:
                self.send_order_queue.put_nowait(ref_prc)
                
            if self.cancel_order_queue.empty() and not self.canceling:
                self.cancel_order_queue.put_nowait(1)
        
        except:
            traceback.print_exc()
            
        
    async def handle_cancel_order(self,oid):
        try:
            if self.order_cache.get(oid):
                if oid not in self.canceling_id:
                    trader_list = sorted(self.ip_load, key=self.ip_load.get)
                    self.canceling_id.add(oid)
                    self.order_cache[oid].message["cancel"] = True
                    await self.publish_to_channel(
                        "mq:hf_signal:cancel_request",
                        {
                            "strategy_name":trader_list[0],
                            "order": self.order_cache[oid].to_json(),
                            "main_sn":self.strategy_name
                        }
                    )

        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
    
    async def send_order(self, price, qty, side, account_name=None, level=None, order_type=OrderType.Limit):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            order_type=order_type,
            position_side=OrderPositionSide.Open,
            account_name=account_name
        )
        extra_info = dict()
        extra_info["cancel"] = False
        extra_info["query"] = 0
        extra_info["life"] = 100
        extra_info["level"] = level
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
            
        try:
            await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                order_type=order_type,
                client_id=client_id,
                account_name=account_name,
                position_side=OrderPositionSide.Open,
            )
        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"{err}")
            await self.internal_fail(client_id,err)