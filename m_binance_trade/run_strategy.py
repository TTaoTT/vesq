import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import pandas as pd
import aiohttp
from asyncio.queues import Queue

from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

# base on s32_1, automatic change base amount,delete some params

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.start = self.config['strategy']['params']["start"]
        self.end = self.config['strategy']['params']["end"]
        self.symbol = self.config['strategy']['params']["symbol"]
        
    
    async def execute(self,start):
        api = self.get_exchange_api("binance")[0]
        res = await api.make_request(
            market="usdt_contract",
            method="GET",
            endpoint="/fapi/v1/aggTrades",
            query=dict(
                symbol=self.symbol,
                startTime=str(start),
                limit=str(1000),
                timestamp=str(int(time.time()*1e3)))
            )
        return res.json
    
    async def get_data(self,start,end):
        data = pd.DataFrame()
        while start < end:
            res = await self.execute(start)
            if res:
                start = res[-1]["T"]
            else:
                break
            await asyncio.sleep(1)
            data1 = pd.DataFrame(res)
            data = pd.concat([data,data1])
        data.to_csv(f"./data/{self.symbol}_{self.start}_{self.end}_trades.csv")
    
    async def strategy_core(self):
        await self.get_data(self.start,self.end)
    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    
    