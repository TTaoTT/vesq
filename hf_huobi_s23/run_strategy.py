import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy

from Estein import Estein

class MyStrategy(Estein):
    """
    1000shib
    {
        "mt_line" : 0.0008701675430150109,
        "std_line": 5.645012072205979e-06,
        "base_amount":750,
        "model_params": {
            "up_c_coef": 7.597565484127043e-08,
            "up_c_int": 0.0011419423437169223,
            "up_f_coef": 6.832285985465625e-07,
            "up_f_int": -0.0004008578623042261,
            "down_c_coef": 7.409106415997389e-08,
            "down_c_int": 0.001142924323096629,
            "down_f_coef": 5.849067660863639e-07,
            "down_f_int": -0.0003493628558276356
    }
    }

    doge
    {
        "mt_line" : 0.0008070218261430952,
        "std_line": 0.001756994568023799,
        "base_amount":20,
        "model_params": {
            "up_c_coef": 1.1248517599236778e-06,
            "up_c_int": 5.306240356248127e-05,
            "up_f_coef": 4.995125935201646e-07,
            "up_f_int": 1.7425333182578644e-05,
            "down_c_coef": 1.0043788661679417e-06,
            "down_c_int": 0.00012143100047970435,
            "down_f_coef": 4.5502511061070124e-07,
            "down_f_int": 4.5965380177283126e-05
    }
        
    }
    
    eth
    {
    "mt_line": 0.0009841303912762056,
    "std_line": 1.324417139087233,
    "base_amount": 0.005,
    "model_params": {
        "up_c_coef": 0.0000222710353,
        "up_c_int": 0.0003944,
        "up_f_coef": 0.0000122213423,
        "up_f_int": 0.00015009,
        "down_c_coef": 0.0000211126932,
        "down_c_int": 0.00040205,
        "down_f_coef": 0.0000143259034,
        "down_f_int": 0.00011922
    }
}
    """
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
    
    def get_order_position(self):
        
        ask_ceiling = Decimal(round(self._params["up_c_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up_c_int"],4))
        ask_floor = Decimal(round(self._params["up_f_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up_f_int"],4))
        
        bid_ceiling = Decimal(round(self._params["down_c_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down_c_int"],4))
        bid_floor = Decimal(round(self._params["down_f_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down_f_int"],4))
        
        sell_spread_tick = round((ask_ceiling - ask_floor) / 3, 5)
        buy_spread_tick = round((bid_ceiling - bid_floor) / 3, 5)

        if sell_spread_tick < 0:
            sell_spread_tick = Decimal(0)
        if buy_spread_tick < 0:
            buy_spread_tick = Decimal(0)

        return ask_floor, bid_floor, sell_spread_tick, buy_spread_tick

    def generate_entry_orders(self):
        if abs(self.pos)>Decimal(100)*self.params["base_amount"] or not self.mp:
            return []
        entry_orders = []
        mp = self.mp
        
        ask_floor, bid_floor, sell_spread_tick, buy_spread_tick = self.get_order_position()

        for i in range(1, 5):
            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor

            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))

                elif cd < 0:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            s_amount = self.get_place_amount(net_loc=i, base_amount=self.params["base_amount"],level=2)
            b_amount = self.get_place_amount(net_loc=i, base_amount=self.params["base_amount"],level=2)

            b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,1]
            s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,1]

            entry_orders.append(b_io)
            entry_orders.append(s_io)

        return entry_orders

    def generate_balance_orders(self):
        if not self.mp:
            return []
        cur_risk = self.pos

        balance_orders = []
        mp = self.mp
        total_amount = Decimal(abs(cur_risk))
        
        ask_floor, bid_floor, sell_spread_tick, buy_spread_tick = self.get_order_position()

        for i in range(4, 0, -1):

            sell_spread = sell_spread_tick * Decimal(i-1) + ask_floor
            buy_spread = buy_spread_tick * Decimal(i-1) + bid_floor
        
            sell_p = mp * (Decimal(1) + Decimal(sell_spread))
            buy_p = mp * (Decimal(1) - Decimal(buy_spread))

            if not self.mp_std_handle.mp_bad():
                sell_p = mp * (Decimal(1) + Decimal(sell_spread))
                buy_p = mp * (Decimal(1) - Decimal(buy_spread))
            else:
                cd = self.mp_std_handle.get_cur_duration()
                if cd > 0:
                    buy_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) - Decimal(buy_spread))
                else:
                    sell_p = Decimal(self.mp_std_handle.get_mean_mp()) * (
                            Decimal(1) + Decimal(sell_spread))

            ex_amount = self.get_place_amount(net_loc=5-i,base_amount=self.params["base_amount"])
            if cur_risk > 0:

                if self.movement_adjust.sell_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                #
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, sell_p, ex_amount, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, sell_p, total_amount, OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
            else:
                if self.movement_adjust.buy_ban_mem_ts is not None:
                    ex_amount = self.get_place_amount(net_loc=i,base_amount=self.params["base_amount"])
                if total_amount > ex_amount:
                    x_io = [self.symbol_1, buy_p, ex_amount, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,-1]
                    total_amount -= ex_amount
                    balance_orders.append(x_io)
                else:
                    if total_amount > 0:
                        x_io = [self.symbol_1, buy_p, total_amount, OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly,i,-1]
                        balance_orders.append(x_io)
                        return balance_orders
        return balance_orders

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
