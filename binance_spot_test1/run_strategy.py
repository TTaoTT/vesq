import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue
from typing import AnyStr, List, Union
import ujson as json
import math

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_executor.order_executor import OrderExecutorTest2, TransOrder
from xex_mm.utils.base import Depth, Trade, Direction, ReferencePriceCalculator
from xex_mm.utils.configs import price_precision, qty_precision

from atom.model.depth import Depth as AtomDepth
import traceback


ART = 100
FREQUENCY = 500
FLOOR = 0.001
BETA_BASE = 1e-2
T = 1000

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.symbol = "binance.btc_usdt.spot.na"
        self.contract = "btc_usdt"
        self.order_cache = dict()
        self.canceling_id = set()
        self.send_order_queue = Queue()
        self.cancel_order_queue = Queue()
        self.bbo_ts = None
        
        self.prc_per_diff = 0.00005
        self.ref_prc_cal = ReferencePriceCalculator()

        self.cancel_ts = 0
        self.canceling_id = set()
        self.send = True
        self.sending = False
        self.canceling = False
        
        self.ap = None
        
        self.last_bbo = 0
        
        self.executor = OrderExecutorTest2(
            iids=["binance.btc_usdt"], 
            contract=self.contract, 
            art=ART, 
            floor=FLOOR,
            onboard=True)
    
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def cancel_order(self, order: Order, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.data
        except Exception as err:
            if not catch_error:
                raise
            if type(err) in (OrderAlreadyCompletedError, OrderNotFoundError):
                self.logger.info(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect True")
                return True
            elif type(err) == ExchangeTemporaryError:
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect False")
                return False
            elif isinstance(err, ExchangeConnectorException):
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, raise unknown api error")
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False
    
    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.critical(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order(o)
        except:
            pass
    
    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))

    async def before_strategy_start(self):
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            await asyncio.sleep(1)
            
        self.direct_subscribe_orderbook(symbol_name=self.symbol, is_incr_depth=True)
    
        symbol_cfg = self.get_symbol_config(self.symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        self.half_step_tick = qty_tick/Decimal(2)
        self.half_price_tick = price_tick/Decimal(2)
        self.price_tick = price_tick
        
        self.api = dict()
        self.order_load_10s = dict()
        self.order_load_daily = dict()
        self.acc_available = dict()
        self.logger.setLevel("WARNING")
        
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
            except:
                pass
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            print(cur_balance,acc)
            self.api[acc] = api
            self.order_load_10s[acc] = 0
            self.order_load_daily[acc] = 0
            self.acc_available[acc] = dict()
            self.acc_available[acc]["btc"] = cur_balance["btc"]["available"]
            self.acc_available[acc]["usdt"] = cur_balance["usdt"]["available"]
            
        c = 0
        while True:
            await asyncio.sleep(1)
            c += 1
            if self.ap:
                l = self.acc_available.values()
                btc = sum(item['btc'] for item in l)*self.ap
                usdt = sum(item['usdt'] for item in l)
                self.starting_cash = btc + usdt
                self.logger.critical(f"starting cash: {self.starting_cash}")
                
                if btc / (btc + usdt) < 0.45:
                    amount = ((btc + usdt) / 2 -btc)
                    qty = amount / self.ap
                    
                    for acc in self.account_names:
                        print(amount)
                        amout_temp = self.acc_available[acc]["usdt"]
                        if amout_temp >= amount:
                            await self.send_order(self.ap*Decimal(1.03),qty,OrderSide.Buy,account_name=acc)
                            break
                        else:
                            await self.send_order(self.ap*Decimal(1.03),self.acc_available[acc]["usdt"]/self.ap/Decimal(1.04),OrderSide.Buy,account_name=acc)
                            amount -= amout_temp
                            qty = amount / self.ap
                elif btc / (btc + usdt) > 0.55:
                    qty = (btc - (btc + usdt) / 2) / self.ap
                    for acc in self.account_names:
                        print(qty)
                        qty_temp = self.acc_available[acc]["btc"]
                        if qty_temp >= qty:
                            await self.send_order(self.ap*Decimal(1 - 0.03),qty,OrderSide.Sell,account_name=acc)
                            break
                        else:
                            await self.send_order(self.ap*Decimal(1 - 0.03),self.acc_available[acc]["btc"],OrderSide.Sell,account_name=acc)
                            qty -= qty_temp
                break
            if c > 1000:
                break
        
        self.starting_btc = 0
        for acc, api in self.api.items():
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data 
            self.starting_btc +=  cur_balance["btc"]["all"]
            print(cur_balance,acc)
        
        self.logger.warning(f"starting btc: {self.starting_btc}")
            
            
    async def on_orderbook(self, symbol, orderbook):
        try:
            self.ap = Decimal(orderbook["asks"][0][0])
            new_depth = Depth.from_dict(depth=orderbook)
            self.executor.update_depth(symbol, new_depth)

            ref_prc = self.ref_prc_cal.calc(self.executor.get_depth(symbol))
            if self.send_order_queue.empty() and not self.sending and self.send:
                self.send_order_queue.put_nowait(ref_prc)
                
            if self.cancel_order_queue.empty() and not self.canceling:
                self.cancel_order_queue.put_nowait(1)
        
        except:
            traceback.print_exc()

    async def on_order(self, order):
        if self.order_cache.get(order.client_id) is None:
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            o = self.order_cache.get(order.client_id)
            try:
                if order.filled_amount > 0:
                    amt_chg = order.filled_amount if o.side == OrderSide.Buy else - order.filled_amount
                    symbol_config = self.get_symbol_config(o.symbol_id)
                    self.executor.update_finished_order(
                        ".".join((symbol_config["exchange_name"],symbol_config["pair"])), 
                        float(amt_chg))
                    
            except Exception as err:
                traceback.print_exc()
                self.logger.critical(f"hanle order err: {err}")
            side = o.side
            acc = o.account_name
            residue = o.requested_amount - order.filled_amount
            if side == OrderSide.Buy:
                self.acc_available[acc]["usdt"] += residue * self.order_cache[order.client_id].requested_price
                self.acc_available[acc]["btc"] += order.filled_amount
            else:
                self.acc_available[acc]["btc"] += residue
                self.acc_available[acc]["usdt"] += order.filled_amount * order.avg_filled_price
                    
            self.order_cache.pop(order.client_id)
            
    
    async def handle_limit(self,headers,acc):
        self.order_load_10s[acc] = int(headers["x-mbx-order-count-10s"])
        self.order_load_daily[acc] = int(headers["x-mbx-order-count-1d"])
        
        self.ip_rate_limit = int(headers["x-mbx-used-weight-1m"])
        if self.ip_rate_limit > 0.9 * 1200:
            self.logger.critical("ip limit")
            self.send = False
            await asyncio.sleep(10)
            self.send = True
    
    async def reduce_count(self):
        while True:
            await asyncio.sleep(0.5)
            acc_list = sorted(self.order_load_10s, key=self.order_load_10s.get)
            self.order_load_10s[acc_list[-1]] -= 1
    
    async def get_balance(self):
        while True:
            await asyncio.sleep(30)
            btc_amount = 0
            usdt_amount = 0
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
                btc_amount += cur_balance["btc"]["all"]
                usdt_amount += cur_balance["usdt"]["all"]
                
            usdt_balance = float(btc_amount*self.ap + usdt_amount - self.starting_btc*self.ap)
            if usdt_balance < 0:
                self.logger.warning(f"btc_amount={btc_amount}, usdt_amount={usdt_amount}, ap={self.ap}, starting cash={self.starting_btc}")
            self.loop.create_task(
                self.push_influx_data(
                    measurement="tt", 
                    tag={"sn":"btc_spot"}, 
                    fields={
                        "usdt_balance":usdt_balance
                        }
                    )
                )
    
    def generate_orders(self, ref_prc):
        return self.executor.get_orders(
            contract=self.contract,
            ref_prc=ref_prc, 
            beta_base=BETA_BASE,
            t=0,
            tau=FREQUENCY,
            T=T
            )
        
    def direction_map(self, side):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell
        
    async def send_order_action(self):
        def send_order_helper(order,acc_list):
            for oid, o in self.order_cache.items():
                if o.side == self.direction_map(order.side) and not self.order_cache[oid].message["cancel"]:
                    if abs(float(o.requested_price) - order.price) / float(o.requested_price) > self.prc_per_diff:
                        self.loop.create_task(self.handle_cancel_order(oid))
                    else:
                        self.order_cache[oid].message["life"] = 100
                    return
            if order.side == Direction.short:
                price = round_decimals_up(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
                for acc in acc_list:
                    if self.acc_available[acc]["btc"] > qty:
                        self.loop.create_task(self.send_order(Decimal(price),Decimal(qty),OrderSide.Sell,account_name=acc))
                        break
                    
            elif order.side == Direction.long:
                price = round_decimals_down(order.price, price_precision[order.iid])
                qty = round(order.quantity, qty_precision[order.iid])
                for acc in acc_list:
                    if self.acc_available[acc]["usdt"] > qty * price:
                        self.loop.create_task(self.send_order(Decimal(price),Decimal(qty),OrderSide.Buy,account_name=acc))
                        break
                            
                            
        while True:
            ref_prc = await self.send_order_queue.get()
            self.sending = True
            orders = self.generate_orders(ref_prc)
            acc_list = sorted(self.order_load_10s, key=self.order_load_10s.get)
            for order in orders:
                send_order_helper(order,acc_list)
                
            self.sending = False
    
    async def handle_cancel_order(self,oid):
        try:
            if self.order_cache.get(oid):
                if oid not in self.canceling_id:
                    self.canceling_id.add(oid)
                    self.order_cache[oid].message["cancel"] = True
                    res = await self.cancel_order(self.order_cache[oid])

                    if self.order_cache.get(oid) and res is False:
                        self.order_cache[oid].message["cancel"] = False
                    self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
            
    async def send_order(self,price,qty,side,account_name=None,order_type=OrderType.Limit,recvWindow=3000):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            order_type=order_type,
            position_side=OrderPositionSide.Open,
            account_name=account_name
        )
        extra_info = dict()
        extra_info["cancel"] = False
        extra_info["life"] = 100
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        
        if side == OrderSide.Buy:
            self.acc_available[account_name]["usdt"] -= price * qty
        else:
            self.acc_available[account_name]["btc"] -= qty
            
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                order_type=order_type,
                client_id=client_id,
                account_name=account_name,
                position_side=OrderPositionSide.Open,
                recvWindow=recvWindow,
            )
            await self.handle_limit(res.headers,account_name)
        except RateLimitException as err:
            self.logger.critical(self.acc_available)
            await self.internal_fail(client_id, err)
        except Exception as err:
            self.logger.critical(f"{err}")
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def cancel_order_action(self):
        while True:
            await self.cancel_order_queue.get()
            self.canceling = True
            for oid, order in self.order_cache.items():
                if not order.message["cancel"] and order.create_ms < time.time()*1e3 - order.message["life"]:
                    self.loop.create_task(self.handle_cancel_order(oid))
            self.canceling = False
    
    async def retreat(self):
        for _ in range(3):
            for api in self.api.values():
                try:
                    await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                except Exception as err:
                    self.logger.critical(f"retreat err:{err}")
            await asyncio.sleep(3)
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    cash = 0
                    for acc in self.account_names:
                        cur_balance = (await self.api[acc].account_balance(self.get_symbol_config(self.symbol))).data
                        cash += cur_balance["btc"]["all"] * self.ap + cur_balance["usdt"]["all"]
                    self.logger.critical(f"ending cash: {cash}")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.update_redis_cache(),
            self.reduce_count(),
            self.get_balance()
            )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
        