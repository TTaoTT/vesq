
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections

from decimal import Decimal 
import asyncio
import time
import numpy as np

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_binance = self.symbol_list[1]
        self.symbol_bit = self.symbol_list[0]
        self.ap_binance = Decimal(0)
        self.ap_bit = Decimal(0)
        self.bp_binance = Decimal(0)
        self.bp_bit = Decimal(0)
        self.mp_binance = Decimal(0)
        self.mp_bit = Decimal(0)
        self.ob_update = False

        self.ap_diff = collections.deque(maxlen=10000)
        self.bp_diff = collections.deque(maxlen=10000)
        self.mp_diff = collections.deque(maxlen=10000)

        self.ap_binance_cache = []
        self.bp_binance_cache = []

    async def before_strategy_start(self):
        # await self._get_symbol_config_from_remote([self.symbol_binance])
        self.direct_subscribe_orderbook(symbol_name=self.symbol_bit)
        self.direct_subscribe_orderbook(symbol_name=self.symbol_binance)
        self.ap_diff.append(0)
        self.bp_diff.append(0)

    # async def on_ticker(self,symbol,ticker):
    #     if self.ap_binance != Decimal(ticker["a"][0]) or self.bp_binance != Decimal(ticker["b"][0]):
    #         self.ap_binance = Decimal(ticker["a"][0])
    #         self.bp_binance = Decimal(ticker["b"][0])

    async def on_orderbook(self, symbol, orderbook):
        if symbol in self.symbol_bit:
            self.ap_bit = orderbook["asks"][0][0]
            self.bp_bit = orderbook["bids"][0][0]
            self.mp_bit = (self.ap_bit + self.bp_bit) / Decimal(2)
            
        elif symbol in self.symbol_binance:
            self.ap_binance = orderbook["asks"][0][0]
            self.bp_binance = orderbook["bids"][0][0]
            self.mp_binance = (self.ap_binance + self.bp_binance) / Decimal(2)

            self.ap_binance_cache.append(self.ap_binance)
            self.bp_binance_cache.append(self.bp_binance)

        else:
            return

        self.ob_update = True
        
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.get_stats(),
            self.set_cache()
        )
    async def get_stats(self):
        while True:
            await asyncio.sleep(0.1)
            if self.ob_update:
                if np.std(self.ap_binance_cache) <= 0.01:
                    self.bp_diff.append(float(self.bp_bit-self.ap_binance))
                if np.std(self.bp_binance_cache) <= 0.01:
                    self.ap_diff.append(float(self.bp_binance-self.ap_bit))
                if np.std(self.ap_binance_cache) <= 0.01 and np.std(self.bp_binance_cache) <= 0.01:
                    self.mp_diff.append(float(self.mp_bit-self.mp_binance))
                self.ob_update = False
            self.ap_binance_cache = []
            self.bp_binance_cache = []

    async def set_cache(self):
        while True:
            await asyncio.sleep(1)
            # api = self.get_exchange_api("bit")[0]
            # api_binance = self.get_exchange_api("binance")[0]
            # balance = (await api.account_balance(self.get_symbol_config(self.symbol_bit))).data
            # equity = balance.get('btc')

            # position = (await api.contract_position (self.get_symbol_config(self.symbol_bit))).data
            # raw = (await api.contract_position (self.get_symbol_config(self.symbol_bit))).json
            # position_binance = (await api_binance.contract_position (self.get_symbol_config(self.symbol_binance))).data
            # cur_balance = await self.get_balance(self.symbol_1)
            # equity = cur_balance.get("btc")
            await self.redis_set_cache(
                {
                "ap_diff":np.median(self.ap_diff),
                "bp_diff":np.median(self.bp_diff),
                "mp_diff":np.median(self.mp_diff),
                "ap_std":np.std(self.ap_diff),
                "bp_std":np.std(self.bp_diff),
                "mp_std":np.std(self.mp_diff),
                "ap_latest":self.ap_diff[-1],
                "bp_latest":self.bp_diff[-1],
                "ap_diff_80":np.quantile(self.ap_diff,0.8),
                "bp_diff_80": np.quantile(self.bp_diff, 0.8),
                # "balance":equity,
                # "bit_position":position,
                # "binance_position":position_binance,
                # "pos_raw":raw
                }
                )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()