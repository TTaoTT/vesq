import json
from concurrent.futures.thread import ThreadPoolExecutor

from strategy_base import *
from web3 import Web3
from web3.middleware import geth_poa_middleware, construct_sign_and_send_raw_middleware

sample = {
    "scan_interval": 2,
    "http_rpc": "xxx",
    "network": "bsc",
    "wallet": "xxx",
    "decimal": 18,
    "jojo_dealer_address": "0xfdsfa",
    "market_contract": {
        "btcusdc": {
            "address": "0xxx",
            "symbol": "jojo.btc_usdc_swap.swap.na"
        },
        "ethusdc": {
            "address": "0xfjsadodfjo",
            "symbol": "jojo.eth_usdc_swap.swap.na"
        }
    }
}


class MyStrategy(CommonStrategy):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.strategy_params = self.config["strategy"]["params"]
        self.heartbeat_order_enabled = False
        self.web3 = Web3(Web3.HTTPProvider(self.strategy_params['http_rpc']))
        self.evt_sig = self.web3.keccak(text="BalanceChange(address,int256,int256)").hex()
        self.liq_contract = dict()
        self.dealer_contract = None
        self.wallet = Web3.toChecksumAddress(self.strategy_params["wallet"])
        self.thread_pool = ThreadPoolExecutor(100)
        self.network = self.strategy_params["network"]  # bsc / polygon / arb
        self.decimal = self.strategy_params["decimal"]

    @staticmethod
    def format_to_decimal(number, decimal_):
        # a * decimal as params -> int
        return int(Decimal(number) * Decimal(pow(10, decimal_)))

    @staticmethod
    def format_from_decimal(number, decimal_):
        # div by decimal, readable number
        return Decimal(number) / Decimal(pow(10, decimal_))

    async def before_strategy_start(self):
        _pass_ = await self.cache_redis.handler.get(f"cache:temp_code:{int(self.wallet, 16)}")
        if not _pass_:
            raise ValueError("launch code not found")
        with open("abi_perpetual.json", "r") as f:
            abi_liq = json.load(f)
        with open("abi.dealer.json", "r") as f:
            abi_dealer = json.load(f)
        self.web3.middleware_onion.add(construct_sign_and_send_raw_middleware(_pass_))
        self.web3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self.dealer_contract = self.web3.eth.contract(address=Web3.toChecksumAddress(self.strategy_params["jojo_dealer_address"]), abi=abi_dealer)
        for k, cfg in self.strategy_params["market_contract"].items():
            self.liq_contract[k] = self.web3.eth.contract(address=Web3.toChecksumAddress(cfg["address"]), abi=abi_liq)
        for cfg in self.strategy_params["market_contract"].values():
            self.direct_subscribe_orderbook(cfg["symbol"], is_testnet=True)

    async def get_risky_accounts(self):
        api = self.get_exchange_api_by_account(self.account_names[0])
        try:
            response = await api.make_request(SWAP, HTTP_GET, "/v1/riskyAccounts")
            return response.json
        except Exception as err:
            if isinstance(err, ExchangeRestApiError):
                self.logger.error(f"get risky account fail: {err.__class__.__name__}")
            else:
                self.logger.error(f"get risky account fail: {err!r}")

    async def scan_forever(self):
        while True:
            await asyncio.sleep(self.strategy_params["scan_interval"])
            temp_accounts = await self.get_risky_accounts()
            if not temp_accounts:
                continue
            for item in temp_accounts:
                if item["isSafe"] is True:
                    continue
                for pos in item["positions"]:
                    try:
                        await self.check_account(item["account"], pos)
                    except Exception as err:
                        self.logger.error(f"check account fail: {err}")

    async def get_mark_price(self, contract_address):
        p = await self.loop.run_in_executor(self.thread_pool, self.dealer_contract.functions.getMarkPrice(contract_address).call)
        return self.format_from_decimal(p, self.decimal)

    async def liq_position(self, account, pos, depth_1):
        market_id = pos["marketId"]
        cfg = self.strategy_params["market_contract"][market_id]
        contract = self.liq_contract[market_id]
        paper_amt_abs_big = self.format_to_decimal(pos["size"], 18)
        credit_amt_big = self.format_to_decimal(Decimal(pos["size"]) * depth_1, self.decimal)
        if pos["side"] == "SHORT":
            paper_amt_abs_big = -paper_amt_abs_big
            credit_amt_big = int(credit_amt_big * 1.02)
        else:
            credit_amt_big = -int(credit_amt_big * 0.98)
        self.logger.info(f"liq params: {account}, ({paper_amt_abs_big}, {credit_amt_big})  depth_1_p={depth_1}")
        r = contract.functions.liquidate(account, paper_amt_abs_big, credit_amt_big).transact(
            {"from": self.wallet, "gasPrice": int(10e9)}
        )
        response = self.web3.eth.wait_for_transaction_receipt(r)
        self.logger.info(f"liq tx {r.hex()} response status = {response['status']}")
        if response["status"] == 1:
            for log in response["logs"]:
                if log["topics"][0].hex() != self.evt_sig:
                    continue
                evt = contract.events.BalanceChange().processLog(log)
                trader = evt["args"]["trader"]
                if trader != self.wallet:
                    continue
                paper_change = self.format_from_decimal(evt["args"]["paperChange"], 18)
                credit_change = self.format_from_decimal(evt["args"]["creditChange"], self.decimal)
                self.logger.info(f"{market_id} paper={paper_change} credit={credit_change}")
                if paper_change > 0:  # long, need to sell
                    amt = paper_change
                    side = OrderSide.Sell
                else:
                    amt = abs(paper_change)
                    side = OrderSide.Buy
                await self.on_liq_order(cfg["symbol"], amt, side, r.hex())

    async def check_account(self, account, pos):
        if pos["status"] != "OPEN":
            return
        if pos["marketId"] not in self.strategy_params["market_contract"]:
            self.logger.warning(f"{pos['marketId']} not support")
            return
        contract_cfg = self.strategy_params["market_contract"][pos["marketId"]]
        liq_price = Decimal(pos["liquidationPrice"])
        pos_side = pos["side"]
        mark_price = await self.get_mark_price(contract_cfg["address"])
        orderbook = self.get_orderbook_data(contract_cfg["symbol"])
        if not orderbook:
            return
        if pos_side == "LONG":
            if len(orderbook["bids"]) == 0:
                return
            if mark_price < liq_price:
                self.logger.info(f"{account} mark_price={mark_price} {pos}")
                await self.liq_position(account, pos, orderbook["bids"][0][0])
        else:
            if len(orderbook["asks"]) == 0:
                return
            if mark_price > liq_price:
                self.logger.info(f"{account} mark_price={mark_price} {pos}")
                await self.liq_position(account, pos, orderbook["asks"][0][0])

    async def get_account_info(self):
        """
        :return:
        {
            "availableCreditAmounts": {                     // available buy/sell credit amount in quote unit
                "btcusdc": {
                    "buy": "36080.4119586",
                    "sell": "26361.5246814"
                }
            },
            "availableMargin": "1318.07623407",             // available margin in quote unit
            "exposure": "4935.443638",                      // exposure in quote unit
            "frozenMargin": "1.1",                          // frozen margin in quote unit
            "isSafe": true,                                 // the current account is safe or not
            "leverage": "3.144098",                         // current leverage
            "marginRate": "0.318056",                       // current margin rate
            "netValue": "1569.748416",                      // net value in quote unit
            "pendingWithdrawPrimaryCreditAmount": "0",      // pending withdraw primary credit amount in quote unit
            "pendingWithdrawSecondaryCreditAmount": "0",    // pending withdraw secondary credit amount in quote unit
            "perpetualBalances": {                          // balances snapshot on the blockchain
                "btcusdc": {
                    "creditAmount": "4801.665755",
                    "paperAmount": "-0.2",
                    "serialNumber": 3
                }
            },
            "positionMargin": "250.57218193",               // position margin in quote unit
            "primaryCreditAmount": "1620.621099",           // primary credit amount in quote unit
            "secondaryCreditAmount": "0"                    // secondary credit amount in quote unit
        }
        """
        api = self.get_exchange_api_by_account(self.account_names[0])
        response = await api.account_balance_all(SWAP, NA)
        return response.json["balances"]

    async def on_liq_order(self, symbol, amount, side, extra_info):
        await self.simple_compulsory_order(symbol, amount, side, OrderPositionSide.Open, extra_info=extra_info)

    async def strategy_core(self):
        await self.scan_forever()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    t = MyStrategy(loop)
    t.run_forever()
