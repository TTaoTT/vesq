import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.trade_cache_buy_1s = list()
        self.trade_cache_sell_1s = list()
        self.trade_cache_buy_1s_large = list()
        self.trade_cache_sell_1s_large = list()

        self.order_cache = dict()
        
        self.pos = Decimal(0)
        
        self.mp = None
        self.spread = Decimal(0)
        self.ap = Decimal(0)
        self.bp = Decimal(0)
        
        self.heartbeat_order_enabled = False

        self.send_order = True
        self.order_storage_enabled = False
        self.last_tick = 0

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])

        symbol = self.params["symbol"]
        self.symbol_1 = f"binance.{symbol}_usdt_swap.usdt_contract.na"

        self.t_1 = Decimal(0)

        self.strategy_start_time = int(time.time()*1e3)
        
        self._order_to_store = Queue()
        self._pos_change = Queue()
        self._send_order = Queue()
        self._cancel_order = Queue()
        
        self.order_total_second = 0
        self.order_total_number = 0
        
        self.ask = Decimal(0)
        self.bid = Decimal(0)
        
        self.canceling_id = set()
        
    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        await self.direct_subscribe_bbo(symbol_name=self.symbol_1)
        self.direct_subscribe_public_trade(symbol_name=self.symbol_1)
        self.direct_subscribe_order_update(symbol_name=self.symbol_1)
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()
        
        self.loop.create_task(self.store_order())
            
    async def on_bbo(self, symbol, bbo):
        self.ap = Decimal(bbo.ask[0])
        self.bp = Decimal(bbo.bid[0])
        self.mp = (self.ap + self.bp)/Decimal(2)
        self.spread = self.ap - self.bp
        if self._send_order.empty() and not self.order_cache:
            self._send_order.put_nowait(1)
        if self._cancel_order.empty() and self.order_cache:
            self._cancel_order.put_nowait(1)
            
    async def on_public_trade(self, symbol, trade: PublicTrade):
        self.last_trade_ts = trade.server_ms
        if trade.side == OrderSide.Buy:
            self.trade_cache_buy_1s.append(float(trade.price))

        else:
            self.trade_cache_sell_1s.append(float(trade.price))

    async def calculate_t_1(self):
        while True:
            await asyncio.sleep(1)
            if self.trade_cache_buy_1s and self.trade_cache_buy_1s[0]!=0:
                buy_imp = (max(self.trade_cache_buy_1s) - self.trade_cache_buy_1s[0]) / self.trade_cache_buy_1s[0]
            else:
                buy_imp = 0

            if self.trade_cache_sell_1s and self.trade_cache_sell_1s[0]!=0:
                sell_imp = (self.trade_cache_sell_1s[0] - min(self.trade_cache_sell_1s)) / self.trade_cache_sell_1s[0]
            else:
                sell_imp = 0

            self.trade_cache_buy_1s = []
            self.trade_cache_sell_1s = []

            self.trade_cache_buy_1s_large.append(buy_imp)
            self.trade_cache_sell_1s_large.append(sell_imp)
            if len(self.trade_cache_buy_1s_large) >= 201:
                self.trade_cache_buy_1s_large.pop(0)
            if len(self.trade_cache_sell_1s_large) >= 201:
                self.trade_cache_sell_1s_large.pop(0)

            t_buy = np.max(self.trade_cache_buy_1s_large)
            t_sell = np.max(self.trade_cache_sell_1s_large)

            self.t_1 = Decimal(max(t_buy,t_sell))
    
    def handle_pos(self, client_id, order):
        amount_changed = order.filled_amount - self.order_cache[client_id].filled_amount
        if not amount_changed:
            return
        if self.order_cache[client_id].side == OrderSide.Buy:
            self.pos += amount_changed
        else:
            self.pos -= amount_changed
        self.logger.warning("position is changed")
        self._pos_change.put_nowait(1)

    async def on_order(self, order: PartialOrder):
        client_id = order.client_id
        if client_id not in list(self.order_cache): 
            return
        
        if order.filled_amount < self.order_cache[client_id].filled_amount:
            return

        self.handle_pos(client_id,order)
        self.order_cache[client_id].filled_amount = order.filled_amount
        self.order_cache[client_id].avg_filled_price = order.avg_filled_price
        self.order_cache[client_id].xchg_id = order.xchg_id
        self.order_cache[client_id].xchg_status = order.xchg_status
        extra_info = self.order_cache[client_id].message
        self.order_cache[client_id].extra_info = f"{'c' if extra_info['source'] == 0 else 'e'}_{extra_info['stop_ts']}"
        tag_info = int(self.params["amount"])
        self.order_cache[client_id].tag = f"{tag_info}"
        if order.xchg_status == OrderStatus.Filled or order.xchg_status == OrderStatus.Failed:
            self.logger.warning(f"...............{order.client_id}..{order.xchg_status}")
        if order.xchg_status in OrderStatus.fin_status():
            if order.xchg_status == OrderStatus.Filled or order.xchg_status == OrderStatus.Failed:
                self.logger.warning(f"...............{order.xchg_status}")
            self.order_total_second += (order.server_ms - self.order_cache[client_id].create_ms) / 1000
            self.order_total_number += 1
            if order.filled_amount > 0:
                o = self.order_cache[client_id]
                o.message = ""
                self._order_to_store.put_nowait(o)
            self.order_cache.pop(client_id)

    async def store_order(self):
        while True:
            order = await self._order_to_store.get()
            await self.post_order_to_system(order, self.session)
            
    async def strategy_core(self):
        await asyncio.sleep(30)
        await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.update_redis_cache(),
            self.calculate_t_1(),
            self.check_position(),
            self.instant_cancel_order(),
            self.handle_pos_change()
        )


    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def execute_order(self, params):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(*params[:3]):
            return
        self.order_cache[client_id] = self.gen_async_order(
            client_id=client_id,
            symbol_name=params[0],
            price=params[1],
            volume=params[2],
            side=params[3],
            position_side=params[4],
            order_type=params[5]
        )
        extra_info = dict()
        extra_info["stop_ts"] = params[6]
        extra_info["cancel"] = 0
        extra_info["source"] =  params[7] # 1 for entry, 0 for close
        extra_info["query"] =  0
        self.order_cache[client_id].message = extra_info
        
        try:
            await self.make_order(
                symbol_name=params[0],
                price=params[1],
                volume=params[2],
                side=params[3],
                position_side=params[4],
                order_type=params[5],
                client_id=client_id
            )
        except InsufficientBalanceError:
            o = self.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order(o)
        except ReduceOnlyOrderFail:
            o = self.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order(o)
        except Exception as err:
            self.logger.warning(f"send order error:{err}")
    
    async def send_order_action(self):
        while True:
            await self._send_order.get()
            sell_p = self.mp * (Decimal(1) + self.params["gap"]/Decimal(2) - Decimal(0.0001))
            buy_p = self.mp * (Decimal(1) - self.params["gap"]/Decimal(2) + Decimal(0.0001))
            if self.t_1<self.params["gap"]/Decimal(8) and self.send_order and not self.order_cache:
                try:
                    asyncio.gather(
                        self.execute_order([self.symbol_1, buy_p, self.params["amount"], OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,20,1]),
                        self.execute_order([self.symbol_1, sell_p, self.params["amount"], OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,20,1])
                    )
                    self.ask = sell_p
                    self.bid = buy_p
                except Exception as err:
                    self.logger.warning(f"send order err, {err}")
                    
    async def batch_cancel(self, oid, order):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                await self.cancel_order(order)
                self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
    
    async def instant_cancel_order(self):
        while True:
            await self._cancel_order.get()
            source = [self.order_cache[key].message["source"] for key in list(self.order_cache)]
            
            if sum(source) == 1:
                asyncio.gather(*[self.batch_cancel(oid, order) for oid, order in self.order_cache.items() if order.message["source"]==1])
            elif sum(source) == 2:
                if (self.ask-self.ap)/self.ap < self.params["gap"]/Decimal(3) or (self.bp-self.bid)/self.bp < self.params["gap"]/Decimal(3):
                    asyncio.gather(*[self.batch_cancel(oid, order) for oid, order in self.order_cache.items()])

    async def check_position(self):
        while True:
            await asyncio.sleep(60)
            try:
                api = self.get_exchange_api("binance")[0]
                position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos != self.pos:
                    self.logger.warning(
                    f"""
                    =========update wrong position==========
                    origin: {self.pos}
                    update: {pos}
                    """
                )
                    self.pos = pos
                if pos > Decimal(0):
                    await self.execute_order([self.symbol_1, self.mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,1,0])  
                elif pos < Decimal(0):
                    await self.execute_order([self.symbol_1, self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,1,0]) 

            except Exception as err:
                self.logger.warning(f'check position err {err}')
                
    async def handle_pos_change(self):
        while True:
            await self._pos_change.get()
            self.logger.warning("start to handle position change")
            try:
                api = self.get_exchange_api("binance")[0]
                position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                pos = position.data["long_qty"] - position.data["short_qty"]
                self.logger.warning(f"current position:{pos}")
                if pos > Decimal(0):
                    await self.execute_order([self.symbol_1, self.mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,1,0])
                elif pos < Decimal(0):
                    await self.execute_order([self.symbol_1, self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,1,0])
            except Exception as err:
                self.logger.warning(f'handle position err {err}')
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            res = (await api.order_match_result(self.get_symbol_config(order.symbol_id), order_id=None, client_id=order.client_id)).data
            if res:
                _order = res
            else:
                _order = None
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*0.2 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].message["query"] += 1
                    if order_new and order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order(order_new)
                    elif self.order_cache[oid].message["query"]>10:
                        self.logger.warning(f"order pop without actually knowing what happend,client_id:{oid}")
                        api = self.get_exchange_api_by_account(order.account_name)
                        await api.flash_cancel_orders(self.get_symbol_config(order.symbol_id))
                        
                        self.order_cache.pop(oid)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send_order = False
                    for i in range(3):
                        await asyncio.gather(
                            *[self.batch_cancel(oid, order) for oid, order in self.order_cache.items()]
                            )
                        api = self.get_exchange_api("binance")[0]
                        position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                        pos = position.data["long_qty"] - position.data["short_qty"]
                        if pos > 0:
                            await self.execute_order([self.symbol_1, self.mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,1,0])
                        else:
                            await self.execute_order([self.symbol_1, self.mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,1,0])
                        await asyncio.sleep(3)
                    await self.redis_set_cache({})
                    
                    self.logger.warning(f"manually exiting")
                    exit()
                        
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        async def update_cache():
            _dict = dict()
            avg_order_second = self.order_total_second/self.order_total_number if self.order_total_number else None
            _dict = {
                "exit": None,
                "current position": self.pos,
                "t_1":self.t_1,
                "amount": self.params["amount"],
                "avg_order_second":avg_order_second
            }
            try:
                await self.redis_set_cache(_dict)
            except Exception as err:
                self.logger.warning(f"set redis cache failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            await update_cache()

    async def cancel_order_action(self):
        
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
                
        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()