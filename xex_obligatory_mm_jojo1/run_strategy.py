from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "preprod")
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2
from atom.helpers import send_ding_talk
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard
from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.strategy_configs.obligatory_mm_config import ObligatoryMmConfig
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_inventory_executor import XexMmDeltaNeutralInventoryExecutor
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig, HeartBeatTolerance
from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor
from xex_mm.xex_depth.xex_depth import XExDepth

from strategy_base.base import CommonStrategy
from atom.model import OrderSide, PartialOrder, OrderStatus, BBODepth, OrderPositionSide, OrderType
from atom.model.depth import Depth as AtomDepth
from atom.model.order import Order as AtomOrder

from atom.helpers import ClientIDGenerator
from asyncio.queues import Queue, LifoQueue
import uuid
from orderedset import OrderedSet
import ujson as json
from typing import Set, AnyStr, Union, Dict, List, Tuple
import math
import traceback
import numpy as np

import time
from decimal import Decimal
import asyncio
import logging


from xex_mm.utils.base import Depth, BBO, Direction, Order, TransOrder, OrderManager, MakerTaker


class XexMmDeltaNeutralExecutor:
    def __init__(self, contract: str, profit_margin_return: float, logger):
        super(XexMmDeltaNeutralExecutor, self).__init__()
        self.contract = contract
        self.profit_margin_return = profit_margin_return

        self._xex_depth: XExDepth = ...
        self._prev_order_map: Dict = {}

        self._fee_map = {}

        self._numerical_tol = 1e-10

        self.logger = logger

    def update_fee_rate(self, contract: str, ex: str, takers_fee: float, makers_fee: float):
        self._fee_map[(contract, ex)] = (takers_fee, makers_fee)

    def get_fee_rate(self, contract: str, ex: str):
        return self._fee_map[(contract, ex)]

    def update_xex_depth(self, xex_depth: XExDepth):
        assert self.contract == xex_depth.contract
        self._xex_depth = xex_depth

    def hedge_increment(self, inc_fill_qty_in_tokens: float, prc: float, maker_fee: float, d: int) -> Tuple[float, List[TransOrder]]:
        if self._xex_depth is Ellipsis:
            self.logger.info(f"self._xex_depth is Ellipsis, no hedge")
            return inc_fill_qty_in_tokens, []

        if len(self._xex_depth.get_ex_names()) == 0:
            self.logger.info(f"self._xex_depth is empty, no hedge")
            return inc_fill_qty_in_tokens, []

        post_fee_xex_depth: Depth = self._xex_depth.get_post_taker_fee_view().get_xex_depth()

        if d == Direction.long:  # long, hit bid to short hedge
            self.logger.info(
                f"maker_fee={maker_fee},prc={prc},pmr={self.profit_margin_return}")
            post_fee_long_prc = prc * (1 + maker_fee)
            post_fee_bid_hedge_prc = post_fee_long_prc * \
                (1 + self.profit_margin_return)
            remaining_qty_in_tokens = inc_fill_qty_in_tokens
            post_fee_bid_quote_prc = np.nan
            for bid_lvl in post_fee_xex_depth.bids:
                bid_prc = bid_lvl.prc
                self.logger.info(
                    f"bid_prc={bid_prc},post_fee_bid_hedge_prc={post_fee_bid_hedge_prc}")
                if bid_prc > post_fee_bid_hedge_prc - self._numerical_tol:
                    post_fee_bid_quote_prc = bid_prc
                    remaining_qty_in_tokens -= bid_lvl.qty
                    if remaining_qty_in_tokens < 0 + self._numerical_tol:
                        break
                    continue
                break
            if np.isnan(post_fee_bid_quote_prc):
                self.logger.info(f"no profitable price to hit, no hedge")
                return inc_fill_qty_in_tokens, []

            total_quoted_qty_in_tokens = 0
            remaining_qty_in_tokens = inc_fill_qty_in_tokens
            orders = []
            for hex in self._xex_depth.get_ex_names():
                hex_taker_fee, hex_maker_fee = self._fee_map[(
                    self.contract, hex)]
                hex_iid = f"{hex}.{self.contract}"
                hex_depth = self._xex_depth.get_depth_for_ex(ex=hex)
                quote_qty_in_tokens = 0
                for bid_lvl in hex_depth.bids:
                    if bid_lvl.prc * (1 - hex_taker_fee) > post_fee_bid_quote_prc - self._numerical_tol:
                        quote_qty_in_tokens += bid_lvl.qty
                        remaining_qty_in_tokens -= bid_lvl.qty
                        if remaining_qty_in_tokens < 0:
                            quote_qty_in_tokens += remaining_qty_in_tokens
                            remaining_qty_in_tokens += (-remaining_qty_in_tokens)
                            break
                        continue
                    break
                if quote_qty_in_tokens > 0:
                    trans_order = TransOrder(
                        side=Direction.short,
                        p=post_fee_bid_quote_prc / (1 - hex_taker_fee),
                        q=quote_qty_in_tokens,
                        iid=hex_iid,
                        floor=np.nan,
                        delta=np.nan,
                        life=1,
                        maker_taker=MakerTaker.taker,
                    )
                    total_quoted_qty_in_tokens += trans_order.quantity
                    assert total_quoted_qty_in_tokens < inc_fill_qty_in_tokens + self._numerical_tol
                    orders.append(trans_order)
            if total_quoted_qty_in_tokens > inc_fill_qty_in_tokens + self._numerical_tol:
                raise RuntimeError(
                    f"total_quoted_qty_in_tokens({total_quoted_qty_in_tokens}) > inc_fill_qty({inc_fill_qty_in_tokens})")
            resid_qty_in_tokens = inc_fill_qty_in_tokens - total_quoted_qty_in_tokens
            self.logger.info(f"Hedged!")
            return resid_qty_in_tokens, orders

        # short, hit ask to long hedge
        self.logger.info(
            f"maker_fee={maker_fee},prc={prc},pmr={self.profit_margin_return}")
        post_fee_short_prc = prc * (1 - maker_fee)
        post_fee_ask_hedge_prc = post_fee_short_prc * \
            (1 - self.profit_margin_return)
        remaining_qty_in_tokens = inc_fill_qty_in_tokens
        post_fee_ask_quote_prc = np.nan
        for ask_lvl in post_fee_xex_depth.asks:
            ask_prc = ask_lvl.prc
            self.logger.info(
                f"ask_prc={ask_prc},post_fee_ask_hedge_prc={post_fee_ask_hedge_prc}")
            if ask_prc < post_fee_ask_hedge_prc + self._numerical_tol:
                post_fee_ask_quote_prc = ask_prc
                remaining_qty_in_tokens -= ask_lvl.qty
                if remaining_qty_in_tokens < 0 + self._numerical_tol:
                    break
                continue
            break
        if math.isnan(post_fee_ask_quote_prc):
            self.logger.info(f"no profitable price to hit, no hedge")
            return inc_fill_qty_in_tokens, []

        total_quoted_qty_in_tokens = 0
        remaining_qty_in_tokens = inc_fill_qty_in_tokens
        orders = []
        for hex in self._xex_depth.get_ex_names():
            hex_taker_fee, hex_maker_fee = self._fee_map[(self.contract, hex)]
            hex_iid = f"{hex}.{self.contract}"
            hex_depth = self._xex_depth.get_depth_for_ex(ex=hex)
            quote_qty_in_tokens = 0
            for ask_lvl in hex_depth.asks:
                if ask_lvl.prc * (1 + hex_taker_fee) < post_fee_ask_quote_prc + self._numerical_tol:
                    quote_qty_in_tokens += ask_lvl.qty
                    remaining_qty_in_tokens -= ask_lvl.qty
                    if remaining_qty_in_tokens < 0:
                        quote_qty_in_tokens += remaining_qty_in_tokens
                        remaining_qty_in_tokens += (-remaining_qty_in_tokens)
                        break
                    continue
                break
            if quote_qty_in_tokens > 0:
                trans_order = TransOrder(
                    side=Direction.long,
                    p=post_fee_ask_quote_prc / (1 + hex_taker_fee),
                    q=quote_qty_in_tokens,
                    iid=hex_iid,
                    floor=np.nan,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.taker,
                )
                total_quoted_qty_in_tokens += trans_order.quantity
                assert total_quoted_qty_in_tokens < inc_fill_qty_in_tokens + self._numerical_tol
                orders.append(trans_order)
        if total_quoted_qty_in_tokens > inc_fill_qty_in_tokens + self._numerical_tol:
            raise RuntimeError(
                f"total_quoted_qty_in_tokens({total_quoted_qty_in_tokens}) > inc_fill_qty({inc_fill_qty_in_tokens})")
        resid_qty_in_tokens = inc_fill_qty_in_tokens - total_quoted_qty_in_tokens
        self.logger.info(f"Hedged!")
        return resid_qty_in_tokens, orders

    def get_inc_fill_qty_in_tokens(self, order: Order) -> float:
        assert order.filled_qty >= 0
        oid = order.order_id
        if oid in self._prev_order_map:
            prev_order: Order = self._prev_order_map[oid]
            if order.filled_qty < prev_order.filled_qty:
                return 0
            inc_fill_qty_in_tokens = max(
                order.filled_qty - prev_order.filled_qty, 0)
            return inc_fill_qty_in_tokens
        return order.filled_qty

    def update_order(self, order: Order) -> Tuple[float, List[TransOrder]]:
        oid = order.order_id
        self.logger.info(
            f"ce={(order.contract, order.ex)},fee_map={self._fee_map}")
        taker_fee, maker_fee = self._fee_map[(order.contract, order.ex)]

        if oid in self._prev_order_map:
            prev_order: Order = self._prev_order_map[oid]
            if order.filled_qty < prev_order.filled_qty:
                self.logger.info(
                    f"Backwards fill qty, curr={order.filled_qty}, prev={prev_order.filled_qty}, no hedge")
                return 0, []
            inc_fill_qty_in_tokens = order.filled_qty - prev_order.filled_qty
            if inc_fill_qty_in_tokens < self._numerical_tol:
                self._prev_order_map[oid] = order
                self.logger.info(
                    f"Zero inc fill qty={inc_fill_qty_in_tokens}, no hedge")
                return inc_fill_qty_in_tokens, []
            resid_qty_in_tokens, orders_to_place = self.hedge_increment(
                inc_fill_qty_in_tokens=inc_fill_qty_in_tokens, prc=order.avg_fill_prc, maker_fee=maker_fee, d=order.side)
            self._prev_order_map[oid] = order
            return resid_qty_in_tokens, orders_to_place

        inc_fill_qty_in_tokens = order.filled_qty
        if inc_fill_qty_in_tokens < self._numerical_tol:
            self._prev_order_map[oid] = order
            self.logger.info(
                f"Zero inc fill qty={inc_fill_qty_in_tokens}, no hedge")
            return inc_fill_qty_in_tokens, []
        resid_qty_in_tokens, orders_to_place = self.hedge_increment(
            inc_fill_qty_in_tokens=inc_fill_qty_in_tokens, prc=order.avg_fill_prc, maker_fee=maker_fee, d=order.side)
        self._prev_order_map[oid] = order
        return resid_qty_in_tokens, orders_to_place

    def remove_order(self, oid: str):
        if oid not in self._prev_order_map:
            return
        self._prev_order_map.pop(oid)


class ClientIDGenerator(ClientIDGenerator):
    @classmethod
    def gen_jojo(cls, **kwargs):
        return uuid.uuid4().hex

    @classmethod
    def gen_jojo18(cls, **kwargs):
        return uuid.uuid4().hex


class ObligatoryExecutor(ObligatoryExecutor):
    def gen_quotes(self, ts: int) -> List[TransOrder]:
        self.on_wakeup(ts=ts)
        orders = []
        if not self._xex_depth.contains("binance"):
            return []
        ref_prc = self._ref_prc_calculator.calc_from_depth(
            depth=self._xex_depth.get_depth_for_ex("binance"), amt_bnd=0)
        for i, (ret, q_ccy2, _) in enumerate(self._get_requirements()):
            ask_prc = ref_prc * (1 + ret)
            bid_prc = ref_prc * (1 - ret)
            q_ccy1 = q_ccy2 / ask_prc
            # l_r = self._get_hit_anomaly_ratio(d=Direction.long, i=i, ts=ts)
            # s_r = self._get_hit_anomaly_ratio(d=Direction.short, i=i, ts=ts)
            # if math.isnan(l_r) or (l_r < 2):
            orders.append(TransOrder(
                side=Direction.short,
                p=ask_prc,
                q=q_ccy1,
                iid=self.iid,
                floor=i,
                delta=np.nan,
                life=1,
                maker_taker=MakerTaker.maker,
            ))
            # if math.isnan(s_r) or (s_r < 2):
            orders.append(TransOrder(
                side=Direction.long,
                p=bid_prc,
                q=q_ccy1,
                iid=self.iid,
                floor=i,
                delta=np.nan,
                life=1,
                maker_taker=MakerTaker.maker,
            ))
        return orders

    def remove_order(self, oid: str):
        if self._prev_order_map.get(oid) != None:
            self._prev_order_map.pop(oid)


class ClientIDGenerator(ClientIDGenerator):
    @classmethod
    def gen_jojo(cls, **kwargs):
        return uuid.uuid4().hex


def round_decimals_up(number: float, decimals: int = 2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor


def round_decimals_down(number: float, decimals: int = 2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.ob_update = False
        self.start_ts = time.time()
        self.oid_to_cid = dict()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.ex_maker = self.config['strategy']['params']['ex_maker']
        self.bnd_amt_in_coin = Decimal(
            self.config['strategy']['params']['bnd_amt_in_coin'])
        self.max_inventory = float(
            self.config['strategy']['params']['max_inventory'])
        self.unknown_order_dict = dict()
        self.counter = 0
        self.filled_counter = 0

        self.cfg = ObligatoryMmConfig(
            version="",
            contract=self.contract,
            ex=self.ex_maker,
            hedge_exs=["binance"],
            profit_margin_return=-np.inf,
            cancel_tolerance=1e-5,
            requirements=[
                (0.001, 20000, 1e-4),
                (0.002, 40000, 2e-4),
                (0.003, 40000, 3e-4),
                (0.004, 40000, 4e-4),
                (0.005, 60000, 5e-4),
                (0.006, 5000, 6e-4),
                (0.007, 10000, 7e-4),
                (0.008, 15000, 8e-4),
                (0.009, 20000, 9e-4),
                (0.01, 25000, 10e-4),
                (0.011, 30000, 11e-4),
                (0.012, 35000, 12e-4),
                (0.013, 40000, 13e-4),
                (0.014, 45000, 14e-4),
                (0.015, 50000, 15e-4),
                (0.016, 55000, 16e-4),
                (0.017, 60000, 17e-4),
                (0.018, 65000, 18e-4),
                (0.019, 70000, 19e-4),
                (0.02, 75000, 20e-4),
            ],
        )

        self._obligatory_order_manager = OrderManager()
        self._hedge_order_manager = OrderManager()
        self._inventory_order_manager = OrderManager()
        self._rebalance_order_manager = OrderManager()
        self._xex_depth = XExDepth(contract=self.cfg.contract)

        self._heart_beat_tol_cfg = HeartBeatTolerance()
        self._last_update_time = {}
        self._state = {}

        self._numeric_tol = 1e-10

        self._obligatory_executor = self.build_obligatory_executor()
        self._delta_neutral_executor = XexMmDeltaNeutralExecutor(contract=self.cfg.contract,
                                                                 profit_margin_return=self.cfg.profit_margin_return,
                                                                 logger=self.logger)
        self._inventory_executor = XexMmDeltaNeutralInventoryExecutor(
            contract=self.cfg.contract)
        for ex in [self.cfg.ex] + self.cfg.hedge_exs:
            makers_fee, takers_fee = FeeConfig().get_fee(ex=ex, contract=self.cfg.contract)
            self._delta_neutral_executor.update_fee_rate(contract=self.cfg.contract, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)
            self._inventory_executor.update_fee_rate(contract=self.cfg.contract, ex=ex,
                                                     makers_fee=makers_fee, takers_fee=takers_fee)
        self.logger.info(f"fee_map={self._delta_neutral_executor._fee_map}")

        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()

        self._hex_set = set(self.cfg.hedge_exs)
        self._inactive_hexs: Set = self._hex_set.copy()
        self._active_hexs: Set = set()
        self._inventory: float = 0
        self._obl_inventory: float = 0
        self._hedge_inventory: float = 0

        self._order_stats = dict()
        self._order_stats["buy_amt"] = 0
        self._order_stats["buy_qty"] = 0
        self._order_stats["sell_amt"] = 0
        self._order_stats["sell_qty"] = 0

        self.canceling_id = set()
        self.quering_id = set()

        self.send = False

        self.trd_amt = 0

        self.taker_book_queue = LifoQueue()
        self.maker_book_queue = LifoQueue()
        self.bbo_queue = LifoQueue()
        self.order_queue = Queue()

    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))

    def is_oex(self, ex: str):
        return ex == self.cfg.ex

    def is_hex(self, ex: str):
        return ex in self._hex_set

    def activate_oex(self, depth: Depth):
        ex = depth.meta_data.ex
        assert self.is_oex(ex=ex)
        self._xex_depth.update_depth_for_ex(depth)
        self.logger.info(f"Activating obligatory ex={ex}")

    def deactivate_oex(self):
        ex = self.cfg.ex
        self._xex_depth.remove_ex(ex=ex)
        self.logger.info(f"Deactivating obligatory ex={ex}")

    def activate_hex(self, depth: Depth):
        ex = depth.meta_data.ex
        assert self.is_hex(ex=ex)
        self._xex_depth.update_depth_for_ex(depth)
        self._inactive_hexs.remove(ex)
        self._active_hexs.add(ex)
        self.logger.info(f"Activating hedge ex={ex}")

    def deactivate_hex(self, ex: str):
        assert ex in self._hex_set
        self._xex_depth.remove_ex(ex=ex)
        self._inactive_hexs.add(ex)
        self._active_hexs.remove(ex)
        self.logger.info(f"Deactivating hedge ex={ex}")

    def build_obligatory_executor(self):
        """ Override this for different exchange / contracts with different MM requirements. """

        oe = ObligatoryExecutor(ex=self.cfg.ex, contract=self.cfg.contract)
        oe.update_requirements(requirements=self.cfg.requirements)
        return oe

    def update_inventory(self, chg_qty: float, key: str, prc: float):
        self.logger.info(f"curr inventory:{self._inventory}, update inventory: {chg_qty}, from:{key}")
        self._inventory += chg_qty
        if key == "obl":
            self._obl_inventory += chg_qty
        elif key == "hedge":
            self._hedge_inventory += chg_qty

        self.trd_amt += abs(chg_qty) * prc

    def direction_map(self, side: Direction):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell

    def cancel_obligatory_orders(self):
        for oid in self._obligatory_order_manager.keys():
            if self._obligatory_order_manager.get_trans_order(oid=oid):
                o = self._obligatory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(
                        oid=oid, order=o, order_manager=self._obligatory_order_manager))

    def cancel_all_hedge_orders(self):
        for oid in self._hedge_order_manager.keys():
            if self._hedge_order_manager.get_trans_order(oid=oid):
                o = self._hedge_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(
                        oid=oid, order=o, order_manager=self._hedge_order_manager))

    def cancel_hedge_orders_for_ex(self, ex: str):
        iid = f"{ex}.{self.cfg.contract}"
        for oid in self._hedge_order_manager.get_oids_for_iid(iid=iid):
            o = self._hedge_order_manager.trans_order_at(oid=oid)
            if not o.cancel:
                self.loop.create_task(self.handle_cancel_order(
                    oid=oid, order=o, order_manager=self._hedge_order_manager))

    def cancel_all_inventory_orders(self):
        for oid in self._inventory_order_manager.keys():
            if self._inventory_order_manager.get_trans_order(oid=oid):
                o = self._inventory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(
                        oid=oid, order=o, order_manager=self._inventory_order_manager))

    def cancel_inventory_orders_for_ex(self, ex: str):
        iid = f"{ex}.{self.cfg.contract}"
        for oid in list(self._inventory_order_manager.get_oids_for_iid(iid=iid)):
            if self._inventory_order_manager.get_trans_order(oid=oid):
                o = self._inventory_order_manager.trans_order_at(oid=oid)
                if not o.cancel:
                    self.loop.create_task(self.handle_cancel_order(
                        oid=oid, order=o, order_manager=self._inventory_order_manager))

    async def before_strategy_start(self):
        self.ex_pair_set = set()
        self.logger.setLevel("CRITICAL")
        maker_symbol = f"{self.cfg.ex}.{self.cfg.contract}.{self.market}.{self.sub_market}"
        if self.cfg.ex == "okex" and self.market == "usdt_contract":
            maker_symbol = f"{self.cfg.ex}.{self.cfg.contract}.swap.cross"
        if self.cfg.ex.startswith("jojo") and self.market == "usdt_contract":
            ccy1, _, _ = self.cfg.contract.split("_")
            maker_symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
        maker_config = self.get_symbol_config(symbol_identity=maker_symbol)
        self.maker_qty_tick = maker_config["qty_tick_size"]
        self.maker_contract_value = maker_config["contract_value"]
        bnd = self.bnd_amt_in_coin / maker_config["contract_value"]
        self.ex_pair_set.add(maker_config["exchange_pair"])
        self.direct_subscribe_order_update(symbol_name=maker_symbol)
        self.direct_subscribe_orderbook(
            symbol_name=maker_symbol, is_incr_depth=True, depth_min_v=bnd, is_testnet=True)

        self.taker_config = dict()
        for ex in self.cfg.hedge_exs:
            symbol = f"{ex}.{self.cfg.contract}.{self.market}.{self.sub_market}"
            if ex == "okex" and self.market == "usdt_contract":
                symbol = f"{ex}.{self.cfg.contract}.swap.cross"
            if ex.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = self.cfg.contract.split("_")
                symbol = f"{ex}.{ccy1}_usdc_swap.swap.na"
            taker_config = self.get_symbol_config(symbol_identity=symbol)
            self.ex_pair_set.add(taker_config["exchange_pair"])
            self.taker_config[ex] = taker_config
            bnd = self.bnd_amt_in_coin / taker_config["contract_value"]
            self.direct_subscribe_order_update(symbol_name=symbol)
            self.direct_subscribe_orderbook(
                symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_bbo(symbol_name=symbol)

        self.loop.create_task(self.handle_ws_update())

        counter = 0
        while True:
            if counter > 1000:
                self.logger.critical("no orderbook comming")
                exit()
            counter += 1
            ref_prc = self._obligatory_executor._ref_prc
            if ref_prc is Ellipsis:
                await asyncio.sleep(1)
                continue
            else:
                await self.rebalance()
                break

        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        base_ccy2 = ccy[1]

        self.ccy2_amt = 0

        for acc in self.account_names:

            ex, _ = acc.split(".")
            api = self.get_exchange_api_by_account(acc)
            iid = f"{ex}.{self.cfg.contract}"
            symbol = f"{iid}.{self.market}.{self.sub_market}"
            if iid.startswith("okex") and self.market == "usdt_contract":
                symbol = f"{iid}.swap.cross"
            if iid.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = self.cfg.contract.split("_")
                symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            self._account_config_[acc]["cfg_pos_mode"][symbol] = 1
            cur_balance = (await api.account_balance(symbol_cfg)).data
            self.logger.warning(f"cur_balance={cur_balance}")
            if ex.startswith("jojo"):
                ccy2 = "usdc"
            else:
                ccy2 = base_ccy2
            self.ccy2_amt += cur_balance[ccy2]["all"]

        self.send = True

    async def on_order(self, order):
        try:
            await self.order_queue.put(order)
        except:
            traceback.print_exc()

    def handle_order(self, order: Union[PartialOrder, AtomOrder]):
        if isinstance(order, PartialOrder):
            if order.exchange_pair not in self.ex_pair_set:
                # self.logger.critical(f"{order.__dict__}")
                return
        elif isinstance(order, AtomOrder):
            symbol_cfg = self.get_symbol_config(order.symbol_id)
            if symbol_cfg["exchange_pair"] not in self.ex_pair_set:
                # self.logger.critical(f"{order.__dict__}")
                return
        try:
            s = time.time()
            oid = order.xchg_id

            if self.oid_to_cid.get(oid) != None:
                cid = self.oid_to_cid[oid]
                order.client_id = cid
                if order.filled_amount > 0:
                    self.logger.critical(
                        f"obligatory contains: {self._obligatory_order_manager.contains_trans_order(oid=cid)}")
                    self.logger.critical(f"order filled: {order.__dict__}")
                    self.logger.critical(
                        f"recieve message delay {int(time.time()*1e3-order.server_ms)}ms")
            else:
                self.unknown_order_dict[oid] = order
                return

            if not self._obligatory_order_manager.contains_trans_order(oid=cid) and \
                    not self._inventory_order_manager.contains_trans_order(oid=cid) and \
                    not self._hedge_order_manager.contains_trans_order(oid=cid) and \
                    not self._rebalance_order_manager.contains_trans_order(oid=cid):
                return

            if self._rebalance_order_manager.contains_trans_order(oid=cid):
                order_obj: Order = Order.from_dict(
                    porder=order,
                    torder=self._rebalance_order_manager.get_trans_order(
                        oid=cid),
                    contract_value=self.maker_contract_value
                )
                """ Just Clean It Up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                    if self.oid_to_cid.get(oid) != None:
                        self.oid_to_cid.pop(oid)

            if self._obligatory_order_manager.contains_trans_order(oid=cid):
                if order.filled_amount > 0:
                    self.logger.critical(
                        f"oid={order.xchg_id}, filled_qty={order.filled_amount}, price={order.avg_filled_price}, status={order.xchg_status}")
                    self.filled_counter += 1

                torder = self._obligatory_order_manager.get_trans_order(
                    oid=cid)
                self.logger.info(f"partial order:{order.__dict__}")
                self.logger.info(f"trans order{torder.__dict__}")
                order_obj: Order = Order.from_dict(
                    porder=order,
                    torder=torder,
                    contract_value=self.maker_contract_value
                )
                lvl_idx = self._obligatory_order_manager.get_trans_order(
                    oid=cid).floor
                order_obj.meta["lvl_idx"] = int(lvl_idx)
                self._obligatory_executor.on_order(order=order_obj)

                """ Update Inventory """
                if order_obj.filled_qty > 0:
                    qty_chg = self._delta_neutral_executor.get_inc_fill_qty_in_tokens(
                        order=order_obj)
                    qty_chg = qty_chg if order_obj.side == Direction.long else -qty_chg
                    self.update_inventory(
                        chg_qty=qty_chg, key="obl", prc=order_obj.prc)
                    if abs(qty_chg) > 0:
                        self.logger.critical(
                            f"position created {torder.iid}, send at time {torder.sent_ts}, side {torder.side}, fill time {order.server_ms}, at price {order_obj.avg_fill_prc}, with quantity {abs(qty_chg)}, recieve message delay {int(time.time()*1e3-order.server_ms)}ms")

                """ Hedge Order """
                resid_qty_in_tokens, hedge_trans_orders = self._delta_neutral_executor.update_order(
                    order=order_obj)
                self.logger.info(f"resid_qty_in_tokens={resid_qty_in_tokens}")
                if (abs(self._delta_neutral_executor.get_inc_fill_qty_in_tokens(order=order_obj)) > 1e-6) and (len(hedge_trans_orders) == 0):
                    self.logger.critical(
                        f"order_obj={order_obj},depth={self._xex_depth}")

                """ Clean up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    self._obligatory_order_manager.pop_trans_order(oid=cid)
                    self._obligatory_executor.remove_order(oid=cid)
                    self._delta_neutral_executor.remove_order(oid=cid)
                    if self.oid_to_cid.get(oid) != None:
                        self.oid_to_cid.pop(oid)
                    if order_obj.side == Direction.long:
                        self._order_stats["buy_amt"] += order_obj.avg_fill_prc * \
                            order_obj.filled_qty
                        self._order_stats["buy_qty"] += order_obj.filled_qty
                    else:
                        self._order_stats["sell_amt"] += order_obj.avg_fill_prc * \
                            order_obj.filled_qty
                        self._order_stats["sell_qty"] += order_obj.filled_qty

                # if abs(resid_qty_in_tokens) > self._numeric_tol:
                #     if self._hedge_order_manager.is_trans_order_empty() and self._inventory_order_manager.is_trans_order_empty():
                #         """ Hedge Resid Qty """
                #         resid_inventory, inven_trans_orders = self._inventory_executor.hedge_inventory(inventory=resid_qty_in_tokens)
                #         for o in inven_trans_orders:
                #             self.hedge_inventory_preparation(order=o, order_manager=self._inventory_order_manager)
                #             self.logger.critical(f"residue inventory order {o.iid}, send at time {o.sent_ts}, side {o.side}, at price {o.price}, with quantity {o.quantity}")
                #             self.loop.create_task(self.send_inventory_order(order=o))
                #     else:
                #         self.cancel_all_hedge_orders()
                #         self.cancel_all_inventory_orders()

                for o in hedge_trans_orders:
                    self.hedge_inventory_preparation(
                        order=o, order_manager=self._hedge_order_manager)
                    self.logger.critical(
                        f"hedge order {o.iid}, send at time {o.sent_ts}, side {o.side}, at price {o.price}, with quantity {o.quantity}, cid={o.client_id}")
                    self.loop.create_task(self.send_hedge_order(order=o))

            if self._hedge_order_manager.contains_trans_order(oid=cid) or self._inventory_order_manager.contains_trans_order(oid=cid):

                if self._hedge_order_manager.contains_trans_order(oid=cid):
                    torder = self._hedge_order_manager.get_trans_order(oid=cid)
                    order_obj: Order = Order.from_dict(
                        porder=order,
                        torder=torder,
                        contract_value=self.maker_contract_value
                    )
                else:
                    torder = self._inventory_order_manager.get_trans_order(
                        oid=cid)
                    order_obj: Order = Order.from_dict(
                        porder=order,
                        torder=torder,
                        contract_value=self.maker_contract_value
                    )
                self.logger.info(f"partial order:{order.__dict__}")
                self.logger.info(f"trans order{torder.__dict__}")

                """ Update inventory """
                if order_obj.filled_qty > 0:
                    qty_chg = self._inventory_executor.get_inc_fill_qty_in_tokens(
                        order=order_obj)
                    qty_chg = qty_chg if order_obj.side == Direction.long else -qty_chg
                    self.update_inventory(
                        chg_qty=qty_chg, key="hedge", prc=order_obj.prc)
                    if abs(qty_chg) > 0:
                        self.logger.critical(
                            f"close position {torder.iid} at time {torder.sent_ts}, side {torder.side}, fill time {order.server_ms}, at price {order_obj.avg_fill_prc}, with quantity {abs(qty_chg)}, recieve message at {int(time.time()*1e3)}")

                self._inventory_executor.update_order(order=order_obj)

                """ Clean up """
                order_status = order_obj.status
                if order_status in OrderStatus.fin_status():
                    if self._hedge_order_manager.contains_trans_order(oid=cid):
                        self._hedge_order_manager.pop_trans_order(oid=cid)
                        if self.oid_to_cid.get(oid) != None:
                            self.oid_to_cid.pop(oid)
                    elif self._inventory_order_manager.contains_trans_order(oid=cid):
                        self._inventory_order_manager.pop_trans_order(oid=cid)
                        if self.oid_to_cid.get(oid) != None:
                            self.oid_to_cid.pop(oid)
                    if order_obj.side == Direction.long:
                        self._order_stats["buy_amt"] += order_obj.avg_fill_prc * \
                            order_obj.filled_qty
                        self._order_stats["buy_qty"] += order_obj.filled_qty
                    else:
                        self._order_stats["sell_amt"] += order_obj.avg_fill_prc * \
                            order_obj.filled_qty
                        self._order_stats["sell_qty"] += order_obj.filled_qty

            """ Hedge Inventory """
            max_qty = Decimal("0")
            for ex in self.cfg.hedge_exs:
                max_qty = max(self.taker_config[ex]["qty_tick_size"], max_qty)
            if abs(self._inventory) >= float(max_qty):
                if self._hedge_order_manager.is_trans_order_empty() and self._inventory_order_manager.is_trans_order_empty():
                    resid_inventory, hedge_trans_orders = self._inventory_executor.hedge_inventory(
                        inventory=self._inventory)
                    for o in hedge_trans_orders:
                        self.hedge_inventory_preparation(
                            order=o, order_manager=self._inventory_order_manager)
                        self.logger.critical(
                            f"inventory order {o.iid}, send at time {o.sent_ts}, side {o.side}, at price {o.price}, with quantity {o.quantity}")
                        self.loop.create_task(
                            self.send_inventory_order(order=o))
                else:
                    self.cancel_all_hedge_orders()
                    self.cancel_all_inventory_orders()

            """ Got an order that has already finished, this is due to receiving msgs in the wrong order. Skip """
            self.logger.info(
                f"Received msg for a finished order {order}, skip.")

            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(
                    f"on order processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle on_order err: {err}")
            traceback.print_exc()
            self.loop.create_task(
                send_ding_talk(title="JOJO", 
                                 message=f"warning: handle order err{traceback.format_exc()}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )


    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            if len(self.unknown_order_dict) > 100000:
                self.logger.critical(
                    f"too many unknown orders: {len(self.unknown_order_dict)}")
            for oid, partial_order in self.unknown_order_dict.copy().items():
                if self.oid_to_cid.get(oid) != None:
                    now_know_order[oid] = partial_order
                    self.unknown_order_dict.pop(oid)
                elif time.time()*1e3 - partial_order.server_ms > 10000:
                    self.unknown_order_dict.pop(oid)
            await asyncio.gather(
                *[self.on_order(partial_order) for _, partial_order in now_know_order.items()]
            )

    async def on_bbo(self, symbol, bbo):
        try:
            await self.bbo_queue.put((symbol, bbo))
        except:
            traceback.print_exc()

    def handle_bbo(self, symbol: str, bbo: BBODepth):
        if not self.ob_update:
            return
        try:
            s = time.time()
            bbo_obj = BBO.from_dict_prod(iid=symbol, bbo_depth=bbo)
            iid = symbol
            if symbol.startswith("jojo") and self.market == "usdt_contract":
                ex, contract = symbol.split(".")
                ccy1, _, _ = contract.split("_")
                contract = f"{ccy1}_busd_swap"
                iid = f"{ex}.{contract}"
            server_ts = bbo_obj.server_time

            """ tx time guard """
            if self.server_time_guard.has_last_server_time(iid=iid):
                last_server_time = self.server_time_guard.get_last_server_time(
                    iid=iid)
                if server_ts < last_server_time:
                    return
            self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

            self._xex_depth.update_depth_for_ex_with_bbo(bbo=bbo_obj)
            self._obligatory_executor.update_xex_depth(
                xex_depth=self._xex_depth)
            self._delta_neutral_executor.update_xex_depth(
                xex_depth=self._xex_depth)
            self._inventory_executor.update_xex_depth(
                xex_depth=self._xex_depth)

            if not self.send:
                self.cancel_obligatory_orders()
                return

            trans_orders = self._obligatory_executor.gen_quotes(
                ts=bbo_obj.local_time)
            buy_send = True
            sell_send = True
            if self._obl_inventory > self.max_inventory:
                buy_send = False
            if self._obl_inventory < -self.max_inventory:
                sell_send = False
            for trans_order in trans_orders:
                if trans_order.side == Direction.long and not buy_send:
                    trans_order.quantity = 0
                if trans_order.side == Direction.short and not sell_send:
                    trans_order.quantity = 0
                self.loop.create_task(
                    self.send_obligatory_order(order=trans_order))
        except Exception as err:
            self.logger.critical(f"handle bbo err: {err}")
            traceback.print_exc()
            self.loop.create_task(
                send_ding_talk(title="JOJO", 
                                 message=f"warning: handle bbo err{traceback.format_exc()}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )

    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            if symbol.startswith(self.ex_maker):
                await self.maker_book_queue.put((symbol, orderbook))
            else:
                await self.taker_book_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")

    def handle_orderbook(self, symbol: str, orderbook: Dict):
        self.ob_update = True
        try:
            self.ap = orderbook["asks"][0][0]
            self.bp = orderbook["bids"][0][0]
        except:
            self.logger.critical(f"{orderbook}")
        try:
            if symbol.startswith("okex"):
                self.logger.info("has okex orderbook")

            s = time.time()
            ex, contract = symbol.split(".")
            if ex.startswith("jojo") and self.market == "usdt_contract":
                ccy1, _, _ = contract.split("_")
                contract = f"{ccy1}_busd_swap"
            ccy = contract.split("_")
            depth_obj: Depth = Depth.from_dict(
                depth=orderbook,
                ccy1=ccy[0],
                ccy2=ccy[1],
                ex=ex,
                contract=contract
            )
            ex = depth_obj.meta_data.ex
            server_ts = depth_obj.server_ts
            local_ts = depth_obj.resp_ts
            iid = f"{ex}.{contract}"

            """ tx time guard """
            if self.server_time_guard.has_last_server_time(iid=iid):
                last_server_time = self.server_time_guard.get_last_server_time(
                    iid=iid)
                if server_ts < last_server_time:
                    return
            self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

            self._xex_depth.update_depth_for_ex(depth=depth_obj)
            self._obligatory_executor.update_xex_depth(
                xex_depth=self._xex_depth)
            self._delta_neutral_executor.update_xex_depth(
                xex_depth=self._xex_depth)
            self._inventory_executor.update_xex_depth(
                xex_depth=self._xex_depth)

            if not self.send:
                self.cancel_obligatory_orders()
                return
            trans_orders = self._obligatory_executor.gen_quotes(
                ts=depth_obj.resp_ts)
            buy_send = True
            sell_send = True
            if self._obl_inventory > self.max_inventory:
                buy_send = False
            if self._obl_inventory < -self.max_inventory:
                sell_send = False
            for trans_order in trans_orders:
                if trans_order.side == Direction.long and not buy_send:
                    trans_order.quantity = 0
                if trans_order.side == Direction.short and not sell_send:
                    trans_order.quantity = 0
                self.loop.create_task(
                    self.send_obligatory_order(order=trans_order))
            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(
                    f"orderbook processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle orderbook err: {err}")
            traceback.print_exc()
            self.loop.create_task(
                send_ding_talk(title="JOJO", 
                                 message=f"warning: handle orderbook err{traceback.format_exc()}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )

    async def handle_ws_update(self):

        while True:
            try:
                if not self.order_queue.empty():
                    order = await self.order_queue.get()
                    self.handle_order(order)
                    continue
                elif self.maker_book_queue.empty() and self.taker_book_queue.empty() and self.bbo_queue.empty():
                    await asyncio.sleep(0.0001)
                elif not self.taker_book_queue.empty():
                    symbol, orderbook = await self.taker_book_queue.get()
                    self.taker_book_queue = LifoQueue()
                    self.handle_orderbook(symbol=symbol, orderbook=orderbook)
                elif not self.bbo_queue.empty():
                    symbol, bbo = await self.bbo_queue.get()
                    self.bbo_queue = LifoQueue()
                    self.handle_bbo(symbol=symbol, bbo=bbo)
                else:
                    symbol, orderbook = await self.maker_book_queue.get()
                    self.mob_queue = LifoQueue()
                    self.handle_orderbook(symbol=symbol, orderbook=orderbook)

                await asyncio.sleep(0.0001)
            except:
                await asyncio.sleep(0.0001)
                traceback.print_exc()

    async def base_send_order(self, order: TransOrder):
        if order.quantity == 0:
            self.logger.info(f"Trying to send qty=0 order, skip: {order.client_id}")
            return None
        iid = order.iid
        side = self.direction_map(side=order.side)
        symbol = f"{iid}.{self.market}.{self.sub_market}"
        if iid.startswith("okex") and self.market == "usdt_contract":
            symbol = f"{iid}.swap.cross"
        if iid.startswith("jojo") and self.market == "usdt_contract":
            ccy1, _, _ = self.cfg.contract.split("_")
            symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"

        if not self.volume_notional_check(symbol=symbol, price=order.price, qty=order.quantity):
            self.logger.info(f"volume_notional_check failed, skip: {order.client_id}, qty={order.quantity}")
            return None

        # if order.side == Direction.long:
        #     if self._inventory * float(self.ap) > 15000:
        #         return None
        # elif order.side == Direction.short:
        #     if self._inventory * float(self.ap) < -15000:
        #         return None

        # if order.floor == 2:
        #     self.logger.info(f"send floor 2 order: {order.__dict__}")
        order_type = OrderType.Limit
        if order.maker_taker == MakerTaker.maker:
            order_type = OrderType.PostOnly
        if order.maker_taker == MakerTaker.taker:
            order_type = OrderType.IOC

        try:
            self.logger.info(f"trying to send:{order.client_id}")
            symbol_cfg = self.get_symbol_config(symbol)
            resp: AtomOrder = await self.make_order(
                symbol_name=symbol,
                price=Decimal(order.price) +
                symbol_cfg["price_tick_size"]/Decimal("2"),
                volume=Decimal(order.quantity) +
                symbol_cfg["qty_tick_size"]/Decimal("2"),
                side=side,
                order_type=order_type,
                position_side=OrderPositionSide.Open,
                client_id=order.client_id
            )
            self.logger.info(f"finish send:{order.client_id}")
        except Exception as err:
            self.logger.critical(f"send order failed, {err}")
            return None

        self.logger.info(f"order resp:{resp}")
        order.update(resp)
        order.price = round(float(resp.requested_price), price_precision[iid])
        order.quantity = round(
            float(resp.requested_amount), qty_precision[iid])
        order.sent_ts = int(time.time()*1e3)
        return resp

    def trim_order_before_sending(self, order: TransOrder):
        ex, contract = order.iid.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        if ex.startswith("jojo") and self.market == "usdt_contract":
            ccy1, _, _ = contract.split("_")
            order.symbol_id = self.get_symbol_config(
                f"{ex}.{ccy1}_usdc_swap.swap.na")["id"]
        elif ex == "okex" and self.market == "usdt_contract":
            order.symbol_id = self.get_symbol_config(
                f"{order.iid}.swap.cross")["id"]
        else:
            order.symbol_id = self.get_symbol_config(
                f"{order.iid}.{self.market}.{self.sub_market}")["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)
        if order.side == Direction.long:
            order.price = round_decimals_down(
                order.price, price_precision[order.iid])
        else:
            order.price = round_decimals_up(
                order.price, price_precision[order.iid])

        order.quantity = round(order.quantity, qty_precision[order.iid])

        return cid

    async def send_obligatory_order(self, order: TransOrder):
        # self.logger.info("sending obligatory order")
        try:
            for oid in self._obligatory_order_manager.keys():
                if self._obligatory_order_manager.get_trans_order(oid):
                    o = self._obligatory_order_manager.trans_order_at(oid=oid)
                else:
                    continue
                if o.side == order.side and o.iid == order.iid and o.floor == order.floor:
                    rel_diff = np.log(order.price / o.price)
                    if abs(rel_diff) > self.cfg.requirements[o.floor][2]:
                        if not o.cancel:
                            self.loop.create_task(self.handle_cancel_order(
                                oid=oid, order=o, order_manager=self._obligatory_order_manager))
                    else:
                        o.sent_ts = int(time.time()*1e3)
                    return
            cid = self.trim_order_before_sending(order)
            self._obligatory_order_manager.add_trans_order(
                order=order, oid=cid)
            self.logger.info("sending obligatory order2")
            resp = await self.base_send_order(order=order)
            if resp:
                self.oid_to_cid[resp.xchg_id] = cid
                order.xchg_id = resp.xchg_id
            else:
                self._obligatory_order_manager.pop_trans_order(oid=cid)
        except:
            traceback.print_exc()

    def hedge_inventory_preparation(self, order: TransOrder, order_manager: OrderManager):
        """
        do this before create task of send_hedge_order and send_inventory_order
        """
        cid = self.trim_order_before_sending(order)
        order_manager.add_trans_order(order=order, oid=cid)

    async def send_hedge_order(self, order: TransOrder):
        try:
            resp = await self.base_send_order(order=order)
        except:
            traceback.print_exc()
        if resp:
            self.oid_to_cid[resp.xchg_id] = order.client_id
            order.xchg_id = resp.xchg_id
            self.logger.info(f"Hedge order send successful: {order.client_id}")
        else:
            self.logger.info(f"Hedge order send failed: {order.client_id}")
            self._hedge_order_manager.pop_trans_order(oid=order.client_id)

    async def send_inventory_order(self, order: TransOrder):
        resp = await self.base_send_order(order=order)
        if resp:
            self.oid_to_cid[resp.xchg_id] = order.client_id
            order.xchg_id = resp.xchg_id
        else:
            self._inventory_order_manager.pop_trans_order(oid=order.client_id)

    async def cancel_order(self, order: TransOrder, catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(
            order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(
                    f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.data
        except Exception as err:
            if not catch_error:
                raise
            else:
                self.logger.critical(
                    f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, redirect False")
                return False

    async def handle_cancel_order(self, oid, order: TransOrder, order_manager: OrderManager):
        async def unit_cancel_order(order: TransOrder):
            try:
                res = await self.cancel_order(order)
                if res is True:
                    order.cancel = True
            except Exception as err:
                self.logger.critical(f'cancel order {oid} err {err}')

        if oid in self.canceling_id:
            return

        self.canceling_id.add(oid)

        counter = 0
        while True:
            if order_manager.get_trans_order(oid=oid) != None:
                o = order_manager.trans_order_at(oid=oid)
                if o.cancel:
                    self.canceling_id.remove(oid)
                    return
            else:
                self.canceling_id.remove(oid)
                return
            if order.xchg_id:
                counter += 1
                self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(0.2)

            if counter > 100:
                if order_manager.get_trans_order(oid=oid) != None:
                    self.logger.info("cancel too many times")
                    try:
                        await send_ding_talk(title="JOJO", message=f"warning: order cancel {counter} times, exchange id={order.xchg_id}",
                                             token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                    except:
                        traceback.print_exc()
                    order_manager.get_trans_order(oid=oid).cancel = True
        
    
    async def rebalance(self):
        # rebalance, use when initializing or emergency exiting.
        ref_prc = self._obligatory_executor._ref_prc
        ccy = self.cfg.contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.logger.info(f"current reference price={ref_prc}")
        if self.market == "spot":
            for acc in self.account_names:
                self.logger.info(f"handle account rebalance: {acc}")
                ex, _ = acc.split(".")
                iid = f"{ex}.{self.cfg.contract}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                api = self.get_exchange_api_by_account(acc)
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    await api.flash_cancel_orders(symbol_cfg)
                except:
                    self.logger.info("no open orders")
                cur_balance = (await api.account_balance(symbol_cfg)).data
                ccy1_amt = float(cur_balance[ccy1]["all"]) * ref_prc
                ccy2_amt = float(cur_balance[ccy2]["all"])
                buy_amt = (ccy2_amt - ccy1_amt) / 2

                p, side = (ref_prc * (1 + 0.03), Direction.long) if buy_amt > 0 else (
                    ref_prc * (1 - 0.03), Direction.short)
                qty = round_decimals_down(
                    abs(buy_amt)/ref_prc, qty_precision[iid])

                order = TransOrder(
                    iid=iid,
                    side=side,
                    p=p,
                    q=qty,
                    floor=None,
                    maker_taker=MakerTaker.taker
                )
                self.logger.info(
                    f"trans order:price={order.price}, quantity={order.quantity},side={order.side},iid={order.iid}")
                cid = self.trim_order_before_sending(order)
                self._rebalance_order_manager.add_trans_order(
                    order=order, oid=cid)
                resp = await self.base_send_order(order=order)
                if resp:
                    self.oid_to_cid[resp.xchg_id] = cid
                    order.xchg_id = resp.xchg_id
                else:
                    self._rebalance_order_manager.pop_trans_order(oid=cid)
                await asyncio.sleep(1)
                cur_balance = (await api.account_balance(symbol_cfg)).data
                self.logger.info(
                    f"""{ccy1}={float(cur_balance[ccy1]["all"])}, {ccy2}={float(cur_balance[ccy2]["all"])} for {acc}""")
                await asyncio.sleep(1)
        elif self.market == "usdt_contract":
            for acc in self.account_names:
                ex, _ = acc.split(".")
                api = self.get_exchange_api_by_account(acc)
                iid = f"{ex}.{self.cfg.contract}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                if iid.startswith("okex"):
                    symbol = f"{iid}.swap.cross"
                if iid.startswith("jojo"):
                    ccy1, _, _ = self.cfg.contract.split("_")
                    symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    all_opening_orders = (await api.all_opening_orders(symbol_cfg)).data
                    for order in all_opening_orders:
                        await self.cancel_order(order)
                except:
                    traceback.print_exc()
                    self.logger.info("no open orders")
                cur_position = (await api.contract_position(symbol_cfg)).data
                self.logger.critical(f"current position: {cur_position}")
                pos = cur_position["long_qty"] - cur_position["short_qty"]

                # p, side = (ref_prc * (1 - 0.001), Direction.short) if pos > 0 else (ref_prc * (1 + 0.001), Direction.long)
                # qty = round_decimals_down(abs(pos), qty_precision[iid])

                # order = TransOrder(
                #     iid=iid,
                #     side=side,
                #     p=p,
                #     q=qty,
                #     floor=None,
                #     maker_taker=MakerTaker.taker
                # )
                # cid = self.trim_order_before_sending(order)
                # self._rebalance_order_manager.add_trans_order(order=order, oid=cid)
                # resp = await self.base_send_order(order=order)
                # if resp:
                #     self.oid_to_cid[resp.xchg_id] = cid
                # else:
                #     self._rebalance_order_manager.pop_trans_order(oid=cid)
                # resp = await self.base_send_order(order=order)
                # await asyncio.sleep(1)
                # cur_position = (await api.contract_position(symbol_cfg)).data
                # pos = cur_position["long_qty"] - cur_position["short_qty"]
                self.logger.info(f"""current position={pos}""")
                self._inventory += float(pos)
                if ex == self.cfg.ex:
                    self._obl_inventory += float(pos)
                elif ex in self.cfg.hedge_exs:
                    self._hedge_inventory += float(pos)
                await asyncio.sleep(1)

    async def get_order_status_direct(self, oid, order: TransOrder, order_manager: OrderManager):
        async def unit_query_order(order: TransOrder):
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(
                    order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=order.client_id,
                )
                new_order: AtomOrder = res.data
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
                else:
                    self.loop.create_task(self.handle_cancel_order(
                        oid=oid, order=order, order_manager=order_manager))
            except Exception as err:
                self.logger.critical(
                    f"direct check order error: {err}, iid={order.iid}, oid={order.xchg_id}, cid={order.client_id}")

        if time.time()*1e3 - order.sent_ts < 1000 or oid in self.quering_id:
            return

        self.quering_id.add(oid)

        counter = 0
        while True:
            if order_manager.get_trans_order(oid=oid) != None:
                if time.time()*1e3 - order.sent_ts < 1000:
                    self.quering_id.remove(oid)
                    return
            else:
                self.quering_id.remove(oid)
                return
            if order.xchg_id:
                counter += 1
                self.loop.create_task(unit_query_order(order))
            await asyncio.sleep(10)

            if counter > 100:
                if order_manager.get_trans_order(oid=oid) != None:
                    self.logger.critical(f"order query too many times")
                    try:
                        await send_ding_talk(title="JOJO", message=f"warning: order query {counter} times, exchange id={order.xchg_id}",
                                             token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                    except:
                        traceback.print_exc()

    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(10)
            self.check_orders(order_manager=self._obligatory_order_manager)
            self.check_orders(order_manager=self._hedge_order_manager)
            self.check_orders(order_manager=self._inventory_order_manager)

    def check_orders(self, order_manager: OrderManager):
        for oid in order_manager.keys():
            if order_manager.get_trans_order(oid):
                o = order_manager.trans_order_at(oid)
                self.loop.create_task(self.get_order_status_direct(
                    oid=oid, order=o, order_manager=order_manager))

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    await self.rebalance()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit": None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def get_local_stats(self):
        while True:
            await asyncio.sleep(30)
            try:
                avg_buy_price = float(
                    self._order_stats["buy_amt"]/self._order_stats["buy_qty"]) if self._order_stats["buy_qty"] else 0
                avg_sell_price = float(
                    self._order_stats["sell_amt"]/self._order_stats["sell_qty"]) if self._order_stats["sell_qty"] else 0
                spread = float((avg_sell_price - avg_buy_price) / (avg_sell_price +
                               avg_buy_price) * 2) if avg_sell_price + avg_buy_price > 0 else float(0)
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt",
                        tag={"sn": self.strategy_name},
                        fields={
                            "avg_buy_price": float(avg_buy_price),
                            "avg_sell_price": float(avg_sell_price),
                            "spread": float(spread),
                            "inventory": float(self._inventory),
                            "obligatory_inventory": float(self._obl_inventory),
                            "hedge_inventory": float(self._hedge_inventory),
                        }
                    )
                )

                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt",
                        tag={"sn": self.strategy_name},
                        fields={
                            "trade_amount_per_second": float(self.trd_amt/(time.time() - self.start_ts)),
                            "pure_profit": float(self.trd_amt * spread / 2)
                        }
                    )
                )

                for oid, order in list(self._obligatory_order_manager.trans_order_items()):
                    if order.side == Direction.short:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt",
                                tag={"sn": self.strategy_name},
                                fields={
                                    "cache_sell_price": float(order.price)
                                }
                            )
                        )
                    else:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt",
                                tag={"sn": self.strategy_name},
                                fields={
                                    "cache_buy_price": float(order.price)
                                }
                            )
                        )

            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()

    async def debug(self):
        while True:
            await asyncio.sleep(30)
            for oid, o in list(self._obligatory_order_manager.trans_order_items()):
                if o.floor != 0:
                    continue
                self.logger.critical(
                    f"order: p={o.price}, q={o.quantity}, side={o.side}, id={oid}, time={time.time()*1e3 - o.sent_ts}")
            self.logger.critical(".................")
            # self.logger.critical(f"attack filled rate={self.filled_counter/self.counter if self.counter else 0}, total attact={self.counter}")

    async def rolling_attack(self):
        while True:
            await asyncio.sleep(1)
            try:
                for oid in self._obligatory_order_manager.keys():
                    if self._obligatory_order_manager.get_trans_order(oid=oid):
                        order = self._obligatory_order_manager.trans_order_at(
                            oid=oid)
                        if order.cancel == True:
                            continue
                        iid = order.iid
                        symbol = f"{iid}.{self.market}.{self.sub_market}"
                        if iid.startswith("okex") and self.market == "usdt_contract":
                            symbol = f"{iid}.swap.cross"
                        if iid.startswith("jojo") and self.market == "usdt_contract":
                            ccy1, _, _ = self.cfg.contract.split("_")
                            symbol = f"{self.cfg.ex}.{ccy1}_usdc_swap.swap.na"
                        symbol_cfg = self.get_symbol_config(symbol)

                        if order.side == Direction.long and order.price >= self.bp:
                            self.logger.info(
                                f"attacking, side=sell, price={order.price}")
                            self.counter += 1
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(
                                    order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                                volume=Decimal(
                                    order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                                side=OrderSide.Sell,
                                order_type=OrderType.IOC,
                                position_side=OrderPositionSide.Open,
                            )
                        elif order.side == Direction.short and order.price <= self.ap:
                            self.logger.info(
                                f"attacking, side=buy, price={order.price}")
                            self.counter += 1
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(
                                    order.price) + symbol_cfg["price_tick_size"]/Decimal("2"),
                                volume=Decimal(
                                    order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2"),
                                side=OrderSide.Buy,
                                order_type=OrderType.IOC,
                                position_side=OrderPositionSide.Open,
                            )
            except:
                traceback.print_exc()

    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.check_unknown_order(),
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.debug(),
                # self.rolling_attack()
                self.get_local_stats()
            )


if __name__ == '__main__':
    logging.getLogger().setLevel("CRITICAL")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
