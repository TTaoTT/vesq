import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self, cache_size=1000) -> None:
        self.buy_ts = 0
        self.buy_max = None
        self.buy_min = None
        self.buy_cache = []
        self.sell_ts = 0
        self.sell_max = None
        self.sell_min = None
        self.sell_cache = []
        self.cache_size = cache_size
        
    def feed_trade(self, trade: dict):
        if trade["t"] > self.buy_ts and not trade["s"]:
            if self.buy_min:
                self.buy_cache.append({"t":self.buy_ts, "pd":self.buy_max-self.buy_min})
            self.buy_max = trade["p"]
            self.buy_min = trade["p"]
            self.buy_ts = trade["t"]
        elif trade["t"] == self.buy_ts and not trade["s"]:
            self.buy_max = max(self.buy_max, trade["p"])
            self.buy_min = min(self.buy_min, trade["p"])
        elif trade["t"] > self.sell_ts and trade["s"]:
            if self.sell_min:
                self.sell_cache.append({"t":self.sell_ts, "pd":self.sell_max - self.sell_min})
            self.sell_max = trade["p"]
            self.sell_min = trade["p"]
            self.sell_ts = trade["t"]
        elif trade["t"] == self.sell_ts and trade["s"]:
            self.sell_max = max(self.sell_max, trade["p"])
            self.sell_min = min(self.sell_min, trade["p"])
        
    def remove_expired(self, ts):
        while self.buy_cache and self.buy_cache[0]["t"] < ts-self.cache_size:
            self.buy_cache.pop(0)
        while self.sell_cache and self.sell_cache[0]["t"] < ts-self.cache_size:
            self.sell_cache.pop(0)

    def get_taker_buy_impulse(self):
        return max(self.buy_cache, key=lambda x:x['pd'])['pd'] if self.buy_cache else 0
    
    def get_taker_sell_impulse(self):
        return max(self.sell_cache, key=lambda x:x['pd'])['pd'] if self.sell_cache else 0
    
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.use_raw_stream = True
        
        self.price_multi = 1e7
        self.qty_multi = 1e7
        
        self.params = self.config['strategy']['params']
        self.trade_cache = PriceDistributionCache(
            cache_size=self.params["cache_size"]
            )

        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        
     

    async def before_strategy_start(self):
        # self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        
        symbol_cfg = self.get_symbol_config(self.symbol)
        self.price_tick = symbol_cfg['price_tick_size']
        self.qty_tick = symbol_cfg['qty_tick_size']
        
    async def on_agg_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        tmp = {
            "t":trade["data"]["E"],
            "p":int(Decimal(trade["data"]["p"])/self.price_tick), 
            "s":trade["data"]["m"]
            }

        self.trade_cache.feed_trade(tmp)
        self.trade_cache.remove_expired(trade["data"]["E"])
        
    async def main_logic(self):
        while True:
            await asyncio.sleep(0.4)
            buy_impulse = self.trade_cache.get_taker_buy_impulse()
            sell_impulse = self.trade_cache.get_taker_sell_impulse()
            if buy_impulse > 3 or sell_impulse>3:
                self.logger.warning(f"""
                buy_impulse:{buy_impulse} , sell_impulse:{sell_impulse}
                """)

    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(1)
        await asyncio.gather(
            self.main_logic()
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
