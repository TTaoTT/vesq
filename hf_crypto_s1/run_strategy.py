import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue
import json

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy



class MpOutput:
    def __init__(self, base_line):
        self.base_std = base_line
        self.mp_queue = collections.deque(maxlen=200)
        self.qt_queue = collections.deque(maxlen=10000)

        self.pre_max_list = collections.deque(maxlen=200)
        self.pre_min_list = collections.deque(maxlen=200)

    def feed_mp(self, mp):
        self.mp_queue.append(float(mp))
        self.update_base_line()

    def get_x_cur_duration(self):
        if len(self.pre_min_list) == 200:
            mp_mean = np.mean(list(self.mp_queue)[-50:])
            if mp_mean > self.pre_max_list[0]:
                return 1
            if mp_mean < self.pre_min_list[0]:
                return -1
        return 0

    def feed_max_logic(self):
        p_max = np.max(list(self.mp_queue))
        p_min = np.min(list(self.mp_queue))
        self.pre_max_list.append(p_max)
        self.pre_min_list.append(p_min)

    def update_base_line(self):
        if len(self.mp_queue) == 200:
            cur_std = self.get_cur_std()
            self.qt_queue.append(cur_std)
            if len(self.qt_queue) == 10000:
                cur_82_std = np.quantile(list(self.qt_queue), 0.82)
                self.base_std = cur_82_std
                self.qt_queue.clear()
            self.feed_max_logic()

    def get_cur_std(self):
        return np.std(list(self.mp_queue)[-100:])

    def mp_bad(self):
        return self.get_cur_std() > self.base_std

    def get_mean_mp(self):
        return np.mean(list(self.mp_queue)[-50:])

    def get_cur_duration(self):
        return self.mp_queue[-1] - self.get_mean_mp()

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = f"crypto.btc_usdt.spot"
        self.trade_cache_buy_1s_large = collections.deque(maxlen=120)
        self.trade_cache_sell_1s_large = collections.deque(maxlen=120)
        
        self.trade_cache_buy_1s = []
        self.trade_cache_sell_1s = []
        
        self._order_buy_to_send = Queue()
        self._order_sell_to_send = Queue()
        self._order_to_store = Queue()
        
        self.buy_order_cache = dict()
        self.sell_order_cache = dict()
        
        self.t_up = Decimal(0.01)
        self.t_down = Decimal(0.01)
        
        self.imb = Decimal(0)
        
        self.mp_std_handle = MpOutput(base_line=float(0.00002))
        
        self.order_storage_enabled = False
        
        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        
        self.unknown_order_dict = {}
        
        self.sell_amount = Decimal(0)
        self.sell_qty = Decimal(0)
        self.sell_avg_price = Decimal(0)
        
        self.buy_amount = Decimal(0)
        self.buy_qty = Decimal(0)
        self.buy_avg_price = Decimal(0)
        
        self.send_order = True
        self.err_count = 0
         
    async def before_strategy_start(self):
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()
        await self._get_symbol_config_from_remote([self.symbol])
        self.direct_subscribe_order_update('crypto',symbols=[self.symbol])
        self.subscribe_public_trade_direct([self.symbol])
        # self.subscribe_public_trade([self.symbol])
        self.subscribe_orderbook_direct([self.symbol])
        self.loop.create_task(self.store_order())
        self.loop.create_task(self.error_counter_reset())
        
    async def error_counter_reset(self):
        while True:
            await asyncio.sleep(60)
            self.err_count = 0
        
    async def on_public_trade(self, symbol, trade: TradeData):
        """
        class TradeData(object):
            # Internal symbol
            symbol = attr.ib()
            # Source exchange name
            source = attr.ib()
            price = attr.ib(converter=num_to_decimal)
            quantity = attr.ib(converter=num_to_decimal)
            side = attr.ib(validator=[attr.validators.instance_of(TradeSide)])
            # Timestamp related
            req_ts = attr.ib(converter=int)
            server_ts = attr.ib(converter=int)

            trade_id = attr.ib(default='', converter=str)
            buy_order_id = attr.ib(default='', converter=str)
            sell_order_id = attr.ib(default='', converter=str)
            resp_ts = attr.ib(factory=time_ms_now, converter=int)
            transaction_ts = attr.ib(factory=time_ms_now, converter=int)
        """
        if trade.side == TradeSide.Buy:
            self.trade_cache_buy_1s.append(float(trade.price))

        else:
            self.trade_cache_sell_1s.append(float(trade.price))
        
    async def on_orderbook(self, symbol, orderbook):
        self.ap = orderbook["asks"][0][0]
        self.bp = orderbook["bids"][0][0]
        self.mp = (self.ap + self.bp)/Decimal(2)
        self.spread = self.ap - self.bp
        self.mp_std_handle.feed_mp(mp=self.mp)
        
    async def on_order(self, xchg_id, order: PartialOrder):
        """
        class PartialOrder(object):
            # used to update redis order
            xchg_status = attr.ib(validator=[attr.validators.instance_of(OrderStatus)])
            filled_amount = attr.ib(converter=num_to_decimal)
            avg_filled_price = attr.ib(converter=num_to_decimal)
            commission_fee = attr.ib(converter=num_to_decimal)
            is_finished = attr.ib()  
            # Timestamp related
            local_msg_ts_ms = attr.ib(converter=int)  # Local ms time receiving this message
            server_evt_ts_ms = attr.ib(converter=int)  # Server ms time for this message
            finished_ts_ms = attr.ib(default=0, converter=int)  # Order finished ts
        """
        if xchg_id not in list(self.buy_order_cache) and xchg_id not in list(self.sell_order_cache):
            # add unknow order dealing funciton, check_unknown_order.
            # before, will only check order after order expired. 
            self.unknown_order_dict[xchg_id] = order
            self.logger.warning(f"find unknow order {xchg_id}")
            return
        
        if xchg_id in list(self.buy_order_cache):
            if order.filled_amount < self.buy_order_cache[xchg_id].filled_amount:
                return

            self.buy_order_cache[xchg_id].filled_amount = order.filled_amount
            self.buy_order_cache[xchg_id].avg_filled_price = order.avg_filled_price
            self.buy_order_cache[xchg_id].is_finished = order.is_finished
            self.buy_order_cache[xchg_id].xchg_status = order.xchg_status
            extra_info = self.buy_order_cache[xchg_id].raw_data
            self.buy_order_cache[xchg_id].extra_info = f"{extra_info['stop_ts']}"

            if order.is_finished:
                if order.filled_amount > 0:
                    self._order_to_store.put_nowait(self.buy_order_cache[xchg_id])
                    self.buy_amount += order.filled_amount * order.avg_filled_price
                    self.buy_qty += order.filled_amount
                self.buy_order_cache.pop(xchg_id)
                if xchg_id in self.unknown_order_dict:
                    self.unknown_order_dict.pop(xchg_id)
        elif xchg_id in list(self.sell_order_cache):
            if order.filled_amount < self.sell_order_cache[xchg_id].filled_amount:
                return

            self.sell_order_cache[xchg_id].filled_amount = order.filled_amount
            self.sell_order_cache[xchg_id].avg_filled_price = order.avg_filled_price
            self.sell_order_cache[xchg_id].is_finished = order.is_finished
            self.sell_order_cache[xchg_id].xchg_status = order.xchg_status
            extra_info = self.sell_order_cache[xchg_id].raw_data
            self.sell_order_cache[xchg_id].extra_info = f"{extra_info['stop_ts']}"

            if order.is_finished:
                if order.filled_amount > 0:
                    self._order_to_store.put_nowait(self.sell_order_cache[xchg_id])
                    self.sell_amount += order.filled_amount * order.avg_filled_price
                    self.sell_qty += order.filled_amount
                self.sell_order_cache.pop(xchg_id)
                if xchg_id in self.unknown_order_dict:
                    self.unknown_order_dict.pop(xchg_id)
                    
    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.sell_order_cache or xchg_id in self.buy_order_cache:
                    self.logger.warning(f"unkonw {xchg_id} order rest back")
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(xchg_id, partial_order) for xchg_id, partial_order in now_know_order.items()]
            )

    async def store_order(self):
        while True:
            order = await self._order_to_store.get()
            await self.post_order_to_system(order, self.session)

    async def calculate_t_1(self):
        while True:
            await asyncio.sleep(1)

            if self.trade_cache_buy_1s and self.trade_cache_buy_1s[0]!=0:
                buy_imp = (max(self.trade_cache_buy_1s) - self.trade_cache_buy_1s[0]) / self.trade_cache_buy_1s[0]
            else:
                buy_imp = 0

            if self.trade_cache_sell_1s and self.trade_cache_sell_1s[0]!=0:
                sell_imp = (self.trade_cache_sell_1s[0] - min(self.trade_cache_sell_1s)) / self.trade_cache_sell_1s[0]
            else:
                sell_imp = 0

            self.trade_cache_buy_1s = []
            self.trade_cache_sell_1s = []

            self.trade_cache_buy_1s_large.append(buy_imp)
            self.trade_cache_sell_1s_large.append(sell_imp)

            self.t_up = max(np.quantile(self.trade_cache_buy_1s_large, float(self.params["q"])), buy_imp)
            self.t_down = max(np.quantile(self.trade_cache_sell_1s_large, float(self.params["q"])), sell_imp)

            self.t_1 = Decimal(max(self.t_up,self.t_down))
            
            if self._order_buy_to_send.empty() and not self.buy_order_cache and self.send_order:
                self._order_buy_to_send.put_nowait(1)
                
            if self._order_sell_to_send.empty() and not self.sell_order_cache and self.send_order:
                self._order_sell_to_send.put_nowait(1)
    
    async def calculate_pos_imb(self):
        while True:
            await asyncio.sleep(0.3)
            try:
                cur_balance = await self.get_balance(self.symbol)
                usdt_equity = cur_balance.get("usdt")
                usdt_amount = usdt_equity.get("all")
                
                btc_equity = cur_balance.get("btc")
                btc_amount = btc_equity.get("all")
                
                if usdt_amount and btc_amount:
                    btc_amount = btc_amount * self.mp
                    self.imb = Decimal((btc_amount - usdt_amount) / (btc_amount + usdt_amount))
            except Exception as err:
                self.logger.warning(f"check position err, {err}")
                
    def weight_function(self, imb):
        imb = float(imb)
        if imb>=0:
            return Decimal(np.log(imb+1) / np.log(2))
        else:
            return -Decimal(np.log(-imb+1) / np.log(2))
        
    async def send_buy_order(self):
        while True:
            await self._order_buy_to_send.get()
            
            spread = self.t_1 * (Decimal(1) + self.weight_function(self.imb))
            price = self.mp * (Decimal(1) - spread)
            if self.mp_std_handle.mp_bad() and self.mp_std_handle.get_cur_duration() > 0:
                price = Decimal(self.mp_std_handle.get_mean_mp()) * (Decimal(1) - spread)
            try:
                order = await self.make_future_order(self.symbol, price, self.params["base_amount"], OrderSide.Buy, OrderPositionSide.Open, OrderTimeInForce.PostOnly)
            except Exception as err:
                self.err_count += 1
                if self.err_count > 10:
                    exit()
                self.logger.warning(f"send order error, {err}")
                order = None
            if order:
                extra_info = dict()
                extra_info["stop_ts"] = 1
                extra_info["cancel"] = 0
                # extra_info["source"] =  params[7] # entry or balance
                extra_info["query"] =  0
                order.raw_data = extra_info
                self.buy_order_cache[order.xchg_id] = order
                
    async def send_sell_order(self):
        while True:
            await self._order_sell_to_send.get()
            spread = self.t_1 * (Decimal(1) - self.weight_function(self.imb))
            price = self.mp * (Decimal(1) + spread)
            if self.mp_std_handle.mp_bad() and self.mp_std_handle.get_cur_duration() < 0:
                price = Decimal(self.mp_std_handle.get_mean_mp()) * (Decimal(1) + spread)
            try:
                order = await self.make_future_order(self.symbol, price, self.params["base_amount"], OrderSide.Sell, OrderPositionSide.Open, OrderTimeInForce.PostOnly)
            except Exception as err:
                self.err_count += 1
                if self.err_count > 10:
                    exit()
                self.logger.warning(f"send order error, {err}")
                order = None
            if order:
                extra_info = dict()
                extra_info["stop_ts"] = 1
                extra_info["cancel"] = 0
                # extra_info["source"] =  params[7] # entry or balance
                extra_info["query"] =  0
                order.raw_data = extra_info
                self.sell_order_cache[order.xchg_id] = order
                
    async def cancel_order_action(self):
        async def get_order_status_direct(order):
            _xchg_name,_pair,_market = order.internal_symbol.split(".")
            api = self._check_api_(_xchg_name)
            try:
                self.logger.warning(f"{_pair} get order {order.xchg_id} from exchange http api")
                _order = await api.order_match_result(_pair,order.xchg_id, extra_opts = {"market":_market})
                
            except Exception as err:
                self.logger.error(f"direct check order error: {err}")
                _order = None
            return _order
    
        while True:
            await asyncio.sleep(0.005)
            for oid, order in self.sell_order_cache.items():
                if time.time()*1e3 - order.created_ts_ms > order.raw_data["stop_ts"] * 1e3 + order.raw_data["cancel"]*1e3*0.2:
                    self.loop.create_task(self.cancel_order(order))
                    self.sell_order_cache[oid].raw_data["cancel"] += 1
                elif time.time()*1e3 - order.created_ts_ms > (order.raw_data["stop_ts"] + order.raw_data["query"] + 1)*1e3 and order.raw_data["cancel"]>0:
                    order_new = await get_order_status_direct(order)
                    if order_new and self.sell_order_cache.get(oid):
                        self.sell_order_cache[oid].raw_data["query"] += 1
                        if order_new.is_finished:
                            self.loop.create_task(self.on_order(oid, order_new))
                        elif self.sell_order_cache[oid].raw_data["query"]>10:
                            exit()
            for oid, order in self.buy_order_cache.items():
                if time.time()*1e3 - order.created_ts_ms > order.raw_data["stop_ts"] * 1e3 + order.raw_data["cancel"]*1e3*0.2:
                    self.loop.create_task(self.cancel_order(order))
                    self.buy_order_cache[oid].raw_data["cancel"] += 1
                elif time.time()*1e3 - order.created_ts_ms > (order.raw_data["stop_ts"] + order.raw_data["query"] + 1)*1e3 and order.raw_data["cancel"]>0:
                    order_new = await get_order_status_direct(order)
                    if order_new and self.buy_order_cache.get(oid):
                        self.buy_order_cache[oid].raw_data["query"] += 1
                        if order_new.is_finished:
                            self.loop.create_task(self.on_order(oid, order_new))
                        elif self.buy_order_cache[oid].raw_data["query"]>10:
                            exit()
                
    async def set_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send_order = False
                    for i in range(3):
                        for order in self.sell_order_cache.values():
                            self.loop.create_task(self.cancel_order(order))
                        for order in self.buy_order_cache.values():
                            self.loop.create_task(self.cancel_order(order))
                        await asyncio.sleep(3)
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")
                
        while True:
            await asyncio.sleep(10)
            await check_cache()
            if self.buy_qty and self.sell_qty:
                self.buy_avg_price = self.buy_amount / self.buy_qty
                self.sell_avg_price = self.sell_amount / self.sell_qty
                await self.redis_set_cache(
                    {
                        "exit":None,
                        "s_avg_price":self.sell_avg_price,
                        "b_avg_price":self.buy_avg_price,
                        "s_amount":self.sell_amount,
                        "b_amount":self.buy_amount,
                        "s_spread":self.t_1 * (Decimal(1) - self.weight_function(self.imb)),
                        "b_spread":self.t_1 * (Decimal(1) + self.weight_function(self.imb)),
                        "balance_imb":self.imb
                    }
                )
                
    
            
    async def strategy_core(self):
        await asyncio.sleep(30)
        await asyncio.gather(
            self.send_buy_order(),
            self.send_sell_order(),
            self.cancel_order_action(),
            self.check_unknown_order(),
            self.calculate_t_1(),
            self.calculate_pos_imb(),
            self.set_cache()
        )
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    