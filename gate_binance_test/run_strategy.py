import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_binance = "binance.1000shib_usdt_swap.swap"
        self.symbol_gate = "gateio.shib_usdt_swap.swap"
        self.ap_binance = Decimal(0)
        self.ap_gate = Decimal(0)
        self.bp_binance = Decimal(0)
        self.bp_gate = Decimal(0)
        self.mp_binance = Decimal(0)
        self.mp_gate = Decimal(0)
        self.ob_update = False

        self.ap_diff = collections.deque(maxlen=10000)
        self.bp_diff = collections.deque(maxlen=10000)
        self.ap_trade_diff = collections.deque(maxlen=10000)
        self.bp_trade_diff = collections.deque(maxlen=10000)
        self.mp_diff = collections.deque(maxlen=10000)

        self.ap_binance_cache = []
        self.bp_binance_cache = []

    async def before_strategy_start(self):
        await self._get_symbol_config_from_remote([self.symbol_binance,self.symbol_gate])
        self.loop.create_task(self._subscribe_orderbook_direct_(self.symbol_gate))
        await self.subscribe_ticker_direct(self.symbol_binance)
        self.direct_subscribe_order_update('gateio',symbols=[self.symbol_gate])
        self.subscribe_public_trade([self.symbol_binance])
        self.ap_diff.append(0)
        self.bp_diff.append(0)
        self.ap_trade_diff.append(0)
        self.bp_trade_diff.append(0)

    # async def on_ticker(self,symbol,ticker):
    #     if self.ap_binance != Decimal(ticker["a"][0]) or self.bp_binance != Decimal(ticker["b"][0]):
    #         self.ap_binance = Decimal(ticker["a"][0])
    #         self.bp_binance = Decimal(ticker["b"][0])

    async def on_public_trade(self, symbol, trade: TradeData):
        if trade.side == TradeSide.Buy:
            self.ap_trade_diff.append(trade.price-self.ap_gate)
        else:
            self.bp_trade_diff.append(self.bp_gate - trade.price)

    async def on_orderbook(self, symbol, orderbook):
        p = orderbook["asks"][0][0]
        if symbol in self.symbol_gate:
            self.ap_gate = orderbook["asks"][0][0] * Decimal(1000)
            self.bp_gate = orderbook["bids"][0][0] * Decimal(1000)
            self.mp_gate = (self.ap_gate + self.bp_gate) / Decimal(2)

        else:
            return

        self.ob_update = True

    async def on_ticker(self, symbol, ticker):
        self.binance_ob_ts = time.time()

        if symbol not in self.symbol_binance:
            return
        if Decimal(ticker["a"][0]) == self.ap_binance or Decimal(ticker["b"][0]) == self.bp_binance:
            return
        
        self.ob_update = True

        self.ap_binance = Decimal(ticker["a"][0])
        self.bp_binance = Decimal(ticker["b"][0])
        self.mp_binance = (self.ap_binance + self.bp_binance) / Decimal(2)
        
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.get_stats(),
            self.set_cache()
        )
    async def get_stats(self):
        while True:
            await asyncio.sleep(10)
            if self.ob_update:
                self.bp_diff.append(float(self.bp_gate-self.ap_binance))
                self.ap_diff.append(float(self.bp_binance-self.ap_gate))
                self.mp_diff.append(float(self.mp_gate-self.mp_binance))
                self.ob_update = False
            self.ap_binance_cache = []
            self.bp_binance_cache = []

    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            res = await self.get_current_position(self.symbol_gate)
            await self.redis_set_cache(
                res
                )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()