import asyncio
import time
from decimal import Decimal

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy


from xex_mm.utils.base import Depth
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_executor.order_executor import OrderExecutorTest, TransOrder
from xex_mm.utils.base import Depth, Trade, Direction, ReferencePriceCalculator

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.use_colo_url = False
        self._symbols = [
            "mexc.axs_usdt_swap.swap.na",
            "binance.axs_usdt_swap.usdt_contract.na",
        ]
        self.ref_prc_cal = ReferencePriceCalculator()
        self.order_cache = dict()
    
    async def before_strategy_start(self):
        for symbol in self._symbols:
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True)
            self.direct_subscribe_order_update(symbol_name=symbol)
        self.executor = OrderExecutorTest(self._symbols)
    
    async def on_order(self, order):
        if self.order_cache.get(order.client_id) is None:
            return
        status = order.xchg_status
        o = self.order_cache.get(order.client_id)
        symbol_config = self.get_symbol_config(o.symbol_id)
        if order.xchg_status in OrderStatus.fin_status():
            if order.filled_amount > 0:
                amt_chg = order.filled_amount if o.side == OrderSide.Buy else - order.filled_amount
                self.executor.update_finished_order(
                    ".".join((symbol_config["exchange_name"],symbol_config["pair"])), 
                    amt_chg)
            self.order_cache.pop(order.client_id)
        
    async def on_orderbook(self, symbol, orderbook):
        lt = orderbook["resp_ts"]
        new_depth = Depth.from_dict(depth=orderbook)
        self.executor.update_depth(symbol.split(".")[0], new_depth)
        
        ref_prc = self.ref_prc_cal.calc(self.executor.get_depth(symbol.split(".")[0]))
        
        if lt > self.genearte_lt + 100:
            self.generate_orders(ref_prc)
            self.genearte_lt = lt
        self.cancel_orders_action()
        
    def generate_orders(self,ref_prc):
        orders = self.executor.get_orders(contract=self.pair, tick_size=self.tick_size, ref_prc=ref_prc)
        for order in orders:
            self.send_order_action(order)
            
    def send_order_action(self, order: TransOrder):
        order_ex, order_pair = order.iid.split(".")
        for oid, o in self.order_cache.items():
            config = self.get_symbol_config(o.symbol_id)
            if o.side == order.side and config["pair"] == order_pair and config["exchange_name"] == order_ex:
                if abs(o.price - order.price) / o.price > self.prc_per_diff:
                    self.loop.create_task(self.handle_cancel_order(o))
                else:
                    self.order_cache[oid].life = 100
                    return
                
        symbol = order.iid
        side = OrderSide.Buy if order.side == Direction.long else OrderSide.Sell
        self.loop.create_task(
            self.send_order(instrument_id=symbol, 
                              side=side,
                              price=order.price,
                              qty=order.quantity,
                              order_type=OrderType.Open)
        )

    async def cancel_orders_action(self):
        while True:
            await asyncio.sleep(0.05)
            for oid, o in self.order_cache.items():
                if o.create_ms < time.time()*1e3 - o.life:
                    self.loop.create_task(self.handle_cancel_order(o))
        
    async def handle_cancel_order(self, order):
        try:
            if self.order_cache.get(order.client_id):
                if order.client_id not in self.canceling_id:
                    self.canceling_id.add(order.client_id)
                    self.order_cache[order.client_id].message["cancel"] = True
                    res = await self.cancel_order(order)
                    if not res:
                        self.order_cache[order.client_id].message["cancel"] = False
        except Exception as err:
            self.logger.warning(f'cancel order {order.client_id} err {err}')
            if order.client_id in self.canceling_id:
                self.canceling_id.remove(order.client_id)
            
    async def send_order(self, symbol, price, qty, side, order_type=OrderType.Limit, recvWindow=100):
        client_id = ClientIDGenerator.gen_client_id("binance")
        
        symbol_cfg = self.get_symbol_config(symbol)
        qty_tick = symbol_cfg["qty_tick_size"]
        price_tick = symbol_cfg['price_tick_size']
        half_step_tick = qty_tick/Decimal(2)
        half_price_tick = price_tick/Decimal(2)
        
        qty += half_step_tick
        price += half_price_tick
        price, qty = self.format_price_volume(symbol, price, qty)
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=symbol,
            price=price,
            volume=qty,
            side=side,
            order_type=order_type,
        )
        extra_info = dict()
        extra_info["cancel"] = False
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
            
        try:
            await self.make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                order_type=order_type,
                client_id=client_id,
                position_side=OrderPositionSide.Open,
                recvWindow=recvWindow,
            )
        except Exception as err:
            self.logger.warning(f"{err}")
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)
    
    async def strategy_core(self):
        await asyncio.sleep(10)
        await asyncio.gather(
            self.cancel_orders_action()
        )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    