
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections

from decimal import Decimal 
import asyncio
import time
import numpy as np

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol_bit = self.symbol_list[0]

    async def before_strategy_start(self):
        await self.direct_subscribe_bbo(symbol_name=self.symbol_bit)
        self.direct_subscribe_orderbook(symbol_name=self.symbol_bit)

    async def on_bbo(self, symbol, bbo):
        self.logger.info(f"{bbo}")
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(60)
            res = await self.get_bbo_data(self.symbol_bit)
            self.logger.info(f"{res}")
    
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()

