import asyncio
from decimal import Decimal
import time 
import pandas as pd

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.xchg = self.config['strategy']['params']['xchg']
        self.pair = self.config['strategy']['params']['pair']
        self.qty = self.config['strategy']['params']['qty']
        self.symbol = self.config['strategy']['symbol_list'][0]
        self.order_cache = dict()
        
        self.send_data = []
        self.cancel_data = []
    
    async def before_strategy_start(self):
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.direct_subscribe_orderbook(symbol_name=self.symbol)
        # self.logger.setLevel("WARNING")
        
    async def on_orderbook(self, symbol, orderbook):
        ap = orderbook["asks"][0][0]
        bp = orderbook["bids"][0][0]
        self.mp = (ap + bp) / Decimal(2)
    
    async def on_order(self, order):
        if order.xchg_status in OrderStatus.fin_status():
            if order.filled_amount > Decimal(0):
                side = self.order_cache[order.xchg_id].side
                self.logger.warning(f"order filled at {order.avg_filled_price},side:{side}")
            self.order_cache.pop(order.xchg_id)
    
    async def main_logic(self):
        while True:
            await asyncio.sleep(1)
            t_send_ts = int(time.time()*1e3)
            t_send, oid = await self.send_order_action()
            if oid:
                t_cancel_ts = int(time.time()*1e3)
                t_cancel = await self.cancel_order_action(oid)
                self.cancel_data.append({"timstamp":t_cancel_ts,"elapsed":t_cancel})
            self.send_data.append({"timstamp":t_send_ts,"elapsed":t_send})
            
    async def save_data(self):
        period = 60*60*24
        self.last_ts = time.time() % period
        while True:
            await asyncio.sleep(1)
            if time.time() % period < self.last_ts:
                data_send = pd.DataFrame(self.send_data)
                data_cancel = pd.DataFrame(self.cancel_data)
                
                data_send.to_csv(f"./data/{self.xchg}_{self.pair}_{int(time.time()*1e3)}_send.csv")
                data_cancel.to_csv(f"./data/{self.xchg}_{self.pair}_{int(time.time()*1e3)}_cancel.csv")
                
                self.send_data = []
                self.cancel_data = []
            self.last_ts = time.time() % period
            
    async def send_order_action(self):
        ts  = time.perf_counter()
        try:
            order = await self.make_order(
                symbol_name=self.symbol,
                price = self.mp * Decimal(0.96),
                volume=Decimal(self.qty),
                side=OrderSide.Buy,
                position_side=OrderPositionSide.Open,
                order_type=OrderType.PostOnly
            )
            extra_info = dict()
            extra_info["stop_ts"] = 5
            order.message = extra_info
            self.order_cache[order.xchg_id] = order
            return time.perf_counter() - ts, order.xchg_id
        except Exception as err:
            self.logger.warning(f"send order err:{err}")
        return time.perf_counter() - ts, None
    
    async def cancel_order_action(self,oid):
        ts  = time.perf_counter()
        try:
            await self.cancel_order(self.order_cache[oid])
        except Exception as err:
            self.logger.warning(f"cancel order err:{err}")
        return time.perf_counter() - ts
    
    async def cancel_in_case(self):
        while True:
            await asyncio.sleep(1)
            try:
                for cid, order in list(self.order_cache.items()):
                    if time.time()*1e3 - order.create_ms > order.message["stop_ts"]*1e3:
                        self.loop.create_task(self.cancel_order(order))
            except Exception as err:
                self.logger.warning(f"cancel in case error: {err}")
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order
    
    async def check_order(self):
        while True:
            await asyncio.sleep(1)
            try:
                for cid, order in list(self.order_cache.items()):
                    if time.time()*1e3 - order.create_ms > order.message["stop_ts"]*1e3 + 1e3:
                        self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{cid}")
                        order_new = await self.get_order_status_direct(order)
                        
                        if self.order_cache.get(cid) is not None and order_new:
                            if order_new.xchg_status in OrderStatus.fin_status():
                                await self.on_order(order_new)
                            else:
                                self.loop.create_task(self.cancel_order(order_new))
            except Exception as err:
                self.logger.warning(f"chcek order failed error: {err}")
                
    async def strategy_core(self):
        await asyncio.gather(
                self.main_logic(),
                self.save_data(),
                self.cancel_in_case(),
                self.check_order()
                )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    
            
            