import asyncio
from decimal import Decimal
import time
import traceback
from strategy_base.base import CommonStrategy
from xex_mm.utils.base import OrderManager, TransOrder

from atom.exceptions import *
from atom.model import *
from atom.helpers import ClientIDGenerator



class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.cancel_ts_dict = dict()
        self.order_manager = dict()
        self.symbol = self.config['strategy']['params']['symbol']
        self.prc = ...
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        
        # api = self.get_exchange_api_by_account(account_name=self.account_names[0])
        # setattr(api,"urls",dict(spot="http://8.218.39.186:38081"))
        
        self.direct_subscribe_orderbook(symbol_name=self.symbol, is_testnet=True)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        
        for acc in self.account_names:
            await asyncio.sleep(1)
            api = self.get_exchange_api_by_account(account_name=acc)
            self.logger.critical(f"cancel order for account: {acc}")
            symbol_cfg = self.get_symbol_config(symbol_identity=self.symbol)
            try:
                await api.flash_cancel_orders(symbol_cfg)
            except:
                self.logger.critical("no open orders")
                
    async def make_ping(self):
        api = self.get_exchange_api_by_account(account_name=self.account_names[0])
        await api.make_request(
            market="spot",
            method="GET",
            endpoint="/v2/timestamp.do"
        )
            
    async def on_orderbook(self, symbol, orderbook):
        try:
            self.prc = orderbook["asks"][0][0]
        except:
            traceback.print_exc()
            
    async def on_order(self, order: PartialOrder):
        try:
            oid = order.xchg_id
            cid = order.client_id
            
            if self.cancel_ts_dict.get(oid) != None and order.xchg_status in OrderStatus.fin_status():
                tlist = self.cancel_ts_dict[oid]
                cancel_action_ts, cancel_finish_ts = tlist
                ts_gap = cancel_finish_ts - cancel_action_ts
                if cancel_finish_ts == 0:
                    ts_gap = int(time.time()*1e3) - cancel_action_ts
                self.logger.critical(f"cancel takes {ts_gap}ms, finished_time={int(time.time()*1e3)}")
                
                self.cancel_ts_dict.pop(oid)
                
            if not self.order_manager.get(oid) == None:
                return
                
            if order.xchg_status in OrderStatus.fin_status() and self.order_manager.get(oid) != None:
                self.order_manager.pop(oid)
        except:
            traceback.print_exc()
            
    async def main_logic(self):
        while True:
            await asyncio.sleep(1)
            if self.prc is Ellipsis:
                continue
            try:
                s = time.time()*1e3
                await self.make_ping()
                self.logger.info(f"elasped time:{int(time.time()*1e3-s)}")
                # resp:Order = await self.make_order(
                #     symbol_name=self.symbol,
                #     price=self.prc * Decimal(0.99),
                #     volume=Decimal(0.01),
                #     side=OrderSide.Buy,
                #     order_type=OrderType.PostOnly,
                #     position_side=OrderPositionSide.Open,
                # )
                # self.order_manager[resp.xchg_id] = resp
                # self.cancel_ts_dict[resp.xchg_id] = [0,0]
                # await asyncio.sleep(0.2)
                # if self.cancel_ts_dict.get(resp.xchg_id) != None:
                #     self.cancel_ts_dict[resp.xchg_id][0] = int(time.time()*1e3)
                # await self.cancel_order(order=resp, catch_error=False)
                # if self.cancel_ts_dict.get(resp.xchg_id) != None:
                #     self.cancel_ts_dict[resp.xchg_id][1] = int(time.time()*1e3)
            except:
                traceback.print_exc()
        
    async def rolling_cancel(self):
        while True:
            await asyncio.sleep(10)
            for oid in list(self.order_manager.keys()):
                if self.order_manager.get(oid):
                    order = self.order_manager[oid]
                    self.loop.create_task(self.cancel_order(order))
                      
                
    async def strategy_core(self):
        api = self.get_exchange_api("lbank")
        self.logger.info(f"{api[0].urls}")
        await asyncio.gather(
            self.main_logic(),
            # self.rolling_cancel()
        )
        

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()