import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.tot = 0
        self.expired = 0
    
    def update_config_before_init(self):
        self.use_colo_url = self.config['strategy']['params']["colo"]
        
    async def execute(self,api):
        await api.make_request(
            market="usdt_contract",
            method="POST",
            endpoint="/fapi/v1/order/test",
            query=dict(symbol="ADAUSDT",side="BUY",type="MARKET",quantity="5",timestamp=str(int(time.time()*1e3))),
            need_sign=True
            )
        return True
        
    async def execute1(self,api):
        await api.make_request(
            market="usdt_contract",
            method="GET",
            endpoint="/fapi/v1/time"
            )
        return True
        
    async def pinging(self):
        api = self.get_exchange_api("binance")[0]
        while True:
            await asyncio.sleep(0.5)
            s = time.time()*1e3
            await self.execute(api)
            t = time.time()*1e3 - s
            self.tot += 1
            if t > 8:
                self.expired += 1
                self.logger.warning(f"order takes time:{t}")
                
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            await self.redis_set_cache({"fail rate":self.expired/self.tot if self.tot else 0})
    
    async def strategy_core(self):
        await asyncio.gather(
            self.pinging(),
            self.set_cache()
        )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()