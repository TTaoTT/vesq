from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "press_prod")

from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from atom.helpers import ClientIDGenerator
from atom.exchange_api.abc import ApiResponse
from atom.exceptions import *

from xex_mm.at_touch_algo_order_strategy.binance.enum import TimeInForce
from xex_mm.at_touch_algo_order_strategy.binance.enum import \
    OrderType as BinanceOrderType
from xex_mm.xex_depth.xex_depth import XExBBO
from xex_mm.utils.enums import Side
from xex_mm.utils.contract_mapping import ContractMapper
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig
from xex_mm.utils.base import Direction, Order, TransOrder, XexInventoryManager, Trade, BBO, Depth, ReferencePriceCalculator, Level
from xex_mm.strategy_configs.at_touch_algo_order_config import Config, QuoteLevelConfig
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard
from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.calculators.price_stability import CalPriceStability, PriceStabilityState
from xex_mm.calculators.latency import CalEMAvgLatency
from xex_mm.at_touch_algo_order_strategy.order_manager import AtTouchBinanceOrderRequestManager, AtTouchHedgeBinanceOrderRequestManager, AtTouchBinanceOrderRequestManagerMap
from xex_mm.at_touch_algo_order_strategy.executor import AtTouchAlgoOrderExecutor, HedgeStopMode
from xex_mm.at_touch_algo_order_strategy.binance.requests import BinanceLimitOrder, BinanceOrderRequest, BinanceStopOrder
from xex_mm.calculators.trade_stability import TradeStabilityState

from strategy_base.base import CommonStrategy
from orderedset import OrderedSet

import ujson as json
import pandas as pd
import numpy as np
from typing import AnyStr, Deque, Dict, Set, Tuple, Union, List
from decimal import Decimal
from collections import defaultdict
from asyncio.queues import LifoQueue, Queue
import traceback
import time
import math
import asyncio
from atom.model.depth import DepthConfig
from atom.logger import BasicLogger

DepthConfig.MAX_DEPTH = 20

class XExDepthManager:
    def __init__(self, contract: str):
        self.contract = contract
        self.depths_map: Dict[str, Depth] = dict()
        self.ref_cal = ReferencePriceCalculator()

    def get_ref_prc(self, ex: str):
        return self.ref_cal.calc_from_depth(depth=self.depths_map[ex], amt_bnd=0)

    def __contains__(self, ex: str):
        return ex in self.depths_map

    def pop(self, ex: str):
        return self.depths_map.pop(ex)

    def on_depth(self, depth: Depth):
        ex = depth.meta_data.ex
        _depth = self.depths_map.get(ex)
        if _depth is not None:
            if depth.server_ts < _depth.server_ts:
                return
        self.depths_map[ex] = depth
        # logging.debug(f"on_depth: ts={self.depths_map[ex].server_ts},best_bid={self.depths_map[ex].best_bid},best_ask={self.depths_map[ex].best_ask}")

    def on_bbo(self, bbo: BBO):
        ex = bbo.meta.ex
        depth = self.depths_map.get(ex)
        if not depth:
            return
        if bbo.server_time < depth.server_ts:
            return
        self.depths_map[ex] = depth.update_depth_with_bbo(bbo=bbo)
        # logging.debug(f"on_bbo: ts={self.depths_map[ex].server_ts},best_bid={self.depths_map[ex].best_bid},best_ask={self.depths_map[ex].best_ask}")

    def on_trade(self, trade: Trade):
        ex = trade.ex
        depth = self.depths_map.get(ex)
        if not depth:
            return
        if trade.tx_time < depth.server_ts:
            return
        self.depths_map[ex] = depth.update_depth_with_trade(trade=trade)
        # logging.debug(f"on_trade: ts={self.depths_map[ex].server_ts},best_bid={self.depths_map[ex].best_bid},best_ask={self.depths_map[ex].best_ask}")

    def on_trigger_limit_sent(self, order_msg: Order):
        ex = order_msg.ex
        depth = self.depths_map.get(ex)
        if not depth:
            return
        if order_msg.tx_time < depth.server_ts:
            return
        self.depths_map[ex] = depth.on_trigger(order_msg=order_msg)
        # logging.debug(f"on_trigger_limit_sent: ts={self.depths_map[ex].server_ts},best_bid={self.depths_map[ex].best_bid},best_ask={self.depths_map[ex].best_ask},cid={order_msg.client_id}")

    def get_asks(self, ex: str) -> List[Level]:
        return self.depths_map[ex].asks

    def get_bids(self, ex: str) -> List[Level]:
        return self.depths_map[ex].bids

    def get_best_ask(self, ex: str):
        return self.depths_map[ex].best_ask

    def get_best_bid(self, ex: str):
        return self.depths_map[ex].best_bid

class AtTouchBinanceOrderRequestManagerMap(AtTouchBinanceOrderRequestManagerMap):
    def get_om(self, cid: str) -> AtTouchBinanceOrderRequestManager:
        if not self.cid_to_ex_map.get(cid):
            return None
        return self.om_map[self.cid_to_ex_map[cid]]

    def get_order(self, cid: str):
        if self.get_om(cid=cid):
            return self.get_om(cid=cid).get_order(cid=cid)
        return None


ACC_LOAD_KEY = "X-MBX-ORDER-COUNT-10S"
IP_LOAD_KEY = "X-MBX-USED-WEIGHT-1M"

class Order(Order):
    @classmethod
    def from_dict(cls, porder: Union[PartialOrder,AtomOrder], local_order: Union[TransOrder,BinanceOrderRequest], contract_value: Decimal):
        xchg_status = porder.xchg_status
        if isinstance(porder, cls):
            return porder
        
        if isinstance(local_order, TransOrder):
            ex, contract = local_order.iid.split(".")
        elif isinstance(local_order, BinanceOrderRequest):
            ex, contract = local_order.piid.split(".")
            
        if isinstance(porder, PartialOrder):
            match_type=porder.extra.get("order_type")
            if porder.raw_data and porder.raw_data.get("o"):
                request_qty = float(porder.raw_data["o"]["q"])
                if porder.raw_data["o"].get("X") == "EXPIRED":
                    xchg_status = OrderStatus.Expired
            else:
                request_qty = local_order.quantity
        elif isinstance(porder, AtomOrder):
            match_type=local_order.order_type_atom
            if porder.raw_data:
                request_qty = float(porder.raw_data["origQty"])
                if porder.raw_data.get("status")== "EXPIRED":
                    xchg_status = OrderStatus.Expired
        return cls(
            contract=contract,
            ex=ex,
            prc=local_order.price,
            qty=request_qty,
            side=local_order.side,
            filled_qty=float(porder.filled_amount * contract_value),
            avg_fill_prc=float(porder.avg_filled_price),
            order_id=porder.xchg_id,
            client_id=local_order.new_client_order_id,
            local_time=porder.local_ms,
            tx_time=porder.server_ms,
            status=xchg_status,
            account_name=porder.account_name,
            match_type=match_type
        )
        
class RateLimitHandler:
    def __init__(self, logger: BasicLogger, loop=asyncio.get_event_loop()) -> None:
        self.hedge_order_ban = False
        self.mm_order_ban = False
        self.execution_ban = False
        
        self.hedge_free_ts = 0
        self.mm_free_ts = 0
        self.execution_free_ts = 0
        self.logger = logger
        loop.create_task(self.free())
    
    async def free(self):
        while True:
            await asyncio.sleep(1)
            curr_time = time.time()
            if self.hedge_order_ban == True and self.hedge_free_ts <= curr_time:
                self.hedge_order_ban = False
            if self.mm_order_ban == True and self.mm_free_ts <= curr_time:
                self.mm_order_ban = False
            if self.execution_ban == True and self.execution_free_ts <= curr_time:
                self.execution_ban = False
                
    def mm_is_ban(self):
        if self.execution_ban or self.hedge_order_ban or self.mm_order_ban:
            return True
        return False
    
    def hedge_is_ban(self):
        if self.execution_ban or self.hedge_order_ban:
            return True
        return False
    
    def execution_is_ban(self):
        return self.execution_ban
    
    def ban_mm_order(self, sec: int):
        free_time = int(time.time()) + sec
        if self.mm_order_ban == False:
            self.logger.critical(f"ban mm order in handler, free at {free_time}")
        self.mm_order_ban = True
        self.mm_free_ts = free_time
        
    def ban_hedge_order(self, sec: int):
        free_time = int(time.time()) + sec
        if self.hedge_order_ban == False:
            self.logger.critical(f"ban hedge order in handler, free at {free_time}")
        self.hedge_order_ban = True
        self.hedge_free_ts = free_time
        
    def ban_execution(self, sec: int):
        free_time = int(time.time()) + sec
        self.logger.critical(f"ban execution in handler, free at {free_time}")
        self.execution_ban = True
        self.execution_free_ts = free_time
        

class HoldingPeriodTracker():
    def __init__(self) -> None:
        self.discret_pos = []
        self.pos = 0

        self.time_qty_sum = 0
        self.qty_sum = 0

    def on_position(self, pos_chg: dict):
        temp = pos_chg["qty"]
        if pos_chg["side"] * self.pos < 0:
            self.pos += temp * pos_chg["side"]
            while self.discret_pos:
                pos = self.discret_pos[0]
                if pos_chg["qty"] > pos["qty"]:
                    self.time_qty_sum += (time.time()-pos["ts"]) * pos["qty"]
                    self.qty_sum += pos["qty"]
                    self.discret_pos.pop(0)
                    pos_chg["qty"] -= pos["qty"]
                elif pos_chg["qty"] < pos["qty"]:
                    self.time_qty_sum += (time.time() -
                                          pos["ts"]) * pos_chg["qty"]
                    self.qty_sum += pos_chg["qty"]
                    pos["qty"] -= pos_chg["qty"]
                    return
                else:
                    self.time_qty_sum += (time.time()-pos["ts"]) * pos["qty"]
                    self.qty_sum += pos["qty"]
                    self.discret_pos.pop(0)
                    return

            self.discret_pos.append(pos_chg)
        else:
            self.pos += temp * pos_chg["side"]
            self.discret_pos.append(pos_chg)
        
    def log_out(self):
        if not self.qty_sum:
            return 0
        res = self.time_qty_sum / self.qty_sum
        self.time_qty_sum = 0
        self.qty_sum = 0
        return res


class StateTracker():
    def __init__(self, logger) -> None:
        self.ref_update = []
        self.send_request = []
        self.cancel_request = []
        self.send_execution = []
        self.cancel_execution = []
        self.inventory_update = []

        self.logger = logger
        self.start_ts = int(time.time()*1e3)

    def add_ref_update(self, best_ask_prc, best_ask_qty, best_bid_prc,
                       best_bid_qty, source, tx_time, timestamp, localtime,
                       in_queue_time, out_queue_time, update_time
                       ):
        self.ref_update.append(
            dict(
                best_ask_prc=best_ask_prc,
                best_ask_qty=best_ask_qty,
                best_bid_prc=best_bid_prc,
                best_bid_qty=best_bid_qty,
                source=source,
                tx_time=tx_time,
                timestamp=timestamp,
                localtime=localtime,
                in_queue_time=in_queue_time,
                out_queue_time=out_queue_time,
                update_time=update_time
            )
        )

    def add_send_request(self, source, decision_time, create_task_time, client_id):
        self.send_request.append(
            dict(
                source=source,
                decision_time=decision_time,
                create_task_time=create_task_time,
                client_id=client_id
            )
        )
        self.logger.info(f"save log send request")

    def add_cancel_request(self, decision_time, create_task_time, client_id):
        self.cancel_request.append(
            dict(
                decision_time=decision_time,
                create_task_time=create_task_time,
                client_id=client_id
            )
        )

    def add_send_execution(self, send_time, finish_time, client_id):
        self.send_execution.append(
            dict(
                send_time=send_time,
                finish_time=finish_time,
                client_id=client_id
            )
        )

    def add_cancel_execution(self, cancel_time, finish_time, client_id):
        self.cancel_execution.append(
            dict(
                cancel_time=cancel_time,
                finish_time=finish_time,
                client_id=client_id
            )
        )

    def add_inventory_update(self, timestamp, localtime, in_queue_time, out_queue_time, client_id, inventory):
        self.inventory_update.append(
            dict(
                timestamp=timestamp,
                localtime=localtime,
                in_queue_time=in_queue_time,
                out_queue_time=out_queue_time,
                client_id=client_id,
                inventory=inventory
            )
        )

    def save_data(self):
        data = pd.DataFrame(self.ref_update)
        data.to_csv(f"./data/ref_update_{self.start_ts}.csv")

        data = pd.DataFrame(self.send_request)
        data.to_csv(f"./data/send_request_{self.start_ts}.csv")

        data = pd.DataFrame(self.cancel_request)
        data.to_csv(f"./data/cancel_request_{self.start_ts}.csv")

        data = pd.DataFrame(self.send_execution)
        data.to_csv(f"./data/send_execution_{self.start_ts}.csv")

        data = pd.DataFrame(self.cancel_execution)
        data.to_csv(f"./data/cancel_execution_{self.start_ts}.csv")

        data = pd.DataFrame(self.inventory_update)
        data.to_csv(f"./data/inventory_update_{self.start_ts}.csv")

        self.ref_update = []
        self.send_request = []
        self.cancel_request = []
        self.send_execution = []
        self.cancel_execution = []
        self.inventory_update = []

        self.start_ts = int(time.time()*1e3)


class LoadBalancer:
    def __init__(self, ccy1: str, ccy2: str, logger) -> None:
        self._position_map = dict()
        self._balance_map = dict()
        self._account_load_map = dict()
        self._ip_load_map = dict()
        self._available_map = dict()
        self._locked_map = dict()

        self.ccy1 = ccy1
        self.ccy2 = ccy2

        self.send_counter = 0
        self.cancel_counter = 0
        self.query_counter = 0

        self.max_leverage = 10
        self.ban_name = set()

        self.logger = logger

    def add_send(self):
        self.send_counter += 1

    def sub_send(self):
        self.send_counter -= 1

    def add_cancel(self):
        self.cancel_counter += 1

    def add_query(self):
        self.query_counter += 1

    def ban_account(self, account_name):
        if account_name:
            self.ban_name.add(account_name)

    def ban_trader(self, trader):
        if trader in self._ip_load_map:
            self._ip_load_map.pop(trader)

    def free_account(self, account_name):
        if account_name in self.ban_name:
            self.ban_name.remove(account_name)

    def setup(self, acc_list: list, trader_list: list):
        acc_list = acc_list.copy()
        trader_list = trader_list.copy()
        for acc in acc_list:
            self._position_map[acc] = 0
            self._balance_map[acc] = 0
            self._account_load_map[acc] = 0
            self._available_map[acc] = dict()
            self._available_map[acc][self.ccy1] = 0
            self._available_map[acc][self.ccy2] = 0
            self._locked_map[acc] = 0

        for trader in trader_list:
            self._ip_load_map[trader] = 0

    def get_acc_list(self):
        return sorted(self._account_load_map, key=self._account_load_map.get)

    def get_trader_list(self):
        return sorted(self._ip_load_map, key=self._ip_load_map.get)

    def get_trader(self):
        return self.get_trader_list()[0]

    def update_available_balance(self, acc: str, available_balance: float, ccy: str):
        self._available_map[acc][ccy] = available_balance

    def split_order(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            new_order = order.clone()
            if order.side == Direction.long:
                temp_qty = self._available_map[acc][ccy2] * 0.99
                if total_qty*order.price > temp_qty:
                    new_order.quantity = temp_qty / order.price
                    new_order.account_name = acc
                    total_qty -= temp_qty / order.price
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break
            elif order.side == Direction.short:
                temp_qty = self._available_map[acc][ccy1] * 0.99
                if total_qty > temp_qty:
                    new_order.quantity = temp_qty
                    new_order.account_name = acc
                    total_qty -= temp_qty
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break

        return splited_orders

    def split_orders_perp(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            new_order = order.clone()
            allowed_qty = self.volume_allowed(
                acc=acc, side=order.side, prc=order.price)
            self.logger.info(
                f"!!!!!!!allowed quantity={allowed_qty}, side={order.side}")
            allowed_qty = 0.1
            if allowed_qty < 0:
                continue
            if total_qty > allowed_qty:
                new_order.quantity = allowed_qty
                new_order.account_name = acc
                splited_orders.append(new_order)
                total_qty -= allowed_qty
            else:
                new_order.quantity = total_qty
                new_order.account_name = acc
                splited_orders.append(new_order)
                break
        return splited_orders

    def send_order_perp(self, order: TransOrder):
        self.logger.info(f"send order:{order.__dict__}")
        if order.side == Direction.long:
            self._locked_map[order.account_name] += order.quantity
        elif order.side == Direction.short:
            self._locked_map[order.account_name] -= order.quantity
        self.logger.info(
            f"locked value:{self._locked_map[order.account_name]}")

    def finished_order_perp(self, torder: TransOrder, order_obj: Order):
        self.logger.info(f"finish torder:{torder.__dict__}")
        self.logger.info(f"finish order_obj:{order_obj.__dict__}")
        if torder.side == Direction.long:
            self._locked_map[torder.account_name] -= torder.quantity
            self._locked_map[torder.account_name] += order_obj.filled_qty
        elif torder.side == Direction.short:
            self._locked_map[torder.account_name] += torder.quantity
            self._locked_map[torder.account_name] -= order_obj.filled_qty
        else:
            raise NotImplementedError()
        self.logger.info(
            f"locked value:{self._locked_map[torder.account_name]}")

    def volume_allowed(self, acc: str, side: Direction, prc: float):
        if side == Direction.long:
            return self._balance_map[acc] * self.max_leverage - self._locked_map[acc] * prc
        else:
            return self._balance_map[acc] * self.max_leverage + self._locked_map[acc] * prc

    def send_order(self, order: TransOrder):
        if not order.account_name:
            raise RuntimeError("order with no account name")
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if order.side == Direction.long:
            self._available_map[order.account_name][ccy2] -= order.price * \
                order.quantity
        elif order.side == Direction.short:
            self._available_map[order.account_name][ccy1] -= order.quantity
        else:
            raise NotImplementedError()

        if self._available_map[order.account_name][ccy1] < 0 or self._available_map[order.account_name][ccy2] < 0:
            raise RuntimeError("order routing wrong")

    def finished_order(self, torder: TransOrder, order_obj: Order):
        _, contract = torder.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if torder.side == Direction.long:
            self._available_map[torder.account_name][ccy2] += torder.price * \
                torder.quantity
            self._available_map[torder.account_name][ccy2] -= order_obj.avg_fill_prc * \
                order_obj.filled_qty
            self._available_map[torder.account_name][ccy1] += order_obj.filled_qty
        elif torder.side == Direction.short:
            self._available_map[torder.account_name][ccy1] += torder.quantity
            self._available_map[torder.account_name][ccy1] -= order_obj.filled_qty
            self._available_map[torder.account_name][ccy2] += order_obj.avg_fill_prc * \
                order_obj.filled_qty
        else:
            raise NotImplementedError()

    def update_balance(self, acc: str, balance: float):
        self._balance_map[acc] = balance

    def update_position(self, acc: str, chg_qty: float):
        self._position_map[acc] += chg_qty

    def update_limit(self, acc: str, trader_name: str, acc_load: Union[str, None], ip_load: Union[str, None]):
        if acc_load != None:
            self._account_load_map[acc] = int(acc_load)
        if ip_load != None:
            self._ip_load_map[trader_name] = int(ip_load)

    def reduce_count(self, reduce: float):
        self._account_load_map[self.get_acc_list()[-1]] -= reduce
        self._ip_load_map[self.get_trader_list()[-1]] -= reduce


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.save_log = StateTracker(logger=self.logger)
        self.time_period = HoldingPeriodTracker()
        self.rate_limit_handler = RateLimitHandler(loop=self.loop, logger=self.logger)
        self.recvWindow = 100
        self.start_ts = time.time()
        self.trd_amt = 0
        self.oid_to_cid = dict()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.bnd_amt_in_coin = Decimal(
            self.config['strategy']['params']['bnd_amt_in_coin'])

        # self.prc_per_diff = 0.00005 / 5
        self._contract_mapper = ContractMapper()

        self.cfg: Config = ...
        self.contract: str = ...

        self._xex = "xex"
        self._numeric_tol = 1e-10

        # TODO
        self._allow_inventory_mismatch_count = 0
        self._on_wakeup_count = 0
        self._last_scheduled_wakeup_ts = ...
        self.ws_queue: Dict[str, Queue] = {
            "trade": Queue(),
            "order": Queue(),
            "orderbook": Queue(),
            "bbo": LifoQueue(),
        }

        self.traders = [
            self.strategy_name,
        ]

        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()
        
        self.lastest_trade = Decimal("0")

    def update_config_before_init(self):
        self.use_colo_url = True

    def init_before_strategy_starts(self):
        self.cfg = Config(
            CONTRACT="btc_usdt_swap",
            EXCHANGES=[
                "binance_busd",
            ],
            PRICE_STABILITY_BUFFER_WINDOW=2,
            FOLDER_NAME=f"",
            QUOTE_LVL_CONFIGS=dict(
                binance_busd=QuoteLevelConfig(
                    STOP_N_TICKS=9, QUOTE_QUANTITY=150, RECV_WINDOW=100),
            ),
            LATENCY_T=100,
            AMOUNT_BOUND=10,
            PRESS_HEDGE_RATIO=0.016,
            MAX_INVEN=5000,
            MSG="",
        )
        self.contract = self.cfg.CONTRACT
        self.depth_manager = XExDepthManager(contract=self.contract)

        self._order_manager_map: AtTouchBinanceOrderRequestManagerMap = AtTouchBinanceOrderRequestManagerMap(contract=self.contract)
        self._inventory_manager = XexInventoryManager(contract=self.cfg.CONTRACT)

        self.executor = AtTouchAlgoOrderExecutor(
            contract=self.contract,
            exs=self.cfg.EXCHANGES,
            quote_level_configs=self.cfg.QUOTE_LVL_CONFIGS,
            amount_bound=self.cfg.AMOUNT_BOUND,
            max_inven=self.cfg.MAX_INVEN,
            press_ratio=self.cfg.PRESS_HEDGE_RATIO
        )
        self.executor.link_inventory_manager(inventory_manager=self._inventory_manager)
        self.executor.link_order_manager_map(order_manager_map=self._order_manager_map)
        self.executor.link_depth_manager(depth_manager=self.depth_manager)

        self._iids = set()
        for ex in self.cfg.EXCHANGES:
            self._iids.add(f"{ex}.{self.cfg.CONTRACT}")

        self._prc_precision = dict()
        for iid in self._iids:
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            self._prc_precision[iid] = price_precision[pp_iid]

        self.latency_cal_map = defaultdict(lambda: CalEMAvgLatency(halflife=self.cfg.LATENCY_T))
        self.price_stability_cal_map = defaultdict(lambda: CalPriceStability(buffer_window=self.cfg.PRICE_STABILITY_BUFFER_WINDOW))


        ccy = self.cfg.CONTRACT.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.load_balancer = LoadBalancer(
            ccy1=ccy1, ccy2=ccy2, logger=self.logger)
        self.load_balancer.setup(
            acc_list=self.account_names,
            trader_list=self.traders
        )

        self.canceling_id = set()
        self.quering_id = set()
        self.quering_triggerd_stop_id = set()

        self.send = False

    def iid_to_binance_symbol(self, iid: str):
        _, contract = iid.split(".")
        ccy = contract.split("_")
        if ccy[-1] == "swap" and ccy[1] in {"usdt", "busd"}:
            symbol = f"{iid}.usdt_contract.na"
        else:
            symbol = f"{iid}.spot.na"
        return symbol

    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        self.init_before_strategy_starts()

        await self.account_preparation()

        for acc in self.account_names:
            ex, _ = acc.split(".")
            for piid in self._iids:
                iid: str = self._contract_mapper.proxy_iid_to_iid(
                    proxy_iid=piid)
                if ex != "binance" or not iid.startswith("binance"):
                    raise NotImplementedError(
                        f"{ex},{iid} piid to symbol mapping is absent")
                symbol = self.iid_to_binance_symbol(iid=iid)
                self.direct_subscribe_order_update(
                    symbol_name=symbol, account_name=acc)
                await asyncio.sleep(1)

        for piid in self._iids:
            iid: str = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
            if not iid.startswith("binance"):
                raise NotImplementedError(
                    f"{iid} piid to symbol mapping is absent")
            symbol = self.iid_to_binance_symbol(iid=iid)

            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            bnd = self.bnd_amt_in_coin / symbol_cfg["contract_value"]
            self.direct_subscribe_orderbook(
                symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_public_trade(symbol_name=symbol)
            self.direct_subscribe_bbo(symbol_name=symbol)

        self.loop.create_task(self.handle_ws_update())

        self.send = True

    async def account_preparation(self):
        # TODO

        for ex in self.cfg.EXCHANGES:
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                ex, _ = acc.split(".")
                for piid in self._iids:
                    iid: str = self._contract_mapper.proxy_iid_to_iid(
                        proxy_iid=piid)
                    if ex != "binance" or not iid.startswith("binance"):
                        raise NotImplementedError(
                            f"{ex},{iid} piid to symbol mapping is absent")
                    symbol = self.iid_to_binance_symbol(iid=iid)
                    try:
                        await api.flash_cancel_orders(self.get_symbol_config(symbol))
                    except:
                        pass

                    cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                    self.load_balancer.update_balance(
                        acc=acc, balance=float(cur_balance["busd"]["all"]))

                    await asyncio.sleep(0.2)
                    cur_position = (await api.contract_position(self.get_symbol_config(symbol))).data
                    self.logger.info(f"current position: {cur_position}")
                    pos = cur_position["long_qty"] - cur_position["short_qty"]

                    self.update_inventory(
                        iid=piid, chg_qty=float(pos), ts=None)

                    await asyncio.sleep(3)

    # websocket handling
    async def on_order(self, order: PartialOrder):
        try:
            await self.ws_queue["order"].put((order, int(time.time()*1e3)))
        except:
            traceback.print_exc()

    async def on_public_trade(self, symbol, trade: PublicTrade):
        try:
            await self.ws_queue["trade"].put((symbol, trade, int(time.time()*1e3)))
        except:
            traceback.print_exc()

    async def on_bbo(self, symbol, bbo: BBODepth):
        try:
            await self.ws_queue["bbo"].put((symbol, bbo, int(time.time()*1e3)))
        except:
            traceback.print_exc()

    async def on_orderbook(self, symbol, orderbook):
        try:
            await self.ws_queue["orderbook"].put((symbol, orderbook, int(time.time()*1e3)))
        except:
            traceback.print_exc()

    def handle_order(self, order: PartialOrder, in_queue_time: int):
        try:
            out_queue_time = int(time.time()*1e3)
            
            if order.xchg_status in OrderStatus.fin_status():
                self.trd_amt += abs(order.filled_amount) * order.avg_filled_price
            
            # self.logger.info(f"on order:{order.__dict__}")
            
            # if order.xchg_status in OrderStatus.fin_status() and order.filled_amount > 0:
            #     self.logger.info(f"order filled:{order.filled_amount}, cid:{order.client_id}, side:{order.extra.get('side')}")
            
            cid = order.client_id
            om = self._order_manager_map
            request_order = om.get_order(cid=cid)
            if not request_order:
                if isinstance(order, PartialOrder) and \
                    order.extra.get("order_type") == OrderType.Limit and \
                        order.xchg_status in OrderStatus.fin_status():
                    self.logger.critical(f"order has been poped wrong, cid={cid}")
                return
        
            # self.logger.info(f"{order.__dict__}")
            order_msg: Order = Order.from_dict(
                porder=order,
                local_order=request_order,
                contract_value=self.get_symbol_config(
                    request_order.symbol)["contract_value"]
            )
            iid = request_order.piid
            ex, contract = iid.split(".")
            
            if cid in om:
                """ Handle Reduce Only Hedge Limits """
                if om.is_hedge_limit(cid=cid):
                    o: BinanceLimitOrder = om.get_order(cid=cid)
                    assert o.reduce_only
                    o.quantity = order_msg.qty
                    o.unfilled_quantity = order_msg.qty - order_msg.filled_qty

                """ Handle Hedge Stop Orders """
                if om.is_hedge_stop(cid=cid) and order_msg.is_limit:
                    o: BinanceStopOrder = om.pop_hedge_stop_order(cid=cid)
                    if o.new_client_order_id in self.quering_triggerd_stop_id:
                        self.quering_triggerd_stop_id.remove(o.new_client_order_id)
                    o.quantity = order_msg.qty
                    o.unfilled_quantity = order_msg.qty - order_msg.filled_qty
                    om.add_hedge_order(order=o.to_limit_order())
                    o.cancel = True

                """ Update Inventory """
                if order_msg.filled_qty > self._numeric_tol:
                    qty_chg = om.get_order_inc_fill_qty_in_tokens(order_msg=order_msg)
                    if qty_chg > self._numeric_tol:
                        inven_chg = order_msg.side * qty_chg
                        self.update_inventory(iid=iid, chg_qty=inven_chg, ts=order_msg.local_time)
                        ex_inven = self._inventory_manager.get_inventory(
                            contract=self.contract, ex=ex)
                        total_inven = self._inventory_manager.total_inventory
                        self.logger.info(f"current inventory:{ex_inven}, cid:{order.client_id}")
                        
                        pos_chg = dict(
                            side=order_msg.side,
                            qty=qty_chg,
                            ts=time.time()
                        )
                        self.time_period.on_position(pos_chg=pos_chg)

                if order_msg.is_limit:
                    om.update_order_msg(order_msg=order_msg)


                """ Clean up """
                order_status = order_msg.status
                if order_status in OrderStatus.fin_status():
                    if om.is_hedge(cid=cid):
                        if om.is_hedge_stop(cid=cid):
                            if order_msg.is_stop:
                                if order_status == OrderStatus.Expired:
                                    om.trigger_stop(ts=self.current_time(), cid=cid)
                                    request_order.canceled = True
                                    # logging.debug(f"ALGO_ORDER {order_status}: ts={ts},oid={oid}")
                                else:
                                    om.pop_hedge_stop_order(cid=cid)
                                    # logging.debug(f"Popped Stop Order {order_status}: ts={ts},oid={oid}")
                        else:
                            if order_msg.is_stop:
                                if order_status == OrderStatus.Expired:
                                    pass
                                else:
                                    raise NotImplementedError()
                            else:
                                # if order_status == OrderStatus.Rejected:
                                #     o = om.get_order(oid=oid)
                                #     logging.debug(f"HEDGE_LIMIT_REJECTED: ts={ts},oid={oid},side={o.side},prc={o.price}")
                                om.pop_order(cid=cid)
                    else:
                        om.pop_order(cid=cid)
                        # logging.debug(f"Pop Order {order_status}: ts={ts},oid={oid}")

                return
        except:
            traceback.print_exc()

    def handle_trade(self, symbol: str, trade: PublicTrade, in_queue_time: int):
        self.lastest_trade = trade.price
        out_queue_time = int(time.time()*1e3)

        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        trade_obj = Trade.from_dict_prod(
            ex=ex,
            contract=contract,
            trade=trade
        )

        if not trade_obj.is_valid:
            return

        """ Latency state """
        self.latency_cal_map[ex].add_trade(trade=trade_obj)
        latency = self.latency_cal_map[ex].get_factor(ts=trade_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        self.depth_manager.on_trade(trade=trade_obj)

        price_stability_cal = self.price_stability_cal_map[ex]
        price_stability_cal.add_trade(trade=trade_obj)
        price_stable = price_stability_cal.get_factor(ts=trade_obj.local_time)
        depth = self.depth_manager.depths_map.get(trade_obj.ex)
        if depth is None:
            return
        price_stability_cal.add_bbo(bbo=depth.to_bbo())

        if price_stable == PriceStabilityState.stable:
            self.update_hedge_orders()
            self.update_mm_orders()
        else:
            wakeup_ts = trade_obj.local_time + self.cfg.PRICE_STABILITY_BUFFER_WINDOW
            if (self._last_scheduled_wakeup_ts is not Ellipsis) and (wakeup_ts <= self._last_scheduled_wakeup_ts):
                return
            self.loop.create_task(
                self.insertWakeup(data=dict(
                timestamp=wakeup_ts,
                contract=trade_obj.contract,
                ex=trade_obj.ex,
            )))
            self._last_scheduled_wakeup_ts = wakeup_ts

    async def insertWakeup(self, data):
        await asyncio.sleep(self.cfg.PRICE_STABILITY_BUFFER_WINDOW)
        self.onWakeup(data)

    def handle_bbo(self, symbol: str, bbo: BBODepth, in_queue_time: int):
        out_queue_time = int(time.time()*1e3)
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        bbo_obj = BBO.from_dict_prod(iid=iid, bbo_depth=bbo)

        if not bbo_obj.valid_bbo:
            return

        contract = bbo_obj.meta.contract
        ex = bbo_obj.meta.ex
        server_ts = bbo_obj.server_time
        local_ts = bbo_obj.local_time

        ism = self.iid_state_manager
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Latency state """
        self.latency_cal_map[ex].add_bbo(bbo=bbo_obj)
        latency = self.latency_cal_map[ex].get_factor(ts=bbo_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        """ Server time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(
                iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.is_active(iid=iid):
            return

        self.depth_manager.on_bbo(bbo=bbo_obj)

        price_stability_cal = self.price_stability_cal_map[ex]
        success = price_stability_cal.add_bbo(bbo=bbo_obj)
        if not success:
            return
        price_stable = price_stability_cal.get_factor(ts=bbo_obj.local_time)
        if price_stable == PriceStabilityState.stable:
            self.update_hedge_orders()
            self.update_mm_orders()

    def handle_orderbook(self, symbol: str, orderbook, in_queue_time: int):
        out_queue_time = int(time.time()*1e3)
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        ccy = contract.split("_")
        depth_obj = Depth.from_dict(
            contract=contract,
            ccy1=ccy[0],
            ccy2=ccy[1],
            ex=ex,
            depth=orderbook
        )
        ex = depth_obj.meta_data.ex
        contract = depth_obj.meta_data.contract
        server_ts = depth_obj.server_ts
        local_ts = depth_obj.resp_ts

        """ Latency state """
        self.latency_cal_map[ex].add_depth(depth=depth_obj)
        latency = self.latency_cal_map[ex].get_factor(ts=depth_obj.resp_ts)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=True)

        """ tx time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(
                iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        ism = self.iid_state_manager
        """ Activate stale state """
        is_in_stale_state = ism.is_in_stale_state(iid=iid)
        if is_in_stale_state:
            ism.enter_nonstale_state(iid=iid)
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.active_iids:
            return

        if not ism.is_active(iid=iid):
            return

        self.depth_manager.on_depth(depth=depth_obj)

        price_stability_cal = self.price_stability_cal_map[ex]
        success = price_stability_cal.add_bbo(bbo=depth_obj.to_bbo())
        if not success:
            return
        price_stable = price_stability_cal.get_factor(ts=depth_obj.resp_ts)
        if price_stable == PriceStabilityState.stable:
            self.update_hedge_orders()
            self.update_mm_orders()

    async def handle_ws_update(self):
        while True:
            try:
                if not self.ws_queue["order"].empty():
                    order, in_queue_time = await self.ws_queue["order"].get()
                    self.handle_order(order, in_queue_time)
                    continue
                elif not self.ws_queue["orderbook"].empty():
                    symbol, orderbook, in_queue_time = await self.ws_queue["orderbook"].get()
                    self.handle_orderbook(symbol, orderbook, in_queue_time)
                    continue
                elif not self.ws_queue["trade"].empty():
                    symbol, trade, in_queue_time = await self.ws_queue["trade"].get()
                    self.handle_trade(symbol, trade, in_queue_time)
                    continue
                elif not self.ws_queue["bbo"].empty():
                    symbol, bbo, in_queue_time = await self.ws_queue["bbo"].get()
                    self.ws_queue["bbo"] = LifoQueue()
                    self.handle_bbo(symbol, bbo, in_queue_time)
                await asyncio.sleep(0)
            except:
                await asyncio.sleep(0)
                traceback.print_exc()

    # base operation, send, cancel, query with load balancer
    def sendBinanceOrder(self, order: BinanceOrderRequest):
        self.trim_order_before_sending(order)
        self.loop.create_task(self.base_send_order(order=order))
        
    def sendBinanceOrderDouble(self, orders: List[BinanceOrderRequest]):
        for order in orders:
            self.trim_order_before_sending(order)
        self.loop.create_task(self.base_send_order_double(orders=orders))
        
    async def base_send_order_double(self, orders: List[BinanceOrderRequest]):
        for order in orders:
            self.loop.create_task(self.base_send_order(order=order))
            await asyncio.sleep(0.005)
        
    async def base_send_order(self, order: BinanceOrderRequest):
        error_code=0
        if not self.volume_notional_check(symbol=order.symbol, price=order.price, qty=order.quantity):
            self.loop.create_task(self.internal_fail(order=order, error_code=error_code))
            return
        try:
            origin_resp = await self.send_order(order=order)
            if isinstance(origin_resp, ApiResponse):
                resp = origin_resp.data
                headers = origin_resp.headers
            else:
                resp = None
                headers = None
                error_code = origin_resp

            if headers:
                self.load_balancer.update_limit(
                    acc=order.account_name,
                    trader_name=self.strategy_name,
                    acc_load=headers.get(ACC_LOAD_KEY),
                    ip_load=headers.get(IP_LOAD_KEY)
                )
                self.logger.info(f"order load:{int(headers.get(ACC_LOAD_KEY))}, ip load:{headers.get(IP_LOAD_KEY)}")
                if headers.get(ACC_LOAD_KEY) and int(headers.get(ACC_LOAD_KEY)) > 100:
                    self.handle_rate_limit(msg=0)
                    if int(headers.get(ACC_LOAD_KEY)) > 200:
                        self.handle_rate_limit(msg=-1015)
            if resp:
                order.update(order=resp)
            else:
                self.loop.create_task(self.internal_fail(order=order, error_code=error_code))

        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"base send order err:{err}")
            self.loop.create_task(self.internal_fail(order=order, error_code=error_code))
            

    async def send_order(self, order: BinanceOrderRequest):
        symbol = order.symbol
        try:
            self._account_config_[
                order.account_name]["cfg_pos_mode"][symbol] = 1
            side = self.direction_map(side=order.side)

            symbol_cfg = self.get_symbol_config(symbol)
            pt = symbol_cfg['price_tick_size']
            send_time = int(time.time()*1e3)
            # self.logger.info(f"sending order with ordertype:{order.order_type_atom},cid:{order.new_client_order_id}")
            if order.stop_price > 0:
                new_stop_price = Decimal(f"{int((Decimal(order.stop_price) + pt/Decimal('2')) / pt) * pt:f}")
                # here for preventing trigger immediately
                if order.side == Side.long and self.lastest_trade >= new_stop_price:
                    return -2021
                elif order.side == Side.short and self.lastest_trade <= new_stop_price:
                    return -2021
                resp: ApiResponse = await self.raw_make_order(
                    symbol_name=symbol,
                    price=Decimal(order.price) +
                    symbol_cfg["price_tick_size"]/Decimal("2"),
                    volume=Decimal(order.quantity) +
                    symbol_cfg["qty_tick_size"]/Decimal("2"),
                    side=side,
                    order_type=order.order_type_atom,
                    position_side=order.position_side_atom,
                    client_id=order.new_client_order_id,
                    account_name=order.account_name,
                    recvWindow=self.recvWindow,
                    stopPrice=new_stop_price,
                    reduce_only=order.reduce_only
                )
            else:
                resp: ApiResponse = await self.raw_make_order(
                    symbol_name=symbol,
                    price=Decimal(order.price) +
                    symbol_cfg["price_tick_size"]/Decimal("2"),
                    volume=Decimal(order.quantity) +
                    symbol_cfg["qty_tick_size"]/Decimal("2"),
                    side=side,
                    order_type=order.order_type_atom,
                    position_side=order.position_side_atom,
                    client_id=order.new_client_order_id,
                    account_name=order.account_name,
                    recvWindow=self.recvWindow,
                    reduce_only=order.reduce_only
                )
            # self.save_log.add_send_execution(send_time=send_time, finish_time=int(time.time()*1e3), client_id=order.new_client_order_id)
            return resp

        except RateLimitException as err:
            code = int(json.loads(err.err_message).get("code"))
            self.logger.info(f"rate limit exception code:{code}")
            self.handle_rate_limit(msg=code)
            return code 
        except Exception as err:
            code = int(json.loads(err.err_message).get("code"))
            self.logger.info(f"send order err:{err}, cid:{order.new_client_order_id}")
            if code == -2022:
                ex,_ = order.piid.split(".")
                self.logger.info(f"current inventory={self._inventory_manager.get_inventory(contract=self.contract, ex=ex)}")
                om = self.get_hedge_order_manager(ex)
                self.logger.info(f"HEDGE LIMIT OUTSTANDING_ORDERS:")
                for oid, o in om.outstanding_orders.items():
                    if isinstance(o, BinanceLimitOrder):
                        self.logger.info(f"oid={oid},o={o}")
            return code
    
    def resend_stop_order(self, order:BinanceOrderRequest, error_code: int):
        if order.type != BinanceOrderType.STOP or error_code in {-2021, -1003}:
            return False
        
        self.logger.info(f"resend code:{error_code}, cid={order.new_client_order_id}")

        ex, _ = order.piid.split(".")
        ex_inven = self._inventory_manager.get_inventory(
                        contract=self.contract, ex=ex)
        if order.side * ex_inven > self._numeric_tol:
            return False
        
        order.sent_ts = int(time.time()*1e3)
        best_bf, back_bf, best_af, back_af, _, _,_,_ = self.executor.get_stop_prc_ranges(ex=ex)
        if order.side == Side.long and best_af <= order.price <= back_af:
            self.loop.create_task(self.base_send_order(order=order))
            return True
        elif order.side == Side.short and back_bf <= order.price <= best_bf:
            self.loop.create_task(self.base_send_order(order=order))
            return True
        return False
    
    def send_cancel_order(self, order: BinanceOrderRequest):
        self.loop.create_task(self.handle_cancel_order(order=order))

    async def handle_cancel_order(self, order: BinanceOrderRequest):
        async def unit_cancel_order(order: BinanceOrderRequest):
            try:
                cancel_time = int(time.time()*1e3)
                origin_resp: ApiResponse = await self.cancel_order(order=order)
                # self.save_log.add_cancel_execution(cancel_time=cancel_time, finish_time=int(time.time()*1e3), client_id=order.new_client_order_id)
                if origin_resp:
                    resp = origin_resp.data
                    headers = origin_resp.headers
                else:
                    resp = False
                    headers = None

                if resp == True:
                    order.canceled = True
                    # self.logger.info(f"confirmed cancel cid={order.new_client_order_id}")
                if headers:
                    self.load_balancer.update_limit(
                        acc=order.account_name,
                        trader_name=self.strategy_name,
                        acc_load=headers.get(ACC_LOAD_KEY),
                        ip_load=headers.get(IP_LOAD_KEY)
                    )
                    if headers.get(IP_LOAD_KEY) and int(headers.get(IP_LOAD_KEY)) > 2000:
                        self.handle_rate_limit(msg=-1015)
            except Exception as err:
                self.logger.critical(f'cancel order {order.xchg_id} err {err}')
        
        cid = order.new_client_order_id
        if cid in self.canceling_id:
            return

        self.canceling_id.add(cid)

        counter = 0
        while True:
            o: BinanceOrderRequest = self._order_manager_map.get_order(cid=cid)
            if o:
                if o.canceled:
                    self.canceling_id.remove(cid)
                    return
            else:
                self.canceling_id.remove(cid)
                return
            if not self.rate_limit_handler.execution_is_ban():
                counter += 1
                self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(0.2)

            if counter > 30:
                o = self._order_manager_map.get_order(cid=cid)
                self.logger.critical(f"cancel order for too many times, cid={o.new_client_order_id}")
                if o:
                    o.canceled = True

    async def cancel_order(self, order: BinanceOrderRequest, catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        try:
            symbol = self.get_symbol_config(order.symbol)
            api = self.get_exchange_api_by_account(order.account_name)
            r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.new_client_order_id)
            self.logger.info(
                f"[cancel order]: {order.xchg_id}/{order.new_client_order_id} {r.data}")
            return r
        except RateLimitException as err:
            self.logger.critical(f"cancel order failed with rate limit: {err}")
            self.handle_rate_limit(msg=-1003)
            return None
        except Exception as err:
            if not catch_error:
                raise
            self.logger.info(f"cancel order failed: {err}, cid={order.new_client_order_id}")
            return None
        
    async def unit_query_order(self, order: BinanceOrderRequest):
        try:
            origin_resp = await self.query_order(order=order)
            if origin_resp:
                resp = origin_resp.data
                headers = origin_resp.headers
            else:
                resp = None
                headers = None

            if headers:
                self.load_balancer.update_limit(
                    acc=order.account_name,
                    trader_name=self.strategy_name,
                    acc_load=headers.get(ACC_LOAD_KEY),
                    ip_load=headers.get(IP_LOAD_KEY)
                )
                if headers.get(IP_LOAD_KEY) and int(headers.get(IP_LOAD_KEY)) > 2000:
                    self.handle_rate_limit(msg=-1015)
        except:
            return
        
        if not resp:
            return
        if resp.xchg_status in OrderStatus.fin_status():
            await self.on_order(resp)

    async def handle_query_order(self, order: BinanceOrderRequest):
        cid = order.new_client_order_id
        if time.time()*1e3 - order.sent_ts < 10*60*1000 or cid in self.quering_id:
            return

        self.quering_id.add(cid)

        counter = 0
        while True:
            o = self._order_manager_map.get_order(cid=cid)
            if o:
                if time.time()*1e3 - order.sent_ts < 10*60*1000:
                    self.quering_id.remove(cid)
                    return
            else:
                self.quering_id.remove(cid)
                return

            if not self.rate_limit_handler.execution_is_ban():
                counter += 1
                self.loop.create_task(self.unit_query_order(order))
            await asyncio.sleep(60)

            if counter > 100:
                o = self._order_manager_map.get_order(cid=cid)
                if o:
                    self.loop.create_task(self.internal_fail(order=o, error_code=0))
                    self.logger.critical(f"query order for too many times, cid={o.new_client_order_id}")
                    
    async def handle_query_expired_stop_order(self, order: BinanceOrderRequest):
        if self.rate_limit_handler.execution_is_ban():
            return
        if self.current_time() - order.timestamp < 10000:
            return
        
        cid = order.new_client_order_id
        if cid in self.quering_triggerd_stop_id:
            return 
        
        self.quering_triggerd_stop_id.add(cid)
        
        
        res: ApiResponse = await self.query_order(order=order)
        if not res:
            return
        
        res_order: AtomOrder = res.data
        
        # pop pending trigger only if the type is still stop and status is expired
        if res_order.raw_data:
            self.logger.critical(f"stop order expired check:{res_order.raw_data}")
            if res_order.raw_data.get("status") == "EXPIRED" and res_order.raw_data.get("type") == "STOP" and res_order.raw_data.get("updateTime"):
                if self.current_time() - res_order.raw_data.get("updateTime") > 10000:
                    self._order_manager_map.pop_pending_trigger_stop(cid=order.new_client_order_id)
                    self.logger.critical(f"stop order actually expired, cid:{order.new_client_order_id}")
        
        if cid in self.quering_triggerd_stop_id:
            self.quering_triggerd_stop_id.remove(cid)
            
        if res.headers:
            self.load_balancer.update_limit(
                acc=order.account_name,
                trader_name=self.strategy_name,
                acc_load=res.headers.get(ACC_LOAD_KEY),
                ip_load=res.headers.get(IP_LOAD_KEY)
            )
            if res.headers.get(IP_LOAD_KEY) and int(res.headers.get(IP_LOAD_KEY)) > 2000:
                self.handle_rate_limit(msg=-1015)
        
    async def query_order(self, order: BinanceOrderRequest):
        try:
            acc = order.account_name
            if not acc:
                acc = self.get_exchange_api_by_account(self.account_names[0])
            api = self.get_exchange_api_by_account(order.account_name)
            resp: ApiResponse = await api.order_match_result(
                symbol=self.get_symbol_config(order.symbol),
                order_id=order.xchg_id,
                client_id=order.new_client_order_id,
            )
            return resp
        except RateLimitException as err:
            self.logger.critical(f"query order failed with rate limit: {err}")
            self.handle_rate_limit(msg=-1003)
            return None
        except Exception as err:
            self.logger.critical(f"query order failed: {err}, cid={order.new_client_order_id}")
            return None

    def handle_rate_limit(self,msg: int):
        if msg == -1003:
            self.logger.info(f"ban execution:{msg}")
            self.rate_limit_handler.ban_execution(sec=30)
        elif msg == -1015:
            # self.logger.info(f"ban stop hedge orders:{msg}")
            self.rate_limit_handler.ban_hedge_order(sec=10)
        elif msg == 0:
            # self.logger.info(f"ban mm orders:{msg}")
            self.rate_limit_handler.ban_mm_order(sec=10)
        
    # strategy base function override
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob

    # strategy utils only for prod
    def direction_map(self, side: Union[Direction, Side]):
        if side == Direction.long or side == Side.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell

    def current_time(self):
        return int(time.time()*1e3)

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def internal_fail(self, order: BinanceOrderRequest, error_code: int):
        if self.resend_stop_order(order=order, error_code=error_code):
            return
        try:
            order = PartialOrder(
                account_name=order.account_name,
                xchg_id=None,
                exchange_pair="",
                client_id=order.new_client_order_id,
                xchg_status=OrderStatus.Failed,
                filled_amount=0,
                avg_filled_price=0,
                commission_fee=0,
                local_ms=int(time.time() * 1e3),
                server_ms=int(time.time() * 1e3),
                raw_data={},
                extra=dict(
                    order_type=order.order_type_atom,
                    price=order.price,
                    quantity=order.quantity
                )
            )
            await self.on_order(order)
        except:
            traceback.print_exc()

    def trim_order_before_sending(self, order: BinanceOrderRequest):
        iid = self._contract_mapper.proxy_iid_to_iid(order.piid)
        ex, _ = iid.split(".")
        order.generate_symbol(self._contract_mapper)
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        order.symbol_id = self.get_symbol_config(order.symbol)["id"]
        order.new_client_order_id = cid
        order.sent_ts = int(time.time()*1e3)
        order.account_name = self.account_names[0]
        # if order.side == Direction.long:
        #     order.price = round_decimals_down(
        #         order.price, price_precision[iid])
        # else:
        #     order.price = round_decimals_up(
        #         order.price, price_precision[iid])

        # order.quantity = round(order.quantity, qty_precision[iid])
        
        self._order_manager_map.cid_to_ex_map[cid] = order.piid.split(".")[0]

        return cid

    def check_orders(self, order_manager: AtTouchBinanceOrderRequestManager):
        for oid, order in order_manager.get_outstanding_orders().items():
            self.loop.create_task(self.handle_query_order(order=order))
            
        for oid, order in order_manager.mm_orm.pending_cancel_orders.items():
            self.loop.create_task(self.handle_query_order(order=order))
            
        for oid, order in order_manager.h_orm.pending_cancel_orders.items():
            self.loop.create_task(self.handle_query_order(order=order))
            
    def check_expired_stop_orders(self, orders_list: Deque[BinanceStopOrder]):
        for order in orders_list:
            self.loop.create_task(self.handle_query_expired_stop_order(order=order))


    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))

    # strategy utils for prod and backtest
    def update_inventory(self, iid: str, chg_qty: float, ts: int):
        ex, contract = iid.split(".")
        self._inventory_manager.increment_inventory(
            contract=contract, ex=ex, qty_chg=chg_qty)

    def deactivate(self, iid: str):
        ex, contract = iid.split(".")
        if ex in self.cfg.EXCHANGES:
            if ex in self.depth_manager:
                self.depth_manager.pop(ex=ex)

        self.cancel_all_orders(ex=ex)

    def _update_latency(self, iid: str, latency: float, allow_rectivate: bool):
        ism = self.iid_state_manager
        is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
        is_active = ism.is_active(iid=iid)
        if not is_in_high_latency_state:
            if ism.is_high_latency(iid=iid, latency=latency):
                ism.enter_high_latency_state(iid=iid)
                if is_active:
                    self.deactivate(iid=iid)
        else:  # is_in_high_latency_state
            if not ism.is_high_latency(iid=iid, latency=latency):
                if allow_rectivate:
                    ism.enter_normal_latency_state(iid=iid)

    def _update_ref_prc_suit(self, contract: str, ex: str):
        success = self._ref_prc_xex_bbo.update_ref_prc(ex=ex)
        if not success:
            return False
        return True

    def onWakeup(self, data):
        self._on_wakeup_count += 1
        ts = data["timestamp"]
        ex = data["ex"]
        price_stability_cal = self.price_stability_cal_map[ex]
        price_stable = price_stability_cal.get_factor(ts=ts)
        if price_stable == PriceStabilityState.stable:
            self.update_hedge_orders()
            self.update_mm_orders()

    def update_hedge_orders(self) -> bool:
        ts = self.current_time()
        trade_decision_map = self.executor.get_hedge_orders(t=ts)
        for ex, trade_decisions in trade_decision_map.items():

            for oid in trade_decisions.limit_orders_to_cancel:
                self.cancel_hedge_limit_order(cid=oid)
                
            has_ioc = False

            while trade_decisions.ioc_orders_to_send:
                order = trade_decisions.ioc_orders_to_send.popleft()
                order.tag = "press_ioc"
                self.send_hedge_limit_order(order=order)
                has_ioc = True

            while not trade_decisions.limit_orders_to_send.empty():
                order = trade_decisions.limit_orders_to_send.get()
                order.tag = "press" if has_ioc else "hedge_limit"
                self.send_hedge_limit_order(order=order)
            if trade_decisions.stop_mode == HedgeStopMode.REFRESH:
                # str_msg = ""
                # for int_prc, orders in trade_decisions.stop_send_new.items():
                #     str_msg += f"\tint_prc={int_prc}\n"
                #     for o in orders:
                #         str_msg += f"\t\torder={o}\n"
                # self.logger.info(f"""refreshing, 
                #                  need to send {str_msg}, 
                #                  need to cancel{trade_decisions.stop_orders_to_cancel} 
                #                  """)
                self.refresh_hedge_stop_orders(ex=ex,
                                               orders_lvls=trade_decisions.stop_send_new,
                                               prc_lvls=trade_decisions.stop_send_new_trigger_prc_lvls,
                                               orders_to_cancel=trade_decisions.stop_orders_to_cancel,
                                               side=trade_decisions.stop_side)
            elif trade_decisions.stop_mode == HedgeStopMode.FIX:
                # str_msg_front = ""
                # for int_prc, orders in trade_decisions.stop_fill_front_orders_to_send.items():
                #     str_msg_front += f"\tint_prc={int_prc}\n"
                #     for o in orders:
                #         str_msg_front += f"\t\torder={o}\n"
                # str_msg_back = ""
                # for int_prc, orders in trade_decisions.stop_fill_back_orders_to_send.items():
                #     str_msg_back += f"\tint_prc={int_prc}\n"
                #     for o in orders:
                #         str_msg_back += f"\t\torder={o}\n"
                # self.logger.info(f"""
                #                  fix, cancel back {trade_decisions.stop_cancel_back_int_prcs}, 
                #                  fill front {str_msg_front},
                #                  fill back {str_msg_back}
                #                  """)
                self.fix_hedge_stop_orders(ex=ex,
                                           cancel_front_int_prcs=trade_decisions.stop_cancel_front_int_prcs,
                                           cancel_back_int_prcs=trade_decisions.stop_cancel_back_int_prcs,
                                           send_front_orders=trade_decisions.stop_fill_front_orders_to_send,
                                           send_back_orders=trade_decisions.stop_fill_back_orders_to_send,
                                           send_front_trigger_prc_lvls=trade_decisions.stop_fill_front_trigger_prc_lvls,
                                           send_back_trigger_prc_lvls=trade_decisions.stop_fill_back_trigger_prc_lvls)
            elif trade_decisions.stop_mode == HedgeStopMode.DO_NOTHING:
                pass
            else:
                raise NotImplementedError()
            # if trade_decisions.msg:
            #     self.logger.info(f"trade decision msg:{trade_decisions.msg}")
        
            # self.logger.info(f"ref prc:{self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex)}")
            # self.logger.info(f"stop_prc_lvls:{self.get_hedge_order_manager(ex=ex).stop_prc_lvls}")
            # om = self.get_hedge_order_manager(ex)
            # self.logger.info(f"HEDGE OUTSTANDING_ORDERS:")
            # for oid, o in om.outstanding_orders.items():
            #     self.logger.info(f"oid={oid},o={o}")
            # self.logger.info(f"HEDGE PENDING_CANCEL_ORDERS: {om.pending_cancel_orders}")
        return True

    def update_mm_orders(self):
        ts = self.current_time()
        trade_decision_map = self.executor.get_mm_orders(t=ts)
        for ex, trade_decisions in trade_decision_map.items():
            for oid in trade_decisions.limit_orders_to_cancel:
                # self.logger.info(f"need to cancel:{oid}")
                self.cancel_mm_limit_order(ex=ex, oid=oid)
            while not trade_decisions.limit_orders_to_send.empty():
                order = trade_decisions.limit_orders_to_send.get()
                # self.logger.info(f"need to send:{order}")
                order.tag = "mm"
                self.send_mm_limit_order(order=order)
                
            # self.logger.info(f"ref prc:{self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex)}")
            # om = self._order_manager_map.om_map[ex].mm_orm
            # self.logger.info(f"MM OUTSTANDING_ORDERS:")
            # for oid, o in om.outstanding_orders.items():
            #     self.logger.info(f"oid={oid},o={o}")
            # self.logger.info(f"MM PENDING_CANCEL_ORDERS: {om.pending_cancel_orders}")

    def get_hedge_order_manager(self, ex: str) -> AtTouchHedgeBinanceOrderRequestManager:
        return self._order_manager_map.om_map[ex].h_orm

    def send_mm_limit_order(self, order: BinanceOrderRequest) -> bool:
        iid = order.piid
        assert iid in self._iids
        order.timestamp = self.current_time()
        if not self.send:
            return
        if self.rate_limit_handler.mm_is_ban():
            return
        ex, _ = iid.split(".")
        # self.logger.info(f"before send {order}")
        order.reduce_only = False
        self.sendBinanceOrder(order=order)
        self._order_manager_map.add_mm_order(order=order)
        # self.logger.info(
        #     f"""
        #     bid outstanding cid={self._order_manager_map.om_map[ex].mm_orm.outstanding_bid_oid}, 
        #     ask outstanding cid={self._order_manager_map.om_map[ex].mm_orm.outstanding_ask_oid}
        #     """
        #     )

    def send_hedge_limit_order(self, order: BinanceLimitOrder) -> bool:
        order.timestamp = self.current_time()
        if not self.send:
            return
        if self.rate_limit_handler.hedge_is_ban():
            return
        order.reduce_only = True
        
        if order.tag == "press":
            orders = [order]
            for _ in range(3):
                orders.append(order.clone())
            self.sendBinanceOrderDouble(orders=orders)
            for trimed_order in orders:
                self._order_manager_map.add_hedge_order(order=trimed_order)
        else:
            self.sendBinanceOrder(order=order)
            self._order_manager_map.add_hedge_order(order=order)

    def _send_hedge_stop_order(self, order: BinanceStopOrder) -> bool:
        order.timestamp = self.current_time()
        if not self.send:
            return
        # self.logger.info(f"send hedge stop order:{order}")
        order.reduce_only = True
        self.sendBinanceOrder(order=order)

    def cancel_mm_limit_order(self, ex: str, oid: str) -> bool:
        om = self._order_manager_map.om_map[ex]
        o = om.get_order(cid=oid)
        if o is None:
            raise RuntimeError()
        if not o.cancel:
            self.send_cancel_order(order=o)
            om.cancel_order(cid=oid)

    def cancel_hedge_limit_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_order(cid=cid)
        if o is None:
            raise RuntimeError()
        if not o.cancel:
            self.send_cancel_order(order=o)
            self._order_manager_map.cancel_order(cid=cid)

    def cancel_hedge_stop_order(self, cid: str) -> bool:
        o = self._order_manager_map.get_outstanding_order(cid=cid)
        if o is None:
            raise RuntimeError()
        if not o.cancel:
            self.send_cancel_order(order=o)

    def cancel_all_hedge_stop_orders(self, ex: str, orders_to_cancel: Set[str]) -> bool:
        stop_orders = self._order_manager_map.get_outstanding_hedge_stop_orders(ex=ex)
        for oid in stop_orders:
            assert oid in orders_to_cancel
            if oid not in orders_to_cancel:
                self.logger.info(f"!!!!!!!,cid={oid}")
            o = self._order_manager_map.get_outstanding_order(cid=oid)
            if o is None:
                self.logger.info(f"???????,cid={oid}")
                raise RuntimeError()
            if not o.cancel:
                self.send_cancel_order(order=o)
        self._order_manager_map.cancel_all_outstanding_hedge_stop_orders(ex=ex)

    def send_new_hedge_stop_orders(self,
                                   ex: str,
                                   order_lvls: Dict[int, Deque[BinanceStopOrder]],
                                   prc_lvls: Deque[Tuple[float, int]],
                                   side: str) -> bool:
        if self.rate_limit_handler.hedge_is_ban():
            return
        for int_trigger_prc, orders in order_lvls.items():
            for order in orders:
                self._send_hedge_stop_order(order=order)
        return self._order_manager_map.update_stop_orders(
            ex=ex, order_lvls=order_lvls, prc_lvls=prc_lvls, side=side)

    def refresh_hedge_stop_orders(self,
                                  ex: str,
                                  orders_lvls: Dict[int, Deque[BinanceStopOrder]],
                                  prc_lvls: Deque[Tuple[float, int]],
                                  orders_to_cancel: Set[str],
                                  side: str) -> bool:

        self.cancel_all_hedge_stop_orders(
            ex=ex, orders_to_cancel=orders_to_cancel)
        self.send_new_hedge_stop_orders(ex=ex,
                                        order_lvls=orders_lvls,
                                        prc_lvls=prc_lvls,
                                        side=side)

    def cancel_front_hedge_stop_orders(self, ex: str, int_prcs: Deque[int]) -> bool:
        stop_lvls_map = self._order_manager_map.get_outstanding_hedge_stop_lvls_map(ex=ex)
        for int_prc in int_prcs:
            for oid in stop_lvls_map[int_prc]:
                self.cancel_hedge_stop_order(cid=oid)
            self._order_manager_map.cancel_outstanding_front_hedge_stop_order(ex=ex, int_prc=int_prc)

    def cancel_back_hedge_stop_orders(self, ex: str, int_prcs: Deque[int]) -> bool:
        stop_lvls_map = self._order_manager_map.get_outstanding_hedge_stop_lvls_map(ex=ex)
        for int_prc in int_prcs:
            for oid in stop_lvls_map[int_prc]:
                self.cancel_hedge_stop_order(cid=oid)

            self._order_manager_map.cancel_outstanding_back_hedge_stop_order(ex=ex, int_prc=int_prc)

    def send_fill_front_hedge_stop_orders(self, ex: str,
                                          orders: Dict[int, Deque[BinanceStopOrder]],
                                          trigger_prc_lvls: Deque[Tuple[float, int]],
                                          ) -> bool:
        if not self.send:
            return
        if self.rate_limit_handler.hedge_is_ban():
            return
        for float_trigger_prc, int_trigger_prc in reversed(trigger_prc_lvls):
            lvl_orders = orders[int_trigger_prc]
            for o in lvl_orders:
                self._send_hedge_stop_order(order=o)
            self._order_manager_map.add_front_stop_orders(
                ex=ex,
                float_trigger_prc=float_trigger_prc,
                int_trigger_prc=int_trigger_prc,
                orders=orders[int_trigger_prc]
            )


    def send_fill_back_hedge_stop_orders(self, ex: str,
                                         orders: Dict[int, Deque[BinanceStopOrder]],
                                         trigger_prc_lvls: Deque[Tuple[float, int]],
                                         ) -> bool:
        if not self.send:
            return
        
        if self.rate_limit_handler.hedge_is_ban():
            return
        for float_trigger_prc, int_trigger_prc in trigger_prc_lvls:
            lvl_orders = orders[int_trigger_prc]
            for o in lvl_orders:
                self._send_hedge_stop_order(order=o)
            self._order_manager_map.add_back_stop_orders(
                ex=ex,
                float_trigger_prc=float_trigger_prc,
                int_trigger_prc=int_trigger_prc,
                orders=orders[int_trigger_prc]
            )


    def fix_hedge_stop_orders(self,
                              ex: str,
                              cancel_front_int_prcs: Deque[int],
                              cancel_back_int_prcs: Deque[int],
                              send_front_orders: Dict[int, Deque[BinanceStopOrder]],
                              send_front_trigger_prc_lvls: Deque[Tuple[float, int]],
                              send_back_orders: Dict[int, Deque[BinanceStopOrder]],
                              send_back_trigger_prc_lvls: Deque[Tuple[float, int]],
                              ) -> bool:
        self.cancel_front_hedge_stop_orders(
            ex=ex, int_prcs=cancel_front_int_prcs)
        self.cancel_back_hedge_stop_orders(
            ex=ex, int_prcs=cancel_back_int_prcs)
        self.send_fill_front_hedge_stop_orders(
            ex=ex, orders=send_front_orders, trigger_prc_lvls=send_front_trigger_prc_lvls)
        self.send_fill_back_hedge_stop_orders(
            ex=ex, orders=send_back_orders, trigger_prc_lvls=send_back_trigger_prc_lvls)

    def cancel_all_orders(self, ex: str) -> bool:
        om = self._order_manager_map.om_map[ex]
        for oid, o in om.get_outstanding_orders().items():
            self.send_cancel_order(order=o)
        om.cancel_all()

    # strategy main logic
    async def reset_pending_cancel_and_hedge_order(self):
        while True:
            await asyncio.sleep(1)
            for _, om in self._order_manager_map.om_map.items():
                self.check_orders(order_manager=om)
                
    async def reset_triggered_stop_order(self):
        while True:
            await asyncio.sleep(1)
            for ex, _ in self._order_manager_map.om_map.items():
                orders_list = self._order_manager_map.get_pending_trigger_orders(ex=ex)
                if orders_list:
                    self.loop.create_task(self.handle_query_expired_stop_order(order=orders_list[0]))
                
    async def load_balancer_reduce_count(self):
        while True:
            await asyncio.sleep(1)
            self.load_balancer.reduce_count(1)

    # stats
    async def get_holding_period(self):
        while True:
            await asyncio.sleep(300)
            try:
                self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                f"holding period": float(self.time_period.log_out()),
                            }
                        )
                    )
            except Exception as err:
                self.logger.critical(f"sending holding period info to grafana err: {err}")
                traceback.print_exc()
                
    async def get_inventory(self):
        while True:
            await asyncio.sleep(5)
            try:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt",
                        tag={"sn": self.strategy_name},
                        fields={
                            "inventory": float(self._inventory_manager.get_inventory(contract=self.cfg.CONTRACT, ex=self.cfg.EXCHANGES[0])),
                        }
                    )
                )
            except Exception as err:
                self.logger.critical(f"sending inventory info to grafana err: {err}")
                traceback.print_exc()
                
    
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(20)
            if not self.send:
                continue
            if self.rate_limit_handler.execution_is_ban():
                continue
            try:
                base_amt = 0

                for dymm_ex in self.cfg.EXCHANGES:
                    for acc in self.account_names:
                        api = self.get_exchange_api_by_account(acc)
                        piid = f"{dymm_ex}.{self.cfg.CONTRACT}"
                        iid = self._contract_mapper.proxy_iid_to_iid(
                            proxy_iid=piid)

                        symbol = f"{iid}.{self.market}.{self.sub_market}"

                        await asyncio.sleep(0.2)
                        cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data

                        base_amt += float(cur_balance["busd"]["all"])
                        self.load_balancer.update_balance(
                            acc=acc, balance=float(cur_balance["busd"]["all"]))
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt",
                                tag={"sn": self.strategy_name},
                                fields={
                                    f"{acc}_acc": float(self.load_balancer._account_load_map[acc])
                                }
                            )
                        )

                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                "balance": float(base_amt),
                            }
                        )
                    )

                for trader in list(self.load_balancer._ip_load_map.keys()):
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                f"{trader}_trader": float(self.load_balancer._ip_load_map[trader])
                            }
                        )
                    )

                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt",
                        tag={"sn": self.strategy_name},
                        fields={
                            "trade_amount_per_second": float(float(self.trd_amt)/(time.time() - self.start_ts)),
                        }
                    )
                )


            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
                
    async def close_all_position(self, symbol):
        try:
            for _ in range(2):
                for acc in self.account_names:
                    api = self.get_exchange_api_by_account(acc)
                    cur_pos = (await api.contract_position(self.get_symbol_config(symbol))).data
                    pos = cur_pos["long_qty"] - cur_pos["short_qty"]
                    if pos == Decimal(0):
                        continue
                    await self.make_order(
                        symbol_name=symbol,
                        price=Decimal(0),
                        volume=abs(pos),
                        side = OrderSide.Buy if pos < 0 else OrderSide.Sell,
                        position_side=OrderPositionSide.Close,
                        order_type= OrderType.Market,
                        account_name=acc,
                    )
                    await asyncio.sleep(0.5)
        except:
            traceback.print_exc()

    async def update_redis_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    self.rate_limit_handler.ban_execution(sec = 100)
                    await asyncio.sleep(5)
                    
                    for ex in self.cfg.EXCHANGES:
                        for acc in self.account_names:
                            api = self.get_exchange_api_by_account(acc)
                            ex, _ = acc.split(".")
                            for piid in self._iids:
                                iid: str = self._contract_mapper.proxy_iid_to_iid(
                                    proxy_iid=piid)
                                if ex != "binance" or not iid.startswith("binance"):
                                    raise NotImplementedError(
                                        f"{ex},{iid} piid to symbol mapping is absent")
                                symbol = self.iid_to_binance_symbol(iid=iid)
                                try:
                                    await api.flash_cancel_orders(self.get_symbol_config(symbol))
                                except:
                                    pass

                                await asyncio.sleep(0.2)
                                await self.close_all_position(symbol)
                                
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache(
                    {
                        "exit": None,
                    }
                )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def log_save_to_local(self):
        while True:
            await asyncio.sleep(60 * 15)
            self.save_log.save_data()

    # strategy core

    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.reset_pending_cancel_and_hedge_order(),
                self.reset_triggered_stop_order(),
                self.get_local_stats(),
                self.get_holding_period(),
                self.get_inventory(),
                self.update_redis_cache(),
                self.load_balancer_reduce_count(),
                # self.log_save_to_local()
            )


if __name__ == '__main__':
    # logging.getLogger().setLevel("WARNING")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
