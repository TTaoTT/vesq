from strategy_base.base import CommonStrategy
from atom.model import PartialOrder, OrderStatus

import asyncio
from decimal import Decimal
from collections import deque
import numpy as np
import time
import pandas as pd

import ujson as json
import traceback

ACC_STRAT_MAP = {
    "binance.mm12":"btc press2",
    "binance.mm13":"eth press2",
    "binance.mm14":"eth press3",
}

class Cache:
    def __init__(self) -> None:
        self.reset()
    
    def reset(self):
        self.stop_pnl = Decimal("0")
        self.limit_pnl = Decimal("0")
        self.stop_amt = Decimal("0")
        self.limit_amt = Decimal("0")
        
    def get_stats(self):
        
        res = dict()
        res["stop_pnl_perU"] = float(self.stop_pnl / self.stop_amt) if self.stop_amt > 0 else float(0)
        res["limit_pnl_perU"] = float(self.limit_pnl / self.limit_amt) if self.limit_amt > 0 else float(0)
        
        total_amt = self.stop_amt + self.limit_amt
        res["stop_per"] = float(self.stop_amt / total_amt) if total_amt > 0 else float(0)
        res["limit_per"] = float(self.limit_amt / total_amt) if total_amt > 0 else float(0)
        
        self.reset()
        
        return res
        
    def update(self, order: PartialOrder):
        raw_order = order.raw_data.get("o")
        if not raw_order:
            return
        if raw_order.get("n"):
            pnl = Decimal(raw_order["rp"]) - Decimal(raw_order["n"])
        else:
            pnl = Decimal(raw_order["rp"])
        
        if raw_order.get("R") == True and raw_order.get("ot") == "STOP":
            self.stop_pnl += pnl
            if order.xchg_status in OrderStatus.fin_status():
                self.stop_amt += abs(order.filled_amount) * order.avg_filled_price
        elif raw_order.get("R") == True and raw_order.get("ot") == "LIMIT" and raw_order.get("f") == "GTX":
            self.limit_pnl += pnl
            if order.xchg_status in OrderStatus.fin_status():
                self.limit_amt += abs(order.filled_amount) * order.avg_filled_price
      
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.last_ts = 0
        self.cache = dict()
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        
        for acc in self.account_names:
            self.cache[acc] = Cache()
            self.direct_subscribe_order_update(
                        symbol_name="binance.btc_busd_swap.usdt_contract.na", account_name=acc)
            
    async def on_order(self, order: PartialOrder): 
        self.cache[order.account_name].update(order)
    
    async def push_influx_data(self, measurement, tag, fields):
        try:
            dt = {
                "timestamp": int(time.time() * 1e3),
                "measurement": measurement,
                "tag": tag,
                "fields": fields
            }
            await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        except:
            traceback.print_exc()
            
    async def strategy_core(self):
        while True:
            await asyncio.sleep(0.1)
            curr_ts = time.time() % 3600
            if curr_ts < self.last_ts:
                try:
                    for acc in self.account_names:
                        res = self.cache[acc].get_stats()
                        self.logger.info(f"acc={acc}, res={res}")
                        for k,v in res.items():
                            self.logger.info(f"{k},{v}")
                            self.loop.create_task(
                                    self.push_influx_data(
                                        measurement="analysis",
                                        tag={"sn": acc},
                                        fields={k:float(v)}
                                    )
                                )
                except:
                    traceback.print_exc()
            self.last_ts = curr_ts
            
            
if __name__ == '__main__':
    # logging.getLogger().setLevel("WARNING")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
