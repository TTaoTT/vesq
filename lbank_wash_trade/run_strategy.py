import random
import os

os.environ["ATOM_PRINT_RESPONSE"] = "0"
from strategy_base import *


class MyStrategy(CommonStrategy):

    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.strategy_params = self.config["strategy"]["params"]
        self.symbols = dict()
        self.bn_symbol = []
        for item in self.strategy_params["coin_list"]:
            base_coin = item["coin"]
            self.symbols[f"lbank.{base_coin}_usdt_swap.swap.na"] = dict(current=0)
            self.bn_symbol.append(f"binance.{base_coin}_usdt_swap.usdt_contract.na")

    def update_config_before_init(self):
        self.use_colo_url = True
        self.custom_symbol_config_exchanges.add("lbank")
        self.custom_symbol_config_exchanges.add("binance")

    async def otc_order(self, params):
        """
             params: {"buyOffsetFlag":"0", "price":16000,"sellOffsetFlag":0,"symbol":"BTCUSDT", "volume":"0.1234"}
             参数参考文档，这几个是最少参数
             """
        api = self.get_exchange_api("lbank")[0]
        api._use_colo_url_ = True
        try:
            response = await api.make_request(
                market=SWAP,
                method=HTTP_POST,
                endpoint="/cfd/openApi/v1/prv/sendOTCTradeInsert",
                body=params,
                need_sign=True
            )
            return response
        
        except Exception as err:
            self.logger.error(f"otc order fail: {params} {err}")
        # 正确返回 response.json = {"data":"BTCUSDT","error_code":0,"msg":"Success","result":"true","success":true}
        # 错误返回  {"data":"44,VolumeNotOnTick","error_code":0,"msg":"Success","result":"true","success":true}， data那里有问题，像这个报错是 volume不是tick size整数倍

    async def on_public_trade(self, symbol, trade: PublicTrade):
        
        
        if time.time() * 1e3 - trade.server_ms > 3e3:
            return
        r = random.choice([1, 2, 3])
        if r != 1:
            return
        ex, pair = symbol.split(".")
        if ex != "binance":
            return
        
        lb_symbol = f"lbank.{pair}.swap.na"
        rand_ = random.randint(100, 1000) / 10000
        q = float(trade.quantity) * rand_
        try:
            p, q = self.format_price_volume(lb_symbol, trade.price, str(q))
            if p * q < 20:
                return
        except ParameterError:
            return
        params = dict(symbol=self.get_symbol_config(lb_symbol)["exchange_pair"])
        params["buyOffsetFlag"] = "0"
        params["sellOffsetFlag"] = "0"
        params["price"] = str(p)
        params["volume"] = str(q)
        params["takerDirection"] = "0" if trade.side == OrderSide.Buy else "1"
        # self.logger.info(params)
        response = await self.otc_order(params)
        if response is not None and response.json["success"] is True and response.json["data"] == params["symbol"]:
            self.symbols[lb_symbol]["current"] += p * q


    # async def process_one(self, symbol):
    #     while True:
    #         await asyncio.sleep(random.randint(5, 10))
    #         depth = await self.get_orderbook_data_redis(symbol)
    #         if not depth:
    #             continue
    #         if len(depth["asks"]) * len(depth["bids"]) == 0:
    #             continue
    #         asks1 = depth["asks"][0]
    #         bids1 = depth["bids"][0]
    #         params = dict(symbol=self.get_symbol_config(symbol)["exchange_pair"])
    #         # {"buyOffsetFlag":"0", "price":16000,"sellOffsetFlag":0,"symbol":"BTCUSDT", "volume":"0.1234"}
    #         rand_ = random.randint(1000, 3000) / 10000
    #         if random.choice([0, 1]) == 0:
    #             price = asks1[0]
    #             direction = "0"  # 买
    #             vol = float(asks1[1]) * rand_
    #         else:
    #             price = bids1[0]
    #             direction = "1"  # 卖
    #             vol = float(bids1[1]) * rand_
    #         try:
    #             p, q = self.format_price_volume(symbol, price, str(vol))
    #         except ParameterError:
    #             continue
    #         params["buyOffsetFlag"] = "0"
    #         params["sellOffsetFlag"] = "0"
    #         params["price"] = str(p)
    #         params["volume"] = str(q)
    #         params["takerDirection"] = direction
    #         self.logger.info(params)
    #         # todo: insert
    #         n = p * q
    #         self.symbols[symbol]["current"] += n
    #         if self.symbols[symbol]["current"] >= self.symbols[symbol]["target"]:
    #             self.logger.info(f"{symbol} finish target, current={self.symbols[symbol]['current']}")
    #             return

    async def before_strategy_start(self):
        self.subscribe_public_trade(self.bn_symbol)

    async def strategy_core(self):
        # for _s in self.symbols:
        #     self.loop.create_task(self.process_one(_s))
        while True:
            await asyncio.sleep(10)  # 避免退出告警，此处用死循环模拟主逻辑
            report = "notional report:\n"
            for sb, cfg in self.symbols.items():
                report += f"{self.get_symbol_config(sb)['base']}: {cfg['current']:.2f} U\n"
            self.logger.info(report)


if __name__ == '__main__':
    s = MyStrategy(asyncio.get_event_loop())
    s.run_forever()
