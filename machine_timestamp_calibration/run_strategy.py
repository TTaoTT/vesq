import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

'/fapi/v1/time'

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.cache = collections.deque(maxlen=10000)
        self.xchg = self.config['strategy']['params']['xchg']
        self.market = self.config['strategy']['params']['market']
        self.request = self.config['strategy']['params']['request']
        self.key = self.config['strategy']['params']['key']

    async def pinging(self):
        api = self.get_exchange_api(self.xchg)[0]
        while True:
            await asyncio.sleep(1)
            s = time.time()*1e3
            res = await api.make_request(
                market=self.market,
                method="GET",
                endpoint=self.request
                )
            t = (time.time()*1e3 + s) / 2
            t_server = res.json[self.key]
            self.cache.append(t - t_server)
            
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            await self.redis_set_cache(
                {
                    "mean":np.mean(self.cache) if self.cache else None,
                    "median":np.median(self.cache) if self.cache else None,
                    "75":np.quantile(self.cache,0.75) if self.cache else None,
                    "25":np.quantile(self.cache,0.25) if self.cache else None,
                    }
                )
            
    
    async def strategy_core(self):
        await asyncio.gather(
            self.pinging(),
            self.set_cache()
        )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()