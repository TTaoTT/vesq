# import os
# os.environ["ATOM_PRINT_RESPONSE"] = '1'

from strategy_base.base import CommonStrategy
import time
import asyncio
from asyncio.queues import Queue, LifoQueue
import traceback

from atom.model import OrderSide, PartialOrder, OrderStatus, BBODepth, OrderPositionSide, OrderType
from atom.model.depth import Depth as AtomDepth
from atom.model.order import Order as AtomOrder
from atom.exceptions import OrderNotFoundError
from typing import Set, AnyStr, Union, Dict, List, Tuple
from decimal import Decimal
import numpy as np

from collections import deque, defaultdict


class OrderCache:
    def __init__(self, max_order_count: float, tolerance:float) -> None:
        self.cache = dict()
        self.max_order_count = max_order_count
        self.tolerance = tolerance
        
    def before_send_check(self, side: OrderSide):
        outstanding_counter = 0
        for oid in self.cache:
            order: AtomOrder = self.cache[oid]
            if order.side == side and order.message["cancel"] == False:
                outstanding_counter += 1
        
        if outstanding_counter >= self.max_order_count:
            return False

        return True
    
    def after_send(self, order: AtomOrder):
        self.cache[order.xchg_id] = order
        order.message = dict()
        order.message["cancel"] = False
        
    def before_cancel_check(self, side, price):
        outstanding_counter = 0
        cancel_order = None
        for oid in self.cache:
            order: AtomOrder = self.cache[oid]
            if order.side == side and order.message["cancel"] == False:
                outstanding_counter += 1
                if not cancel_order and abs(np.log(float(order.requested_price / price))) > self.tolerance:
                    cancel_order = order
                
        if outstanding_counter >= self.max_order_count:
            return cancel_order
        else:
            return None
    
    def before_cancel(self, order: AtomOrder):
        order.message["cancel"] = True
    
    def on_msg(self, order: Union[PartialOrder, AtomOrder]):
        if order.xchg_id not in self.cache:
            return False
        
        if order.xchg_status in OrderStatus.fin_status():
            self.cache.pop(order.xchg_id)
        
        return True
    

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.send = False
        
        self.contracts = {
            # "btc_usdt_swap": "lbank.mm1",
            # "eth_usdt_swap": "lbank.mm2",
            "gmt_usdt_swap": "lbank.mm3",
            # "xrp_usdt_swap": "lbank.mm4",
            # "ape_usdt_swap": "lbank.mm5",
            # "matic_usdt_swap": "lbank.mm6",
            # "ada_usdt_swap": "lbank.mm7",
            # "gala_usdt_swap": "lbank.mm8",
            # "axs_usdt_swap": "lbank.mm9",
            # "sol_usdt_swap": "lbank.mm10",
            # "sand_usdt_swap": "lbank.mm11",
            # "etc_usdt_swap": "lbank.mm12",
            # "atom_usdt_swap": "lbank.mm13",
            # "near_usdt_swap": "lbank.mm14",
            # "doge_usdt_swap": "lbank.mm15",
        }
        
        self.gap = 0.002
        self.vol = float(self.config['strategy']['params']['vol'])
        self.max_order_count = float(self.config['strategy']['params']['max_order_count'])
        
        self.mp = dict()
        self.ret = defaultdict(deque)
        self.gap_map = dict()
        self.om = dict()
        self.unknown_order_dict = dict()
        for contract in self.contracts:
            self.om[contract] = OrderCache(max_order_count=self.max_order_count, tolerance=self.gap*0.1)
            self.unknown_order_dict[contract] = dict()
            
        self.close_order_cache = OrderCache(max_order_count=self.max_order_count, tolerance=self.gap*0.1)
        
        self.order_queue = Queue()
        self.orderbook_queue = LifoQueue()
        
        self.canceling_id = set()
        self.quering_id = set()
        
        self.symbol_config_by_exchange_pair = dict()
        self.all_pair = set()
        
    async def update_max_order_volume(self):
        while True:
            await asyncio.sleep(3600)
            try:
                for contract in self.contracts:
                    symbol = f"lbank.{contract}.swap.na"
                    symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                    symbol_cfg["max_order_volume"] = await self.get_max_order_volume(exchange_pair=symbol_cfg["exchange_pair"])
            except:
                pass
    
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        all_config = self.all_symbol_config()
        for contract in self.contracts:
            self.direct_subscribe_orderbook(symbol_name=f"binance.{contract}.usdt_contract.na", is_incr_depth=True)
            
            symbol = f"lbank.{contract}.swap.na"
            all_config[symbol]["max_order_volume"] = await self.get_max_order_volume(exchange_pair=all_config[symbol]["exchange_pair"])
            
            self.symbol_config_by_exchange_pair[all_config[symbol]["exchange_pair"]] = all_config[symbol]
            self.all_pair.add(all_config[symbol]["exchange_pair"])
        
       
        self.loop.create_task(self.handle_ws_update())
            
        for contract in self.contracts:
            try:
                account_name = self.contracts[contract]
                self.direct_subscribe_order_update(symbol_name=f"lbank.{contract}.swap.na",account_name=account_name)
                api = self.get_exchange_api_by_account(account_name=account_name)
                symbol_cfg = self.get_symbol_config(symbol_identity=f"lbank.{contract}.swap.na")
                som: OrderCache = self.om[contract]
                all_opening_orders: List[AtomOrder] = (await api.all_opening_orders(symbol_cfg)).data
                for order in all_opening_orders:
                    som.after_send(order)
            except:
                self.logger.info(f"{traceback.format_exc()}")
                
        
        self.send = True
                    
    async def get_max_order_volume(self, exchange_pair):
        api = self.get_exchange_api(exchange_name="lbank")[0]
        response = await api.make_request("swap", "GET", "/cfd/openApi/v1/pub/instrument", query={"productGroup":"SwapU"}, need_sign=False)
        config_list = response.json["data"]
        for config in config_list:
            if config["symbol"] == exchange_pair:
                self.logger.info(config)
                return Decimal(config["maxOrderVolume"])
            
    async def on_order(self, order: PartialOrder):
        if isinstance(order, PartialOrder) and order.exchange_pair not in self.all_pair:
            return
        try:
            await self.order_queue.put(order)
        except:
            traceback.print_exc()
    
    def handle_order(self, order: Union[PartialOrder, AtomOrder]):
        if isinstance(order, AtomOrder):
            sc = self.get_symbol_config(symbol_identity=order.symbol_id)
            exchange_pair = sc["exchange_pair"]
        else:
            exchange_pair = order.exchange_pair
        
        symbol_cfg = self.symbol_config_by_exchange_pair[exchange_pair]
        contract = symbol_cfg["pair"]
        if not self.om[contract].on_msg(order=order) and not self.close_order_cache.on_msg(order=order):
            self.unknown_order_dict[contract][order.xchg_id] = order
        
            
    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.01)
            now_know_order = {}
            for contract in self.contracts:
                for xchg_id, partial_order in self.unknown_order_dict[contract].copy().items():
                    if xchg_id in self.om[contract].cache or xchg_id in self.close_order_cache.cache:
                        now_know_order[xchg_id] = partial_order
                        self.unknown_order_dict[contract].pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(partial_order) for xchg_id, partial_order in now_know_order.items()]
            )
        
    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            await self.orderbook_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")
        
    def handle_orderbook(self, symbol: str, orderbook: Dict):
        try:
            ex, contract = symbol.split(".")
            ap = orderbook["asks"][0][0]
            bp = orderbook["bids"][0][0]
            
            curr_mp = (ap + bp) / 2
            if self.mp.get(contract):
                last_price = self.mp.get(contract)
                self.ret[contract].append(abs(np.log(float(curr_mp/last_price))))

            self.mp[contract] = curr_mp
        except:
            traceback.print_exc()
    
    async def handle_ws_update(self):
        while True:
            try:
                if not self.order_queue.empty():
                    order = await self.order_queue.get()
                    self.handle_order(order)
                    continue
                elif self.orderbook_queue.empty():
                    await asyncio.sleep(0.001)
                else:
                    symbol, orderbook = await self.orderbook_queue.get()
                    self.orderbook_queue = LifoQueue()
                    self.handle_orderbook(symbol=symbol, orderbook=orderbook)

                await asyncio.sleep(0.001)
            except:
                await asyncio.sleep(0.001)
                traceback.print_exc()
                
    async def base_send_order(self, contract, symbol, side, som:OrderCache):
        if not self.send:
            return
        price = self.mp.get(contract)
        if not price:
            return
        gap = self.gap_map.get(contract)
        if not gap:
            return
        
        try:
            price = Decimal(price * Decimal(1+gap)) if side == OrderSide.Sell else Decimal(price * Decimal(1-gap))
            volume = Decimal(self.vol) / price
            max_order_vol = self.get_symbol_config(symbol_identity=symbol).get("max_order_volume")
            if max_order_vol:
                volume = min(max_order_vol,volume)
            else:
                return
            order:AtomOrder = await self.make_order(
                symbol_name=symbol,
                price=price,
                volume=volume,
                side=side,
                position_side=OrderPositionSide.Open,
                account_name=self.contracts[contract]
                )
            som.after_send(order=order)
            
            
        except Exception as err:
            self.logger.warning(f"send order err:{err}")  
              
    async def main_logic(self):
        while True:
            await asyncio.sleep(5)
            for contract in self.contracts:
                symbol = f"lbank.{contract}.swap.na"
                som: OrderCache = self.om[contract]
                
                price = self.mp.get(contract)
                if not price:
                    continue
                
                if som.before_send_check(side=OrderSide.Buy):
                    self.loop.create_task(self.base_send_order(contract=contract, symbol=symbol, side=OrderSide.Buy, som=som))
                if som.before_send_check(side=OrderSide.Sell):
                    self.loop.create_task(self.base_send_order(contract=contract, symbol=symbol, side=OrderSide.Sell, som=som))
                   
                buy_cancel_order: AtomOrder = som.before_cancel_check(side=OrderSide.Buy, price=price)
                sell_cancel_order: AtomOrder = som.before_cancel_check(side=OrderSide.Sell, price=price)
                if buy_cancel_order:
                    som.before_cancel(buy_cancel_order)
                    self.loop.create_task(self.handle_cancel_order(order=buy_cancel_order, som=som))
                if sell_cancel_order:
                    som.before_cancel(sell_cancel_order)
                    self.loop.create_task(self.handle_cancel_order(order=sell_cancel_order, som=som))
                    
    async def handle_query_order(self, order: AtomOrder, som: OrderCache):
        async def unit_query_order(order: AtomOrder, counter:int):
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(
                    order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=order.client_id,
                )
                new_order: AtomOrder = res.data
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
            except OrderNotFoundError as err:
                counter += 1
                self.logger.critical(
                    f"order not found: {err}, oid={order.xchg_id}, cid={order.client_id}")
            except Exception as err:
                self.logger.critical(
                    f"direct check order error: {err}, oid={order.xchg_id}, cid={order.client_id}")
        
        oid = order.xchg_id
        window = 10000
        if oid not in self.canceling_id or order.message.get("cancel") == False:
            return
        
        if oid in self.quering_id:
            return
        
        if time.time()*1e3 - order.create_ms  < window:
            return

        self.quering_id.add(oid)
        
        counter = 0
        while True:
            if som.cache.get(oid) == None:
                self.quering_id.remove(oid)
                return
            if order.xchg_id:
                await unit_query_order(order, counter=counter)
                if counter > 0:
                    som.cache.pop(oid)
                    self.logger.critical(f"order not found pop, order id={order.xchg_id}, cid={order.client_id}")
                    self.quering_id.remove(oid)
                    return
            await asyncio.sleep(30)
                    
    async def handle_cancel_order(self, order: AtomOrder, som: OrderCache):
        async def unit_cancel_order(order: AtomOrder):
            try:
                await self.cancel_order(order)
            except Exception as err:
                self.logger.critical(f'cancel order err {err}')

        oid = order.xchg_id
        if oid in self.canceling_id:
            return

        self.canceling_id.add(oid)

        while True:
            if som.cache.get(oid) == None:
                self.canceling_id.remove(oid)
                return
                
            if order.xchg_id:
                await unit_cancel_order(order)
            await asyncio.sleep(1)
            
        
    async def cancel_order(self, order: AtomOrder, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.data
        except Exception as err:
            if not catch_error:
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False
            
    async def update_redis_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    for acc in self.account_names:
                        api = self.get_exchange_api_by_account(acc)
                        for contract in self.contracts:
                            symbol_cfg = self.get_symbol_config(symbol_identity=f"lbank.{contract}.swap.na")
                            try:
                                await api.flash_cancel_orders(symbol_cfg)
                            except:
                                self.logger.info("no open orders")
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                om_len = 0
                unknown_order_dict_len = 0
                for contract in self.contracts:
                    om_len += len(self.om[contract].cache)
                    unknown_order_dict_len += len(self.unknown_order_dict[contract])
                await self.redis_set_cache(
                    {
                    "exit": None,
                    "om len":om_len,
                    "onknown order len":unknown_order_dict_len,
                    "close_order_cache_len":len(self.close_order_cache.cache),
                    "order_queue_len": self.order_queue.qsize(),
                    "orderbook_queue_len": self.orderbook_queue.qsize(),
                    "canceling_id_len":len(self.canceling_id),
                    "quering_id_len":len(self.quering_id),
                    }
                    )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(10)
            await check_cache()
    
    async def query_order(self):
        while True:
            await asyncio.sleep(1)
            for contract in self.contracts:
                som: OrderCache= self.om[contract]
                for order in som.cache.values():
                    self.loop.create_task(self.handle_query_order(order=order, som=som))
            
    async def update_gap_map(self):
        while True:
            await asyncio.sleep(3)    
                        
            c = dict()
            for contract in self.ret:
                c[contract] = np.quantile(self.ret[contract], 0.99)
                self.gap_map[contract] =  max(self.gap, c[contract] * 3)
            await self.redis_set_cache(c)
                    
            
    async def strategy_core(self):
        await asyncio.gather(
            self.check_unknown_order(),
            self.main_logic(),
            self.query_order(),
            self.update_redis_cache(),
            self.update_gap_map(),
            self.update_max_order_volume(),
        )
        
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    
                    
                