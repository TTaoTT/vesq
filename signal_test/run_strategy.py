import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue
import pickle
import hmac
import hashlib

from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

SYMBOLS = [
    "binance.eth_usdt_swap.usdt_contract.na",
    "binance.btc_usdt_swap.usdt_contract.na"
    ]

PRICE_FACTOR = None
BIG_TRADE = Decimal(0)

url = "http://35.73.223.244:5555/public/tool/hf_model"
        
def sign():
    key = 'a0744ec83963312fsdf642e38471367c3fasf2f2sdfa'
    timestamp = int(time.time())
    signature = hmac.new(key.encode(), str(timestamp).encode(), hashlib.sha256).hexdigest()
    return dict(timestamp=timestamp.__str__(), signature=signature)

class TickData:
    def __init__(self, ask_price=None, bid_price=None, ask_qty=None, bid_qty=None, spread=None) -> None:
        self.taker_buy_trade_price = []
        self.taker_buy_trade_qty = []
        self.taker_buy_qty_sum = Decimal(0)
        self.taker_buy_count = Decimal(0)
        self.taker_buy_max = Decimal(0)
        self.taker_buy_min = Decimal(2**31)
        self.taker_buy_curr_punc = Decimal(0)
        
        self.taker_sell_trade_price = []
        self.taker_sell_trade_qty = []
        self.taker_sell_qty_sum = Decimal(0)
        self.taker_sell_count = Decimal(0)
        self.taker_sell_max = Decimal(0)
        self.taker_sell_min = Decimal(2**31)
        self.taker_sell_curr_punc = Decimal(0)
        
        self.ask_price = ask_price
        self.bid_price = bid_price
        self.ask_qty = ask_qty
        self.bid_qty = bid_qty
        self.spread = spread
        
        self.last_ask_qty = None
        self.last_bid_qty = None
        
        # could be collected before the tick is completely filled.
        # initialize when creating the tick in on_public_trade
        self.mean1 = None
        self.mean5 = None
        self.corr1 = None
        self.corr2 = None
        self.mean_qty = None
        
        self.a_qty_change = None
        self.b_qty_change = None
        self.spast_ask_withdraw = None
        self.spast_bid_withdraw = None
        self.spast_ask_putforawrd = None
        self.spast_bid_putforawrd = None
        self.past_ask_withdraw = None
        self.past_bid_withdraw = None
        self.past_ask_putforawrd = None
        self.past_bid_putforawrd = None
        
        # should be filled right before calculating signal
        self.price_center = None
        self.qty_to_ask_qty = None
        self.qty_to_bid_qty = None
        
        self.punc = None
        
        self.f_ask_change = None
        self.f_bid_change = None
        self.trade_direction = None
        self.f_trade_qty = None
        self.f_trade_punc = None
        self.ask_withdraw = None
        self.bid_withdraw = None
        self.ask_putforawrd = None
        self.bid_putforawrd = None
        self.converge_time = None
        self.number = None
        self.delta_t = None
        
    def init_past_feature(
        self,
        return_sum_1s,
        return_sum_5s,
        return_200ms_cache,
        qty_200ms_cache,
        bbo_past_1s_cache,
        bbo_past_10s_cache,
        bbo_past_30s_cache,
        last_trade_ts,
        ts,
        last_ask_qty,
        last_bid_qty
        ):
        self.mean1 = return_sum_1s / Decimal(1000)
        self.mean5 = return_sum_5s / Decimal(5000)
        self.corr1 = np.corrcoef(return_200ms_cache[-10:],qty_200ms_cache[-10:])[0][1]
        self.corr2 = np.corrcoef(return_200ms_cache,qty_200ms_cache)[0][1]
        
        self.mean_qty = np.mean(qty_200ms_cache[-40:])
        
        self.a_qty_change = bbo_past_1s_cache["ask_qty"][-1] - bbo_past_1s_cache["ask_qty"][0] if bbo_past_1s_cache["ask_qty"] else Decimal(0)
        self.b_qty_change = bbo_past_1s_cache["bid_qty"][-1] - bbo_past_1s_cache["bid_qty"][0] if bbo_past_1s_cache["bid_qty"] else Decimal(0)
        
        self.spast_ask_withdraw = bbo_past_10s_cache["ask_wd_sum"]
        self.spast_bid_withdraw = bbo_past_10s_cache["bid_wd_sum"]
        self.spast_ask_putforawrd = bbo_past_10s_cache["ask_p_sum"]
        self.spast_bid_putforawrd = bbo_past_10s_cache["bid_p_sum"]
        
        self.past_ask_withdraw = bbo_past_30s_cache["ask_wd_sum"]
        self.past_bid_withdraw = bbo_past_30s_cache["bid_wd_sum"]
        self.past_ask_putforawrd = bbo_past_30s_cache["ask_p_sum"]
        self.past_bid_putforawrd = bbo_past_30s_cache["bid_p_sum"]
        
        self.delta_t = ts - last_trade_ts
        self.last_ask_qty = last_ask_qty
        self.last_bid_qty = last_bid_qty
        
        
    def add_trade(self, trade: PublicTrade):
        if trade.side == OrderSide.Buy:
            self.taker_buy_trade_price.append(trade.price)
            self.taker_buy_trade_qty.append(trade.quantity)
            self.taker_buy_qty_sum += trade.quantity
            self.taker_buy_max = max(self.taker_buy_max, trade.price)
            self.taker_buy_min = min(self.taker_buy_min, trade.price)
            # TODO check if this need to be converted to how many jumps
            self.taker_buy_curr_punc = self.taker_buy_max - self.taker_buy_min
            self.taker_buy_count += Decimal(1)
        else:
            self.taker_sell_trade_price.append(trade.price)
            self.taker_sell_trade_qty.append(trade.quantity)
            self.taker_sell_qty_sum += trade.quantity
            self.taker_sell_max = max(self.taker_sell_max, trade.price)
            self.taker_sell_min = min(self.taker_sell_min, trade.price)
            self.taker_sell_curr_punc = self.taker_sell_max - self.taker_sell_min
            self.taker_sell_count += Decimal(1)
    
    def get_qty_to_ask_qty(self):
        if not self.last_ask_qty:
            return 0
        if self.punc > 0:
            return self.taker_buy_qty_sum / self.last_ask_qty
        elif self.punc < 0:
            return self.taker_sell_qty_sum / self.last_ask_qty
        else:
            return (self.taker_buy_qty_sum + self.taker_sell_qty_sum) / self.last_ask_qty
    
    def get_qty_to_bid_qty(self):
        if not self.last_bid_qty:
            return 0
        if self.punc > 0:
            return self.taker_buy_qty_sum / self.last_bid_qty
        elif self.punc < 0:
            return self.taker_sell_qty_sum / self.last_bid_qty
        else:
            return (self.taker_buy_qty_sum + self.taker_sell_qty_sum) / self.last_bid_qty
            
    def get_price_center(self):
        if self.punc > 0:
            temp = np.array(self.taker_buy_trade_price)
            return np.sum(np.power((temp/np.sum(temp)),2))
        elif self.punc < 0:
            temp = np.array(self.taker_sell_trade_price)
            return np.sum(np.power((temp/np.sum(temp)),2))
        else:
            return 1
        
    def get_number(self):
        if self.punc > 0:
            self.number = self.taker_buy_count
        elif self.punc < 0:
            self.number = self.taker_sell_count
        else:
            self.number = Decimal(1)
        
    def get_all_features(self):
        return [
            self.punc,
            self.qty_to_ask_qty,
            self.qty_to_bid_qty,
            self.a_qty_change,
            self.b_qty_change,
            self.f_ask_change,
            self.f_bid_change,
            self.trade_direction,
            self.f_trade_qty,
            self.f_trade_punc,
            self.ask_withdraw,
            self.bid_withdraw,
            self.ask_putforawrd,
            self.bid_putforawrd,
            self.spast_ask_withdraw,
            self.spast_bid_withdraw,
            self.spast_ask_putforawrd,
            self.spast_bid_putforawrd,
            self.past_ask_withdraw,
            self.past_bid_withdraw,
            self.past_ask_putforawrd,
            self.past_bid_putforawrd,
            self.converge_time,
            self.number,
            self.ask_price,
            self.price_center,
            self.delta_t,
            self.mean1,
            self.mean5,
            self.corr1,
            self.corr2,
            self.mean_qty
        ]
        
    def get_all_features_dict(self):
        return {
            "punc":self.punc,
            "qty_to_ask_qty":self.qty_to_ask_qty,
            "qty_to_bid_qty":self.qty_to_bid_qty,
            "a_qty_change":self.a_qty_change,
            "b_qty_change":self.b_qty_change,
            "f_ask_change":self.f_ask_change,
            "f_bid_change":self.f_bid_change,
            "trade_direction":self.trade_direction,
            "f_trade_qty":self.f_trade_qty,
            "f_trade_punc":self.f_trade_punc,
            "ask_withdraw":self.ask_withdraw,
            "bid_withdraw":self.bid_withdraw,
            "ask_putforawrd":self.ask_putforawrd,
            "bid_putforawrd":self.bid_putforawrd,
            "spast_ask_withdraw":self.spast_ask_withdraw,
            "spast_bid_withdraw":self.spast_bid_withdraw,
            "spast_ask_putforawrd":self.spast_ask_putforawrd,
            "spast_bid_putforawrd":self.spast_bid_putforawrd,
            "past_ask_withdraw":self.past_ask_withdraw,
            "past_bid_withdraw":self.past_bid_withdraw,
            "past_ask_putforawrd":self.past_ask_putforawrd,
            "past_bid_putforawrd":self.past_bid_putforawrd,
            "converge_time":self.converge_time,
            "number":self.number,
            "ask_price":self.ask_price,
            "price_center":self.price_center,
            "delta_t":self.delta_t,
            "mean1":self.mean1,
            "mean5":self.mean5,
            "corr1":self.corr1,
            "corr2":self.corr2,
            "mean_qty":self.mean_qty
        }

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.cache = dict()
        self.bbo_update = Queue()
        self.trade_update = Queue()
        self.last_send_trade_signal_ts = 0
        self.bbo_past_1s_cache = dict()
        self.bbo_past_10s_cache = dict()
        self.bbo_past_30s_cache = dict()
        self.bbo_future_cache = dict()
        self.server_ms = Decimal(0)
        
        self.trade_handling_time = collections.deque(maxlen=10000)
        self.bbo_handling_time = collections.deque(maxlen=10000)
        self.cache_handling_time = collections.deque(maxlen=10000)
        self.cache_1ms_handling_time = collections.deque(maxlen=10000)
        self.init_features_handling_time = collections.deque(maxlen=10000)
        
        self.price_cache = dict()
        self.return_cache_1s = dict()
        self.return_cache_5s = dict()
        self.return_sum_1s = dict()
        self.return_sum_5s = dict()
        
        self.return_200ms_cache = dict()
        self.qty_200ms_cache = dict()
        
        self.bp = dict()
        self.ap = dict()
        self.bq = dict()
        self.aq = dict()
        
        self.temp_vol =dict()
        
        self.price_period_in_second = 5
        
        self.last_trade_ts = dict()
        
    async def before_strategy_start(self):
        for symbol in SYMBOLS:
            self.direct_subscribe_public_trade(symbol_name=symbol)
            await self.direct_subscribe_bbo(symbol_name=symbol)
            
            symbol = symbol.split(".")[:2]
            symbol = ".".join(symbol)
        
            self.cache[symbol] = dict()
            self.cache[symbol]["key"] = []
            self.cache[symbol]["value"] = []
            
            self.price_cache[symbol] = None
            self.return_cache_1s[symbol] = []
            self.return_cache_5s[symbol] = []
            self.return_sum_1s[symbol] = Decimal(0)
            self.return_sum_5s[symbol] = Decimal(0)
            self.return_200ms_cache[symbol] = []
            self.qty_200ms_cache[symbol] = []
            self.last_trade_ts[symbol] = int(0)
            
            self.bbo_past_1s_cache[symbol] = {
                "ts":[],
                "ask_qty":[],
                "bid_qty":[]
            }
            self.bbo_past_10s_cache[symbol] = {
                "ts":[],
                "ask_wd":[],
                "ask_p":[],
                "bid_wd":[],
                "bid_p":[],
                "ask_wd_sum":Decimal(0),
                "ask_p_sum":Decimal(0),
                "bid_wd_sum":Decimal(0),
                "bid_p_sum":Decimal(0),
                "last_ask":None,
                "last_ask_qty":None,
                "last_bid":None,
                "last_bid_qty":None,
            }
            self.bbo_past_30s_cache[symbol] = {
                "ts":[],
                "ask_wd":[],
                "ask_p":[],
                "bid_wd":[],
                "bid_p":[],
                "ask_wd_sum":Decimal(0),
                "ask_p_sum":Decimal(0),
                "bid_wd_sum":Decimal(0),
                "bid_p_sum":Decimal(0),
                "last_ask":None,
                "last_ask_qty":None,
                "last_bid":None,
                "last_bid_qty":None,
            }
            
            self.bbo_future_cache[symbol] = {
                "ts":[],
                "ask_wd":[],
                "ask_p":[],
                "bid_wd":[],
                "bid_p":[],
                "ask_wd_sum":Decimal(0),
                "ask_p_sum":Decimal(0),
                "bid_wd_sum":Decimal(0),
                "bid_p_sum":Decimal(0),
                "ask_price":[],
                "bid_price":[]
            }
            
            self.temp_vol[symbol] = Decimal(0)
            
            self.bp[symbol] = None
            self.ap[symbol] = None
            self.bq[symbol] = None
            self.aq[symbol] = None
            
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()
        
        try:
            await self.get_data_async()
        except Exception as err:
            self.logger.warning(f"get params err:{err}")
            raise
        
    async def on_public_trade(self, symbol, trade: PublicTrade):
        start = time.time()*1e3
        self.temp_vol[symbol] += trade.quantity
        key = self.cache[symbol]["key"]
        value = self.cache[symbol]["value"]
        trade.price = trade.price * Decimal(PRICE_FACTOR)
        if key:
            last_ask_qty = value[-1].ask_qty
            last_bid_qty = value[-1].bid_qty
        else:
            last_ask_qty = None
            last_bid_qty = None
        if not key or trade.server_ms > key[-1]:
            key.append(trade.server_ms)
            value.append(TickData())
            value[-1].add_trade(trade)
            s = time.time()*1e3
            value[-1].init_past_feature(
                self.return_sum_1s[symbol],
                self.return_sum_5s[symbol],
                self.return_200ms_cache[symbol],
                self.qty_200ms_cache[symbol],
                self.bbo_past_1s_cache[symbol],
                self.bbo_past_10s_cache[symbol],
                self.bbo_past_30s_cache[symbol],
                self.last_trade_ts[symbol],
                trade.server_ms,
                last_ask_qty,
                last_bid_qty
                )
            self.init_features_handling_time.append(time.time()*1e3-s)
        elif trade.server_ms < key[0]:
            key = [trade.server_ms] + key
            value = [TickData()] + value
            value[0].add_trade(trade)
            s = time.time()*1e3
            value[0].init_past_feature(
                self.return_sum_1s[symbol],
                self.return_sum_5s[symbol],
                self.return_200ms_cache[symbol],
                self.qty_200ms_cache[symbol],
                self.bbo_past_1s_cache[symbol],
                self.bbo_past_10s_cache[symbol],
                self.bbo_past_30s_cache[symbol],
                self.last_trade_ts[symbol],
                trade.server_ms,
                last_ask_qty,
                last_bid_qty
                )
            self.logger.warning(f"init past features cost:{round(time.time()*1e3-s,2)}")
        elif trade.server_ms == key[-1]:
            value[-1].add_trade(trade)
        elif trade.server_ms < key[-1]:
            i = len(key)-1
            while i >0 and trade.server_ms < key[i]:
                i -= 1
                if trade.server_ms == key[i]:
                    value[i].add_trade(trade)
                    break
                elif trade.server_ms > key[i]:
                    key = key[0:i+1] + [trade.server_ms] + key[i+1:]
                    value = value[0:i+1] + [TickData()] + value[i+1:]
                    value[i+1].add_trade(trade)
                    value[i+1].init_past_feature(
                        self.return_sum_1s[symbol],
                        self.return_sum_5s[symbol],
                        self.return_200ms_cache[symbol],
                        self.qty_200ms_cache[symbol],
                        self.bbo_past_1s_cache[symbol],
                        self.bbo_past_10s_cache[symbol],
                        self.bbo_past_30s_cache[symbol],
                        self.last_trade_ts[symbol],
                        trade.server_ms,
                        last_ask_qty,
                        last_bid_qty
                        )
                    break
        self.server_ms = max(trade.server_ms, self.server_ms)

        self.trade_handling_time.append(time.time()*1e3-start)
        
    async def on_bbo(self, symbol, bbo: BBODepth):
        start = time.time()*1e3
        key = self.cache[symbol]["key"]
        value = self.cache[symbol]["value"]
        ask_price = Decimal(bbo.ask[0]) * Decimal(PRICE_FACTOR)
        ask_qty = Decimal(bbo.ask[1])
        bid_price = Decimal(bbo.bid[0]) * Decimal(PRICE_FACTOR)
        bid_qty = Decimal(bbo.bid[1])
        spread = ask_price - bid_price
        
        self.ap[symbol] = ask_price
        self.aq[symbol] = ask_qty
        self.bp[symbol] = bid_price
        self.bq[symbol] = bid_qty

        if not key or bbo.server_ms > key[-1]:
            key.append(bbo.server_ms)
            value.append(TickData(
                ask_price=ask_price,
                bid_price=bid_price,
                ask_qty=ask_qty, 
                bid_qty=bid_qty,
                spread=spread
                )
            )
        elif bbo.server_ms < key[0]:
            key = [bbo.server_ms] + key
            value = [TickData(ask_price=ask_price, bid_price=bid_price, ask_qty=ask_qty, bid_qty=bid_qty,spread=spread)] + value
        elif bbo.server_ms == key[-1]:
            value[-1].ask_price = ask_price
            value[-1].bid_price = bid_price
            value[-1].ask_qty = ask_qty
            value[-1].bid_qty = bid_qty
        elif bbo.server_ms < key[-1]:
            i = len(key)-1
            while i >0 and bbo.server_ms < key[i]:
                i -= 1
                if bbo.server_ms == key[i]:
                    value[i].ask_price = ask_price
                    value[i].bid_price = bid_price
                    value[i].ask_qty = ask_qty
                    value[i].bid_qty = bid_qty
                    break
                elif bbo.server_ms > key[i]:
                    key = key[0:i+1] + [bbo.server_ms] + key[i+1:]
                    value = value[0:i+1] + [TickData(ask_price=ask_price, bid_price=bid_price, ask_qty=ask_qty, bid_qty=bid_qty,spread=spread)] + value[i+1:]
                    break
        self.server_ms = max(bbo.server_ms, self.server_ms)
        self.bbo_past_1s_cache[symbol]["ts"].append(bbo.server_ms)
        self.bbo_past_10s_cache[symbol]["ts"].append(bbo.server_ms)
        self.bbo_past_30s_cache[symbol]["ts"].append(bbo.server_ms)
        self.bbo_future_cache[symbol]["ts"].append(bbo.server_ms)
        if not self.bbo_past_10s_cache[symbol]["last_ask"]:
            temp_wd = Decimal(0)
            temp_p = Decimal(0)
        elif ask_price > self.bbo_past_10s_cache[symbol]["last_ask"]:
            temp_wd = self.bbo_past_10s_cache[symbol]["last_ask_qty"]
            temp_p = Decimal(0)
        elif ask_price == self.bbo_past_10s_cache[symbol]["last_ask"]:
            temp_wd = self.bbo_past_10s_cache[symbol]["last_ask_qty"] - ask_qty
            temp_p = ask_qty - self.bbo_past_10s_cache[symbol]["last_ask_qty"]
        else:
            temp_wd = Decimal(0)
            temp_p = ask_qty
        temp_wd= max(Decimal(0),temp_wd)
        temp_p= max(Decimal(0),temp_p)
        self.bbo_past_10s_cache[symbol]["ask_wd"].append(temp_wd)
        self.bbo_past_10s_cache[symbol]["ask_wd_sum"] += temp_wd
        self.bbo_past_10s_cache[symbol]["ask_p"].append(temp_p)
        self.bbo_past_10s_cache[symbol]["ask_p_sum"] += temp_p
        
        self.bbo_past_30s_cache[symbol]["ask_wd"].append(temp_wd)
        self.bbo_past_30s_cache[symbol]["ask_wd_sum"] += temp_wd
        self.bbo_past_30s_cache[symbol]["ask_p"].append(temp_p)
        self.bbo_past_30s_cache[symbol]["ask_p_sum"] += temp_p
        
        self.bbo_future_cache[symbol]["ask_wd"].append(temp_wd)
        self.bbo_future_cache[symbol]["ask_wd_sum"] += temp_wd
        self.bbo_future_cache[symbol]["ask_p"].append(temp_p)
        self.bbo_future_cache[symbol]["ask_p_sum"] += temp_p
        
        if not self.bbo_past_10s_cache[symbol]["last_bid"]:
            temp_wd = Decimal(0)
            temp_p = Decimal(0)
        elif bid_price > self.bbo_past_10s_cache[symbol]["last_bid"]:
            temp_wd = Decimal(0)
            temp_p = bid_qty
        elif bid_price == self.bbo_past_10s_cache[symbol]["last_bid"]:
            temp_wd = self.bbo_past_10s_cache[symbol]["last_bid_qty"] - bid_qty
            temp_p = bid_qty - self.bbo_past_10s_cache[symbol]["last_bid_qty"]
        else:
            temp_wd = self.bbo_past_10s_cache[symbol]["last_bid_qty"]
            temp_p = Decimal(0)
        temp_wd = max(Decimal(0),temp_wd)
        temp_p = max(Decimal(0),temp_p)
        
        self.bbo_past_10s_cache[symbol]["bid_wd"].append(temp_wd)
        self.bbo_past_10s_cache[symbol]["bid_wd_sum"] += temp_wd
        self.bbo_past_10s_cache[symbol]["bid_p"].append(temp_p)
        self.bbo_past_10s_cache[symbol]["bid_p_sum"] += temp_p
        
        self.bbo_past_30s_cache[symbol]["bid_wd"].append(temp_wd)
        self.bbo_past_30s_cache[symbol]["bid_wd_sum"] += temp_wd
        self.bbo_past_30s_cache[symbol]["bid_p"].append(temp_p)
        self.bbo_past_30s_cache[symbol]["bid_p_sum"] += temp_p
        
        self.bbo_future_cache[symbol]["bid_wd"].append(temp_wd)
        self.bbo_future_cache[symbol]["bid_wd_sum"] += temp_wd
        self.bbo_future_cache[symbol]["bid_p"].append(temp_p)
        self.bbo_future_cache[symbol]["bid_p_sum"] += temp_p
        
        self.bbo_past_10s_cache[symbol]["last_ask"] = ask_price
        self.bbo_past_10s_cache[symbol]["last_ask_qty"] = ask_qty
        self.bbo_past_10s_cache[symbol]["last_bid"] = bid_price
        self.bbo_past_10s_cache[symbol]["last_bid_qty"] = bid_qty
        
        self.bbo_past_30s_cache[symbol]["last_ask"] = ask_price
        self.bbo_past_30s_cache[symbol]["last_ask_qty"] = ask_qty
        self.bbo_past_30s_cache[symbol]["last_bid"] = bid_price
        self.bbo_past_30s_cache[symbol]["last_bid_qty"] = bid_qty
        
        self.bbo_future_cache[symbol]["ask_price"].append(ask_price)
        self.bbo_future_cache[symbol]["bid_price"].append(bid_price)
        self.bbo_past_1s_cache[symbol]["ask_qty"].append(ask_qty)
        self.bbo_past_1s_cache[symbol]["bid_qty"].append(bid_qty)

        self.bbo_handling_time.append(time.time()*1e3-start)
            
    async def update_cache(self):
        while True:
            await asyncio.sleep(0.002)
            start = time.time()*1e3
            for symbol in SYMBOLS:
                symbol = symbol.split(".")[:2]
                symbol = ".".join(symbol)
                key = self.cache[symbol]["key"]
                value = self.cache[symbol]["value"]
                
                while key and key[0] < self.server_ms - Decimal(10*1e3):
                    key.pop(0)
                    value.pop(0)
                
                while self.bbo_past_1s_cache[symbol]["ts"] and self.bbo_past_1s_cache[symbol]["ts"][0] < self.server_ms - Decimal(1e3):
                    self.bbo_past_1s_cache[symbol]["ts"].pop(0)
                    self.bbo_past_1s_cache[symbol]["ask_qty"].pop(0)
                    self.bbo_past_1s_cache[symbol]["bid_qty"].pop(0)
                    
                    
                while self.bbo_past_10s_cache[symbol]["ts"] and self.bbo_past_10s_cache[symbol]["ts"][0] < self.server_ms - Decimal(10*1e3):
                    self.bbo_past_10s_cache[symbol]["ts"].pop(0)
                    ask_wd = self.bbo_past_10s_cache[symbol]["ask_wd"].pop(0)
                    ask_p = self.bbo_past_10s_cache[symbol]["ask_p"].pop(0)
                    bid_wd = self.bbo_past_10s_cache[symbol]["bid_wd"].pop(0)
                    bid_p = self.bbo_past_10s_cache[symbol]["bid_p"].pop(0)
                    self.bbo_past_10s_cache[symbol]["ask_wd_sum"] -= ask_wd
                    self.bbo_past_10s_cache[symbol]["ask_p_sum"] -= ask_p
                    self.bbo_past_10s_cache[symbol]["bid_wd_sum"] -= bid_wd
                    self.bbo_past_10s_cache[symbol]["bid_p_sum"] -= bid_p
                    
                while self.bbo_past_30s_cache[symbol]["ts"] and self.bbo_past_30s_cache[symbol]["ts"][0] < self.server_ms - Decimal(30*1e3):
                    self.bbo_past_30s_cache[symbol]["ts"].pop(0)
                    ask_wd = self.bbo_past_30s_cache[symbol]["ask_wd"].pop(0)
                    ask_p = self.bbo_past_30s_cache[symbol]["ask_p"].pop(0)
                    bid_wd = self.bbo_past_30s_cache[symbol]["bid_wd"].pop(0)
                    bid_p = self.bbo_past_30s_cache[symbol]["bid_p"].pop(0)
                    self.bbo_past_30s_cache[symbol]["ask_wd_sum"] -= ask_wd
                    self.bbo_past_30s_cache[symbol]["ask_p_sum"] -= ask_p
                    self.bbo_past_30s_cache[symbol]["bid_wd_sum"] -= bid_wd
                    self.bbo_past_30s_cache[symbol]["bid_p_sum"] -= bid_p
                    
                while self.bbo_future_cache[symbol]["ts"] and self.bbo_future_cache[symbol]["ts"][0] < self.server_ms - Decimal(100):
                    self.bbo_future_cache[symbol]["ts"].pop(0)
                    ask_wd = self.bbo_future_cache[symbol]["ask_wd"].pop(0)
                    ask_p = self.bbo_future_cache[symbol]["ask_p"].pop(0)
                    bid_wd = self.bbo_future_cache[symbol]["bid_wd"].pop(0)
                    bid_p = self.bbo_future_cache[symbol]["bid_p"].pop(0)
                    self.bbo_future_cache[symbol]["ask_price"].pop(0)
                    self.bbo_future_cache[symbol]["bid_price"].pop(0)
                    self.bbo_future_cache[symbol]["ask_wd_sum"] -= ask_wd
                    self.bbo_future_cache[symbol]["ask_p_sum"] -= ask_p
                    self.bbo_future_cache[symbol]["bid_wd_sum"] -= bid_wd
                    self.bbo_future_cache[symbol]["bid_p_sum"] -= bid_p
                    
            self.cache_handling_time.append(time.time()*1e3-start)

    async def calculate_and_pulish(self):
        
        while True:
            await asyncio.sleep(0.001)
            for symbol in SYMBOLS:
                symbol = symbol.split(".")[:2]
                symbol = ".".join(symbol)
                key = self.cache[symbol]["key"]
                value = self.cache[symbol]["value"]
                if value:
                    value[-1].punc = value[-1].taker_buy_curr_punc - value[-1].taker_sell_curr_punc
                try:
                    if value and (value[-1].taker_sell_qty_sum > BIG_TRADE or value[-1].taker_buy_qty_sum > BIG_TRADE or value[-1].punc > 0):
                        self.last_trade_ts[symbol] = key[-1]
                    if key and self.server_ms - 100 > key[0]:
                        i = 0
                        while key and self.server_ms - 100 > key[i]:
                            value[i].punc = value[i].taker_buy_curr_punc - value[i].taker_sell_curr_punc
                            if value[i].taker_sell_qty_sum < BIG_TRADE and value[i].taker_buy_qty_sum < BIG_TRADE and \
                                value[i].punc == 0:
                                key.pop(i)
                                value.pop(i)
                                continue
                            value[i].get_number()
                            value[i].price_center = value[i].get_price_center()
                            value[i].qty_to_ask_qty = value[i].get_qty_to_ask_qty()
                            value[i].qty_to_bid_qty = value[i].get_qty_to_bid_qty()
                            value[i].f_ask_change = self.bbo_future_cache[symbol]["ask_price"][-1] - self.bbo_future_cache[symbol]["ask_price"][0]
                            value[i].f_bid_change = self.bbo_future_cache[symbol]["bid_price"][-1] - self.bbo_future_cache[symbol]["bid_price"][0]
                            
                            j = i + 1 
                            trade_qty = Decimal(0)
                            trade_punc = Decimal(0)
                            trade_direction = Decimal(0)
                            converge_time = Decimal(125)
                            
                            while j <len(key):
                                trade_qty += value[j].taker_buy_qty_sum + value[j].taker_sell_qty_sum
                                punc = value[j].taker_buy_curr_punc - value[j].taker_sell_curr_punc 
                                trade_punc += punc
                                side = punc * value[i].punc
                                if side > 0:
                                    trade_direction += 1
                                elif side < 0:
                                    trade_direction -= 1
                                if value[j].ask_price - value[j].bid_price < Decimal(1)*Decimal(1.5):
                                    converge_time = key[j] - key[i]
                                j += 1
                            
                            value[i].trade_direction = trade_direction
                            value[i].f_trade_qty = trade_qty
                            value[i].f_trade_punc = trade_punc
                            value[i].converge_time = converge_time
                            
                            value[i].ask_withdraw = self.bbo_future_cache[symbol]["ask_wd_sum"]
                            value[i].bid_withdraw = self.bbo_future_cache[symbol]["bid_wd_sum"]
                            value[i].ask_putforawrd = self.bbo_future_cache[symbol]["ask_p_sum"]
                            value[i].bid_putforawrd = self.bbo_future_cache[symbol]["bid_p_sum"]
                            
                            features = value[i].get_all_features()
                            # features_dict = value[i].get_all_features_dict()
                            
                            if symbol == "binance.eth_usdt_swap" and None not in features:
                                self.logger.warning("cal features sucess")
                                try:
                                    features = np.array(features, dtype=float)
                                    bid_predict = self.bid_model.predict([features])[0]
                                    self.logger.warning(f"bid predict:{bid_predict}")
                                    # self.logger.warning(f"bid predict features:{features_dict}")
                                    self.loop.create_tast(self.publish_to_channel("mq:hf_signal:test_signal",{"side":"bid","predict":bid_predict,"ts":ts}))
                                except:
                                    pass
                                try:
                                    features = np.array(features, dtype=float)
                                    ask_predict = self.ask_model.predict([features])[0]
                                    self.logger.warning(f"ask predict:{ask_predict}")
                                    self.loop.create_task(self.publish_to_channel("mq:hf_signal:test_signal",{"side":"ask","predict":ask_predict,"ts":ts}))
                                except:
                                    pass
                            features = None
                            ts = key[i]
                            
                            key.pop(i)
                            value.pop(i)

                except Exception as err:
                    self.logger.warning(f"cal featreus failed:{err}")
                    key.pop(i)
                    value.pop(i)
                
                
                        
    async def update_price_cache(self):
        while True:
            await asyncio.sleep(0.001)
            s = time.time()*1e3
            for symbol in SYMBOLS:
                symbol = symbol.split(".")[:2]
                symbol = ".".join(symbol)
                p = (self.bp[symbol]*self.aq[symbol] + self.ap[symbol]*self.bq[symbol]) / (self.bq[symbol] + self.aq[symbol])
                if self.price_cache[symbol]:
                    ret = (p-self.price_cache[symbol])/self.price_cache[symbol]
                    # self.logger.warning(f"{symbol}ret:{ret}")
                    self.return_cache_1s[symbol].append(ret)
                    self.return_cache_5s[symbol].append(ret)
                    self.return_sum_1s[symbol] += ret
                    self.return_sum_5s[symbol] += ret
                self.price_cache[symbol] = p
                
                while len(self.return_cache_5s[symbol]) > 1000 * self.price_period_in_second:
                    ret = self.return_cache_5s[symbol].pop(0)
                    self.return_sum_5s[symbol] -= ret
                    
                while len(self.return_cache_1s[symbol]) > 1000 * 1:
                    ret = self.return_cache_1s[symbol].pop(0)
                    self.return_sum_1s[symbol] -= ret
                    
            self.cache_1ms_handling_time.append(time.time()*1e3-s)
                    
    async def update_200ms_cache(self):
        last_price=dict()
        for symbol in SYMBOLS:
            symbol = symbol.split(".")[:2]
            symbol = ".".join(symbol)
            last_price[symbol] = None
        while True:
            await asyncio.sleep(0.2)
            for symbol in SYMBOLS:
                symbol = symbol.split(".")[:2]
                symbol = ".".join(symbol)
                
                p = (self.bp[symbol]*self.aq[symbol] + self.ap[symbol]*self.bq[symbol]) / (self.bq[symbol] + self.aq[symbol])
                if last_price[symbol]:
                    ret = (p - last_price[symbol]) / last_price[symbol]
                    self.return_200ms_cache[symbol].append(float(ret))
                    self.qty_200ms_cache[symbol].append(float(self.temp_vol[symbol]))
                last_price[symbol] = p
            
                self.temp_vol[symbol] = Decimal(0)
                
                while len(self.return_200ms_cache[symbol]) > 150:
                    self.return_200ms_cache[symbol].pop(0)
                
                while len(self.qty_200ms_cache[symbol]) > 150:
                    self.qty_200ms_cache[symbol].pop(0)
            
    async def set_cache(self):
        while True:
            await asyncio.sleep(1)
            await self.redis_set_cache(
                {
                "trade handeling time":np.mean(self.trade_handling_time) if self.trade_handling_time else 0,
                "bbo handeling time":np.mean(self.bbo_handling_time) if self.bbo_handling_time else 0,
                "cache handeling time":np.mean(self.cache_handling_time) if self.cache_handling_time else 0,
                "cache 1ms handeling time":np.mean(self.cache_1ms_handling_time) if self.cache_1ms_handling_time else 0,
                "feature init time":np.mean(self.init_features_handling_time) if self.init_features_handling_time else 0
                }
            )
            
    async def get_params(self):
        
        while True:
            await asyncio.sleep(60*60)
            try:
                await self.get_data_async()
            except Exception as err:
                self.logger.warning(f"get params err:{err}")
                
    async def get_data_async(self):
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=dict(filename="binance.eth_usdt_swap.pkl"), headers=sign()) as conn:
                data = await conn.content.readany()
                model = pickle.loads(data)
                self.ask_model = model["ask_model"]
                self.bid_model = model["bid_model"]
                global BIG_TRADE, PRICE_FACTOR
                BIG_TRADE = model["bid_trade"]
                PRICE_FACTOR = model["pf"]
                self.logger.warning(f"{PRICE_FACTOR}")
        
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache(),
            self.update_cache(),
            self.update_price_cache(),
            self.calculate_and_pulish(),
            self.update_200ms_cache()
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()