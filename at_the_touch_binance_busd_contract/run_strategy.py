from unittest.mock import AsyncMagicMixin
from xml.etree.ElementTree import TreeBuilder
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2
import logging
import asyncio
from decimal import Decimal
from inspect import trace
import time
import pandas as pd
from asyncio.queues import Queue
import os
import numpy as np
import traceback
import math
from typing import Set, AnyStr, Union, Dict, List
import ujson as json
from orderedset import OrderedSet
import uuid
from asyncio.queues import Queue, LifoQueue
from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from strategy_base.base import CommonStrategy

from xex_mm.strategy_configs.xex_mm_config import PredRefPrcMode, QuoteLevelConfig

from xex_mm.strategy_configs.at_touch_config import Config, QuoteLevelConfig
from xex_mm.xex_executor.params import ModelParams
from xex_mm.utils.base import Depth, BBO, Direction, Order, TransOrder, MakerTaker, Trade, XexInventoryManager, ReferencePriceCalculator
from xex_mm.utils.base import MsgOrderManager as _MsgOrderManager
from xex_mm.utils.base import OrderManager as _OrderManager
from xex_mm.utils.base import TransOrderManager


from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig, HeartBeatTolerance
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_inventory_executor import XexMmDeltaNeutralInventoryExecutor
from xex_mm.strategy_configs.obligatory_mm_config import ObligatoryMmConfig

from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard

from xex_mm.utils.parallel import FunctorDistributionEngine
import math

from xex_mm.xex_depth.xex_depth import XExDepth, XExBBO
from xex_mm.xex_executor.order_executor2 import OrderExecutorTest2
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor

from xex_mm.signals.LocalCalculator import LocalCalculatorBase
from xex_mm.signals.SignalCalculator import SignalCalculatorBase

from xex_mm.utils.contract_mapping import ContractMapper
from xex_mm.at_touch_strategy.order_manager import AtTouchHedgeOrderManager as _AtTouchHedgeOrderManager
from xex_mm.at_touch_strategy.executor import AtTouchExecutor
from xex_mm.calculators.trade_stability import TradeStabilityState

from collections import defaultdict

from xex_mm.utils.configs import QuantityPrecisionManager

ACC_LOAD_KEY = "X-MBX-ORDER-COUNT-10S"
IP_LOAD_KEY = "X-MBX-USED-WEIGHT-1M"

class MsgOrderManager(_MsgOrderManager):
    def get_order_inc_fill_qty_in_tokens(self, order: Order) -> float:
        assert order.filled_qty >= 0
        oid = order.client_id
        if oid in self._order_cache:
            prev_order: Order = self._order_cache[oid]
            inc_fill_qty_in_tokens = max(order.filled_qty - prev_order.filled_qty, 0)
            return inc_fill_qty_in_tokens
        return order.filled_qty

    def update_order(self, order: Order):
        oid = order.client_id
        self._order_cache[oid] = order
        int_prc = self._int_prc(prc=order.prc)
        if order.side == Direction.long:  # bid
            self.bid_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        elif order.side == Direction.short:  # ask
            self.ask_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        else:
            raise NotImplementedError()
        self._oid_to_iid_map[oid] = order.iid
        if order.iid not in self._iid_to_oids_map:
            self._iid_to_oids_map[order.iid] = OrderedSet()
        self._iid_to_oids_map[order.iid].add(oid)


class OrderManager(_OrderManager):
    def __init__(self):
        self.trans_order_manager = TransOrderManager()
        self.msg_order_manager = MsgOrderManager()
        

class AtTouchHedgeOrderManager(OrderManager):
    def __init__(self, logger):
        super(AtTouchHedgeOrderManager, self).__init__()
        self.hedge_order_oids: OrderedSet[str] = OrderedSet()
        self.pending_cancel_hedge_oids: Set[str] = set()
        self.hedge_orders_side: int = ...
        self.hedge_orders_int_prc: int = ...
        self.hedge_order_total_unfilled_qty: int = 0
        self.quantity_precision_manager = QuantityPrecisionManager()
        self.quantity_precision_map = dict()
        self.logger = logger
        
    def is_hedge_order(self, oid: str):
        return oid in self.hedge_order_oids

    def get_quantity_precision(self, iid: str):
        if iid in self.quantity_precision_map:
            return self.quantity_precision_map[iid]
        quantity_precision = self.quantity_precision_manager.get_quantity_precision_by_iid(iid=iid)
        self.quantity_precision_map[iid] = quantity_precision
        return quantity_precision
        
    def update_order(self, order: Order):
        if not self.msg_order_manager.contains_order(oid=order.client_id):
            if order.client_id in self.hedge_order_oids:
                trans_order = self.trans_order_manager.order_at(oid=order.client_id)
                hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
                self.hedge_order_total_unfilled_qty -= trans_order.quantity
                self.hedge_order_total_unfilled_qty += (order.qty - order.filled_qty)
                # self.logger.info(f"Update order-no msg order: cid={order.client_id}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},trans_order.quantity={trans_order.quantity},order.qty={order.qty},order.filled_qty={order.filled_qty},hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
                iid = f"{order.ex}.{order.contract}"
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
            elif order.client_id in self.pending_cancel_hedge_oids:
                pass
            else:
                pass
                # self.logger.info(f"recieve order msg for finished order:{order.__dict__}")
        else:
            if order.client_id in self.hedge_order_oids:
                prev_order: Order = self.msg_order_manager.order_at(order.client_id)
                hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
                self.hedge_order_total_unfilled_qty -= (prev_order.qty - prev_order.filled_qty)
                self.hedge_order_total_unfilled_qty += (order.qty - order.filled_qty)
                # self.logger.info(f"Update order-with msg order：cid={order.client_id}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},prev_order.qty={prev_order.qty},prev_order.filled_qty={prev_order.filled_qty},order.qty={order.qty},order.filled_qty={order.filled_qty},hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
                iid = f"{order.ex}.{order.contract}"
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
            elif order.client_id in self.pending_cancel_hedge_oids:
                pass
            else:
                pass
                # self.logger.info(f"recieve order msg for finished order:{order.__dict__}")
        super(AtTouchHedgeOrderManager, self).update_order(order=order)
        
    def add_hedge_trans_order(self, hedge_order: TransOrder, oid: str) -> bool:
        if self.hedge_orders_side is not Ellipsis:
            assert hedge_order.side == self.hedge_orders_side
        else:
            self.hedge_orders_side = hedge_order.side
        int_hedge_prc = self.float_prc_to_int_prc(float_prc=hedge_order.price)
        if self.hedge_orders_int_prc is not Ellipsis:
            assert int_hedge_prc == self.hedge_orders_int_prc
        else:
            self.hedge_orders_int_prc = int_hedge_prc
        self.hedge_order_oids.add(oid)
        hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
        self.hedge_order_total_unfilled_qty += hedge_order.quantity
        # self.logger.info(f"Add hedge trans order: oid={oid}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},hedge_order.quantity={hedge_order.quantity},hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
        self.add_trans_order(order=hedge_order, oid=oid)
        return True

    def hedge_order_pending_cancel(self, oid: str) -> bool:
        hedge_order = self.get_trans_order(oid=oid)
        # hedge_order.cancel = True
        # self.logger.info(f"enter pending cancel, cid={oid}")
        self.hedge_order_oids.remove(oid)
        self.pending_cancel_hedge_oids.add(oid)
        if self.msg_order_manager.contains_order(oid=oid):
            order = self.msg_order_manager.order_at(oid=oid)
            hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
            self.hedge_order_total_unfilled_qty -= (order.qty - order.filled_qty)
            # self.logger.info(f"Hedge order pending cancel - with msg order: oid={oid}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},order.qty={order.qty}，order.filled_qty={order.filled_qty},hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
            iid = f"{order.ex}.{order.contract}"
            assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
        else:
            hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
            self.hedge_order_total_unfilled_qty -= hedge_order.quantity
            # self.logger.info(f"Hedge order pending cancel - no msg order: oid={oid}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},hedge_order.quantity={hedge_order.quantity}，hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
            assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=hedge_order.iid)
        if len(self.hedge_order_oids) == 0:
            self.hedge_orders_side = ...
            self.hedge_orders_int_prc = ...
            self.hedge_order_total_unfilled_qty = 0
        return True

    def pop_hedge_order(self, oid: str) -> bool:
        if oid in self.pending_cancel_hedge_oids:
            self.pending_cancel_hedge_oids.remove(oid)
        else:
            self.hedge_order_oids.remove(oid)
            hedge_order = self.get_trans_order(oid=oid)
            assert not hedge_order.cancel, oid
            if self.msg_order_manager.contains_order(oid=oid):
                order = self.msg_order_manager.order_at(oid=oid)
                iid = f"{order.ex}.{order.contract}"
                assert abs(order.qty - hedge_order.quantity) < 10 ** -self.get_quantity_precision(iid=iid)
                hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
                self.hedge_order_total_unfilled_qty -= (order.qty - order.filled_qty)
                # self.logger.info(f"Pop hegde order - with msg order: oid={oid}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},order.qty={order.qty}，order.filled_qty={order.filled_qty}，hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
                iid = f"{order.ex}.{order.contract}"
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
            else:
                hedge_order_total_unfilled_qty_before = self.hedge_order_total_unfilled_qty
                self.hedge_order_total_unfilled_qty -= hedge_order.quantity
                # self.logger.info(f"Pop hegde order - no msg order: oid={oid}, hedge_order_total_unfilled_qty_before={hedge_order_total_unfilled_qty_before},hedge_order.quantity={hedge_order.quantity}，hedge_order_total_unfilled_qty_after={self.hedge_order_total_unfilled_qty}")
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=hedge_order.iid)
            if len(self.hedge_order_oids) == 0:
                self.hedge_orders_side = ...
                self.hedge_orders_int_prc = ...
                self.hedge_order_total_unfilled_qty = 0
        self.pop_trans_order(oid=oid)
        self.pop_order(oid=oid)
        return True
    
    def get_hedge_trans_orders(self) -> List[TransOrder]:
        return [self.get_trans_order(oid=oid) for oid in self.hedge_order_oids]


def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class LoadBalancer:
    def __init__(self, ccy1: str, ccy2: str) -> None:
        self._position_map = dict()
        self._balance_map = dict()
        self._account_load_map = dict()
        self._ip_load_map = dict()
        self._available_map = dict()
        self._locked_map = dict()
        
        self.ccy1 = ccy1
        self.ccy2 = ccy2
        
        self.send_counter = 0
        self.cancel_counter = 0
        self.query_counter = 0
        
        self.max_leverage = 10
        self.ban_name = set()
        
    def add_send(self):
        self.send_counter += 1
        
    def sub_send(self):
        self.send_counter -= 1
        
    def add_cancel(self):
        self.cancel_counter += 1
        
    def add_query(self):
        self.query_counter += 1
        
    def ban_account(self, account_name):
        if account_name:
            self.ban_name.add(account_name)
            
    def ban_trader(self, trader):
        if trader in self._ip_load_map:
            self._ip_load_map.pop(trader)
    
    def free_account(self, account_name):
        if account_name in self.ban_name:
            self.ban_name.remove(account_name)
    
    def setup(self, acc_list: list, trader_list: list):
        acc_list = acc_list.copy()
        trader_list = trader_list.copy()
        for acc in acc_list:
            self._position_map[acc] = 0
            self._balance_map[acc] = 0
            self._account_load_map[acc] = 0
            self._available_map[acc] = dict()
            self._available_map[acc][self.ccy1] = 0
            self._available_map[acc][self.ccy2] = 0
            self._locked_map[acc] = 0
        
        for trader in trader_list:
            self._ip_load_map[trader] = 0
        
    
    def get_acc_list(self):
        return sorted(self._account_load_map, key=self._account_load_map.get)
        
    def get_trader_list(self):
        return sorted(self._ip_load_map, key=self._ip_load_map.get)
    
    def get_trader(self):
        return self.get_trader_list()[0]
    
    def update_available_balance(self, acc: str, available_balance: float, ccy: str):
        self._available_map[acc][ccy] = available_balance
        
    def split_order(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            new_order = order.clone()
            if order.side == Direction.long:
                temp_qty = self._available_map[acc][ccy2] * 0.99
                if total_qty*order.price > temp_qty:
                    new_order.quantity = temp_qty / order.price
                    new_order.account_name = acc
                    total_qty -= temp_qty / order.price
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break
            elif order.side == Direction.short:
                temp_qty = self._available_map[acc][ccy1] * 0.99
                if total_qty > temp_qty:
                    new_order.quantity = temp_qty
                    new_order.account_name = acc
                    total_qty -= temp_qty
                    splited_orders.append(new_order)
                else:
                    new_order.quantity = total_qty
                    new_order.account_name = acc
                    splited_orders.append(new_order)
                    break

        return splited_orders
    
    def split_orders_perp(self, order: TransOrder):
        splited_orders = []
        total_qty = order.quantity
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        for acc in self.get_acc_list():
            new_order = order.clone()
            allowed_qty = self.volume_allowed(acc=acc, side=order.side) / order.price
            if allowed_qty < 0:
                continue
            if total_qty > allowed_qty:
                new_order.quantity = allowed_qty
                new_order.account_name = acc
                splited_orders.append(new_order)
                total_qty -= allowed_qty
            else:
                new_order.quantity = total_qty
                new_order.account_name = acc
                splited_orders.append(new_order)
                break
        return splited_orders
    
    def send_order_perp(self, order: TransOrder):
        if order.side == Direction.long:
            self._locked_map[order.account_name] += order.price * order.quantity
        elif order.side == Direction.short:
            self._locked_map[order.account_name] -= order.price * order.quantity
    
    def finished_order_perp(self, torder: TransOrder, order_obj: Order):
        if torder.side == Direction.long:
            self._locked_map[torder.account_name] -= torder.price * torder.quantity
            self._locked_map[torder.account_name] += order_obj.avg_fill_prc * order_obj.filled_qty
        elif torder.side == Direction.short:
            self._locked_map[torder.account_name] += torder.price * torder.quantity
            self._locked_map[torder.account_name] -= order_obj.avg_fill_prc * order_obj.filled_qty
        else:
            raise NotImplementedError()
            
    def volume_allowed(self, acc: str, side: Direction):
        if side == Direction.long:
            return self._balance_map[acc] * self.max_leverage - self._locked_map[acc]
        else:
            return self._balance_map[acc] * self.max_leverage + self._locked_map[acc]
        
    def send_order(self, order: TransOrder):
        if not order.account_name:
            raise RuntimeError("order with no account name")
        _, contract = order.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if order.side == Direction.long:
            self._available_map[order.account_name][ccy2] -= order.price * order.quantity
        elif order.side == Direction.short:
            self._available_map[order.account_name][ccy1] -= order.quantity
        else:
            raise NotImplementedError()
            
        if self._available_map[order.account_name][ccy1] < 0 or self._available_map[order.account_name][ccy2] < 0:
            raise RuntimeError("order routing wrong") 
               
    def finished_order(self, torder: TransOrder, order_obj: Order):
        _, contract = torder.iid.split(".")
        ccy = contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if torder.side == Direction.long:
            self._available_map[torder.account_name][ccy2] += torder.price * torder.quantity
            self._available_map[torder.account_name][ccy2] -= order_obj.avg_fill_prc * order_obj.filled_qty
            self._available_map[torder.account_name][ccy1] += order_obj.filled_qty
        elif torder.side == Direction.short:
            self._available_map[torder.account_name][ccy1] += torder.quantity
            self._available_map[torder.account_name][ccy1] -= order_obj.filled_qty
            self._available_map[torder.account_name][ccy2] += order_obj.avg_fill_prc * order_obj.filled_qty
        else:
            raise NotImplementedError()
        
    def update_balance(self, acc:str, balance:float):
        self._balance_map[acc] = balance
    
    def update_position(self, acc: str, chg_qty: float):
        self._position_map[acc] += chg_qty
    
    def update_limit(self, acc: str, trader_name: str, acc_load: Union[str, None], ip_load: Union[str, None]):
        if acc_load != None:
            self._account_load_map[acc] = int(acc_load)
        if ip_load != None:
            self._ip_load_map[trader_name] = int(ip_load)

    def reduce_count(self, reduce: float):
        self._account_load_map[self.get_acc_list()[-1]] -= reduce
        self._ip_load_map[self.get_trader_list()[-1]] -= reduce
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.start_ts = time.time()
        self.oid_to_cid = dict()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.bnd_amt_in_coin = Decimal(self.config['strategy']['params']['bnd_amt_in_coin'])
        self.unknown_order_dict = dict()
        
        self._mm_order_manager_map = defaultdict(lambda: OrderManager())
        self._rebalance_manager_map = defaultdict(lambda: OrderManager())
        self._hedge_order_manager_map = defaultdict(lambda: AtTouchHedgeOrderManager(logger=self.logger))
        self._global_order_manager = OrderManager()
        # self.prc_per_diff = 0.00005 / 5
        self._contract_mapper = ContractMapper()
        self._ref_prc_cal = ReferencePriceCalculator()
        self.send = True
        self.stats: List[Dict] = []
        self.bbo_index = 0
        self._n_def_cancel = 0
        self._prev_bbo: BBO = ...

        self.cfg: Config = ...

        self._prev_ref_prc: Dict = dict()
        self._ref_prc_map: Dict = dict()
        self._xex_ref_prc_map: Dict = dict()
        self._ret_map: Dict = dict()

        self._mm_exchanges = set()
        self._snap_hedge_exchanges = set()
        self._exchanges = set()
        self._xex = "xex"
        self._numeric_tol = 1e-10

        # TODO
        self._allow_inventory_mismatch_count = 0
        self._on_wakeup_count = 0
        self.ws_queue = {
            "trade": Queue(),
            "order": Queue(),
            "orderbook": LifoQueue(),
            "bbo": LifoQueue(),
        }
        
        self.traders = [
                "trader_test",
            ]
        
        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()
    
    # preparation
    def init_before_strategy_starts(self):
        self.cfg = Config(
            CONTRACT=self.contract,
            MM_EXCHANGES=[
                # "binance",
                "binance_busd",
            ],
            ART=1e3,
            SIGMAT=1e4,
            HST=1e4,
            RETT=1e3,
            REFPRCT=1e5,
            INVENT=1e5,
            DMT=1e3,
            TRL_COUNT=1000,
            TRADE_STABILITY_BUFFER_WINDOW=1,
            REF_PRC_RESET_SIGMA_MULTIPLIER=1e10,
            TAU_LB=100,
            LATENCYT=100,
            QUOTE_DEFENSE_T=1000,
            EVENT_COUNT_HALFLIFE=100,
            BBO_SWITCH=True,
            FOLDER_NAME="",
            TRADE_DEFENSE_CANCEL_AR_MULTIPLIER=3,
            DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER=3,
            DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER=3,
            BETA=0,
            T=10000,
            PRED_ON=False,
            PRED_REF_PRC_MODE=PredRefPrcMode.xex,
            PROFIT_MARGIN_RETURN=1e-3,
            MDL_PARAMS=ModelParams(
                pred_on=False,
                trade_symbol_to_related_symbols_map={}, 
                trade_symbol_to_model_path_map={},
            ),
            MSG="",
            QUOTE_LVL_CONFIGS=dict(
                binance=QuoteLevelConfig(RETURN_UB=1e-5, RETURN_STEP_SIZE=1e-5, QUOTE_QUANTITY=2000, CANCEL_TOL=1e-6),
                binance_busd=QuoteLevelConfig(RETURN_UB=1e-5, RETURN_STEP_SIZE=1e-5, QUOTE_QUANTITY=25, CANCEL_TOL=1e-6),
            ),
            MAX_INVEN=500,
            CONSECUTIVE_FILL_LIMIT=2,
        )
        self._ref_prc_xex_bbo = XExBBO(contract=self.contract)
        self._post_cost_ref_prc_xex_bbo = XExBBO(contract=self.contract)
        self._depth_map = dict()

        self.signal_calculator_map = dict()
        mdl_params = self.cfg.MDL_PARAMS
        for trd_sym, rel_syms in mdl_params.trade_symbol_to_related_symbols_map.items():
            self.signal_calculator_map[trd_sym] = SignalCalculatorBase(pred_on=mdl_params.pred_on,
                                                                       trade_symbol=trd_sym,
                                                                       related_symbols=rel_syms,
                                                                       params=...)
        self.local_calculator = LocalCalculatorBase(
            art=self.cfg.ART,
            sigmat=self.cfg.SIGMAT,
            hst=self.cfg.HST,
            rett=self.cfg.RETT,
            refprct=self.cfg.REFPRCT,
            latencyt=self.cfg.LATENCYT,
            invent=self.cfg.INVENT,
            dmt=self.cfg.DMT,
            quote_defense_t=self.cfg.QUOTE_DEFENSE_T,
            event_count_halflife=self.cfg.EVENT_COUNT_HALFLIFE,
            trl_count_halflife=self.cfg.TRL_COUNT,
            trade_stability_buffer_window=self.cfg.TRADE_STABILITY_BUFFER_WINDOW,
        )

        self.executor = AtTouchExecutor(
            contract=self.contract,
            amt_bnd=0,  # This is handled when building depth
            pred_on=self.cfg.PRED_ON,
            pred_ref_prc_mode=self.cfg.PRED_REF_PRC_MODE,
            beta=self.cfg.BETA,
            ref_prc_reset_sigma_multiplier=self.cfg.REF_PRC_RESET_SIGMA_MULTIPLIER,
            tau_lb=self.cfg.TAU_LB,
            T=self.cfg.T,
            mdl_params=self.cfg.MDL_PARAMS,
            local_cal=self.local_calculator,
            quote_lvl_cfgs=self.cfg.QUOTE_LVL_CONFIGS,
            max_inven=self.cfg.MAX_INVEN,
            consecutive_fill_limit=self.cfg.CONSECUTIVE_FILL_LIMIT,
        )
        self.executor.link_mm_order_manager_map(mm_order_manager_map=self._mm_order_manager_map)
        self.executor.link_hedge_order_manager_map(hedge_order_manager_map=self._hedge_order_manager_map)
        if self.cfg.PRED_ON:
            for trd_sym, model_path in self.cfg.MDL_PARAMS.trade_symbol_to_model_path_map.items():
                self.signal_calculator_map[trd_sym].load_ref_prc_model(pickle_address=model_path)
                ex, contract = trd_sym.split(".")
                self.executor.update_signal_cal(signal_cal=self.signal_calculator_map[trd_sym], ex=ex)

        self._mm_exchanges = set(self.cfg.MM_EXCHANGES)

        self._mm_iids = set()
        for ex in self.cfg.MM_EXCHANGES:
            self._mm_iids.add(f"{ex}.{self.cfg.CONTRACT}")
            self.logger.info(f"{ex}.{self.cfg.CONTRACT}")

        self._prc_precision = dict()
        for iid in self._mm_iids:
            pp_iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=iid)
            self._prc_precision[iid] = price_precision[pp_iid]
            
        self._inventory_manager = XexInventoryManager(contract=self.cfg.CONTRACT)
        
        ccy = self.cfg.CONTRACT.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        self.load_balancer = LoadBalancer(ccy1=ccy1, ccy2=ccy2)
        self.load_balancer.setup(
            acc_list=self.account_names,
            trader_list=self.traders
        )
        
        self.canceling_id = set()
        self.quering_id = set()
        
        self.send = False
        
    async def before_strategy_start(self):
        self.logger.setLevel("INFO")
        self.init_before_strategy_starts()
        for acc in self.account_names:  
            ex, _ = acc.split(".")
            for piid in self._mm_iids:
                iid: str = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
                if iid.startswith(ex):
                    symbol = f"{iid}.{self.market}.{self.sub_market}"
                    self.direct_subscribe_order_update(symbol_name=symbol,account_name=acc)
                    await asyncio.sleep(1)
        
        for piid in self._mm_iids:
            symbol = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
            symbol = f"{symbol}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            bnd = self.bnd_amt_in_coin / symbol_cfg["contract_value"]
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_public_trade(symbol_name=symbol)
            self.direct_subscribe_bbo(symbol_name=symbol)
        
        self.loop.create_task(self.handle_ws_update())

        self.loop.create_task(
            self.subscribe_to_channel(
                ["mq:hf_signal:send_response"],
                self.process_send_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                ["mq:hf_signal:cancel_response"],
                self.process_cancel_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                ["mq:hf_signal:query_response"],
                self.process_query_response
                )
            )
        
        
        self.send_response = dict()
        self.cancel_response = dict()
        self.query_response = dict()
        
        counter = 0
        while True:
            await asyncio.sleep(1)
            counter += 1
            
            has_price = True
            for ex in self.cfg.MM_EXCHANGES:
                if self._ref_prc_map.get((self.cfg.CONTRACT, ex)) == None:
                    has_price = False
                    
            if has_price:
                await self.account_preparation()
                break
            
            if counter > 100:
                self.logger.critical("no price info coming")
                exit()
        
        self.logger.critical("start......")
        
        self.send = True
    
    async def account_preparation(self):
        # TODO 
        
        for ex in self.cfg.MM_EXCHANGES:
            ref_prc = self._ref_prc_map.get((self.cfg.CONTRACT, ex))
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                ex, _ = acc.split(".")
                for piid in self._mm_iids:
                    iid: str = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
                    if iid.startswith(ex):
                        symbol = f"{iid}.{self.market}.{self.sub_market}"
                        try:
                            await api.flash_cancel_orders(self.get_symbol_config(symbol))
                        except:
                            pass
                        
                        cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data
                        self.load_balancer.update_balance(acc=acc, balance=float(cur_balance["busd"]["all"]))
                        
                        await asyncio.sleep(0.2)
                        cur_position = (await api.contract_position(self.get_symbol_config(symbol))).data
                        self.logger.info(f"current position: {cur_position}")
                        pos = cur_position["long_qty"] - cur_position["short_qty"]
                        
                        p, side = (ref_prc * (1 - 0.001), Direction.short) if pos > 0 else (ref_prc * (1 + 0.001), Direction.long)
                        qty = round_decimals_down(abs(pos), qty_precision[iid])
                        
                        order = TransOrder(
                            side=side,
                            p=p,
                            q=qty,
                            iid=piid,
                            floor=0,
                            maker_taker=MakerTaker.taker,
                        )
                        piid = order.iid
                        iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
                        order.symbol=f"{iid}.{self.market}.{self.sub_market}"
                        self.logger.info(f"order: {order.__dict__}")
                        cid = self.trim_order_before_sending(order)
                        self._rebalance_manager_map[order.iid.split(".")[0]].add_trans_order(order=order, oid=cid)
                        self._global_order_manager.add_trans_order(order=order, oid=cid)
                        self.loop.create_task(self.base_send_order(order=order, order_manager=self._rebalance_manager_map[order.iid.split(".")[0]]))
                        await asyncio.sleep(3)
        
    # websocket handling
    async def on_order(self, order: PartialOrder):
        try:
            await self.ws_queue["order"].put(order)
        except:
            traceback.print_exc()
    
    async def on_public_trade(self, symbol, trade: PublicTrade):
        try:
            await self.ws_queue["trade"].put((symbol, trade))
        except:
            traceback.print_exc()
        
    async def on_bbo(self, symbol, bbo: BBODepth):
        try:
            await self.ws_queue["bbo"].put((symbol, bbo))
        except:
            traceback.print_exc()
    
    async def on_orderbook(self, symbol, orderbook):
        try:
            await self.ws_queue["orderbook"].put((symbol, orderbook))
        except:
            traceback.print_exc()
        
    def handle_order(self, order: PartialOrder):
        oid = order.client_id
        
        if not self._global_order_manager.contains_trans_order(oid=oid):
            return
        
        piid = self._global_order_manager.trans_order_at(oid=oid).iid
        ex, contract = piid.split(".")
        if self._mm_order_manager_map[ex].contains_trans_order(oid=oid):
            torder = self._mm_order_manager_map[ex].trans_order_at(oid=oid)
            iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=torder.iid)
            order_obj: Order = Order.from_dict(
                porder=order,
                torder=torder,
                contract_value=self.get_symbol_config(f"{iid}.{self.market}.{self.sub_market}")["contract_value"]
                )

            """ Update Inventory """
            qty_chg = 0
            if order_obj.filled_qty > self._numeric_tol:
                qty_chg = self._mm_order_manager_map[ex].get_order_inc_fill_qty_in_tokens(order=order_obj)
                if qty_chg > self._numeric_tol:
                    self.update_inventory(iid=piid, chg_qty=order_obj.side * qty_chg, ts=order_obj.local_time)
                    consecutive_fill_tracker = self.local_calculator.consecutive_fill_tracker_map[(contract, ex)]
                    consecutive_fill_tracker.on_fill(d=order_obj.side, qty=qty_chg, oid=order_obj.client_id)
                    consecutive_fill_direction, n_consecutive_fills = consecutive_fill_tracker.get_factor(ts=order_obj.local_time)
                    self.logger.info(f"consecutive_fill_direction={consecutive_fill_direction}, n_consecutive_fills={n_consecutive_fills}")
                    self.executor.update_n_consecutive_hits(consecutive_fill_direction=consecutive_fill_direction, n_consecutive_fills=n_consecutive_fills)

            self._mm_order_manager_map[ex].update_order(order=order_obj)

            """ Clean up """
            order_status = order.xchg_status
            if order_status in OrderStatus.fin_status():
                self.load_balancer.finished_order_perp(torder=torder, order_obj=order_obj)
                self._mm_order_manager_map[ex].pop_trans_order(oid=oid)
                self._global_order_manager.pop_trans_order(oid=oid)
                self._mm_order_manager_map[ex].pop_order(oid=oid)
                

            """ On fill """
            if qty_chg > self._numeric_tol:

                """ Hedge on inventory change """
                self.update_hedge_orders()

                if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                    assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
                else:
                    I = self._inventory_manager.total_inventory
                    H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                    ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                    if abs(I - H) * ref_prc > self.executor.inven_tol:
                        assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                        self._allow_inventory_mismatch_count = 0

                # trade_obj = Trade(ex=order_obj.ex,
                #           contract=order_obj.contract,
                #           price=order_obj.avg_fill_prc,
                #           quantity=qty_chg,
                #           tx_time=order_obj.local_time,
                #           local_time=order_obj.local_time,
                #           side=-order_obj.side,
                #           mexc_side=-order_obj.side,
                #           uid=order_obj.client_id)
                # halfsprd = self.local_calculator.instant_halfsprd_cal_map[(order_obj.contract, order_obj.ex)].get_factor(ts=order_obj.local_time)
                # if halfsprd is None:
                #     return
                # success = self._ref_prc_xex_bbo.update_trade(trade=trade_obj, halfsprd=halfsprd)
                # if not success:
                #     return
                # success = self._update_ref_prc_suit(contract=trade_obj.contract, ex=trade_obj.ex)
                # if not success:
                #     return
                # success = self.executor.update_depth(depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=trade_obj.ex))
                # if not success:
                #     return
                #
                # trade_stability_cal = self.local_calculator.trade_stability_cal_map[(order_obj.contract, order_obj.ex)]
                # trade_stability_cal.add_trade(trade=trade_obj)
                # trade_stable = trade_stability_cal.get_factor(ts=trade_obj.local_time)
                # if trade_stable == TradeStabilityState.stable:
                #     data = self.update_mm_orders()

            return

        if self._hedge_order_manager_map[ex].contains_trans_order(oid=oid):
            torder = self._hedge_order_manager_map[ex].trans_order_at(oid=oid)
            iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=torder.iid)
            order_obj: Order = Order.from_dict(
                porder=order,
                torder=torder,
                contract_value=self.get_symbol_config(f"{iid}.{self.market}.{self.sub_market}")["contract_value"]
                )

            """ Update Inventory """
            qty_chg = 0
            if order_obj.filled_qty > self._numeric_tol:
                qty_chg = self._hedge_order_manager_map[ex].get_order_inc_fill_qty_in_tokens(order=order_obj)
                if qty_chg > self._numeric_tol:
                    self.update_inventory(iid=piid, chg_qty=order_obj.side * qty_chg, ts=order_obj.local_time)

            if order_obj.client_id in self._hedge_order_manager_map[ex].pending_cancel_hedge_oids:
                self._allow_inventory_mismatch_count += 1
            self._hedge_order_manager_map[ex].update_order(order=order_obj)

            """ Clean up """
            order_status = order.xchg_status
            if order_status in OrderStatus.fin_status():
                self.load_balancer.finished_order_perp(torder=torder, order_obj=order_obj)
                if order_status == OrderStatus.Failed:
                    # logging.debug(f"Hedge order failed: {self._hedge_order_manager.get_trans_order(oid=oid)}")
                    self._allow_inventory_mismatch_count += 1
                self._hedge_order_manager_map[ex].pop_hedge_order(oid=oid)
                self._global_order_manager.pop_trans_order(oid=oid)
                

            """ Hedge on inventory change """
            if qty_chg > self._numeric_tol:

                self.update_hedge_orders()

                if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                    assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
                else:
                    I = self._inventory_manager.total_inventory
                    H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                    ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                    if abs(I - H) * ref_prc > self.executor.inven_tol:
                        assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                        self._allow_inventory_mismatch_count = 0

                # trade_obj = Trade(ex=order_obj.ex,
                #           contract=order_obj.contract,
                #           price=order_obj.avg_fill_prc,
                #           quantity=qty_chg,
                #           tx_time=order_obj.local_time,
                #           local_time=order_obj.local_time,
                #           side=-order_obj.side,
                #           mexc_side=-order_obj.side,
                #           uid=order_obj.client_id)
                # halfsprd = self.local_calculator.instant_halfsprd_cal_map[(order_obj.contract, order_obj.ex)].get_factor(ts=order_obj.local_time)
                # if halfsprd is None:
                #     return
                # success = self._ref_prc_xex_bbo.update_trade(trade=trade_obj, halfsprd=halfsprd)
                # if not success:
                #     return
                # success = self._update_ref_prc_suit(contract=trade_obj.contract, ex=trade_obj.ex)
                # if not success:
                #     return
                # success = self.executor.update_depth(depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=trade_obj.ex))
                # if not success:
                #     return
                #
                # trade_stability_cal = self.local_calculator.trade_stability_cal_map[(order_obj.contract, order_obj.ex)]
                # trade_stability_cal.add_trade(trade=trade_obj)
                # trade_stable = trade_stability_cal.get_factor(ts=trade_obj.local_time)
                # if trade_stable == TradeStabilityState.stable:
                #     data = self.update_mm_orders()

            return

    
    def handle_trade(self, symbol: str, trade: PublicTrade):
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        trade_obj = Trade.from_dict_prod(
            ex=ex,
            contract=contract,
            trade=trade
        )
        
        iid_tup = (contract, ex)

        if not trade_obj.is_valid:
            return

        """ Latency state """
        self.local_calculator.latency_cal_map[iid_tup].add_trade(trade=trade_obj)
        latency = self.local_calculator.latency_cal_map[iid_tup].get_factor(ts=trade_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        if self.cfg.MDL_PARAMS.is_related_symbol(iid=iid):
            trd_syms = self.cfg.MDL_PARAMS.related_symbol_to_trade_symbols_map[iid]
            for trd_sym in trd_syms:
                self.signal_calculator_map[trd_sym].update_trade(trade=trade_obj)
        elif self.cfg.MDL_PARAMS.is_trade_symbol(iid=iid):
            self.signal_calculator_map[iid].update_trade(trade=trade_obj)
        else:
            pass

        halfsprd = self.local_calculator.instant_halfsprd_cal_map[(trade_obj.contract, trade_obj.ex)].get_factor(ts=trade_obj.local_time)
        if halfsprd is None:
            return
        success = self._ref_prc_xex_bbo.update_trade(trade=trade_obj, halfsprd=halfsprd)
        if not success:
            return
        success = self._update_ref_prc_suit(contract=contract, ex=ex)
        if not success:
            return

        success = self.executor.update_depth(depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex))
        if not success:
            return
        success = self.executor.update_trade(trade_obj)
        if not success:
            return

        trade_stability_cal = self.local_calculator.trade_stability_cal_map[(trade_obj.contract, trade_obj.ex)]
        trade_stability_cal.add_trade(trade=trade_obj)
        trade_stable = trade_stability_cal.get_factor(ts=trade_obj.local_time)
        trade_stability_cal.add_bbo(bbo=self._ref_prc_xex_bbo.get_depth_for_ex(ex=trade_obj.ex).to_bbo())
        if trade_stable == TradeStabilityState.stable:

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            self.update_hedge_orders()

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            data = self.update_mm_orders()
        else:
            self.loop.create_task(
                self.insertWakeup(data=dict(
                    timestamp=trade_obj.local_time + self.cfg.TRADE_STABILITY_BUFFER_WINDOW,
                    contract=trade_obj.contract,
                    ex=trade_obj.ex,
                ))
            )
            
    async def insertWakeup(self, data):
        await asyncio.sleep(self.cfg.TRADE_STABILITY_BUFFER_WINDOW)
        self.onWakeup(data)
        
    def handle_bbo(self, symbol :str, bbo: BBODepth):
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        
        bbo_obj = BBO.from_dict_prod(iid=iid, bbo_depth=bbo)
        
        if not bbo_obj.valid_bbo:
            return

        contract = bbo_obj.meta.contract
        ex = bbo_obj.meta.ex
        server_ts = bbo_obj.server_time
        local_ts = bbo_obj.local_time
        iid_tup = (contract, ex)

        ism = self.iid_state_manager
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Latency state """
        self.local_calculator.latency_cal_map[iid_tup].add_bbo(bbo=bbo_obj)
        latency = self.local_calculator.latency_cal_map[iid_tup].get_factor(ts=bbo_obj.local_time)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

        """ Server time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.is_active(iid=iid):
            return

        if self._prev_bbo is not Ellipsis:
            bid_prc_chged = abs(math.log(bbo_obj.best_bid_prc / self._prev_bbo.best_bid_prc)) > 1e-6
            ask_prc_chged = abs(math.log(bbo_obj.best_ask_prc / self._prev_bbo.best_ask_prc)) > 1e-6
            if not ask_prc_chged and not bid_prc_chged:
                return

        if ex not in self.executor.active_exs:
            return

        """ Record prev ref prc """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
            if not success:
                raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=self._xex, ref_prc=ref_prc)
            if not success:
                raise RuntimeError()

        """ Update ref prc """
        success = self._ref_prc_xex_bbo.update_bbo(bbo=bbo_obj)
        if not success:
            return
        success = self._update_ref_prc_suit(contract=contract, ex=ex)
        if not success:
            return

        """ Update depth metrics """
        success = self.executor.update_depth(depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex))
        if not success:
            return
        success = self.executor.update_bbo(bbo=bbo_obj)
        if not success:
            return
        success = self.executor.add_active_ex(ex=ex)
        if not success:
            return

        """ Compute and record ref prcs and returns """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=ex)
            if prev_ref_prc is not None:
                ret = math.log(ref_prc / prev_ref_prc)
                success = self._update_ret(contract=contract, ex=ex, ret=ret)
                if not success:
                    raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=self._xex)
            if prev_ref_prc is not None:
                ret = math.log(ref_prc / prev_ref_prc)
                success = self._update_ret(contract=contract, ex=self._xex, ret=ret)
                if not success:
                    raise RuntimeError()

        trade_stability_cal = self.local_calculator.trade_stability_cal_map[(bbo_obj.meta.contract, bbo_obj.meta.ex)]
        success = trade_stability_cal.add_bbo(bbo=bbo_obj)
        if not success:
            return
        trade_stable = trade_stability_cal.get_factor(ts=bbo_obj.local_time)
        if trade_stable == TradeStabilityState.stable:

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            self.update_hedge_orders()

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            data = self.update_mm_orders()

        """ Defensive cancel is fast. """
        # ex, contract = iid.split(".")
        # self.defensive_cancel_orders_on_bbo(contract=contract, ex=ex, bbo=bbo_obj)
        self._prev_bbo = bbo_obj

        if not self.executor.onboard:
            self.stats.append(self.executor.get_stats(ts=bbo_obj.local_time))
            for i, (int_prc, oid_set) in enumerate(self._mm_order_manager_map[ex].ask_ladder.items()):
                outstanding_oids = [oid for oid in oid_set if not self._mm_order_manager_map[ex].get_trans_order(oid).cancel]
                if outstanding_oids:
                    self.stats[-1][f"ask_lvl_{i}"] = self._mm_order_manager_map[ex].int_prc_to_float_prc(int_prc=int_prc)
            for i, (int_prc, oid_set) in enumerate(self._mm_order_manager_map[ex].bid_ladder.items()):
                outstanding_oids = [oid for oid in oid_set if not self._mm_order_manager_map[ex].get_trans_order(oid).cancel]
                if outstanding_oids:
                    self.stats[-1][f"bid_lvl_{i}"] = self._mm_order_manager_map[ex].int_prc_to_float_prc(int_prc=int_prc)
    
    def handle_orderbook(self, symbol: str, orderbook):
        iid = self._contract_mapper.iid_to_proxy_iid(iid=symbol)
        ex, contract = iid.split(".")
        ccy = contract.split("_")
        depth_obj = Depth.from_dict(
            contract=contract, 
            ccy1=ccy[0], 
            ccy2=ccy[1],
            ex=ex,
            depth=orderbook
            )
        ex = depth_obj.meta_data.ex
        contract = depth_obj.meta_data.contract
        server_ts = depth_obj.server_ts
        local_ts = depth_obj.resp_ts

        """ Latency state """
        iid_tup = (contract, ex)
        self.local_calculator.latency_cal_map[iid_tup].add_depth(depth=depth_obj)
        latency = self.local_calculator.latency_cal_map[iid_tup].get_factor(ts=depth_obj.resp_ts)
        self._update_latency(iid=iid, latency=latency, allow_rectivate=True)

        """ tx time guard """
        if self.server_time_guard.has_last_server_time(iid=iid):
            last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
            if server_ts < last_server_time:
                return
        self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

        ism = self.iid_state_manager
        """ Activate stale state """
        is_in_stale_state = ism.is_in_stale_state(iid=iid)
        if is_in_stale_state:
            ism.enter_nonstale_state(iid=iid)
        ism.update_heart_beat_time(iid=iid, ts=local_ts)

        """ Check stale """
        for nsiids in ism.nonstale_iids.copy():
            is_active = ism.is_active(iid=nsiids)
            if ism.is_stale(iid=nsiids, ts=local_ts):
                ism.enter_stale_state(iid=nsiids)
                if is_active:
                    self.deactivate(iid=nsiids)

        if not ism.active_iids:
            return

        if not ism.is_active(iid=iid):
            return

        """ Record prev ref prc """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=ex, ref_prc=self._get_ref_prc(contract=contract, ex=ex))
            if not success:
                raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            success = self._update_prev_ref_prc(contract=contract, ex=self._xex, ref_prc=self._get_ref_prc(contract=contract, ex=self._xex))
            if not success:
                raise RuntimeError()

        """ Update depth metrics """
        if self.cfg.MDL_PARAMS.is_related_symbol(iid=iid):
            trd_syms = self.cfg.MDL_PARAMS.related_symbol_to_trade_symbols_map[iid]
            for trd_sym in trd_syms:
                self.signal_calculator_map[trd_sym].update_depth(depth=depth_obj)
        elif self.cfg.MDL_PARAMS.is_trade_symbol(iid=iid):
            self.signal_calculator_map[iid].update_depth(depth=depth_obj)
        else:
            pass

        """ Update ref prc"""
        success = self._ref_prc_xex_bbo.update_depth(depth=depth_obj)
        if not success:
            return
        success = self._update_ref_prc_suit(contract=contract, ex=ex)
        if not success:
            return

        """ Update XExdepth """
        success = self.executor.update_depth(depth=depth_obj)
        if not success:
            return
        success = self.executor.add_active_ex(ex=ex)
        if not success:
            return

        """ Compute and record ref prcs and returns """
        ref_prc = self._get_ref_prc(contract=contract, ex=ex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=ex)
            if prev_ref_prc is not None:
                success = self._update_ret(contract=contract, ex=ex, ret=math.log(ref_prc / prev_ref_prc))
                if not success:
                    raise RuntimeError()

        ref_prc = self._get_ref_prc(contract=contract, ex=self._xex)
        if ref_prc is not None:
            prev_ref_prc = self._get_prev_ref_prc(contract=contract, ex=self._xex)
            if prev_ref_prc is not None:
                success = self._update_ret(contract=contract, ex=self._xex, ret=math.log(ref_prc / prev_ref_prc))
                if not success:
                    raise RuntimeError()

        trade_stability_cal = self.local_calculator.trade_stability_cal_map[(depth_obj.meta_data.contract, depth_obj.meta_data.ex)]
        success = trade_stability_cal.add_bbo(bbo=depth_obj.to_bbo())
        if not success:
            return
        trade_stable = trade_stability_cal.get_factor(ts=depth_obj.resp_ts)
        if trade_stable == TradeStabilityState.stable:

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            self.update_hedge_orders()

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            data = self.update_mm_orders()

        if not self.executor.onboard:
            self.stats.append(self.executor.get_stats(ts=depth_obj.resp_ts))
            for i, (int_prc, oid_set) in enumerate(self._mm_order_manager_map[ex].ask_ladder.items()):
                outstanding_oids = [oid for oid in oid_set if not self._mm_order_manager_map[ex].get_trans_order(oid).cancel]
                if outstanding_oids:
                    self.stats[-1][f"ask_lvl_{i}"] = self._mm_order_manager_map[ex].int_prc_to_float_prc(int_prc=int_prc)
            for i, (int_prc, oid_set) in enumerate(self._mm_order_manager_map[ex].bid_ladder.items()):
                outstanding_oids = [oid for oid in oid_set if not self._mm_order_manager_map[ex].get_trans_order(oid).cancel]
                if outstanding_oids:
                    self.stats[-1][f"bid_lvl_{i}"] = self._mm_order_manager_map[ex].int_prc_to_float_prc(int_prc=int_prc)
        
    async def handle_ws_update(self):
        while True:
            try:
                if not self.ws_queue["order"].empty():
                    order = await self.ws_queue["order"].get()
                    self.handle_order(order)
                    continue
                elif not self.ws_queue["trade"].empty():
                    symbol, trade = await self.ws_queue["trade"].get()
                    self.handle_trade(symbol, trade)
                    continue
                elif self.ws_queue["orderbook"].empty() and self.ws_queue["bbo"].empty():
                    await asyncio.sleep(0.001)
                    continue
                elif not self.ws_queue["orderbook"].empty():
                    symbol, orderbook = await self.ws_queue["orderbook"].get()
                    self.ws_queue["orderbook"] = LifoQueue()
                    self.handle_orderbook(symbol, orderbook)
                elif not self.ws_queue["bbo"].empty():
                    symbol, bbo = await self.ws_queue["bbo"].get()
                    self.ws_queue["bbo"] = LifoQueue()
                    self.handle_bbo(symbol, bbo)
                    
                await asyncio.sleep(0.0001)
            except:
                await asyncio.sleep(0.0001)
                traceback.print_exc()
                
    
    # base operation, send, cancel, query with load balancer
    async def base_send_order(self, order: TransOrder, order_manager: OrderManager) -> Union[AtomOrder, None]:
        if not self.volume_notional_check(symbol=order.symbol, price=order.price, qty=order.quantity):
            self.loop.create_task(self.internal_fail(client_id=order.client_id, acc=order.account_name, order_manager=order_manager))
            return
        order.maker_taker = MakerTaker.maker
        new_order = order.to_dict()
        trader = self.load_balancer.get_trader()
        try:
            self.send_response[order.client_id] = {
                "future":asyncio.Future(), 
                "create_time":int(time.time()), 
                "trader":trader,
                "client_id":order.client_id,
                "order_manager":order_manager
                }
            s = time.time()*1e3
            await self.publish_to_channel(
                f"mq:hf_signal:send_request:{trader}", 
                {
                    "order": new_order, 
                    "strategy_name": self.strategy_name,
                    "trader": trader
                    }
                )
            resp, headers = await self.send_response[order.client_id]["future"]
            self.send_response.pop(order.client_id, None)
            if isinstance(headers, dict):
                self.load_balancer.update_limit(
                    acc=order.account_name,
                    trader_name=trader,
                    acc_load=headers.get(ACC_LOAD_KEY),
                    ip_load=headers.get(IP_LOAD_KEY)
                    )
                if headers.get(ACC_LOAD_KEY) and int(headers.get(ACC_LOAD_KEY)) > 200:
                    self.loop.create_task(self.handle_rate_limit(msg=-1015, account_name=order.account_name))
            if resp:
                order.xchg_id = resp.xchg_id
            else:
                self.load_balancer.sub_send()
                self.loop.create_task(self.internal_fail(client_id=order.client_id, acc=order.account_name, order_manager=order_manager, err=headers))
                if isinstance(headers, str):
                    self.logger.info(f"info about this failed order: {headers}")
                # self.logger.info(f"available load balancer: {self.load_balancer._available_map[order.account_name]}")
                # api = self.get_exchange_api_by_account(account_name=order.account_name)
                # cur_balance = (await api.account_balance(self.get_symbol_config(order.symbol))).data
                # self.logger.info(f"""query balance btc:{float(cur_balance["btc"]["available"])}, usdt:{float(cur_balance["usdt"]["available"])}""")
                
        except Exception as err:
            traceback.print_exc()
            self.logger.critical(f"base send order err:{err}")
            self.load_balancer.sub_send()
            self.loop.create_task(self.internal_fail(client_id=order.client_id, acc=order.account_name, order_manager=order_manager, err=err))
        
    async def handle_cancel_order(self, order: TransOrder, order_manager: OrderManager):
        async def unit_cancel_order(order: TransOrder):
            trader = self.load_balancer.get_trader()
            symbol = self._contract_mapper.proxy_iid_to_iid(order.iid)
            ex, _ = symbol.split(".")
            cancel_id = ClientIDGenerator.gen_client_id(exchange_name=ex)
            order.cancel_id = cancel_id
            new_order = order.to_dict()
            try:
                self.cancel_response[cancel_id] = {
                    "future":asyncio.Future(), 
                    "create_time":int(time.time()), 
                    "trader":trader,
                    "client_id":order.client_id,
                    "order_manager":order_manager
                }
                self.load_balancer.add_cancel()
                await self.publish_to_channel(
                    f"mq:hf_signal:cancel_request:{trader}", 
                    {
                        "order": new_order, 
                        "strategy_name": self.strategy_name,
                        "trader": trader
                        }
                    )
                res, headers = await self.cancel_response[cancel_id]["future"]
                self.cancel_response.pop(cancel_id, None)
                if res == True:
                    order.cancel = True
                    # self.logger.info(f"confirmed cancel cid={order.client_id}")
                if isinstance(headers, dict):
                    self.load_balancer.update_limit(
                        acc=order.account_name,
                        trader_name=trader,
                        acc_load=headers.get(ACC_LOAD_KEY),
                        ip_load=headers.get(IP_LOAD_KEY)
                    )
            except Exception as err:
                self.logger.critical(f'cancel order {order.xchg_id} err {err}')
        
        cid = order.client_id
        if cid in self.canceling_id:
            return
        
        self.canceling_id.add(cid)
    
        counter = 0
        while True:
            if order_manager.get_trans_order(oid=cid) != None:
                o = order_manager.trans_order_at(oid=cid)
                if o.cancel:
                    self.canceling_id.remove(cid)
                    return
            else:
                self.canceling_id.remove(cid)
                return
            counter += 1
            self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(0.1)
            
            if counter > 1000:
                if order_manager.get_trans_order(oid=cid) != None:
                    o = order_manager.trans_order_at(oid=cid)
                    self.logger.warning(f"too many cancel orders, cache order={o.__dict__}")
                self.logger.warning(f"too many cancel orders, order={order.__dict__}")
                exit()
    
    async def handle_query_order(self, order: TransOrder, order_manager: OrderManager):
        async def unit_query_order(order: TransOrder):
            trader = self.load_balancer.get_trader()
            symbol = self._contract_mapper.proxy_iid_to_iid(order.iid)
            ex, _ = symbol.split(".")
            query_id = ClientIDGenerator.gen_client_id(exchange_name=ex)
            order.query_id = query_id
            new_order = order.to_dict()
            try:
                self.query_response[query_id] = {
                    "future":asyncio.Future(), 
                    "create_time":int(time.time()), 
                    "trader":trader,
                    "client_id":order.client_id,
                    "order_manager":order_manager
                }
                self.load_balancer.add_query()
                await self.publish_to_channel(
                        f"mq:hf_signal:query_request:{trader}", 
                        {
                            "order": new_order, 
                            "strategy_name": self.strategy_name,
                            "trader": trader
                            }
                        )
                resp, headers = await self.query_response[query_id]["future"]
                self.query_response.pop(query_id, None)
                if isinstance(headers, dict):
                    self.load_balancer.update_limit(
                        acc=order.account_name,
                        trader_name=trader,
                        acc_load=headers.get(ACC_LOAD_KEY),
                        ip_load=headers.get(IP_LOAD_KEY)
                    )
            except:
                return

            if not resp:
                return
            if resp.xchg_status in OrderStatus.fin_status():
                await self.on_order(resp)
            # else:
            #     await self.handle_cancel_order(order=order, order_manager=order_manager)
        
        cid = order.client_id  
        if time.time()*1e3 - order.sent_ts < 1000 or cid in self.quering_id:
            return
        
        self.quering_id.add(cid)
        
        counter = 0
        while True:
            if order_manager.get_trans_order(oid=cid) != None:
                o =  order_manager.trans_order_at(oid=cid)
                if time.time()*1e3 - order.sent_ts < 1000:
                    self.quering_id.remove(cid)
                    return
            else:
                self.quering_id.remove(cid)
                return
            counter += 1
            self.loop.create_task(unit_query_order(order))
            await asyncio.sleep(10)
            
            if counter > 1000:
                if order_manager.get_trans_order(oid=cid) != None:
                    o = order_manager.trans_order_at(oid=cid)
                    self.logger.critical(f"too many query orders, cache order={o.__dict__}")
                self.logger.critical(f"too many query orders, order={order.__dict__}")
                exit()
                
    async def handle_rate_limit(self, msg=None, account_name=None):
        if msg == -1015:
            self.load_balancer.ban_account(account_name)
            await asyncio.sleep(5)
            self.load_balancer.free_account(account_name)
            return
        self.logger.info("reach rate limit")
        self.send = False
        await asyncio.sleep(30)
        self.send = True
    
    def set_result_to_future(self, fut:asyncio.Future, data):
        try:
            fut.set_result(data)
        except:
            pass
    
    async def process_send_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        if self.send_response.get(payload["client_id"]) == None:
            return
        future: asyncio.Future = self.send_response[payload["client_id"]]["future"]
        if payload["status"] == False:
            self.logger.info(f"error message: {payload['data']}")
            if payload.get("rate_limit"):
                self.loop.create_task(self.handle_rate_limit(msg=int(payload.get("rate_limit")), account_name=payload.get("account_name")))
            self.set_result_to_future(future, (None, payload["data"]))
        else:
            self.set_result_to_future(future, (AtomOrder.from_dict(payload["data"]), payload["headers"]))
        
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        if self.cancel_response.get(payload["cancel_id"]) == None:
            return
        if payload["status"] == False and payload.get("rate_limit"):
            self.loop.create_task(self.handle_rate_limit())
        future: asyncio.Future = self.cancel_response[payload["cancel_id"]]["future"]
        self.set_result_to_future(future, (payload["status"],payload["headers"]))
        
        
    async def process_query_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        if self.query_response.get(payload["query_id"]) == None:
            return
        future: asyncio.Future = self.query_response[payload["query_id"]]["future"]
        if payload["status"] == False:
            if payload.get("rate_limit"):
                self.loop.create_task(self.handle_rate_limit())
            self.set_result_to_future(future, (None, payload["headers"]))
        else:
            self.set_result_to_future(future, (AtomOrder.from_dict(payload["data"]), payload["headers"]))
    
    async def check_process_timeout(self):
        def unit_check(data):
            cur_time = int(time.time())
            for key in data:
                create_time = data[key]["create_time"]
                om = data[key]["order_manager"]
                cid = data[key]["client_id"]
                
                if cur_time - create_time > 5:
                    if om.get_trans_order(oid=cid) == None:
                        self.set_result_to_future(data[key]["future"], (None, None))
                        continue
                    self.set_result_to_future(data[key]["future"], (None, data[key]['trader']))
                    # self.load_balancer.ban_trader(data[key]["trader"])
                    self.logger.info(f"trader {data[key]['trader']} not working properly, cid={cid}")
        
        
        while True:
            await asyncio.sleep(2)
            unit_check(self.send_response)
            await asyncio.sleep(2)
            unit_check(self.cancel_response)
            await asyncio.sleep(2)
            unit_check(self.query_response)
        
    # strategy base function override
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    # strategy utils only for prod
    async def internal_fail(self, client_id: str, acc: str, order_manager: OrderManager, err=None):
        try:
            if err:
                self.logger.info(f"failed order: {err}, acc:{acc}, client_id={client_id}")
            if order_manager.contains_trans_order(oid=client_id):
                o = order_manager.trans_order_at(oid=client_id)
            else:
                self.logger.info("cannot find this failed order everywhere")
                return
            order = PartialOrder(
                account_name=o.account_name,
                xchg_id=None,
                exchange_pair="",
                client_id=client_id,
                xchg_status=OrderStatus.Failed,
                filled_amount=0,
                avg_filled_price=0,
                commission_fee=0,
                local_ms=int(time.time() * 1e3),
                server_ms=int(time.time() * 1e3),
                )
            await self.on_order(order)
        except:
            traceback.print_exc()
            
    def trim_order_before_sending(self, order: TransOrder):
        symbol = self._contract_mapper.proxy_iid_to_iid(order.iid)
        ex, _ = symbol.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        order.symbol_id = self.get_symbol_config(order.symbol)["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[symbol])
        else:
            order.price = round_decimals_up(order.price, price_precision[symbol])
        
        order.quantity = round(order.quantity, qty_precision[symbol])
        
        return cid
    
    def check_orders(self, order_manager: OrderManager):
        for oid in order_manager.keys():
            if order_manager.get_trans_order(oid):
                o = order_manager.trans_order_at(oid)
                self.loop.create_task(self.handle_query_order(order=o, order_manager=order_manager))
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
    
    # strategy utils for prod and backtest
    def update_inventory(self, iid: str, chg_qty: float, ts: int):
        ex, contract = iid.split(".")
        self._inventory_manager.increment_inventory(contract=contract, ex=ex, qty_chg=chg_qty)
        if contract == self.executor.contract:
            ex_inven = self._inventory_manager.get_inventory(contract=contract, ex=ex)
            self.executor.update_inventory(inventory=ex_inven, contract=contract, ex=ex, ts=ts)
            self.logger.info(f"iid={iid},current inventory={ex_inven}")
    
    def defensive_cancel_orders_on_trade(self, contract: str, ex: str, trd: Trade):

        mm_mdl = self.executor.mm_mdl
        local_cal = self.local_calculator
        ts = trd.local_time

        iid_tup = (contract, ex)
        depth_freq, bbo_freq, trade_freq = local_cal.update_freq_cal_map[iid_tup].get_factor(ts=ts)
        if depth_freq is None:
            return

        tau = depth_freq  # order update frequency
        pred_las, pred_sas = local_cal.pred_trade_arrival_size(contract=contract, ex=ex, ts=ts)
        if (pred_las is None) or (pred_sas is None):
            return
        pred_lat, pred_sat = local_cal.pred_trade_arrival_time(contract=contract, ex=ex, ts=ts)
        if (pred_lat is None) or (pred_sat is None):
            return
        pred_lat = max(1, pred_lat)
        pred_sat = max(1, pred_sat)
        curr_las, curr_sas = local_cal.impulse_arrival_size_cal_map[iid_tup].get_factor(ts=ts)
        if (curr_las is None) or (curr_sas is None):
            return
        curr_lat, curr_sat = local_cal.impulse_arrival_time_cal_map[iid_tup].get_factor(ts=ts)
        if (curr_lat is None) or (curr_sat is None):
            return
        curr_lat = max(1, curr_lat)
        curr_sat = max(1, curr_sat)
        avg_las, avg_sas = local_cal.arrival_size_cal_map[iid_tup].get_factor(ts=ts)
        if (avg_las is None) or (avg_sas is None):
            return
        avg_lat, avg_sat = local_cal.arrival_time_cal_map[iid_tup].get_factor(ts=ts)
        if (avg_lat is None) or (avg_sat is None):
            return
        ref_prc = mm_mdl.get_ref_prc(contract=contract, ex=ex)

        ask_prc_touch_line = ref_prc
        bid_prc_touch_line = ref_prc

        if trd.side == Direction.long:
            frequent_event = max(1/pred_lat, 1/curr_lat) > avg_lat * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            large_quantity = max(pred_las, curr_las) > avg_las * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            if frequent_event or large_quantity:
                lar = max(pred_las, curr_las, pred_las * tau / pred_lat, curr_las * tau / curr_lat)
                a_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="a_s")
                if a_s is None:
                    return
                b_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="b_s")
                if b_s is None:
                    return
                s_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="s_s")
                if s_s is None:
                    return
                delta = (lar + self.executor.mm_mdl._epsilon - b_s) / max(1e-6, a_s)
                ask_prc_touch_line = max(ref_prc + s_s + delta, 0)
        elif trd.side == Direction.short:
            frequent_event = max(1/pred_sat, 1/curr_sat) > avg_sat * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            large_quantity = max(pred_sas, curr_sas) > avg_sas * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            if frequent_event or large_quantity:
                sar = max(pred_sas, curr_sas, pred_sas * tau / pred_sat, curr_sas * tau / curr_sat)
                a_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="a_l")
                if a_l is None:
                    return
                b_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="b_l")
                if b_l is None:
                    return
                s_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="s_l")
                if s_l is None:
                    return
                delta = (sar + self.executor.mm_mdl._epsilon - b_l) / max(1e-6, a_l)
                bid_prc_touch_line = max(ref_prc - s_l - delta, 0)
        else:
            raise NotImplementedError()

        I = self._inventory_manager.total_inventory
        self.defensive_cancel_orders(ex=ex, ask_touch_line=ask_prc_touch_line, bid_touch_line=bid_prc_touch_line, I=I)
        self._n_def_cancel += 1

    def _update_latency(self, iid: str, latency: float, allow_rectivate: bool):
        ism = self.iid_state_manager
        is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
        is_active = ism.is_active(iid=iid)
        if not is_in_high_latency_state:
            if ism.is_high_latency(iid=iid, latency=latency):
                ism.enter_high_latency_state(iid=iid)
                if is_active:
                    self.deactivate(iid=iid)
        else:  # is_in_high_latency_state
            if not ism.is_high_latency(iid=iid, latency=latency):
                if allow_rectivate:
                    ism.enter_normal_latency_state(iid=iid)
    
    def defensive_cancel_orders_on_bbo(self, contract: str, ex: str, bbo: BBO):

        """ SEx or XEx? That is the question. """


        exec = self.executor
        mm_mdl = exec.mm_mdl
        sig_cal = self.signal_calculator_map[self._main_signal_iid()]
        local_cal = self.local_calculator
        ts = bbo.local_time

        if not mm_mdl.has_depth_metrics(contract=contract, ex=ex):
            return

        tau = exec.get_tau(ex=ex, ts=ts)
        if tau is None:
            return

        ref_prc = mm_mdl.get_ref_prc(contract=contract, ex=ex)
        if ref_prc is None:
            return

        if self.cfg.PRED_ON:
            pred_ret = sig_cal.predict_ref_prc_ret(ms=tau)
        else:
            pred_ret = local_cal.predict_ref_prc_ret(contract=contract, ex=ex, ts=ts)
        if pred_ret is None:
            pred_ret = 0
        pred_long_half_sprd = local_cal.predict_long_half_spread(contract=contract, ex=ex, ts=ts)
        pred_short_half_sprd = local_cal.predict_short_half_spread(contract=contract, ex=ex, ts=ts)
        pred_ref_prc = ref_prc * (1 + pred_ret * tau)
        pred_bid_touch_line = pred_ref_prc - pred_long_half_sprd
        pred_ask_touch_line = pred_ref_prc + pred_short_half_sprd

        curr_ret = local_cal.curr_impulse_ref_prc_ret(contract=contract, ex=ex, ts=ts)
        if curr_ret is None:
            curr_ret = 0
        curr_long_half_sprd = local_cal.curr_impulse_long_half_spread(contract=contract, ex=ex, ts=ts)
        curr_short_half_sprd = local_cal.curr_impulse_short_half_spread(contract=contract, ex=ex, ts=ts)
        curr_ref_prc = ref_prc * (1 + curr_ret * tau)
        curr_bid_touch_line = curr_ref_prc - curr_long_half_sprd
        curr_ask_touch_line = curr_ref_prc + curr_short_half_sprd

        abs_ret = max(abs(curr_ret), abs(pred_ret))
        half_sprd = max(pred_long_half_sprd, pred_short_half_sprd, curr_long_half_sprd, curr_short_half_sprd)
        bid_touch_line = min(pred_bid_touch_line, curr_bid_touch_line)
        ask_touch_line = max(pred_ask_touch_line, curr_ask_touch_line)

        avg_sigma = local_cal.curr_ema_sigma(contract=contract, ex=ex, ts=ts)
        large_abs_ret_bnd = avg_sigma * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER
        lhs = local_cal.curr_impulse_long_half_spread(contract=contract, ex=ex, ts=ts)
        shs = local_cal.curr_emavg_short_half_spread(contract=contract, ex=ex, ts=ts)
        avg_half_sprd = (lhs + shs) / 2
        large_hs_bnd = avg_half_sprd * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER

        large_ret = abs_ret > large_abs_ret_bnd
        large_sprd = half_sprd > large_hs_bnd

        if large_sprd or large_ret:
            I = self._inventory_manager.total_inventory
            self.defensive_cancel_orders(ex=ex, ask_touch_line=ask_touch_line, bid_touch_line=bid_touch_line, I=I)
            self._n_def_cancel += 1

            # logging.info(f"n_def_cancel={self._n_def_cancel},ask_touch_line={ask_prc_touch_line}, bid_touch_line={bid_prc_touch_line}, I={I}")

    def _update_prev_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        self._prev_ref_prc[(contract, ex)] = ref_prc
        return True

    def _get_prev_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._prev_ref_prc.get((contract, ex))

    def _remove_prev_ref_prc(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._prev_ref_prc:
            self._prev_ref_prc.pop((contract, ex))

    def _update_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        ce = (contract, ex)
        self._ref_prc_map[ce] = ref_prc
        self.local_calculator.trade_ret_lambda_cal_map[ce].update_ref_prc(ref_prc=ref_prc)
        if contract == self.executor.contract:
            self.executor.update_ref_prc(ex=ex, ref_prc=ref_prc)
        return True

    def _get_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._ref_prc_map.get((contract, ex))

    def _remove_ref_prc(self, contract: str, ex: str) -> bool:
        self._ref_prc_map.pop((contract, ex), None)
        return True

    def _update_xex_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        self._xex_ref_prc_map[(contract, ex)] = ref_prc
        if contract == self.executor.contract:
            self.executor.update_xex_ref_prc(ex=ex, ref_prc=ref_prc)
        return True

    def _get_xex_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._xex_ref_prc_map.get((contract, ex))

    def _remove_xex_ref_prc(self, contract: str, ex: str) -> bool:
        self._xex_ref_prc_map.pop((contract, ex), None)
        return True

    def _update_ret(self, contract: str, ex: str, ret: float) -> bool:
        self._ret_map[(contract, ex)] = ret
        return True

    def _get_ret(self, contract: str, ex: str) -> Union[float, None]:
        return self._ret_map.get((contract, ex))

    def _remove_ret(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._ret_map:
            self._ret_map.pop(key)

    def deactivate(self, iid: str):
        ex, contract = iid.split(".")
        xex = "xex"
        if ex in self._mm_exchanges:
            if self._ref_prc_xex_bbo.contains(ex=ex):
                self._ref_prc_xex_bbo.remove_ex(ex=ex)
            if self._post_cost_ref_prc_xex_bbo.contains(ex=ex):
                self._post_cost_ref_prc_xex_bbo.remove_ex(ex=ex)
            if ex in self._depth_map:
                self._depth_map.pop(ex)
        ref_prc = self._post_cost_ref_prc_xex_bbo.get_xex_ref_prc()
        if ref_prc is not Ellipsis:
            self._update_xex_ref_prc(contract=contract, ex=xex, ref_prc=ref_prc)
        self.executor.remove_active_ex(ex=ex)
        self.cancel_all_orders_for_iid(iid=iid)

    def _update_ref_prc_suit(self, contract: str, ex: str):
        success = self._ref_prc_xex_bbo.update_ref_prc(ex=ex)
        if not success:
            return False
        ref_prc = self._ref_prc_xex_bbo.get_ref_prc(ex=ex)
        if ref_prc is None:
            return False
        success = self._update_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
        if not success:
            return False
        success = self._post_cost_ref_prc_xex_bbo.update_depth_for_ex(
            depth=self._ref_prc_xex_bbo.get_depth_for_ex(ex=ex).get_post_cost_view())
        if not success:
            return False
        success = self._post_cost_ref_prc_xex_bbo.update_emavg_ref_prc_sprd(ex=ex)
        if not success:
            return False
        ref_prc = self._post_cost_ref_prc_xex_bbo.get_shifted_xex_ref_prc(ex=ex)
        if ref_prc is None:
            return False
        success = self._update_xex_ref_prc(contract=contract, ex=ex, ref_prc=ref_prc)
        if not success:
            return False
        ref_prc = self._post_cost_ref_prc_xex_bbo.get_xex_ref_prc()
        if ref_prc is Ellipsis:
            return False
        success = self._update_xex_ref_prc(contract=contract, ex=self._xex, ref_prc=ref_prc)
        if not success:
            return False
        return True
    
    def onWakeup(self, data):

        ts = data["timestamp"]
        contract = data["contract"]
        ex = data["ex"]
        trade_stability_cal = self.local_calculator.trade_stability_cal_map[(contract, ex)]
        trade_stable = trade_stability_cal.get_factor(ts=ts)
        if trade_stable == TradeStabilityState.stable:

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            self.update_hedge_orders()

            if self._hedge_order_manager_map[ex].hedge_orders_side is Ellipsis:
                assert self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty == 0
            else:
                I = self._inventory_manager.total_inventory
                H = -self._hedge_order_manager_map[ex].hedge_orders_side * self._hedge_order_manager_map[ex].hedge_order_total_unfilled_qty
                ref_prc = self._get_ref_prc(contract=self.contract, ex=ex)
                if abs(I - H) * ref_prc > self.executor.inven_tol:
                    assert self._allow_inventory_mismatch_count > 0, f"{I}_{H}"
                    self._allow_inventory_mismatch_count = 0

            data = self.update_mm_orders()

    def update_hedge_orders(self):
        hedge_orders_to_send_map, hedge_oids_to_cancel_map = self.executor.get_hedge_orders()
        self.logger.info(f"hedge trade desicison")
        self.logger.info(f"{hedge_orders_to_send_map}")
        self.logger.info(f"{hedge_oids_to_cancel_map}")
        for ex, oids in hedge_oids_to_cancel_map.items():
            for oid in oids:
                self._cancel_hedge_order(ex=ex, oid=oid)
        for ex, orders in hedge_orders_to_send_map.items():
            for order in orders:
                piid = order.iid
                iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
                order.symbol=f"{iid}.{self.market}.{self.sub_market}"
                self.send_hedge_order(order=order, order_manager=self._hedge_order_manager_map[ex])
            self.logger.info(f"ex={ex}, outstanding_hedge_orders=")
            for oid, o in self._hedge_order_manager_map[ex].trans_order_items():
                setattr(o, "update_time", self.current_time())
                setattr(o, "life_span", o.update_time - o.sent_ts)
                self.logger.info(f"oid={oid}, order={o.__dict__}")

    def update_mm_orders(self):
        mm_orders_to_send_map, mm_oids_to_cancel_map, data = self.executor.get_orders(t=self.current_time())
        self.logger.info(f"mm trade desicison")
        self.logger.info(f"{mm_orders_to_send_map}")
        self.logger.info(f"{mm_oids_to_cancel_map}")
        for ex, oids in mm_oids_to_cancel_map.items():
            for oid in oids:
                self._cancel_mm_order(ex=ex, oid=oid)
        for ex, orders in mm_orders_to_send_map.items():
            for order in orders:
                piid = order.iid
                iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)
                order.symbol=f"{iid}.{self.market}.{self.sub_market}"
                self.send_mm_order(order, self._mm_order_manager_map[ex])
            self.logger.info(f"ex={ex}, outstanding_mm_orders=")
            for oid, o in self._mm_order_manager_map[ex].trans_order_items():
                setattr(o, "update_time", self.current_time())
                setattr(o, "life_span", o.update_time - o.sent_ts)
                self.logger.info(f"oid={oid}, order={o.__dict__}")
        return data
    
    def send_mm_order(self, order: TransOrder, order_manager: OrderManager):
        assert order.iid in self._mm_iids
        if not self.send:
            return
        orders = self.load_balancer.split_orders_perp(order=order)
        for order in orders:
            self.load_balancer.send_order_perp(order=order)
            cid = self.trim_order_before_sending(order)
            order_manager.add_trans_order(order=order, oid=cid)
            self._global_order_manager.add_trans_order(order=order, oid=cid)
            self.loop.create_task(self.base_send_order(order=order,order_manager=order_manager))
    
    def send_hedge_order(self, order: TransOrder, order_manager: AtTouchHedgeOrderManager):
        assert order.iid in self._mm_iids
        if not self.send:
            return
        # orders = self.load_balancer.split_orders_perp(order=order)
        # for order in orders:
        order.account_name = self.account_names[0]
        self.load_balancer.send_order_perp(order=order)
        cid = self.trim_order_before_sending(order)
        order_manager.add_hedge_trans_order(hedge_order=order, oid=cid)
        self._global_order_manager.add_trans_order(order=order, oid=cid)
        self.loop.create_task(self.base_send_order(order=order,order_manager=order_manager))

    def _cancel_mm_order(self, ex: str, oid: str):
        o = self._mm_order_manager_map[ex].trans_order_at(oid=oid)
        if not o.cancel:
            self.loop.create_task(self.handle_cancel_order(order=o, order_manager=self._mm_order_manager_map[ex]))

    def _cancel_hedge_order(self, ex: str, oid: str):
        o = self._hedge_order_manager_map[ex].trans_order_at(oid=oid)
        if not o.cancel:
            self.loop.create_task(self.handle_cancel_order(order=o, order_manager=self._hedge_order_manager_map[ex]))
            # logging.debug(f"Cancel hedge order: {self.current_time()} {oid} {o}")
            self._hedge_order_manager_map[ex].hedge_order_pending_cancel(oid=oid)

    def defensive_cancel_orders(self, ex: str, ask_touch_line: float, bid_touch_line: float, I: float):
        if I >= 0:  # long
            for int_prc, oid_set in self._mm_order_manager_map[ex].bid_ladder.items():
                prc = int_prc / self._mm_order_manager_map[ex].trans_order_manager.price_multilier
                if prc > bid_touch_line:
                    for oid in oid_set:
                        self._cancel_mm_order(ex=ex, oid=oid)
                    continue
                break
        if I <= 0:  # short
            for int_prc, oid_set in self._mm_order_manager_map[ex].ask_ladder.items():
                prc = int_prc / self._mm_order_manager_map[ex].trans_order_manager.price_multilier
                if prc < ask_touch_line:
                    for oid in oid_set:
                        order = self._mm_order_manager_map[ex].trans_order_at(oid)
                        if not order.cancel:
                            self.loop.create_task(self.handle_cancel_order(order=order, order_manager=self._mm_order_manager_map[ex]))
                    continue
                break

    def _cancel_mm_orders_for_iid(self, iid: str):
        ex, contract = iid.split(".")
        for oid in self._mm_order_manager_map[ex].get_oids_for_iid(iid=iid):
            self._cancel_mm_order(ex=ex, oid=oid)

    def _cancel_hedge_orders_for_iid(self, iid: str):
        ex, contract = iid.split(".")
        for oid in self._hedge_order_manager_map[ex].get_oids_for_iid(iid=iid):
            self._cancel_hedge_order(ex=ex, oid=oid)

    def cancel_all_orders_for_iid(self, iid: str):
        self._cancel_mm_orders_for_iid(iid=iid)
        self._cancel_hedge_orders_for_iid(iid=iid)
        
    def current_time(self):
        return int(time.time()*1e3)

    # strategy main logic
    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(10)
            for om in self._mm_order_manager_map.values():
                self.check_orders(order_manager=om)
            for om in self._hedge_order_manager_map.values():
                self.check_orders(order_manager=om)
            
    async def load_balancer_reduce_count(self):
        while True:
            await asyncio.sleep(1)
            self.load_balancer.reduce_count(1)
            
    # stats
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(20)
            try:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":self.strategy_name}, 
                        fields={
                            "inventory":float(self._inventory_manager.get_inventory(contract=self.cfg.CONTRACT, ex=self.cfg.MM_EXCHANGES[0])),
                            }
                        )
                    )
                
                for oid, order in list(self._global_order_manager.trans_order_items()):
                    if order.side == Direction.short and order.floor == 0:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    "cache_sell_price":float(order.price)
                                    }
                                )
                            )
                    elif order.side == Direction.long and order.floor == 0:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    "cache_buy_price":float(order.price)
                                    }
                                )
                            )
                
                
                base_amt = 0

                for dymm_ex in self.cfg.MM_EXCHANGES:
                    ref_prc = self._ref_prc_map.get((self.cfg.CONTRACT, dymm_ex))
                    for acc in self.account_names:
                        api = self.get_exchange_api_by_account(acc)
                        piid = f"{dymm_ex}.{self.cfg.CONTRACT}"
                        iid = self._contract_mapper.proxy_iid_to_iid(proxy_iid=piid)

                        symbol = f"{iid}.{self.market}.{self.sub_market}"
                        
                        await asyncio.sleep(0.2)
                        cur_balance = (await api.account_balance(self.get_symbol_config(symbol))).data

                        base_amt += float(cur_balance["busd"]["all"])
                        self.load_balancer.update_balance(acc=acc, balance=float(cur_balance["busd"]["all"]))
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="tt", 
                                tag={"sn":self.strategy_name}, 
                                fields={
                                    f"{acc}_load":float(self.load_balancer._account_load_map[acc])
                                    }
                                )
                            ) 

                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt", 
                            tag={"sn":self.strategy_name}, 
                            fields={
                                "balance": float(base_amt),
                                }
                            )
                        ) 
                
                for trader in list(self.load_balancer._ip_load_map.keys()):
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt", 
                            tag={"sn":self.strategy_name}, 
                            fields={
                                f"{trader}_load": float(self.load_balancer._ip_load_map[trader])
                            }
                        )
                    )
                    
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="tt", 
                        tag={"sn":self.strategy_name}, 
                        fields={
                            f"send per min": float(self.load_balancer.send_counter) / 20 * 60,
                            f"cancel per min": float(self.load_balancer.cancel_counter) / 20 * 60,
                            f"query per min": float(self.load_balancer.query_counter) / 20 * 60,
                        }
                    )
                )
                self.load_balancer.send_counter = 0
                self.load_balancer.cancel_counter = 0
                self.load_balancer.query_counter = 0
                
            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
            
    async def update_redis_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    ccy = self.cfg.CONTRACT.split("_")
                    for dymm_ex in self.cfg.MM_EXCHANGES:
                        for acc in self.account_names:
                            api = self.get_exchange_api_by_account(acc)
                            ex, _ = acc.split(".")
                            if dymm_ex.endswith("_spot"):
                                contract = f"{ccy[0]}_{ccy[1]}"
                            else:
                                contract = self.cfg.CONTRACT
                            symbol = f"{ex}.{contract}.{self.market}.{self.sub_market}"
                            try:
                                await api.flash_cancel_orders(self.get_symbol_config(symbol))
                            except:
                                pass
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache(
                    {
                        "exit":None, 
                        }
                    )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
        
    # strategy core
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.reset_missing_order_action(),
                self.get_local_stats(),
                self.update_redis_cache(),
                self.load_balancer_reduce_count(),
                self.check_process_timeout()
            )
            
if __name__ == '__main__':
    # logging.getLogger().setLevel("WARNING")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()