
from xex_mm.calculators.arrival_rate import CalImpulseArrivalRate, CalEMAvgArrivalRate
from xex_mm.calculators.arrival_size import CalEMAvgArrivalSize, CalImpulseArrivalSize
from xex_mm.calculators.arrival_time import CalEMAvgArrivalTime, CalImpulseArrivalTime
from xex_mm.calculators.latency import CalEMAvgLatency
from xex_mm.calculators.ref_prc import CalEMAvgRefPrc
from xex_mm.calculators.ref_prc_return import CalEMAvgRet, CalImpulseRet
from xex_mm.calculators.update_frequency import CalUpdateFrequency
from xex_mm.calculators.sigma import CalImpulseSigma, CalEMAvgSigma
from xex_mm.calculators.half_sprd import CalImpulseHalfSpread, CalEMAvgHalfSpread
from xex_mm.signals.SignalCalculator import SignalCalculatorBase
from xex_mm.utils.base import Depth, Trade, BBO
from xex_mm.utils.inventory import CalEMAvgInventorySigma


class LocalCalibCalculator(SignalCalculatorBase):
    def __init__(self, art: float, sigmat: float, hst: float, rett: float, refprct: float,
                 latencyt: float, invent: float, event_count_halflife: int):
        super(LocalCalibCalculator, self).__init__()
        self._art = art
        self._sigmat = sigmat
        self._hst = hst
        self._rett = rett
        self._refprct = refprct
        self._latencyt = latencyt
        self._invent = invent
        self._event_count_halflife = event_count_halflife
        self.calib_map = dict()
        self._setup_calib()

    def _setup_calib(self):
        self.calib_map["PastEMAvgArrivalRate"] = CalEMAvgArrivalRate(halflife=self._art)
        self.calib_map["PastImpulseArrivalRate"] = CalImpulseArrivalRate(halflife=self._art)
        self.calib_map["PastEMAvgSigma"] = CalEMAvgSigma(halflife=self._sigmat)
        self.calib_map["PastImpulseSigma"] = CalImpulseSigma(halflife=self._sigmat)
        self.calib_map["CalEMAvgHalfSpread"] = CalEMAvgHalfSpread(halflife=self._hst)
        self.calib_map["CalImpulseHalfSpread"] = CalImpulseHalfSpread(halflife=self._hst)
        self.calib_map["PastEMAvgReturn"] = CalEMAvgRet(halflife=self._rett)
        self.calib_map["PastImpulseReturn"] = CalImpulseRet(halflife=self._rett)

        self.local_latency_cal = CalEMAvgLatency(halflife=self._latencyt)
        self.local_ref_prc_cal = CalEMAvgRefPrc(halflife=self._refprct)
        self.local_arrival_size_cal = CalEMAvgArrivalSize(halflife=self._event_count_halflife)
        self.local_arrival_time_cal = CalEMAvgArrivalTime(halflife=self._event_count_halflife)
        self.local_impulse_arrival_size_cal = CalImpulseArrivalSize(halflife=self._event_count_halflife)
        self.local_impulse_arrival_time_cal = CalImpulseArrivalTime(halflife=self._event_count_halflife)
        self.local_inven_sigma_cal = CalEMAvgInventorySigma(halflife=self._invent)
        self.local_update_freq_cal = CalUpdateFrequency(halflife=self._event_count_halflife)

    def update_depth(self, depth: Depth):
        """
        Args:
            depth (Depth): list of level class,
            depth level default to 20.
            ask price 1: depth.asks[0].prc,
            ask quantity 1: depth.asks[0].qty,
            use resp_ts(in ms)
        """
        self.calib_map["PastEMAvgReturn"].add_depth(depth)
        self.calib_map["PastImpulseReturn"].add_depth(depth)
        self.calib_map["PastEMAvgArrivalRate"].add_depth(depth)
        self.calib_map["PastImpulseArrivalRate"].add_depth(depth)
        self.calib_map["PastEMAvgSigma"].add_depth(depth)
        self.calib_map["PastImpulseSigma"].add_depth(depth)
        self.calib_map["CalEMAvgHalfSpread"].add_depth(depth)
        self.calib_map["CalImpulseHalfSpread"].add_depth(depth)

        self.local_latency_cal.add_depth(depth=depth)
        self.local_ref_prc_cal.add_depth(depth=depth)
        self.local_update_freq_cal.add_depth(depth=depth)

    def update_trade(self, trade: Trade):
        """
        Args:
            trade (Trade):
            use local_time(in ms)
        """
        # self._local_factor_map["PastEMAvgReturn"].add_trade(trade)
        # self._local_factor_map["PastImpulseReturn"].add_trade(trade)
        self.calib_map["PastEMAvgArrivalRate"].add_trade(trade)
        self.calib_map["PastImpulseArrivalRate"].add_trade(trade)
        # self._local_factor_map["PastEMAvgSigma"].add_trade(trade)
        # self._local_factor_map["PastImpulseSigma"].add_trade(trade)
        self.calib_map["CalEMAvgHalfSpread"].add_trade(trade)
        self.calib_map["CalImpulseHalfSpread"].add_trade(trade)

        self.local_latency_cal.add_trade(trade=trade)
        # self.local_ref_prc_cal.add_trade(trade=trade)
        self.local_arrival_size_cal.add_trade(trade=trade)
        self.local_arrival_time_cal.add_trade(trade=trade)
        self.local_impulse_arrival_size_cal.add_trade(trade=trade)
        self.local_impulse_arrival_time_cal.add_trade(trade=trade)
        self.local_update_freq_cal.add_trade(trade=trade)

    def update_bbo(self, bbo: BBO):
        self.calib_map["PastEMAvgArrivalRate"].add_bbo(bbo=bbo)
        self.calib_map["PastImpulseArrivalRate"].add_bbo(bbo=bbo)
        self.calib_map["PastEMAvgSigma"].add_bbo(bbo=bbo)
        self.calib_map["PastImpulseSigma"].add_bbo(bbo=bbo)
        self.calib_map["CalEMAvgHalfSpread"].add_bbo(bbo=bbo)
        self.calib_map["CalImpulseHalfSpread"].add_bbo(bbo=bbo)
        self.calib_map["PastEMAvgReturn"].add_bbo(bbo=bbo)
        self.calib_map["PastImpulseReturn"].add_bbo(bbo=bbo)

        self.local_latency_cal.add_bbo(bbo=bbo)
        self.local_ref_prc_cal.add_bbo(bbo=bbo)
        self.local_update_freq_cal.add_bbo(bbo=bbo)

    def predict_ref_prc_ret(self, ts: int):
        return self.curr_emavg_ref_prc_ret(ts=ts)

    """ Predict Function """

    def predict_long_arrival_rate(self, ts: int):
        return self.curr_emavg_long_arrival_rate(ts=ts)

    def predict_short_arrival_rate(self, ts: int):
        return self.curr_emavg_short_arrival_rate(ts=ts)

    def predict_long_half_spread(self, ts: int):
        return self.curr_impulse_long_half_spread(ts=ts)

    def predict_short_half_spread(self, ts: int):
        return self.curr_impulse_short_half_spread(ts=ts)

    def predict_sigma(self, ts: int):
        return self.curr_ema_sigma(ts=ts)

    def pred_latency(self, ts: int):
        return self.curr_emavg_latency(ts=ts)

    def pred_trade_arrival_size(self, ts: int):
        return self.curr_trade_arrival_size(ts=ts)

    def pred_trade_arrival_time(self, ts: int):
        return self.curr_trade_arrival_time(ts=ts)

    """ Current Stats """

    def curr_impulse_ref_prc_ret(self, ts: int):
        par: CalImpulseRet = self.calib_map["PastImpulseReturn"]
        return par.get_factor(ts=ts)

    def curr_emavg_ref_prc_ret(self, ts: int):
        par: CalEMAvgRet = self.calib_map["PastEMAvgReturn"]
        return par.get_factor(ts=ts)

    def curr_emavg_ref_prc(self, ts: int):
        return self.local_ref_prc_cal.get_factor(ts=ts)

    def curr_emavg_long_arrival_rate(self, ts: int):
        par: CalEMAvgArrivalRate = self.calib_map["PastEMAvgArrivalRate"]
        return par.get_factor(ts=ts)[0]

    def curr_emavg_short_arrival_rate(self, ts: int):
        par: CalEMAvgArrivalRate = self.calib_map["PastEMAvgArrivalRate"]
        return par.get_factor(ts=ts)[1]

    def curr_impulse_long_arrival_rate(self, ts: int):
        par: CalEMAvgArrivalRate = self.calib_map["PastImpulseArrivalRate"]
        return par.get_factor(ts=ts)[0]

    def curr_impulse_short_arrival_rate(self, ts: int):
        par: CalEMAvgArrivalRate = self.calib_map["PastImpulseArrivalRate"]
        return par.get_factor(ts=ts)[1]

    def curr_emavg_long_half_spread(self, ts: int):
        ps: CalEMAvgHalfSpread = self.calib_map["CalEMAvgHalfSpread"]
        return ps.get_factor(ts=ts)

    def curr_emavg_short_half_spread(self, ts: int):
        ps: CalEMAvgHalfSpread = self.calib_map["CalEMAvgHalfSpread"]
        return ps.get_factor(ts=ts)

    def curr_impulse_long_half_spread(self, ts: int):
        ps: CalImpulseHalfSpread = self.calib_map["CalImpulseHalfSpread"]
        return ps.get_factor(ts=ts)

    def curr_impulse_short_half_spread(self, ts: int):
        ps: CalImpulseHalfSpread = self.calib_map["CalImpulseHalfSpread"]
        return ps.get_factor(ts=ts)

    def curr_ema_sigma(self, ts: int):
        ps: CalEMAvgSigma = self.calib_map["PastEMAvgSigma"]
        return ps.get_factor(ts=ts)

    def curr_impulse_sigma(self, ts: int):
        ps: CalImpulseSigma = self.calib_map["PastImpulseSigma"]
        return ps.get_factor(ts=ts)

    def curr_emavg_latency(self, ts: int):
        return self.local_latency_cal.get_factor(ts=ts)

    def curr_trade_arrival_size(self, ts: int):
        return self.local_arrival_size_cal.get_factor(ts=ts)

    def curr_trade_arrival_time(self, ts: int):
        return self.local_arrival_time_cal.get_factor(ts=ts)

    def curr_inven_sigma(self, ts: int):
        return self.local_inven_sigma_cal.get_factor(ts=ts)

