from xex_mm.calculators.em import EMEvent
from xex_mm.utils.base import Depth, BBO, Trade
from xex_mm.utils.calc_node import CalRealTime


class CalEMAvgLatency(CalRealTime):
    def __init__(self, halflife):
        super(CalEMAvgLatency, self).__init__()
        self._emavg_latency = EMEvent(halflife=halflife)

    def add_depth(self, depth: Depth):
        latency = depth.resp_ts - depth.server_ts
        self._emavg_latency.on_val(val=latency)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            latency = bbo.local_time - bbo.server_time
            self._emavg_latency.on_val(val=latency)

    def add_trade(self, trade: Trade):
        latency = trade.local_time - trade.tx_time
        self._emavg_latency.on_val(val=latency)

    def get_factor(self, ts: int):
        return self._emavg_latency.get_avg()