from typing import List, Tuple

import numpy as np

from xex_mm.calculators.em import EMAvgNoDecay
from xex_mm.utils.base import Depth, BBO, Level
from xex_mm.utils.calc_node import CalRealTime


class CalEMAvgDepthMetrics(CalRealTime):
    def __init__(self, halflife: float):
        super(CalEMAvgDepthMetrics, self).__init__()
        self._halflife = halflife
        self._long_slope_cal = EMAvgNoDecay(halflife=self._halflife)
        self._long_intercept_cal = EMAvgNoDecay(halflife=self._halflife)
        self._short_slope_cal = EMAvgNoDecay(halflife=self._halflife)
        self._short_intercept_cal = EMAvgNoDecay(halflife=self._halflife)
        self._numeric_tol = 1e-10
        self.await_reset = True

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def _update_depth_metrics_on_depth(self, long_q_intercept: float, long_q_slope: float,
                                       short_q_intercept: float, short_q_slope: float, ts: int):
        if self.await_reset:
            self._long_intercept_cal.init(long_q_intercept, ts=ts)
            self._long_slope_cal.init(long_q_slope, ts=ts)
            self._short_intercept_cal.init(short_q_intercept, ts=ts)
            self._short_slope_cal.init(short_q_slope, ts=ts)
            self.await_reset = False
            return
        if not self._long_intercept_cal.is_initialized or not self._long_slope_cal.is_initialized or \
                not self._short_intercept_cal.is_initialized or not self._short_slope_cal.is_initialized:
            self._long_intercept_cal.init(avg=long_q_intercept, ts=ts)
            self._long_slope_cal.init(avg=long_q_slope, ts=ts)
            self._short_intercept_cal.init(short_q_intercept, ts=ts)
            self._short_slope_cal.init(short_q_slope, ts=ts)
            return
        self._long_intercept_cal.on_val(val=long_q_intercept, ts=ts)
        self._long_slope_cal.on_val(val=long_q_slope, ts=ts)
        self._short_intercept_cal.on_val(val=short_q_intercept, ts=ts)
        self._short_slope_cal.on_val(val=short_q_slope, ts=ts)

    def _update_depth_metrics_on_bbo(self, long_q_intercept: float, short_q_intercept: float, ts: int):
        if self.await_reset:
            self._long_intercept_cal.init(long_q_intercept, ts=ts)
            self._short_intercept_cal.init(short_q_intercept, ts=ts)
            self.await_reset = False
            return
        if not self._long_intercept_cal.is_initialized or not self._short_intercept_cal.is_initialized:
            self._long_intercept_cal.init(avg=long_q_intercept, ts=ts)
            self._short_intercept_cal.init(short_q_intercept, ts=ts)
            return
        self._long_intercept_cal.on_val(val=long_q_intercept, ts=ts)
        self._short_intercept_cal.on_val(val=short_q_intercept, ts=ts)

    def _calib_Q_func(self, side: List[Level]) -> Tuple[float, float]:
        if len(side) == 0:
            return 0, 0
        if len(side) == 1:
            return 0, side[0].qty
        Q = 0
        Qs = []
        ps = []
        for lvl in side:
            Q += lvl.qty
            Qs.append(Q)
            ps.append(lvl.prc)
        X = np.abs(np.log(np.array(ps) / ps[0]))
        y = np.array(Qs) - Qs[0]
        beta = 1 / X.dot(X) * X.dot(y)
        return beta, Qs[0]

    def add_depth(self, depth: Depth):
        long_slope, long_intercept = self._calib_Q_func(side=depth.bids)
        short_slope, short_intercept = self._calib_Q_func(side=depth.asks)
        self._update_depth_metrics_on_depth(long_q_intercept=long_intercept, long_q_slope=long_slope,
                                            short_q_intercept=short_intercept, short_q_slope=short_slope,
                                            ts=depth.resp_ts)

    def add_bbo(self, bbo: BBO):
        self._update_depth_metrics_on_bbo(long_q_intercept=bbo.best_bid_qty, short_q_intercept=bbo.best_ask_qty, ts=bbo.local_time)

    def get_factor(self, ts: int):
        is_ready = True
        is_ready = is_ready and self._long_intercept_cal.is_initialized
        is_ready = is_ready and self._long_slope_cal.is_initialized
        is_ready = is_ready and self._short_intercept_cal.is_initialized
        is_ready = is_ready and self._short_slope_cal.is_initialized
        if is_ready:
            return self._long_intercept_cal.get_avg(ts=ts), \
                   self._long_slope_cal.get_avg(ts=ts), \
                   self._short_intercept_cal.get_avg(ts=ts), \
                   self._short_slope_cal.get_avg(ts=ts)
        return None
