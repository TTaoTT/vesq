from xex_mm.utils.base import Trade, BBO
from xex_mm.utils.calc_node import CalRealTime
from xex_mm.utils.enums import Direction, Side


class TradeStabilityState:
    reprice = "reprice"
    stable = "stable"


class CalTradeStability(CalRealTime):
    """
    Any trade outside of at-touch range triggers unstable state.
    Re-enter stable state when no trades outside of at-touch range within buffer window.
    """
    def __init__(self, buffer_window: int):
        super(CalTradeStability, self).__init__()
        self.buffer_window = buffer_window
        self._best_ask = ...
        self._best_bid = ...
        self.last_unstable_trade_ts = ...
        self._state: str = TradeStabilityState.reprice
        self._numeric_tol = 1e-10

    def on_wakeup(self, ts: int) -> bool:
        if self._state == TradeStabilityState.stable:
            return True
        if self._state == TradeStabilityState.reprice:
            if self.last_unstable_trade_ts is Ellipsis:
                self._state = TradeStabilityState.stable
                return True
            trade_time_gap = ts - self.last_unstable_trade_ts
            if trade_time_gap >= self.buffer_window:
                self._state = TradeStabilityState.stable
                return True
            return True
        raise NotImplementedError()

    def add_bbo(self, bbo: BBO) -> bool:
        """ Identify at touch """
        if bbo.valid_bbo:
            self._best_bid = bbo.best_bid_prc
            self._best_ask = bbo.best_ask_prc
        return True

    def add_trade(self, trade: Trade) -> bool:
        """ Identify trade in or outside of buffer window. """
        if trade.side == Side.long:
            if self._best_ask is Ellipsis:
                self._best_ask = trade.price
                return True
            if trade.price > self._best_ask + self._numeric_tol:
                self._state = TradeStabilityState.reprice
                self.last_unstable_trade_ts = trade.local_time
                return True
            if trade.price < self._best_ask - self._numeric_tol:
                self._state = TradeStabilityState.reprice
                self.last_unstable_trade_ts = trade.local_time
                return True
        elif trade.side == Side.short:
            if self._best_bid is Ellipsis:
                self._best_bid = trade.price
                return True
            if trade.price < self._best_bid - self._numeric_tol:
                self._state = TradeStabilityState.reprice
                self.last_unstable_trade_ts = trade.local_time
                return True
            if trade.price > self._best_bid + self._numeric_tol:
                self._state = TradeStabilityState.reprice
                self.last_unstable_trade_ts = trade.local_time
                return True
        if self._state == TradeStabilityState.reprice:
            if self.last_unstable_trade_ts is Ellipsis:
                self._state = TradeStabilityState.stable
                return True
            trade_time_gap = trade.local_time - self.last_unstable_trade_ts
            if trade_time_gap >= self.buffer_window:
                self._state = TradeStabilityState.stable
                return True
        return True

    def get_factor(self, ts: int) -> str:
        self.on_wakeup(ts=ts)
        return self._state