from xex_mm.calculators.em import EMAvgDecay
from xex_mm.calculators.impulse_decay import ImpulseDecay
from xex_mm.utils.base import Trade
from xex_mm.utils.calc_node import CalRealTime
from xex_mm.utils.enums import Direction


class CalEMAvgArrivalSize(CalRealTime):
    def __init__(self, halflife: float) -> None:
        super(CalEMAvgArrivalSize, self).__init__()
        self._halflife = halflife  # num events
        self._l_ar_calc = EMAvgDecay(halflife=self._halflife)
        self._s_ar_calc = EMAvgDecay(halflife=self._halflife)
        self._last_trd_idx = 0
        self._last_trd_ts = -1

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife})"

    def add_trade(self, trade: Trade):
        curr_ts = max(self._last_trd_ts, trade.local_time)
        if curr_ts <= self._last_trd_ts + 1:  # 1 ms buffer
            trd_idx = self._last_trd_idx
        else:
            trd_idx = self._last_trd_idx + 1
        if trade.side == Direction.long:
            self._l_ar_calc.on_val(val=trade.quantity, ts=trd_idx)
            self._last_trd_idx = trd_idx
            self._last_trd_ts = curr_ts
        elif trade.side == Direction.short:
            self._s_ar_calc.on_val(val=trade.quantity, ts=trd_idx)
            self._last_trd_idx = trd_idx
            self._last_trd_ts = curr_ts
        else:
            raise NotImplementedError(f"Unsupported trade side = {trade.side}")

    def get_factor(self, ts: int):
        lar = None
        if self._l_ar_calc.is_initialized:
            lar = self._l_ar_calc.get_avg(ts=self._last_trd_idx)
        sar = None
        if self._s_ar_calc.is_initialized:
            sar = self._s_ar_calc.get_avg(ts=self._last_trd_idx)
        return lar, sar


class CalImpulseArrivalSize(CalRealTime):
    def __init__(self, halflife: float) -> None:
        super(CalImpulseArrivalSize, self).__init__()
        self._halflife = halflife  # num events
        self._las_calc = ImpulseDecay(halflife=self._halflife)
        self._sas_calc = ImpulseDecay(halflife=self._halflife)
        self._last_trd_idx = 0
        self._last_trd_ts = -1

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife})"

    def add_trade(self, trade: Trade):
        curr_ts = max(self._last_trd_ts, trade.local_time)
        if curr_ts <= self._last_trd_ts + 1:  # 1 ms buffer
            trd_idx = self._last_trd_idx
        else:
            trd_idx = self._last_trd_idx + 1
        if trade.side == Direction.long:
            self._las_calc.on_impulse(imp=trade.quantity, ts=trd_idx)
            self._last_trd_idx = trd_idx
            self._last_trd_ts = curr_ts
        elif trade.side == Direction.short:
            self._sas_calc.on_impulse(imp=trade.quantity, ts=trd_idx)
            self._last_trd_idx = trd_idx
            self._last_trd_ts = curr_ts
        else:
            raise NotImplementedError(f"Unsupported trade side = {trade.side}")

    def get_factor(self, ts: int):
        lar = None
        if self._las_calc.is_initialized:
            lar = self._las_calc.get_impulse(ts=self._last_trd_idx)
        sar = None
        if self._sas_calc.is_initialized:
            sar = self._sas_calc.get_impulse(ts=self._last_trd_idx)
        return lar, sar
