import math
from typing import Union

from xex_mm.calculators.em import EMAvgDecay, EMAvgNoDecay
from xex_mm.calculators.sigma import CalEMAvgSigma
from xex_mm.utils.base import ReferencePriceCalculator, Depth, BBO, Trade
from xex_mm.utils.calc_node import CalRealTime


class CalEMAvgRefPrc(CalRealTime):
    def __init__(self, halflife: float):
        super(CalEMAvgRefPrc, self).__init__()
        self._halflife = halflife
        self._ref_prc_cal = ReferencePriceCalculator()
        self._ema_ref_prc_calc = EMAvgNoDecay(halflife=self._halflife)
        self._numeric_tol = 1e-10
        self.await_reset = True

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def _update_ref_prc(self, ref_prc: float, ts: int):
        if self.await_reset:
            self._ema_ref_prc_calc.init(avg=ref_prc, ts=ts)
            self.await_reset = False
            return
        if not self._ema_ref_prc_calc.is_initialized:
            self._ema_ref_prc_calc.init(avg=ref_prc, ts=ts)
            return
        self._ema_ref_prc_calc.on_val(val=ref_prc, ts=ts)

    def add_depth(self, depth: Depth):
        ref_prc = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        self._update_ref_prc(ref_prc=ref_prc, ts=depth.resp_ts)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        self._update_ref_prc(ref_prc=trade.price, ts=trade.local_time)

    def get_factor(self, ts: int):
        if self._ema_ref_prc_calc.is_initialized:
            return self._ema_ref_prc_calc.get_avg(ts=ts)
        return None


class CalEMAvgRelRefPrcSprd(CalRealTime):
    def __init__(self, iid1: str, iid2: str, halflife: float):
        super(CalEMAvgRelRefPrcSprd, self).__init__()
        self.iid1 = iid1
        self.iid2 = iid2
        self._ref_prc_1 = ...
        self._ref_prc_2 = ...
        self._halflife = halflife
        self._ref_prc_cal = ReferencePriceCalculator()
        self._ema_ref_prc_sprd_calc = EMAvgNoDecay(halflife=self._halflife)
        self._numeric_tol = 1e-10
        self.await_reset = True

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def _update_rel_ref_prc_sprd(self, rel_ref_prc_sprd: float, ts: int):
        if self.await_reset:
            self._ema_ref_prc_sprd_calc.init(avg=rel_ref_prc_sprd, ts=ts)
            self.await_reset = False
            return
        if not self._ema_ref_prc_sprd_calc.is_initialized:
            self._ema_ref_prc_sprd_calc.init(avg=rel_ref_prc_sprd, ts=ts)
            return
        self._ema_ref_prc_sprd_calc.on_val(val=rel_ref_prc_sprd, ts=ts)

    def _get_rel_ref_prc_sprd(self):
        if self._ref_prc_1 is Ellipsis:
            return
        if self._ref_prc_2 is Ellipsis:
            return
        return math.log(self._ref_prc_2 / self._ref_prc_1)

    def add_depth(self, depth: Depth):
        iid = f"{depth.meta_data.ex}.{depth.meta_data.contract}"

        if iid == self.iid1:
            self._ref_prc_1 = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        elif iid == self.iid2:
            self._ref_prc_2 = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        else:
            return

        rel_ref_prc_sprd = self._get_rel_ref_prc_sprd()
        if rel_ref_prc_sprd is None:
            return

        self._update_rel_ref_prc_sprd(rel_ref_prc_sprd=rel_ref_prc_sprd, ts=depth.resp_ts)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        iid = f"{trade.ex}.{trade.contract}"
        if iid == self.iid1:
            self._ref_prc_1 = trade.price
        elif iid == self.iid2:
            self._ref_prc_2 = trade.price
        else:
            return
        rel_ref_prc_sprd = self._get_rel_ref_prc_sprd()
        if rel_ref_prc_sprd is None:
            return

        self._update_rel_ref_prc_sprd(rel_ref_prc_sprd=rel_ref_prc_sprd, ts=trade.local_time)

    def get_factor(self, ts: int):
        if self._ema_ref_prc_sprd_calc.is_initialized:
            return self._ema_ref_prc_sprd_calc.get_avg(ts=ts)
        return None


class CalEMAvgRelRefPrcSprd2:
    def __init__(self, iid: str, halflife: float):
        self.iid = iid
        self._ref_prc = ...
        self._xex_ref_prc = ...
        self._halflife = halflife
        self._ref_prc_cal = ReferencePriceCalculator()
        self._ema_ref_prc_sprd_calc = EMAvgNoDecay(halflife=self._halflife)
        self._numeric_tol = 1e-10
        self.await_reset = True

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def update_rel_ref_prc_sprd(self, ts: int) -> bool:
        rel_ref_prc_sprd = self._get_rel_ref_prc_sprd()
        if rel_ref_prc_sprd is None:
            return False
        if self.await_reset:
            self._ema_ref_prc_sprd_calc.init(avg=rel_ref_prc_sprd, ts=ts)
            self.await_reset = False
            return True
        if not self._ema_ref_prc_sprd_calc.is_initialized:
            self._ema_ref_prc_sprd_calc.init(avg=rel_ref_prc_sprd, ts=ts)
            return False
        self._ema_ref_prc_sprd_calc.on_val(val=rel_ref_prc_sprd, ts=ts)
        return True

    def _get_rel_ref_prc_sprd(self) -> Union[float, None]:
        if self._ref_prc is Ellipsis:
            return
        if self._xex_ref_prc is Ellipsis:
            return
        return math.log(self._ref_prc / self._xex_ref_prc)

    def update_xex_ref_prc(self, xex_ref_prc: float) -> float:
        self._xex_ref_prc = xex_ref_prc
        return True

    def update_ref_prc(self, ref_prc: float) -> float:
        self._ref_prc = ref_prc
        return True

    def get_ema_ref_prc_sprd(self, ts: int):
        if self._ema_ref_prc_sprd_calc.is_initialized:
            return self._ema_ref_prc_sprd_calc.get_avg(ts=ts)
        return None