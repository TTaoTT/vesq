import math

from xex_mm.calculators.em import EMAvgDecay
from xex_mm.calculators.impulse_decay import ImpulseDecay
from xex_mm.utils.base import Depth, BBO, ReferencePriceCalculator, Trade
from xex_mm.utils.calc_node import CalRealTime


class CalEMAvgRet(CalRealTime):
    def __init__(self, halflife):
        super(CalEMAvgRet, self).__init__()
        self._halflife = halflife
        self._ref_prc_cal = ReferencePriceCalculator()
        self._ema_ret_calc = EMAvgDecay(halflife=self._halflife)
        self._prev_ref_prc = ...
        self._last_update_time = ...

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def on_wakeup(self, ts: int):
        self._ema_ret_calc.on_wakeup(ts=ts)

    def has_pre_ref_prc(self):
        return self._prev_ref_prc is not Ellipsis

    def has_last_update_time(self):
        return self._last_update_time is not Ellipsis

    def _update_ret(self, ref_prc: float, ts: int):
        if self.has_pre_ref_prc():
            ret = math.log(ref_prc / self._prev_ref_prc)
            self._ema_ret_calc.on_val(val=ret, ts=ts)
        self._prev_ref_prc = ref_prc
        self._last_update_time = ts

    def add_depth(self, depth: Depth):
        ref_prc = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        ts = depth.resp_ts
        self._update_ret(ref_prc=ref_prc, ts=ts)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        ref_prc = trade.price
        ts = trade.local_time
        self._update_ret(ref_prc=ref_prc, ts=ts)

    def get_factor(self, ts: int):
        if self._ema_ret_calc.is_initialized:
            return self._ema_ret_calc.get_avg(ts=ts)
        return None


class CalImpulseRet(CalRealTime):
    def __init__(self, halflife):
        super(CalImpulseRet, self).__init__()
        self._halflife = halflife
        self._ref_prc_cal = ReferencePriceCalculator()
        self._impulse_ret_calc = ImpulseDecay(halflife=self._halflife)
        self._prev_ref_prc = ...
        self._last_update_time = ...

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def on_wakeup(self, ts: int):
        self._impulse_ret_calc.on_wakeup(ts=ts)

    def has_pre_ref_prc(self):
        return self._prev_ref_prc is not Ellipsis

    def has_last_update_time(self):
        return self._last_update_time is not Ellipsis

    def _update_ret(self, ref_prc: float, ts: int):
        if self.has_pre_ref_prc():
            ret = math.log(ref_prc / self._prev_ref_prc)
            self._impulse_ret_calc.on_impulse(imp=ret, ts=ts)
        self._prev_ref_prc = ref_prc
        self._last_update_time = ts

    def add_depth(self, depth: Depth):
        ref_prc = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        ts = depth.resp_ts
        self._update_ret(ref_prc=ref_prc, ts=ts)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        ref_prc = trade.price
        ts = trade.local_time
        self._update_ret(ref_prc=ref_prc, ts=ts)

    def get_factor(self, ts: int):
        if self._impulse_ret_calc.is_initialized:
            return self._impulse_ret_calc.get_impulse(ts=ts)
        return None