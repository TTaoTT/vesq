from xex_mm.calculators.em import EMAvgNoDecay
from xex_mm.utils.base import Depth, BBO, Trade
from xex_mm.utils.calc_node import CalRealTime


class CalPastHalfSpread(CalRealTime):
    def __init__(self, t: int) -> None:
        self.__spread__arr = []
        self.__spread_sum = 0
        self.__ts_arr = []
        self.__arr_len = 0
        self._t = t

    def __repr__(self):
        return f"{self.__class__.__name__}({self._t})"

    def add_depth(self, depth: Depth):
        spread = (depth.asks[0].prc - depth.bids[0].prc) / 2
        self.__spread__arr.append(spread)
        self.__spread_sum += spread
        self.__ts_arr.append(depth.resp_ts)
        self.__arr_len += 1

        while self.__ts_arr and self.__ts_arr[0] < depth.resp_ts - self._t:
            s = self.__spread__arr.pop(0)
            self.__ts_arr.pop(0)
            self.__spread_sum -= s
            self.__arr_len -= 1

    def get_factor(self, ts: int):
        return self.__spread_sum / self.__arr_len if self.__arr_len else None


class CalInstantHalfSpread(CalRealTime):
    def __init__(self):
        super(CalInstantHalfSpread, self).__init__()
        self._sprd = ...

    def __repr__(self):
        return f"{self.__class__.__name__}()"

    def add_depth(self, depth: Depth):
        self._sprd = (depth.asks[0].prc - depth.bids[0].prc) / 2

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            self._sprd = (bbo.best_ask_prc - bbo.best_bid_prc) / 2

    def has_sprd(self):
        return self._sprd is not Ellipsis

    def get_factor(self, ts: int):
        if self.has_sprd():
            return self._sprd
        return None


class CalEMAvgHalfSpread(CalRealTime):
    def __init__(self, halflife):
        super(CalEMAvgHalfSpread, self).__init__()
        self._halflife = halflife
        self._halfsprd_cal = EMAvgNoDecay(halflife=self._halflife)

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{4})"

    def add_depth(self, depth: Depth):
        sprd = (depth.asks[0].prc - depth.bids[0].prc) / 2
        if not self._halfsprd_cal.is_initialized:
            self._halfsprd_cal.init(avg=sprd, ts=depth.resp_ts)
            return
        self._halfsprd_cal.on_val(val=sprd, ts=depth.resp_ts)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            sprd = (bbo.best_ask_prc - bbo.best_bid_prc) / 2
            if not self._halfsprd_cal.is_initialized:
                self._halfsprd_cal.init(avg=sprd, ts=bbo.local_time)
                return
            self._halfsprd_cal.on_val(val=sprd, ts=bbo.local_time)

    def get_factor(self, ts: int):
        return self._halfsprd_cal.get_avg(ts=ts)


class CalImpulseHalfSpread(CalRealTime):
    def __init__(self, halflife):
        super(CalImpulseHalfSpread, self).__init__()
        self._halflife = halflife
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval
        self._sprd = ...
        self._last_update_time = ...

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{3})"

    def has_sprd(self):
        return self._sprd is not Ellipsis

    def has_last_update_time(self):
        return self._last_update_time is not Ellipsis

    def on_wakeup(self, ts: int):
        if self.has_sprd():
            if self.has_last_update_time():
                dt = ts - self._last_update_time
                self._sprd *= (self._lambda ** dt)
        self._last_update_time = ts

    def add_depth(self, depth: Depth):
        sprd = (depth.asks[0].prc - depth.bids[0].prc) / 2
        if self.has_sprd():
            self.on_wakeup(ts=depth.resp_ts)
            self._sprd = max(self._sprd, sprd)
        else:
            self._sprd = sprd
        self._last_update_time = depth.resp_ts

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            sprd = (bbo.best_ask_prc - bbo.best_bid_prc) / 2
            if self.has_sprd():
                self.on_wakeup(ts=bbo.local_time)
                self._sprd = max(self._sprd, sprd)
            else:
                self._sprd = sprd
            self._last_update_time = bbo.local_time

    def get_factor(self, ts: int):
        if self.has_sprd():
            self.on_wakeup(ts=ts)
            return self._sprd
        return None


class CalSpreadMultiplier(CalRealTime):
    def __init__(self, halflife: int):
        self._count = 0
        self._halflife = halflife
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval
        self._last_trade_side = ...
        self._last_update_ts = ...

    def on_wakeup(self, ts: int):
        if self._last_update_ts is Ellipsis:
            self._last_update_ts = ts
            return
        dt = ts - self._last_update_ts
        self._count = max(self._count * (self._lambda ** dt), 1)
        self._last_update_ts = ts

    def add_trade(self, trade: Trade):
        self.on_wakeup(ts=trade.local_time)
        if self._last_trade_side is Ellipsis:
            self._last_trade_side = trade.side
            self._count += 1
            return
        if trade.side == self._last_trade_side:
            self._count += 1
            return
        self._last_trade_side = trade.side
        self._count = 1

    def get_factor(self, ts: int):
        self.on_wakeup(ts=ts)
        return self._last_trade_side, 1
        return self._last_trade_side, max(self._count, 1) ** 2
