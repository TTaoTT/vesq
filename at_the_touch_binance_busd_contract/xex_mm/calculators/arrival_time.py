from xex_mm.calculators.em import EMAvgDecay
from xex_mm.calculators.impulse_decay import ImpulseDecay
from xex_mm.utils.base import Trade
from xex_mm.utils.calc_node import CalRealTime
from xex_mm.utils.enums import Direction


class CalEMAvgArrivalTime(CalRealTime):
    def __init__(self, halflife: float) -> None:
        super(CalEMAvgArrivalTime, self).__init__()
        self._halflife = halflife  # num events
        self._at_calcs = {
            Direction.long: EMAvgDecay(halflife=self._halflife),
            Direction.short: EMAvgDecay(halflife=self._halflife),
        }
        self._last_trd_time = {
            Direction.long: -1,
            Direction.short: -1,
        }
        self._last_trd_idx = 0

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife})"

    def add_trade(self, trade: Trade):
        if self._last_trd_time[trade.side] == -1:
            self._last_trd_time[trade.side] = trade.local_time
            return
        ts = trade.local_time
        dt = ts - self._last_trd_time[trade.side]
        dt = max(dt, 0)
        trd_idx = self._last_trd_idx + 1
        self._at_calcs[trade.side].on_val(val=dt, ts=trd_idx)
        self._last_trd_time[trade.side] = ts
        self._last_trd_idx = trd_idx

    def get_factor(self, ts: int):
        lat = self._at_calcs[Direction.long].get_avg(ts=self._last_trd_idx)
        if lat is None:
            return
        sat = self._at_calcs[Direction.short].get_avg(ts=self._last_trd_idx)
        if sat is None:
            return
        lat = max(1, lat)
        sat = max(1, sat)
        return lat, sat


class CalImpulseArrivalTime(CalRealTime):
    def __init__(self, halflife: float) -> None:
        super(CalImpulseArrivalTime, self).__init__()
        self._halflife = halflife  # num events
        self._at_calcs = {
            Direction.long: ImpulseDecay(halflife=self._halflife),
            Direction.short: ImpulseDecay(halflife=self._halflife),
        }
        self._last_trd_time = {
            Direction.long: -1,
            Direction.short: -1,
        }
        self._last_trd_idx = 0

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife})"

    def add_trade(self, trade: Trade):
        if self._last_trd_time[trade.side] == -1:
            self._last_trd_time[trade.side] = trade.local_time
            return
        ts = trade.local_time
        dt = ts - self._last_trd_time[trade.side]
        trd_idx = self._last_trd_idx + 1
        self._at_calcs[trade.side].on_impulse(imp=dt, ts=trd_idx)
        self._last_trd_time[trade.side] = ts
        self._last_trd_idx = trd_idx

    def get_factor(self, ts: int):
        lat = self._at_calcs[Direction.long].get_impulse(ts=self._last_trd_idx)
        sat = self._at_calcs[Direction.short].get_impulse(ts=self._last_trd_idx)
        return lat, sat
