import math

import numpy as np

from xex_mm.calculators.em import EMAvgDecay
from xex_mm.utils.base import ReferencePriceCalculator, Depth, BBO, Trade
from xex_mm.utils.calc_node import CalRealTime


class CalPastSigma(CalRealTime):
    def __init__(self, t: int, t_f: int, amt_bnd: float) -> None:
        assert t >= t_f * 3
        self.__t_f = t_f
        self.__ret_arr = []
        self.__mp_arr = []
        self.__mp_ts_arr = []
        self.__ret_ts_arr = []
        self.__ret_square_sum = 0
        self.__ret_len = 0
        self.__t = t
        self.__amt_bnd = amt_bnd
        self.ref_prc_cal = ReferencePriceCalculator()
        self.last_ref_prc = None
        self.last_t = None

    def add_depth(self, depth: Depth):
        if not self.last_ref_prc or self.last_ref_prc <= 0:
            self.last_ref_prc = self.ref_prc_cal.calc_from_depth(depth, self.__amt_bnd)
            return

        curr_ref_prc = self.ref_prc_cal.calc_from_depth(depth, self.__amt_bnd)
        if self.__mp_ts_arr and depth.resp_ts - self.__mp_ts_arr[0] >= self.__t_f:
            ret = np.log(curr_ref_prc/self.__mp_arr[0])
            self.__ret_arr.append(ret)
            self.__ret_ts_arr.append(depth.resp_ts)
            self.__ret_len += 1
            self.__ret_square_sum += ret ** 2
            while self.__ret_ts_arr and self.__ret_ts_arr[0] < depth.resp_ts - self.__t:
                self.__ret_ts_arr.pop(0)
                ret = self.__ret_arr.pop(0)
                self.__ret_len -= 1
                self.__ret_square_sum -= ret ** 2
        while self.__mp_ts_arr and depth.resp_ts - self.__mp_ts_arr[0] >= self.__t_f:
            self.__mp_ts_arr.pop(0)
            self.__mp_arr.pop(0)

        self.__mp_arr.append(curr_ref_prc)
        self.__mp_ts_arr.append(depth.resp_ts)

    def get_factor(self, ts: int):
        if self.__ret_len <= 0 or self.__ret_square_sum<0:
            return None
        return np.sqrt(max(self.__ret_square_sum, 0) / self.__ret_len /self.__t_f)


class CalPastInstantSigma(CalRealTime):
    def __init__(self):
        self._ref_prc_cal = ReferencePriceCalculator()
        self._curr_ref_prc = ...
        self._curr_ref_prc_ts = ...
        self._prev_ref_prc = ...
        self._prev_ref_prc_ts = ...
        self._sigma = ...

    def __repr__(self):
        return f"{self.__class__.__name__}(v{1})"

    def has_prev_ref_prc(self) -> bool:
        return self._prev_ref_prc is not Ellipsis

    def has_curr_ref_prc(self) -> bool:
        return self._curr_ref_prc is not Ellipsis

    def has_sigma(self) -> bool:
        return self._sigma is not Ellipsis

    def _update_ref_prc(self, ref_prc: float, ts: int):
        if not self.has_curr_ref_prc():
            self._curr_ref_prc = ref_prc
            self._curr_ref_prc_ts = ts
        elif not self.has_prev_ref_prc():
            if ts <= self._curr_ref_prc_ts:
                self._curr_ref_prc = ref_prc
                self._curr_ref_prc_ts = max(ts, self._curr_ref_prc_ts)
            else:
                self._prev_ref_prc = self._curr_ref_prc
                self._prev_ref_prc_ts = self._curr_ref_prc_ts
                self._curr_ref_prc = ref_prc
                self._curr_ref_prc_ts = ts
                ret = math.log(self._curr_ref_prc / self._prev_ref_prc)
                dt = self._curr_ref_prc_ts - self._prev_ref_prc_ts
                var = ret ** 2
                self._sigma = math.sqrt(var / dt)
        else:
            if ts <= self._curr_ref_prc_ts:
                self._curr_ref_prc = ref_prc
                self._curr_ref_prc_ts = max(ts, self._curr_ref_prc_ts)
                ret = math.log(self._curr_ref_prc / self._prev_ref_prc)
                dt = self._curr_ref_prc_ts - self._prev_ref_prc_ts
                var = ret ** 2
                self._sigma = math.sqrt(var / dt)
            else:
                self._prev_ref_prc = self._curr_ref_prc
                self._prev_ref_prc_ts = self._curr_ref_prc_ts
                self._curr_ref_prc = ref_prc
                self._curr_ref_prc_ts = ts
                ret = math.log(self._curr_ref_prc / self._prev_ref_prc)
                dt = self._curr_ref_prc_ts - self._prev_ref_prc_ts
                var = ret ** 2
                self._sigma = math.sqrt(var / dt)

    def add_depth(self, depth: Depth):
        ref_prc = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        ts = depth.resp_ts
        self._update_ref_prc(ref_prc=ref_prc, ts=ts)

    def add_bbo(self, bbo: BBO):
        self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        ref_prc = trade.price
        ts = trade.local_time
        self._update_ref_prc(ref_prc=ref_prc, ts=ts)

    def get_factor(self, ts: int):
        if self.has_sigma():
            return self._sigma
        return None


class CalImpulseSigma(CalRealTime):
    def __init__(self, halflife: float):
        self._instant_sigma_cal = CalPastInstantSigma()
        self._halflife = halflife
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval
        self._sigma = ...
        self._last_update_time = ...

    def __repr__(self):
        return f"{self.__class__.__name__}({self._instant_sigma_cal},{self._halflife},v{1})"

    def has_sigma(self) -> bool:
        return self._sigma is not Ellipsis

    def has_last_update_time(self) -> bool:
        return self._last_update_time is not Ellipsis

    def on_wakeup(self, ts: int):
        if self.has_sigma():
            if self.has_last_update_time():
                dt = ts - self._last_update_time
                self._sigma *= (self._lambda ** dt)
        self._last_update_time = ts

    def _update_sigma(self, ts: int):
        if self._instant_sigma_cal.has_sigma():
            sigma = self._instant_sigma_cal.get_factor(ts=ts)
            if self.has_sigma():
                self.on_wakeup(ts=ts)
                self._sigma = max(self._sigma, sigma)
            else:
                self._sigma = sigma
        self._last_update_time = ts

    def add_depth(self, depth: Depth):
        self._instant_sigma_cal.add_depth(depth=depth)
        self._update_sigma(ts=depth.resp_ts)

    def add_bbo(self, bbo: BBO):
        self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        self._instant_sigma_cal.add_trade(trade=trade)
        self._update_sigma(ts=trade.local_time)

    def get_factor(self, ts: int):
        if self.has_sigma():
            self.on_wakeup(ts=ts)
            return self._sigma
        return None


class CalEMAvgSigma(CalRealTime):
    def __init__(self, halflife):
        super(CalEMAvgSigma, self).__init__()
        self._halflife = halflife
        self._ref_prc_cal = ReferencePriceCalculator()
        self._ema_var_calc = EMAvgDecay(halflife=self._halflife)
        self._prev_ref_prc = ...
        self._last_update_time = ...

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v{1})"

    def on_wakeup(self, ts: int):
        self._ema_var_calc.on_wakeup(ts=ts)

    def has_pre_ref_prc(self):
        return self._prev_ref_prc is not Ellipsis

    def has_last_update_time(self):
        return self._last_update_time is not Ellipsis

    def _update_sigma(self, ref_prc: float, ts: int):
        if self.has_pre_ref_prc():
            ret = math.log(ref_prc / self._prev_ref_prc)
            self._ema_var_calc.on_val(val=abs(ret), ts=ts)
        self._prev_ref_prc = ref_prc
        self._last_update_time = ts

    def add_depth(self, depth: Depth):
        ref_prc = self._ref_prc_cal.calc_from_depth(depth=depth, amt_bnd=0)
        self._update_sigma(ref_prc=ref_prc, ts=depth.resp_ts)

    def add_bbo(self, bbo: BBO):
        if bbo.valid_bbo:
            self.add_depth(depth=bbo.to_depth())

    def add_trade(self, trade: Trade):
        self._update_sigma(ref_prc=trade.price, ts=trade.local_time)

    def get_factor(self, ts: int):
        if self._ema_var_calc.is_initialized:
            return self._ema_var_calc.get_avg(ts=ts)
        return None
