import math

from xex_mm.calculators.em import EMEvent
from xex_mm.utils.base import Trade
from xex_mm.utils.calc_node import CalRealTime
from xex_mm.utils.enums import Side


class TradeReturnLambdaCal(CalRealTime):
    def __init__(self, halflife: float):
        self._long_emavg_trade_ret_cal = EMEvent(halflife=halflife)
        self._short_emavg_trade_ret_cal = EMEvent(halflife=halflife)
        self._ref_prc = ...
        self._long_idx = 0
        self._short_idx = 0
        self._numeric_tol = 1e-10

    def update_ref_prc(self, ref_prc: float) -> bool:
        self._ref_prc = ref_prc
        return True

    def add_trade(self, trade: Trade):
        if self._ref_prc is Ellipsis:
            return
        prc = trade.price
        if prc >= self._ref_prc:
            r = math.log(prc / self._ref_prc)
            if not self._long_emavg_trade_ret_cal.is_initialized:
                self._long_emavg_trade_ret_cal.init(avg=r)
                return
            self._long_emavg_trade_ret_cal.on_val(val=r, event_idx=self._long_idx)
            self._long_idx += 1
        elif prc < self._ref_prc:
            r = math.log(self._ref_prc / prc)
            if not self._short_emavg_trade_ret_cal.is_initialized:
                self._short_emavg_trade_ret_cal.init(avg=r)
                return
            self._short_emavg_trade_ret_cal.on_val(val=r, event_idx=self._short_idx)
            self._short_idx += 1

    def get_factor(self, ts: int):
        long_mean_r = self._long_emavg_trade_ret_cal.get_avg()
        if long_mean_r is None:
            return
        short_mean_r = self._short_emavg_trade_ret_cal.get_avg()
        if short_mean_r is None:
            return
        mean_r_floor = 1e-5
        long_mean_r = max(long_mean_r, mean_r_floor)
        short_mean_r = max(short_mean_r, mean_r_floor)
        long_trl = 1 / max(long_mean_r, self._numeric_tol)
        short_trl = 1 / max(short_mean_r, self._numeric_tol)
        return long_trl, short_trl