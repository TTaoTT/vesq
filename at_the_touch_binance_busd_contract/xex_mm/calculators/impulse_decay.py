class ImpulseDecay:
    def __init__(self, halflife: float):
        self._imp = ...
        self._halflife = halflife  # ms
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval
        self._last_imp = ...
        self._last_update_time = -1
        self._numeric_tol = 1e-10
        self.is_initialized: bool = False

    def init(self, imp: float, ts: int):
        self._imp = imp
        self._last_imp = imp
        self._last_update_time = ts
        self.is_initialized = True

    def on_wakeup(self, ts: int):
        if not self.is_initialized:
            return
        dt = ts - self._last_update_time
        lambda_dt = self._lambda ** dt
        self._imp *= lambda_dt
        if abs(self._imp) < self._numeric_tol:
            self._imp = 0
        self._last_update_time = ts

    def on_impulse(self, imp: float, ts: int):
        if not self.is_initialized:
            self.init(imp=imp, ts=ts)
            return
        if ts <= self._last_update_time:
            imp = self._last_imp + imp
            self._imp = max(self._imp, imp)
            self._last_imp = imp
            return
        self.on_wakeup(ts=ts)
        self._imp = max(self._imp, imp)
        self._last_imp = imp
        self._last_update_time = ts

    def get_impulse(self, ts: int):
        if not self.is_initialized:
            return None
        self.on_wakeup(ts=ts)
        return self._imp
