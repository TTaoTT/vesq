from xex_mm.calculators.em import EMEvent
from xex_mm.utils.base import Trade, Depth, BBO
from xex_mm.utils.calc_node import CalRealTime


class CalUpdateFrequency(CalRealTime):
    def __init__(self, halflife):
        self._last_trade_update_ts = None
        self._last_depth_update_ts = None
        self._last_bbo_update_ts = None
        self._emavg_trade_update_freq = EMEvent(halflife=halflife)
        self._emavg_depth_update_freq = EMEvent(halflife=halflife)
        self._emavg_bbo_update_freq = EMEvent(halflife=halflife)

    def add_trade(self, trade: Trade):
        if self._last_trade_update_ts is None:
            self._last_trade_update_ts = trade.local_time
            return
        dt = trade.local_time - self._last_trade_update_ts
        self._emavg_trade_update_freq.on_val(val=dt)
        self._last_trade_update_ts = trade.local_time

    def add_depth(self, depth: Depth):
        if self._last_depth_update_ts is None:
            self._last_depth_update_ts = depth.resp_ts
            return
        dt = depth.resp_ts - self._last_depth_update_ts
        self._emavg_depth_update_freq.on_val(val=dt)
        self._last_depth_update_ts = depth.resp_ts

    def add_bbo(self, bbo: BBO):
        if self._last_bbo_update_ts is None:
            self._last_bbo_update_ts = bbo.local_time
            return
        dt = bbo.local_time - self._last_bbo_update_ts
        self._emavg_bbo_update_freq.on_val(val=dt)
        self._last_bbo_update_ts = bbo.local_time

    def get_factor(self, ts: int):
        depth_freq = self._emavg_depth_update_freq.get_avg()
        bbo_freq = self._emavg_bbo_update_freq.get_avg()
        trade_freq = self._emavg_trade_update_freq.get_avg()
        return depth_freq, bbo_freq, trade_freq