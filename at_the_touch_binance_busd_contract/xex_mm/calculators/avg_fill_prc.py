import math

from xex_mm.utils.enums import Direction


class AvgFillPrc:
    def __init__(self, d: Direction):
        self.d = d
        self._avg_fill_prc = math.nan