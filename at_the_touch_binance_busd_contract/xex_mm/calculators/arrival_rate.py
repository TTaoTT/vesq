import logging
from collections import deque

from typing import Tuple

from xex_mm.calculators.em import EMAvgDecay, EMSum
from xex_mm.calculators.impulse_decay import ImpulseDecay
from xex_mm.utils.base import Depth, BBO

from xex_mm.utils.base import Trade
from xex_mm.utils.calc_node import CalRealTime

class TradeQty:
    def __init__(self, quantity: float, local_time: int) -> None:
        self.quantity = quantity
        self.local_time = local_time


class RollingWindowArrivalRateCal(CalRealTime):
    def __init__(self, t: float) -> None:
        self._long_trd_q = deque()
        self._short_trd_q = deque()
        self._long_trd_reple_q = deque()
        self._short_trd_reple_q = deque()
        self._long_qty = 0
        self._short_qty = 0
        self._lar = 0
        self._sar = 0
        self._t = t
        self._last_depth = None
        self._fld_long_qty = 0
        self._fld_short_qty = 0
        self._lrr = 0
        self._srr = 0
        self._betwn_depth_long_trd_q = 0
        self._betwn_depth_short_trd_q = 0

    def __repr__(self):
        return f"{self.__class__.__name__}({self._t})"

    def on_wakeup(self, ts: int):
        while len(self._long_trd_q) > 0 and ts - self._long_trd_q[0].local_time >= self._t:
            rmv_trd: Trade = self._long_trd_q.popleft()
            self._long_qty -= rmv_trd.quantity
        self._lar = self._long_qty / self._t
        while len(self._short_trd_q) > 0 and ts - self._short_trd_q[0].local_time >= self._t:
            rmv_trd: Trade = self._short_trd_q.popleft()
            self._short_qty -= rmv_trd.quantity
        self._sar = self._short_qty / self._t
        while len(self._long_trd_reple_q) > 0  and ts - self._long_trd_reple_q[0].local_time >= self._t:
            rmv_qty: TradeQty = self._long_trd_reple_q.popleft()
            self._fld_long_qty -= rmv_qty.quantity
        self._lrr = self._fld_long_qty / self._t
        while  len(self._short_trd_reple_q) > 0 and ts - self._short_trd_reple_q[0].local_time >= self._t:
            rmv_qty: TradeQty = self._short_trd_reple_q.popleft()
            self._fld_short_qty -= rmv_qty.quantity
        self._srr = self._fld_short_qty / self._t

    def add_trade(self, trade: Trade):
        if trade.side == 1:
            self._long_trd_q.append(trade)
            self._long_qty += trade.quantity
            self._betwn_depth_long_trd_q += trade.quantity
        else:
            self._short_trd_q.append(trade)
            self._short_qty += trade.quantity
            self._betwn_depth_short_trd_q += trade.quantity
        trd_ts = trade.local_time
        self.on_wakeup(ts=trd_ts)

    def add_depth(self, depth: Depth):
        if not self._last_depth:
            self._last_depth = depth
            return

        self.on_wakeup(ts=depth.resp_ts)

        ask_price = depth.asks[0].prc
        ask_qty = depth.asks[0].qty
        bid_price = depth.bids[0].prc
        bid_qty = depth.bids[0].qty

        last_ask_price = self._last_depth.asks[0].prc
        last_ask_qty = self._last_depth.asks[0].qty
        last_bid_price = self._last_depth.bids[0].prc
        last_bid_qty = self._last_depth.bids[0].qty

        ask_replenishment = self._betwn_depth_long_trd_q
        bid_replenishment = self._betwn_depth_short_trd_q

        if ask_price > last_ask_price:
            for last_ask in self._last_depth.asks:
                if last_ask.prc < ask_price:
                    ask_replenishment -= last_ask.qty
                elif last_ask.prc == ask_price:
                    ask_replenishment += ask_qty - last_ask.qty
                else:
                    break
        elif ask_price <= last_ask_price:
            for ask in depth.asks:
                if ask.prc < last_ask_price:
                    ask_replenishment += ask.qty
                elif ask.prc == last_ask_price:
                    ask_replenishment += ask.qty - last_ask_qty
                else:
                    break

        if bid_price < last_bid_price:
            for last_bid in self._last_depth.bids:
                if last_bid.prc > bid_price:
                    bid_replenishment -= last_bid.qty
                elif last_bid.prc == bid_price:
                    bid_replenishment += bid_qty - last_bid.qty
                else:
                    break
        elif bid_price >= last_bid_price:
            for bid in self._last_depth.bids:
                if bid.prc > last_bid_price:
                    bid_replenishment += bid.qty
                elif bid.prc == last_bid_price:
                    bid_replenishment += bid.qty - last_bid_qty
                else:
                    break

        self._long_trd_reple_q.append(TradeQty(quantity=ask_replenishment, local_time=depth.resp_ts))
        self._short_trd_reple_q.append(TradeQty(quantity=bid_replenishment, local_time=depth.resp_ts))
        
        self._fld_long_qty += ask_replenishment
        self._fld_short_qty += bid_replenishment
        
        self._last_depth = depth
        self._betwn_depth_long_trd_q = 0
        self._betwn_depth_short_trd_q = 0
        
    def get_factor(self, ts: int) -> Tuple[float, float]:
        self.on_wakeup(ts=ts)
        return self._lar - self._lrr, self._sar - self._srr
    
    def record(self, ts: int):
        self.on_wakeup(ts=ts)
        return self._lar, self._lrr, self._sar, self._srr


class CalEMAvgArrivalRate(CalRealTime):
    def __init__(self, halflife: float) -> None:
        super(CalEMAvgArrivalRate, self).__init__()
        self._halflife = halflife
        self._l_ar_calc = EMAvgDecay(halflife=self._halflife)
        self._s_ar_calc = EMAvgDecay(halflife=self._halflife)
        self._l_rr_calc = EMAvgDecay(halflife=self._halflife)
        self._s_rr_calc = EMAvgDecay(halflife=self._halflife)
        self._last_depth = None
        self._betwn_depth_long_trd_qty = 0
        self._betwn_depth_short_trd_qty = 0

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife})"

    def add_trade(self, trade: Trade):
        if trade.side == 1:
            self._l_ar_calc.on_val(val=trade.quantity, ts=trade.local_time)
            self._betwn_depth_long_trd_qty += trade.quantity
        elif trade.side == -1:
            self._s_ar_calc.on_val(val=trade.quantity, ts=trade.local_time)
            self._betwn_depth_short_trd_qty += trade.quantity
        else:
            raise NotImplementedError(f"Unsupported trade side = {trade.side}")

    def add_depth(self, depth: Depth):
        if not self._last_depth:
            self._last_depth = depth
            return

        ask_price = depth.asks[0].prc
        ask_qty = depth.asks[0].qty
        bid_price = depth.bids[0].prc
        bid_qty = depth.bids[0].qty

        last_ask_price = self._last_depth.asks[0].prc
        last_ask_qty = self._last_depth.asks[0].qty
        last_bid_price = self._last_depth.bids[0].prc
        last_bid_qty = self._last_depth.bids[0].qty

        ask_replenishment = self._betwn_depth_long_trd_qty
        bid_replenishment = self._betwn_depth_short_trd_qty

        if ask_price > last_ask_price:
            for last_ask in self._last_depth.asks:
                if last_ask.prc < ask_price:
                    ask_replenishment -= last_ask.qty
                elif last_ask.prc == ask_price:
                    ask_replenishment += ask_qty - last_ask.qty
                else:
                    break
        elif ask_price <= last_ask_price:
            for ask in depth.asks:
                if ask.prc < last_ask_price:
                    ask_replenishment += ask.qty
                elif ask.prc == last_ask_price:
                    ask_replenishment += ask.qty - last_ask_qty
                else:
                    break

        if bid_price < last_bid_price:
            for last_bid in self._last_depth.bids:
                if last_bid.prc > bid_price:
                    bid_replenishment -= last_bid.qty
                elif last_bid.prc == bid_price:
                    bid_replenishment += bid_qty - last_bid.qty
                else:
                    break
        elif bid_price >= last_bid_price:
            for bid in self._last_depth.bids:
                if bid.prc > last_bid_price:
                    bid_replenishment += bid.qty
                elif bid.prc == last_bid_price:
                    bid_replenishment += bid.qty - last_bid_qty
                else:
                    break

        self._l_rr_calc.on_val(val=ask_replenishment, ts=depth.resp_ts)
        self._s_rr_calc.on_val(val=bid_replenishment, ts=depth.resp_ts)

        self._last_depth = depth
        self._betwn_depth_long_trd_qty = 0
        self._betwn_depth_short_trd_qty = 0

    def add_bbo(self, bbo: BBO):
        self.add_depth(depth=bbo.to_depth())

    def get_factor(self, ts: int):
        lar = 0
        if self._l_ar_calc.is_initialized:
            lar = self._l_ar_calc.get_avg(ts=ts)
        lrr = 0
        if self._l_rr_calc.is_initialized:
            lrr = self._l_rr_calc.get_avg(ts=ts)
        sar = 0
        if self._s_ar_calc.is_initialized:
            sar = self._s_ar_calc.get_avg(ts=ts)
        srr = 0
        if self._s_rr_calc.is_initialized:
            srr = self._s_rr_calc.get_avg(ts=ts)
        return lar, sar

    def record(self, ts: int):
        lar = 0
        if self._l_ar_calc.is_initialized:
            lar = self._l_ar_calc.get_avg(ts=ts)
        lrr = 0
        if self._l_rr_calc.is_initialized:
            lrr = self._l_rr_calc.get_avg(ts=ts)
        sar = 0
        if self._s_ar_calc.is_initialized:
            sar = self._s_ar_calc.get_avg(ts=ts)
        srr = 0
        if self._s_rr_calc.is_initialized:
            srr = self._s_rr_calc.get_avg(ts=ts)
        return lar, lrr, sar, srr


class TrdQtySumCal(CalRealTime):
    def __init__(self, halflife: int):
        super(TrdQtySumCal, self).__init__()
        self._l_decay_sum = EMSum(halflife=halflife)
        self._s_decay_sum = EMSum(halflife=halflife)

    def add_trade(self, trade: Trade):
        if trade.side == 1:
            self._l_decay_sum.on_val(val=trade.quantity, ts=trade.local_time)
        elif trade.side == -1:
            self._s_decay_sum.on_val(val=trade.quantity, ts=trade.local_time)
        else:
            raise NotImplementedError(f"Unsupported trade side = {trade.side}")

    def get_factor(self, ts: int):
        return self._l_decay_sum.get_sum(ts=ts), self._s_decay_sum.get_sum(ts=ts)


class CalImpulseArrivalRate(CalRealTime):
    def __init__(self, halflife: float) -> None:
        super(CalImpulseArrivalRate, self).__init__()
        self._halflife = halflife
        self._l_ar_calc = ImpulseDecay(halflife=self._halflife)
        self._s_ar_calc = ImpulseDecay(halflife=self._halflife)
        self._l_rr_calc = ImpulseDecay(halflife=self._halflife)
        self._s_rr_calc = ImpulseDecay(halflife=self._halflife)

        self._curr_betwn_depth_long_trd_qty = 0
        self._curr_betwn_depth_short_trd_qty = 0
        self._prev_betwn_depth_long_trd_qty = 0
        self._prev_betwn_depth_short_trd_qty = 0
        self._curr_depth: Depth = ...
        self._prev_depth: Depth = ...

        self._last_long_trd: Trade = ...
        self._last_short_trd: Trade = ...
        self._last_long_dt: int = ...
        self._last_short_dt: int = ...

    def __repr__(self):
        return f"{self.__class__.__name__}({self._halflife},v5)"

    def has_last_long_trd(self):
        return self._last_long_trd is not Ellipsis

    def has_last_short_trd(self):
        return self._last_short_trd is not Ellipsis

    def has_curr_depth(self):
        return self._curr_depth is not Ellipsis

    def has_prev_depth(self):
        return self._prev_depth is not Ellipsis

    def has_last_long_dt(self):
        return self._last_long_dt is not Ellipsis

    def has_last_short_dt(self):
        return self._last_short_dt is not Ellipsis

    def add_trade(self, trade: Trade):
        if trade.side == 1:
            if not self.has_last_long_trd():
                self._last_long_trd = trade
                return
            dt = trade.local_time - self._last_long_trd.local_time
            if dt <= 0:
                if self.has_last_long_dt():
                    instant_ar = trade.quantity / self._last_long_dt
                    self._l_ar_calc.on_impulse(imp=instant_ar, ts=trade.local_time)
            else:
                instant_ar = trade.quantity / dt
                self._l_ar_calc.on_impulse(imp=instant_ar, ts=trade.local_time)
                self._last_long_dt = dt
            self._last_long_trd = trade
            self._curr_betwn_depth_long_trd_qty += trade.quantity
        elif trade.side == -1:
            if not self.has_last_short_trd():
                self._last_short_trd = trade
                return
            dt = trade.local_time - self._last_short_trd.local_time
            if dt <= 0:
                if self.has_last_short_dt():
                    instant_ar = trade.quantity / self._last_short_dt
                    self._s_ar_calc.on_impulse(imp=instant_ar, ts=trade.local_time)
            else:
                instant_ar = trade.quantity / dt
                self._s_ar_calc.on_impulse(imp=instant_ar, ts=trade.local_time)
                self._last_short_dt = dt
            self._last_short_trd = trade
            self._curr_betwn_depth_short_trd_qty += trade.quantity
        else:
            raise NotImplementedError(f"Unsupported trade side = {trade.side}")

    def _update_rr(self):

        ask_price = self._curr_depth.asks[0].prc
        ask_qty = self._curr_depth.asks[0].qty
        bid_price = self._curr_depth.bids[0].prc
        bid_qty = self._curr_depth.bids[0].qty

        last_ask_price = self._prev_depth.asks[0].prc
        last_ask_qty = self._prev_depth.asks[0].qty
        last_bid_price = self._prev_depth.bids[0].prc
        last_bid_qty = self._prev_depth.bids[0].qty

        ask_replenishment = self._prev_betwn_depth_long_trd_qty
        bid_replenishment = self._prev_betwn_depth_short_trd_qty

        if ask_price > last_ask_price:
            for last_ask in self._prev_depth.asks:
                if last_ask.prc < ask_price:
                    ask_replenishment -= last_ask.qty
                elif last_ask.prc == ask_price:
                    ask_replenishment += ask_qty - last_ask.qty
                else:
                    break
        elif ask_price <= last_ask_price:
            for ask in self._curr_depth.asks:
                if ask.prc < last_ask_price:
                    ask_replenishment += ask.qty
                elif ask.prc == last_ask_price:
                    ask_replenishment += ask.qty - last_ask_qty
                else:
                    break

        if bid_price < last_bid_price:
            for last_bid in self._prev_depth.bids:
                if last_bid.prc > bid_price:
                    bid_replenishment -= last_bid.qty
                elif last_bid.prc == bid_price:
                    bid_replenishment += bid_qty - last_bid.qty
                else:
                    break
        elif bid_price >= last_bid_price:
            for bid in self._curr_depth.bids:
                if bid.prc > last_bid_price:
                    bid_replenishment += bid.qty
                elif bid.prc == last_bid_price:
                    bid_replenishment += bid.qty - last_bid_qty
                else:
                    break

        dt = self._curr_depth.resp_ts - self._prev_depth.resp_ts
        ask_rr = ask_replenishment / dt
        bid_rr = bid_replenishment / dt
        self._l_rr_calc.on_impulse(imp=ask_rr, ts=self._curr_depth.resp_ts)
        self._s_rr_calc.on_impulse(imp=bid_rr, ts=self._curr_depth.resp_ts)

    def add_depth(self, depth: Depth):
        if not self.has_curr_depth():
            self._curr_depth = depth
            self._curr_betwn_depth_long_trd_qty = 0
            self._curr_betwn_depth_short_trd_qty = 0
            return

        ts = depth.resp_ts
        if not self.has_prev_depth():
            if ts < self._curr_depth.resp_ts:
                logging.warning(f"Received depth whose localtime ({ts}) is smaller than last observed localtime ({self._curr_depth.resp_ts}). Skipped")
                return
            elif ts <= self._curr_depth.resp_ts:
                self._curr_depth = depth
                self._curr_betwn_depth_long_trd_qty = 0
                self._curr_betwn_depth_short_trd_qty = 0
            else:
                self._prev_depth = self._curr_depth
                self._prev_betwn_depth_long_trd_qty = self._curr_betwn_depth_long_trd_qty
                self._prev_betwn_depth_short_trd_qty = self._curr_betwn_depth_short_trd_qty
                self._curr_depth = depth
                self._curr_betwn_depth_long_trd_qty = 0
                self._curr_betwn_depth_short_trd_qty = 0
                self._update_rr()
        else:
            if ts < self._curr_depth.resp_ts:
                logging.warning(
                    f"Received depth whose localtime ({ts}) is smaller than last observed localtime ({self._curr_depth.resp_ts}). Skipped")
                return
            elif ts == self._curr_depth.resp_ts:
                self._curr_depth = depth
                self._curr_betwn_depth_long_trd_qty = 0
                self._curr_betwn_depth_short_trd_qty = 0
                self._update_rr()
            else:
                self._prev_depth = self._curr_depth
                self._prev_betwn_depth_long_trd_qty = self._curr_betwn_depth_long_trd_qty
                self._prev_betwn_depth_short_trd_qty = self._curr_betwn_depth_short_trd_qty
                self._curr_depth = depth
                self._curr_betwn_depth_long_trd_qty = 0
                self._curr_betwn_depth_short_trd_qty = 0
                self._update_rr()

    def add_bbo(self, bbo: BBO):
        self.add_depth(depth=bbo.to_depth())

    def get_factor(self, ts: int):
        lar = 0
        if self._l_ar_calc.is_initialized:
            lar = self._l_ar_calc.get_impulse(ts=ts)
        lrr = 0
        if self._l_rr_calc.is_initialized:
            lrr = self._l_rr_calc.get_impulse(ts=ts)
        sar = 0
        if self._s_ar_calc.is_initialized:
            sar = self._s_ar_calc.get_impulse(ts=ts)
        srr = 0
        if self._s_rr_calc.is_initialized:
            srr = self._s_rr_calc.get_impulse(ts=ts)
        return lar - lrr, sar - srr

    def record(self, ts: int):
        lar = 0
        if self._l_ar_calc.is_initialized:
            lar = self._l_ar_calc.get_impulse(ts=ts)
        lrr = 0
        if self._l_rr_calc.is_initialized:
            lrr = self._l_rr_calc.get_impulse(ts=ts)
        sar = 0
        if self._s_ar_calc.is_initialized:
            sar = self._s_ar_calc.get_impulse(ts=ts)
        srr = 0
        if self._s_rr_calc.is_initialized:
            srr = self._s_rr_calc.get_impulse(ts=ts)
        return lar, lrr, sar, srr


class CalPastInstantArrivalRate(CalRealTime):
    def __init__(self, halflife: float) -> None:
        self._halflife = halflife  # ms
        self._lambda = 0.5 ** (1 / self._halflife)  # per 1 ms interval

        self._long_curr_trade_ts = None
        self._long_last_trade_ts = None
        self._lar_last_update_time = None
        self._long_curr_trade_qty = 0
        self._short_curr_trade_ts = None
        self._short_last_trade_ts = None
        self._sar_last_update_time = None
        self._short_curr_trade_qty = 0

        self._lar = 0
        self._sar = 0

    def add_trade(self, trade: Trade):
        if trade.side == 1:
            if self._long_curr_trade_ts and (trade.local_time <= self._long_curr_trade_ts):
                self._long_curr_trade_qty += trade.quantity
            else:
                self._long_last_trade_ts = self._long_curr_trade_ts
                self._long_curr_trade_ts = trade.local_time
                self._long_curr_trade_qty = trade.quantity

            if self._long_last_trade_ts:
                trd_dt = max(self._long_curr_trade_ts - self._long_last_trade_ts, 0)
                lar = self._long_curr_trade_qty / trd_dt
                if self._lar_last_update_time:
                    decay_dt = max(self._long_curr_trade_ts - self._lar_last_update_time, 0)
                    decayed_lar = self._lar * self._lambda ** decay_dt
                    self._lar = max(lar, decayed_lar)
                else:
                    self._lar = lar
                self._lar_last_update_time = self._long_curr_trade_ts

        elif trade.side == -1:
            if self._short_curr_trade_ts and (trade.local_time <= self._short_curr_trade_ts):
                self._short_curr_trade_qty += trade.quantity
            else:
                self._short_last_trade_ts = self._short_curr_trade_ts
                self._short_curr_trade_ts = trade.local_time
                self._short_curr_trade_qty = trade.quantity

            if self._short_last_trade_ts:
                trd_dt = max(self._short_curr_trade_ts - self._short_last_trade_ts, 0)
                sar = self._short_curr_trade_qty / trd_dt
                if self._sar_last_update_time:
                    decay_dt = max(self._short_curr_trade_ts - self._sar_last_update_time, 0)
                    decayed_sar = self._sar * self._lambda ** decay_dt
                    self._sar = max(sar, decayed_sar)
                else:
                    self._sar = sar
                self._sar_last_update_time = self._short_curr_trade_ts
        else:
            raise NotImplementedError()

    def get_factor(self, ts: int):
        if self._lar_last_update_time:
            lar_decay_dt = max(ts - self._lar_last_update_time, 0)
        else:
            lar_decay_dt = 0
        self._lar *= self._lambda ** lar_decay_dt
        self._lar_last_update_time = ts
        if self._sar_last_update_time:
            sar_decay_dt = max(ts - self._sar_last_update_time, 0)
        else:
            sar_decay_dt = 0
        self._sar *= self._lambda ** sar_decay_dt
        self._sar_last_update_time = ts
        return self._lar, self._sar
