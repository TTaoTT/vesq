import math


class EMAvgDecay:
    def __init__(self, halflife: float):
        self._avg = ...
        self._halflife = halflife  # ms
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval
        self.last_update_time = -1
        self._numeric_tol = 1e-10
        self.is_initialized: bool = False
        self.soft_reset: bool = False

    def init(self, avg: float, ts: int):
        self._avg = avg
        self.last_update_time = ts
        self.is_initialized = True

    def on_wakeup(self, ts: int):
        if not self.is_initialized:
            return
        dt = ts - self.last_update_time
        dt = max(dt, 0)
        lambda_dt = self._lambda ** dt
        self._avg *= lambda_dt
        if abs(self._avg) < self._numeric_tol:
            self.soft_reset = True
        self.last_update_time = ts

    def on_val(self, val: float, ts: int):
        if math.isnan(val) or math.isinf(val):
            return

        if not self.is_initialized:
            self.init(avg=val, ts=ts)
        if self.soft_reset:
            self._avg = val
            self.soft_reset = False
        else:
            self.on_wakeup(ts=ts)
            self._avg += val * (1 - self._lambda)
        self.last_update_time = ts

    def get_avg(self, ts: int):
        if self.is_initialized:
            self.on_wakeup(ts=ts)
            return self._avg
        return None


class EMAvgNoDecay:
    def __init__(self, halflife: float):
        self._avg = ...
        self._halflife = halflife  # ms
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval
        self._numeric_tol = 1e-10
        self.is_initialized: bool = False
        self._last_val_ts = ...
        self._last_val = ...

    def init(self, avg: float, ts: int):
        self._avg = avg
        self.is_initialized = True
        self._last_val_ts = ts
        self._last_val = avg

    def on_val(self, val: float, ts: int):
        if math.isnan(val) or math.isinf(val):
            return

        if not self.is_initialized:
            self.init(avg=val, ts=ts)
            return

        dt = ts - self._last_val_ts
        if dt <= 0:
            self._avg -= self._last_val * (1 - self._lambda)
            self._avg += val * (1 - self._lambda)
            self._last_val_ts = ts
            self._last_val = val
            return

        self._avg *= self._lambda ** dt
        if dt > 1:
            n = dt - 1
            a1 = self._lambda
            self._avg += self._last_val * (1 - self._lambda) * a1 * (1 - self._lambda ** n) / (1 - self._lambda)
        self._avg += val * (1 - self._lambda)
        self._last_val_ts = ts
        self._last_val = val

    def get_avg(self, ts: int):
        if self.is_initialized:
            self.on_val(val=self._last_val, ts=ts)
            return self._avg
        return None


class EMSum:
    def __init__(self, halflife: int):
        self._sum = 0
        self._halflife = halflife  # ms
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 ms interval

        self._last_update_time = -1

        self._numeric_tol = 1e-10

    def on_wakeup(self, ts: int):
        dt = ts - self._last_update_time
        dt = max(dt, 0)
        lambda_dt = self._lambda ** dt
        self._sum *= lambda_dt
        if abs(self._sum) < self._numeric_tol:
            self._sum = 0
        self._last_update_time = ts

    def on_val(self, val: float, ts: int):
        if math.isnan(val) or math.isinf(val):
            return

        self.on_wakeup(ts=ts)
        self._sum += val
        self._last_update_time = ts

    def get_sum(self, ts: int):
        self.on_wakeup(ts=ts)
        return self._sum


class EMEvent:
    def __init__(self, halflife: float):
        self._avg = ...
        self._halflife = halflife  # events
        self._lambda = 0.5 ** (1/self._halflife)  # per 1 event
        self._numeric_tol = 1e-10
        self.is_initialized: bool = False
        self._last_val = ...
        self._event_idx = 0

    def init(self, avg: float):
        self._avg = avg
        self.is_initialized = True
        self._last_val = avg
        self._event_idx += 1

    def on_val(self, val: float, event_idx: int=None):
        if math.isnan(val) or math.isinf(val):
            return

        if not self.is_initialized:
            self.init(avg=val)
            return

        if event_idx is None:
            event_idx = self._event_idx + 1

        if event_idx <= self._event_idx:
            self._avg -= self._last_val * (1 - self._lambda)
            self._avg += val * (1 - self._lambda)
            self._last_val = val
            return

        self._avg *= self._lambda
        self._avg += val * (1 - self._lambda)
        self._event_idx += 1
        self._last_val = val

    def get_avg(self):
        if self.is_initialized:
            return self._avg
        return None

