from xex_mm.utils.calc_node import CalRealTime
from xex_mm.utils.enums import Direction


class ConsecutiveOrderFillTracker(CalRealTime):
    def __init__(self):
        super(ConsecutiveOrderFillTracker, self).__init__()
        self._consecutive_fill_direction = ...
        self._consecutive_oids_filled = set()

    def on_fill(self, d: Direction, qty: float, oid: str) -> bool:
        if d == self._consecutive_fill_direction:
            self._consecutive_oids_filled.add(oid)
            return True
        self._consecutive_oids_filled.clear()
        self._consecutive_oids_filled.add(oid)
        self._consecutive_fill_direction = d
        return True

    def get_factor(self, ts: int):
        return self._consecutive_fill_direction, len(self._consecutive_oids_filled)
