from collections import deque
from typing import List, Dict

from xex_mm.utils.base import Exchange, Depth


class XEx(Exchange):
    def __init__(self, exs: List[Exchange]):
        self.exs = exs

        self.__order_id_to_ex_map: Dict[int, Exchange] = {}

    def get_latest_depth(self, n_lvls: int):
        merged_depth = Depth(asks=deque(), bids=deque())
        for ex in self.exs:
            merged_depth.layer(ex.get_latest_depth(n_lvls=n_lvls))
        return merged_depth

    def get_latest_trade(self):
        pass

