from typing import Set, List

from orderedset import OrderedSet

from xex_mm.utils.base import OrderManager, TransOrder, Order
from xex_mm.utils.configs import QuantityPrecisionManager


class AtTouchHedgeOrderManager(OrderManager):
    def __init__(self):
        super(AtTouchHedgeOrderManager, self).__init__()
        self.hedge_order_oids: OrderedSet[str] = OrderedSet()
        self.pending_cancel_hedge_oids: Set[str] = set()
        self.hedge_orders_side: int = ...
        self.hedge_orders_int_prc: int = ...
        self.hedge_order_total_unfilled_qty: int = 0
        self.quantity_precision_manager = QuantityPrecisionManager()
        self.quantity_precision_map = dict()

    def is_hedge_order(self, oid: str):
        return oid in self.hedge_order_oids

    def get_quantity_precision(self, iid: str):
        if iid in self.quantity_precision_map:
            return self.quantity_precision_map[iid]
        quantity_precision = self.quantity_precision_manager.get_quantity_precision_by_iid(iid=iid)
        self.quantity_precision_map[iid] = quantity_precision
        return quantity_precision

    def update_order(self, order: Order):
        if not self.msg_order_manager.contains_order(oid=order.order_id):
            if order.order_id in self.hedge_order_oids:
                trans_order = self.trans_order_manager.order_at(oid=order.order_id)
                self.hedge_order_total_unfilled_qty -= trans_order.quantity
                self.hedge_order_total_unfilled_qty += (order.qty - order.filled_qty)
                iid = f"{order.ex}.{order.contract}"
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
            elif order.order_id in self.pending_cancel_hedge_oids:
                pass
            else:
                raise RuntimeError()
        else:
            if order.order_id in self.hedge_order_oids:
                prev_order: Order = self.msg_order_manager.order_at(order.order_id)
                self.hedge_order_total_unfilled_qty -= (prev_order.qty - prev_order.filled_qty)
                self.hedge_order_total_unfilled_qty += (order.qty - order.filled_qty)
                iid = f"{order.ex}.{order.contract}"
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
            elif order.order_id in self.pending_cancel_hedge_oids:
                pass
            else:
                raise RuntimeError()
        super(AtTouchHedgeOrderManager, self).update_order(order=order)

    def add_hedge_trans_order(self, hedge_order: TransOrder, oid: str) -> bool:
        if self.hedge_orders_side is not Ellipsis:
            assert hedge_order.side == self.hedge_orders_side
        else:
            self.hedge_orders_side = hedge_order.side
        int_hedge_prc = self.float_prc_to_int_prc(float_prc=hedge_order.price)
        if self.hedge_orders_int_prc is not Ellipsis:
            assert int_hedge_prc == self.hedge_orders_int_prc
        else:
            self.hedge_orders_int_prc = int_hedge_prc
        self.hedge_order_oids.add(oid)
        self.hedge_order_total_unfilled_qty += hedge_order.quantity
        self.add_trans_order(order=hedge_order, oid=oid)
        return True

    def hedge_order_pending_cancel(self, oid: str) -> bool:
        hedge_order = self.get_trans_order(oid=oid)
        hedge_order.cancel = True
        self.hedge_order_oids.remove(oid)
        self.pending_cancel_hedge_oids.add(oid)
        if self.msg_order_manager.contains_order(oid=oid):
            order = self.msg_order_manager.order_at(oid=oid)
            self.hedge_order_total_unfilled_qty -= (order.qty - order.filled_qty)
            iid = f"{order.ex}.{order.contract}"
            assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
        else:
            self.hedge_order_total_unfilled_qty -= hedge_order.quantity
            assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=hedge_order.iid)
        if len(self.hedge_order_oids) == 0:
            self.hedge_orders_side = ...
            self.hedge_orders_int_prc = ...
            self.hedge_order_total_unfilled_qty = 0
        return True

    def pop_hedge_order(self, oid: str) -> bool:
        if oid in self.pending_cancel_hedge_oids:
            self.pending_cancel_hedge_oids.remove(oid)
        else:
            self.hedge_order_oids.remove(oid)
            hedge_order = self.get_trans_order(oid=oid)
            assert not hedge_order.cancel
            if self.msg_order_manager.contains_order(oid=oid):
                order = self.msg_order_manager.order_at(oid=oid)
                iid = f"{order.ex}.{order.contract}"
                assert abs(order.qty - hedge_order.quantity) < 10 ** -self.get_quantity_precision(iid=iid)
                self.hedge_order_total_unfilled_qty -= (order.qty - order.filled_qty)
                iid = f"{order.ex}.{order.contract}"
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=iid)
            else:
                self.hedge_order_total_unfilled_qty -= hedge_order.quantity
                assert self.hedge_order_total_unfilled_qty > -10**-self.get_quantity_precision(iid=hedge_order.iid)
            if len(self.hedge_order_oids) == 0:
                self.hedge_orders_side = ...
                self.hedge_orders_int_prc = ...
                self.hedge_order_total_unfilled_qty = 0
        self.pop_trans_order(oid=oid)
        self.pop_order(oid=oid)
        return True

    def get_hedge_trans_orders(self) -> List[TransOrder]:
        return [self.get_trans_order(oid=oid) for oid in self.hedge_order_oids]