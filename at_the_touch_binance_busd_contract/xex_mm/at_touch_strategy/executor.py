import logging
import math
from collections import defaultdict
from typing import Dict, List, Any, Tuple, Union, Set

import numpy as np
import pandas as pd

from xex_mm.signals.LocalCalculator import LocalCalculatorBase
from xex_mm.strategy_configs.at_touch_config import PredRefPrcMode, QuoteLevelConfig
from xex_mm.utils.base import Trade, BBO, TransOrder, XexInventoryManager, Depth, OrderManager
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.utils.configs import FeeConfig

from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model_no_partial_fill2 import \
    SingleArrivalRateXExMmModelNoPartialFill

from xex_mm.signals.SignalCalculator import SignalCalculatorBase
from xex_mm.xex_executor.params import ModelParams
from xex_mm.at_touch_strategy.order_manager import AtTouchHedgeOrderManager


class AtTouchExecutor:
    def __init__(self,
                 contract: str,
                 amt_bnd: float,
                 pred_on: bool,
                 ref_prc_reset_sigma_multiplier: float,
                 beta: float,
                 tau_lb: int,
                 T: int,
                 pred_ref_prc_mode: PredRefPrcMode,
                 quote_lvl_cfgs: Dict[str, QuoteLevelConfig],
                 max_inven: float,
                 consecutive_fill_limit: int,
                 mdl_params: ModelParams,
                 local_cal: LocalCalculatorBase,
                 onboard: bool = False) -> None:

        self.contract = contract
        self.active_exs = set()

        self._depth_map: Dict[str, Depth] = dict()
        self.amt_bnd = amt_bnd
        self._setup_mm_model()
        self.pred_on = pred_on
        self._mdl_params = mdl_params

        self._ref_prc_reset_sigma_multiplier = ref_prc_reset_sigma_multiplier
        self.inven_bnd = 1000
        self.beta = beta
        self._pred_ref_prc_mode = pred_ref_prc_mode
        self.quote_lvl_cfgs = quote_lvl_cfgs
        self.max_inven = max_inven
        self.onboard = onboard
        self.pnl_decomp = []
        self._tau_lb = tau_lb
        self.T = T

        self.local_cal = local_cal
        self.signal_cal_map = dict()
        self.inventory_manager = XexInventoryManager(contract=self.contract)

        self._ref_prc_map = dict()
        self._xex_ref_prc_map = dict()

        self._mm_order_manager_map: Dict[str, OrderManager] = ...
        self._hedge_order_manager_map: Dict[str, AtTouchHedgeOrderManager] = ...

        self._xex = "xex"

        self._numeric_tol = 1e-10
        self.inven_tol = 1

        self._consecutive_fill_limit = consecutive_fill_limit
        self._consecutive_fill_direction = ...
        self._n_consecutive_fills = 0

    def update_n_consecutive_hits(self, consecutive_fill_direction: int, n_consecutive_fills: float):
        self._consecutive_fill_direction = consecutive_fill_direction
        self._n_consecutive_fills = n_consecutive_fills

    def _setup_mm_model(self):
        self.mm_mdl = SingleArrivalRateXExMmModelNoPartialFill(fee_rate_map=FeeConfig().to_dict())

    def update_signal_cal(self, signal_cal: SignalCalculatorBase, ex: str) -> bool:
        self.signal_cal_map[ex] = signal_cal
        return True

    def get_signal_cal(self, ex: str):
        return self.signal_cal_map.get(ex)

    def get_tau(self, ex: str, ts: int):
        depth_freq, bbo_freq, trade_freq = self.local_cal.update_freq_cal_map[(self.contract, ex)].get_factor(ts=ts)
        if depth_freq is None:
            return
        depth_freq = max(self._tau_lb, depth_freq)
        return depth_freq

    def update_ref_prc(self, ex: str, ref_prc: float):
        self._ref_prc_map[ex] = ref_prc
        success = self.mm_mdl.update_ref_prc(contract=self.contract, ex=ex, ref_prc=ref_prc)
        if not success:
            return False
        return True

    def get_ref_prc(self, ex: str) -> Union[float, None]:
        return self._ref_prc_map.get(ex)

    def update_xex_ref_prc(self, ex: str, ref_prc: float) -> bool:
        self._xex_ref_prc_map[ex] = ref_prc
        return True

    def get_xex_ref_prc(self, ex: str) -> Union[float, None]:
        return self._xex_ref_prc_map.get(ex)

    def update_prediction(self, ex: str, ts: int) -> bool:
        tau = self.get_tau(ex=ex, ts=ts)
        if tau is None:
            return False

        contract = self.contract

        ref_prc = self.get_ref_prc(ex=ex)

        if self.pred_on:
            pred_las, pred_sas = self.local_cal.pred_trade_arrival_size(contract=contract, ex=ex, ts=ts)
            if (pred_las is None) or (pred_sas is None):
                return False
            pred_lat, pred_sat = self.local_cal.pred_trade_arrival_time(contract=contract, ex=ex, ts=ts)
            if (pred_lat is None) or (pred_sat is None):
                return False

            pred_lhs = self.local_cal.predict_long_half_spread(contract=contract, ex=ex, ts=ts)
            if pred_lhs is None:
                return False
            pred_shs = self.local_cal.predict_short_half_spread(contract=contract, ex=ex, ts=ts)
            if pred_shs is None:
                return False
            pred_sigma = self.local_cal.predict_sigma(contract=contract, ex=ex, ts=ts)
            if pred_sigma is None:
                return False
            pred_ref_prc_ret = self.get_signal_cal(ex=ex).predict_ref_prc_ret(ms=tau)
            if pred_ref_prc_ret is not None:
                pred_ref_prc = ref_prc * math.exp(pred_ref_prc_ret * tau)
            else:
                return False
            pred_latency = self.local_cal.pred_latency(contract=contract, ex=ex, ts=ts)
            if pred_latency is None:
                return False
        else:
            if not self.mm_mdl.has_depth_metrics(contract=contract, ex=ex):
                return False

            pred_sigma = self.local_cal.curr_ema_sigma(contract=contract, ex=ex, ts=ts)
            if pred_sigma is None:
                return False

            las_sas = self.local_cal.curr_trade_arrival_size(contract=contract, ex=ex, ts=ts)
            if las_sas is None:
                return False
            pred_las, pred_sas = las_sas
            if (pred_las is None) or (pred_sas is None):
                return False

            lat_sat = self.local_cal.curr_trade_arrival_time(contract=contract, ex=ex, ts=ts)
            if lat_sat is None:
                return False
            pred_lat, pred_sat = lat_sat
            if (pred_lat is None) or (pred_sat is None):
                return False

            pred_lhs = self.local_cal.curr_emavg_long_half_spread(contract=contract, ex=ex, ts=ts)
            if pred_lhs is None:
                return False
            pred_shs = self.local_cal.curr_emavg_short_half_spread(contract=contract, ex=ex, ts=ts)
            if pred_shs is None:
                return False

            if self._pred_ref_prc_mode == PredRefPrcMode.neutral:
                pred_ref_prc = ref_prc
                pred_ref_prc_ret = 0
            elif self._pred_ref_prc_mode == PredRefPrcMode.xex:
                pred_ref_prc = self.get_xex_ref_prc(ex=ex)
                pred_ref_prc_ret = math.log(pred_ref_prc / ref_prc)
            elif self._pred_ref_prc_mode == PredRefPrcMode.ret:
                pred_ref_prc_ret = self.local_cal.curr_emavg_ref_prc_ret(contract=contract, ex=ex, ts=ts)
                if pred_ref_prc_ret is None:
                    pred_ref_prc_ret = 0
                pred_ref_prc = ref_prc * math.exp(pred_ref_prc_ret * tau)
            elif self._pred_ref_prc_mode == PredRefPrcMode.prc:
                pred_ref_prc = self.local_cal.curr_emavg_ref_prc(contract=contract, ex=ex, ts=ts)
                pred_ref_prc_ret = math.log(pred_ref_prc/ref_prc)
                # reset
                reset_bnd = math.sqrt(tau) * pred_sigma * self._ref_prc_reset_sigma_multiplier
                prc_lag = abs(ref_prc - pred_ref_prc)
                if (abs(pred_ref_prc_ret) > reset_bnd) or (prc_lag > reset_bnd):
                    pred_ref_prc = ref_prc
                    pred_ref_prc_ret = 0
                    self.local_cal.ref_prc_cal_map[(contract, ex)].await_reset = True
                    raise RuntimeError(f"pred_ref_prc_ret={pred_ref_prc_ret}, prc_lag={prc_lag}, reset_ret_bnd={reset_bnd}, ref_prc_reset_sigma_multiplier={self._ref_prc_reset_sigma_multiplier}, pred_sigma={pred_sigma}")
            elif self._pred_ref_prc_mode == PredRefPrcMode.tar:
                raise NotImplementedError("LAR, SAR deprecated")
            else:
                raise NotImplementedError()
            pred_latency = self.local_cal.curr_emavg_latency(contract=contract, ex=ex, ts=ts)

        pred_lat = max(pred_lat, 1)
        pred_sat = max(pred_sat, 1)

        self.mm_mdl.update_pred_arrival_size_in_tokens(
            contract=contract, ex=ex, d=Direction.long, arrival_size_in_tokens=pred_las)
        self.mm_mdl.update_pred_arrival_size_in_tokens(
            contract=contract, ex=ex, d=Direction.short, arrival_size_in_tokens=pred_sas)
        self.mm_mdl.update_pred_arrival_time(
            contract=contract, ex=ex, d=Direction.long, arrival_time=pred_lat)
        self.mm_mdl.update_pred_arrival_time(
            contract=contract, ex=ex, d=Direction.short, arrival_time=pred_sat)

        self.mm_mdl.update_pred_sigma(
            contract=contract, ex=ex,
            pred=pred_sigma)
        self.mm_mdl.update_pred_half_spread(
            contract=contract, ex=ex, d=Direction.long,
            pred=pred_lhs/ref_prc)
        self.mm_mdl.update_pred_half_spread(
            contract=contract, ex=ex, d=Direction.short,
            pred=pred_shs/ref_prc)
        self.mm_mdl.update_pred_ref_prc_ret(
            contract=contract, ex=ex,
            pred=math.log(pred_ref_prc/ref_prc))
        self.mm_mdl.update_pred_latency(
            contract=contract, ex=ex,
            pred_latency=pred_latency
        )

        return None not in {pred_las, pred_sas, pred_lat, pred_sat, pred_lhs, pred_shs, pred_sigma, pred_ref_prc_ret}

    def get_trl(self, ex: str, ts: int) -> Union[Tuple[float, float], None]:
        return self.local_cal.trade_ret_lambda_cal_map[(self.contract, ex)].get_factor(ts=ts)

    def update_mm_mdl_with_local_cal(self, ex: str, ts: int) -> bool:
        tau = self.get_tau(ex=ex, ts=ts)
        if tau is not None:
            self.mm_mdl.update_tau(contract=self.contract, ex=ex, tau=tau)

        las_sas = self.local_cal.curr_trade_arrival_size(contract=self.contract, ex=ex, ts=ts)
        lat_sat = self.local_cal.curr_trade_arrival_time(contract=self.contract, ex=ex, ts=ts)
        ref_prc = self.get_ref_prc(ex=ex)
        if (tau is not None) and (las_sas is not None) and (lat_sat is not None) and (ref_prc is not None):
            las, sas = las_sas
            lat, sat = lat_sat
            if (las is not None) and (sas is not None) and (lat is not None) and (sat is not None):
                mult = 20
                q_grp_size_ccy2 = min(las, sas) * tau / max(lat, sat) * ref_prc / mult
                self.mm_mdl.update_q_grp_size_ccy2(contract=self.contract, ex=ex, q_grp_size_ccy2=q_grp_size_ccy2)

        dms = self.local_cal.depth_metrics_cal_map[(self.contract, ex)].get_factor(ts=ts)
        q_grp_size_ccy2 = self.mm_mdl.get_q_grp_size_ccy2(contract=self.contract, ex=ex)
        if (dms is not None) and (q_grp_size_ccy2 is not None) and (ref_prc is not None):
            b_l, a_l, b_s, a_s = [x * ref_prc / q_grp_size_ccy2 for x in dms]
            self.mm_mdl.update_depth_metrics(contract=self.contract, ex=ex,
                                             b_l=b_l, a_l=a_l, b_s=b_s, a_s=a_s)

        res = self.get_trl(ex=ex, ts=ts)
        if res is not None:
            long_trade_ret_lambda, short_trade_ret_lambda = res
            if (long_trade_ret_lambda is not None) and (short_trade_ret_lambda is not None):
                lambda_cap = 1e5
                long_trade_ret_lambda = max(long_trade_ret_lambda, lambda_cap)
                short_trade_ret_lambda = max(short_trade_ret_lambda, lambda_cap)
                self.mm_mdl.update_trade_ret_lambda(contract=self.contract, ex=ex, d=Direction.long, trade_ret_lambda=long_trade_ret_lambda)
                self.mm_mdl.update_trade_ret_lambda(contract=self.contract, ex=ex, d=Direction.short, trade_ret_lambda=short_trade_ret_lambda)

        return True

    def update_depth(self, depth: Depth) -> bool:
        assert depth.meta_data.contract == self.contract

        self._depth_map[depth.meta_data.ex] = depth

        success = self.local_cal.update_depth(depth=depth)
        if not success:
            return False

        ex = depth.meta_data.ex
        ts = depth.resp_ts
        success = self.update_mm_mdl_with_local_cal(ex=ex, ts=ts)
        if not success:
            return False

        success = self.mm_mdl.update_depth(contract=self.contract, ex=ex, depth=depth)
        if not success:
            return False

        return True

    def add_active_ex(self, ex: str) -> bool:
        self.active_exs.add(ex)
        return True

    def remove_active_ex(self, ex: str) -> bool:
        if ex in self.active_exs:
            self.active_exs.remove(ex)
        return True

    def update_bbo(self, bbo: BBO) -> bool:

        """ Full update with bbo is slow. """
        contract = bbo.meta.contract
        if contract != self.contract:
            return False
        if not bbo.valid_bbo:
            return False
        ex = bbo.meta.ex
        if ex not in self.active_exs:
            return False

        success = self.local_cal.update_bbo(bbo=bbo)
        if not success:
            return False

        ts = bbo.local_time
        success = self.update_mm_mdl_with_local_cal(ex=ex, ts=ts)
        if not success:
            return False

        success = self.mm_mdl.update_bbo(contract=self.contract, ex=ex, bbo=bbo)
        if not success:
            return False

        """ Partial update with bbo is fast. """
        # self._mm_mdl.update_bbo(contract=contract, ex=ex, bbo=bbo)  # Partial, no xex update
        # # TODO: Factor update?

        return True

    def update_trade(self, trade: Trade) -> bool:
        assert trade.contract == self.contract
        self.local_cal.update_trade(trade=trade)

        ts = trade.local_time
        ex = trade.ex
        success = self.update_mm_mdl_with_local_cal(ex=ex, ts=ts)
        if not success:
            return False
        return True

    def update_inventory(self, inventory: float, contract: str, ex: str, ts: int) -> bool:
        iid = f"{ex}.{contract}"
        self.inventory_manager.update_inventory(contract=contract, ex=ex, inven=inventory)
        self.local_cal.inven_sigma_cal_map[iid].on_inventory(inven=self.inventory_manager.total_inventory, ts=ts)
        return True

    def link_mm_order_manager_map(self, mm_order_manager_map: Dict[str, OrderManager]) -> bool:
        self._mm_order_manager_map = mm_order_manager_map
        return True

    def link_hedge_order_manager_map(self, hedge_order_manager_map: Dict[str, AtTouchHedgeOrderManager]) -> bool:
        self._hedge_order_manager_map = hedge_order_manager_map
        return True

    def cal_pnl_decomp(self, ref_prc: float, delta: float, price: float, qty: float, side: int, type: str, ts: int):
        self.pnl_decomp.append(
            {
                "ref_prc":ref_prc,
                "spread_pnl":delta*qty,
                "aggression_pnl":(price - ref_prc - delta) * qty if side == -1 else (ref_prc - price - delta) * qty,
                "inven_chg": qty*side,
                "maker_amt":qty*price if type == "maker" else 0,
                "taker_amt":qty*price if type == "taker" else 0,
                "ts": ts
            }
        )

    def get_pnl_decomp_result(self):
        if len(self.pnl_decomp) == 0:
            return pd.DataFrame()

        df = pd.DataFrame(self.pnl_decomp)
        df["inven"] = df["inven_chg"].cumsum()
        df["maket_pnl"] = df["ref_prc"].diff() * df["inven"].shift()

        return df

    def is_stats_ready(self, contract: str):
        xex = "xex"
        depth_metrics_ready = self.mm_mdl.has_depth_metrics(contract=contract, ex=xex)
        return depth_metrics_ready

    def _update_stats(self, stats: Dict[str, Any], name: str, val: Any):
        if val is None:
            return
        stats[name] = val

    def get_stats(self, ts: int) -> Dict:
        stats = {}
        self._update_stats(stats=stats, name="ts", val=ts)
        self._update_stats(stats=stats, name="inven", val=self.inventory_manager.total_inventory)
        for ex, inven in self.inventory_manager.partial_inventory.items():
            self._update_stats(stats=stats, name=f"{ex}_inven", val=inven)
            self._update_stats(stats=stats, name=f"{ex}-inven_sigma", val=self.local_cal.inven_sigma_cal_map[(self.contract, ex)].get_factor(ts=ts))
        ref_prc = self.get_xex_ref_prc(ex=self._xex)
        if ref_prc is not None:
            self._update_stats(stats=stats, name="xex_ref_prc", val=ref_prc)
        for ex in self.active_exs:
            res = self.local_cal.depth_metrics_cal_map[(self.contract, ex)].get_factor(ts=ts)
            if res is not None:
                l_intercept, l_slope, s_intercept, s_slope = res
                self._update_stats(stats=stats, name=f"{ex}-long_intercept", val=l_intercept)
                self._update_stats(stats=stats, name=f"{ex}-long_slope", val=l_slope)
                self._update_stats(stats=stats, name=f"{ex}-short_intercept", val=s_intercept)
                self._update_stats(stats=stats, name=f"{ex}-short_slope", val=s_slope)
            self._update_stats(stats=stats, name=f"{ex}_q_grp_size_ccy2", val=self.mm_mdl.get_q_grp_size_ccy2(contract=self.contract, ex=ex))
            self._update_stats(stats=stats, name=f"{ex}_ref_prc", val=self.get_ref_prc(ex=ex))
            self._update_stats(stats=stats, name=f"{ex}_xex_ref_prc", val=self.get_xex_ref_prc(ex=ex))
            a_l = self.mm_mdl.get_depth_metric(contract=self.contract, ex=ex, metric="a_l")
            self._update_stats(stats=stats, name=f"{ex}_a_l", val=a_l)
            b_l = self.mm_mdl.get_depth_metric(contract=self.contract, ex=ex, metric="b_l")
            self._update_stats(stats=stats, name=f"{ex}_b_l", val=b_l)
            a_s = self.mm_mdl.get_depth_metric(contract=self.contract, ex=ex, metric="a_s")
            self._update_stats(stats=stats, name=f"{ex}_a_s", val=a_s)
            b_s = self.mm_mdl.get_depth_metric(contract=self.contract, ex=ex, metric="b_s")
            self._update_stats(stats=stats, name=f"{ex}_b_s", val=b_s)
            self._update_stats(stats=stats, name=f"{ex}_s_l", val=self.mm_mdl.get_depth_metric(contract=self.contract, ex=ex, metric="s_l"))
            self._update_stats(stats=stats, name=f"{ex}_s_s", val=self.mm_mdl.get_depth_metric(contract=self.contract, ex=ex, metric="s_s"))
            res = self.local_cal.sprd_mult_cal_map[(self.contract, ex)].get_factor(ts=ts)
            if res is not None:
                side, hs_mult = res
                if side is not Ellipsis:
                    self._update_stats(stats=stats, name=f"{ex}_hs_mult", val=side * hs_mult)
            res = self.get_trl(ex=ex, ts=ts)
            if res is not None:
                long_trl, short_trl = res
                if (long_trl is not None) and (short_trl is not None):
                    self._update_stats(stats=stats, name=f"{ex}_long_trl", val=long_trl)
                    self._update_stats(stats=stats, name=f"{ex}_short_trl", val=short_trl)
            if self.pred_on:
                self._update_stats(stats=stats, name=f"{ex}_sigma", val=self.local_cal.predict_sigma(contract=self.contract, ex=ex, ts=ts))
                las, sas = self.local_cal.pred_trade_arrival_size(contract=self.contract, ex=ex, ts=ts)
                self._update_stats(stats=stats, name=f"{ex}_las", val=las)
                self._update_stats(stats=stats, name=f"{ex}_sas", val=sas)
                lat, sat = self.local_cal.pred_trade_arrival_time(contract=self.contract, ex=ex, ts=ts)
                self._update_stats(stats=stats, name=f"{ex}_lat", val=lat)
                self._update_stats(stats=stats, name=f"{ex}_sat", val=sat)
            else:
                self._update_stats(stats=stats, name=f"{ex}_mdl_sigma", val=self.mm_mdl.get_pred_sigma(contract=self.contract, ex=ex))
                mdl_pred_ref_prc_ret = self.mm_mdl.get_pred_ref_prc_ret(contract=self.contract, ex=ex)
                self._update_stats(stats=stats, name=f"{ex}_mdl_ref_prc_ret", val=mdl_pred_ref_prc_ret)
                ref_prc = self.get_ref_prc(ex=ex)
                if ref_prc is not None and mdl_pred_ref_prc_ret is not None:
                    mdl_pred_ref_prc = ref_prc * math.exp(mdl_pred_ref_prc_ret)
                    self._update_stats(stats=stats, name=f"{ex}_mdl_pred_ref_prc", val=mdl_pred_ref_prc)
                self._update_stats(stats=stats, name=f"{ex}_sigma", val=self.local_cal.curr_ema_sigma(contract=self.contract, ex=ex, ts=ts))
                self._update_stats(stats=stats, name=f"{ex}_impulse_sigma", val=self.local_cal.curr_impulse_sigma(contract=self.contract, ex=ex, ts=ts))
                self._update_stats(stats=stats, name=f"{ex}_emavg_ref_prc", val=self.local_cal.curr_emavg_ref_prc(contract=self.contract, ex=ex, ts=ts))
                self._update_stats(stats=stats, name=f"{ex}_emavg_ref_prc_ret", val=self.local_cal.curr_emavg_ref_prc_ret(contract=self.contract, ex=ex, ts=ts))
                las_sas = self.local_cal.curr_trade_arrival_size(contract=self.contract, ex=ex, ts=ts)
                if las_sas is not None:
                    las, sas = las_sas
                    if (las is not None) and (sas is not None):
                        self._update_stats(stats=stats, name=f"{ex}_las", val=las)
                        self._update_stats(stats=stats, name=f"{ex}_sas", val=sas)
                lat_sat = self.local_cal.curr_trade_arrival_time(contract=self.contract, ex=ex, ts=ts)
                if lat_sat is not None:
                    lat, sat = lat_sat
                    if (lat is not None) and (sat is not None):
                        self._update_stats(stats=stats, name=f"{ex}_lat", val=lat)
                        self._update_stats(stats=stats, name=f"{ex}_sat", val=sat)
                depth_freq, bbo_freq, trade_freq = self.local_cal.update_freq_cal_map[(self.contract, ex)].get_factor(ts=ts)
                self._update_stats(stats=stats, name=f"{ex}_depth_freq", val=depth_freq)
                self._update_stats(stats=stats, name=f"{ex}_bbo_freq", val=bbo_freq)
                self._update_stats(stats=stats, name=f"{ex}_trade_freq", val=trade_freq)
                self._update_stats(stats=stats, name=f"{ex}_emavg_latency", val=self.local_cal.curr_emavg_latency(contract=self.contract, ex=ex, ts=ts))
        return stats

    def _should_quote_direction(self, quote_direction: int, quantity_to_quote: float):
        if quote_direction == self._consecutive_fill_direction:
            if self._n_consecutive_fills > self._consecutive_fill_limit:
                return False
        if quantity_to_quote < self._numeric_tol:
            return False
        return True

    def get_orders(self, t: int):
        contract = self.contract
        I_ref_prc = self.get_xex_ref_prc(ex=self._xex)
        I_ccy1 = self.inventory_manager.total_inventory

        mm_orders_to_send_map = defaultdict(lambda: [])
        mm_oids_to_cancel_map = defaultdict(lambda: set())

        data = []
        for _ex in self.active_exs:
            mm_orders_to_send = mm_orders_to_send_map[_ex]
            mm_oids_to_cancel = mm_oids_to_cancel_map[_ex]

            ex_I_in_ccy1 = self.inventory_manager.get_inventory(contract=self.contract, ex=_ex)
            ex_I_in_ccy2 = ex_I_in_ccy1 * I_ref_prc
            for d in [Direction.long, Direction.short]:
                if _ex not in self._depth_map:
                    continue
                quote_lvl_cfg = self.quote_lvl_cfgs[_ex]
                q_in_ccy2 = quote_lvl_cfg.QUOTE_QUANTITY
                if ex_I_in_ccy2 * d > 0:
                    q_in_ccy2 = max(q_in_ccy2 * (1 - (ex_I_in_ccy2 / self.max_inven) ** 2), 0)

                ref_prc = self.get_ref_prc(ex=_ex)
                if d == Direction.long:
                    best_bid = self._depth_map[_ex].bids[0]
                    best_bid_int_prc = self._mm_order_manager_map[_ex].float_prc_to_int_prc(float_prc=best_bid.prc)
                    back_bid_float_prc = min(ref_prc * math.exp(-quote_lvl_cfg.RETURN_UB), best_bid.prc)
                    back_bid_int_prc = self._mm_order_manager_map[_ex].float_prc_to_int_prc(float_prc=back_bid_float_prc)
                    s_l = math.log(ref_prc/best_bid.prc)
                    """ Manage order vector """
                    if len(self._mm_order_manager_map[_ex].bid_ladder) == 0:
                        if not self._should_quote_direction(quote_direction=d, quantity_to_quote=q_in_ccy2):
                            pass
                        else:
                            # mm order
                            quote_rets = np.arange(s_l, quote_lvl_cfg.RETURN_UB, quote_lvl_cfg.RETURN_STEP_SIZE)
                            for r in quote_rets:
                                p = ref_prc * math.exp(-r)
                                mm_orders_to_send.append(TransOrder(
                                    side=d,
                                    p=p,
                                    q=q_in_ccy2 / ref_prc,
                                    iid=".".join((_ex, contract)),
                                    floor=0,
                                    delta=ref_prc - p,
                                    maker_taker=MakerTaker.maker
                                ))
                    else:
                        # cancel front
                        for int_prc, oid_set in self._mm_order_manager_map[_ex].bid_ladder.items():
                            if len(oid_set) == 0:
                                self._mm_order_manager_map[_ex].bid_ladder.pop(int_prc)
                                continue
                            if int_prc > best_bid_int_prc:
                                mm_oids_to_cancel |= oid_set
                            else:
                                break
                        # cancel back
                        for int_prc, oid_set in reversed(self._mm_order_manager_map[_ex].bid_ladder.items()):
                            if len(oid_set) == 0:
                                self._mm_order_manager_map[_ex].bid_ladder.pop(int_prc)
                                continue
                            if int_prc < back_bid_int_prc:
                                mm_oids_to_cancel |= oid_set
                            else:
                                break
                        # fill front
                        if not self._should_quote_direction(quote_direction=d, quantity_to_quote=q_in_ccy2):
                            pass
                        else:
                            our_best_bid_int_prc = self._mm_order_manager_map[_ex].bid_ladder.keys()[0]
                            if best_bid_int_prc > our_best_bid_int_prc:
                                our_best_bid_float_prc = self._mm_order_manager_map[_ex].int_prc_to_float_prc(int_prc=our_best_bid_int_prc)
                                float_prc = best_bid.prc
                                while (float_prc > our_best_bid_float_prc + self._numeric_tol) and (float_prc > back_bid_float_prc - self._numeric_tol):
                                    mm_orders_to_send.append(TransOrder(
                                        side=d,
                                        p=float_prc,
                                        q=q_in_ccy2 / ref_prc,
                                        iid=".".join((_ex, contract)),
                                        floor=0,
                                        delta=ref_prc - float_prc,
                                        maker_taker=MakerTaker.maker
                                    ))
                                    float_prc -= ref_prc * quote_lvl_cfg.RETURN_STEP_SIZE
                            else:
                                pass
                        # fill back
                        if not self._should_quote_direction(quote_direction=d, quantity_to_quote=q_in_ccy2):
                            pass
                        else:
                            our_back_bid_int_prc = self._mm_order_manager_map[_ex].bid_ladder.keys()[-1]
                            if back_bid_int_prc < our_back_bid_int_prc:
                                our_back_bid_float_prc = self._mm_order_manager_map[_ex].int_prc_to_float_prc(int_prc=our_back_bid_int_prc)
                                float_prc = min(our_back_bid_float_prc - ref_prc * quote_lvl_cfg.RETURN_STEP_SIZE, best_bid.prc)
                                while float_prc > back_bid_float_prc - self._numeric_tol:
                                    mm_orders_to_send.append(TransOrder(
                                        side=d,
                                        p=float_prc,
                                        q=q_in_ccy2 / ref_prc,
                                        iid=".".join((_ex, contract)),
                                        floor=0,
                                        delta=ref_prc - float_prc,
                                        maker_taker=MakerTaker.maker
                                    ))
                                    float_prc -= ref_prc * quote_lvl_cfg.RETURN_STEP_SIZE
                            else:
                                pass

                elif d == Direction.short:
                    best_ask = self._depth_map[_ex].asks[0]
                    best_ask_int_prc = self._mm_order_manager_map[_ex].float_prc_to_int_prc(float_prc=best_ask.prc)
                    back_ask_float_prc = max(best_ask.prc, ref_prc * math.exp(quote_lvl_cfg.RETURN_UB))
                    back_ask_int_prc = self._mm_order_manager_map[_ex].float_prc_to_int_prc(float_prc=back_ask_float_prc)
                    s_s = math.log(best_ask.prc/ref_prc)
                    """ Manager order vector """
                    if len(self._mm_order_manager_map[_ex].ask_ladder) == 0:
                        if not self._should_quote_direction(quote_direction=d, quantity_to_quote=q_in_ccy2):
                            pass
                        else:
                            # mm order
                            quote_rets = np.arange(s_s, quote_lvl_cfg.RETURN_UB, quote_lvl_cfg.RETURN_STEP_SIZE)
                            for i, r in enumerate(quote_rets):
                                p = ref_prc * math.exp(r)
                                mm_orders_to_send.append(TransOrder(
                                    side=d,
                                    p=p,
                                    q=q_in_ccy2 / ref_prc,
                                    iid=".".join((_ex, contract)),
                                    floor=0,
                                    delta=p - ref_prc,
                                    maker_taker=MakerTaker.maker
                                ))
                    else:
                        # cancel front
                        for int_prc, oid_set in self._mm_order_manager_map[_ex].ask_ladder.items():
                            if len(oid_set) == 0:
                                self._mm_order_manager_map[_ex].bid_ladder.pop(int_prc)
                                continue
                            if int_prc < best_ask_int_prc:
                                mm_oids_to_cancel |= oid_set
                            else:
                                break
                        # cancel back
                        for int_prc, oid_set in reversed(self._mm_order_manager_map[_ex].ask_ladder.items()):
                            if len(oid_set) == 0:
                                self._mm_order_manager_map[_ex].bid_ladder.pop(int_prc)
                                continue
                            if int_prc > back_ask_int_prc:
                                mm_oids_to_cancel |= oid_set
                            else:
                                break
                        # fill front
                        if not self._should_quote_direction(quote_direction=d, quantity_to_quote=q_in_ccy2):
                            pass
                        else:
                            our_best_ask_int_prc = self._mm_order_manager_map[_ex].ask_ladder.keys()[0]
                            if best_ask_int_prc < our_best_ask_int_prc:
                                our_best_ask_float_prc = self._mm_order_manager_map[_ex].int_prc_to_float_prc(int_prc=our_best_ask_int_prc)
                                float_prc = best_ask.prc
                                while (float_prc < our_best_ask_float_prc - self._numeric_tol) and (float_prc < back_ask_float_prc + self._numeric_tol):
                                    mm_orders_to_send.append(TransOrder(
                                        side=d,
                                        p=float_prc,
                                        q=q_in_ccy2 / ref_prc,
                                        iid=".".join((_ex, contract)),
                                        floor=0,
                                        delta=float_prc - ref_prc,
                                        maker_taker=MakerTaker.maker
                                    ))
                                    float_prc += ref_prc * quote_lvl_cfg.RETURN_STEP_SIZE
                            else:
                                pass
                        # fill back
                        if not self._should_quote_direction(quote_direction=d, quantity_to_quote=q_in_ccy2):
                            pass
                        else:
                            our_back_ask_int_prc = self._mm_order_manager_map[_ex].ask_ladder.keys()[-1]
                            if back_ask_int_prc > our_back_ask_int_prc:
                                our_back_ask_float_prc = self._mm_order_manager_map[_ex].int_prc_to_float_prc(int_prc=our_back_ask_int_prc)
                                float_prc = max(our_back_ask_float_prc + ref_prc * quote_lvl_cfg.RETURN_STEP_SIZE, best_ask.prc)
                                while float_prc < back_ask_float_prc + self._numeric_tol:
                                    mm_orders_to_send.append(TransOrder(
                                        side=d,
                                        p=float_prc,
                                        q=q_in_ccy2 / ref_prc,
                                        iid=".".join((_ex, contract)),
                                        floor=0,
                                        delta=float_prc - ref_prc,
                                        maker_taker=MakerTaker.maker
                                    ))
                                    float_prc += ref_prc * quote_lvl_cfg.RETURN_STEP_SIZE
                            else:
                                pass
                else:
                    raise RuntimeError()

            mm_orders_to_send_map[_ex] = mm_orders_to_send
            mm_oids_to_cancel_map[_ex] = mm_oids_to_cancel

        return mm_orders_to_send_map, mm_oids_to_cancel_map, data

    def get_hedge_orders(self):
        I_ref_prc = self.get_xex_ref_prc(ex=self._xex)
        xex_I_in_ccy1 = self.inventory_manager.total_inventory
        xex_I_in_ccy2 = xex_I_in_ccy1 * I_ref_prc
        hedge_oids_to_cancel_map = defaultdict(lambda: set())
        hedge_orders_to_send_map = defaultdict(lambda: list())
        for _ex in self.active_exs:
            hedge_oids_to_cancel = hedge_oids_to_cancel_map[_ex]
            hedge_orders_to_send = hedge_orders_to_send_map[_ex]
            ref_prc = I_ref_prc
            ex_I_in_ccy1 = self.inventory_manager.get_inventory(contract=self.contract, ex=_ex)
            ex_I_in_ccy2 = ex_I_in_ccy1 * I_ref_prc
            I_in_ccy2 = xex_I_in_ccy2
            if I_in_ccy2 < 0:
                best_bid = self._depth_map[_ex].bids[0]
                best_bid_int_prc = self._hedge_order_manager_map[_ex].float_prc_to_int_prc(float_prc=best_bid.prc)
                # check if hedge orders are where they should be
                if (self._hedge_order_manager_map[_ex].hedge_orders_side is Ellipsis) or (
                        self._hedge_order_manager_map[_ex].hedge_orders_side == Direction.long):
                    if (self._hedge_order_manager_map[_ex].hedge_orders_int_prc is Ellipsis) or (
                            self._hedge_order_manager_map[_ex].hedge_orders_int_prc == best_bid_int_prc):
                        result_hedge_qty_in_ccy2 = self._hedge_order_manager_map[_ex].hedge_order_total_unfilled_qty * ref_prc
                        for hedge_oid in reversed(self._hedge_order_manager_map[_ex].hedge_order_oids):
                            if result_hedge_qty_in_ccy2 > -I_in_ccy2 + self.inven_tol:
                                if self._hedge_order_manager_map[_ex].trans_order_manager.is_order_canceled(oid=hedge_oid):
                                    continue
                                # we will be here if over hedged
                                hedge_oids_to_cancel.add(hedge_oid)
                                if self._hedge_order_manager_map[_ex].msg_order_manager.contains_order(oid=hedge_oid):
                                    o = self._hedge_order_manager_map[_ex].msg_order_manager.get_order(oid=hedge_oid)
                                    result_hedge_qty_in_ccy2 -= (o.qty - o.filled_qty) * ref_prc
                                elif self._hedge_order_manager_map[_ex].trans_order_manager.contains_order(oid=hedge_oid):
                                    o = self._hedge_order_manager_map[_ex].trans_order_manager.get_order(oid=hedge_oid)
                                    result_hedge_qty_in_ccy2 -= o.quantity * ref_prc
                                else:
                                    raise RuntimeError()
                                continue
                            break
                        hedge_qty_gap_in_ccy2 = -I_in_ccy2 - result_hedge_qty_in_ccy2
                        if hedge_qty_gap_in_ccy2 > self.inven_tol:
                            # we will be here if under hedged
                            hedge_orders_to_send.append(TransOrder(
                                side=Direction.long,
                                p=best_bid.prc,
                                q=hedge_qty_gap_in_ccy2 / ref_prc,
                                iid=".".join((_ex, self.contract)),
                                floor=1,
                                delta=best_bid.prc - ref_prc,
                                maker_taker=MakerTaker.maker
                            ))
                    else:
                        hedge_oids_to_cancel |= self._hedge_order_manager_map[_ex].hedge_order_oids
                        if -I_in_ccy2 > self.inven_tol:
                            hedge_orders_to_send.append(TransOrder(
                                side=Direction.long,
                                p=best_bid.prc,
                                q=-I_in_ccy2 / ref_prc,
                                iid=".".join((_ex, self.contract)),
                                floor=1,
                                delta=best_bid.prc - ref_prc,
                                maker_taker=MakerTaker.maker
                            ))
                else:
                    hedge_oids_to_cancel |= self._hedge_order_manager_map[_ex].hedge_order_oids
                    if -I_in_ccy2 > self.inven_tol:
                        hedge_orders_to_send.append(TransOrder(
                            side=Direction.long,
                            p=best_bid.prc,
                            q=-I_in_ccy2 / ref_prc,
                            iid=".".join((_ex, self.contract)),
                            floor=1,
                            delta=best_bid.prc - ref_prc,
                            maker_taker=MakerTaker.maker
                        ))
            elif I_in_ccy2 > 0:
                best_ask = self._depth_map[_ex].asks[0]
                best_ask_int_prc = self._hedge_order_manager_map[_ex].float_prc_to_int_prc(float_prc=best_ask.prc)
                # check if hedge orders are where they should be
                if (self._hedge_order_manager_map[_ex].hedge_orders_side is Ellipsis) or (
                        self._hedge_order_manager_map[_ex].hedge_orders_side == Direction.short):
                    if (self._hedge_order_manager_map[_ex].hedge_orders_int_prc is Ellipsis) or (
                            self._hedge_order_manager_map[_ex].hedge_orders_int_prc == best_ask_int_prc):
                        result_hedge_qty_in_ccy2 = self._hedge_order_manager_map[_ex].hedge_order_total_unfilled_qty * ref_prc
                        for hedge_oid in reversed(self._hedge_order_manager_map[_ex].hedge_order_oids):
                            if result_hedge_qty_in_ccy2 > I_in_ccy2 + self.inven_tol:
                                # we will be here if over hedged
                                if self._hedge_order_manager_map[_ex].trans_order_manager.is_order_canceled(oid=hedge_oid):
                                    continue
                                hedge_oids_to_cancel.add(hedge_oid)
                                if self._hedge_order_manager_map[_ex].msg_order_manager.contains_order(oid=hedge_oid):
                                    o = self._hedge_order_manager_map[_ex].msg_order_manager.order_at(oid=hedge_oid)
                                    result_hedge_qty_in_ccy2 -= (o.qty - o.filled_qty) * ref_prc
                                elif self._hedge_order_manager_map[_ex].trans_order_manager.contains_order(oid=hedge_oid):
                                    o = self._hedge_order_manager_map[_ex].trans_order_manager.order_at(oid=hedge_oid)
                                    result_hedge_qty_in_ccy2 -= o.quantity * ref_prc
                                else:
                                    raise RuntimeError()
                                continue
                            break
                        hedge_qty_gap_in_ccy2 = I_in_ccy2 - result_hedge_qty_in_ccy2
                        if hedge_qty_gap_in_ccy2 > self.inven_tol:
                            # we will be here if under hedged
                            hedge_orders_to_send.append(TransOrder(
                                side=Direction.short,
                                p=best_ask.prc,
                                q=hedge_qty_gap_in_ccy2 / ref_prc,
                                iid=".".join((_ex, self.contract)),
                                floor=1,
                                delta=best_ask.prc - ref_prc,
                                maker_taker=MakerTaker.maker
                            ))
                    else:
                        hedge_oids_to_cancel |= self._hedge_order_manager_map[_ex].hedge_order_oids
                        if I_in_ccy2 > self.inven_tol:
                            hedge_orders_to_send.append(TransOrder(
                                side=Direction.short,
                                p=best_ask.prc,
                                q=I_in_ccy2 / ref_prc,
                                iid=".".join((_ex, self.contract)),
                                floor=1,
                                delta=best_ask.prc - ref_prc,
                                maker_taker=MakerTaker.maker
                            ))
                else:
                    hedge_oids_to_cancel |= self._hedge_order_manager_map[_ex].hedge_order_oids
                    if I_in_ccy2 > self.inven_tol:
                        hedge_orders_to_send.append(TransOrder(
                            side=Direction.short,
                            p=best_ask.prc,
                            q=I_in_ccy2 / ref_prc,
                            iid=".".join((_ex, self.contract)),
                            floor=1,
                            delta=best_ask.prc - ref_prc,
                            maker_taker=MakerTaker.maker
                        ))
            else:
                hedge_oids_to_cancel |= self._hedge_order_manager_map[_ex].hedge_order_oids
            hedge_oids_to_cancel_map[_ex] = hedge_oids_to_cancel
            hedge_orders_to_send_map[_ex] = hedge_orders_to_send
        return hedge_orders_to_send_map, hedge_oids_to_cancel_map