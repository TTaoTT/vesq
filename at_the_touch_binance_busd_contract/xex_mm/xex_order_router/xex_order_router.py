from decimal import Decimal
from typing import Dict

import numpy as np

from xex_mm.utils.enums import Direction
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_order_router.fill_probability_engine import TradingVolumeFillScoreEngine


class XExRouter:
    def __init__(self):
        self.__xex_depth_map: Dict[str, XExDepth] = {}

    def get_destination_exchange(self, symbol: str, price: Decimal, quantity: Decimal, direction: Direction) -> str:
        xex_depth = self.__xex_depth_map[symbol]
        engine = TradingVolumeFillScoreEngine()
        min_score = np.inf
        min_score_ex = ...
        for ex_name in xex_depth.get_ex_names():
            depth = xex_depth.get_depth_for_ex(ex=ex_name)
            _24h_trade_vol = xex_depth.get_24h_trade_vol_for_ex(ex_name=ex_name)
            score = engine.calc_score(depth=depth, trade_vol=_24h_trade_vol,
                                      price=price, quantity=quantity, direction=direction)
            if score < min_score:
                min_score = score
                min_score_ex = ex_name
        if min_score_ex is Ellipsis:
            raise RuntimeError("No exchanges?")
        return min_score_ex

    def update_xex_depth(self, symbol: str, xex_depth: XExDepth):
        self.__xex_depth_map[symbol] = xex_depth
