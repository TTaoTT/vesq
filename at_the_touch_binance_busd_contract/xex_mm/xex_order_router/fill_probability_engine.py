from decimal import Decimal

from xex_mm.utils.base import Depth
from xex_mm.utils.enums import Direction


class TradingVolumeFillScoreEngine:
    def calc_score(self, depth: Depth, trade_vol: Decimal, price: Decimal, quantity: Decimal, direction: Direction) -> Decimal:
        if direction == Direction.long:
            qty_needed = 0
            for bid in depth.bids:
                if bid.prc >= price:
                    qty_needed += bid.qty
                    continue
                break
            qty_needed += quantity
            return qty_needed / trade_vol

        if direction == Direction.short:
            qty_needed = 0
            for ask in depth.asks:
                if ask.prc <= price:
                    qty_needed += ask.qty
                    continue
                break
            qty_needed += quantity
            return qty_needed / trade_vol

        raise NotImplementedError()
