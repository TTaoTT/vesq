import math
from decimal import Decimal
from enum import Enum

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from hf.utils.PriceDistribution import VolumeCache, SpreadCalculatorLite
from xex_mm.pnl_analyzer.utils import PositionBuilder
from xex_mm.utils.data import DataLoader


class Side(Enum):
    bid = 0
    ask = 1


class DepthFields:
    ask1_prc = "ask_price"
    bid1_prc = "bid_price"


class PnLAnalyzer:
    def _calc_mid_price(self, depth: pd.DataFrame) -> pd.Series:
        """
        :param depth: Should have at least 2 columns: ask1_prc, bid1_prc
        :return: mid_prc
        """
        return (depth[DepthFields.ask1_prc] + depth[DepthFields.bid1_prc]) / 2

    def _calc_zero_aggression_spread(self, pub_trd: pd.DataFrame, trd_sprd: pd.DataFrame, mid_prc: pd.Series) -> pd.Series:
        """
        :param trd_sprd: has dup index
        """

        vc = VolumeCache()
        scl = SpreadCalculatorLite()
        a = 2.7e-7
        b = 0.0002
        trd_sprd_ts = pd.Series(index=trd_sprd.index.unique())
        mid_prc.name = "mid_prc"
        df = pd.concat([pub_trd, mid_prc, trd_sprd_ts], axis=1).ffill().loc[trd_sprd_ts.index, :]
        df.index.name = "tx_time"
        sprds = []
        for trd_dct in trd_sprd.reset_index().to_dict(orient="record"):
            state = df.loc[trd_dct["tx_time"], :]
            _trd_dct = dict(
                t=trd_dct["tx_time"],
                p=Decimal(float(state['price'])),
                q=Decimal(float(state['qty'])),
            )
            vc.feed_trade(trade=_trd_dct)
            vc.remove_expired(ts=_trd_dct["t"])
            amt = vc.get_amount()
            keppa = 1 / (a * math.sqrt(amt) + b)
            mp = state["mid_prc"]
            if trd_dct['sign'] == 1:
                sprds.append(scl.get_bid(inven=0, keppa=keppa) * mp)
            else:
                sprds.append(scl.get_ask(inven=0, keppa=keppa) * mp)
        return pd.Series(sprds, index=trd_sprd.index, name="zero_agg_sprd")

    def _calc_trade_spread(self, mid_prc: pd.Series, own_trd: pd.DataFrame) -> pd.DataFrame:
        """
        :param mid_prc: Self explanatory
        :param own_trd: 2 columns:
            trd_prc: Self expanatory
            trd_dir: int, binary, 1 means long, -1 means short (from our own perspective)
            has dup index.
        :return: has dup time [-inf, inf]
        """
        own_trd_ts = pd.Series(index=own_trd.index.unique())
        mid_prc = pd.concat([mid_prc, own_trd_ts], axis=1).ffill().loc[own_trd_ts.index, mid_prc.name]
        trd_sprds = []
        for dct in own_trd.reset_index().to_dict(orient="record"):
            mp = mid_prc.loc[dct["tx_time"], ]
            trd_sprd = (mp - dct["price"]) * dct["sign"]
            trd_sprds.append((trd_sprd, dct["sign"], dct["qty"]))
        return pd.DataFrame(trd_sprds, columns=["trd_sprd", "sign", "qty"], index=own_trd.index)

    ############################################ API ##############################################

    def calc_market_pnl(self, mid_price: pd.Series, position: pd.Series):
        """
        :param mid_price: Self explanatory.
        :param position: Self explanatory.
        :return: Incremental market/inventory pnl
        """
        mid_price.name = "mid_price"
        position.name = "position"
        df = pd.concat([mid_price, position], axis=1).ffill().loc[position.index, :]
        return df[mid_price.name].diff() * df[position.name].shift()

    def calc_aggression_pnl(self, traded_sprd: pd.DataFrame, zero_agg_sprd: pd.Series):
        """
        :param traded_sprd: Actual fills' price spread from the mid price at the moment. Has hup index
        :param zero_agg_sprd: Zero-aggression spread from model at the moment. Has dup index
        :param trade_qty:  Quantity filled at the moment.
        :return: PnL due to being aggressive (for hedging or alpha capture).
        """

        assert np.all(traded_sprd.index == zero_agg_sprd.index)
        agg_pnl = []
        for i in range(traded_sprd.shape[0]):
            t_sprd = traded_sprd.iloc[i, :]
            za_sprd = zero_agg_sprd.iloc[i]
            agg_pnl.append((t_sprd["trd_sprd"] - za_sprd) * t_sprd["qty"])
        return pd.Series(agg_pnl, index=traded_sprd.index, name="aggression_pnl")

    def calc_spread_pnl(self, traded_sprd: pd.DataFrame, zero_agg_sprd: pd.Series):
        assert np.all(traded_sprd.index == zero_agg_sprd.index)
        sprd_pnl = []
        for i in range(traded_sprd.shape[0]):
            t_sprd = traded_sprd.iloc[i, :]
            za_sprd = zero_agg_sprd.iloc[i]
            sprd_pnl.append(za_sprd * t_sprd["qty"])
        return pd.Series(sprd_pnl, index=traded_sprd.index, name="spread_pnl")

    def calc_makers_rebate_pnl(self, own_trd: pd.DataFrame, makers_rebate_rate: float):
        maker_trds = own_trd[own_trd["maker"]]
        return maker_trds["price"] * maker_trds["qty"].to_numpy() * makers_rebate_rate

    def calc_takers_fee_pnl(self, own_trd: pd.DataFrame, takers_fee_rate: float):
        taker_trds = own_trd[~own_trd["maker"]]
        return - taker_trds["price"] * taker_trds["qty"].to_numpy() * takers_fee_rate

    def calc_capital_pnl(self):
        raise NotImplementedError()

    def calc_market_response(self):
        raise NotImplementedError()

    def calc_pnl(self, own_trd: pd.DataFrame, depth: pd.DataFrame) -> pd.Series:
        long_amt = 0
        short_amt = 0
        long_qty = 0
        short_qty = 0
        long_avg_prc = np.nan
        short_avg_prc = np.nan
        own_trd_ts = pd.Series(index=own_trd.index.unique())
        depth = pd.concat([depth, own_trd_ts], axis=1).ffill().loc[own_trd_ts.index, :]
        pnls = []
        for dct in own_trd.reset_index().to_dict(orient="record"):
            depth_state = depth.loc[dct["tx_time"], :]
            if dct["side"] == "BUY":
                long_amt += dct["price"] * dct["qty"]
                long_qty += dct["qty"]
                long_avg_prc = long_amt / long_qty
            else:
                short_amt += dct["price"] * dct["qty"]
                short_qty += dct["qty"]
                short_avg_prc = short_amt / short_qty

            if (long_qty > 0) and (short_qty == 0):
                pnl = long_qty * (depth_state[DepthFields.bid1_prc] - long_avg_prc)
            elif (long_qty == 0) and (short_qty > 0):
                pnl = long_qty * (short_avg_prc - depth_state[DepthFields.ask1_prc])
            elif long_qty > short_qty:
                pnl = short_qty * (short_avg_prc - long_avg_prc) + (long_qty - short_qty) * (depth_state[DepthFields.bid1_prc] - long_avg_prc)
            elif short_qty > long_qty:
                pnl = long_qty * (short_avg_prc - long_avg_prc) + (short_qty - long_qty) * (short_avg_prc - depth_state[DepthFields.ask1_prc])
            else:
                pnl = long_qty * (short_avg_prc - long_avg_prc)
            pnls.append(pnl)
        return pd.Series(pnls, index=own_trd.index, name="pnl")


def test():
    pa = PnLAnalyzer()

    o = DataLoader()
    date = 20220705
    params = dict(
        ex="binance",
        pair="eth_usdt_swap",
        date=date,
    )
    ob = o.read_bbo(**params)
    pub_trd = o.read_trades(**params)
    filename = "transactions_0.5_1ht_no_risk_control_mp"
    own_trd = o.read_own_trades_bt(filename=filename, date=str(date))

    pos = PositionBuilder().build(trades=own_trd)

    # pnl = pa.calc_pnl(own_trd=own_trd, depth=ob)
    # pnl.plot()
    # plt.show()

    mid_prc = pa._calc_mid_price(depth=ob)
    mkt_pnl = pa.calc_market_pnl(
        mid_price=mid_prc,
        position=pos,
    )
    trd_sprd = pa._calc_trade_spread(
        mid_prc=mid_prc,
        own_trd=own_trd,
    )
    zero_agg_sprd = pa._calc_zero_aggression_spread(
        pub_trd=pub_trd,
        trd_sprd=trd_sprd,
        mid_prc=mid_prc,
    )
    agg_pnl = pa.calc_aggression_pnl(
        traded_sprd=trd_sprd,
        zero_agg_sprd=zero_agg_sprd,
    )
    sprd_pnl = pa.calc_spread_pnl(traded_sprd=trd_sprd, zero_agg_sprd=zero_agg_sprd)
    makers_rebate = pa.calc_makers_rebate_pnl(own_trd=own_trd, makers_rebate_rate=0.000005)
    takers_fee = pa.calc_takers_fee_pnl(own_trd=own_trd, takers_fee_rate=0.00003)

    mkt_pnl.name = "mkt_pnl"
    agg_pnl.name = "agg_pnl"
    sprd_pnl.name = "sprd_pnl"
    makers_rebate.name = "makers_rebate"
    takers_fee.name = "takers_fee"
    mkt_pnl = mkt_pnl.cumsum()
    agg_pnl = agg_pnl.cumsum()
    sprd_pnl = sprd_pnl.cumsum()
    makers_rebate = makers_rebate.cumsum()
    takers_fee = takers_fee.cumsum()
    agg_pnl = agg_pnl.loc[~agg_pnl.index.duplicated(keep="last")]
    sprd_pnl = sprd_pnl.loc[~sprd_pnl.index.duplicated(keep="last")]
    makers_rebate = makers_rebate.loc[~makers_rebate.index.duplicated(keep="last")]
    takers_fee = takers_fee.loc[~takers_fee.index.duplicated(keep="last")]
    pnls = pd.concat([
        mkt_pnl,
        agg_pnl,
        sprd_pnl,
        makers_rebate,
        takers_fee,
    ], axis=1).ffill()
    pnls["sum_component_pnl"] = pnls.sum(axis=1)
    pnls.plot()
    plt.show()

    # pd.concat([pnl, pnls["sum_component_pnl"]], axis=1).ffill().plot()
    # plt.show()


if __name__ == '__main__':
    test()