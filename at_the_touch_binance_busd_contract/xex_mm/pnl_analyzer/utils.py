import pandas as pd

import matplotlib.pyplot as plt

from xex_mm.utils.data import DataLoader


class PositionBuilder:
    def build(self, trades: pd.DataFrame) -> pd.Series:
        pos_ser: pd.Series = (trades["sign"] * trades["qty"].to_numpy()).cumsum()
        return pos_ser.loc[~pos_ser.index.duplicated(keep="last")]


if __name__ == '__main__':
    o = DataLoader()
    # df = o.read_trade(
    #     ex="binance",
    #     pair="eth_usdt_swap",
    #     date=20220630,
    # )
    # print(df)
    filename = "binance.mktbinance25_ETHUSDT_1656720000000_1656806400000_user_trades"
    df = o.read_own_trades(filename=filename)
    pb = PositionBuilder().build(trades=df)
    print(pb)
    pb.plot()
    plt.show()
