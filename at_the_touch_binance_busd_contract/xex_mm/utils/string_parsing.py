
def contract_to_ccys(contract: str):
    tup = contract.split("_")
    if len(tup) == 2:
        ccy1, ccy2 = tup
    elif len(tup) == 3:
        ccy1, ccy2, contract_type = tup
    else:
        raise NotImplementedError()
    return ccy1, ccy2