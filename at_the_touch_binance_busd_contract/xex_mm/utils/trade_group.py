import math
import os
from dataclasses import dataclass
from datetime import datetime
from typing import List

import pandas as pd
import tables

from xex_mm.utils.base import Trade


@dataclass
class TrdGrp:
    ttl_qty: float
    max_prc: float
    min_prc: float
    start_ts: int
    end_ts: int
    trd_grp: List[Trade]

    def record(self):
        data = self.__dict__.copy()
        data.pop("trd_grp")
        return data


class TrdGrpTracker:
    def __init__(self):
        self._trd_grps: List[List[TrdGrp]] = [[], []]
        self._trd_grp: List[List[Trade]] = [[], []]
        self._ttl_qty = [0., 0.]
        self._max_prc = [0., 0.]
        self._min_prc = [math.inf, math.inf]
        self._start_ts = [..., ...]
        self._end_ts = [..., ...]
        self._last_ts = [..., ...]
        self._grp_ts_tol = 3  # ms

    def _add_trd_to_grp(self, trd: Trade):
        side_idx = {1:0, -1:1}[trd.side]
        self._trd_grp[side_idx].append(trd)
        self._ttl_qty[side_idx] += trd.quantity
        self._max_prc[side_idx] = max(self._max_prc[side_idx], trd.price)
        self._min_prc[side_idx] = min(self._min_prc[side_idx], trd.price)
        self._last_ts[side_idx] = trd.local_time
        if self._start_ts[side_idx] is Ellipsis:
            self._start_ts[side_idx] = trd.local_time
        self._end_ts[side_idx] = trd.local_time

    def _reset_trd_grp(self, side_idx: int):
        self._trd_grp[side_idx] = []
        self._ttl_qty[side_idx] = 0
        self._max_prc[side_idx] = 0
        self._min_prc[side_idx] = math.inf
        self._start_ts[side_idx] = ...
        self._end_ts[side_idx] = ...

    def on_trade(self, trd: Trade):
        side_idx = {1:0, -1:1}[trd.side]
        if self._last_ts[side_idx] is Ellipsis:
            self._last_ts[side_idx] = trd.local_time
        if trd.local_time - self._last_ts[side_idx] > self._grp_ts_tol:
            self._trd_grps[side_idx].append(TrdGrp(
                ttl_qty=self._ttl_qty[side_idx],
                max_prc=self._max_prc[side_idx],
                min_prc=self._min_prc[side_idx],
                trd_grp=self._trd_grp[side_idx],
                start_ts=self._start_ts[side_idx],
                end_ts=self._end_ts[side_idx],
            ))
        self._add_trd_to_grp(trd=trd)

    def export(self):
        data = []
        for side_idx, trg_grps_for_side in enumerate(self._trd_grps):
            for trd_grp in trg_grps_for_side:
                trd_grp_dct = trd_grp.record()
                trd_grp_dct["side"] = [1, -1][side_idx]
                data.append(trd_grp_dct)
        return pd.DataFrame(data)


class TrdDfGrper:
    def __init__(self):
        self._trd_grp_tracker = TrdGrpTracker()

    def get_trades(self, contract: str, ex: str, dt: str, refresh: bool = False):
        feather_cache_dir = "/home/xinge/user_storage/yy/feather_cache"
        feather_path = f"{feather_cache_dir}/trades_df/{contract}/{ex}/{dt}.feather"
        if not refresh and os.path.exists(feather_path):
            return pd.read_feather(feather_path).set_index("datetime")

        file_path_raw = f"/home/xinge/hf_data/raw_data/{dt}/{ex}.{contract}_{dt}.h5"

        with tables.open_file(file_path_raw, "r") as tfile:
            dname = "/config"
            dnode = tfile.get_node(dname)
            pf = dnode.col("price_multiplier")[0]
            cv = dnode.col("contract_value")[0]
            try:
                qf = dnode.col("qty_multiplier")[0]
            except:
                qf = 1

        f = tables.File(file_path_raw, mode="r")

        df_trade = pd.DataFrame(f.get_node("/agg_trade")[:])
        print(df_trade.shape)

        df_trade['datetime'] = df_trade['localtime'].map(lambda x: datetime.fromtimestamp(x / 1e3))
        df_trade["price"] = df_trade["price"] / pf
        df_trade["qty"] = df_trade["qty"] / qf * cv
        df_trade.set_index("datetime", inplace=True)

        f.close()

        os.makedirs(os.path.dirname(feather_path), exist_ok=True)
        df_trade.reset_index().to_feather(feather_path)

        return df_trade

    def grp(self, contract: str, ex: str, trd_df: pd.DataFrame):
        for trd_dct in trd_df.to_records():
            trd = Trade(
                ex=ex,
                contract=contract,
                price=trd_dct["price"],
                quantity=trd_dct["qty"],
                tx_time=trd_dct["tx_time"],
                local_time=trd_dct["localtime"],
                side={1:1, 2:-1}[trd_dct["side"]],
                mexc_side=0,
                uid=trd_dct["trade_id"],
            )
            self._trd_grp_tracker.on_trade(trd=trd)

        return self._trd_grp_tracker.export()

    def run(self, contract: str, ex: str, dt: str):
        trd_df = self.get_trades(contract=contract, ex=ex, dt=dt)
        df = self.grp(contract=contract, ex=ex, trd_df=trd_df)
        return df


if __name__ == '__main__':
    contract = "btc_usdt_swap"
    ex = "binance"
    dt = "20220819"

    df = TrdDfGrper().run(contract=contract, ex=ex, dt=dt)
    print(df)
