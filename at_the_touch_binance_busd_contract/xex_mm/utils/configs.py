from pathlib import Path
from typing import Tuple

from xex_mm.utils.contract_mapping import ContractMapper
from xex_mm.utils.enums import MakerTaker

"""
binance: 9B USDT / 24h
mexc: ??? / 24h
okex: 1.6B USDT / 24h
woox: 30M USDT / 24h
gateio: 300M USDT / 24h
crypto: 65M USDT / 24h
"""


class PathConfig:
    user_path = f"{Path().home()}/user_storage/yy"  # Change this
    data_path = f"{user_path}/data"
    strategy_config_path = f"{user_path}/strategy_configs"
    
class FeeStructure:
    def __init__(self, maker: float, taker: float) -> None:
        self.maker = maker
        self.taker = taker


class FeeConfig:
    def __init__(self) -> None:
        self.contract_mapper = ContractMapper()
        self.config = {
            # BINANCE
            "binance.btc_usdt": FeeStructure(maker=0, taker=0),
            "binance.axs_usdt_swap": FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.luna_usdt_swap": FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.btc_usdt_swap": FeeStructure(maker=-0.00004, taker=0.0003),
            "binance.eth_usdt_swap": FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.btc_busd_swap': FeeStructure(maker=-0.00014, taker=0.0003),
            'binance.eth_busd_swap': FeeStructure(maker=-0.00014, taker=0.0003),
            'binance.doge_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.1000lunc_busd_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.luna2_busd_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.luna2_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.etc_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.sol_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.reef_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.ape_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.ape_usdt': FeeStructure(maker=0.0007, taker=0.001),
            'binance.chz_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.chz_usdt': FeeStructure(maker=0.0007, taker=0.001),
            'binance.hnt_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.band_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.mask_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            # MEXC
            "mexc.axs_usdt_swap":FeeStructure(maker=-0.0001, taker=0.0001),
            "mexc.luna_usdt_swap":FeeStructure(maker=-0.0001, taker=0.0001),
            "mexc.btc_usdt":FeeStructure(maker=-0.0001, taker=0.0005),
            # WOO
            "woo.btc_usdt_swap": FeeStructure(maker=0, taker=0),
            # GATEIOcd ~hf
            "gateio.btc_usdt": FeeStructure(maker=0, taker=0),  # VIP MM2
            "gateio.btc_usdt_swap": FeeStructure(maker=-0.000125, taker=0.000225),  # VIP MM2
            # OKEX
            "okex.btc_usdt": FeeStructure(maker=0, taker=0.0002), # TODO maker sure this is right
            "okex.btc_usdt_swap": FeeStructure(maker=0, taker=0.0002),  # vip5 ???
            "okex.ape_usdt_swap": FeeStructure(maker=0, taker=0.0002),  # vip5 ???
            "okex.chz_usdt_swap": FeeStructure(maker=0, taker=0.0002),  # vip5 ???
            # CRYPTO, same for all token pairs
            "crypto.btc_usdt": FeeStructure(maker=-0.00003, taker=0.00025),
            "crypto.btc_usdt_swap": FeeStructure(maker=-0.00002, taker=0.00015),
            # JOJO
            "jojo.btc_usdt_swap": FeeStructure(maker=-0.0002, taker=0.001),

        }
    
    def get_fee(self, ex: str, contract: str):
        contract, ex = self.contract_mapper.proxy_CE_to_CE(proxy_contract=contract, proxy_ex=ex)
        return self.config[f"{ex}.{contract}"].maker, self.config[f"{ex}.{contract}"].taker

    def to_dict(self):
        dct = dict()
        for iid, fee_struct in self.config.items():
            iid = self.contract_mapper.iid_to_proxy_iid(iid=iid)
            ex, contract = iid.split(".")
            if contract not in dct:
                dct[contract] = dict()
            dct[contract][ex] = {
                MakerTaker.maker: fee_struct.maker,
                MakerTaker.taker: fee_struct.taker,
            }
        return dct


class FundingCostConfig:
    def __init__(self) -> None:
        self.contract_mapper = ContractMapper()
        self.config = dict()

    def get_funding_cost(self, ex: str, contract: str):
        contract, ex = self.contract_mapper.proxy_CE_to_CE(proxy_contract=contract, proxy_ex=ex)
        iid = f"{ex}.{contract}"
        return self.config.get(iid, 0)

    def to_dict(self):
        return self.__dict__


class FundingCompensationConfig:
    def __init__(self) -> None:
        self.contract_mapper = ContractMapper()
        self.config = dict()

    def get_funding_compensation(self, ex: str, contract: str):
        contract, ex = self.contract_mapper.proxy_CE_to_CE(proxy_contract=contract, proxy_ex=ex)
        iid = f"{ex}.{contract}"
        return self.config.get(iid, 0)

    def to_dict(self):
        return self.__dict__


class LatencyCostConfig:
    def __init__(self) -> None:
        self.contract_mapper = ContractMapper()
        self.config = dict()

    def get_latency_cost(self, ex: str, contract: str):
        contract, ex = self.contract_mapper.proxy_CE_to_CE(proxy_contract=contract, proxy_ex=ex)
        iid = f"{ex}.{contract}"
        return self.config.get(iid, 0)

    def to_dict(self):
        return self.__dict__


class HeartBeatTolerance:
    heart_beat_tol_map = {}

    def get_heart_beat_tol_by_ce(self, ex: str, contract: str) -> int:
        if self.heart_beat_tol_map.get(f"{ex}.{contract}") == None:
            return 1000
        return self.heart_beat_tol_map[f"{ex}.{contract}"]

    def get_heart_beat_tol_by_iid(self, iid: str) -> int:
        if self.heart_beat_tol_map.get(iid) == None:
            return 1000
        return self.heart_beat_tol_map[iid]


class LatencyBoundConfig:
    latency_bound_map = {}

    def get_latency_bound_by_iid(self, iid: str) -> Tuple[int, int]:  # (high_latency_bound, normal_latency_bound)
        if self.latency_bound_map.get(iid) == None:
            return (1000, 1000)
        return self.latency_bound_map[iid]


class DataStartDate:
    by_ex = {
        "crypto": "20220706",
        "gateio": "20220702",
        "woo": "20220818",
        "okex": "20220621",
    }

    def __init__(self):
        pass


    
price_precision = {
    'binance.btc_usdt': 2,
    'binance.btc_usdt_swap': 1,
    'binance.btc_busd_swap': 1,
    'binance.eth_usdt_swap': 2,
    'binance.eth_busd_swap': 2,
    'binance.axs_usdt_swap': 2,
    'binance.doge_usdt_swap': 5,
    'binance.1000lunc_busd_swap': 4,
    'binance.luna2_busd_swap': 4,
    'binance.luna2_usdt_swap': 4,
    'binance.etc_usdt_swap': 3,
    'binance.sol_usdt_swap': 2,
    'binance.reef_usdt_swap': 6,
    'binance.ape_usdt_swap':3,
    'binance.ape_usdt':3,
    'binance.chz_usdt_swap':5,
    'binance.chz_usdt':4,
    'binance.hnt_usdt_swap':3,
    'binance.band_usdt_swap':4,
    'binance.mask_usdt_swap':3,

    'mexc.btc_usdt': 2,
    'mexc.eth_usdt_swap': 2,
    'mexc.axs_usdt_swap': 5,

    "gateio.btc_usdt_swap": 1,
    "gateio.btc_usdt": 2,

    "okex.btc_usdt": 1,
    "okex.btc_usdt_swap": 1,
    "okex.ape_usdt_swap": 4,
    "okex.chz_usdt": 5,
    "okex.chz_usdt_swap": 5,

    "woo.btc_usdt_swap": 1,

    "crypto.btc_usdt_swap": 2,
    
    "jojo.btc_usdt_swap": 2
}


class PricePrecisionManager:
    def __init__(self):
        self.contract_mapper = ContractMapper()

    def get_price_precision_by_iid(self, iid: str) -> float:
        return price_precision[self.contract_mapper.proxy_iid_to_iid(proxy_iid=iid)]

    def get_price_precision_by_ce(self, contract: str, ex: str) -> float:
        iid = f"{ex}.{contract}"
        return self.get_price_precision_by_iid(iid=iid)


qty_precision = {
    'binance.btc_usdt': 5,
    'binance.btc_usdt_swap': 3,
    'binance.btc_busd_swap': 3,
    'binance.eth_usdt_swap': 3,
    'binance.eth_busd_swap': 3,
    'binance.axs_usdt_swap': 0,
    'binance.doge_usdt_swap': 1,
    'binance.1000lunc_busd_swap': 0,
    'binance.luna2_busd_swap': 0,
    'binance.luna2_usdt_swap': 0,
    'binance.etc_usdt_swap': 2,
    'binance.sol_usdt_swap': 0,
    'binance.reef_usdt_swap': 0,
    'binance.ape_usdt_swap':0,
    'binance.ape_usdt':2,
    'binance.chz_usdt_swap':0,
    'binance.chz_usdt':0,
    'binance.hnt_usdt_swap':0,
    'binance.band_usdt_swap':1,
    'binance.mask_usdt_swap':0,

    'mexc.btc_usdt': 6,
    'mexc.eth_usdt_swap': 2,
    'mexc.axs_usdt_swap': 0,

    "gateio.btc_usdt_swap": 4,
    "gateio.btc_usdt": 6,

    "okex.btc_usdt": 8,
    "okex.btc_usdt_swap": 3,
    "okex.ape_usdt_swap": 3,
    "okex.chz_usdt": 1,
    "okex.chz_usdt_swap": 1,

    "woo.btc_usdt_swap": 3,

    "crypto.btc_usdt_swap": 6,
    
    "jojo.btc_usdt_swap": 6
}


class QuantityPrecisionManager:
    def __init__(self):
        self.contract_mapper = ContractMapper()

    def get_quantity_precision_by_iid(self, iid: str) -> float:
        return qty_precision[self.contract_mapper.proxy_iid_to_iid(proxy_iid=iid)]

    def get_quantity_precision_by_ce(self, contract: str, ex: str) -> float:
        iid = f"{ex}.{contract}"
        return self.get_quantity_precision_by_iid(iid=iid)