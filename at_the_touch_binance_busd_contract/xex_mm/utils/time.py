import logging
import os.path
import time
from typing import Dict

import pandas as pd


class RuntimeTracker:
    def __init__(self):
        self._time_data = []

    def add(self, data: dict):
        self._time_data.append(data)
        if data["ellapsed"] > 100:
            logging.info(f"Runtime Tracker: {data}")

    def dump(self, path: str):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        data = pd.DataFrame(self._time_data)
        data.to_feather(path)
        logging.info(f"Runtime data wrote to: {path}")


grtt = RuntimeTracker()


class FuncTimer:
    def __init__(self, runtime_tracker: RuntimeTracker = grtt):
        self._runtime_tracker = runtime_tracker

    def __call__(self, func):
        def wrapper(*arg, **kw):
            '''source: http://www.daniweb.com/code/snippet368.html'''
            t1 = time.time()
            res = func(*arg, **kw)
            t2 = time.time()
            dt = (t2 - t1) * 1000.
            if isinstance(self._runtime_tracker, RuntimeTracker):
                self._runtime_tracker.add(data=dict(
                    func=func.__name__,
                    ellapsed=dt,
                ))
            else:
                logging.info(f"{func.__name__} took {dt} ms.")
            return res
        return wrapper


class ContextTimer:
    def __init__(self, runtime_tracker: RuntimeTracker = grtt, **meta_data):
        self.rtt = runtime_tracker
        self.meta_data = meta_data
        self.st = None
        self.et = None

    def __enter__(self):
        self.st = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.et = time.time()
        dt = (self.et - self.st) * 1000.
        if isinstance(self.rtt, RuntimeTracker):
            self.rtt.add(data=dict(
                ellapsed=dt,
                **self.meta_data
            ))