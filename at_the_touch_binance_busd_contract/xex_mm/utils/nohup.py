import logging
import os
import sys
import pathlib
from typing import List


def execute_command(cmd: str):
    os.system(f"echo {cmd}")
    os.system(cmd)


def nohup(
    script_path: str,
    out_dir: str = ...,
    interpreter_path: str = sys.executable,
):
    filename = pathlib.Path(script_path).stem
    if not isinstance(out_dir, str):
        out_dir = f"{pathlib.Path().home()}/nohup_out"
    os.makedirs(out_dir, exist_ok=True)
    out_path = f"{out_dir}/{filename}.out"
    execute_command(f"nohup {interpreter_path} -u {script_path} > {out_path} &")
    logging.info(f"Executed nohup command for {script_path}, out path {out_path}")


def batch_nohup(
    script_paths: List[str],
    out_dir: str = ...,
    interpreter_path: str = sys.executable,
):
    for path in script_paths:
        nohup(script_path=path, out_dir=out_dir, interpreter_path=interpreter_path)