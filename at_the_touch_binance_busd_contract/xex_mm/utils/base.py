import datetime
import logging
import math
from _operator import neg
from dataclasses import dataclass
from decimal import Decimal
from typing import List, Dict, Union, Tuple, Iterable
import numpy as np
from orderedset import OrderedSet
from sortedcontainers import SortedDict

from xex_mm.utils.configs import FeeConfig, FundingCostConfig, FundingCompensationConfig, LatencyCostConfig, \
    PricePrecisionManager, QuantityPrecisionManager
from atom.model.order import PartialOrder, OrderStatus, OrderSide
from atom.model.order import Order as AtomOrder
from atom.model.depth import BBODepth
from atom.model.trade import PublicTrade

from xex_mm.utils.contract_mapping import ContractMapper
from xex_mm.utils.enums import Direction, MakerTaker, DepthCcyType, DepthQtyGroupType, DepthPrcGroupType, \
    DepthAtTouchGuardType, DepthCompletenessState, DepthShadowState, DepthXExState, DepthTakerFeeState, \
    DepthPrcShiftState, DepthFundingCostState, DepthFundingCompensationState, DepthLatencyCostState, Side
from xex_mm.utils.string_parsing import contract_to_ccys

DEPTH_LEVEL = 20

class TransOrder:
    def __init__(self, side: Direction, p: float, q: float, iid: str, floor: float, delta: float = 0, life: int = 100,
                 maker_taker=MakerTaker.undefined, sent_ts=None, cancel=False, message=None, query=0,symbol_id=None,
                 account_name=None, tag=None, xchg_id=None, client_id=None, cancel_id=None, query_id=None, symbol=None) -> None:
        self.side = side
        self.price = p
        self.quantity = q
        self.iid = iid
        self.life = life
        self.floor = floor
        self.delta = delta

        self.sent_ts = sent_ts
        self.cancel = cancel
        self.message = message
        self.query = query
        self.maker_taker = maker_taker
        self.cancel_id = cancel_id
        self.query_id = query_id
        self.symbol = symbol

        # for on board update
        self.symbol_id = symbol_id
        self.account_name = account_name
        self.tag = tag
        self.xchg_id = xchg_id
        self.client_id = client_id

    def update(self, order: AtomOrder):
        self.symbol_id = order.symbol_id
        self.account_name = order.account_name
        self.tag = order.tag
        self.xchg_id = order.xchg_id
        self.client_id = order.client_id

    def generate_symbol(self, contract_mapper: ContractMapper):
        # only for binance spot and usdt_contract for now.
        if self.iid.startswith("binance"):
            if contract_mapper.is_spot_proxy_iid(self.iid):
                symbol = contract_mapper.proxy_iid_to_iid(self.iid)
                self.symbol = f"{symbol}.spot.na"
            else:
                self.symbol = f"{self.iid}.usdt_contract.na"
        else:
            raise RuntimeError()

    def to_dict(self):
        return dict(
            side=self.side,
            price=self.price,
            quantity=self.quantity,
            iid=self.iid,
            life=self.life,
            floor=self.floor,
            delta=self.delta,
            sent_ts=self.sent_ts,
            cancel=self.cancel,
            message=self.message,
            query=self.query,
            maker_taker=self.maker_taker,
            symbol_id=self.symbol_id,
            account_name=self.account_name,
            tag=self.tag,
            xchg_id=self.xchg_id,
            client_id=self.client_id,
            cancel_id=self.cancel_id,
            query_id=self.query_id,
            symbol=self.symbol,
        )

    @classmethod
    def from_dict(cls, payload):
        return cls(
            side=payload["side"],
            p=payload["price"],
            q=payload["quantity"],
            iid=payload["iid"],
            floor=payload["floor"],
            delta=payload["delta"],
            life=payload["life"],
            sent_ts = payload["sent_ts"],
            cancel = payload["cancel"],
            message = payload["message"],
            query = payload["query"],
            maker_taker = payload["maker_taker"],
            symbol_id = payload["symbol_id"],
            account_name = payload["account_name"],
            tag = payload["tag"],
            xchg_id = payload["xchg_id"],
            client_id = payload["client_id"],
            cancel_id = payload["cancel_id"],
            query_id = payload["query_id"],
            symbol = payload["symbol"],
            )

    def clone(self):
        return self.__class__(
            side=self.side,
            p=self.price,
            q=self.quantity,
            iid=self.iid,
            life=self.life,
            floor=self.floor,
            delta=self.delta,
            sent_ts=self.sent_ts,
            cancel=self.cancel,
            message=self.message,
            query=self.query,
            maker_taker=self.maker_taker,
            symbol_id=self.symbol_id,
            account_name=self.account_name,
            tag=self.tag,
            xchg_id=self.xchg_id,
            client_id=self.client_id,
            cancel_id=self.cancel_id,
            query_id=self.query_id,
            symbol=self.symbol,
        )

    def __repr__(self):
        return f"{self.__dict__}"


class Order:
    def __init__(self,
                 contract: str,
                 ex: str,
                 prc: float,
                 qty: float,
                 side: Direction,
                 filled_qty: float,
                 avg_fill_prc: float,
                 order_id: str,
                 client_id:str,
                 local_time: int,
                 status: OrderStatus,
                 account_name: str,
                 ):
        self.contract = contract
        self.ex = ex
        self.prc = prc
        self.qty = qty
        self.side = side
        self.filled_qty = filled_qty
        self.avg_fill_prc = avg_fill_prc
        self.order_id = order_id
        self.xchg_id = order_id
        self.client_id = client_id
        self.local_time = local_time
        self.status = status
        self.account_name = account_name
        self.meta = dict()

    @property
    def iid(self):
        return f"{self.ex}.{self.contract}"

    @classmethod
    def from_dict(cls, porder: PartialOrder, torder: TransOrder, contract_value: Decimal):
        if isinstance(porder, cls):
            return porder
        ex, contract = torder.iid.split(".")
        return cls(
            contract=contract,
            ex=ex,
            prc=torder.price,
            qty=torder.quantity,
            side=torder.side,
            filled_qty=float(porder.filled_amount * contract_value),
            avg_fill_prc=float(porder.avg_filled_price),
            order_id=porder.xchg_id,
            client_id=torder.client_id,
            local_time=porder.local_ms,
            status=porder.xchg_status,
            account_name=porder.account_name,
        )

    @classmethod
    def from_dict_bt(cls, order_dict: Dict, scalers: Tuple):
        iid = order_dict["instrument_id"]
        ex, contract = iid.split(".")
        pf, qf, cv = scalers
        avg_fill_prc = order_dict.get("price_avg")
        avg_fill_prc = np.nan if avg_fill_prc is None else avg_fill_prc
        o = cls(
            contract=contract,
            ex=ex,
            prc=order_dict["price"] / pf,
            qty=order_dict["qty"] / qf * cv,
            side=Direction.long if order_dict["side"] == 1 else Direction.short,
            filled_qty=order_dict["filled_qty"] / qf * cv,
            avg_fill_prc=avg_fill_prc,
            order_id=order_dict["order_id"],
            local_time=order_dict["update_time"],
            status=order_dict["status"],
            client_id=order_dict["order_id"],
            account_name="",
        )
        return o


class Level:
    def __init__(self, prc: float, qty: Union[int, float,]):
        """
        Quantity is in units of coins/tokens.
        """
        self._neumeric_tol = 1e-10
        assert prc > 0
        self.prc = prc
        if abs(qty) < self._neumeric_tol:
            qty = 0
        assert qty >= 0
        self.qty = qty

    def __add__(self, other):
        assert isinstance(other, Level)
        assert abs(self.prc - other.prc) < 1e-6
        qty = self.qty + other.qty
        assert qty >= 0
        return self.__class__(prc=self.prc, qty=qty)

    def __sub__(self, other):
        assert isinstance(other, Level)
        assert abs(self.prc - other.prc) < 1e-6
        qty = self.qty - other.qty
        if abs(qty) < self._neumeric_tol:
            qty = 0
        assert qty >= 0
        return self.__class__(prc=self.prc, qty=qty)

    def __repr__(self):
        return f"{self.__class__.__name__}(prc={self.prc},qty={self.qty})"

    def clone(self):
        return self.__class__(prc=self.prc, qty=self.qty)


@dataclass
class BBOMeta:
    contract: str
    ex: str


class BBO:
    def __init__(self, best_ask_prc: float, best_ask_qty: float, best_bid_prc: float, best_bid_qty: float, local_time: int, server_time: int, meta: BBOMeta) -> None:
        self.best_ask_prc = best_ask_prc
        self.best_ask_qty = best_ask_qty
        self.best_bid_prc = best_bid_prc
        self.best_bid_qty = best_bid_qty
        self.local_time = local_time
        self.server_time = server_time
        self.valid_best_bid = self._valid_best_bid()
        self.valid_best_ask = self._valid_best_ask()
        self.valid_bbo = self._valid_bbo()
        self.meta = meta

    def _valid_best_bid(self) -> bool:
        if math.isnan(self.best_bid_prc):
            return False
        if math.isinf(self.best_bid_prc):
            return False
        if self.best_bid_prc == 0:
            return False
        if self.best_bid_qty == 0:
            return False
        return True

    def _valid_best_ask(self) -> bool:
        if math.isnan(self.best_ask_prc):
            return False
        if math.isinf(self.best_ask_prc):
            return False
        if self.best_ask_prc == 0:
            return False
        if self.best_ask_qty == 0:
            return False
        return True

    def _valid_bbo(self) -> bool:
        return self.best_ask_prc > self.best_bid_prc

    def to_depth(self):
        contract = self.meta.contract
        ccy1, ccy2 = contract_to_ccys(contract=contract)
        meta_data = DepthMetaData(
            contract=contract,
            ccy1=ccy1,
            ccy2=ccy2,
            ex=self.meta.ex,
            depth_ccy_type=DepthCcyType.ccy1,
            depth_qty_group_type=DepthQtyGroupType.base,
            depth_prc_group_type=DepthPrcGroupType.base,
            depth_at_touch_guard_type=DepthAtTouchGuardType.unguarded,
            depth_completeness_state=DepthCompletenessState.depth_update,
            depth_shadow_state=DepthShadowState.real,
            depth_xex_state=DepthXExState.SEx,
            depth_taker_fee_state=DepthTakerFeeState.pre_taker_fee,
            depth_prc_shift_state=DepthPrcShiftState.unshifted,
            depth_funding_cost_state=DepthFundingCostState.pre_funding_cost,
            depth_funding_compensation_state=DepthFundingCompensationState.pre_funding_compensation,
            depth_latency_cost_state=DepthLatencyCostState.pre_latency_cost,
        )
        return Depth(
            asks=[Level(prc=self.best_ask_prc, qty=self.best_ask_qty)],
            bids=[Level(prc=self.best_bid_prc, qty=self.best_bid_qty)],
            resp_ts=self.local_time,
            server_ts=self.server_time,
            meta_data=meta_data,
        )

    @classmethod
    def from_dict_prod(cls, iid: str, bbo_depth: BBODepth):
        """
        {
            ask: [price, quantity],
            bid: [price, quantity],
            sequence_id: 1234
            server_ms: 1591843843560,
            local_ms: 1591843843560,

        }
        """
        if isinstance(bbo_depth, cls):
            return bbo_depth
        ex, contract = iid.split(".")
        meta = BBOMeta(contract=contract, ex=ex)
        return cls(best_ask_prc=float(bbo_depth.ask[0]),
                   best_ask_qty=float(bbo_depth.ask[1]),
                   best_bid_prc=float(bbo_depth.bid[0]),
                   best_bid_qty=float(bbo_depth.bid[1]),
                   local_time=bbo_depth.local_ms,
                   server_time=bbo_depth.server_ms,
                   meta=meta)

    @classmethod
    def from_dict_bt(cls, bbo_dict: Dict, scalers: Tuple):
        pf, qf, cv = scalers
        ex, contract = bbo_dict["instrument_id"].split(".")
        meta = BBOMeta(contract=contract, ex=ex)
        return cls(best_ask_prc=bbo_dict["ask_price"] / pf,
                   best_ask_qty=bbo_dict["ask_qty"] / qf * cv,
                   best_bid_prc=bbo_dict["bid_price"] / pf,
                   best_bid_qty=bbo_dict["bid_qty"] / qf * cv,
                   local_time=bbo_dict["localtime"],
                   server_time=bbo_dict["tx_time"],
                   meta=meta)


class DepthMetaData:
    def __init__(self,
                 contract: str,
                 ccy1: str,
                 ccy2: str,
                 ex: str,
                 depth_ccy_type: str,
                 depth_qty_group_type: str,
                 depth_prc_group_type: str,
                 depth_at_touch_guard_type: str,
                 depth_completeness_state: str,
                 depth_shadow_state: str,
                 depth_xex_state: str,
                 depth_prc_shift_state: str,
                 depth_taker_fee_state: str,
                 depth_funding_cost_state: str,
                 depth_funding_compensation_state: str,
                 depth_latency_cost_state: str,
                 ):
        self.contract = contract
        self.ccy1 = ccy1
        self.ccy2 = ccy2
        self.ex = ex
        self.depth_ccy_type = depth_ccy_type
        self.depth_qty_group_type = depth_qty_group_type
        self.depth_prc_group_type = depth_prc_group_type
        self.depth_at_touch_guard_type = depth_at_touch_guard_type
        self.depth_completeness_state = depth_completeness_state
        self.depth_shadow_state = depth_shadow_state
        self.depth_xex_state = depth_xex_state
        self.depth_prc_shift_state = depth_prc_shift_state
        self.depth_taker_fee_state = depth_taker_fee_state
        self.depth_funding_cost_state = depth_funding_cost_state
        self.depth_funding_compensation_state = depth_funding_compensation_state
        self.depth_latency_cost_state = depth_latency_cost_state

    def clone(self):
        return self.__class__(
            contract=self.contract,
            ccy1=self.ccy1,
            ccy2=self.ccy2,
            ex=self.ex,
            depth_ccy_type=self.depth_ccy_type,
            depth_qty_group_type=self.depth_qty_group_type,
            depth_prc_group_type=self.depth_prc_group_type,
            depth_at_touch_guard_type=self.depth_at_touch_guard_type,
            depth_completeness_state=self.depth_completeness_state,
            depth_shadow_state=self.depth_shadow_state,
            depth_xex_state=self.depth_xex_state,
            depth_prc_shift_state=self.depth_prc_shift_state,
            depth_taker_fee_state=self.depth_taker_fee_state,
            depth_funding_cost_state=self.depth_funding_cost_state,
            depth_funding_compensation_state=self.depth_funding_compensation_state,
            depth_latency_cost_state=self.depth_latency_cost_state,
        )


class Trade:
    def __init__(self, ex: str, contract: str, price: float, quantity: float, tx_time: int, local_time: int, side: int, mexc_side: int, uid: str):
        self.ex = ex
        self.contract = contract
        self.price = price
        self.quantity = quantity
        self.tx_time = tx_time
        self.local_time = local_time
        self.side = side
        self.mexc_side = mexc_side
        self.mexc_uid = uid
        self.is_valid = self._is_valid()

    def _is_valid(self) -> bool:
        if self.quantity == 0:
            return False
        return True

    @classmethod
    def from_dict_prod(cls, ex: str, contract: str, trade: PublicTrade):
        if isinstance(trade, cls):
            return trade
        return cls(
            ex=ex,
            contract=contract,
            price=float(trade.price),
            quantity=float(trade.quantity),
            tx_time=int(trade.transaction_ms),
            local_time=int(trade.local_ms),
            side=1 if trade.side == OrderSide.Buy else -1,
            mexc_side=None,
            uid=None
        )

    @classmethod
    def from_dict_bt(cls, trade: Dict, scalers: Tuple):
        iid = trade["instrument_id"]
        ex, contract = iid.split(".")
        pf, qf, cv = scalers
        return cls(
            ex=ex,
            contract=contract,
            price=trade["price"] / pf,
            quantity=trade["qty"] / qf * cv,
            tx_time=trade["tx_time"],
            local_time=trade["localtime"],
            side=trade["side"],
            mexc_side=trade["mexc_side"] if trade.get("mexc_side") else None,
            uid=trade["uid"] if trade.get("uid") else -1
        )

    def __repr__(self):
        return f"contract={self.contract},ex={self.ex},prc={self.price},qty={self.quantity},tx_time={self.tx_time}," \
               f"local_time={self.local_time},side={self.side},mexc_side={self.mexc_side},mexc_uid={self.mexc_uid}"

    def clone(self):
        return self.__class__(ex=self.ex,
                              contract=self.contract,
                              price=self.price,
                              quantity=self.quantity,
                              tx_time=self.tx_time,
                              local_time=self.local_time,
                              side=self.side,
                              mexc_side=self.mexc_side,
                              uid=self.mexc_uid)


class Depth:
    """
    This is a simple implementation. Assuming we don't care about queue.
    """
    _neumeric_tol: float = 1e-10

    def __init__(self, asks: List[Level], bids: List[Level], resp_ts: int, server_ts: int, meta_data: DepthMetaData):
        self.asks = asks
        self.bids = bids
        if (len(asks) > 0) and (len(bids) > 0):
            if meta_data.ex != "xex":
                assert self.asks[0].prc > self.bids[0].prc
        self.resp_ts = resp_ts
        self.server_ts = server_ts
        self.meta_data = meta_data

        # Stateful attributes
        self.mp = None

        self.price_precision_manager = PricePrecisionManager()
        self.quantity_precision_manager = QuantityPrecisionManager()
        self.price_precision_map = dict()
        self.quantity_precision_map = dict()

    def get_price_precision(self, contract: str, ex: str):
        ce = (contract, ex)
        if ce in self.price_precision_map:
            return self.price_precision_map[ce]
        price_precision = self.price_precision_manager.get_price_precision_by_ce(contract=contract, ex=ex)
        self.price_precision_map[ce] = price_precision
        return price_precision

    def get_quantity_precision(self, contract: str, ex: str):
        ce = (contract, ex)
        if ce in self.quantity_precision_map:
            return self.quantity_precision_map[ce]
        quantity_precision = self.quantity_precision_manager.get_quantity_precision_by_ce(contract=contract, ex=ex)
        self.quantity_precision_map[ce] = quantity_precision
        return quantity_precision

    def __repr__(self):
        max_len = -1
        ask_lvl_strs = []
        rev_asks = self.asks.copy()
        rev_asks.reverse()
        for l in rev_asks:
            sl = str(l)
            if len(sl) > max_len:
                max_len = len(sl)
            ask_lvl_strs.append(sl)
        bid_lvl_strs = []
        for l in self.bids:
            sl = str(l)
            if len(sl) > max_len:
                max_len = len(sl)
            bid_lvl_strs.append(sl)
        s = f"resp_ts={self.resp_ts}\nserver_ts={self.server_ts}\nasks:\n"
        for ls in ask_lvl_strs:
            s += f"{ls}\n"
        s += "bids:\n"
        for ls in bid_lvl_strs:
            s += f"{ls}\n"
        return s

    @classmethod
    def _safe_append_level(cls, side: List[Level], lvl: Level):
        assert lvl.prc > 0
        if abs(lvl.qty) < cls._neumeric_tol:
            lvl.qty = 0
        assert lvl.qty >= 0
        if lvl.qty > 0:
            side.append(lvl)

    @classmethod
    def __add_asks(cls, lhs: List[Level], rhs: List[Level]) -> List[Level]:
        side = []
        li = 0
        ri = 0
        while (li < len(lhs)) or (ri < len(rhs)):
            if (li < len(lhs)) and (ri < len(rhs)):
                if lhs[li].prc < rhs[ri].prc:
                    cls._safe_append_level(side=side, lvl=lhs[li])
                    li += 1
                elif lhs[li].prc > rhs[ri].prc:
                    cls._safe_append_level(side=side, lvl=rhs[ri])
                    ri += 1
                else:
                    cls._safe_append_level(side=side, lvl=lhs[li] + rhs[ri])
                    li += 1
                    ri += 1
            elif (li < len(lhs)) and (ri >= len(rhs)):
                for lvl in lhs[li:]:
                    cls._safe_append_level(side=side, lvl=lvl)
                li = len(lhs)
            elif (li >= len(lhs)) and (ri < len(rhs)):
                for lvl in rhs[ri:]:
                    cls._safe_append_level(side=side, lvl=lvl)
                ri = len(rhs)
            else:
                raise RuntimeError()
        return side

    @classmethod
    def __add_bids(cls, lhs: List[Level], rhs: List[Level]) -> List[Level]:
        side = []
        li = 0
        ri = 0
        while (li < len(lhs)) or (ri < len(rhs)):
            if (li < len(lhs)) and (ri < len(rhs)):
                if lhs[li].prc > rhs[ri].prc:
                    cls._safe_append_level(side=side, lvl=lhs[li])
                    li += 1
                elif lhs[li].prc < rhs[ri].prc:
                    cls._safe_append_level(side=side, lvl=rhs[ri])
                    ri += 1
                else:
                    cls._safe_append_level(side=side, lvl=lhs[li] + rhs[ri])
                    li += 1
                    ri += 1
            elif (li < len(lhs)) and (ri >= len(rhs)):
                for lvl in lhs[li:]:
                    cls._safe_append_level(side=side, lvl=lvl)
                li = len(lhs)
            elif (li >= len(lhs)) and (ri < len(rhs)):
                for lvl in rhs[ri:]:
                    cls._safe_append_level(side=side, lvl=lvl)
                ri = len(rhs)
            else:
                raise RuntimeError()
        return side

    def __add__(self, other):
        assert isinstance(other, Depth)
        xex_asks = self.__add_asks(lhs=self.asks, rhs=other.asks)
        xex_bids = self.__add_bids(lhs=self.bids, rhs=other.bids)
        xex_resp_ts = max(self.resp_ts, other.resp_ts)
        xex_server_ts = max(self.server_ts, other.server_ts)
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_xex_state = DepthXExState.XEx
        return self.__class__(asks=xex_asks, bids=xex_bids, resp_ts=xex_resp_ts, server_ts=xex_server_ts,
                              meta_data=new_meta_data)

    @classmethod
    def __sub_asks(cls, lhs: List[Level], rhs: List[Level]) -> List[Level]:
        side = []
        li = 0
        ri = 0
        while (li < len(lhs)) or (ri < len(rhs)):
            if (li < len(lhs)) and (ri < len(rhs)):
                if lhs[li].prc < rhs[ri].prc:
                    cls._safe_append_level(side=side, lvl=lhs[li])
                    li += 1
                elif lhs[li].prc > rhs[ri].prc:
                    raise RuntimeError(f"Subtracting a price level that does not exist on LHS? RHS={rhs[ri]}")
                else:
                    cls._safe_append_level(side=side, lvl=lhs[li] - rhs[ri])
                    li += 1
                    ri += 1
            elif (li < len(lhs)) and (ri >= len(rhs)):
                for lvl in lhs[li:]:
                    cls._safe_append_level(side=side, lvl=lvl)
                li = len(lhs)
            elif (li >= len(lhs)) and (ri < len(rhs)):
                raise RuntimeError(f"Subtracting a price level that does not exist on LHS? RHS={rhs[ri:]}")
            else:
                raise RuntimeError()
        return side

    @classmethod
    def __sub_bids(cls, lhs: List[Level], rhs: List[Level]) -> List[Level]:
        side = []
        li = 0
        ri = 0
        while (li < len(lhs)) or (ri < len(rhs)):
            if (li < len(lhs)) and (ri < len(rhs)):
                if lhs[li].prc > rhs[ri].prc:
                    cls._safe_append_level(side=side, lvl=lhs[li])
                    li += 1
                elif lhs[li].prc < rhs[ri].prc:
                    raise RuntimeError(f"Subtracting a price level that does not exist on LHS? RHS={rhs[ri]}")
                else:
                    cls._safe_append_level(side=side, lvl=lhs[li] - rhs[ri])
                    li += 1
                    ri += 1
            elif (li < len(lhs)) and (ri >= len(rhs)):
                for lvl in lhs[li:]:
                    cls._safe_append_level(side=side, lvl=lvl)
                li = len(lhs)
            elif (li >= len(lhs)) and (ri < len(rhs)):
                raise RuntimeError(f"Subtracting a price level that does not exist on LHS? RHS={rhs[ri:]}")
            else:
                raise RuntimeError()
        return side

    def __sub__(self, other):
        assert isinstance(other, Depth)
        xex_asks = self.__sub_asks(lhs=self.asks, rhs=other.asks)
        xex_bids = self.__sub_bids(lhs=self.bids, rhs=other.bids)
        xex_resp_ts = max(self.resp_ts, other.resp_ts)
        xex_server_ts = max(self.server_ts, other.server_ts)
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_xex_state = DepthXExState.XEx
        return self.__class__(asks=xex_asks, bids=xex_bids, resp_ts=xex_resp_ts, server_ts=xex_server_ts,
                              meta_data=new_meta_data)

    def clone(self):
        clone_asks = [lvl.clone() for lvl in self.asks]
        clone_bids = [lvl.clone() for lvl in self.bids]
        return self.__class__(asks=clone_asks, bids=clone_bids, resp_ts=self.resp_ts, server_ts=self.server_ts,
                              meta_data=self.meta_data.clone())

    def _get_shadow_side(self, q: int, side: List[Level]):
        cum_q = 0
        shadow_side = []
        for lvl in side:
            if cum_q + lvl.qty < q:
                cum_q += lvl.qty
                continue
            if (cum_q < q) and (cum_q + lvl.qty >= q):
                cum_q += lvl.qty
                remaining_q = cum_q - q
                if remaining_q > 0:
                    shadow_side.append(Level(prc=lvl.prc, qty=remaining_q))
                continue
            if (cum_q >= q):
                shadow_side.append(lvl.clone())
                continue
            raise RuntimeError()
        return shadow_side

    def get_shadow_depth(self, q: int, d: Direction):
        assert q > 0
        if d == Direction.long:
            shadow_asks = self._get_shadow_side(q=q, side=self.asks)
            shadow_bids = [lvl.clone() for lvl in self.bids]
            new_meta_data = self.meta_data.clone()
            new_meta_data.depth_shadow_state = DepthShadowState.shadow
            return self.__class__(asks=shadow_asks, bids=shadow_bids, resp_ts=self.resp_ts, server_ts=self.server_ts,
                                  meta_data=new_meta_data)

        if d == Direction.short:
            shadow_bids = self._get_shadow_side(q=q, side=self.bids)
            shadow_asks = [lvl.clone() for lvl in self.asks]
            new_meta_data = self.meta_data.clone()
            new_meta_data.depth_shadow_state = DepthShadowState.shadow
            return self.__class__(asks=shadow_asks, bids=shadow_bids, resp_ts=self.resp_ts, server_ts=self.server_ts,
                                  meta_data=new_meta_data)

    @classmethod
    def from_dict(cls, contract: str, ccy1: str, ccy2: str, ex: str, depth: Dict):
        """
        :param depth:

            Example:

                {
                    'asks': [[Decimal(9417), Decimal(31941)] ....],
                    'bids': [[]...],
                    'resp_ts': 1591843843560,
                    'server_ts': 1591843843560,
                }

            Asks and bids are lists of pairs (price, quantity).

        :return: Depth representation.
        """

        ask_lvls = []
        for prc, qty in depth["asks"]:
            cls._safe_append_level(side=ask_lvls, lvl=Level(prc=float(prc), qty=float(qty)))
        bid_lvls = []
        for prc, qty in depth["bids"]:
            cls._safe_append_level(side=bid_lvls, lvl=Level(prc=float(prc), qty=float(qty)))
        meta_data = DepthMetaData(
            contract=contract,
            ccy1=ccy1,
            ccy2=ccy2,
            ex=ex,
            depth_ccy_type=DepthCcyType.ccy1,
            depth_qty_group_type=DepthQtyGroupType.base,
            depth_prc_group_type=DepthPrcGroupType.base,
            depth_at_touch_guard_type=DepthAtTouchGuardType.unguarded,
            depth_completeness_state=DepthCompletenessState.depth_update,
            depth_shadow_state=DepthShadowState.real,
            depth_xex_state=DepthXExState.SEx,
            depth_taker_fee_state=DepthTakerFeeState.pre_taker_fee,
            depth_prc_shift_state=DepthPrcShiftState.unshifted,
            depth_funding_cost_state=DepthFundingCostState.pre_funding_cost,
            depth_funding_compensation_state=DepthFundingCompensationState.pre_funding_compensation,
            depth_latency_cost_state=DepthLatencyCostState.pre_latency_cost,
        )
        return cls(asks=ask_lvls, bids=bid_lvls, resp_ts=depth["resp_ts"], server_ts=depth["server_ts"], meta_data=meta_data)


    @staticmethod
    def order_queue_rebuild(order_queue, scalers: Tuple, level: int):
        order_queue_new = []
        pf, qf, cv = scalers
        for price, amount in order_queue:
            order_queue_new.append([price/pf, (amount/qf) * cv])
            if len(order_queue_new) == level:
                return order_queue_new
        return order_queue_new

    @classmethod
    def generate_backtest_order_book(cls, orderbook, scalers: Tuple, level: int):
        """
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob_res = dict()
        ob_res['server_ts'] = orderbook['tx_time']
        ob_res['resp_ts'] = orderbook['localtime']
        ob_res['asks'] = cls.order_queue_rebuild(order_queue=orderbook['asks'].items(), scalers=scalers, level=level)
        ob_res['bids'] = cls.order_queue_rebuild(order_queue=orderbook['bids'].items(), scalers=scalers, level=level)
        if ob_res['asks'] is None or ob_res['bids'] is None:
            return False
        return ob_res

    @classmethod
    def from_dict_bt(cls, depth_dict: Dict, scalers: Tuple, level: int):
        iid = depth_dict["instrument_id"]
        ex, contract = iid.split(".")
        server_ts = depth_dict['tx_time']
        # logging.info(f"Depth localtime = {depth['localtime']}")
        ob_res = cls.generate_backtest_order_book(depth_dict, scalers=scalers, level=level)
        if ob_res is False:
            print(f'{iid} bad depth ({datetime.datetime.fromtimestamp(server_ts / 1e3)}):', ob_res)
        ccy1, ccy2 = contract_to_ccys(contract=contract)
        depth_obj = Depth.from_dict(
            depth=ob_res,
            contract=contract,
            ccy1=ccy1,
            ccy2=ccy2,
            ex=ex,
        )
        return depth_obj

    def get_qty_grp_view(self, q_grp_size: float):
        new_asks = []
        for lvl in self.asks:
            try:
                new_asks.append(Level(prc=lvl.prc, qty=max(int(round(lvl.qty / q_grp_size)), 1)))
            except Exception as e:
                logging.error(f"lvl={lvl}, q_grp_size={q_grp_size}, depth_meta={self.meta_data}, resp_ts={self.resp_ts}, server_ts={self.server_ts}")
                raise e
        new_bids = []
        for lvl in self.bids:
            try:
                new_bids.append(Level(prc=lvl.prc, qty=max(int(round(lvl.qty / q_grp_size)), 1)))
            except Exception as e:
                logging.error(f"lvl={lvl}, q_grp_size={q_grp_size}, depth_meta={self.meta_data}, resp_ts={self.resp_ts}, server_ts={self.server_ts}")
                raise e
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_qty_group_type = DepthQtyGroupType.grouped
        return self.__class__(bids=new_bids, asks=new_asks, resp_ts=self.resp_ts, server_ts=self.server_ts,
                              meta_data=new_meta_data)

    def get_ccy2_view(self):
        new_asks = []
        for lvl in self.asks:
            new_asks.append(Level(prc=lvl.prc, qty=lvl.qty * lvl.prc))
        new_bids = []
        for lvl in self.bids:
            new_bids.append(Level(prc=lvl.prc, qty=lvl.qty * lvl.prc))
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_ccy_type = DepthCcyType.ccy2
        return self.__class__(bids=new_bids, asks=new_asks, resp_ts=self.resp_ts, server_ts=self.server_ts,
                              meta_data=new_meta_data)

    def get_at_touch_guarded_view(self, amt_bnd):
        _asks = []
        _started = False
        for lvl in self.asks:
            if _started:
                _asks.append(lvl)
                continue
            if self.meta_data.depth_ccy_type == DepthCcyType.ccy1:
                amt = lvl.qty * lvl.prc
            elif self.meta_data.depth_ccy_type == DepthCcyType.ccy2:
                amt = lvl.qty  # U
            else:
                raise NotImplementedError()
            if amt > amt_bnd:
                _started = True
                _asks.append(lvl)
                continue
        _bids = []
        _started = False
        for lvl in self.bids:
            if _started:
                _bids.append(lvl)
                continue
            if self.meta_data.depth_ccy_type == DepthCcyType.ccy1:
                amt = lvl.qty * lvl.prc
            elif self.meta_data.depth_ccy_type == DepthCcyType.ccy2:
                amt = lvl.qty
            else:
                raise NotImplementedError()
            if amt > amt_bnd:
                _started = True
                _bids.append(lvl)
                continue
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_at_touch_guard_type = DepthAtTouchGuardType.guarded
        return self.__class__(asks=_asks, bids=_bids, resp_ts=self.resp_ts, server_ts=self.server_ts,
                              meta_data=new_meta_data)

    def update_depth_with_bbo(self, bbo: BBO):
        if not bbo.valid_bbo:
            return self.clone()

        tol = 1e-10
        _asks = []
        for ask in self.asks:
            if ask.prc < bbo.best_ask_prc + tol:
                continue
            _asks.append(ask)
        _asks = [Level(prc=bbo.best_ask_prc, qty=bbo.best_ask_qty)] + _asks
        _bids = []
        for bid in self.bids:
            if bid.prc > bbo.best_bid_prc - tol:
                continue
            _bids.append(bid)
        _bids = [Level(prc=bbo.best_bid_prc, qty=bbo.best_bid_qty)] + _bids
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_completeness_state = DepthCompletenessState.bbo_update
        return self.__class__(asks=_asks, bids=_bids, resp_ts=bbo.local_time, server_ts=bbo.local_time,
                              meta_data=new_meta_data)

    def update_depth_with_trade(self, trade: Trade, halfsprd: float):
        """ This is a sub-optimal approach compared to BBO """
        if not trade.is_valid:
            return self.clone()

        tol = 1e-10
        if trade.side == Side.long:
            _asks = []
            for ask in self.asks:
                if ask.prc < trade.price + tol:
                    continue
                _asks.append(ask)
            _asks = [Level(prc=trade.price, qty=trade.quantity)] + _asks
            _bids = []
            for bid in self.bids:
                if bid.prc > trade.price - tol:
                    continue
                _bids.append(bid)
            prc_tick = 10**-self.get_price_precision(contract=trade.contract, ex=trade.ex)
            qty_tick = 10**-self.get_quantity_precision(contract=trade.contract, ex=trade.ex)
            implied_best_bid = trade.price - 2 * halfsprd
            if (len(_bids) == 0) or (implied_best_bid - _bids[0].prc > prc_tick / 2):
                _bids = [Level(prc=implied_best_bid, qty=qty_tick)] + _bids
        elif trade.side == Side.short:
            _bids = []
            for bid in self.bids:
                if bid.prc > trade.price - tol:
                    continue
                _bids.append(bid)
            _bids = [Level(prc=trade.price, qty=trade.quantity)] + _bids
            _asks = []
            for ask in self.asks:
                if ask.prc < trade.price + tol:
                    continue
                _asks.append(ask)
            prc_tick = 10**-self.get_price_precision(contract=trade.contract, ex=trade.ex)
            qty_tick = 10**-self.get_quantity_precision(contract=trade.contract, ex=trade.ex)
            implied_best_ask = trade.price + 2 * halfsprd
            if (len(_asks) == 0) or (_asks[0].prc - implied_best_ask > prc_tick / 2):
                _asks = [Level(prc=implied_best_ask, qty=qty_tick)] + _asks
        else:
            raise RuntimeError(f"Unsupported side: {trade.side}")
        new_meta_data = self.meta_data.clone()
        new_meta_data.depth_completeness_state = DepthCompletenessState.trade_update
        return self.__class__(asks=_asks, bids=_bids, resp_ts=trade.local_time, server_ts=trade.local_time,
                              meta_data=new_meta_data)


    def get_prc_shifted_view(self, rel_prc_shift: float):
        post_fee_asks = [Level(prc=ask_lvl.prc * math.exp(rel_prc_shift), qty=ask_lvl.qty) for ask_lvl in self.asks]
        post_fee_bids = [Level(prc=bid_lvl.prc * math.exp(rel_prc_shift), qty=bid_lvl.qty) for bid_lvl in self.bids]
        meta = self.meta_data.clone()
        meta.depth_prc_shift_state = DepthPrcShiftState.shifted
        return self.__class__(asks=post_fee_asks, bids=post_fee_bids, resp_ts=self.resp_ts, server_ts=self.server_ts,
                              meta_data=meta)

    def get_cost_shifted_view(self, rel_cost_shift: float):
        post_fee_asks = [Level(prc=ask_lvl.prc * math.exp(rel_cost_shift), qty=ask_lvl.qty) for ask_lvl in self.asks]
        post_fee_bids = [Level(prc=bid_lvl.prc * math.exp(-rel_cost_shift), qty=bid_lvl.qty) for bid_lvl in self.bids]
        meta = self.meta_data.clone()
        return self.__class__(asks=post_fee_asks, bids=post_fee_bids, resp_ts=self.resp_ts, server_ts=self.server_ts,
                              meta_data=meta)

    def get_post_taker_fee_view(self):
        maker_fee, taker_fee = FeeConfig().get_fee(ex=self.meta_data.ex, contract=self.meta_data.contract)
        depth = self.get_cost_shifted_view(rel_cost_shift=taker_fee)
        depth.meta_data.depth_taker_fee_state = DepthTakerFeeState.post_taker_fee
        return depth

    def get_post_funding_cost_view(self):
        funding_cost = FundingCostConfig().get_funding_cost(ex=self.meta_data.ex, contract=self.meta_data.contract)
        depth = self.get_cost_shifted_view(rel_cost_shift=funding_cost)
        depth.meta_data.depth_funding_cost_state = DepthFundingCostState.post_funding_cost
        return depth

    def get_post_funding_compensation_view(self):
        funding_compensation = FundingCompensationConfig().get_funding_compensation(ex=self.meta_data.ex, contract=self.meta_data.contract)
        depth = self.get_cost_shifted_view(rel_cost_shift=funding_compensation)
        depth.meta_data.depth_funding_compensation_state = DepthFundingCompensationState.post_funding_compensation
        return depth

    def get_post_latency_cost_view(self):
        latency_cost = LatencyCostConfig().get_latency_cost(ex=self.meta_data.ex, contract=self.meta_data.contract)
        depth = self.get_cost_shifted_view(rel_cost_shift=latency_cost)
        depth.meta_data.depth_latency_cost_state = DepthLatencyCostState.post_latency_cost
        return depth

    def get_post_cost_view(self):
        depth = self.get_post_taker_fee_view()
        depth = depth.get_post_funding_cost_view()
        depth = depth.get_post_funding_compensation_view()
        depth = depth.get_post_latency_cost_view()
        return depth

    def to_bbo(self):
        meta = BBOMeta(
            contract=self.meta_data.contract,
            ex=self.meta_data.ex,
        )
        best_ask = self.asks[0]
        best_bid = self.bids[0]
        return BBO(
            best_ask_prc=best_ask.prc,
            best_ask_qty=best_ask.qty,
            best_bid_prc=best_bid.prc,
            best_bid_qty=best_bid.qty,
            local_time=self.resp_ts,
            server_time=self.server_ts,
            meta=meta,
        )


class Exchange:
    def __init__(self):
        self.__depth: Depth = ...
        self.__24h_trade_vol: Decimal = ...

    def update_depth(self, depth: Depth):
        self.__depth = depth

    def update_24h_trade_vol(self, _24h_trade_vol: Decimal):
        self.__24h_trade_vol = _24h_trade_vol

    def get_latest_depth(self, n_lvls: int):
        raise NotImplementedError()

    def get_latest_trade(self):
        raise NotImplementedError()

    def place_order(self, symbol: str, price: float, quantity: float, is_bid: bool, is_open: bool):
        raise NotImplementedError()

    def cancel_order(self, order_id: int):
        raise NotImplementedError()

class ReferencePriceCalculator:
    """ Spot calculator """
    def _calc_side_avg_prc_and_qty(self, side: List[Level]) -> Tuple[float, int]:
        sum_amt = 0
        sum_qty = 0
        for i, lvl in enumerate(side):
            if i >= DEPTH_LEVEL:
                break
            sum_qty += lvl.qty
            sum_amt += lvl.prc * lvl.qty
        return sum_amt / sum_qty, sum_qty

    def _cal_ref_prc_bound(self, side: List[Level], qty_bound: float):
        lvl = Level(prc=np.nan, qty=0)
        for lvl in side:
            if lvl.qty > qty_bound:
                return lvl.prc
        assert lvl.qty > 0
        return lvl.prc

    def calc_from_depth(self, depth: Depth, amt_bnd) -> float:
        if len(depth.asks) == 0:
            return np.nan
        if len(depth.bids) == 0:
            return np.nan
        ask = np.nan
        bid = np.nan
        if depth.meta_data.depth_completeness_state == DepthCompletenessState.depth_update:
            for tick in depth.asks:
                if tick.qty * tick.prc > amt_bnd:
                    ask = tick.prc
                    break
            for tick in depth.bids:
                if tick.qty * tick.prc > amt_bnd:
                    bid = tick.prc
                    break
        elif (depth.meta_data.depth_completeness_state == DepthCompletenessState.bbo_update) or \
                (depth.meta_data.depth_completeness_state == DepthCompletenessState.trade_update):
            for tick in depth.asks:
                ask = tick.prc
                break
            for tick in depth.bids:
                bid = tick.prc
                break
        else:
            raise NotImplementedError()
        return (ask + bid) / 2

    def calc_from_bbo(self, bbo: BBO) -> float:
        if not bbo.valid_best_ask:
            return np.nan
        if not bbo.valid_best_bid:
            return np.nan
        """ 
        Ref prc calculated from bbo is only used to defensively cancel orders, thus using small order quantity is fine.
        We will not end up quoting aggressively. 
        """
        return (bbo.best_ask_prc + bbo.best_bid_prc) / 2


class OrderMessage:
    def __init__(self, message_type, payload: Dict):
        self.message_type = message_type
        self.payload = payload


class TransOrderManager:
    def __init__(self):
        self._order_cache: Dict[str, TransOrder] = {}
        self.ask_ladder = SortedDict()
        self.bid_ladder = SortedDict(neg)
        self._oid_to_iid_map: Dict[str, str] = {}
        self._iid_to_oids_map: Dict[str, OrderedSet] = {}
        self.price_multilier = 1e6

    def contains_order(self, oid: str) -> bool:
        return oid in self._order_cache

    def get_order(self, oid: str) -> TransOrder:
        return self._order_cache.get(oid)

    def order_at(self, oid: str) -> TransOrder:
        return self._order_cache[oid]

    def pop_order(self, oid: str) -> Union[TransOrder, None]:
        """ worst: O(n), avg: O(1)-ish """
        if not self.contains_order(oid=oid):
            return
        order = self.order_at(oid=oid)
        int_prc = self.float_prc_to_int_prc(float_prc=order.price)
        if order.side == Direction.long: # bid
            order_set: OrderedSet = self.bid_ladder[int_prc]
            order_set.remove(oid)
            if len(order_set) == 0:
                self.bid_ladder.pop(int_prc)
        elif order.side == Direction.short: # ask
            order_set: OrderedSet = self.ask_ladder[int_prc]
            order_set.remove(oid)
            if len(order_set) == 0:
                self.ask_ladder.pop(int_prc)
        else:
            raise NotImplementedError(f"Unsupported order.side={order.side}")
        self._iid_to_oids_map[self._oid_to_iid_map[oid]].remove(oid)
        self._oid_to_iid_map.pop(oid)
        return self._order_cache.pop(oid)

    def order_items(self) -> Iterable[Tuple[str, TransOrder]]:
        return self._order_cache.items()

    def float_prc_to_int_prc(self, float_prc: float) -> int:
        return int(round(float_prc * self.price_multilier))

    def int_prc_to_float_prc(self, int_prc: int) -> float:
        return int_prc / self.price_multilier

    def keys(self) -> List[str]:
        return list(self._order_cache.keys())

    def add_order(self, order: TransOrder, oid: str):
        self._order_cache[oid] = order
        int_prc = self.float_prc_to_int_prc(float_prc=order.price)
        if order.side == Direction.long:  # bid
            self.bid_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        elif order.side == Direction.short:  # ask
            self.ask_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        else:
            raise NotImplementedError()
        self._oid_to_iid_map[oid] = order.iid
        if order.iid not in self._iid_to_oids_map:
            self._iid_to_oids_map[order.iid] = OrderedSet()
        self._iid_to_oids_map[order.iid].add(oid)

    def is_order_empty(self) -> bool:
        return len(self._order_cache) == 0

    def get_oids_for_iid(self, iid: str) -> List[str]:
        return self._iid_to_oids_map.get(iid, OrderedSet())

    def is_order_canceled(self, oid: str) -> bool:
        return self.get_order(oid=oid).cancel


class MsgOrderManager:
    def __init__(self):
        self._order_cache = {}
        self.ask_ladder: SortedDict[str, List[str]] = SortedDict()
        self.bid_ladder: SortedDict[str, List[str]] = SortedDict(neg)
        self._oid_to_iid_map: Dict[str, str] = {}
        self._iid_to_oids_map: Dict[str, OrderedSet] = {}
        self.price_multilier = 1e6

    def get_order_inc_fill_qty_in_tokens(self, order: Order) -> float:
        assert order.filled_qty >= 0
        oid = order.order_id
        if oid in self._order_cache:
            prev_order: Order = self._order_cache[oid]
            inc_fill_qty_in_tokens = max(order.filled_qty - prev_order.filled_qty, 0)
            return inc_fill_qty_in_tokens
        return order.filled_qty

    def update_order(self, order: Order):
        oid = order.order_id
        self._order_cache[oid] = order
        int_prc = self._int_prc(prc=order.prc)
        if order.side == Direction.long:  # bid
            self.bid_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        elif order.side == Direction.short:  # ask
            self.ask_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        else:
            raise NotImplementedError()
        self._oid_to_iid_map[oid] = order.iid
        if order.iid not in self._iid_to_oids_map:
            self._iid_to_oids_map[order.iid] = OrderedSet()
        self._iid_to_oids_map[order.iid].add(oid)

    def contains_order(self, oid: str) -> bool:
        return oid in self._order_cache

    def get_order(self, oid: str) -> Order:
        return self._order_cache.get(oid)

    def order_at(self, oid: str) -> Order:
        return self._order_cache[oid]

    def pop_order(self, oid: str) -> Union[Order, None]:
        """ worst: O(n), avg: O(1)-ish """
        if not self.contains_order(oid=oid):
            return
        order = self.order_at(oid=oid)
        int_prc = self._int_prc(prc=order.prc)
        if order.side == Direction.long: # bid
            order_set: OrderedSet = self.bid_ladder[int_prc]
            order_set.remove(oid)
            if len(order_set) == 0:
                self.bid_ladder.pop(int_prc)
        elif order.side == Direction.short: # ask
            order_set: OrderedSet = self.ask_ladder[int_prc]
            order_set.remove(oid)
            if len(order_set) == 0:
                self.ask_ladder.pop(int_prc)
        else:
            raise NotImplementedError(f"Unsupported order.side={order.side}")
        self._iid_to_oids_map[self._oid_to_iid_map[oid]].remove(oid)
        self._oid_to_iid_map.pop(oid)
        return self._order_cache.pop(oid)

    def order_items(self) -> Iterable[Tuple[str, Order]]:
        return self._order_cache.items()

    def _int_prc(self, prc: float) -> int:
        return int(prc * self.price_multilier)

    def prc_to_int_prc(self, prc: float):
        return self._int_prc(prc=prc)

    def int_prc_to_prc(self, int_prc: int):
        return int_prc / self.price_multilier

    def keys(self) -> List[str]:
        return list(self._order_cache.keys())

    def add_order(self, order: Order, oid: str):
        self._order_cache[oid] = order
        int_prc = self._int_prc(prc=order.prc)
        if order.side == Direction.long:  # bid
            self.bid_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        elif order.side == Direction.short:  # ask
            self.ask_ladder.setdefault(int_prc, OrderedSet()).add(oid)
        else:
            raise NotImplementedError()
        self._oid_to_iid_map[oid] = order.iid
        if order.iid not in self._iid_to_oids_map:
            self._iid_to_oids_map[order.iid] = OrderedSet()
        self._iid_to_oids_map[order.iid].add(oid)

    def is_trans_order_empty(self) -> bool:
        return len(self._order_cache) == 0

    def get_oids_for_iid(self, iid: str) -> List[str]:
        return self._iid_to_oids_map.get(iid, OrderedSet())


class OrderManager:
    def __init__(self):
        self.trans_order_manager = TransOrderManager()
        self.msg_order_manager = MsgOrderManager()

    @property
    def bid_ladder(self):
        return self.trans_order_manager.bid_ladder

    @property
    def ask_ladder(self):
        return self.trans_order_manager.ask_ladder

    def get_order_inc_fill_qty_in_tokens(self, order: Order) -> float:
        return self.msg_order_manager.get_order_inc_fill_qty_in_tokens(order=order)

    def update_order(self, order: Order):
        return self.msg_order_manager.update_order(order=order)

    def pop_order(self, oid: str):
        return self.msg_order_manager.pop_order(oid=oid)

    def contains_trans_order(self, oid: str) -> bool:
        return self.trans_order_manager.contains_order(oid=oid)

    def get_trans_order(self, oid: str) -> TransOrder:
        return self.trans_order_manager.get_order(oid=oid)

    def trans_order_at(self, oid: str) -> TransOrder:
        return self.trans_order_manager.order_at(oid=oid)

    def pop_trans_order(self, oid: str) -> Union[TransOrder, None]:
        return self.trans_order_manager.pop_order(oid=oid)

    def trans_order_items(self) -> Iterable[Tuple[str, TransOrder]]:
        return self.trans_order_manager.order_items()

    def float_prc_to_int_prc(self, float_prc: float) -> int:
        return self.trans_order_manager.float_prc_to_int_prc(float_prc=float_prc)

    def int_prc_to_float_prc(self, int_prc: int) -> float:
        return self.trans_order_manager.int_prc_to_float_prc(int_prc=int_prc)

    def keys(self) -> List[str]:
        return self.trans_order_manager.keys()

    def add_trans_order(self, order: TransOrder, oid: str):
        return self.trans_order_manager.add_order(order=order, oid=oid)

    def is_trans_order_empty(self) -> bool:
        return self.trans_order_manager.is_order_empty()

    def get_oids_for_iid(self, iid: str) -> List[str]:
        return self.trans_order_manager.get_oids_for_iid(iid=iid)


class XexInventoryManager:
    def __init__(self, contract: str):
        self.contract = contract
        self.total_inventory = 0
        self.partial_inventory = dict()

    def update_inventory(self, contract: str, ex: str, inven: float):
        assert contract == self.contract
        if ex not in self.partial_inventory:
            self.partial_inventory[ex] = 0
        self.total_inventory -= self.partial_inventory[ex]
        self.partial_inventory[ex] = inven
        self.total_inventory += self.partial_inventory[ex]

    def increment_inventory(self, contract: str, ex: str, qty_chg: float):
        assert contract == self.contract
        if ex not in self.partial_inventory:
            self.partial_inventory[ex] = 0
        self.partial_inventory[ex] += qty_chg
        self.total_inventory += qty_chg

    def get_inventory(self, contract: str, ex: str):
        assert contract == self.contract
        if ex not in self.partial_inventory:
            return 0
        return self.partial_inventory[ex]


