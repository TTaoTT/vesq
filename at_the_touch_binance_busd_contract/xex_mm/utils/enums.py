class Side:
    long = 1
    short = -1


class Direction:
    long = 1
    short = -1


class MakerTaker:
    maker = 0
    taker = 1
    partial_taker = 2
    undefined = 3


class ExState:
    active = "active"
    inactive = "inactive"


class DepthCcyType:
    ccy1 = "ccy1"
    ccy2 = "ccy2"


class DepthQtyGroupType:
    base = "base"
    grouped = "grouped"


class DepthPrcGroupType:
    base = "base"
    grouped = "grouped"


class DepthAtTouchGuardType:
    unguarded = "unguarded"
    guarded = "guarded"


class DepthCompletenessState:
    bbo_update = "bbo_update"
    trade_update = "trade_update"
    depth_update = "depth_update"


class DepthShadowState:
    real = "real"
    shadow = "shadow"


class DepthXExState:
    SEx = "SEx"
    XEx = "XEx"


class DepthPrcShiftState:
    unshifted = "unshifted"
    shifted = "shifted"


class DepthTakerFeeState:
    pre_taker_fee = "pre_taker_fee"
    post_taker_fee = "post_taker_fee"


class DepthFundingCostState:
    pre_funding_cost = "pre_funding_cost"
    post_funding_cost = "post_funding_cost"


class DepthFundingCompensationState:
    pre_funding_compensation = "pre_funding_compensation"
    post_funding_compensation = "post_funding_compensation"


class DepthLatencyCostState:
    pre_latency_cost = "pre_latency_cost"
    post_latency_cost = "post_latency_cost"