from xex_mm.calculators.em import EMAvgNoDecay
from xex_mm.utils.calc_node import CalRealTime


class CalEMAvgInventorySigma(CalRealTime):
    def __init__(self, halflife: float):
        self.emavg_inven_sigma = EMAvgNoDecay(halflife=halflife)

    def on_inventory(self, inven: float, ts: int):
        self.emavg_inven_sigma.on_val(val=abs(inven), ts=ts)

    def get_factor(self, ts: int):
        return self.emavg_inven_sigma.get_avg(ts=ts)

