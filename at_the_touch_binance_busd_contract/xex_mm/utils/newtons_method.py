import logging

import numpy as np


class VectorizedLinearNewtonsMethod:
    """
    Ax+b=0
    """

    def __init__(self, tol: float, max_iter: int, debug: bool = False):
        self.tol = tol
        self.max_iter = max_iter
        self._debug = debug

        # STATE
        self._A = ...
        self._b = ...
        self._n = ...
        self._n_iter = 0

    def set_A_b(self, A: np.ndarray, b: np.ndarray):
        assert len(A.shape) == 2
        m, n = A.shape
        assert m == n
        assert len(b.shape) == 1
        n, = b.shape
        assert n == m
        self._n = n
        self._A = A
        self._b = b
        self._n_iter = 0

    def eval(self, x):
        resid = np.sum(self._A.dot(x) + self._b)

        if self._debug:
            logging.info(f"resid={resid}")
            logging.info(f"--------------iter {self._n_iter}--------------")

        while (abs(resid) > self.tol) and (self._n_iter < self.max_iter):
            dx = self._A.T.dot(np.ones(self._n))

            if self._debug:
                logging.info(f"dx={dx}")

            x_step = - resid / dx / self._n

            if self._debug:
                logging.info(f"x_step={x_step}")

            x = x + x_step
            x = np.maximum(x, 0)

            if self._debug:
                logging.info(f"x={x}")

            resid = np.sum(self._A.dot(x) + self._b)

            if self._debug:
                logging.info(f"resid={resid}")

            self._n_iter += 1

            if self._debug:
                logging.info(f"--------------iter {self._n_iter}--------------")

        return x, resid, self._n_iter


if __name__ == '__main__':
    A = np.array([[1., -1., 1., -1.], [1., 0., 0., 0.], [1., 1., 1., 1.], [1., 2., 4., 8.]])
    b = np.array([14., 4., 2., 2.])
    o = VectorizedLinearNewtonsMethod(tol=1e-6, max_iter=100)
    o.set_A_b(A=A, b=b)
    x, resid, n_iter = o.eval(x=np.zeros(len(b)))
    print(x, resid, n_iter)