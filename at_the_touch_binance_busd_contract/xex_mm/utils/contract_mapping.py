from typing import Tuple


class ContractMapper:
    def _spot_CE_to_proxy_spot_CE(self, spot_contract: str, spot_ex: str) -> Tuple[str, str]:
        return f"{spot_contract}_swap", f"{spot_ex}_spot"

    def _proxy_spot_CE_to_spot_CE(self, proxy_spot_contract: str, proxy_spot_ex: str) -> Tuple[str, str]:
        return proxy_spot_contract.replace("_swap", ""), proxy_spot_ex.replace("_spot", "")

    def _spot_iid_to_proxy_spot_iid(self, spot_iid: str):
        ex, contract = spot_iid.split(".")
        proxy_contract, proxy_ex = self._spot_CE_to_proxy_spot_CE(spot_contract=contract, spot_ex=ex)
        return f"{proxy_ex}.{proxy_contract}"

    def _proxy_spot_iid_to_spot_iid(self, proxy_spot_iid: str):
        proxy_ex, proxy_contract = proxy_spot_iid.split(".")
        contract, ex = self._proxy_spot_CE_to_spot_CE(proxy_spot_contract=proxy_contract, proxy_spot_ex=proxy_ex)
        return f"{ex}.{contract}"

    def _is_spot_proxy_ex(self, ex: str):
        return ex.endswith("_spot")

    def is_spot_proxy_iid(self, iid: str):
        ex, contract = iid.split(".")
        return self._is_spot_proxy_ex(ex=ex)

    def _is_spot_contract(self, contract: str):
        parts = contract.split("_")
        return len(parts) == 2

    def _is_spot_iid(self, iid:str):
        ex, contract = iid.split(".")
        return self._is_spot_contract(contract=contract)

    def _busd_CE_to_proxy_busd_CE(self, busd_contract: str, busd_ex: str) -> Tuple[str, str]:
        return busd_contract.replace("busd", "usdt"), f"{busd_ex}_busd"

    def _proxy_busd_CE_to_busd_CE(self, proxy_busd_contract: str, proxy_busd_ex: str) -> Tuple[str, str]:
        return proxy_busd_contract.replace("usdt", "busd"), proxy_busd_ex.replace("_busd", "")

    def _busd_iid_to_proxy_busd_iid(self, busd_iid: str):
        ex, contract = busd_iid.split(".")
        proxy_contract, proxy_ex = self._busd_CE_to_proxy_busd_CE(busd_contract=contract, busd_ex=ex)
        return f"{proxy_ex}.{proxy_contract}"

    def _proxy_busd_iid_to_busd_iid(self, proxy_busd_iid: str):
        proxy_ex, proxy_contract = proxy_busd_iid.split(".")
        contract, ex = self._proxy_busd_CE_to_busd_CE(proxy_busd_contract=proxy_contract, proxy_busd_ex=proxy_ex)
        return f"{ex}.{contract}"

    def _is_busd_proxy_ex(self, ex: str):
        return ex.endswith("_busd")

    def _is_busd_proxy_iid(self, iid: str):
        ex, contract = iid.split(".")
        return self._is_busd_proxy_ex(ex=ex)

    def _is_busd_contract(self, contract: str):
        parts = contract.split("_")
        return parts[1] == "busd"

    def _is_busd_iid(self, iid: str):
        ex, contract = iid.split(".")
        return self._is_busd_contract(contract=contract)

    ######################################### Generic Mapping #####################################################
    def map_iid(self, iid: str):
        if self._is_spot_iid(iid):
            iid = self._spot_iid_to_proxy_spot_iid(spot_iid=iid)
        if self._is_busd_iid(iid):
            iid = self._busd_iid_to_proxy_busd_iid(busd_iid=iid)
        return iid

    def _peel_proxy_CE(self, proxy_contract: str, proxy_ex: str) -> Tuple[bool, str, str]:
        if self._is_spot_proxy_ex(ex=proxy_ex):
            proxy_contract, proxy_ex = self._proxy_spot_CE_to_spot_CE(proxy_spot_contract=proxy_contract, proxy_spot_ex=proxy_ex)
            return True, proxy_contract, proxy_ex
        if self._is_busd_proxy_ex(ex=proxy_ex):
            proxy_contract, proxy_ex = self._proxy_busd_CE_to_busd_CE(proxy_busd_contract=proxy_contract, proxy_busd_ex=proxy_ex)
            return True, proxy_contract, proxy_ex
        return False, proxy_contract, proxy_ex

    def proxy_CE_to_CE(self, proxy_contract: str, proxy_ex: str):
        while True:
            peeled, proxy_contract, proxy_ex = self._peel_proxy_CE(proxy_contract=proxy_contract, proxy_ex=proxy_ex)
            if not peeled:
                return proxy_contract, proxy_ex

    def proxy_iid_to_iid(self, proxy_iid: str):
        e, c = proxy_iid.split(".")
        c, e = self.proxy_CE_to_CE(proxy_contract=c, proxy_ex=e)
        return f"{e}.{c}"

    def _layer_proxy_CE(self, contract: str, ex: str) -> Tuple[bool, str, str]:
        if self._is_spot_contract(contract=contract):
            contract, ex = self._spot_CE_to_proxy_spot_CE(spot_contract=contract, spot_ex=ex)
            return True, contract, ex
        if self._is_busd_contract(contract=contract):
            contract, ex = self._busd_CE_to_proxy_busd_CE(busd_contract=contract, busd_ex=ex)
            return True, contract, ex
        return False, contract, ex

    def CE_to_proxy_CE(self, contract: str, ex: str):
        while True:
            layered, contract, ex = self._layer_proxy_CE(contract=contract, ex=ex)
            if not layered:
                return contract, ex

    def iid_to_proxy_iid(self, iid: str):
        e, c = iid.split(".")
        c, e = self.CE_to_proxy_CE(contract=c, ex=e)
        return f"{e}.{c}"


if __name__ == '__main__':
    print(ContractMapper().proxy_CE_to_CE(f"btc_usdt_swap", "binance_spot_busd"))