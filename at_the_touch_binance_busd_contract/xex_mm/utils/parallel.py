import traceback
from multiprocessing import Pool
from typing import List


class FunctorDistributionEngine:
    def __init__(self, n_procs: int = 10, maxtasksperchild: int = 1, chunksize: int = 1, silent_fail: bool = False):
        self.n_procs = n_procs
        self.maxtasksperchild = maxtasksperchild
        self.chunksize = chunksize
        self.silent_fail = silent_fail

    def _run_ftor(self, ftor):
        try:
            return ftor()
        except Exception as e:
            print(traceback.format_exc())
            print('ftor', ftor)
            if not self.silent_fail:
                raise e

    def run(self, ftors: List):
        if (self.n_procs <= 1) or (len(ftors) <= 1):
            return [self._run_ftor(ftor=ftor) for ftor in ftors]

        with Pool(self.n_procs, maxtasksperchild=self.maxtasksperchild) as pool:
            return pool.map(func=self._run_ftor, iterable=ftors, chunksize=self.chunksize)