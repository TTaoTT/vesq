import math
from typing import List, Tuple


def float_gcd(a, b, tol):
    if (a < b):
        return float_gcd(b, a, tol)

    # base case
    if (abs(b) < tol):
        return a
    else:
        return float_gcd(b, a - math.floor(a / b) * b, tol)


def tick_size_gcd(tick_sizes: List[float]) -> float:
    if len(tick_sizes) == 0:
        raise RuntimeError()
    if len(tick_sizes) == 1:
        return tick_sizes[0]
    pow_10s = []
    for x in tick_sizes:
        _expo = math.log10(x)
        pow_10 = math.ceil(_expo) if _expo >= 0 else math.floor(_expo)
        pow_10s.append(pow_10)
    tol = 10**min(pow_10s)
    a = tick_sizes[0]
    for b in tick_sizes[1:]:
        a = float_gcd(a, b, tol)
    return a


if __name__ == '__main__':
    print(tick_size_gcd([
        10,
        3,
        0.01,
        0.005,
    ]))