from xex_mm.utils.enums import Side, Direction


def side_to_direction(side: Side) -> Direction:
    if side == Side.long:
        return Direction.long
    if side == Side.short:
        return Direction.short
    raise NotImplementedError(f"Unsupported side: {side}")


def direction_to_side(d: Direction) -> Side:
    if d == Direction.long:
        return Side.long
    if d == Direction.short:
        return Side.short
    raise NotImplementedError(f"Unsupported direction: {d}")


def flip_side(side: Side):
    if side == Side.long:
        return Side.short
    if side == Side.short:
        return Side.long
    raise NotImplementedError(f"Unsupported side: {side}")


def flip_direction(d: Direction):
    if d == Direction.long:
        return Direction.short
    if d == Direction.short:
        return Direction.long
    raise NotImplementedError(f"Unsupported direction: {d}")

