import logging
import os
import pickle
import traceback
from typing import List, Dict
from datetime import datetime, timedelta

import pandas as pd
from xex_mm.utils.base import Depth, Trade, BBO, Order

from xex_mm.utils.configs import PathConfig
from xex_mm.utils.enums import Direction


class DfCalcNode:

    def __repr__(self):
        return self._get_name()

    def _get_path(self, datetime: str):
        return f"{PathConfig.data_path}/CalcNode/{datetime}/{self._get_name()}.feather"

    def get(self, datetime: str, refresh: bool = False) -> pd.DataFrame:
        path = self._get_path(datetime=datetime)
        if os.path.exists(path) and not refresh:
            logging.info(f"Reading: {path}")
            try:
                return pd.read_feather(path).set_index("ts")
            except Exception as e:
                traceback.print_stack()
                pass
        logging.info(f"Calculating {self.__class__.__name__}...")
        df = self._calc(datetime=datetime)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        logging.info(f"Writing: {path}")
        df.reset_index("ts").to_feather(path)
        return df
    
    def get_chunks(self,start_datetime: str, end_datetime: str, refresh: bool = False) -> pd.DataFrame:
        start_dt = datetime.strptime(start_datetime, '%Y%m%d%H')
        end_dt = datetime.strptime(end_datetime, '%Y%m%d%H')
        cur_dt = start_dt
        df = []
        while cur_dt <= end_dt:
            dt = cur_dt.strftime('%Y%m%d%H')
            df.append(self.get(datetime=dt, refresh=refresh))
            cur_dt += timedelta(hours=1)
        return pd.concat(df)
        

    ##################################### IMPLEMENT BELOW #####################################

    def _get_name(self) -> str:
        raise NotImplementedError()

    def _calc(self, datetime: str) -> pd.DataFrame:
        raise NotImplementedError()
    
    def _validation(self, datetime: str) -> pd.DataFrame:
        raise NotImplementedError()


class ModelCalcNode:
    def __init__(self, x_nodes: List[DfCalcNode], y_nodes: List[DfCalcNode]):
        self._x_nodes = x_nodes
        self._y_nodes = y_nodes

    def _get_path(self, datetime: str):
        return f"{PathConfig.data_path}/CalcNode/{datetime}/{self._get_name()}.pickle"

    def get(self, start_datetime: str, end_datetime: str, refresh: bool = False):
        path = self._get_path(datetime=f"{start_datetime}_{end_datetime}")
        if os.path.exists(path) and not refresh:
            with open(path, "rb") as f:
                return pickle.load(f)
        obj = self._calc(start_datetime=start_datetime, end_datetime=end_datetime)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, "wb") as f:
            pickle.dump(obj, f)
        return obj

    ##################################### IMPLEMENT BELOW #####################################

    def _get_name(self) -> str:
        raise NotImplementedError()

    def _calc(self, start_datetime: str, end_datetime: str):
        raise NotImplementedError()
    
    
class CalRealTime:
    def on_wakeup(self, ts: int):
        pass

    def add_trade(self, trade: Trade):
        pass
    
    def add_depth(self, depth: Depth):
        pass

    def add_bbo(self, bbo: BBO):
        pass

    def on_order(self, order: Order):
        pass

    def on_fill(self, d: Direction, qty: float, oid: str):
        pass

    def get_factor(self, ts: int):
        pass
