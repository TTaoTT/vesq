import logging
from typing import Tuple, Union

from MiGo import set_jupyter_default_env
set_jupyter_default_env()

import os
from datetime import datetime, timedelta

import h5py
import pandas as pd
import tables

from MiGo.client import InternalClient


class DataLoader:
    def __init__(self):
        self._client = InternalClient()

    def __h5_path(self, ex: str, pair: str, date: int):
        return f"{DataPaths.depth_data_dir}/{date}/{ex}.{pair}_{date}.h5"

    def _cache_path(self, ex: str, pair: str, dt: str, name: str):
        return f"{DataPaths.depth_data_dir}/{dt}/{ex}.{pair}_{dt}_{name}.feather"

    def _try_read_cache(self, ex: str, pair: str, dt: str, name: str) -> Tuple[bool, Union[None, pd.DataFrame]]:
        path = self._cache_path(ex=ex, pair=pair, dt=dt, name=name)
        if os.path.exists(path):
            return True, pd.read_feather(path).set_index("ts")
        return False, None

    def _cache_df(self, ex: str, pair: str, dt: str, name: str, df: pd.DataFrame):
        path = self._cache_path(ex=ex, pair=pair, dt=dt, name=name)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        df.reset_index("ts").to_feather(path)

    def open_h5(self, ex: str, pair: str, date: int):
        path = f"{DataPaths.depth_data_dir}/{date}/{ex}.{pair}_{date}.h5"
        f_1 = h5py.File(path, "r")
        dset = f_1.get('config')[:]
        pf = dset[0][3]
        f_1.close()
        f = tables.File(path, mode="r")
        return f, pf

    def read_bbo(self, ex: str, pair: str, date: int):
        bbo_path = self._cache_path(ex=ex, pair=pair, dt=str(date), name="bbo")
        # if os.path.exists(bbo_path):
        #     return pd.read_feather(bbo_path).set_index("tx_time")
        f, pf = self.open_h5(ex=ex, pair=pair, date=date)
        df = pd.DataFrame(f.get_node("/bbo")[0:])
        df["ask_price"] = df['ask_price'] / pf
        df["bid_price"] = df['bid_price'] / pf
        df['datetime'] = df['timestamp'].map(lambda x: datetime.fromtimestamp(x / 1e3))
        df["mp"] = (df["ask_price"] + df["bid_price"]) / 2
        df["imb"] = (df["bid_qty"] - df["ask_qty"]) / (df["bid_qty"] + df["ask_qty"])
        df.set_index("tx_time", inplace=True)
        df = df.loc[~df.index.duplicated(keep="last"), :]
        # df.reset_index().to_feather(bbo_path)
        f.close()
        return df

    def read_depth(self, ex: str, pair: str, start_date: str, end_date: str) -> pd.DataFrame:
        start = datetime.strptime(start_date, '%Y%m%d%H')
        end = datetime.strptime(end_date, '%Y%m%d%H')
        res = []
        while start < end:
            tmp = pd.read_csv(f"{DataPaths.depth_merged_dir}/depth_{ex}.{pair}_{start.strftime('%Y%m%d%H')}")
            start += timedelta(hours=1)
            res.append(tmp)
        df = pd.concat(res)
        df["ask_price"] = df['ap1']
        df["bid_price"] = df['bp1']
        df['datetime'] = df['localtime'].map(lambda x: datetime.fromtimestamp(x / 1e3))
        df["mp"] = (df["ask_price"] + df["bid_price"]) / 2
        df.set_index("localtime", inplace=True)
        df = df.loc[~df.index.duplicated(keep="last"), :]

        return df[["ask_price","bid_price","datetime","mp"]]

    def read_one_day(self, ex, symbol, start_ts):
        depth_file = f"~/user_storage/wangyifei_data/orderbook_h5/{ex}"
        start_dt = datetime.strptime(start_ts, "%Y%m%d") + timedelta(hours=8)
        depth = list()
        for i in range(12):
            sdt = datetime.strftime(start_dt + i * timedelta(hours=2), "%Y%m%d%H")
            edt = datetime.strftime(start_dt + (1 + i) * timedelta(hours=2), "%Y%m%d%H")

            success, df = self._try_read_cache(ex=ex, pair=symbol, dt=f"{sdt}-{edt}", name="depth")

            if not success:
                file_name = os.path.join(depth_file, symbol, f"beta_boost_{ex}_{symbol}_{sdt}_{edt}.h5")
                df = pd.read_hdf(file_name).set_index("ts")
                self._cache_df(ex=ex, pair=symbol, dt=f"{sdt}-{edt}", name="depth", df=df)

            depth.append(df)
        df = pd.concat(depth, axis=0)

        return df

    def read_depth2(self, ex: str, contract: str, start_str: str, end_str: str):
        logging.info(f"Reading depth: ex={ex}, contract={contract}, start_str={start_str}, end_str={end_str}")
        depth = list()
        start_ts, end_ts = self._client.parse_time(start_str), self._client.parse_time(end_str)
        for i in range(start_ts, end_ts, 86400):
            depth.append(self.read_one_day(ex, contract, datetime.strftime(datetime.utcfromtimestamp(i), "%Y%m%d")))
        logging.info(f"Read depth: ex={ex}, contract={contract}, start_str={start_str}, end_str={end_str}")
        return pd.concat(depth, axis=0)

    def read_trades(self, ex: str, pair: str, date: int):
        f, pf = self.open_h5(ex=ex, pair=pair, date=date)
        df = pd.DataFrame(f.get_node("/agg_trade")[0:])
        df['datetime'] = df['tx_time'].map(lambda x: datetime.fromtimestamp(x / 1e3))
        df["price"] = df["price"] / pf
        df["amount"] = df["price"] * df["qty"]
        df.set_index("tx_time", inplace=True)
        df = df.loc[~df.index.duplicated(keep="last"), :]
        f.close()
        return df

    def read_own_trades(self, filename: str) -> pd.DataFrame:
        path = f"{DataPaths.temp_data_dir}/{filename}.csv"
        df = pd.read_csv(path)
        df.loc[df.side == "BUY", "sign"] = 1
        df.loc[df.side == "SELL", "sign"] = -1
        df.set_index("time", inplace=True)
        df.index.name = "tx_time"
        return df

    def read_own_trades_bt(self, filename: str, date: str) -> pd.DataFrame:
        path = f"{DataPaths.temp_bt_dir}/{date}/{filename}.csv"
        df = pd.read_csv(path)
        df = df[(df.trade_type != "CloseOnEnd") & (df.price*df.qty>1)]
        df["int_side"] = df.side
        df.drop(columns=['side'], inplace=True)
        df.loc[df.int_side == 1, "side"] = "BUY"
        df.loc[df.int_side != 1, "side"] = "SELL"
        df.drop(columns=['int_side'], inplace=True)
        df.loc[df.side == "BUY", "sign"] = 1
        df.loc[df.side == "SELL", "sign"] = -1
        df["maker"] = df["trade_type"] == "maker"
        df.set_index("timestamp", inplace=True)
        df.index.name = "tx_time"
        return df


class DataPaths:
    depth_data_dir = "/home/psdz/hf_data/raw_data"
    temp_data_dir = "/home/psdz/hf_task/temp_history_order"
    temp_bt_dir = "/home/psdz/user_storage/TT/"
    depth_merged_dir = "/home/psdz/user_storage/TT/calibration_data"
