//
// Created by aurth on 8/24/2022.
//

#ifndef CYTHON_XCON_XCONMMMODEL_H
#define CYTHON_XCON_XCONMMMODEL_H

#include <iostream>
#include <nlopt.hpp>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <queue>
#include "Enums.h"
#include "Eigen/Dense"

typedef std::unordered_map<std::string, double> CMap;
typedef std::unordered_map<std::string, std::unordered_map<std::string, double>> CEMap;
typedef std::unordered_map<std::string, std::unordered_map<std::string, std::unordered_map<int, double>>> CEDMap;
typedef std::unordered_map<std::string, double> CovMap;
typedef std::unordered_map<std::string, std::unordered_map<std::string, std::unordered_map<int, double>>> FeeMap;
typedef std::unordered_map<std::string, std::unordered_map<std::string, std::unordered_map<std::string, double>>> DepthMetrics;
typedef std::unordered_set<std::string> CESet;
typedef std::pair<std::string, std::string> CE;
typedef std::vector<CE> CEs;
typedef std::tuple<std::string, std::string, int> Entry;
typedef std::set<Entry> Entries;


class DeltaObjectiveFuncForNlopt {
private:
    const Eigen::VectorXd& q_arr;
    const Eigen::VectorXd& q_tick_arr;
    const Eigen::VectorXd& tilde_p_arr;
    const Eigen::VectorXd& s_d_arr;
    const Eigen::VectorXd& p_hat_arr;
    const Eigen::VectorXd& s_o_hat_arr;
    const Eigen::VectorXd& c_maker_arr;
    const Eigen::VectorXd& c_taker_arr;
    const Eigen::VectorXd& one_d_arr;
    const Eigen::VectorXd& I_arr;
    const Eigen::VectorXd& tilde_cov_arr;
    const Eigen::VectorXd& beta_arr;
    const Eigen::VectorXd& taker_bnd_arr;
    const Eigen::VectorXd& lambda_arr;
    const Eigen::VectorXd& epsilon_arr;
    const Eigen::VectorXd& a_d_arr;
    const Eigen::VectorXd& b_d_arr;

    int n;
    const Eigen::MatrixXd diag_one_d_arr;
    const Eigen::VectorXd one_arr;
    const Eigen::VectorXd dr_arr;
    const Eigen::VectorXd beta_dr_arr;
    const Eigen::VectorXd amt_entry_arr;
    const Eigen::MatrixXd exit_prc_mat;

    int eval_count = 0;

public:
    DeltaObjectiveFuncForNlopt(
        const Eigen::VectorXd& q_arr,
        const Eigen::VectorXd& q_tick_arr,
        const Eigen::VectorXd& tilde_p_arr,
        const Eigen::VectorXd& s_d_arr,
        const Eigen::VectorXd& p_hat_arr,
        const Eigen::VectorXd& s_o_hat_arr,
        const Eigen::VectorXd& c_maker_arr,
        const Eigen::VectorXd& c_taker_arr,
        const Eigen::VectorXd& one_d_arr,
        const Eigen::VectorXd& I_arr,
        const Eigen::VectorXd& tilde_cov_arr,
        const Eigen::VectorXd& beta_arr,
        const Eigen::VectorXd& taker_bnd_arr,
        const Eigen::VectorXd& lambda_arr,
        const Eigen::VectorXd& epsilon_arr,
        const Eigen::VectorXd& a_d_arr,
        const Eigen::VectorXd& b_d_arr,
    );

    double operator() (const std::vector<double> &x, std::vector<double> &grad, void* f_data);
};



class XConMMModel {
private:
    CEDMap ar_map;
    CEMap ref_prc_map;
    CEDMap half_sprd_map;
    CovMap cov_map;
    FeeMap fee_map;
    CMap q_grp_size_map;
    DepthMetrics depth_metrics;

    int n;
    CESet ces_to_solve_set;
    CEs ces_to_solve_vec;

    Entries entries;
    Eigen::VectorXd q_grp_size_arr;
    Eigen::VectorXd lambda_arr;
    Eigen::VectorXd p_hat_arr;
    Eigen::VectorXd tilde_p_arr;
    Eigen::VectorXd s_l_arr;
    Eigen::VectorXd s_s_arr;
    Eigen::VectorXd s_d_arr;
    Eigen::VectorXd SEx_tilde_p_arr;
    Eigen::VectorXd SEx_s_l_arr;
    Eigen::VectorXd SEx_s_s_arr;
    Eigen::VectorXd s_o_hat_arr;
    Eigen::VectorXd c_maker_arr;
    Eigen::VectorXd c_taker_arr;
    Eigen::MatrixXd cov_mat;
    Eigen::VectorXd a_d_arr;
    Eigen::VectorXd b_d_arr;
    Eigen::VectorXd epsilon_arr;
    Eigen::VectorXd one_d_arr;
    Eigen::VectorXd taker_bnd_arr;

    double epsilon = -0.5;

    /***************************************** STATE MANAGEMENT *****************************************/


    /***************************************** COMPUTATION *****************************************/

    double GetLambdaForD(const std::string& contract, const std::string& ex, int d);
    double OptimalPriceLevel(
        const Eigen::VectorXd& qs,
        const Eigen::VectorXd& betas,
        const Eigen::VectorXd& Ts,
        const Eigen::VectorXd& taus,
        const Eigen::VectorXd& Is,
        Eigen::VectorXd& deltas
    );

public:
    XConMMModel();

    /***************************************** STATE MANAGEMENT *****************************************/

    void UpdatePredArrivalRate(const std::string& contract, const std::string& ex, int d, double val);
    double GetPredArrivalRate(const std::string& contract, const std::string& ex, int d);

    void UpdatePredRefPrc(const std::string& contract, const std::string& ex, double ref_prc);
    double GetPredRefPrc(const std::string& contract, const std::string& ex);

    void UpdatePredHalfSpread(const std::string& contract, const std::string& ex, int d, double val);
    double GetPredHalfSpread(const std::string& contract, const std::string& ex, int d);

    static std::string GetCovKey(const std::string& contract1, const std::string& ex1, const std::string& contract2, const std::string& ex2);
    void UpdatePredCov(const std::string& contract1, const std::string& ex1, const std::string& contract2, const std::string& ex2, double val);
    double GetPredCov(const std::string& contract1, const std::string& ex1, const std::string& contract2, const std::string& ex2);

    void UpdateFeeRate(const std::string& contract, const std::string& ex, int maker_taker, double val);
    double GetFeeRate(const std::string& contract, const std::string& ex, int maker_taker);

    void UpdateQGrpSize(const std::string& contract, double val);
    double GetQGrpSize(const std::string& contract);

    void UpdateDepthMetrics(const std::string& contract, const std::string& ex, const std::string& metric, double val);
    double GetDepthMetrics(const std::string& contract, const std::string& ex, const std::string& metric);
    
    void AddCEToSolve(const std::string& contract, const std::string& ex);
    void ResetCEsToSolve();
    void UpdateVectors();

    /***************************************** COMPUTATION *****************************************/



};


#endif //CYTHON_XCON_XCONMMMODEL_H
