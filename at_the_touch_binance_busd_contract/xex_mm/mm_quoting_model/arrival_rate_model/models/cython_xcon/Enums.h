//
// Created by aurth on 8/25/2022.
//

#ifndef CYTHON_XCON_ENUMS_H
#define CYTHON_XCON_ENUMS_H

enum Direction {l, s};

enum MakerTaker {maker, taker};

#endif //CYTHON_XCON_ENUMS_H
