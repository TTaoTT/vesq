# distutils: language = c++
# cython: always_allow_keywords=True

from cython_xcon_mm_model_src cimport XConMMModel
from libcpp.string cimport string


cdef class CyXConMMModel:

    cdef XConMMModel c_xcon  # Holds a C++ instance

    def __init__(self):
        self.c_xcon = XConMMModel()

    ############################################ Params Control #################################################

    def update_pred_arrival_rate(self, const string& contract, const string& ex, int d, double val):
        self.c_xcon.UpdatePredArrivalRate(contract=contract, ex=ex, d=d, val=val)

    def get_pred_arrival_rate(self, const string& contract, const string& ex, int d):
        return self.c_xcon.GetPredArrivalRate(contract=contract, ex=ex, d=d)

    def update_pred_ref_prc(self, const string& contract, const string& ex, double val):
        self.c_xcon.UpdatePredRefPrc(contract=contract, ex=ex, val=val)

    def get_pred_ref_prc(self, const string& contract, const string& ex):
        return self.c_xcon.GetPredRefPrc(contract=contract, ex=ex)

    def update_pred_half_sprd(self, const string& contract, const string& ex, int d, double val):
        self.c_xcon.UpdatePredHalfSpread(contract=contract, ex=ex, d=d, val=val)

    def get_pred_half_sprd(self, const string& contract, const string& ex, int d):
        return self.c_xcon.GetPredHalfSpread(contract=contract, ex=ex, d=d)

    @staticmethod
    def get_cov_key(const string& contract1, const string& ex1, const string& contract2, const string& ex2):
        return XConMMModel.GetCovKey(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2)

    def update_pred_cov(self, const string& contract1, const string& ex1, const string& contract2, const string& ex2, double val):
        self.c_xcon.UpdatePredCov(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2, val=val)

    def get_pred_cov(self, const string& contract1, const string& ex1, const string& contract2, const string& ex2):
        return self.c_xcon.GetPredCov(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2)

    def update_fee_rate(self, const string& contract, const string& ex, int maker_taker, double val):
        self.c_xcon.UpdateFeeRate(contract=contract, ex=ex, maker_taker=maker_taker, val=val)

    def get_fee_rate(self, const string& contract, const string& ex, int maker_taker):
        return self.c_xcon.GetFeeRate(contract=contract, ex=ex, maker_taker=maker_taker)

    def update_q_grp_size(self, const string& contract, double val):
        self.c_xcon.UpdateQGrpSize(contract=contract, val=val)

    def get_q_grp_size(self, const string& contract):
        return self.c_xcon.GetQGrpSize(contract=contract)

    def update_depth_metrics(self, const string& contract, const string& ex, const string& metric, double val):
        self.c_xcon.UpdateDepthMetrics(contract=contract, ex=ex, metric=metric, val=val)

    def get_depth_metrics(self, const string& contract, const string& ex, const string& metric):
        return self.c_xcon.GetDepthMetrics(contract=contract, ex=ex, metric=metric)







