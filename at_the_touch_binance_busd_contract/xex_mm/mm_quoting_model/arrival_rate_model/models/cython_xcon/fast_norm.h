//
// Created by aurth on 9/6/2022.
//

#ifndef CYTHON_XCON_FAST_NORM_H
#define CYTHON_XCON_FAST_NORM_H


#include "cmath"


double std_norm_pdf(double z) {
    return 1 / std::sqrt(2 * M_PI) * std::exp(- std::pow(z, 2) / 2);
}


double fast_std_norm_cdf(double z) {
    if (z >= 0) {
        return 0.5 + 0.5 * std::sqrt(1 - std::exp(-5./8. * std::pow(z, 2.)));
    }
    return 1 - fast_std_norm_cdf(-z);
}


#endif //CYTHON_XCON_FAST_NORM_H
