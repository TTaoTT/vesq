//
// Created by aurth on 8/24/2022.
//

#include "XConMMModel.h"
#include "fast_norm.h"

#include <utility>

XConMMModel::XConMMModel() {

}

void XConMMModel::AddCEToSolve(const std::string &contract, const std::string &ex) {
    std::string key = "(" + contract + "," + ex + ")";
    if (ces_to_solve_set.count(key) == 0) {
        ces_to_solve_set.insert(key);
        ces_to_solve_vec.push_back({contract, ex});
        n += 2;
        return;
    }
}

void XConMMModel::ResetCEsToSolve() {
    ces_to_solve_set.clear();
    ces_to_solve_vec.clear();
    n = 0;
}


void XConMMModel::UpdatePredArrivalRate(const std::string &contract, const std::string &ex, int d, double val) {
    ar_map[contract][ex][d] = val;
}

double XConMMModel::GetPredArrivalRate(const std::string &contract, const std::string &ex, int d) {
    return ar_map[contract][ex][d];
}

void XConMMModel::UpdatePredRefPrc(const std::string& contract, const std::string& ex, double val) {
    ref_prc_map[contract][ex] = val;
}

double XConMMModel::GetPredRefPrc(const std::string& contract, const std::string& ex) {
    return ref_prc_map[contract][ex];
}

void XConMMModel::UpdatePredHalfSpread(const std::string &contract, const std::string &ex, int d, double val) {
    half_sprd_map[contract][ex][d] = val;
}

double XConMMModel::GetPredHalfSpread(const std::string &contract, const std::string &ex, int d) {
    return half_sprd_map[contract][ex][d];
}

std::string XConMMModel::GetCovKey(const std::string &contract1, const std::string &ex1, const std::string &contract2,
                              const std::string &ex2) {
    typedef std::pair<std::string, std::string> CE;
    CE key1 {contract1, ex1};
    CE key2 {contract2, ex2};
    if (key1 <= key2)
        return "((" + key1.first + "," + key1.second + "),(" + key2.first + "," + key2.second + "))";
    return "((" + key2.first + "," + key2.second + "),(" + key1.first + "," + key1.second + "))";
}

void XConMMModel::UpdatePredCov(const std::string &contract1, const std::string &ex1,
                                const std::string &contract2, const std::string &ex2, double val) {
    cov_map[GetCovKey(contract1, ex1, contract2, ex2)] = val;
}

double XConMMModel::GetPredCov(const std::string &contract1, const std::string &ex1,
                               const std::string &contract2, const std::string &ex2) {
    return cov_map[GetCovKey(contract1, ex1, contract2, ex2)];
}

void XConMMModel::UpdateFeeRate(const std::string &contract, const std::string &ex, int maker_taker, double val) {
    fee_map[contract][ex][maker_taker] = val;
}

double XConMMModel::GetFeeRate(const std::string &contract, const std::string &ex, int maker_taker) {
    return fee_map[contract][ex][maker_taker];
}

void XConMMModel::UpdateQGrpSize(const std::string &contract, double val) {
    if ((q_grp_size_map.count(contract) > 0) and (std::fabs((q_grp_size_map[contract] - val)) > 1e-10)) {
        throw std::runtime_error("Q_grp_size can only be set once! Trying to set twice. Value before: " + std::to_string(q_grp_size_map[contract]) + ", value after: " + std::to_string(val) + "\n");
    }
    q_grp_size_map[contract] = val;
}

double XConMMModel::GetQGrpSize(const std::string &contract) {
    return q_grp_size_map[contract];
}

void XConMMModel::UpdateDepthMetrics(const std::string &contract, const std::string &ex, const std::string &metric, double val) {
    depth_metrics[contract][ex][metric] = val;
}

double XConMMModel::GetDepthMetrics(const std::string &contract, const std::string &ex, const std::string &metric) {
    return depth_metrics[contract][ex][metric];
}

double XConMMModel::GetLambdaForD(const std::string &contract, const std::string &ex, int d) {
    d = (d == Direction::s) ? Direction::l : Direction::s;
    return GetPredArrivalRate(contract, ex, d=d) / GetQGrpSize(contract);
}

void XConMMModel::UpdateVectors() {

    entries.clear();
    q_grp_size_arr.resize(n);
    lambda_arr.resize(n);
    p_hat_arr.resize(n);
    tilde_p_arr.resize(n);
    s_l_arr.resize(n);
    s_s_arr.resize(n);
    s_d_arr.resize(n);
    SEx_tilde_p_arr.resize(n);
    SEx_s_l_arr.resize(n);
    SEx_s_s_arr.resize(n);
    s_o_hat_arr.resize(n);
    c_maker_arr.resize(n);
    c_taker_arr.resize(n);
    cov_mat.resize(n, n);
    a_d_arr.resize(n);
    b_d_arr.resize(n);
    epsilon_arr.resize(n);
    one_d_arr.resize(n);
    taker_bnd_arr.resize(n);

    if (ces_to_solve_vec.size() * 2 != n) {
        throw std::runtime_error("ces_to_solve_vec.size() and n mismatch: ces_to_solve_vec.size=" +
            std::to_string(ces_to_solve_vec.size()) + ",n=" + std::to_string(n));
    }

    std::string xex = "xex";

    for (int i=0; i<ces_to_solve_vec.size(); i++) {
        const CE& pair = ces_to_solve_vec[i];
        entries.insert({pair.first, pair.second, Direction::l});
        entries.insert({pair.first, pair.second, Direction::s});
        q_grp_size_arr(2 * i) = GetQGrpSize(pair.first);
        q_grp_size_arr(2 * i + 1) = GetQGrpSize(pair.first);
        lambda_arr(2 * i) = GetLambdaForD(pair.first, pair.second, Direction::l);
        lambda_arr(2 * i + 1) = GetLambdaForD(pair.first, pair.second, Direction::s);
        p_hat_arr(2 * i) = GetPredRefPrc(pair.first, pair.second);
        p_hat_arr(2 * i + 1) = GetPredRefPrc(pair.first, pair.second);
        tilde_p_arr(2 * i) = GetDepthMetrics(pair.first, xex, "tilde_p");
        tilde_p_arr(2 * i + 1) = GetDepthMetrics(pair.first, xex, "tilde_p");
        s_l_arr(2 * i) = GetDepthMetrics(pair.first, xex, "s_l");
        s_l_arr(2 * i + 1) = GetDepthMetrics(pair.first, xex, "s_l");
        s_s_arr(2 * i) = GetDepthMetrics(pair.first, xex, "s_s");
        s_s_arr(2 * i + 1) = GetDepthMetrics(pair.first, xex, "s_s");
        s_d_arr(2 * i) = s_l_arr(2 * i);
        s_d_arr(2 * i + 1) = s_s_arr(2 * i + 1);
        SEx_tilde_p_arr(2 * i) = GetDepthMetrics(pair.first, pair.second, "tilde_p");
        SEx_tilde_p_arr(2 * i + 1) = GetDepthMetrics(pair.first, pair.second, "tilde_p");
        SEx_s_l_arr(2 * i) = GetDepthMetrics(pair.first, pair.second, "s_l");
        SEx_s_l_arr(2 * i + 1) = GetDepthMetrics(pair.first, pair.second, "s_l");
        SEx_s_s_arr(2 * i) = GetDepthMetrics(pair.first, pair.second, "s_s");
        SEx_s_s_arr(2 * i + 1) = GetDepthMetrics(pair.first, pair.second, "s_s");
        s_o_hat_arr(2 * i) = GetPredHalfSpread(pair.first, pair.second, Direction::s);
        s_o_hat_arr(2 * i + 1) = GetPredHalfSpread(pair.first, pair.second, Direction::l);
        c_maker_arr(2 * i) = GetFeeRate(pair.first, pair.second, MakerTaker::maker);
        c_maker_arr(2 * i + 1) = GetFeeRate(pair.first, pair.second, MakerTaker::maker);
        c_taker_arr(2 * i) = GetPredHalfSpread(pair.first, pair.second, MakerTaker::taker);
        c_taker_arr(2 * i + 1) = GetPredHalfSpread(pair.first, pair.second, MakerTaker::taker);
        for (int j=0; j<ces_to_solve_vec.size(); j++) {
            const CE& pair2 = ces_to_solve_vec[j];
            double cov = GetPredCov(pair.first, pair.second, pair2.first, pair2.second);
            cov_mat(2 * i, 2 * j) = cov;
            cov_mat(2 * i + 1, 2 * j) = -cov;
            cov_mat(2 * i, 2 * j + 1) = -cov;
            cov_mat(2 * i + 1, 2 * j + 1) = cov;
        }
        a_d_arr(2 * i) = GetDepthMetrics(pair.first, xex, "a_l");
        a_d_arr(2 * i + 1) = GetDepthMetrics(pair.first, xex, "a_s");
        b_d_arr(2 * i) = GetDepthMetrics(pair.first, xex, "b_l");
        b_d_arr(2 * i + 1) = GetDepthMetrics(pair.first, xex, "b_s");
        epsilon_arr(2 * i) = epsilon;
        epsilon_arr(2 * i + 1) = epsilon;
        one_d_arr(2 * i) = 1;
        one_d_arr(2 * i + 1) = -1;
        taker_bnd_arr(2 * i) = tilde_p_arr(2 * i) - s_l_arr(2 * i) - (SEx_tilde_p_arr(2 * i) + SEx_s_s_arr(2 * i));
        taker_bnd_arr(2 * i + 1) = SEx_tilde_p_arr(2 * i + 1) - SEx_s_l_arr(2 * i + 1) - (tilde_p_arr(2 * i + 1) + s_s_arr(2 * i + 1));
    }
}

double XConMMModel::OptimalPriceLevel(const Eigen::VectorXd &qs, const Eigen::VectorXd &betas, const Eigen::VectorXd &Ts,
                               const Eigen::VectorXd &taus, const Eigen::VectorXd &Is,
                                      Eigen::VectorXd& deltas) {
    if (qs.rows() != n) {
        throw std::runtime_error("qs dimension mismatch.");
    }
    if (betas.rows() != n) {
        throw std::runtime_error("betas dimension mismatch.");
    }
    if (Ts.rows() != n) {
        throw std::runtime_error("Ts dimension mismatch.");
    }
    if (taus.rows() != n) {
        throw std::runtime_error("taus dimension mismatch.");
    }
    if (Is.rows() != n) {
        throw std::runtime_error("Is dimension mismatch.");
    }
    deltas.resize(n);

    Eigen::MatrixXd diag_q_tick(q_grp_size_arr.asDiagonal());
    Eigen::MatrixXd diag_sqrt_T_tau((Ts - taus).array().sqrt().matrix().asDiagonal());

    auto tilde_cov_arr = diag_q_tick * diag_sqrt_T_tau * cov_mat * diag_sqrt_T_tau * diag_q_tick;

    nlopt::opt opt(nlopt::algorithm::LD_SLSQP, n);
    DeltaObjectiveFuncForNlopt f(
        qs,
        q_grp_size_arr,
        tilde_p_arr,
        s_d_arr,
        p_hat_arr,
        s_o_hat_arr,
        c_maker_arr,
        c_taker_arr,
        one_d_arr,
        Is,
        tilde_cov_arr,
        betas,
        taker_bnd_arr,
        lambda_arr,
        epsilon_arr,
        a_d_arr,
        b_d_arr
    );
    opt.set_max_objective(nlopt::vfunc f, void* f_data)
    nlopt::opt::set_ftol_rel(1e-6)
    nlopt::opt::set_ftol_abs(1e-2)
    nlopt::opt::set_xtol_rel(1e-6)
    nlopt::opt::set_xtol_abs(1e-2)
    nlopt::opt::set_maxeval(30)

    return 0;
}


int main(int argc, char* argv[]) {
    XConMMModel o;

    o.UpdatePredArrivalRate("contract", "ex", 1, 2);
    std::cout << o.GetPredArrivalRate("contract", "ex", 1) << std::endl;

    o.UpdatePredCov("contract1", "ex1", "contract2", "ex2", 0.123);
    std::cout << o.GetPredCov("contract1", "ex1", "contract2", "ex2") << std::endl;

    nlopt::opt opt(nlopt::algorithm::LD_SLSQP, 4);
    std::cout << opt.get_dimension() << std::endl;

    int n_rows = 2;
    int n_cols = 2;
    Eigen::MatrixXf m(n_rows, n_cols);
    for (int i=0; i<n_rows; i++) {
        for (int j=0; j<n_rows; j++) {
            m(i, j) = (i + 1) * (j + 1);
        }
    }

    Eigen::MatrixXf m2(n_rows, n_cols);
    for (int i=0; i<n_rows; i++) {
        for (int j=0; j<n_rows; j++) {
            m2(i, j) = (i + 1) * (j + 1);
        }
    }

    auto m3 = m * m2;
    std::cout << m << std::endl;
    std::cout << m2 << std::endl;
    std::cout << m3 << std::endl;
    std::cout << m3.array().sqrt().matrix() << std::endl;

    int n = 3;
    Eigen::VectorXd v(n);
    for (int i=0; i<n; i++) {
        v(i) = i;
    }

    std::cout << "v=" << v << std::endl;
    std::cout << "v.sqrt=" << v.array().sqrt() << std::endl;
    std::cout << Eigen::MatrixXd(v.asDiagonal()) << std::endl;


    v.resize(14);
    std::cout << v << std::endl;

    std::cout << v.rows() << std::endl;
    std::cout << v.cols() << std::endl;

}

DeltaObjectiveFuncForNlopt::DeltaObjectiveFuncForNlopt(const Eigen::VectorXd &q_arr, const Eigen::VectorXd &q_tick_arr,
                                                       const Eigen::VectorXd &tilde_p_arr,
                                                       const Eigen::VectorXd &s_d_arr, const Eigen::VectorXd &p_hat_arr,
                                                       const Eigen::VectorXd &s_o_hat_arr,
                                                       const Eigen::VectorXd &c_maker_arr,
                                                       const Eigen::VectorXd &c_taker_arr,
                                                       const Eigen::VectorXd &one_d_arr, const Eigen::VectorXd &I_arr,
                                                       const Eigen::VectorXd &tilde_cov_arr,
                                                       const Eigen::VectorXd &beta_arr,
                                                       const Eigen::VectorXd &taker_bnd_arr,
                                                       const Eigen::VectorXd &lambda_arr,
                                                       const Eigen::VectorXd &epsilon_arr,
                                                       const Eigen::VectorXd &a_d_arr, const Eigen::VectorXd &b_d_arr)
                                                       : q_arr(q_arr), q_tick_arr(q_tick_arr), tilde_p_arr(tilde_p_arr),
                                                       s_d_arr(s_d_arr), p_hat_arr(p_hat_arr), s_o_hat_arr(s_o_hat_arr),
                                                       c_maker_arr(c_maker_arr), c_taker_arr(c_taker_arr), one_d_arr(one_d_arr),
                                                       I_arr(I_arr), tilde_cov_arr(tilde_cov_arr), beta_arr(beta_arr),
                                                       taker_bnd_arr(taker_bnd_arr), lambda_arr(lambda_arr), epsilon_arr(epsilon_arr),
                                                       a_d_arr(a_d_arr), b_d_arr(b_d_arr), n(q_arr.rows()),
                                                       diag_one_d_arr(one_d_arr.asDiagonal()),
                                                       one_arr(Eigen::VectorXd::Ones(n)),
                                                       dr_arr(diag_one_d_arr * tilde_cov_arr * (I_arr + diag_one_d_arr * (q_arr + one_arr / 2))),
                                                       beta_dr_arr(beta_arr.asDiagonal() * one_arr),
                                                       amt_entry_arr(q_tick_arr.asDiagonal() * one_arr),
                                                       exit_prc_mat(Eigen::MatrixXd(p_hat_arr.asDiagonal()) - diag_one_d_arr * s_o_hat_arr.asDiagonal()),
                                                       eval_count(0) {

}

double DeltaObjectiveFuncForNlopt::operator()(const std::vector<double> &x, std::vector<double> &grad, void *f_data) {
    Eigen::VectorXd delta_arr = Eigen::VectorXd::Map(x.data(), x.size());
    Eigen::VectorXd p_entry_arr = tilde_p_arr - diag_one_d_arr * s_d_arr - diag_one_d_arr * delta_arr;
    Eigen::VectorXd q_token_arr = Eigen::MatrixXd(p_entry_arr.asDiagonal()).inverse() * amt_entry_arr;
    Eigen::VectorXd amt_exit_arr = exit_prc_mat * q_token_arr;
    std::vector<double> fee_vec(x.size());
    std::vector<double> maker_idx_vec;
    std::vector<double> taker_idx_vec;
    maker_idx_vec.reserve(x.size());
    taker_idx_vec.reserve(x.size());
    double delta, taker_bnd;
    for (int i=0; i<x.size(); i++) {
        delta = delta_arr[i];
        taker_bnd = taker_bnd_arr[i];
        if (delta > taker_bnd) {
            fee_vec[i] = c_maker_arr[i];
            maker_idx_vec.push_back(i);
        } else {
            fee_vec[i] = c_taker_arr[i];
            taker_idx_vec.push_back(i);
        }
    }
    Eigen::VectorXd fee_arr = Eigen::VectorXd::Map(fee_vec.data(), fee_vec.size());
    Eigen::VectorXd pnl_arr = diag_one_d_arr * (amt_exit_arr - amt_entry_arr) - fee_arr.asDiagonal() * q_token_arr;
    Eigen::VectorXd U_arr = pnl_arr - beta_dr_arr;
    Eigen::VectorXd _q_arr = a_d_arr * delta_arr + b_d_arr + q_arr;
    Eigen::VectorXd _mu_arr = lambda_arr - epsilon_arr;
    Eigen::VectorXd _sigma_arr = lambda_arr.array().sqrt().matrix();
    Eigen::VectorXd _q_z_arr = ((_q_arr - _mu_arr).array() / _sigma_arr.array()).matrix();
    std::vector<double> pr_vec(_q_z_arr.size());
    double q_z;
    for (int i=0; i<_q_z_arr.size(); i++) {
        q_z = _q_z_arr[i];
        pr_vec[i] = 1 - fast_std_norm_cdf(q_z);
    }
    Eigen::VectorXd pr_arr = Eigen::VectorXd::Map(pr_vec.data(), pr_vec.size());
    for (int i : maker_idx_vec) {
        U_arr(i) *= pr_arr(i);
    }
    Eigen::VectorXd dpnl_arr = (diag_one_d_arr * exit_prc_mat - Eigen::MatrixXd(fee_arr.asDiagonal())) * amt_entry_arr * (1 / p_entry_arr.array()).matrix().asDiagonal() * diag_one_d_arr * (1 / p_entry_arr.array()).matrix();
    std::vector<double> pdf_vec(_q_z_arr.size());
    for (int i=0; i<_q_z_arr.size(); i++) {
        q_z = _q_z_arr[i];
        pdf_vec[i] = std_norm_pdf(q_z);
    }
    Eigen::VectorXd pdf_arr = Eigen::VectorXd::Map(pdf_vec.data(), pdf_vec.size());
    Eigen::VectorXd dU_arr = - pdf_arr * a_d_arr * U_arr + pr_arr * dpnl_arr;
    for (int i : taker_idx_vec) {
        U_arr(i) = pnl_arr(i);
    }
    double U = U_arr.sum();
    if (!grad.empty()) {
        Eigen::VectorXd::Map(&grad[0], dU_arr.size()) = dU_arr;
    }

    return U;
}
