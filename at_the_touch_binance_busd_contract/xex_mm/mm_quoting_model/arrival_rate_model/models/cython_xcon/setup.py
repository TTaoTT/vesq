from setuptools import setup, find_packages

from Cython.Build import cythonize
from Cython.Distutils import build_ext
from distutils.extension import Extension

# setup(ext_modules=cythonize("cython_xcon_mm_model.pyx", libraries=["nlopt"],  language="c++"))

setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=[
        Extension("cython_xcon_mm_model",
                  sources=["cython_xcon_mm_model.pyx"],
                  libraries=["nlopt"],
                  language="c++",
                  )
    ],
    name="cython_xcon_mm_model",
)
