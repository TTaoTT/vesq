
from libcpp.string cimport string


cdef extern from "XConMMModel.cpp":
    pass

# Declare the class with cdef
cdef extern from "XConMMModel.h":
    cdef cppclass XConMMModel:
        XConMMModel() except +

        ################################### STATE MANAGEMENT #########################################
        void UpdatePredArrivalRate(const string& contract, const string& ex, int d, double val)
        double GetPredArrivalRate(const string& contract, const string& ex, int d)
        void UpdatePredRefPrc(const string& contract, const string& ex, double val)
        double GetPredRefPrc(const string& contract, const string& ex)
        void UpdatePredHalfSpread(const string& contract, const string& ex, int d, double val)
        double GetPredHalfSpread(const string& contract, const string& ex, int d)
        @staticmethod
        string GetCovKey(const string& contract1, const string& ex1, const string& contract2, const string& ex2)
        void UpdatePredCov(const string& contract1, const string& ex1, const string& contract2, const string& ex2, double val)
        double GetPredCov(const string& contract1, const string& ex1, const string& contract2, const string& ex2)
        void UpdateFeeRate(const string& contract, const string& ex, int maker_taker, double val)
        double GetFeeRate(const string& contract, const string& ex, int maker_taker)
        void UpdateQGrpSize(const string& contract, double val) except +
        double GetQGrpSize(const string& contract)
        void UpdateDepthMetrics(const string& contract, const string& ex, const string& metric, double val)
        double GetDepthMetrics(const string& contract, const string& ex, const string& metric)

        ################################### COMPUTATION #########################################