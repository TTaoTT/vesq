import math
from collections import defaultdict
from dataclasses import dataclass
from typing import Dict, List, Tuple, Union

import numpy as np
from xex_mm.utils.base import Level, Depth, ReferencePriceCalculator, BBO
from xex_mm.utils.enums import Direction, MakerTaker

@dataclass
class PriceSolverMessage:
    q_filter_ratio: float = 0
    Q_filter_ratio: float = 0
    U_lb_filter_ratio: float = 0
    Pr_lb_filter_ratio: float = 0


@dataclass
class QuantitySolverMessage:
    U_lb_filtered: bool = False
    EU_filtered: bool = False


class SingleArrivalRateXExMmModelNoPartialFill:
    """
    Simplified version of SingleArrivalRateXExMmModel, no grid search, approximated optimal price and quantity computed
    in O(1) time complexity.
    """

    def __init__(self, fee_rate_map: Dict):
        self._fee_rate_map = fee_rate_map  # contract, ex, maker_taker

        self._q_grp_size_ccy2_map = {}  # contract, ex
        self._arrival_rate_return_space_lower_bound_map = {}
        self._arrival_rate_return_space_upper_bound_map = {}
        self._tau_map = {}  # contract, ex, side

        self._pred_ref_prc_ret_map = {}  # contract, ex
        self._pred_half_sprd_map = {}  # contract, ex, side
        self._pred_sigma_map = {}  # contract, ex
        self._pred_arrival_size_in_tokens_map = {}  # contract, ex, side
        self._pred_arrival_time_map = {}  # contract, ex, side
        self._pred_latency_map = {}  # contract, ex

        self._ref_prc_map = {}  # contract, ex
        self._depth_metrics = defaultdict(lambda: {})  # (contract, ex), metric
        self._trade_ret_lambda_map = defaultdict(lambda: {})

        self._numeric_tol = 1e-10
        self._min_sigma = 1e-7

    ############################################ Params Control #################################################

    def _update_ex_contract_d_map(self, contract: str, ex: str, d: int, pred: Union[float, None], pred_map: Dict):
        if contract not in pred_map:
            pred_map[contract] = {}
        if ex not in pred_map[contract]:
            pred_map[contract][ex] = {}
        pred_map[contract][ex][d] = pred

    def _update_CE_map(self, contract: str, ex: str, pred: Union[float, None], pred_map: Dict):
        if contract not in pred_map:
            pred_map[contract] = {}
        pred_map[contract][ex] = pred

    def update_tau(self, contract: str, ex: str, tau: float):
        self._update_CE_map(contract=contract, ex=ex, pred=tau, pred_map=self._tau_map)

    def get_tau(self, contract: str, ex: str):
        return self._get_CE_val(contract=contract, ex=ex, CE_map=self._tau_map)

    def update_pred_latency(self, contract: str, ex: str, pred_latency: int):
        self._update_CE_map(contract=contract, ex=ex, pred=pred_latency, pred_map=self._pred_latency_map)

    def get_pred_latency(self, contract: str, ex: str):
        if contract not in self._pred_latency_map:
            return 0
        if ex not in self._pred_latency_map[contract]:
            return 0
        return self._pred_latency_map[contract][ex]

    def _get_CE_val(self, contract: str, ex: str, CE_map: Dict):
        if contract not in CE_map:
            return None
        if ex not in CE_map[contract]:
            return None
        return CE_map[contract][ex]

    def _get_CED_val(self, contract: str, ex: str, d: int, CED_map: Dict):
        if contract not in CED_map:
            return None
        if ex not in CED_map[contract]:
            return None
        if d not in CED_map[contract][ex]:
            return None
        return CED_map[contract][ex][d]

    def update_pred_arrival_size_in_tokens(self, contract: str, ex: str, d: int, arrival_size_in_tokens: float):
        self._update_ex_contract_d_map(contract=contract, ex=ex, d=d,
                                       pred=arrival_size_in_tokens, pred_map=self._pred_arrival_size_in_tokens_map)

    def get_pred_arrival_size_in_tokens(self, contract: str, ex: str, d: int):
        return self._get_CED_val(contract=contract, ex=ex, d=d, CED_map=self._pred_arrival_size_in_tokens_map)

    def update_pred_arrival_time(self, contract: str, ex: str, d: int, arrival_time: float):
        self._update_ex_contract_d_map(contract=contract, ex=ex, d=d,
                                       pred=arrival_time, pred_map=self._pred_arrival_time_map)

    def get_pred_arrival_time(self, contract: str, ex: str, d: int):
        return self._get_CED_val(contract=contract, ex=ex, d=d, CED_map=self._pred_arrival_time_map)

    def update_pred_ref_prc_ret(self, contract: str, ex: str, pred: Union[float, None]):
        self._update_CE_map(contract=contract, ex=ex, pred=pred,
                            pred_map=self._pred_ref_prc_ret_map)

    def get_pred_ref_prc_ret(self, contract: str, ex: str) -> Union[float, None]:
        if contract not in self._pred_ref_prc_ret_map:
            return
        if ex not in self._pred_ref_prc_ret_map[contract]:
            return
        ret = self._pred_ref_prc_ret_map[contract][ex]
        if ret is None:
            return
        return ret

    def update_pred_half_spread(self, contract: str, ex: str, d: int, pred: Union[float, None]):
        self._update_ex_contract_d_map(contract=contract, ex=ex, d=d, pred=pred,
                                       pred_map=self._pred_half_sprd_map)

    def get_pred_half_sprd(self, contract: str, ex: str, d: int):
        if contract not in self._pred_half_sprd_map:
            return
        if ex not in self._pred_half_sprd_map[contract]:
            return
        if d not in self._pred_half_sprd_map[contract][ex]:
            return
        hs = self._pred_half_sprd_map[contract][ex][d]
        if hs is None:
            return
        return hs

    def update_pred_sigma(self, contract: str, ex: str, pred: Union[float, None]):
        self._update_CE_map(contract=contract, ex=ex, pred=pred,
                            pred_map=self._pred_sigma_map)

    def get_pred_sigma(self, contract: str, ex: str):
        if contract not in self._pred_sigma_map:
            return
        if ex not in self._pred_sigma_map[contract]:
            return
        sigma = self._pred_sigma_map[contract][ex]
        if sigma is None:
            return
        return max(self._min_sigma, sigma)

    def _get_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker):
        return self._fee_rate_map[contract][ex][maker_taker]

    def update_q_grp_size_ccy2(self, contract: str, ex: str, q_grp_size_ccy2: float) -> bool:
        self._q_grp_size_ccy2_map[(contract, ex)] = q_grp_size_ccy2
        return True

    def has_q_grp_size_ccy2(self, contract: str, ex: str) -> bool:
        return (contract, ex) in self._q_grp_size_ccy2_map

    def get_q_grp_size_ccy2(self, contract: str, ex: str) -> Union[float, None]:
        return self._q_grp_size_ccy2_map.get((contract, ex))

    def _get_q_grp_size_ccy1(self, contract: str, ex: str):
        if not self.has_q_grp_size_ccy2(contract=contract, ex=ex):
            return
        if not self._depth_metrics[(contract, ex)]:
            return
        ref_prc = self.get_ref_prc(contract=contract, ex=ex)
        return self.get_q_grp_size_ccy2(contract=contract, ex=ex) / ref_prc

    # def get_lambda_for_quote_d(self, contract: str, ex: str, quote_d: int) -> Union[float, None]:
    #     ar_d = Direction.long if quote_d == Direction.short else Direction.short
    #     q_grp_size_in_ccy1 = self._get_q_grp_size_ccy1(contract=contract, ex=ex)
    #     if q_grp_size_in_ccy1 is None:
    #         return
    #     tau = self.get_tau(contract=contract, ex=ex)
    #     trd_size_in_ccy1 = self.get_pred_arrival_size_in_tokens(contract=contract, ex=ex, d=ar_d)
    #     trd_size_in_q_grp = trd_size_in_ccy1 / q_grp_size_in_ccy1
    #     trd_time_interval_in_ms = self.get_pred_arrival_time(contract=contract, ex=ex, d=ar_d)
    #     num_trades_in_tau = max(tau / trd_time_interval_in_ms, 1)
    #     ar_in_q_grp = num_trades_in_tau * trd_size_in_q_grp
    #     ar_in_q_grp = max(ar_in_q_grp, - self._epsilon + self._numeric_tol)
    #     return ar_in_q_grp

    def _calib_Q_func(self, side: List[Level]) -> Tuple[float, float]:
        if len(side) == 0:
            return 0, 0
        if len(side) == 1:
            return 0, side[0].qty
        Q = 0
        Qs = []
        ps = []
        for lvl in side:
            Q += lvl.qty
            Qs.append(Q)
            ps.append(lvl.prc)
        X = np.abs(np.log(np.array(ps) / ps[0]))
        y = np.array(Qs) - Qs[0]
        beta = 1 / X.dot(X) * X.dot(y)
        return beta, Qs[0]

    def update_ref_prc(self, contract: str, ex: str, ref_prc: float) -> bool:
        """ Call this before update depth """
        if contract not in self._ref_prc_map:
            self._ref_prc_map[contract] = dict()
        self._ref_prc_map[contract][ex] = ref_prc
        return True

    def get_ref_prc(self, contract: str, ex: str) -> Union[float, None]:
        return self._ref_prc_map.get(contract, {}).get(ex)

    def update_depth_metrics(self, contract: str, ex: str, **kwargs) -> bool:
        self._depth_metrics[(contract, ex)].update(kwargs)
        return True

    def update_trade_ret_lambda(self, contract: str, ex: str, d: int, trade_ret_lambda: float) -> bool:
        self._trade_ret_lambda_map[(contract, ex)][d] = trade_ret_lambda
        return True

    def get_trade_ret_lambda(self, contract: str, ex: str, quote_d: int) -> Union[float, None]:
        trade_d = Direction.long if quote_d == Direction.short else Direction.short
        return self._trade_ret_lambda_map[(contract, ex)].get(trade_d)

    def update_depth(self, contract: str, ex: str, depth: Depth) -> bool:
        """ Call update ref prc before this """
        tilde_p = self.get_ref_prc(contract=contract, ex=ex)
        if tilde_p is None:
            return False
        best_ask = depth.asks[0].prc if len(depth.asks) > 0 else math.inf
        best_bid = depth.bids[0].prc if len(depth.bids) > 0 else 0
        s_l = math.log(tilde_p / best_bid)
        s_s = math.log(best_ask / tilde_p)
        metrics = dict(
            best_bid=best_bid,
            best_ask=best_ask,
            s_l=s_l,
            s_s=s_s,
        )
        self.update_depth_metrics(contract=contract, ex=ex, **metrics)
        return True

    def remove_depth_metrics(self, contract: str, ex: str) -> bool:
        self._depth_metrics.pop((contract, ex), None)
        return True

    def update_bbo(self, contract: str, ex: str, bbo: BBO):
        tilde_p = self.get_ref_prc(contract=contract, ex=ex)
        if tilde_p is None:
            return False
        best_ask = bbo.best_ask_prc if bbo.valid_best_ask else math.inf
        best_bid = bbo.best_bid_prc if bbo.valid_best_bid else 0
        s_l = math.log(tilde_p / best_bid)
        s_s = math.log(best_ask / tilde_p)
        metrics = dict(
            best_bid = best_bid,
            best_ask = best_ask,
            s_l = s_l,
            s_s = s_s,
        )
        q_grp_size_ccy1 = self._get_q_grp_size_ccy1(contract=contract, ex=ex)
        if q_grp_size_ccy1 is not None:
            metrics.update(q_grp_size_ccy1=q_grp_size_ccy1)
        self.update_depth_metrics(contract=contract, ex=ex, **metrics)
        return True

    def get_depth_metric(self, contract: str, ex: str, metric: str) -> Union[float, None]:
        return self._depth_metrics[(contract, ex)].get(metric)

    def get_depth_metrics(self, contract: str, ex: str) -> Dict:
        return self._depth_metrics[(contract, ex)]

    def has_depth_metrics(self, contract: str, ex: str) -> bool:
        return bool(self._depth_metrics[(contract, ex)])

    def update_ar_return_space_lower_bound(self, contract: str, val: float):
        self._arrival_rate_return_space_lower_bound_map[contract] = val

    def update_ar_return_space_upper_bound(self, contract: str, val: float):
        self._arrival_rate_return_space_upper_bound_map[contract] = val

    def _get_ar_return_space_lower_bound(self, contract: str):
        return self._arrival_rate_return_space_lower_bound_map[contract]

    def _get_ar_return_space_upper_bound(self, contract: str):
        return self._arrival_rate_return_space_upper_bound_map[contract]

    ############################################ Computation Methods #################################################
    def _dR(self,
           d: int,
           I: float,  # ccy2 grp
           ex_I: float,  # ccy2 grp
           q: float,  # ccy2 grp
           sigma: float,
           T: float,  # ms
           beta: float,
           latency: float,  # ms
           ) -> Union[float, np.ndarray]:
        mI = I + ex_I
        if mI * d >= 0:
            dI = 2 * mI * d * q + q ** 2
        else:  # mI * d < 0
            if q <= abs(mI):
                dI = 2 * mI * d * q + q ** 2
            else:
                dI = - mI ** 2 + (q - abs(mI)) ** 2
        dR = 0.5 * beta * sigma ** 2 * (T + latency) * dI
        return dR

    def _delta_hat(self,
                   d: int,
                   r: float,  # ret
                   s_o_hat: float,  # ret
                   s_d: float,  # ret
                   I: float,  # ccy2 grp
                   ex_I: float,  # ccy2 grp
                   q: float,  # ccy2 grp
                   sigma: float,
                   T: float,  # ms
                   beta: float,
                   fee: float,  # ret
                   latency: float,  # ms
                   ) -> float:
        dR = self._dR(d=d, I=I, ex_I=ex_I, q=q, sigma=sigma, T=T, beta=beta, latency=latency)
        delta = dR / q - (d * r + s_d - s_o_hat - fee)
        return delta + self._numeric_tol

    def _U(self,
           d: int,
           r: float,  # ret
           s_o_hat: float,  # ret
           s_d: float,  # ret
           delta: Union[float, np.ndarray],  # ret
           I: float,  # ccy2 grp
           ex_I: float,  # ccy2 grp
           q: float,  # ccy2 grp
           sigma: float,
           T: float,  # ms
           beta: float,
           fee: float,  # ret
           latency: float,  # ms
           ) -> Union[float, np.ndarray]:
        pnl = (delta + d * r + s_d - s_o_hat - fee) * q
        dR = self._dR(d=d, I=I, ex_I=ex_I, q=q, sigma=sigma, T=T, beta=beta, latency=latency)
        return pnl - dR

    def _fill_prob_array_to_delta_array(self, contract: str, ex: str, d: int, fill_prob_arr: np.ndarray) -> Union[np.ndarray, None]:
        tau = self.get_tau(contract=contract, ex=ex)
        if tau is None:
            return
        pred_art = self.get_pred_arrival_time(contract=contract, ex=ex, d=d)
        if pred_art is None:
            return
        nar = tau / pred_art
        trade_ret_lambda = self.get_trade_ret_lambda(contract=contract, ex=ex, quote_d=d)
        if trade_ret_lambda is None:
            return
        exp_dist_quantile = (np.minimum(np.maximum(1 - fill_prob_arr, 0), 1)) ** (1/nar)
        exp_dist_quantile_trade_ret = - np.log(np.minimum(np.maximum(1 - exp_dist_quantile, 0), 1)) / trade_ret_lambda
        return exp_dist_quantile_trade_ret

    def _optimal_price_level(self,
                             contract: str,
                             ex: str,
                             q: int,  # ccy2 grp
                             d: int,
                             I: int,  # ccy2 grp
                             ex_I: int,  # ccy2 grp
                             T: float,
                             beta: float,
                             target_fill_prob: Union[float, None],
                             fill_prob_cutoff: float,
                             ) \
            -> Tuple[float, float, int, PriceSolverMessage]:

        msg = PriceSolverMessage()

        SEx_tilde_p = self.get_ref_prc(contract=contract, ex=ex)
        SEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        tilde_p = self.get_ref_prc(contract=contract, ex=ex)
        XEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        XEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        s_d = XEx_s_l if d == Direction.long else XEx_s_s
        r = self.get_pred_ref_prc_ret(contract=contract, ex=ex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=ex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        sigma = self.get_pred_sigma(contract=contract, ex=ex)
        c_maker = self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker)

        if d == Direction.long:
            taker_bnd = math.log((tilde_p * math.exp(-XEx_s_l)) / (SEx_tilde_p * math.exp(SEx_s_s)))
        else:
            taker_bnd = math.log((SEx_tilde_p * math.exp(-SEx_s_l)) / (tilde_p * math.exp(XEx_s_s)))

        latency = self.get_pred_latency(contract=contract, ex=ex)

        if target_fill_prob is None:
            pr_arr = np.array([0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
        else:
            if 1 - target_fill_prob < fill_prob_cutoff:
                pr_arr = np.array([1])
            else:
                pr_arr = np.array([target_fill_prob])
        delta_arr = self._fill_prob_array_to_delta_array(contract=contract, ex=ex, d=d, fill_prob_arr=pr_arr)
        if delta_arr is None:
            return 0, math.nan, MakerTaker.undefined, msg

        delta_lb = self._delta_hat(
            d=d,
            r=r,
            s_o_hat=s_o_hat,
            s_d=s_d,
            I=I,
            ex_I=ex_I,
            q=q,
            sigma=sigma,
            T=T,
            beta=beta,
            fee=c_maker,
            latency=latency,
        )
        delta_filter = (delta_arr > delta_lb) & (delta_arr > taker_bnd)
        delta_arr = delta_arr[delta_filter]
        pr_arr = pr_arr[delta_filter]
        if len(delta_arr) == 0:
            return 0, np.nan, MakerTaker.maker, msg
            # delta_arr = np.array([max(delta_lb, taker_bnd) + 0.0001])
            # pr_arr = np.array([0])
        U_arr = self._U(
            d=d,
            r=r,
            s_o_hat=s_o_hat,
            s_d=s_d,
            delta=delta_arr,
            I=I,
            ex_I=ex_I,
            q=q,
            sigma=sigma,
            T=T,
            beta=beta,
            fee=c_maker,
            latency=latency,
        )
        U_filter = U_arr > 0
        msg.U_lb_filter_ratio = np.sum(~U_filter) / U_filter.shape[0]
        if abs(msg.U_lb_filter_ratio - 1) < self._numeric_tol:
            return 0, math.nan, MakerTaker.undefined, msg
        U_arr = U_arr[U_filter]
        delta_arr = delta_arr[U_filter]
        pr_arr = pr_arr[U_filter]
        EU_arr: np.ndarray = pr_arr * U_arr
        max_idx = EU_arr.argmax()
        delta_hat = delta_arr[max_idx]
        EU_hat = EU_arr[max_idx]
        return EU_hat, delta_hat, MakerTaker.maker, msg

    def optimal_price_level(self,
                            contract: str,
                            ex: str,
                            q: float,  # ccy2
                            d: int,
                            I: float,  # ccy2
                            ex_I: float,  # ccy2
                            T: float,  # ms
                            beta: float,
                            target_fill_prob: Union[float, None],
                            fill_prob_cutoff: float,
                            ) -> Tuple[float, float, int, PriceSolverMessage]:
        q_grp_size_ccy2 = self.get_q_grp_size_ccy2(contract=contract, ex=ex)
        int_I = int(round(I / q_grp_size_ccy2))
        int_ex_I = int(round(ex_I / q_grp_size_ccy2))
        int_q = max(int(round(q / q_grp_size_ccy2)), 1)
        beta = max(beta, self._numeric_tol)
        return self._optimal_price_level(contract=contract, ex=ex, q=int_q, d=d, I=int_I, ex_I=int_ex_I, T=T, beta=beta,
                                         target_fill_prob=target_fill_prob, fill_prob_cutoff=fill_prob_cutoff)

    def _q(self,
           d: int,
           r: float,  # ret
           s_o_hat: float,  # ret
           s_d: float,  # ret
           delta: Union[float, np.ndarray],  # ret
           I: float,  # ccy2 grp
           ex_I: float,  # ccy2 grp
           sigma: float,
           T: float,  # ms
           beta: float,
           fee: float,  # ret
           latency: float,  # ms
           ):
        mI = I + ex_I
        base_q_solution = (delta + r + s_d - s_o_hat - fee) / (beta * sigma ** 2 * (T + latency)) - mI * d
        if mI * d >= 0:
            if base_q_solution < 0:
                return 0
            return base_q_solution
        if base_q_solution < 0:
            return 0
        if base_q_solution <= abs(mI):
            return base_q_solution
        flip_q_solution = (delta + r + s_d - s_o_hat - fee) / (beta * sigma ** 2 * (T + latency)) + abs(mI)
        assert flip_q_solution >= abs(mI)
        return flip_q_solution

    def _delta_to_fill_prob(self, contract: str, ex: str, d: int, delta: float) -> Union[float, None]:
        tau = self.get_tau(contract=contract, ex=ex)
        if tau is None:
            return
        pred_art = self.get_pred_arrival_time(contract=contract, ex=ex, d=d)
        if pred_art is None:
            return
        nar = tau / pred_art
        trade_ret_lambda = self.get_trade_ret_lambda(contract=contract, ex=ex, quote_d=d)
        if trade_ret_lambda is None:
            return
        fill_prob = 1 - (1 - math.exp(-trade_ret_lambda * delta)) ** nar
        return fill_prob

    def _optimal_quantity(self,
                          contract: str,
                          ex: str,
                          d: int,
                          delta: float,  # ret
                          I: int,  # ccy2 qty
                          ex_I: int,  # ccy2 qty
                          T: float,  # ms
                          beta: float,
                          ) -> Tuple[float, float, QuantitySolverMessage]:

        msg = QuantitySolverMessage()

        if math.isnan(delta):
            return 0, 0, msg

        SEx_tilde_p = self.get_ref_prc(contract=contract, ex=ex)
        SEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        tilde_p = self.get_ref_prc(contract=contract, ex=ex)
        XEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        XEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        if d == Direction.long:
            s_d = XEx_s_l
        else:
            s_d = XEx_s_s
        sigma = self.get_pred_sigma(contract=contract, ex=ex)
        r = self.get_pred_ref_prc_ret(contract=contract, ex=ex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=ex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        latency = self.get_pred_latency(contract=contract, ex=ex)

        max_U = -self._numeric_tol
        q_hat = 0

        c_maker = self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker)
        c_taker = self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker)

        if d == Direction.long:
            taker_bnd = math.log((tilde_p * math.exp(-XEx_s_l)) / (SEx_tilde_p * math.exp(SEx_s_s)))
        else:
            taker_bnd = math.log((SEx_tilde_p * math.exp(-SEx_s_l)) / (tilde_p * math.exp(XEx_s_s)))

        """
        unit_pnl = (d * (p_hat - tilde_p) + s_d + delta - s_o_hat) - c_maker * tilde_p
        pnl = 1 * q_tick_size_ccy1 * unit_pnl
        dr = sigma * tilde_p * math.sqrt(tau + latency) * q_tick_size_ccy1 * 2 * (d * I_beta * I * q_beta + q_beta ** 2 * q)  + q_beta ** 2
        U = pnl - dr
        
        pnl = beta * dr
        a = [q_tick_size_ccy1 * (d * (p_hat - tilde_p) + s_d + delta - s_o_hat) - c_maker * tilde_p] / 
                [beta * sigma * tilde_p * math.sqrt(tau + latency) * q_tick_size_ccy1]
        a = 2 * (d * I * I_beta * q_beta + q * q_beta ** 2) + q_beta ** 2
        q = (a - q_beta ** 2 - 2 * d * I * I_beta * q_beta) / (2 * q_beta ** 2)
        """

        if delta < taker_bnd:
            # taker
            q = self._q(
                d=d,
                r=r,
                s_o_hat=s_o_hat,
                s_d=s_d,
                delta=delta,
                I=I,
                ex_I=ex_I,
                sigma=sigma,
                T=T,
                beta=beta,
                fee=c_taker,
                latency=latency,
            )
            assert q > 0
            q = max(q, 0)
            U = self._U(
                d=d,
                r=r,
                s_o_hat=s_o_hat,
                s_d=s_d,
                delta=delta,
                I=I,
                ex_I=ex_I,
                q=q,
                sigma=sigma,
                T=T,
                beta=beta,
                fee=c_taker,
                latency=latency,
            )
            if U > max_U:
                max_U = U
                q_hat = q

        else:
            # maker
            U_q = self._q(
                d=d,
                r=r,
                s_o_hat=s_o_hat,
                s_d=s_d,
                delta=delta,
                I=I,
                ex_I=ex_I,
                sigma=sigma,
                T=T,
                beta=beta,
                fee=c_maker,
                latency=latency,
            )
            assert U_q >= 0
            U_q = max(U_q, 0)
            q = max(U_q, 0)
            U = self._U(
                d=d,
                r=r,
                s_o_hat=s_o_hat,
                s_d=s_d,
                delta=delta,
                I=I,
                ex_I=ex_I,
                q=q,
                sigma=sigma,
                T=T,
                beta=beta,
                fee=c_maker,
                latency=latency,
            )
            Pr = self._delta_to_fill_prob(contract=contract, ex=ex, d=d, delta=delta)
            EU = U * Pr
            if EU > max_U:
                max_U = EU
                q_hat = q
            else:
                msg.EU_filtered = True

        max_U = 0 if q_hat == 0 else max_U
        return max_U, q_hat, msg

    def optimal_quantity(self,
                         contract: str,
                         ex: str,
                         d: int,
                         delta: float,  # ret
                         I: float,  # ccy2 qty
                         ex_I: float,  # ccy2 qty
                         T: float,  # ms
                         beta: float,
                         ) -> Tuple[float, float, QuantitySolverMessage]:
        q_grp_size_ccy2 = self.get_q_grp_size_ccy2(contract=contract, ex=ex)
        int_I = int(round(I / q_grp_size_ccy2))
        int_ex_I = int(round(ex_I / q_grp_size_ccy2))
        beta = max(beta, self._numeric_tol)
        U, q, msg = self._optimal_quantity(contract=contract, ex=ex, d=d, delta=delta, I=int_I, ex_I=int_ex_I, T=T, beta=beta)
        q_cap = 1e10
        q *= q_grp_size_ccy2  # ccy2
        return U, min(q_cap, q), msg
