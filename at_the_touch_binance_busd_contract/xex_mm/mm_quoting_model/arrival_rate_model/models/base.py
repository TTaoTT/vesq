import heapq

from typing import List, Dict

import numpy as np
from scipy.stats import poisson

from xex_mm.signals.factors.trade_informedness import TradeInformednessNode
from xex_mm.signals.factors.past_spread import PastHalfSpreadNode
from xex_mm.signals.factors.past_sigma import PastSigmaNode
from xex_mm.signals.factors.past_arrival_rate import SinglePastArrivalRateNode
from xex_mm.signals.labels.forward_ref_prc_ret import FowardRefPrcRetNode
from xex_mm.signals.labels.forward_sigma import FowardSigmaNode
from xex_mm.signals.labels.forward_spread import FowardSpreadNode
from xex_mm.mm_quoting_model.arrival_rate_model.calibrator import LinearModelCalibrator


class ArrivalRateLadder:
    def __init__(self):
        self.__ladder: List[int] = []
        self.__map: Dict[int, float] = {}
        self.__tick_size = np.nan

    def update_arrival_rate_for_n_ticks(self, n_ticks: int, arrival_rate: float):
        heapq.heappush(self.__ladder, n_ticks)
        self.__map[n_ticks] = arrival_rate

    def get_arrival_rate_for_n_ticks(self, n_tick: int):
        if n_tick not in self.__map:
            raise RuntimeError()
        return self.__map[n_tick]

    def __price_diff_to_n_ticks(self, price_diff: float, tick_size: float):
        tol = 1e-10
        new_n_ticks = price_diff / tick_size
        if (new_n_ticks - round(new_n_ticks)) > tol:
            raise RuntimeError(f"Price diff is not fully divisible by price tick ({self.__tick_size}). Price tick is likely outdated!")
        return int(new_n_ticks)

    def update_tick_size(self, tick_size: float):
        tol = 1e-10
        if abs(self.__tick_size - tick_size) < tol:
            return
        # update ladder and map, order maintained
        __ladder = []
        __map = {}
        for n_ticks in self.__ladder:
            price_diff = n_ticks * self.__tick_size
            new_n_ticks = self.__price_diff_to_n_ticks(price_diff=price_diff, tick_size=tick_size)
            __map[new_n_ticks] = self.__map[n_ticks]
            __ladder.append(new_n_ticks)
        self.__ladder = __ladder
        self.__map = __map
        self.__tick_size = tick_size

    def update_arrival_rate_for_price_diff(self, price_diff: float, arrival_rate: float):
        self.update_arrival_rate_for_n_ticks(
            n_ticks=self.__price_diff_to_n_ticks(
                price_diff=price_diff,
                tick_size=self.__tick_size,
            ),
            arrival_rate=arrival_rate,
        )

    def get_arrival_rate_for_price_diff(self, price_diff: float) -> float:
        return self.get_arrival_rate_for_n_ticks(
            n_tick=self.__price_diff_to_n_ticks(
                price_diff=price_diff,
                tick_size=self.__tick_size,
            )
        )


class AlphaLayeredArrivalRateLadder(ArrivalRateLadder):
    def __init__(self):
        super(AlphaLayeredArrivalRateLadder, self).__init__()
        self.__alpha_ladder = []
        self.__alpha_map = {}

        """ This class require more thinking. How to calibrate with alpha? """


class ArrivalRateMmModel:
    def __init__(self, arrival_rate_ladder: AlphaLayeredArrivalRateLadder):
        self.__arrival_rate_ladder = arrival_rate_ladder

    def prob_poisson(self, arrival_rate: float, k: float, t: float) -> float:
        """
        :param arrival_rate: Trades per ms.
        :param k: Number of quantity=1 trades arrived, or quantity if the one trade that arrives.
        :param t: Time window before which trades arrive, in ms.
        """
        return poisson.pmf(k, arrival_rate * t)

    def prob_trade_arrival_for_n_ticks(self, n_ticks: int, k: float, t: float) -> float:
        return self.prob_poisson(
            arrival_rate=self.__arrival_rate_ladder.get_arrival_rate_for_n_ticks(
                n_tick=n_ticks,
            ),
            k=k,
            t=t,
        )

    def prod_trade_arrival_for_price_diff(self, price_diff: float, k: float, t: float) -> float:
        return self.prob_poisson(
            arrival_rate=self.__arrival_rate_ladder.get_arrival_rate_for_price_diff(
                price_diff=price_diff,
            ),
            k=k,
            t=t,
        )

class ReferencePriceModel:
    """ Predictive """
    def __init__(self, ex, pair, start_datetime, end_datetime) -> None:
        x_nodes = []
        x_nodes.append(TradeInformednessNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(SinglePastArrivalRateNode(ex=ex, pair=pair, t=1000))

        y_node = FowardRefPrcRetNode(ex=ex, pair=pair, t=1000)
        model_node = LinearModelCalibrator(x_nodes=x_nodes, y_nodes=[y_node])
        self.model = model_node.get(start_datetime=start_datetime,end_datetime=end_datetime)[0]

    def predict(self, xs: np.ndarray, curr_prc) -> float:
        return curr_prc * (self.model.predict(xs) + 1)


class SpreadModel:
    def __init__(self, ex, pair, start_datetime, end_datetime) -> None:
        x_nodes = []
        x_nodes.append(TradeInformednessNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(PastSigmaNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(SinglePastArrivalRateNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(PastHalfSpreadNode(ex=ex, pair=pair, t=1000))
        
        y_node = FowardSpreadNode(ex=ex, pair=pair, t=1000)
        model_node = LinearModelCalibrator(x_nodes=x_nodes, y_nodes=[y_node])
        self.model = model_node.get(start_datetime=start_datetime,end_datetime=end_datetime)[0]
        
    def predict(self, xs: np.ndarray) -> float:
        return self.model.predict(xs)


class SigmaModel:
    def __init__(self, ex, pair, start_datetime, end_datetime) -> None:
        x_nodes = []
        x_nodes.append(TradeInformednessNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(PastSigmaNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(SinglePastArrivalRateNode(ex=ex, pair=pair, t=1000))
        x_nodes.append(PastHalfSpreadNode(ex=ex, pair=pair, t=1000))
        
        y_node = FowardSigmaNode(ex=ex, pair=pair, t=1000)
        model_node = LinearModelCalibrator(x_nodes=x_nodes, y_nodes=[y_node])
        self.model = model_node.get(start_datetime=start_datetime,end_datetime=end_datetime)[0]

    def predict(self, xs: np.ndarray) -> float:
        return self.model.predict(xs)


class ArrivalRateMktDepthDecayCoefModel:
    def predict(self, xs: np.ndarray) -> float:
        raise NotImplementedError()


class ArrivalRateQtyDecayCoefModel:
    def predict(self, xs: np.ndarray) -> float:
        raise NotImplementedError()


