import math
from dataclasses import dataclass
from typing import Dict, List, Tuple, Union

import numpy as np
from xex_mm.mm_quoting_model.arrival_rate_model.utils.fast_norm import fast_norm_cdf, fast_norm_cdf_inv, \
    fast_norm_cdf_vec
from xex_mm.utils.base import Level, Depth, ReferencePriceCalculator, BBO
from xex_mm.utils.enums import Direction, MakerTaker, DepthCompletenessState

@dataclass
class PriceSolverMessage:
    q_filter_ratio: float = 0
    Q_filter_ratio: float = 0
    U_lb_filter_ratio: float = 0
    Pr_lb_filter_ratio: float = 0


@dataclass
class QuantitySolverMessage:
    U_lb_filtered: bool = False
    EU_filtered: bool = False


class SingleArrivalRateXExMmModelNoPartialFill:
    """
    Simplified version of SingleArrivalRateXExMmModel, no grid search, approximated optimal price and quantity computed
    in O(1) time complexity.
    """

    def __init__(self, fee_rate_map: Dict):
        self._fee_rate_map = fee_rate_map  # contract, ex, maker_taker

        self._q_grp_size_ccy2_map = {}  # contract
        self._arrival_rate_return_space_lower_bound_map = {}
        self._arrival_rate_return_space_upper_bound_map = {}
        self._tau_map = {}  # contract, ex, side

        self._pred_ref_prc_map = {}  # contract, ex
        self._pred_half_sprd_map = {}  # contract, ex, side
        self._pred_sigma_map = {}  # contract, ex
        self._pred_arrival_size_in_tokens_map = {}  # contract, ex, side
        self._pred_arrival_time_map = {}  # contract, ex, side
        self._pred_latency_map = {}  # contract, ex

        self._depth_metrics = {}  # contract, ex, metric

        self._k_1 = 2.5
        self._k_2 = 2.5
        self._g = 3
        self._epsilon = -0.5
        self._numeric_tol = 1e-10
        self._min_sigma = 1e-15
        self._ref_prc_calculator = ReferencePriceCalculator()
        self._xex = "xex"


    ############################################ Params Control #################################################

    def update_q_grp_size_ccy2(self, contract: str, q_grp_size_ccy2: float):
        self._q_grp_size_ccy2_map[contract] = q_grp_size_ccy2

    def _update_ex_contract_d_map(self, contract: str, ex: str, d: Direction, pred: Union[float, None], pred_map: Dict):
        if contract not in pred_map:
            pred_map[contract] = {}
        if ex not in pred_map[contract]:
            pred_map[contract][ex] = {}
        pred_map[contract][ex][d] = pred

    def _update_CE_map(self, contract: str, ex: str, pred: Union[float, None], pred_map: Dict):
        if contract not in pred_map:
            pred_map[contract] = {}
        pred_map[contract][ex] = pred

    def update_tau(self, contract: str, ex: str, tau: float):
        self._update_CE_map(contract=contract, ex=ex, pred=tau, pred_map=self._tau_map)

    def get_tau(self, contract: str, ex: str):
        return self._get_CE_val(contract=contract, ex=ex, CE_map=self._tau_map)

    def update_pred_latency(self, contract: str, ex: str, pred_latency: int):
        self._update_CE_map(contract=contract, ex=ex, pred=pred_latency, pred_map=self._pred_latency_map)

    def get_pred_latency(self, contract: str, ex: str):
        if contract not in self._pred_latency_map:
            return 0
        if ex not in self._pred_latency_map[contract]:
            return 0
        return self._pred_latency_map[contract][ex]

    def _get_CE_val(self, contract: str, ex: str, CE_map: Dict):
        if contract not in CE_map:
            return None
        if ex not in CE_map[contract]:
            return None
        return CE_map[contract][ex]

    def _get_CED_val(self, contract: str, ex: str, d: Direction, CED_map: Dict):
        if contract not in CED_map:
            return None
        if ex not in CED_map[contract]:
            return None
        if d not in CED_map[contract][ex]:
            return None
        return CED_map[contract][ex][d]

    def update_pred_arrival_size_in_tokens(self, contract: str, ex: str, d: Direction, arrival_size_in_tokens: float):
        self._update_ex_contract_d_map(contract=contract, ex=ex, d=d,
                                       pred=arrival_size_in_tokens, pred_map=self._pred_arrival_size_in_tokens_map)

    def get_pred_arrival_size_in_tokens(self, contract: str, ex: str, d: Direction):
        return self._get_CED_val(contract=contract, ex=ex, d=d, CED_map=self._pred_arrival_size_in_tokens_map)

    def update_pred_arrival_time(self, contract: str, ex: str, d: Direction, arrival_time: float):
        self._update_ex_contract_d_map(contract=contract, ex=ex, d=d,
                                       pred=arrival_time, pred_map=self._pred_arrival_time_map)

    def get_pred_arrival_time(self, contract: str, ex: str, d: Direction):
        return self._get_CED_val(contract=contract, ex=ex, d=d, CED_map=self._pred_arrival_time_map)

    def update_pred_ref_prc(self, contract: str, ex: str, pred: Union[float, None]):
        self._update_CE_map(contract=contract, ex=ex, pred=pred,
                            pred_map=self._pred_ref_prc_map)

    def get_pred_ref_prc(self, contract: str, ex: str) -> float:
        if contract not in self._pred_ref_prc_map:
            return 
        if ex not in self._pred_ref_prc_map[contract]:
            return 
        rp = self._pred_ref_prc_map[contract][ex]
        if rp is None:
            return 
        return rp

    def update_pred_half_spread(self, contract: str, ex: str, d: Direction, pred: Union[float, None]):
        self._update_ex_contract_d_map(contract=contract, ex=ex, d=d, pred=pred,
                                       pred_map=self._pred_half_sprd_map)

    def get_pred_half_sprd(self, contract: str, ex: str, d: Direction):
        if contract not in self._pred_half_sprd_map:
            return
        if ex not in self._pred_half_sprd_map[contract]:
            return
        if d not in self._pred_half_sprd_map[contract][ex]:
            return
        hs = self._pred_half_sprd_map[contract][ex][d]
        if hs is None:
            return
        return hs

    def update_pred_sigma(self, contract: str, ex: str, pred: Union[float, None]):
        self._update_CE_map(contract=contract, ex=ex, pred=pred,
                            pred_map=self._pred_sigma_map)

    def get_pred_sigma(self, contract: str, ex: str):
        if contract not in self._pred_sigma_map:
            return
        if ex not in self._pred_sigma_map[contract]:
            return
        sigma = self._pred_sigma_map[contract][ex]
        if sigma is None:
            return
        return max(self._min_sigma, sigma)

    def _get_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker):
        return self._fee_rate_map[contract][ex][maker_taker]

    def init_q_grp_size_ccy2_from_depth(self, depth: Depth):
        ask = depth.asks[0]
        bid = depth.bids[0]
        q_grp_size_ccy2 = (bid.prc * bid.qty + ask.prc * ask.qty) / 2 / 20
        self.update_q_grp_size_ccy2(contract=depth.meta_data.contract, q_grp_size_ccy2=q_grp_size_ccy2)

    def has_q_grp_size_ccy2(self, contract: str):
        return contract in self._q_grp_size_ccy2_map

    def get_q_grp_size_ccy2(self, contract: str):
        return self._q_grp_size_ccy2_map[contract]

    def _get_q_grp_size_ccy1(self, contract: str):
        return self.get_depth_metric(contract=contract, ex="xex", metric="q_grp_size_ccy1")

    def _get_lambda_for_quote_d(self, contract: str, ex: str, quote_d: Direction) -> float:
        ar_d = Direction.long if quote_d == Direction.short else Direction.short
        q_grp_size_in_ccy1 = self._get_q_grp_size_ccy1(contract=contract)
        tau = self.get_tau(contract=contract, ex=self._xex)
        trd_size_in_ccy1 = self.get_pred_arrival_size_in_tokens(contract=contract, ex=ex, d=ar_d)
        trd_size_in_q_grp = trd_size_in_ccy1 / q_grp_size_in_ccy1
        trd_time_interval_in_ms = self.get_pred_arrival_time(contract=contract, ex=ex, d=ar_d)
        num_trades_in_tau = max(tau / trd_time_interval_in_ms, 1)
        ar_in_q_grp = num_trades_in_tau * trd_size_in_q_grp
        ar_in_q_grp = max(ar_in_q_grp, - self._epsilon + self._numeric_tol)
        return ar_in_q_grp

    def _calib_Q_func(self, side: List[Level]) -> Tuple[float, float]:
        if len(side) == 0:
            return 0, 0
        if len(side) == 1:
            return 0, side[0].qty
        Q = 0
        Qs = []
        ps = []
        for lvl in side:
            Q += lvl.qty
            Qs.append(Q)
            ps.append(lvl.prc)
        X = np.abs(np.array(ps) - ps[0])
        y = np.array(Qs) - Qs[0]
        beta = 1 / X.dot(X) * X.dot(y)
        return beta, Qs[0]

    def update_depth(self, contract: str, ex: str, depth: Depth, amt_bnd):
        if contract not in self._depth_metrics:
            self._depth_metrics[contract] = {}
        if ex not in self._depth_metrics[contract]:
            self._depth_metrics[contract][ex] = {}
        tilde_p = self._depth_to_ref_prc(depth=depth, amt_bnd=amt_bnd)
        best_ask = depth.asks[0].prc if len(depth.asks) > 0 else math.inf
        best_bid = depth.bids[0].prc if len(depth.bids) > 0 else 0
        s_l = tilde_p - best_bid
        s_s = best_ask - tilde_p
        if not self.has_q_grp_size_ccy2(contract=contract):
            self.init_q_grp_size_ccy2_from_depth(depth=depth)
        q_grp_size_ccy2 = self.get_q_grp_size_ccy2(contract=contract)
        q_grp_size = q_grp_size_ccy2 / tilde_p  # CCY1
        int_depth = depth.get_qty_grp_view(q_grp_size=q_grp_size)
        metrics = dict(
            tilde_p=tilde_p,
            best_bid=best_bid,
            best_ask=best_ask,
            s_l=s_l,
            s_s=s_s,
            q_grp_size_ccy1=q_grp_size,
        )
        if int_depth.meta_data.depth_completeness_state == DepthCompletenessState.depth_update:
            a_l, b_l = self._calib_Q_func(side=int_depth.bids)
            a_s, b_s = self._calib_Q_func(side=int_depth.asks)
            metrics.update(
                a_l=a_l,
                b_l=b_l,
                a_s=a_s,
                b_s=b_s,
            )
        elif int_depth.meta_data.depth_completeness_state == DepthCompletenessState.bbo_update:
            a_l, b_l = self._calib_Q_func(side=int_depth.bids)
            a_s, b_s = self._calib_Q_func(side=int_depth.asks)
            metrics.update(
                a_l=a_l,
                b_l=b_l,
                a_s=a_s,
                b_s=b_s,
            )
            # if len(int_depth.bids) > 0:
            #     metrics.update(
            #         b_l=int_depth.bids[0].qty,
            #     )
            # if len(int_depth.asks) > 0:
            #     metrics.update(
            #         b_s=int_depth.asks[0].qty,
            #     )
        else:
            raise NotImplementedError()
        self._depth_metrics[contract][ex].update(metrics)

    def remove_depth_metrics(self, contract: str, ex: str):
        if contract in self._depth_metrics:
            if ex in self._depth_metrics[contract]:
                self._depth_metrics[contract].pop(ex)

    def update_bbo(self, contract: str, ex: str, bbo: BBO):
        if contract not in self._depth_metrics:
            self._depth_metrics[contract] = {}
        if ex not in self._depth_metrics[contract]:
            self._depth_metrics[contract][ex] = {}
        tilde_p = self._bbo_to_ref_prc(bbo=bbo)
        best_ask = bbo.best_ask_prc if bbo.valid_best_ask else math.inf
        best_bid = bbo.best_bid_prc if bbo.valid_best_bid else 0
        s_l = tilde_p - best_bid
        s_s = best_ask - tilde_p
        q_grp_size = self.get_q_grp_size_ccy2(contract=contract) / tilde_p  # CCY1
        b_l = bbo.best_bid_qty
        b_s = bbo.best_ask_qty
        _map = dict(
            tilde_p = tilde_p,
            best_bid = best_bid,
            best_ask = best_ask,
            s_l = s_l,
            s_s = s_s,
            b_l = b_l,
            b_s = b_s,
            q_grp_size_ccy1 = q_grp_size,
        )
        self._depth_metrics[contract][ex].update(_map)

    def get_depth_metric(self, contract: str, ex: str, metric: str) -> float:
        return self._depth_metrics[contract][ex][metric]

    def get_depth_metrics(self, contract: str, ex: str) -> Dict:
        return self._depth_metrics[contract][ex]

    def has_depth_metrics(self, contract: str, ex: str) -> bool:
        if contract not in self._depth_metrics:
            return False
        if ex not in self._depth_metrics[contract]:
            return False
        return True

    def update_ar_return_space_lower_bound(self, contract: str, val: float):
        self._arrival_rate_return_space_lower_bound_map[contract] = val

    def update_ar_return_space_upper_bound(self, contract: str, val: float):
        self._arrival_rate_return_space_upper_bound_map[contract] = val

    def _get_ar_return_space_lower_bound(self, contract: str):
        return self._arrival_rate_return_space_lower_bound_map[contract]

    def _get_ar_return_space_upper_bound(self, contract: str):
        return self._arrival_rate_return_space_upper_bound_map[contract]

    ############################################ Computation Methods #################################################

    def _depth_to_ref_prc(self, depth: Depth, amt_bnd: float):
        return self._ref_prc_calculator.calc_from_depth(depth=depth, amt_bnd=amt_bnd)

    def _bbo_to_ref_prc(self, bbo: BBO):
        return self._ref_prc_calculator.calc_from_bbo(bbo=bbo)

    def _get_unit_c_maker(self, contract: str, ex: str, p: float, q_tick_size: float):
        return p * self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker) * q_tick_size

    def _get_unit_c_taker(self, contract: str, ex: str, p: float, q_tick_size: float):
        return p * self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker) * q_tick_size

    def _c_pnl(self, p_hat: float, s_o_hat: float, tilde_p: float, s_d: float, d: Direction, q_tick_size: float):
        I_d = 1 if d == Direction.long else -1
        return (I_d * (p_hat - tilde_p) + s_d - s_o_hat) * q_tick_size

    def _pnl(self, p_hat: float, s_o_hat: float, tilde_p: float, s_d: float, delta_d: float, d: Direction, q_tick_size: float):
        return delta_d * q_tick_size + self._c_pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, d=d, q_tick_size=q_tick_size)

    def _U_taker(self, pnl: float, c_taker: float, beta: float, dR1: float):
        return pnl - c_taker - beta * dR1

    def _Q_d(self, delta_d: float, a_d: float, b_d: float):
        return a_d * delta_d + b_d

    def _a_d_1(self, k_1: float, lambda_d: float):
        return 1 / k_1**2 / lambda_d

    def _b_d_1(self, k_1: float, k_2: float, lambda_d: float, epsilon: float, g: float):
        val = (k_1 * math.sqrt(lambda_d) - lambda_d - epsilon) / (k_1 ** 2 * lambda_d)
        if lambda_d + epsilon < k_1 * math.sqrt(lambda_d):
            val += self._c_d(k_1=k_1, k_2=k_2, lambda_d=lambda_d, epsilon=epsilon, g=g)
        return val

    def _a_d_2(self, k_2: float, lambda_d: float):
        return - 1 / k_2**2 / lambda_d

    def _b_d_2(self, k_1: float, k_2: float, lambda_d: float, epsilon: float, g: float):
        val = (k_2*math.sqrt(lambda_d)+lambda_d+epsilon) / (k_2**2 * lambda_d)
        if lambda_d + epsilon < k_1 * math.sqrt(lambda_d):
            val += self._c_d(k_1=k_1, k_2=k_2, lambda_d=lambda_d, epsilon=epsilon, g=g)
        return val

    def _c_d(self, k_1: float, k_2: float, lambda_d: float, epsilon: float, g: float):
        return (k_1 * math.sqrt(lambda_d) - lambda_d - epsilon)**2 / (2 * g * k_1**2 * lambda_d * (k_2 * math.sqrt(lambda_d) + lambda_d + epsilon))

    def _dR_q(self, tilde_p: float, sigma: float, T: float, tau: float, I: int, q: int, d: Direction, q_tick_size: float):
        I_d = 1 if d == Direction.long else -1
        return 0.5 * (tilde_p * sigma) ** 2 * (T - tau) * (2 * I * I_d * q + q ** 2) * q_tick_size ** 2

    def _pr(self, delta: float, a_p: float, b_p: float, c_p: float):
        return a_p * delta ** 2 + b_p * delta + c_p

    def _U_maker(self, pr: float, pnl: float, beta: float, dR1_d: float, c_maker: float):
        return pr * (pnl - c_maker - beta * dR1_d)

    def _range_bounding(self, x: float, lb: float, ub: float, l_close: bool, r_close: bool):
        if x < lb:
            if l_close:
                return lb
            return lb  + self._numeric_tol
        if x > ub:
            if r_close:
                return ub
            return ub - self._numeric_tol
        return x

    def _delta_hat(self,
                   d: Direction,
                   p_hat: float,
                   s_o_hat: float,
                   tilde_p: float,
                   s_d: float,
                   I: float,
                   ex_I: float,
                   q: float,
                   sigma: float,
                   tau: float,
                   q_beta: float,
                   I_beta: float,
                   c_maker: float,
                   latency: float,
                   ) -> float:
        Iq = 2 * I_beta * (I + ex_I) * d * q_beta * q  + (q_beta * q) ** 2
        delta = sigma * tilde_p * math.sqrt(tau + latency) * Iq / q + c_maker * tilde_p + s_o_hat - d * (p_hat - tilde_p) - s_d
        return delta + self._numeric_tol

    def _U_from_delta(self,
                      q_tick_size_ccy1: float,
                      d: Direction,
                      p_hat: float,
                      s_o_hat: float,
                      tilde_p: float,
                      s_d: float,
                      delta_arr: np.ndarray,
                      I: float,
                      ex_I: float,
                      q: float,
                      sigma:float,
                      tau: float,
                      q_beta: float,
                      I_beta: float,
                      c_maker: float,
                      latency: float,
                      ) -> np.ndarray:
        unit_pnl = (d * (p_hat - tilde_p) + s_d + delta_arr - s_o_hat) - c_maker * tilde_p
        pnl = q * q_tick_size_ccy1 * unit_pnl
        Iq = 2 * I_beta * (I + ex_I) * d * q_beta * q + (q_beta * q) ** 2
        dr = sigma * tilde_p * math.sqrt(tau + latency) * q_tick_size_ccy1 * Iq
        return pnl - q_beta * dr

    def _optimal_price_level_q1(self, contract: str, ex: str, d: Direction, beta: float, T: float, tau: float, I: int) -> Tuple[float, float]:
        xex = "xex"
        SEx_tilde_p = self.get_depth_metric(contract=contract, ex=ex, metric="tilde_p")
        SEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        XEx_tilde_p = self.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        XEx_s_l = self.get_depth_metric(contract=contract, ex=xex, metric="s_l")
        XEx_s_s = self.get_depth_metric(contract=contract, ex=xex, metric="s_s")
        s_d = XEx_s_l if d == Direction.long else XEx_s_s
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        q_grp_size = self._get_q_grp_size_ccy1(contract=contract)
        c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=XEx_tilde_p, q_tick_size=q_grp_size)

        # do nothing
        max_U = 0
        delta_d_hat = np.nan

        # optimal for delta_l in (-inf, -s_l-s_s]
        if d == Direction.long:
            taker_delta_bnd = (XEx_tilde_p - XEx_s_l) - (SEx_tilde_p + SEx_s_s)
        else:
            taker_delta_bnd = (SEx_tilde_p - SEx_s_l) - (XEx_tilde_p + XEx_s_s)
        delta = taker_delta_bnd
        dR1 = self._dR_q(tilde_p=XEx_tilde_p, sigma=sigma, T=T, tau=tau, I=I, q=1, d=d, q_tick_size=q_grp_size)
        max_taker_U = self._U_taker(pnl=self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_grp_size),
                                    c_taker=c_taker, beta=beta, dR1=dR1)
        if max_taker_U > max_U:
            # print(f"taker: ex={ex}, d={d} U={max_taker_U}, delta={delta_d}")
            max_U = max_taker_U
            delta_d_hat = delta

        # optimal for  delta_l in (-s_l-s_s, +inf)

        """ delta_l in (-s_l-s_s, 0) """
        lambda_d = self._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=d, tau=tau)
        epsilon = self._epsilon
        a_d_1 = self._a_d_1(k_1=self._k_1, lambda_d=lambda_d)
        b_d_1 = self._b_d_1(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_2 = self._a_d_2(k_2=self._k_2, lambda_d=lambda_d)
        b_d_2 = self._b_d_2(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_name = "a_l" if d == Direction.long else "a_s"
        b_d_name = "b_l" if d == Direction.long else "b_s"
        a_d = self.get_depth_metric(contract=contract, ex=xex, metric=a_d_name)
        b_d = self.get_depth_metric(contract=contract, ex=xex, metric=b_d_name)
        c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=XEx_tilde_p, q_tick_size=q_grp_size)
        vtx1 = (-b_d_1 - a_d_1 * b_d) / (a_d_1 * a_d)
        vtx2 = (lambda_d + epsilon - b_d) / a_d
        vtx3 = (-b_d_2 - a_d_1 * b_d) / (a_d_2 * a_d)

        """ range -s_l-s_s <= delta <= 0: quoting in center, no take no queue """
        lb = taker_delta_bnd
        ub = 0
        if lb < ub:  # range exists
            delta = -self._numeric_tol
            pnl = self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_grp_size)
            if vtx1 > 0:
                U = pnl - c_maker - beta * dR1
            elif (vtx1 <= 0) and (0 <= vtx2):
                U = (1 - b_d_1) * (pnl - c_maker - beta * dR1)
            elif (vtx2 <= 0) and (0 <= vtx3):
                U = (1 - b_d_2) * (pnl - c_maker - beta * dR1)
            else:
                U = 0
            if U > max_U + self._numeric_tol:
                # print(f"maker 0: ex={ex}, d={d} U={max_maker_U}, delta={delta_d}")
                max_U = U
                delta_d_hat = delta

        """ max(0, -s_l-s_s) <= delta <= vtx1 """
        lb = max(taker_delta_bnd, 0)
        ub = vtx1
        if lb <= ub:  # range exists
            delta = ub
            pnl = self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_grp_size)
            U = pnl - c_maker - beta * dR1
            if U > max_U + self._numeric_tol:
                max_U = U
                delta_d_hat = delta

        """ max(0, -s_l-s_s, vtx1) <= delta <= vtx2 """
        ub = vtx2
        lb = max(0, taker_delta_bnd, vtx1)
        if ub >= lb:  # range exists
            a_p_1 = - a_d_1 * a_d ** 2 / 2
            b_p_1 = - a_d * (a_d_1 * b_d + b_d_1)
            if vtx1 <= 0:
                c_p_1 = 1
            else:
                c_p_1 = 1 + a_d_1 * a_d ** 2 / 2 * vtx1 ** 2  + a_d * (a_d_1 * b_d + b_d_1) * vtx1
            c_pnl = self._c_pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, d=d, q_tick_size=q_grp_size)
            a_u = 3 * a_p_1
            b_u = 2 * a_p_1 * (c_pnl - c_maker - beta * dR1) + b_p_1
            c_u = c_p_1 + b_p_1 * (c_pnl - c_maker - beta * dR1)
            _x = b_u ** 2 - 4 * a_u * c_u
            if _x >= 0:
                roots = [(-b_u + math.sqrt(_x))/2/a_u, (-b_u - math.sqrt(_x))/2/a_u]
            else:
                roots = [lb, ub]
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            for delta in roots:
                U = self._U_maker(
                    pr=self._pr(delta=delta, a_p=a_p_1, b_p=b_p_1, c_p=c_p_1),
                    pnl=self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_grp_size),
                    beta=beta, dR1_d=dR1, c_maker=c_maker
                )
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta

        """ max(vtx2, 0, -s_l-s_s) <= delta <= vtx3 """
        lb = max(vtx2, taker_delta_bnd, 0)
        ub = vtx3
        if ub > lb:
            a_p = - a_d_2 * a_d ** 2 / 2
            b_p = - a_d * (a_d_2 * b_d + b_d_2)
            if vtx2 >= 0:
                a_p_1 = - a_d_1 * a_d ** 2 / 2
                b_p_1 = - a_d * (a_d_1 * b_d + b_d_1)
                if vtx1 <= 0:
                    c_p_1 = 1
                else:
                    c_p_1 = 1 + a_d_1 * a_d ** 2 / 2 * vtx1 ** 2 + a_d * (a_d_1 * b_d + b_d_1) * vtx1
                cdf_1 = self._pr(delta=vtx2, a_p=a_p_1, b_p=b_p_1, c_p=c_p_1)
                c_p = 1 - cdf_1 + a_d_2 * a_d ** 2 / 2 * vtx2 ** 2 + a_d * (a_d_2 * b_d + b_d_2) * vtx2
            else:
                c_p = 1
            c_pnl = self._c_pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, d=d, q_tick_size=q_grp_size)
            a_u = 3 * a_p
            b_u = 2 * a_p * (c_pnl - c_maker - beta * dR1) + b_p
            c_u = c_p + b_p * (c_pnl - c_maker - beta * dR1)
            _x = b_u ** 2 - 4 * a_u * c_u
            if _x >= 0:
                roots = [(-b_u + math.sqrt(_x)) / 2 / a_u, (-b_u - math.sqrt(_x)) / 2 / a_u]
            else:
                roots = [lb, ub]
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            for delta in roots:
                U = self._U_maker(
                    pr=self._pr(delta=delta, a_p=a_p, b_p=b_p, c_p=c_p),
                    pnl=self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=XEx_tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_grp_size),
                    beta=beta, dR1_d=dR1, c_maker=c_maker
                )
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta

        """ delta_l in (max(-(b_d_2 + a_d_2 * b_d) / (a_d_2 * a_d), -s_l-s_s), inf) """
        # fill prob = 0, U = 0

        return max_U, delta_d_hat

    def optimal_price_level_q1(self, contract: str, ex: str, d: Direction, beta: float, T: float, tau: float, I: float) -> Tuple[float, float]:
        int_I = int(round(I / self._get_q_grp_size_ccy1(contract=contract)))
        return self._optimal_price_level_q1(contract=contract, ex=ex, d=d, beta=beta, T=T, tau=tau, I=int_I)

    def _optimal_price_level_q(self, contract: str, ex: str, q: int, d: Direction, beta: float, T: float, tau: float, I: int, U_lb: float) -> Tuple[float, float, int]:
        xex = "xex"
        SEx_tilde_p = self.get_depth_metric(contract=contract, ex=ex, metric="tilde_p")
        SEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        SEx_a_l = self.get_depth_metric(contract=contract, ex=ex, metric="a_l")
        SEx_a_s = self.get_depth_metric(contract=contract, ex=ex, metric="a_s")
        SEx_b_l = self.get_depth_metric(contract=contract, ex=ex, metric="b_l")
        SEx_b_s = self.get_depth_metric(contract=contract, ex=ex, metric="b_s")
        tilde_p = self.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        XEx_s_l = self.get_depth_metric(contract=contract, ex=xex, metric="s_l")
        XEx_s_s = self.get_depth_metric(contract=contract, ex=xex, metric="s_s")
        s_d = XEx_s_l if d == Direction.long else XEx_s_s
        SEx_b_o = SEx_b_s if d == Direction.long else SEx_b_l
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        q_tick_size = self._get_q_grp_size_ccy1(contract=contract)
        c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=tilde_p, q_tick_size=1)

        # do nothing
        max_U = U_lb - self._numeric_tol
        delta_d_hat = np.nan
        taker_maker_flag = MakerTaker.undefined

        # optimal for delta_l in (-inf, -s_l-s_s]
        if d == Direction.long:
            taker_bnd = (tilde_p - XEx_s_l) - (SEx_tilde_p + SEx_s_s)
            A = -SEx_a_s
            B = SEx_a_s * taker_bnd + SEx_b_s
        else:
            taker_bnd = (SEx_tilde_p - SEx_s_l) - (tilde_p + XEx_s_s)
            A = -SEx_a_l
            B = SEx_a_l * taker_bnd + SEx_b_l

        lambda_d = self._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=d, tau=tau)
        epsilon = self._epsilon
        a_d_1 = self._a_d_1(k_1=self._k_1, lambda_d=lambda_d)
        b_d_1 = self._b_d_1(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_2 = self._a_d_2(k_2=self._k_2, lambda_d=lambda_d)
        b_d_2 = self._b_d_2(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_name = "a_l" if d == Direction.long else "a_s"
        b_d_name = "b_l" if d == Direction.long else "b_s"
        a_d = max(self.get_depth_metric(contract=contract, ex=xex, metric=a_d_name), self._numeric_tol)
        b_d = max(self.get_depth_metric(contract=contract, ex=xex, metric=b_d_name), self._numeric_tol)
        c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=tilde_p, q_tick_size=1)
        q_v1 = - b_d_1 / a_d_1
        q_v2 = lambda_d + epsilon
        q_v3 = - b_d_2 / a_d_2

        """ range 1: delta \in [-inf, taker_bnd] -> have taker """

        pure_taker_bnd = (q - B) / A if q >= SEx_b_o else taker_bnd
        if pure_taker_bnd >= taker_bnd + self._numeric_tol:
            raise RuntimeError()

        """ range 1.1: delta \in [-inf, pure_taker_bnd] -> pure taker """
        delta = pure_taker_bnd
        U = q * q_tick_size * (self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=1) - c_taker) \
            - beta * self._dR_q(tilde_p=tilde_p, sigma=sigma, T=T, tau=tau, I=I, q=q, d=d, q_tick_size=q_tick_size)
        if U > max_U + self._numeric_tol:
            max_U = U
            delta_d_hat = delta
            taker_maker_flag = MakerTaker.taker

        """ range 1.2.1: delta \in [pure_taker_bnd, min(taker_bnd,0)] -> taker + maker """

        A_ub_1 = - A
        B_ub_1 = q - B
        A_ub_2 = A ** 2
        B_ub_2 = -2 * A * (q - B)
        C_ub_2 = (q - B) ** 2

        delta_v1 = (q_v1 - B_ub_1) / A_ub_1
        delta_v2 = (q_v2 - B_ub_1) / A_ub_1
        delta_v3 = (q_v3 - B_ub_1) / A_ub_1

        C_R = -beta * 0.5 * (q_tick_size * tilde_p * sigma) ** 2 * (T - tau)
        A_u_taker = q_tick_size * A + C_R * A ** 2
        I_d = 1 if d == Direction.long else -1
        B_u_taker = q_tick_size * (A * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + B) + C_R * (2 * A * B + 2 * I * I_d * A)
        C_u_taker = q_tick_size * B * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + C_R * (B ** 2 + 2 * I * I_d * B)

        A_u_full = -q_tick_size * A + C_R * A ** 2
        B_u_full = q_tick_size * (-A * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_maker) + q - B) + C_R * (
                    -2 * A * (q - B) - 2 * I * I_d * A)
        C_u_full = q_tick_size * (q - B) * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_maker) + C_R * (
                    (q - B) ** 2 + 2 * I * I_d * (q - B))

        """ range 1.2.1.1: delta \in [pure_taker_bnd, min(taker_bnd, delta_v1, 0)] -> taker + maker: < vtx1 """
        lb = pure_taker_bnd
        ub = min(taker_bnd, delta_v1, 0)
        if lb <= ub:
            root = - (B_u_taker + B_u_full) / 2 / (A_u_taker + A_u_full)
            delta = self._range_bounding(x=root, lb=lb, ub=ub, l_close=False, r_close=True)
            U = (A_u_taker + A_u_full) * delta ** 2 + (B_u_taker + B_u_full) * delta + (C_u_taker + C_u_full)
            if U > max_U + self._numeric_tol:
                delta_d_hat = delta
                max_U = U
                taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.1.2: delta \in [max(pure_taker_bnd, delta_v1), min(taker_bnd, delta_v2, 0)] -> taker + maker: vtx1 ~ vtx2 """
        A_p_full = - a_d_1 / 2 * A_ub_2
        B_p_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
        C_p_full = a_d_1 / 2 * q_v2 ** 2 + b_d_1 * q_v2 - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * q_v2 ** 2 - b_d_2 * q_v2

        A_E_full = A_p_full * A_u_full
        B_E_full = A_p_full * B_u_full + A_u_full * B_p_full
        C_E_full = A_p_full * C_u_full + B_p_full * B_u_full + C_p_full * A_u_full
        D_E_full = C_p_full * B_u_full + B_p_full * C_u_full
        E_E_full = C_p_full * C_u_full

        lb = max(pure_taker_bnd, delta_v1)
        ub = min(taker_bnd, delta_v2, 0)
        if lb <= ub:
            poly_arr = np.array([A_E_full, B_E_full, C_E_full + A_u_taker, D_E_full + B_u_taker, E_E_full + C_u_taker])
            roots = np.roots(poly_arr)
            roots = roots[np.isreal(roots)].real
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            Us = np.polyval(poly_arr, roots)
            for U, delta in zip(Us, roots):
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta
                    taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.1.3: delta \in [max(pure_taker_bnd, delta_v2), min(taker_bnd, delta_v3, 0)] -> taker + maker: vtx2 ~ vtx3 """
        A_p_full = - a_d_2 / 2 * A_ub_2
        B_p_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
        C_p_full = a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1

        A_E_full = A_p_full * A_u_full
        B_E_full = A_p_full * B_u_full + A_u_full * B_p_full
        C_E_full = A_p_full * C_u_full + B_p_full * B_u_full + C_p_full * A_u_full
        D_E_full = C_p_full * B_u_full + B_p_full * C_u_full
        E_E_full = C_p_full * C_u_full

        lb = max(pure_taker_bnd, delta_v2)
        ub = min(taker_bnd, delta_v3, 0)
        if lb <= ub:
            poly_arr = np.array([A_E_full, B_E_full, C_E_full + A_u_taker, D_E_full + B_u_taker, E_E_full + C_u_taker])
            roots = np.roots(poly_arr)
            roots = roots[np.isreal(roots)].real
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            Us = np.polyval(poly_arr, roots)
            for U, delta in zip(Us, roots):
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta
                    taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.1.4: delta \in [max(pure_taker_bnd, delta_v3), min(taker_bnd, 0)] -> taker + maker: > vtx3 """
        lb = max(pure_taker_bnd, delta_v3)
        ub = min(taker_bnd, 0)
        if lb <= ub:
            delta = - B_u_taker / A_u_taker
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = A_u_taker * delta ** 2 + B_u_taker * delta + C_u_taker
            if U > max_U + self._numeric_tol:
                max_U = U
                delta_d_hat = delta
                taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.2: delta \in [max(0,pure_taker_bnd), taker_bnd] -> taker + maker """

        A_ub_1 = a_d - A
        B_ub_1 = q - b_d - B
        A_ub_2 = (a_d - A) ** 2
        B_ub_2 = 2 * (a_d - A) * (q + b_d - B)
        C_ub_2 = (q + b_d - B) ** 2
        delta_v1 = (q_v1 - B_ub_1) / A_ub_1
        delta_v2 = (q_v2 - B_ub_1) / A_ub_1
        delta_v3 = (q_v3 - B_ub_1) / A_ub_1

        C_R = -beta * 0.5 * (q_tick_size * tilde_p * sigma) ** 2 * (T - tau)
        A_u_taker = q_tick_size * A + C_R * A**2
        I_d = 1 if d == Direction.long else -1
        B_u_taker = q_tick_size * (A * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + B) + C_R * (2 * A * B + 2 * I * I_d * A)
        C_u_taker = q_tick_size * B * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + C_R * (B ** 2 + 2 * I * I_d * B)

        A_u_full = -q_tick_size * A+C_R*A**2
        B_u_full = q_tick_size * (-A * (I_d * (p_hat - tilde_p)+s_d-s_o_hat-c_maker) + q - B) + C_R*(-2 * A * (q - B) - 2*I*I_d * A)
        C_u_full = q_tick_size * (q - B) * (I_d * (p_hat - tilde_p)+s_d-s_o_hat-c_maker) + C_R * ((q - B) ** 2 + 2 * I * I_d * (q - B))

        """ range 1.2.2.1: delta \in [max(pure_taker_bnd, 0), min(taker_bnd, delta_v1)] -> taker + maker: < vtx1 """
        lb = max(pure_taker_bnd, 0)
        ub = min(taker_bnd, delta_v1)
        if lb <= ub:
            root = - (B_u_taker + B_u_full) / 2 / (A_u_taker + A_u_full)
            delta = self._range_bounding(x=root, lb=lb, ub=ub, l_close=False, r_close=True)
            U = (A_u_taker + A_u_full) * delta ** 2 + (B_u_taker + B_u_full) * delta + (C_u_taker + C_u_full)
            if U > max_U + self._numeric_tol:
                delta_d_hat = delta
                max_U = U
                taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.2.2: delta \in [max(pure_taker_bnd, delta_v1, 0), min(taker_bnd, delta_v2)] -> taker + maker: vtx1 ~ vtx2 """
        A_p_full = - a_d_1 / 2 * A_ub_2
        B_p_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
        C_p_full = a_d_1/2*q_v2**2 + b_d_1 * q_v2 - a_d_1/2*C_ub_2 - b_d_1 * B_ub_1 + a_d_2/2 * q_v3**2 + b_d_2 * q_v3 - a_d_2/2 * q_v2**2 - b_d_2*q_v2

        A_E_full = A_p_full * A_u_full
        B_E_full = A_p_full * B_u_full + A_u_full * B_p_full
        C_E_full = A_p_full * C_u_full + B_p_full * B_u_full + C_p_full * A_u_full
        D_E_full = C_p_full * B_u_full + B_p_full * C_u_full
        E_E_full = C_p_full * C_u_full

        lb = max(pure_taker_bnd, delta_v1, 0)
        ub = min(taker_bnd, delta_v2)
        if lb <= ub:
            poly_arr = np.array([A_E_full, B_E_full, C_E_full + A_u_taker, D_E_full + B_u_taker, E_E_full + C_u_taker])
            roots = np.roots(poly_arr)
            roots = roots[np.isreal(roots)].real
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            Us = np.polyval(poly_arr, roots)
            for U, delta in zip(Us, roots):
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta
                    taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.2.3: delta \in [max(pure_taker_bnd, delta_v2, 0), min(taker_bnd, delta_v3)] -> taker + maker: vtx2 ~ vtx3 """
        A_p_full = - a_d_2 / 2 * A_ub_2
        B_p_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
        C_p_full = a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1

        A_E_full = A_p_full * A_u_full
        B_E_full = A_p_full * B_u_full + A_u_full * B_p_full
        C_E_full = A_p_full * C_u_full + B_p_full * B_u_full + C_p_full * A_u_full
        D_E_full = C_p_full * B_u_full + B_p_full * C_u_full
        E_E_full = C_p_full * C_u_full

        lb = max(pure_taker_bnd, delta_v2, 0)
        ub = min(taker_bnd, delta_v3)
        if lb <= ub:
            poly_arr = np.array([A_E_full, B_E_full, C_E_full + A_u_taker, D_E_full + B_u_taker, E_E_full + C_u_taker])
            roots = np.roots(poly_arr)
            roots = roots[np.isreal(roots)].real
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            Us = np.polyval(poly_arr, roots)
            for U, delta in zip(Us, roots):
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta
                    taker_maker_flag = MakerTaker.partial_taker

        """ range 1.2.2.4: delta \in [max(pure_taker_bnd, delta_v3, 0), taker_bnd] -> taker + maker: > vtx3 """
        lb = max(pure_taker_bnd, delta_v3, 0)
        ub = taker_bnd
        if lb <= ub:
            delta = - B_u_taker / A_u_taker
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = A_u_taker * delta ** 2 + B_u_taker * delta + C_u_taker
            if U > max_U + self._numeric_tol:
                max_U = U
                delta_d_hat = delta
                taker_maker_flag = MakerTaker.partial_taker

        """ range 1.3: delta \in [taker_bnd, +inf] -> maker """
        A_u_full = q_tick_size * q
        B_u_full = q_tick_size * q * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_maker) + C_R * (2 * I * I_d * q + q ** 2)

        """ range 1.3.1: delta \in [taker_bnd, 0] -> maker """

        if taker_bnd <= 0:
            delta = -self._numeric_tol
            _U = A_u_full * delta + B_u_full
            if q <= q_v1:
                """ range 1.3.1.1: maker: q < vtx1 """
                U = _U
            elif (q_v1 <= q) and (q <= q_v2):
                """ range 1.3.1.2: maker: vtx1 ~ vtx2 """
                pr = a_d_1 / 2 * q_v2 ** 2 + b_d_1 * q_v2 - a_d_1 / 2 * q ** 2 - b_d_1 * q + a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * q_v2 ** 2 - b_d_2 * q_v2
                U = _U * pr
            elif (q_v2 <= q) and (q <= q_v3):
                """ range 1.3.1.3: maker: vtx2 ~ vtx3 """
                pr = a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * q ** 2 - b_d_2 * q
                U= _U * pr

            if U > max_U + self._numeric_tol:
                max_U = U
                delta_d_hat = delta
                taker_maker_flag = MakerTaker.maker

        """ range 1.3.2: delta \in [max(0, taker_bnd), +inf] -> maker """

        A_ub_1 = a_d
        B_ub_1 = b_d + q
        A_ub_2 = a_d ** 2
        B_ub_2 = 2 * a_d * (q + b_d)
        C_ub_2 = (q + b_d) ** 2
        delta_v1 = (q_v1 - B_ub_1) / A_ub_1
        delta_v2 = (q_v2 - B_ub_1) / A_ub_1
        delta_v3 = (q_v3 - B_ub_1) / A_ub_1

        """ range 1.3.2.1: delta \in [max(0, taker_bnd), delta_v1] -> maker: < vtx1 """
        lb = max(0, taker_bnd)
        ub = delta_v1
        if lb <= ub:
            delta = delta_v1
            U = A_u_full * delta + B_u_full
            if U > max_U + self._numeric_tol:
                max_U = U
                delta_d_hat = delta
                taker_maker_flag = MakerTaker.maker

        """ range 1.3.2.2: delta \in [max(taker_bnd, delta_v1, 0), delta_v2] -> maker: vtx1 ~ vtx2 """
        A_p_full = - a_d_1 / 2  * A_ub_2
        B_p_full = - a_d_1 / 2  * B_ub_2 - b_d_1 * A_ub_1
        C_p_full = a_d_1 / 2 * q_v2 ** 2 + b_d_1 * q_v2 - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * q_v2 ** 2 - b_d_2 * q_v2

        A_E_full = A_p_full * A_u_full
        B_E_full = A_p_full * B_u_full + A_u_full * B_p_full
        C_E_full = B_p_full * B_u_full + C_p_full * A_u_full
        D_E_full = C_p_full * B_u_full

        lb = max(taker_bnd, delta_v1, 0)
        ub = delta_v2
        if lb <= ub:
            poly_arr = np.array([A_E_full, B_E_full, C_E_full, D_E_full])
            roots = np.roots(poly_arr)
            roots = roots[np.isreal(roots)].real
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            Us = np.polyval(poly_arr, roots)
            for U, delta in zip(Us, roots):
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta
                    taker_maker_flag = MakerTaker.maker

        """ range 1.3.2.3: delta \in [max(taker_bnd, delta_v2, 0), delta_v3] -> maker: vtx2 ~ vtx3 """
        A_p_full = - a_d_2 / 2 * A_ub_2
        B_p_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
        C_p_full = a_d_2 / 2 * q_v3 ** 2 + b_d_2 * q_v3 - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1

        A_E_full = A_p_full * A_u_full
        B_E_full = A_p_full * B_u_full + A_u_full * B_p_full
        C_E_full = B_p_full * B_u_full + C_p_full * A_u_full
        D_E_full = C_p_full * B_u_full

        lb = max(taker_bnd, delta_v2, 0)
        ub = delta_v3
        if lb <= ub:
            poly_arr = np.array([A_E_full, B_E_full, C_E_full, D_E_full])
            roots = np.roots(poly_arr)
            roots = roots[np.isreal(roots)].real
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            Us = np.polyval(poly_arr, roots)
            for U, delta in zip(Us, roots):
                if U > max_U + self._numeric_tol:
                    max_U = U
                    delta_d_hat = delta
                    taker_maker_flag = MakerTaker.maker

        """ range 1.3.2.4: delta \in [max(taker_bnd, delta_v3, 0), +inf] -> maker: > vtx3 """
        # can't fully fill, no utility

        return max_U, delta_d_hat, taker_maker_flag

    def optimal_price_level_q(self, contract: str, ex: str, q: float, d: Direction, beta: float, T: float, tau: float, I: float, U_lb: float) -> Tuple[float, float, int]:
        q_grp_size = self._get_q_grp_size_ccy1(contract=contract)
        int_I = int(round(I / q_grp_size))
        int_q = max(int(round(q / q_grp_size)), 1)
        return self._optimal_price_level_q(contract=contract, ex=ex, q=int_q, d=d, beta=beta, T=T, tau=tau, I=int_I, U_lb=U_lb)

    def _optimal_price_level_q2(self, contract: str, ex: str, q: int, d: Direction, I: int, ex_I: int, q_beta: float, I_beta: float) \
            -> Tuple[float, float, int, PriceSolverMessage]:
        xex = "xex"
        SEx_tilde_p = self.get_depth_metric(contract=contract, ex=ex, metric="tilde_p")
        SEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        SEx_a_l = self.get_depth_metric(contract=contract, ex=ex, metric="a_l")
        SEx_a_s = self.get_depth_metric(contract=contract, ex=ex, metric="a_s")
        SEx_b_l = self.get_depth_metric(contract=contract, ex=ex, metric="b_l")
        SEx_b_s = self.get_depth_metric(contract=contract, ex=ex, metric="b_s")
        tilde_p = self.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        XEx_s_l = self.get_depth_metric(contract=contract, ex=xex, metric="s_l")
        XEx_s_s = self.get_depth_metric(contract=contract, ex=xex, metric="s_s")
        s_d = XEx_s_l if d == Direction.long else XEx_s_s
        SEx_b_o = SEx_b_s if d == Direction.long else SEx_b_l
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        q_tick_size_ccy1 = self._get_q_grp_size_ccy1(contract=contract)

        lambda_d = self._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=d)
        a_d_name = "a_l" if d == Direction.long else "a_s"
        b_d_name = "b_l" if d == Direction.long else "b_s"
        a_d = max(self.get_depth_metric(contract=contract, ex=xex, metric=a_d_name), self._numeric_tol)
        b_d = max(self.get_depth_metric(contract=contract, ex=xex, metric=b_d_name), self._numeric_tol)
        c_maker = self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker)

        if d == Direction.long:
            taker_bnd = (tilde_p - XEx_s_l) - (SEx_tilde_p + SEx_s_s)
        else:
            taker_bnd = (SEx_tilde_p - SEx_s_l) - (tilde_p + XEx_s_s)

        msg = PriceSolverMessage()

        tau = self.get_tau(contract=contract, ex=self._xex)
        latency = self.get_pred_latency(contract=contract, ex=ex)
        loc = lambda_d + self._epsilon
        scale = math.sqrt(lambda_d)
        zs = np.arange(-3, 4, 1)
        pr_arr = 1 - fast_norm_cdf_vec(zs)
        q_arr = loc + zs * scale
        q_filter = q_arr >= 0
        msg.q_filter_ratio = np.sum(~q_filter) / q_filter.shape[0]
        if abs(msg.q_filter_ratio - 1) < self._numeric_tol:
            return 0, math.nan, MakerTaker.undefined, msg
        q_arr = q_arr[q_filter]
        pr_arr = pr_arr[q_filter]
        Q_arr = q_arr - q
        Q_filter = Q_arr >= 0
        msg.Q_filter_ratio = np.sum(~Q_filter) / Q_filter.shape[0]
        if abs(msg.Q_filter_ratio - 1) < self._numeric_tol:
            return 0, math.nan, MakerTaker.undefined, msg
        Q_arr = Q_arr[Q_filter]
        pr_arr = pr_arr[Q_filter]
        msg.Pr_lb_filter_ratio = 0
        delta_arr = (Q_arr - b_d) / a_d
        delta_lb = self._delta_hat(
            d=d,
            p_hat=p_hat,
            s_o_hat=s_o_hat,
            tilde_p=tilde_p,
            s_d=s_d,
            I=I,
            ex_I=ex_I,
            q=q,
            sigma=sigma,
            tau=tau,
            q_beta=q_beta,
            I_beta=I_beta,
            c_maker=c_maker,
            latency=latency,
        )
        delta_filter = (delta_arr > delta_lb) & (delta_arr > taker_bnd)
        delta_arr = delta_arr[delta_filter]
        pr_arr = pr_arr[delta_filter]
        if len(delta_arr) == 0:
            # return 0, np.nan, MakerTaker.maker, msg
            if d == Direction.long:
                if delta_lb > tilde_p - s_d:
                    return 0, np.nan, MakerTaker.maker, msg
            delta_arr = np.array([max(delta_lb, taker_bnd) + tilde_p * 0.0001])
            pr_arr = np.array([0])
        U_arr = self._U_from_delta(
            q_tick_size_ccy1=q_tick_size_ccy1,
            d=d,
            p_hat=p_hat,
            s_o_hat=s_o_hat,
            tilde_p=tilde_p,
            s_d=s_d,
            delta_arr=delta_arr,
            I=I,
            ex_I=ex_I,
            q=q,
            sigma=sigma,
            tau=tau,
            q_beta=q_beta,
            I_beta=I_beta,
            c_maker=c_maker,
            latency=latency,
        )
        U_filter = U_arr > 0
        msg.U_lb_filter_ratio = np.sum(~U_filter) / U_filter.shape[0]
        if abs(msg.U_lb_filter_ratio - 1) < self._numeric_tol:
            return 0, math.nan, MakerTaker.undefined, msg
        U_arr = U_arr[U_filter]
        delta_arr = delta_arr[U_filter]
        pr_arr = pr_arr[U_filter]
        EU_arr: np.ndarray = pr_arr * U_arr
        max_idx = EU_arr.argmax()
        delta_hat = delta_arr[max_idx]
        EU_hat = EU_arr[max_idx]
        return EU_hat, delta_hat, MakerTaker.maker, msg

    def optimal_price_level_q2(self, contract: str, ex: str, q: float, d: Direction,
                               I: float, ex_I: float, q_beta: float, I_beta: float) -> Tuple[float, float, int, PriceSolverMessage]:
        q_grp_size = self._get_q_grp_size_ccy1(contract=contract)
        int_I = int(round(I / q_grp_size))
        int_ex_I = int(round(ex_I / q_grp_size))
        int_q = max(int(round(q / q_grp_size)), 1)
        return self._optimal_price_level_q2(contract=contract, ex=ex, q=int_q, d=d, I=int_I, ex_I=int_ex_I, q_beta=q_beta, I_beta=I_beta)

    def _optimal_quantity(self, contract: str, ex: str, d: Direction, delta: float, beta: float, T: float, tau: float, I: int, U_lb_multiplier: float) -> Tuple[float, float]:
        xex = "xex"
        tilde_p = self.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        SEx_tilde_p = self.get_depth_metric(contract=contract, ex=ex, metric="tilde_p")
        if d == Direction.long:
            s_d = self.get_depth_metric(contract=contract, ex=xex, metric="s_l")
            s_o = self.get_depth_metric(contract=contract, ex=xex, metric="s_s")
            a_d = self.get_depth_metric(contract=contract, ex=xex, metric="a_l")
            b_d = self.get_depth_metric(contract=contract, ex=xex, metric="b_l")
            a_o = self.get_depth_metric(contract=contract, ex=xex, metric="a_s")
            b_o = self.get_depth_metric(contract=contract, ex=xex, metric="b_s")
            SEx_s_d = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
            SEx_s_o = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
            SEx_a_o = self.get_depth_metric(contract=contract, ex=ex, metric="a_s")
            SEx_b_o = self.get_depth_metric(contract=contract, ex=ex, metric="b_s")
        else:
            s_d = self.get_depth_metric(contract=contract, ex=xex, metric="s_s")
            s_o = self.get_depth_metric(contract=contract, ex=xex, metric="s_l")
            a_d = self.get_depth_metric(contract=contract, ex=xex, metric="a_s")
            b_d = self.get_depth_metric(contract=contract, ex=xex, metric="b_s")
            a_o = self.get_depth_metric(contract=contract, ex=xex, metric="a_l")
            b_o = self.get_depth_metric(contract=contract, ex=xex, metric="b_l")
            SEx_s_d = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
            SEx_s_o = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
            SEx_a_o = self.get_depth_metric(contract=contract, ex=ex, metric="a_l")
            SEx_b_o = self.get_depth_metric(contract=contract, ex=ex, metric="b_l")
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        q_tick_size_ccy1 = self._get_q_grp_size_ccy1(contract=contract)

        max_U = self._numeric_tol
        q_hat = 0

        """ Taker side """
        pnl = self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_tick_size_ccy1)
        c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=tilde_p, q_tick_size=q_tick_size_ccy1)
        a_u = -beta * 0.5 * (q_tick_size_ccy1 * tilde_p * sigma) ** 2 * (T - tau)
        I_d = 1 if d == Direction.long else -1
        b_u = pnl - c_taker - beta * (q_tick_size_ccy1 * tilde_p * sigma) ** 2 * (T - tau) * I * I_d
        if d == Direction.long:
            taker_delta_bnd = tilde_p - s_d - SEx_tilde_p - SEx_s_o
        else:
            taker_delta_bnd = SEx_tilde_p - SEx_s_o - tilde_p - s_d
        if delta <= taker_delta_bnd:
            roots = [0, -b_u/a_u]
            lb = 0
            if d == Direction.long:
                p = tilde_p - s_d - delta
                delta_o = p - SEx_tilde_p - SEx_s_o
            else:
                p = tilde_p + s_d + delta
                delta_o = SEx_tilde_p - SEx_s_o - p
            Q = self._Q_d(delta_d=delta_o, a_d=SEx_a_o, b_d=SEx_b_o)
            ub = Q
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            for q in roots:
                U = a_u * q**2 + b_u * q
                if U > max_U + self._numeric_tol:
                    q_hat = q
                    max_U = U

        """ delta_d > -s_d-s_o """
        if (delta > taker_delta_bnd) and (delta < 0):
            Q = 0
        else:
            Q = self._Q_d(delta_d=delta, a_d=a_d, b_d=b_d)
        lambda_d = self._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=d, tau=tau)
        a_d_1 = self._a_d_1(k_1=self._k_1, lambda_d=lambda_d)
        b_d_1 = self._b_d_1(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=self._epsilon, g=self._g)
        a_d_2 = self._a_d_2(k_2=self._k_2, lambda_d=lambda_d)
        b_d_2 = self._b_d_2(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=self._epsilon, g=self._g)
        c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=tilde_p, q_tick_size=q_tick_size_ccy1)
        b_u = pnl - c_maker - beta * (q_tick_size_ccy1 * tilde_p * sigma) ** 2 * (T - tau) * I * I_d

        vtx1 = -b_d_1/a_d_1
        vtx2 = lambda_d + self._epsilon
        vtx3 = -b_d_2/a_d_2

        """ range 1: [0, vtx1] """
        if vtx1 >= 0:  # range exists
            if Q <= vtx1:  # q exists
                roots = [0, -b_u/a_u]
                lb = 0
                ub = vtx1 - Q
                roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
                for q in roots:
                    U = a_u * q**2 + b_u * q
                    if U > max_U + self._numeric_tol:
                        q_hat = q
                        max_U = U

        """ range 2: [vtx1, vtx2] """
        if vtx2 >= 0:  # range exists
            if Q <= vtx2:  # q exists
                lb = max(0., vtx1 - Q)
                ub = vtx2 - Q
                if vtx1 <= 0:  # integrate from 0
                    a_p_1 = - a_d_1/2
                    b_p_1 = - (a_d_1 * Q + b_d_1)
                    c_p_1 = 1 - a_d_1/2 * Q ** 2 - b_d_1 * Q
                else:  # integrate from vtx1
                    a_p_1 = - a_d_1 / 2
                    b_p_1 = - (a_d_1 * Q + b_d_1)
                    c_p_1 = 1 - a_d_1 / 2 * Q ** 2 - b_d_1 * Q + a_d_1/2*(-b_d_1/a_d_1)**2 + b_d_1*(-b_d_1/a_d_1)
                roots = np.roots([4*a_p_1*a_u, 3*(a_p_1*b_u+b_p_1*a_u), 2*(c_p_1*a_u+b_p_1*b_u), c_p_1*b_u])
                roots = roots[np.isreal(roots)].real
                roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
                for q in roots:
                    U = (a_p_1*q**2 + b_p_1*q + c_p_1)*(a_u*q**2 + b_u*q)
                    if U > max_U + self._numeric_tol:
                        q_hat = q
                        max_U = U

        """ range 3: [vtx2, vtx3] """
        if vtx3 >= 0:  # range exists
            if Q <= vtx3:  # q exists
                lb = max(0., vtx2 - Q)
                ub = vtx3 - Q
                if vtx2 >= 0:
                    a_p = - a_d_2 / 2
                    b_p = - (a_d_2 * Q + b_d_2)
                    pr_1 = a_d_1 / 2 * vtx2**2 + b_d_1 * vtx2
                    c_p = 1 - pr_1 - a_d_2 / 2 * Q ** 2 - b_d_2 * Q + a_d_2 / 2 * vtx2 ** 2 + b_d_2 * vtx2
                else:
                    a_p = - a_d_2 / 2
                    b_p = - (a_d_2 * Q + b_d_2)
                    c_p = 1 - a_d_2 / 2 * Q ** 2 - b_d_2 * Q
                roots = np.roots([4 * a_p * a_u, 3 * (a_p * b_u + b_p * a_u), 2 * (c_p * a_u + b_p * b_u), c_p * b_u])
                roots = roots[np.isreal(roots)].real
                roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
                for q in roots:
                    U = (a_p * q ** 2 + b_p * q + c_p) * (a_u * q ** 2 + b_u * q)
                    if U > max_U + self._numeric_tol:
                        q_hat = q
                        max_U = U

        return max_U, q_hat

    def optimal_quantity(self, contract: str, ex: str, d: Direction, delta: float, beta: float, T: float, tau: float, I: float, U_lb_multiplier: float) -> Tuple[float, float]:
        q_grp_size = self._get_q_grp_size_ccy1(contract=contract)
        int_I = int(round(I / q_grp_size))
        U, q = self._optimal_quantity(contract=contract, ex=ex, d=d, delta=delta, beta=beta, T=T, tau=tau, I=int_I, U_lb_multiplier=U_lb_multiplier)
        return U, q * q_grp_size

    def _optimal_quantity2(self, contract: str, ex: str, d: Direction, delta: float, I: int, ex_I: int, q_beta: float, I_beta: float) -> Tuple[float, float, QuantitySolverMessage]:

        msg = QuantitySolverMessage()

        if math.isnan(delta):
            return 0, 0, msg

        xex = "xex"
        SEx_tilde_p = self.get_depth_metric(contract=contract, ex=ex, metric="tilde_p")
        SEx_s_l = self.get_depth_metric(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metric(contract=contract, ex=ex, metric="s_s")
        SEx_a_l = self.get_depth_metric(contract=contract, ex=ex, metric="a_l")
        SEx_b_l = self.get_depth_metric(contract=contract, ex=ex, metric="b_l")
        SEx_a_s = self.get_depth_metric(contract=contract, ex=ex, metric="a_s")
        SEx_b_s = self.get_depth_metric(contract=contract, ex=ex, metric="b_s")
        tilde_p = self.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        XEx_s_l = self.get_depth_metric(contract=contract, ex=xex, metric="s_l")
        XEx_s_s = self.get_depth_metric(contract=contract, ex=xex, metric="s_s")
        XEx_a_l = self.get_depth_metric(contract=contract, ex=xex, metric="a_l")
        XEx_b_l = self.get_depth_metric(contract=contract, ex=xex, metric="b_l")
        XEx_a_s = self.get_depth_metric(contract=contract, ex=xex, metric="a_s")
        XEx_b_s = self.get_depth_metric(contract=contract, ex=xex, metric="b_s")
        if d == Direction.long:
            SEx_a_o = SEx_a_s
            SEx_b_o = SEx_b_s
            s_d = XEx_s_l
            a_d = XEx_a_l
            b_d = XEx_b_l
        else:
            SEx_a_o = SEx_a_l
            SEx_b_o = SEx_b_l
            s_d = XEx_s_s
            a_d = XEx_a_s
            b_d = XEx_b_s
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        q_tick_size_ccy1 = self._get_q_grp_size_ccy1(contract=contract)
        tau = self.get_tau(contract=contract, ex=self._xex)
        latency = self.get_pred_latency(contract=contract, ex=ex)

        max_U = -self._numeric_tol
        q_hat = 0
        P_tol = 1e-10

        c_maker = self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker)
        c_taker = self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker)

        if d == Direction.long:
            taker_bnd = (tilde_p - XEx_s_l) - (SEx_tilde_p + SEx_s_s)
        else:
            taker_bnd = (SEx_tilde_p - SEx_s_l) - (tilde_p + XEx_s_s)

        """
        unit_pnl = (d * (p_hat - tilde_p) + s_d + delta - s_o_hat) - c_maker * tilde_p
        pnl = 1 * q_tick_size_ccy1 * unit_pnl
        dr = sigma * tilde_p * math.sqrt(tau + latency) * q_tick_size_ccy1 * 2 * (d * I_beta * I * q_beta + q_beta ** 2 * q)  + q_beta ** 2
        U = pnl - dr
        
        pnl = beta * dr
        a = [q_tick_size_ccy1 * (d * (p_hat - tilde_p) + s_d + delta - s_o_hat) - c_maker * tilde_p] / 
                [beta * sigma * tilde_p * math.sqrt(tau + latency) * q_tick_size_ccy1]
        a = 2 * (d * I * I_beta * q_beta + q * q_beta ** 2) + q_beta ** 2
        q = (a - q_beta ** 2 - 2 * d * I * I_beta * q_beta) / (2 * q_beta ** 2)
        """

        if delta < taker_bnd:
            # taker
            unit_pnl = (d * (p_hat - tilde_p) + s_d + delta - s_o_hat) - c_taker * tilde_p
            unit_dr = sigma * tilde_p * math.sqrt(tau + latency)
            a = unit_pnl / unit_dr
            q = (a - q_beta ** 2 - 2 * d * (I + ex_I) * I_beta * q_beta) / (2 * q_beta ** 2)
            assert q > 0
            q = max(q, 0)
            U = (unit_pnl - unit_dr * 2 * (d * I_beta * (I + ex_I) * q_beta + q_beta ** 2 * q) + q_beta ** 2) * q_tick_size_ccy1
            if U > max_U:
                max_U = U
                q_hat = q

        else:
            # maker
            unit_pnl = (d * (p_hat - tilde_p) + s_d + delta - s_o_hat) - c_maker * tilde_p
            unit_dr = sigma * tilde_p * math.sqrt(tau + latency)
            a = unit_pnl / unit_dr
            U_q = (a - q_beta ** 2 - 2 * d * (I + ex_I) * I_beta * q_beta) / (2 * q_beta ** 2)
            assert U_q >= 0
            U_q = max(U_q, 0)
            lambda_d = self._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=d)
            P_q = math.sqrt(lambda_d) * fast_norm_cdf_inv(1 - P_tol) - (a_d * delta + b_d - lambda_d - self._epsilon)
            # q = max(min(U_q, P_q), 0)
            q = max(U_q, 0)
            _q = a_d * delta + b_d + q
            _z = (_q - lambda_d - self._epsilon) / math.sqrt(lambda_d)
            Id = 2 * (d * I_beta * (I + ex_I) * q_beta + q_beta ** 2 * q) + q_beta ** 2
            Id_sign = 1 if Id >= 0 else -1
            U = (unit_pnl - unit_dr * abs(Id) * Id_sign) * q_tick_size_ccy1
            Pr = 1 - fast_norm_cdf(_z)
            EU = U * Pr
            if EU > max_U:
                max_U = EU
                q_hat = q
            else:
                msg.EU_filtered = True

        max_U = 0 if q_hat == 0 else max_U
        return max_U, q_hat, msg

    def optimal_quantity2(self, contract: str, ex: str, d: Direction, delta: float, I: float, ex_I: float, q_beta: float, I_beta: float) -> Tuple[float, float, QuantitySolverMessage]:
        q_grp_size = self._get_q_grp_size_ccy1(contract=contract)
        int_I = int(round(I / q_grp_size))
        int_ex_I = int(round(ex_I / q_grp_size))
        U, q, msg = self._optimal_quantity2(contract=contract, ex=ex, d=d, delta=delta, I=int_I, ex_I=int_ex_I, q_beta=q_beta, I_beta=I_beta)
        q_cap = 1e10
        q *= q_grp_size
        return U, min(q_cap, q), msg
