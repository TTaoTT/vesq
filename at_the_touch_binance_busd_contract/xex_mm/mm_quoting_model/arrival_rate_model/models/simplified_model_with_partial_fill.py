import math
from typing import Dict, List, Tuple

import numpy as np
from xex_mm.utils.base import Level, Depth, ReferencePriceCalculator
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.utils.methods import tick_size_gcd


class SingleArrivalRateXExMmModelWithPartialFill:
    """
    Simplified version of SingleArrivalRateXExMmModel, no grid search, approximated optimal price and quantity computed
    in O(1) time complexity.
    """

    def __init__(self,
                 ref_prc_mdl_map: Dict,
                 half_sprd_mdl_map: Dict,
                 sigma_mdl_map: Dict,
                 arrival_rate_mdl_map: Dict,
                 arrival_rate_mkt_depth_decay_coef_mdl_map: Dict,
                 arrival_rate_qty_decay_coef_mdl_map: Dict,
                 fee_rate_map: Dict,
                 qty_tick_size_map: Dict,
                 prc_tick_size_map: Dict,
                 ):

        self._ref_prc_model_map = ref_prc_mdl_map  # contract, ex
        self._half_sprd_model_map = half_sprd_mdl_map  # contract, ex, side
        self._sigma_model_map = sigma_mdl_map  # contract, ex
        self._arrival_rate_model_map = arrival_rate_mdl_map  # contract, ex, side
        self._arrival_rate_mkt_depth_decay_coef_mdl_map = arrival_rate_mkt_depth_decay_coef_mdl_map  # contract, ex, side
        self._arrival_rate_qty_decay_coef_mdl_map = arrival_rate_qty_decay_coef_mdl_map  # contract, ex, side
        self._fee_rate_map = fee_rate_map  # contract, ex, maker_taker
        self._qty_tick_size_map = qty_tick_size_map  # contract, ex
        self._prc_tick_size_map = prc_tick_size_map  # contract, ex

        self._pred_ref_prc_map = {}  # contract, ex
        self._pred_half_sprd_map = {}  # contract, ex, side
        self._pred_sigma_map = {}  # contract, ex
        self._pred_arrival_rate_map = {}  # contract, ex, side
        self._pred_arrival_rate_mkt_depth_decay_coef_map = {}  # contract, ex, side
        self._pred_arrival_rate_qty_decay_coef_map = {}  # contract, ex, side
        self._qty_tick_size_gcd_map = {}  # contract
        self._update_qty_tick_size_gcd_map()

        self._depth_metrics = {}  # contract, ex, metric

        self._k_1 = 2.5
        self._k_2 = 2.5
        self._g = 3
        self._epsilon = -0.5
        self._numeric_tol = 1e-10
        self._min_sigma = 1e-6


    ############################################ Params Control #################################################

    def _update_ex_contract_d_preds(self, contract: str, ex: str, d: Direction, xs: np.ndarray, mdl_map: Dict, pred_map: Dict):
        mdl = mdl_map[contract][ex][d]
        if contract not in pred_map:
            pred_map[contract] = {}
        if ex not in pred_map[contract]:
            pred_map[contract][ex] = {}
        pred_map[contract][ex][d] = mdl.predict(xs=xs)

    def _update_ex_contract_preds(self, contract: str, ex: str, xs: np.ndarray, mdl_map: Dict, pred_map: Dict):
        mdl = mdl_map[contract][ex]
        if contract not in pred_map:
            pred_map[contract] = {}
        pred_map[contract][ex] = mdl.predict(xs=xs)

    def update_pred_arrival_rate(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_model_map,
                                         pred_map=self._pred_arrival_rate_map)

    def get_pred_arrival_rate(self, contract: str, ex: str, d: Direction) -> float:
        return self._pred_arrival_rate_map[contract][ex][d]

    def update_pred_ref_prc(self, contract: str, ex: str, xs: np.ndarray):
        self._update_ex_contract_preds(contract=contract, ex=ex, xs=xs,
                                       mdl_map=self._ref_prc_model_map,
                                       pred_map=self._pred_ref_prc_map)

    def get_pred_ref_prc(self, contract: str, ex: str) -> float:
        return self._pred_ref_prc_map[contract][ex]

    def update_pred_half_spread(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._half_sprd_model_map,
                                         pred_map=self._pred_half_sprd_map)

    def get_pred_half_sprd(self, contract: str, ex: str, d: Direction):
        return self._pred_half_sprd_map[contract][ex][d]

    def update_pred_sigma(self, contract: str, ex: str, xs: np.ndarray):
        self._update_ex_contract_preds(contract=contract, ex=ex, xs=xs,
                                       mdl_map=self._sigma_model_map,
                                       pred_map=self._pred_sigma_map)

    def get_pred_sigma(self, contract: str, ex: str):
        sigma = self._pred_sigma_map[contract][ex]
        return max(self._min_sigma, sigma)

    def update_pred_arrival_rate_mkt_depth_decay_coef(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_mkt_depth_decay_coef_mdl_map,
                                         pred_map=self._pred_arrival_rate_mkt_depth_decay_coef_map)

    def get_pred_arrival_rate_mkt_depth_decay_coef(self, contract: str, ex: str, d: Direction):
        return self._pred_arrival_rate_mkt_depth_decay_coef_map[contract][ex][d]

    def update_pred_arrival_rate_qty_decay_coef(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_qty_decay_coef_mdl_map,
                                         pred_map=self._pred_arrival_rate_qty_decay_coef_map)

    def get_pred_arrival_rate_qty_decay_coef(self, contract: str, ex: str, d: Direction):
        return self._pred_arrival_rate_qty_decay_coef_map[contract][ex][d]

    def _get_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker):
        return self._fee_rate_map[contract][ex][maker_taker]

    def _update_qty_tick_size_gcd_map(self):
        for contract, xex_tick_size_map in self._qty_tick_size_map.items():
            self._qty_tick_size_gcd_map[contract] = tick_size_gcd(tick_sizes=list(xex_tick_size_map.values()))

    def _get_qty_tick_size(self, contract: str, ex: str):
        return self._qty_tick_size_map[contract][ex]

    def _get_qty_tick_size_gcd(self, contract: str) -> float:
        return self._qty_tick_size_gcd_map[contract]

    def _get_prc_tick_size(self, contract: str, ex: str) -> float:
        return self._prc_tick_size_map[contract][ex]

    def _get_lambda_for_d(self, contract: str, ex: str, d: Direction) -> float:
        d = Direction.long if d == Direction.short else Direction.short
        return self.get_pred_arrival_rate(contract=contract, ex=ex, d=d) / self._get_qty_tick_size_gcd(contract=contract)

    def _calib_Q_func(self, side: List[Level]) -> Tuple[float, float]:
        if len(side) == 0:
            return 0, 0
        if len(side) == 1:
            return 0, side[0].qty
        Q = 0
        Qs = []
        ps = []
        for lvl in side:
            Q += lvl.qty
            Qs.append(Q)
            ps.append(lvl.prc)
        X = np.abs(np.array(ps) - ps[0])
        y = np.array(Qs) - Qs[0]
        beta = 1 / X.dot(X) * X.dot(y)
        return beta, Qs[0]

    def update_depth_metrics(self, contract: str, ex: str, depth: Depth, amt_bnd: float = 100):
        if contract not in self._depth_metrics:
            self._depth_metrics[contract] = {}
        if ex not in self._depth_metrics[contract]:
            self._depth_metrics[contract][ex] = {}
        guarded_depth: Depth = depth.get_at_touch_guarded_view(amt_bnd=amt_bnd)
        tilde_p = self._depth_to_ref_prc(depth=guarded_depth, amt_bnd=amt_bnd)
        best_ask = guarded_depth.asks[0].prc if len(guarded_depth.asks) > 0 else math.inf
        best_bid = guarded_depth.bids[0].prc if len(guarded_depth.bids) > 0 else math.inf
        s_l = tilde_p - best_bid
        s_s = best_ask - tilde_p
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        int_depth = guarded_depth.get_qty_grp_view(q_grp_size=q_tick_size)
        a_l, b_l = self._calib_Q_func(side=int_depth.bids)
        a_s, b_s = self._calib_Q_func(side=int_depth.asks)
        _map = dict(
            tilde_p = tilde_p,
            s_l = s_l,
            s_s = s_s,
            a_l = a_l,
            b_l = b_l,
            a_s = a_s,
            b_s = b_s,
        )
        self._depth_metrics[contract][ex] = _map

    def get_depth_metrics(self, contract: str, ex: str, metric: str):
        return self._depth_metrics[contract][ex][metric]

    ############################################ Computation Methods #################################################

    def _depth_to_ref_prc(self, depth: Depth, amt_bnd: float):
        return ReferencePriceCalculator().calc_from_depth(depth=depth, amt_bnd=amt_bnd)

    def _get_unit_c_maker(self, contract: str, ex: str, p: float, q_tick_size: float):
        return p * self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker) * q_tick_size

    def _get_unit_c_taker(self, contract: str, ex: str, p: float, q_tick_size: float):
        return p * self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker) * q_tick_size

    def _c_pnl(self, p_hat: float, s_o_hat: float, tilde_p: float, s_d: float, d: Direction, q_tick_size: float):
        I_d = 1 if d == Direction.long else -1
        return (I_d * (p_hat - tilde_p) + s_d - s_o_hat) * q_tick_size

    def _pnl(self, p_hat: float, s_o_hat: float, tilde_p: float, s_d: float, delta_d: float, d: Direction, q_tick_size: float):
        return delta_d * q_tick_size + self._c_pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, d=d, q_tick_size=q_tick_size)

    def _U_taker(self, pnl: float, c_taker: float, beta: float, dR1: float):
        return pnl - c_taker - beta * dR1

    def _Q_d(self, delta_d: float, a_d: float, b_d: float):
        return a_d * delta_d + b_d

    def _a_d_1(self, k_1: float, lambda_d: float):
        return 1 / k_1**2 / lambda_d

    def _b_d_1(self, k_1: float, k_2: float, lambda_d: float, epsilon: float, g: float):
        val = (k_1 * math.sqrt(lambda_d) - lambda_d - epsilon) / (k_1 ** 2 * lambda_d)
        if lambda_d + epsilon < k_1 * math.sqrt(lambda_d):
            val += self._c_d(k_1=k_1, k_2=k_2, lambda_d=lambda_d, epsilon=epsilon, g=g)
        return val

    def _a_d_2(self, k_2: float, lambda_d: float):
        return - 1 / k_2**2 / lambda_d

    def _b_d_2(self, k_1: float, k_2: float, lambda_d: float, epsilon: float, g: float):
        val = (k_2 * math.sqrt(lambda_d) + lambda_d + epsilon) / (k_2 ** 2 * lambda_d)
        if lambda_d + epsilon < k_1 * math.sqrt(lambda_d):
            val += self._c_d(k_1=k_1, k_2=k_2, lambda_d=lambda_d, epsilon=epsilon, g=g)
        return val

    def _c_d(self, k_1: float, k_2: float, lambda_d: float, epsilon: float, g: float):
        return (k_1 * math.sqrt(lambda_d) - lambda_d - epsilon)**2 / (2 * g * k_1**2 * lambda_d * (k_2 * math.sqrt(lambda_d) + lambda_d + epsilon))

    def _dR_q(self, tilde_p: float, sigma: float, T: float, tau: float, I: int, q: int, d: Direction, q_tick_size: float):
        I_d = 1 if d == Direction.long else -1
        return 0.5 * (tilde_p * sigma) ** 2 * (T - tau) * (2 * I * I_d * q + q ** 2) * q_tick_size ** 2

    def _pr(self, delta: float, a_p: float, b_p: float, c_p: float):
        return a_p * delta ** 2 + b_p * delta + c_p

    def _U_maker(self, pr: float, pnl: float, beta: float, dR1_d: float, c_maker: float):
        return pr * (pnl - c_maker - beta * dR1_d)

    def _range_bounding(self, x: float, lb: float, ub: float, l_close: bool, r_close: bool):
        if x < lb:
            if l_close:
                return lb
            return lb + self._numeric_tol
        if x > ub:
            if r_close:
                return ub
            return ub - self._numeric_tol
        return x

    def _optimal_price_level(self, contract: str, ex: str, q: int, d: Direction, beta: float, T: float, tau: float, I: int) -> Tuple[float, float]:
        xex = "xex"
        SEx_tilde_p = self.get_depth_metrics(contract=contract, ex=ex, metric="tilde_p")
        SEx_s_l = self.get_depth_metrics(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metrics(contract=contract, ex=ex, metric="s_s")
        SEx_a_l = self.get_depth_metrics(contract=contract, ex=ex, metric="a_l")
        SEx_a_s = self.get_depth_metrics(contract=contract, ex=ex, metric="a_s")
        SEx_b_l = self.get_depth_metrics(contract=contract, ex=ex, metric="b_l")
        SEx_b_s = self.get_depth_metrics(contract=contract, ex=ex, metric="b_s")
        SEx_b_o = SEx_b_s if d == Direction.long else SEx_b_l
        tilde_p = self.get_depth_metrics(contract=contract, ex=xex, metric="tilde_p")
        s_l = self.get_depth_metrics(contract=contract, ex=xex, metric="s_l")
        s_s = self.get_depth_metrics(contract=contract, ex=xex, metric="s_s")
        s_d = s_l if d == Direction.long else s_s
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=tilde_p, q_tick_size=1)
        I_d = 1 if d == Direction.long else -1

        # do nothing
        max_U = 0
        delta_hat = np.nan

        # optimal for delta_l in (-inf, -s_l-s_s]
        if d == Direction.long:
            taker_bnd = (tilde_p - s_l) - (SEx_tilde_p + SEx_s_s)
            A_Q_o = -SEx_a_s
            B_Q_o = SEx_a_s * taker_bnd + SEx_b_s
        else:
            taker_bnd = (SEx_tilde_p - SEx_s_l) - (tilde_p + s_s)
            A_Q_o = -SEx_a_l
            B_Q_o = SEx_a_l * taker_bnd + SEx_b_l

        lambda_d = self._get_lambda_for_d(contract=contract, ex=xex, d=d) * tau
        epsilon = self._epsilon
        a_d_1 = self._a_d_1(k_1=self._k_1, lambda_d=lambda_d)
        b_d_1 = self._b_d_1(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_2 = self._a_d_2(k_2=self._k_2, lambda_d=lambda_d)
        b_d_2 = self._b_d_2(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_name = "a_l" if d == Direction.long else "a_s"
        b_d_name = "b_l" if d == Direction.long else "b_s"
        a_d = self.get_depth_metrics(contract=contract, ex=xex, metric=a_d_name)
        b_d = self.get_depth_metrics(contract=contract, ex=xex, metric=b_d_name)
        c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=tilde_p, q_tick_size=1)
        q_v1 = - b_d_1 / a_d_1
        q_v2 = lambda_d + epsilon
        q_v3 = - b_d_2 / a_d_2
        C_R = -beta * 0.5 * (q_tick_size * tilde_p * sigma) ** 2 * (T - tau)

        """ 1. Pure Taker """
        pure_taker_bnd = (q - B_Q_o) / A_Q_o if q >= SEx_b_o else taker_bnd
        if pure_taker_bnd >= taker_bnd + self._numeric_tol:
            raise RuntimeError()

        delta = pure_taker_bnd
        U = q * q_tick_size * (self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=1) - c_taker) \
            - beta * self._dR_q(tilde_p=tilde_p, sigma=sigma, T=T, tau=tau, I=I, q=q, d=d, q_tick_size=q_tick_size)
        if U > max_U:
            max_U = U
            delta_hat = delta

        """ 2. Partial Taker """
        A_U_taker = q_tick_size * A_Q_o + C_R * A_Q_o ** 2
        B_U_taker = q_tick_size * A_Q_o * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + q_tick_size * B_Q_o + 2 * C_R * I * I_d * A_Q_o + 2 * C_R * A_Q_o * B_Q_o
        C_U_taker = q_tick_size * B_Q_o * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + 2 * C_R * I * I_d * B_Q_o + C_R * B_Q_o ** 2

        A_U_full = -q_tick_size * A_Q_o + C_R * A_Q_o ** 2
        B_U_full = q_tick_size * (-A_Q_o*(I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_maker) + (q - B_Q_o)) - 2 * C_R * I * I_d * A_Q_o - 2 * C_R * A_Q_o * (q - B_Q_o)
        C_U_full = q_tick_size * (q - B_Q_o) * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_maker) + 2 * C_R * I * I_d * (q - B_Q_o) + C_R * (q - B_Q_o) ** 2

        """ 2.1 No Queue """
        A_ub_1 = - A_Q_o
        B_ub_1 = q - B_Q_o
        A_ub_2 = A_ub_1 ** 2
        B_ub_2 = 2 * A_ub_1 * B_ub_1
        C_ub_2 = B_ub_1 ** 2
        A_ub_3 = A_ub_1 ** 3
        B_ub_3 = 3 * A_ub_1 ** 2 * B_ub_1
        C_ub_3 = 3 * A_ub_1 * B_ub_1 ** 2
        D_ub_3 = B_ub_1 ** 3
        A_ub_4 = A_ub_1 ** 4
        B_ub_4 = 4 * A_ub_1 ** 3 * B_ub_1
        C_ub_4 = 6 * A_ub_1 ** 2 * B_ub_1 ** 2
        D_ub_4 = 4 * A_ub_1 * B_ub_1 ** 3
        E_ub_4 = B_ub_1 ** 4

        delta_v1 = (q_v1 - B_ub_1) / A_ub_1
        delta_v2 = (q_v2 - B_ub_1) / A_ub_1
        delta_v3 = (q_v3 - B_ub_1) / A_ub_1

        A_U_partial = C_R
        A_B_U_partial = q_tick_size
        B_B_U_partial = q_tick_size * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + 2 * C_R * I * I_d

        A_E_partial_int_1_C = lambda C: a_d_1 * A_B_U_partial / 3 * C ** 3 + b_d_1 * A_B_U_partial / 2 * C ** 2
        B_E_partial_int_1_C = lambda C: a_d_1 * A_U_partial / 4 * C ** 4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C ** 3 + b_d_1 * B_B_U_partial / 2 * C ** 2

        A_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * A_ub_4 + a_d_1 * A_B_U_partial / 3 * A_ub_3
        B_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * B_ub_4 + a_d_1 * A_B_U_partial / 3 * B_ub_3 + b_d_1 * A_U_partial / 3 * A_ub_3 + a_d_1 * B_B_U_partial / 3 * A_ub_3 + b_d_1 * A_B_U_partial / 2 * A_ub_2
        C_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * C_ub_4 + a_d_1 * A_B_U_partial / 3 * C_ub_3 + b_d_1 * A_U_partial / 3 * B_ub_3 + a_d_1 * B_B_U_partial / 3 * B_ub_3 + b_d_1 * A_B_U_partial / 2 * B_ub_2 + b_d_1 * B_B_U_partial / 2 * A_ub_2
        D_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * D_ub_4 + a_d_1 * A_B_U_partial / 3 * D_ub_3 + b_d_1 * A_U_partial / 3 * C_ub_3 + a_d_1 * B_B_U_partial / 3 * C_ub_3 + b_d_1 * A_B_U_partial / 2 * C_ub_2 + b_d_1 * B_B_U_partial / 2 * B_ub_2
        E_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * E_ub_4 + b_d_1 * A_U_partial / 3 * D_ub_3 + a_d_1 * B_B_U_partial / 3 * D_ub_3 + b_d_1 * B_B_U_partial / 2 * C_ub_2

        A_E_partial_int_2_C = lambda C: a_d_2 * A_B_U_partial / 3 * C ** 3 + b_d_2 * A_B_U_partial / 2 * C ** 2
        B_E_partial_int_2_C = lambda C: a_d_2 * A_U_partial / 4 * C ** 4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C ** 3 + b_d_2 * B_B_U_partial / 2 * C ** 2

        A_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * A_ub_4 + a_d_2 * A_B_U_partial / 3 * A_ub_3
        B_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * B_ub_4 + a_d_2 * A_B_U_partial / 3 * B_ub_3 + b_d_2 * A_U_partial / 3 * A_ub_3 + a_d_2 * B_B_U_partial / 3 * A_ub_3 + b_d_2 * A_B_U_partial / 2 * A_ub_2
        C_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * C_ub_4 + a_d_2 * A_B_U_partial / 3 * C_ub_3 + b_d_2 * A_U_partial / 3 * B_ub_3 + a_d_2 * B_B_U_partial / 3 * B_ub_3 + b_d_2 * A_B_U_partial / 2 * B_ub_2 + b_d_2 * B_B_U_partial / 2 * A_ub_2
        D_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * D_ub_4 + a_d_2 * A_B_U_partial / 3 * D_ub_3 + b_d_2 * A_U_partial / 3 * C_ub_3 + a_d_2 * B_B_U_partial / 3 * C_ub_3 + b_d_2 * A_B_U_partial / 2 * C_ub_2 + b_d_2 * B_B_U_partial / 2 * B_ub_2
        E_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * E_ub_4 + b_d_2 * A_U_partial / 3 * D_ub_3 + a_d_2 * B_B_U_partial / 3 * D_ub_3 + b_d_2 * B_B_U_partial / 2 * C_ub_2

        """ 2.1.1 lb in [-inf, q_v1], ub in [-inf, q_v1] """
        lb = pure_taker_bnd
        ub = min(0, taker_bnd, (q_v1 - B_ub_1) / A_ub_1)
        if (0 <= q_v1) and (lb <= ub):
            delta = - (B_U_taker + B_U_full) / 2 / (A_U_taker + A_U_full)
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = (A_U_taker + A_U_full) * delta ** 2 + (B_U_taker + B_U_full) * delta + (C_U_taker + C_U_full)
            if U > max_U:
                max_U = U
                delta_hat = delta

        """ 2.1.2 lb in [-inf, q_v1], ub in [q_v1, q_v2] """

        A_E_partial_int_1_C_q_v1 = A_E_partial_int_1_C(q_v1)
        B_E_partial_int_1_C_q_v1 = B_E_partial_int_1_C(q_v1)

        lb = max(pure_taker_bnd, delta_v1)
        ub = min(0, taker_bnd, delta_v2)

        if (0 <= q_v1) and (lb <= ub):

            A_E_partial = A_E_partial_int_1_ub
            B_E_partial = B_E_partial_int_1_ub
            C_E_partial = C_E_partial_int_1_ub
            D_E_partial = D_E_partial_int_1_ub - A_E_partial_int_1_C_q_v1
            E_E_partial = E_E_partial_int_1_ub - B_E_partial_int_1_C_q_v1

            A_P_full = - a_d_1 / 2 * A_ub_2
            B_P_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
            C_P_full = - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker), D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.1.3 lb in [-inf, q_v1], ub in [q_v2, q_v3] """
        A_E_partial_int_1_C_q_v2 = A_E_partial_int_1_C(q_v2)
        B_E_partial_int_1_C_q_v2 = B_E_partial_int_1_C(q_v2)
        A_E_partial_int_2_C_q_v2 = A_E_partial_int_2_C(q_v2)
        B_E_partial_int_2_C_q_v2 = B_E_partial_int_2_C(q_v2)

        lb = max(pure_taker_bnd, delta_v2)
        ub = min(0, taker_bnd, delta_v3)

        if (0 <= q_v1) and (lb <= ub):

            A_E_partial = A_E_partial_int_2_ub
            B_E_partial = B_E_partial_int_2_ub
            C_E_partial = C_E_partial_int_2_ub
            D_E_partial = D_E_partial_int_2_ub + A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1 - A_E_partial_int_2_C_q_v2
            E_E_partial = E_E_partial_int_2_ub + B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1 - B_E_partial_int_2_C_q_v2

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                            C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                                D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.1.4 lb in [-inf, q_v1], ub in [q_v3, inf] """

        A_E_partial_int_2_C_q_v3 = A_E_partial_int_2_C(q_v3)
        B_E_partial_int_2_C_q_v3 = B_E_partial_int_2_C(q_v3)

        lb = max(pure_taker_bnd, delta_v3)
        ub = min(0, taker_bnd)

        if (0 <= q_v1) and (lb <= ub):

            A_E_partial = A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
            B_E_partial = B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2

            delta = - (B_U_taker - A_E_partial) / 2 / A_U_taker

            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = A_U_taker * delta ** 2 + (B_U_taker + A_E_partial) * delta + (C_U_taker + B_E_partial)
            if U > max_U:
                max_U = U
                delta_hat = delta

        """ 2.1.5 lb in [q_v1, q_v2], ub in [q_v1, q_v2] """
        lb = max(pure_taker_bnd, delta_v1)
        ub = min(0, taker_bnd, delta_v2)

        if (0 >= q_v1) and (0 <= q_v2) and (lb <= ub):

            A_E_partial = A_E_partial_int_1_ub
            B_E_partial = B_E_partial_int_1_ub
            C_E_partial = C_E_partial_int_1_ub
            D_E_partial = D_E_partial_int_1_ub
            E_E_partial = E_E_partial_int_1_ub

            A_P_full = - a_d_1 / 2 * A_ub_2
            B_P_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
            C_P_full = - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.1.6 lb in [q_v1, q_v2], ub in [q_v2, q_v3] """

        lb = max(pure_taker_bnd, delta_v2)
        ub = min(0, taker_bnd, delta_v3)

        if (0 >= q_v1) and (0 <= q_v2) and (lb <= ub):

            A_E_partial = A_E_partial_int_2_ub
            B_E_partial = B_E_partial_int_2_ub
            C_E_partial = C_E_partial_int_2_ub
            D_E_partial = D_E_partial_int_2_ub + A_E_partial_int_1_C_q_v2 - A_E_partial_int_2_C_q_v2
            E_E_partial = E_E_partial_int_2_ub + B_E_partial_int_1_C_q_v2 - B_E_partial_int_2_C_q_v2

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.1.7 lb in [q_v1, q_v2], ub in [q_v3, inf] """

        lb = max(pure_taker_bnd, delta_v3)
        ub = min(0, taker_bnd)

        if (0 >= q_v1) and (0 <= q_v2) and (lb <= ub):
            A_E_partial = A_E_partial_int_1_C_q_v2 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
            B_E_partial = B_E_partial_int_1_C_q_v2 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2

            delta = - (B_U_taker + A_E_partial) / 2 / A_U_taker
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = A_U_taker * delta ** 2 + (B_U_taker + A_E_partial) * delta + (C_U_taker + B_E_partial)
            if U > max_U:
                delta_hat = delta
                max_U = U

        """ 2.1.8 lb in [q_v2, q_v3], ub in [q_v2, q_v3] """
        lb = max(pure_taker_bnd, delta_v2)
        ub = min(0, taker_bnd, delta_v3)

        if (0 >= q_v2) and (0 <= q_v3) and (lb <= ub):

            A_E_partial = A_E_partial_int_2_ub
            B_E_partial = B_E_partial_int_2_ub
            C_E_partial = C_E_partial_int_2_ub
            D_E_partial = D_E_partial_int_2_ub
            E_E_partial = E_E_partial_int_2_ub

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.1.9 lb in [q_v2, q_v3], ub in [q_v3, inf] """
        lb = max(pure_taker_bnd, delta_v3)
        ub = min(0, taker_bnd)

        if (0 >= q_v2) and (0 <= q_v3) and (lb <= ub):
            A_E_partial = A_E_partial_int_2_C_q_v3
            B_E_partial = B_E_partial_int_2_C_q_v3

            delta = - (B_U_taker + A_E_partial) / 2 / A_U_taker
            U = A_U_taker * delta ** 2 + (B_U_taker + A_E_partial) * delta + (C_U_taker + B_E_partial)
            if U > max_U:
                delta_hat = delta
                max_U = U

        """ 2.1.10 lb in [q_v3, inf], ub in [q_v3, inf] """
        lb = max(pure_taker_bnd, delta_v3)
        ub = min(0, taker_bnd)

        if (0 >= q_v3) and (lb <= ub):
            delta = - B_U_taker / 2 / A_U_taker
            U = A_U_taker * delta ** 2 + B_U_taker * delta + C_U_taker
            if U > max_U:
                delta_hat = delta
                max_U = U

        """ 2.2 Queued """
        A_lb_1 = a_d
        B_lb_1 = b_d
        A_lb_2 = A_lb_1 ** 2
        B_lb_2 = 2 * A_lb_1 * B_lb_1
        C_lb_2 = B_lb_1 ** 2
        A_lb_3 = A_lb_1 ** 3
        B_lb_3 = 3 * A_lb_1 ** 2 * B_lb_1
        C_lb_3 = 3 * A_lb_1 * B_lb_1 ** 2
        D_lb_3 = B_lb_1 ** 3
        A_lb_4 = A_lb_1 ** 4
        B_lb_4 = 4 * A_lb_1 ** 3 * B_lb_1
        C_lb_4 = 6 * A_lb_1 ** 2 * B_lb_1 ** 2
        D_lb_4 = 4 * A_lb_1 * B_lb_1 ** 3
        E_lb_4 = B_lb_1 ** 4

        A_ub_1 = a_d - A_Q_o
        B_ub_1 = b_d + q - B_Q_o
        A_ub_2 = A_ub_1 ** 2
        B_ub_2 = 2 * A_ub_1 * B_ub_1
        C_ub_2 = B_ub_1 ** 2
        A_ub_3 = A_ub_1 ** 3
        B_ub_3 = 3 * A_ub_1 ** 2 * B_ub_1
        C_ub_3 = 3 * A_ub_1 * B_ub_1 ** 2
        D_ub_3 = B_ub_1 ** 3
        A_ub_4 = A_ub_1 ** 4
        B_ub_4 = 4 * A_ub_1 ** 3 * B_ub_1
        C_ub_4 = 6 * A_ub_1 ** 2 * B_ub_1 ** 2
        D_ub_4 = 4 * A_ub_1 * B_ub_1 ** 3
        E_ub_4 = B_ub_1 ** 4

        delta_v1_lb = (q_v1 - B_lb_1) / A_lb_1
        delta_v2_lb = (q_v2 - B_lb_1) / A_lb_1
        delta_v3_lb = (q_v3 - B_lb_1) / A_lb_1

        delta_v1_ub = (q_v1 - B_ub_1) / A_ub_1
        delta_v2_ub = (q_v2 - B_ub_1) / A_ub_1
        delta_v3_ub = (q_v3 - B_ub_1) / A_ub_1

        A_U_partial = C_R
        A_B_U_partial = q_tick_size - 2 * C_R * a_d
        B_B_U_partial = q_tick_size * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + 2 * C_R * I * I_d - 2 * C_R * b_d
        A_C_U_partial = C_R * a_d - q_tick_size * a_d
        B_C_U_partial = - q_tick_size * a_d * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) - q_tick_size * b_d - 2 * C_R * I * I_d * a_d + 2 * C_R * a_d * b_d
        C_C_U_partial = - q_tick_size * b_d * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) - 2 * C_R * I * I_d * b_d + C_R * b_d ** 2

        A_E_partial_int_1_C = lambda C: a_d_1 * A_C_U_partial / 2 * C ** 2 + b_d_1 * A_C_U_partial * C
        B_E_partial_int_1_C = lambda C: a_d_1 * A_B_U_partial / 3 * C ** 3 + (b_d_1 * A_B_U_partial + a_d_1 * B_C_U_partial) / 2 * C ** 2 + b_d_1 * B_C_U_partial * C
        C_E_partial_int_1_C = lambda C: a_d_1 * A_U_partial / 4 * C ** 4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C ** 3 + (b_d_1 * B_B_U_partial + a_d_1 * C_C_U_partial) / 2 * C ** 2 + b_d_1 * C_C_U_partial * C

        A_E_partial_int_2_C = lambda C: a_d_2 * A_C_U_partial / 2 * C ** 2 + b_d_2 * A_C_U_partial * C
        B_E_partial_int_2_C = lambda C: a_d_2 * A_B_U_partial / 3 * C ** 3 + (b_d_2 * A_B_U_partial + a_d_2 * B_C_U_partial) / 2 * C ** 2 + b_d_2 * B_C_U_partial * C
        C_E_partial_int_2_C = lambda C: a_d_2 * A_U_partial / 4 * C ** 4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C ** 3 + (b_d_2 * B_B_U_partial + a_d_2 * C_C_U_partial) / 2 * C ** 2 + b_d_2 * C_C_U_partial * C

        A_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * A_ub_4 + a_d_1 * A_B_U_partial / 3 * A_ub_3 + a_d_1 * A_C_U_partial / 2 * A_ub_2
        B_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * B_ub_4 + a_d_1 * A_B_U_partial / 3 * B_ub_3 + a_d_1 * A_C_U_partial / 2 * B_ub_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * A_ub_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * A_ub_2
        C_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * C_ub_4 + a_d_1 * A_B_U_partial / 3 * C_ub_3 + a_d_1 * A_C_U_partial / 2 * C_ub_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * B_ub_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * B_ub_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * A_ub_2
        D_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * D_ub_4 + a_d_1 * A_B_U_partial / 3 * D_ub_3 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C_ub_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * C_ub_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * B_ub_2
        E_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * E_ub_4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * D_ub_3 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * C_ub_2

        A_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * A_ub_4 + a_d_2 * A_B_U_partial / 3 * A_ub_3 + a_d_2 * A_C_U_partial / 2 * A_ub_2
        B_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * B_ub_4 + a_d_2 * A_B_U_partial / 3 * B_ub_3 + a_d_2 * A_C_U_partial / 2 * B_ub_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * A_ub_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * A_ub_2
        C_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * C_ub_4 + a_d_2 * A_B_U_partial / 3 * C_ub_3 + a_d_2 * A_C_U_partial / 2 * C_ub_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * B_ub_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * B_ub_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * A_ub_2
        D_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * D_ub_4 + a_d_2 * A_B_U_partial / 3 * D_ub_3 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C_ub_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * C_ub_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * B_ub_2
        E_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * E_ub_4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * D_ub_3 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * C_ub_2

        A_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * A_lb_4 + a_d_1 * A_B_U_partial / 3 * A_lb_3 + a_d_1 * A_C_U_partial / 2 * A_lb_2
        B_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * B_lb_4 + a_d_1 * A_B_U_partial / 3 * B_lb_3 + a_d_1 * A_C_U_partial / 2 * B_lb_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * A_lb_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * A_lb_2
        C_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * C_lb_4 + a_d_1 * A_B_U_partial / 3 * C_lb_3 + a_d_1 * A_C_U_partial / 2 * C_lb_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * B_lb_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * B_lb_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * A_lb_2
        D_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * D_lb_4 + a_d_1 * A_B_U_partial / 3 * D_lb_3 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C_lb_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * C_lb_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * B_lb_2
        E_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * E_lb_4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * D_lb_3 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * C_lb_2

        A_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * A_lb_4 + a_d_2 * A_B_U_partial / 3 * A_lb_3 + a_d_2 * A_C_U_partial / 2 * A_lb_2
        B_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * B_lb_4 + a_d_2 * A_B_U_partial / 3 * B_lb_3 + a_d_2 * A_C_U_partial / 2 * B_lb_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * A_lb_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * A_lb_2
        C_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * C_lb_4 + a_d_2 * A_B_U_partial / 3 * C_lb_3 + a_d_2 * A_C_U_partial / 2 * C_lb_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * B_lb_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * B_lb_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * A_lb_2
        D_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * D_lb_4 + a_d_2 * A_B_U_partial / 3 * D_lb_3 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C_lb_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * C_lb_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * B_lb_2
        E_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * E_lb_4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * D_lb_3 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * C_lb_2

        A_E_partial_int_1_C_q_v1 = A_E_partial_int_1_C(q_v1)
        B_E_partial_int_1_C_q_v1 = B_E_partial_int_1_C(q_v1)
        C_E_partial_int_1_C_q_v1 = C_E_partial_int_1_C(q_v1)
        A_E_partial_int_1_C_q_v2 = A_E_partial_int_1_C(q_v2)
        B_E_partial_int_1_C_q_v2 = B_E_partial_int_1_C(q_v2)
        C_E_partial_int_1_C_q_v2 = C_E_partial_int_1_C(q_v2)

        A_E_partial_int_2_C_q_v2 = A_E_partial_int_2_C(q_v2)
        B_E_partial_int_2_C_q_v2 = B_E_partial_int_2_C(q_v2)
        C_E_partial_int_2_C_q_v2 = C_E_partial_int_2_C(q_v2)
        A_E_partial_int_2_C_q_v3 = A_E_partial_int_2_C(q_v3)
        B_E_partial_int_2_C_q_v3 = B_E_partial_int_2_C(q_v3)
        C_E_partial_int_2_C_q_v3 = C_E_partial_int_2_C(q_v3)

        """ 2.2.1 lb in [-inf, q_v1], ub in [-inf, q_v1] """
        lb = max(pure_taker_bnd, 0)
        ub = min(taker_bnd, delta_v1_lb, delta_v1_ub)
        if lb <= ub:
            delta = - (B_U_taker + B_U_full) / 2 / (A_U_taker + A_U_full)
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = (A_U_taker + A_U_full) * delta ** 2 + (B_U_taker + B_U_full) * delta + (C_U_taker + C_U_full)
            if U > max_U:
                max_U = U
                delta_hat = delta

        """ 2.2.2 lb in [-inf, q_v1], ub in [q_v1, q_v2] """
        lb = max(pure_taker_bnd, 0, delta_v1_ub)
        ub = min(taker_bnd, delta_v1_lb, delta_v1_lb, delta_v2_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_1_ub
            B_E_partial = B_E_partial_int_1_ub
            C_E_partial = C_E_partial_int_1_ub - A_E_partial_int_1_C_q_v1
            D_E_partial = D_E_partial_int_1_ub - B_E_partial_int_1_C_q_v1
            E_E_partial = E_E_partial_int_1_ub - C_E_partial_int_1_C_q_v1

            A_P_full = - a_d_1 / 2 * A_ub_2
            B_P_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
            C_P_full = - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                            C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                                D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.3 lb in [-inf, q_v1], ub in [q_v2, q_v3] """
        lb = max(pure_taker_bnd, 0, delta_v2_ub)
        ub = min(taker_bnd, delta_v1_lb, delta_v1_lb, delta_v3_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_2_ub
            B_E_partial = B_E_partial_int_2_ub
            C_E_partial = C_E_partial_int_2_ub - A_E_partial_int_2_C_q_v2 + A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1
            D_E_partial = D_E_partial_int_2_ub - B_E_partial_int_2_C_q_v2 + B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1
            E_E_partial = E_E_partial_int_2_ub - C_E_partial_int_2_C_q_v2 + C_E_partial_int_1_C_q_v2 - C_E_partial_int_1_C_q_v1

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 / 2 * q_v2 ** 2 + b_d_2 * q_v2

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.4 lb in [-inf, q_v1], ub in [q_v3, inf] """
        lb = max(pure_taker_bnd, 0, delta_v3_ub)
        ub = min(taker_bnd, delta_v1_lb, delta_v1_lb)
        if lb <= ub:
            A_E_partial = A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
            B_E_partial = B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2
            C_E_partial = C_E_partial_int_1_C_q_v2 - C_E_partial_int_1_C_q_v1 + C_E_partial_int_2_C_q_v3 - C_E_partial_int_2_C_q_v2

            delta = - (B_U_taker + B_E_partial) / 2 / (A_U_taker + A_E_partial)
            U = (A_U_taker + A_E_partial) * delta ** 2 + (B_U_taker + B_E_partial) * delta + (C_U_taker + C_E_partial)
            if U > max_U:
                delta_hat = delta
                max_U = U

        """ 2.2.5 lb in [q_v1, q_v2], ub in [q_v1, q_v2] """
        lb = max(pure_taker_bnd, 0, delta_v1_lb, delta_v1_ub)
        ub = min(taker_bnd, delta_v2_lb, delta_v2_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_1_ub - A_E_partial_int_1_lb
            B_E_partial = B_E_partial_int_1_ub - B_E_partial_int_1_lb
            C_E_partial = C_E_partial_int_1_ub - C_E_partial_int_1_lb
            D_E_partial = D_E_partial_int_1_ub - D_E_partial_int_1_lb
            E_E_partial = E_E_partial_int_1_ub - E_E_partial_int_1_lb

            A_P_full = - a_d_1 / 2 * A_ub_2
            B_P_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
            C_P_full = - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.6 lb in [q_v1, q_v2], ub in [q_v2, q_v3] """
        lb = max(pure_taker_bnd, 0, delta_v1_lb, delta_v2_ub)
        ub = min(taker_bnd, delta_v2_lb, delta_v3_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_2_ub - A_E_partial_int_1_lb
            B_E_partial = B_E_partial_int_2_ub - B_E_partial_int_1_lb
            C_E_partial = C_E_partial_int_2_ub - C_E_partial_int_1_lb + A_E_partial_int_1_C_q_v2 - A_E_partial_int_2_C_q_v2
            D_E_partial = D_E_partial_int_2_ub - D_E_partial_int_1_lb + B_E_partial_int_1_C_q_v2 - B_E_partial_int_2_C_q_v2
            E_E_partial = E_E_partial_int_2_ub - E_E_partial_int_1_lb + C_E_partial_int_1_C_q_v2 - C_E_partial_int_2_C_q_v2

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 / 2 * q_v2 ** 2 + b_d_2 * q_v2

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.7 lb in [q_v1, q_v2], ub in [q_v3, inf] """
        lb = max(pure_taker_bnd, 0, delta_v1_lb, delta_v3_ub)
        ub = min(taker_bnd, delta_v2_lb)
        if lb <= ub:
            A_E_partial = - A_E_partial_int_1_lb
            B_E_partial = - B_E_partial_int_1_lb
            C_E_partial = - C_E_partial_int_1_lb + A_E_partial_int_1_C_q_v2 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
            D_E_partial = - D_E_partial_int_1_lb + B_E_partial_int_1_C_q_v2 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2
            E_E_partial = - E_E_partial_int_1_lb + C_E_partial_int_1_C_q_v2 + C_E_partial_int_2_C_q_v3 - C_E_partial_int_2_C_q_v2

            roots = np.roots([4 * A_E_partial, 3 * B_E_partial, 2 * (C_E_partial + A_U_taker), D_E_partial + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = A_E_partial * delta ** 4 + B_E_partial * delta ** 3 + (
                        C_E_partial + A_U_taker) * delta ** 2 + (
                            D_E_partial + B_U_taker) * delta + (E_E_partial + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.8 lb in [q_v2, q_v3], ub in [q_v2, q_v3] """
        lb = max(pure_taker_bnd, 0, delta_v2_lb, delta_v2_ub)
        ub = min(taker_bnd, delta_v3_lb, delta_v3_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_2_ub - A_E_partial_int_2_lb
            B_E_partial = B_E_partial_int_2_ub - B_E_partial_int_2_lb
            C_E_partial = C_E_partial_int_2_ub - C_E_partial_int_2_lb
            D_E_partial = D_E_partial_int_2_ub - D_E_partial_int_2_lb
            E_E_partial = E_E_partial_int_2_ub - E_E_partial_int_2_lb

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 / 2 * q_v2 ** 2 + b_d_2 * q_v2

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.9 lb in [q_v2, q_v3], ub in [q_v3, inf] """
        lb = max(pure_taker_bnd, 0, delta_v2_lb, delta_v3_ub)
        ub = min(taker_bnd, delta_v3_lb)
        if lb <= ub:
            A_E_partial = - A_E_partial_int_2_lb
            B_E_partial = - B_E_partial_int_2_lb
            C_E_partial = - C_E_partial_int_2_lb + A_E_partial_int_2_C_q_v3
            D_E_partial = - D_E_partial_int_2_lb + B_E_partial_int_2_C_q_v3
            E_E_partial = - E_E_partial_int_2_lb + C_E_partial_int_2_C_q_v3

            roots = np.roots([4 * A_E_partial, 3 * B_E_partial, 2 * (C_E_partial + A_U_taker), D_E_partial + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = A_E_partial * delta ** 4 + B_E_partial * delta ** 3 + (
                        C_E_partial + A_U_taker) * delta ** 2 + (
                            D_E_partial + B_U_taker) * delta + (E_E_partial + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 2.2.10 lb in [q_v3, inf], ub in [q_v3, inf] """
        lb = max(pure_taker_bnd, 0, delta_v3_lb, delta_v3_ub)
        ub = taker_bnd
        if lb <= ub:
            delta = - B_U_taker / 2 / A_U_taker
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = A_U_taker * delta ** 2 + B_U_taker * delta + C_U_taker
            if U > max_U:
                max_U = U
                delta_hat = delta

        """ 3. Maker """
        A_U_full = 0
        B_U_full = q * q_tick_size
        C_U_full = q * q_tick_size * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_maker) + C_R * 2 * I * I_d * q + C_R * q ** 2

        """ 3.1 No Queue """
        A_U_partial = C_R
        A_B_U_partial = q_tick_size
        B_B_U_partial = q_tick_size * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + 2 * C_R * I * I_d

        A_E_partial_int_1_C = lambda C: a_d_1 * A_B_U_partial / 3 * C ** 3 + b_d_1 * A_B_U_partial / 2 * C ** 2
        B_E_partial_int_1_C = lambda C: a_d_1 * A_U_partial / 4 * C ** 4 + (
                    b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C ** 3 + b_d_1 * B_B_U_partial / 2 * C ** 2
        A_E_partial_int_2_C = lambda C: a_d_2 * A_B_U_partial / 3 * C ** 3 + b_d_2 * A_B_U_partial / 2 * C ** 2
        B_E_partial_int_2_C = lambda C: a_d_2 * A_U_partial / 4 * C ** 4 + (
                    b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C ** 3 + b_d_2 * B_B_U_partial / 2 * C ** 2

        A_E_partial_int_1_C_q = A_E_partial_int_1_C(q)
        B_E_partial_int_1_C_q = B_E_partial_int_1_C(q)
        A_E_partial_int_1_C_q_v1 = A_E_partial_int_1_C(q_v1)
        B_E_partial_int_1_C_q_v1 = B_E_partial_int_1_C(q_v1)
        A_E_partial_int_1_C_q_v2 = A_E_partial_int_1_C(q_v2)
        B_E_partial_int_1_C_q_v2 = B_E_partial_int_1_C(q_v2)

        A_E_partial_int_2_C_q = A_E_partial_int_2_C(q)
        B_E_partial_int_2_C_q = B_E_partial_int_2_C(q)
        A_E_partial_int_2_C_q_v2 = A_E_partial_int_2_C(q_v2)
        B_E_partial_int_2_C_q_v2 = B_E_partial_int_2_C(q_v2)
        A_E_partial_int_2_C_q_v3 = A_E_partial_int_2_C(q_v3)
        B_E_partial_int_2_C_q_v3 = B_E_partial_int_2_C(q_v3)

        lb = taker_bnd
        ub = 0
        if lb <= ub:

            """ 3.1.1 lb in [-inf, q_v1], ub in [-inf, q_v1] """
            if (0 <= q_v1) and (q <= q_v1):
                if B_U_full >= 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = B_U_full * delta + C_U_full
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.2 lb in [-inf, q_v1], ub in [q_v1, q_v2] """
            if (0 <= q_v1) and (q >= q_v1) and (q <= q_v2):
                A_E_partial = A_E_partial_int_1_C_q - A_E_partial_int_1_C_q_v1
                B_E_partial = B_E_partial_int_1_C_q - B_E_partial_int_1_C_q_v1

                pr = - a_d_1 / 2 * q ** 2 - b_d_1 * q + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1
                B_E_full = pr * B_U_full
                C_E_full = pr * C_U_full

                if B_E_full + A_E_partial > 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = (B_E_full + A_E_partial) * delta + (C_E_full + B_E_partial)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.3 lb in [-inf, q_v1], ub in [q_v2, q_v3] """
            if (0 <= q_v1) and (q >= q_v2) and (q <= q_v3):
                A_E_partial = A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1 + A_E_partial_int_2_C_q - A_E_partial_int_2_C_q_v2
                B_E_partial = B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1 + B_E_partial_int_2_C_q - B_E_partial_int_2_C_q_v2

                pr = - a_d_1 / 2 * q ** 2 - b_d_1 * q + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2  + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 * q_v2 ** 2 + b_d_2 * q_v2
                B_E_full = pr * B_U_full
                C_E_full = pr * C_U_full

                if B_E_full + A_E_partial > 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = (B_E_full + A_E_partial) * delta + (C_E_full + B_E_partial)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.4 lb in [-inf, q_v1], ub in [q_v3, inf] """
            if (0 <= q_v1) and (q >= q_v3):
                A_E_partial = A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
                B_E_partial = B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2
                if A_E_partial >= 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = A_E_partial * delta + B_E_partial
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.5 lb in [q_v1, q_v2], ub in [q_v1, q_v2] """
            if (0 <= q_v2) and (0 >= q_v1) and (q <= q_v2) and (q >= q_v1):
                A_E_partial = A_E_partial_int_1_C_q
                B_E_partial = B_E_partial_int_1_C_q

                pr = - a_d_1 / 2 * q ** 2 - b_d_1 * q + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1
                B_E_full = pr * B_U_full
                C_E_full = pr * C_U_full

                if B_E_full + A_E_partial > 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = (B_E_full + A_E_partial) * delta + (C_E_full + B_E_partial)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.6 lb in [q_v1, q_v2], ub in [q_v2, q_v3] """
            if (0 <= q_v2) and (0 >= q_v1) and (q <= q_v3) and (q >= q_v2):
                A_E_partial = A_E_partial_int_2_C_q + A_E_partial_int_1_C_q_v2 - A_E_partial_int_2_C_q_v2
                B_E_partial = B_E_partial_int_2_C_q + B_E_partial_int_1_C_q_v2 - B_E_partial_int_2_C_q_v2

                pr = - a_d_1 / 2 * q ** 2 - b_d_1 * q + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 * q_v2 ** 2 + b_d_2 * q_v2
                B_E_full = pr * B_U_full
                C_E_full = pr * C_U_full

                if B_E_full + A_E_partial > 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = (B_E_full + A_E_partial) * delta + (C_E_full + B_E_partial)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.7 lb in [q_v1, q_v2], ub in [q_v3, inf] """
            if (0 <= q_v2) and (0 >= q_v1) and (q >= q_v3):
                A_E_partial = A_E_partial_int_2_C_q_v3 + A_E_partial_int_1_C_q_v2 - A_E_partial_int_2_C_q_v2
                B_E_partial = B_E_partial_int_2_C_q_v3 + B_E_partial_int_1_C_q_v2 - B_E_partial_int_2_C_q_v2

                if A_E_partial >= 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = A_E_partial * delta + B_E_partial
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.8 lb in [q_v2, q_v3], ub in [q_v2, q_v3] """
            if (0 <= q_v3) and (0 >= q_v2) and (q <= q_v3) and (q >= q_v2):
                A_E_partial = A_E_partial_int_2_C_q
                B_E_partial = B_E_partial_int_2_C_q

                pr = - a_d_1 / 2 * q ** 2 - b_d_1 * q + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 * q_v2 ** 2 + b_d_2 * q_v2
                B_E_full = pr * B_U_full
                C_E_full = pr * C_U_full

                if B_E_full + A_E_partial > 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = (B_E_full + A_E_partial) * delta + (C_E_full + B_E_partial)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.9 lb in [q_v2, q_v3], ub in [q_v3, inf] """
            if (0 <= q_v3) and (0 >= q_v2) and (q >= q_v3):
                A_E_partial = A_E_partial_int_2_C_q_v3
                B_E_partial = B_E_partial_int_2_C_q_v3

                if A_E_partial >= 0:
                    delta = -self._numeric_tol
                else:
                    delta = taker_bnd + self._numeric_tol
                U = A_E_partial * delta + B_E_partial
                if U > max_U:
                    delta_hat = delta
                    max_U = U

            """ 3.1.10 lb in [q_v3, inf], ub in [q_v3, inf] """
            # do nothing

        """ 3.2 Queued """
        A_lb_1 = a_d
        B_lb_1 = b_d
        A_lb_2 = A_lb_1 ** 2
        B_lb_2 = 2 * A_lb_1 * B_lb_1
        C_lb_2 = B_lb_1 ** 2
        A_lb_3 = A_lb_1 ** 3
        B_lb_3 = 3 * A_lb_1 ** 2 * B_lb_1
        C_lb_3 = 3 * A_lb_1 * B_lb_1 ** 2
        D_lb_3 = B_lb_1 ** 3
        A_lb_4 = A_lb_1 ** 4
        B_lb_4 = 4 * A_lb_1 ** 3 * B_lb_1
        C_lb_4 = 6 * A_lb_1 ** 2 * B_lb_1 ** 2
        D_lb_4 = 4 * A_lb_1 * B_lb_1 ** 3
        E_lb_4 = B_lb_1 ** 4

        A_ub_1 = a_d
        B_ub_1 = b_d + q
        A_ub_2 = A_ub_1 ** 2
        B_ub_2 = 2 * A_ub_1 * B_ub_1
        C_ub_2 = B_ub_1 ** 2
        A_ub_3 = A_ub_1 ** 3
        B_ub_3 = 3 * A_ub_1 ** 2 * B_ub_1
        C_ub_3 = 3 * A_ub_1 * B_ub_1 ** 2
        D_ub_3 = B_ub_1 ** 3
        A_ub_4 = A_ub_1 ** 4
        B_ub_4 = 4 * A_ub_1 ** 3 * B_ub_1
        C_ub_4 = 6 * A_ub_1 ** 2 * B_ub_1 ** 2
        D_ub_4 = 4 * A_ub_1 * B_ub_1 ** 3
        E_ub_4 = B_ub_1 ** 4

        delta_v1_lb = (q_v1 - B_lb_1) / A_lb_1
        delta_v2_lb = (q_v2 - B_lb_1) / A_lb_1
        delta_v3_lb = (q_v3 - B_lb_1) / A_lb_1

        delta_v1_ub = (q_v1 - B_ub_1) / A_ub_1
        delta_v2_ub = (q_v2 - B_ub_1) / A_ub_1
        delta_v3_ub = (q_v3 - B_ub_1) / A_ub_1

        A_U_partial = C_R
        A_B_U_partial = q_tick_size - 2 * C_R * a_d
        B_B_U_partial = q_tick_size * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) + 2 * C_R * I * I_d - 2 * C_R * b_d
        A_C_U_partial = C_R * a_d - q_tick_size * a_d
        B_C_U_partial = - q_tick_size * a_d * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) - q_tick_size * b_d - 2 * C_R * I * I_d * a_d + 2 * C_R * a_d * b_d
        C_C_U_partial = - q_tick_size * b_d * (I_d * (p_hat - tilde_p) + s_d - s_o_hat - c_taker) - 2 * C_R * I * I_d * b_d + C_R * b_d ** 2

        A_E_partial_int_1_C = lambda C: a_d_1 * A_C_U_partial / 2 * C ** 2 + b_d_1 * A_C_U_partial * C
        B_E_partial_int_1_C = lambda C: a_d_1 * A_B_U_partial / 3 * C ** 3 + (b_d_1 * A_B_U_partial + a_d_1 * B_C_U_partial) / 2 * C ** 2 + b_d_1 * B_C_U_partial * C
        C_E_partial_int_1_C = lambda C: a_d_1 * A_U_partial / 4 * C ** 4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C ** 3 + (b_d_1 * B_B_U_partial + a_d_1 * C_C_U_partial) / 2 * C ** 2 + b_d_1 * C_C_U_partial * C

        A_E_partial_int_2_C = lambda C: a_d_2 * A_C_U_partial / 2 * C ** 2 + b_d_2 * A_C_U_partial * C
        B_E_partial_int_2_C = lambda C: a_d_2 * A_B_U_partial / 3 * C ** 3 + (b_d_2 * A_B_U_partial + a_d_2 * B_C_U_partial) / 2 * C ** 2 + b_d_2 * B_C_U_partial * C
        C_E_partial_int_2_C = lambda C: a_d_2 * A_U_partial / 4 * C ** 4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C ** 3 + (b_d_2 * B_B_U_partial + a_d_2 * C_C_U_partial) / 2 * C ** 2 + b_d_2 * C_C_U_partial * C

        A_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * A_ub_4 + a_d_1 * A_B_U_partial / 3 * A_ub_3 + a_d_1 * A_C_U_partial / 2 * A_ub_2
        B_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * B_ub_4 + a_d_1 * A_B_U_partial / 3 * B_ub_3 + a_d_1 * A_C_U_partial / 2 * B_ub_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * A_ub_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * A_ub_2
        C_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * C_ub_4 + a_d_1 * A_B_U_partial / 3 * C_ub_3 + a_d_1 * A_C_U_partial / 2 * C_ub_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * B_ub_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * B_ub_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * A_ub_2
        D_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * D_ub_4 + a_d_1 * A_B_U_partial / 3 * D_ub_3 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C_ub_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * C_ub_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * B_ub_2
        E_E_partial_int_1_ub = a_d_1 * A_U_partial / 4 * E_ub_4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * D_ub_3 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * C_ub_2

        A_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * A_ub_4 + a_d_2 * A_B_U_partial / 3 * A_ub_3 + a_d_2 * A_C_U_partial / 2 * A_ub_2
        B_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * B_ub_4 + a_d_2 * A_B_U_partial / 3 * B_ub_3 + a_d_2 * A_C_U_partial / 2 * B_ub_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * A_ub_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * A_ub_2
        C_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * C_ub_4 + a_d_2 * A_B_U_partial / 3 * C_ub_3 + a_d_2 * A_C_U_partial / 2 * C_ub_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * B_ub_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * B_ub_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * A_ub_2
        D_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * D_ub_4 + a_d_2 * A_B_U_partial / 3 * D_ub_3 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C_ub_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * C_ub_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * B_ub_2
        E_E_partial_int_2_ub = a_d_2 * A_U_partial / 4 * E_ub_4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * D_ub_3 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * C_ub_2

        A_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * A_lb_4 + a_d_1 * A_B_U_partial / 3 * A_lb_3 + a_d_1 * A_C_U_partial / 2 * A_lb_2
        B_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * B_lb_4 + a_d_1 * A_B_U_partial / 3 * B_lb_3 + a_d_1 * A_C_U_partial / 2 * B_lb_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * A_lb_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * A_lb_2
        C_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * C_lb_4 + a_d_1 * A_B_U_partial / 3 * C_lb_3 + a_d_1 * A_C_U_partial / 2 * C_lb_2 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * B_lb_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * B_lb_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * A_lb_2
        D_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * D_lb_4 + a_d_1 * A_B_U_partial / 3 * D_lb_3 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * C_lb_3 + (a_d_1 * B_C_U_partial + b_d_1 * A_B_U_partial) / 2 * C_lb_2 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * B_lb_2
        E_E_partial_int_1_lb = a_d_1 * A_U_partial / 4 * E_lb_4 + (b_d_1 * A_U_partial + a_d_1 * B_B_U_partial) / 3 * D_lb_3 + (a_d_1 * C_C_U_partial + b_d_1 * B_B_U_partial) / 2 * C_lb_2

        A_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * A_lb_4 + a_d_2 * A_B_U_partial / 3 * A_lb_3 + a_d_2 * A_C_U_partial / 2 * A_lb_2
        B_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * B_lb_4 + a_d_2 * A_B_U_partial / 3 * B_lb_3 + a_d_2 * A_C_U_partial / 2 * B_lb_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * A_lb_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * A_lb_2
        C_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * C_lb_4 + a_d_2 * A_B_U_partial / 3 * C_lb_3 + a_d_2 * A_C_U_partial / 2 * C_lb_2 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * B_lb_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * B_lb_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * A_lb_2
        D_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * D_lb_4 + a_d_2 * A_B_U_partial / 3 * D_lb_3 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * C_lb_3 + (a_d_2 * B_C_U_partial + b_d_2 * A_B_U_partial) / 2 * C_lb_2 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * B_lb_2
        E_E_partial_int_2_lb = a_d_2 * A_U_partial / 4 * E_lb_4 + (b_d_2 * A_U_partial + a_d_2 * B_B_U_partial) / 3 * D_lb_3 + (a_d_2 * C_C_U_partial + b_d_2 * B_B_U_partial) / 2 * C_lb_2

        A_E_partial_int_1_C_q_v1 = A_E_partial_int_1_C(q_v1)
        B_E_partial_int_1_C_q_v1 = B_E_partial_int_1_C(q_v1)
        C_E_partial_int_1_C_q_v1 = C_E_partial_int_1_C(q_v1)
        A_E_partial_int_1_C_q_v2 = A_E_partial_int_1_C(q_v2)
        B_E_partial_int_1_C_q_v2 = B_E_partial_int_1_C(q_v2)
        C_E_partial_int_1_C_q_v2 = C_E_partial_int_1_C(q_v2)

        A_E_partial_int_2_C_q_v2 = A_E_partial_int_2_C(q_v2)
        B_E_partial_int_2_C_q_v2 = B_E_partial_int_2_C(q_v2)
        C_E_partial_int_2_C_q_v2 = C_E_partial_int_2_C(q_v2)
        A_E_partial_int_2_C_q_v3 = A_E_partial_int_2_C(q_v3)
        B_E_partial_int_2_C_q_v3 = B_E_partial_int_2_C(q_v3)
        C_E_partial_int_2_C_q_v3 = C_E_partial_int_2_C(q_v3)


        """ 3.2.1 lb in [-inf, q_v1], ub in [-inf, q_v1] """
        lb = max(taker_bnd, 0)
        ub = min(delta_v1_lb, delta_v1_ub)
        if lb <= ub:
            delta = - (B_U_taker + B_U_full) / 2 / (A_U_taker + A_U_full)
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = (A_U_taker + A_U_full) * delta ** 2 + (B_U_taker + B_U_full) * delta + (C_U_taker + C_U_full)
            if U > max_U:
                max_U = U
                delta_hat = delta

        """ 3.2.2 lb in [-inf, q_v1], ub in [q_v1, q_v2] """
        lb = max(taker_bnd, 0, delta_v1_ub)
        ub = min(delta_v1_lb, delta_v1_lb, delta_v2_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_1_ub
            B_E_partial = B_E_partial_int_1_ub
            C_E_partial = C_E_partial_int_1_ub - A_E_partial_int_1_C_q_v1
            D_E_partial = D_E_partial_int_1_ub - B_E_partial_int_1_C_q_v1
            E_E_partial = E_E_partial_int_1_ub - C_E_partial_int_1_C_q_v1

            A_P_full = - a_d_1 / 2 * A_ub_2
            B_P_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
            C_P_full = - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.3 lb in [-inf, q_v1], ub in [q_v2, q_v3] """
        lb = max(taker_bnd, 0, delta_v2_ub)
        ub = min(delta_v1_lb, delta_v1_lb, delta_v3_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_2_ub
            B_E_partial = B_E_partial_int_2_ub
            C_E_partial = C_E_partial_int_2_ub - A_E_partial_int_2_C_q_v2 + A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1
            D_E_partial = D_E_partial_int_2_ub - B_E_partial_int_2_C_q_v2 + B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1
            E_E_partial = E_E_partial_int_2_ub - C_E_partial_int_2_C_q_v2 + C_E_partial_int_1_C_q_v2 - C_E_partial_int_1_C_q_v1

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 / 2 * q_v2 ** 2 + b_d_2 * q_v2

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.4 lb in [-inf, q_v1], ub in [q_v3, inf] """
        lb = max(taker_bnd, 0, delta_v3_ub)
        ub = min(delta_v1_lb, delta_v1_lb)
        if lb <= ub:
            A_E_partial = A_E_partial_int_1_C_q_v2 - A_E_partial_int_1_C_q_v1 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
            B_E_partial = B_E_partial_int_1_C_q_v2 - B_E_partial_int_1_C_q_v1 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2
            C_E_partial = C_E_partial_int_1_C_q_v2 - C_E_partial_int_1_C_q_v1 + C_E_partial_int_2_C_q_v3 - C_E_partial_int_2_C_q_v2

            delta = - (B_U_taker + B_E_partial) / 2 / (A_U_taker + A_E_partial)
            U = (A_U_taker + A_E_partial) * delta ** 2 + (B_U_taker + B_E_partial) * delta + (C_U_taker + C_E_partial)
            if U > max_U:
                delta_hat = delta
                max_U = U

        """ 3.2.5 lb in [q_v1, q_v2], ub in [q_v1, q_v2] """
        lb = max(taker_bnd, 0, delta_v1_lb, delta_v1_ub)
        ub = min(delta_v2_lb, delta_v2_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_1_ub - A_E_partial_int_1_lb
            B_E_partial = B_E_partial_int_1_ub - B_E_partial_int_1_lb
            C_E_partial = C_E_partial_int_1_ub - C_E_partial_int_1_lb
            D_E_partial = D_E_partial_int_1_ub - D_E_partial_int_1_lb
            E_E_partial = E_E_partial_int_1_ub - E_E_partial_int_1_lb

            A_P_full = - a_d_1 / 2 * A_ub_2
            B_P_full = - a_d_1 / 2 * B_ub_2 - b_d_1 * A_ub_1
            C_P_full = - a_d_1 / 2 * C_ub_2 - b_d_1 * B_ub_1 + 1 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.6 lb in [q_v1, q_v2], ub in [q_v2, q_v3] """
        lb = max(taker_bnd, 0, delta_v1_lb, delta_v2_ub)
        ub = min(delta_v2_lb, delta_v3_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_2_ub - A_E_partial_int_1_lb
            B_E_partial = B_E_partial_int_2_ub - B_E_partial_int_1_lb
            C_E_partial = C_E_partial_int_2_ub - C_E_partial_int_1_lb + A_E_partial_int_1_C_q_v2 - A_E_partial_int_2_C_q_v2
            D_E_partial = D_E_partial_int_2_ub - D_E_partial_int_1_lb + B_E_partial_int_1_C_q_v2 - B_E_partial_int_2_C_q_v2
            E_E_partial = E_E_partial_int_2_ub - E_E_partial_int_1_lb + C_E_partial_int_1_C_q_v2 - C_E_partial_int_2_C_q_v2

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 / 2 * q_v2 ** 2 + b_d_2 * q_v2

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.7 lb in [q_v1, q_v2], ub in [q_v3, inf] """
        lb = max(taker_bnd, 0, delta_v1_lb, delta_v3_ub)
        ub = delta_v2_lb
        if lb <= ub:
            A_E_partial = - A_E_partial_int_1_lb
            B_E_partial = - B_E_partial_int_1_lb
            C_E_partial = - C_E_partial_int_1_lb + A_E_partial_int_1_C_q_v2 + A_E_partial_int_2_C_q_v3 - A_E_partial_int_2_C_q_v2
            D_E_partial = - D_E_partial_int_1_lb + B_E_partial_int_1_C_q_v2 + B_E_partial_int_2_C_q_v3 - B_E_partial_int_2_C_q_v2
            E_E_partial = - E_E_partial_int_1_lb + C_E_partial_int_1_C_q_v2 + C_E_partial_int_2_C_q_v3 - C_E_partial_int_2_C_q_v2

            roots = np.roots([4 * A_E_partial, 3 * B_E_partial, 2 * (C_E_partial + A_U_taker), D_E_partial + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = A_E_partial * delta ** 4 + B_E_partial * delta ** 3 + (
                        C_E_partial + A_U_taker) * delta ** 2 + (
                            D_E_partial + B_U_taker) * delta + (E_E_partial + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.8 lb in [q_v2, q_v3], ub in [q_v2, q_v3] """
        lb = max(taker_bnd, 0, delta_v2_lb, delta_v2_ub)
        ub = min(delta_v3_lb, delta_v3_ub)
        if lb <= ub:
            A_E_partial = A_E_partial_int_2_ub - A_E_partial_int_2_lb
            B_E_partial = B_E_partial_int_2_ub - B_E_partial_int_2_lb
            C_E_partial = C_E_partial_int_2_ub - C_E_partial_int_2_lb
            D_E_partial = D_E_partial_int_2_ub - D_E_partial_int_2_lb
            E_E_partial = E_E_partial_int_2_ub - E_E_partial_int_2_lb

            A_P_full = - a_d_2 / 2 * A_ub_2
            B_P_full = - a_d_2 / 2 * B_ub_2 - b_d_2 * A_ub_1
            C_P_full = - a_d_2 / 2 * C_ub_2 - b_d_2 * B_ub_1 + 1 - a_d_1 / 2 * q_v2 ** 2 - b_d_1 * q_v2 + a_d_1 / 2 * q_v1 ** 2 + b_d_1 * q_v1 + a_d_2 / 2 * q_v2 ** 2 + b_d_2 * q_v2

            A_E_full = A_P_full * A_U_full
            B_E_full = A_P_full * B_U_full + A_U_full * B_P_full
            C_E_full = A_P_full * C_U_full + A_U_full * C_P_full + B_P_full * B_U_full
            D_E_full = B_P_full * C_U_full + B_U_full * C_P_full
            E_E_full = C_P_full * C_U_full

            roots = np.roots(
                [4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full + A_U_taker),
                 D_E_partial + D_E_full + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = (A_E_partial + A_E_full) * delta ** 4 + (B_E_partial + B_E_full) * delta ** 3 + (
                        C_E_partial + C_E_full + A_U_taker) * delta ** 2 + (
                            D_E_partial + D_E_full + B_U_taker) * delta + (E_E_partial + E_E_full + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.9 lb in [q_v2, q_v3], ub in [q_v3, inf] """
        lb = max(taker_bnd, 0, delta_v2_lb, delta_v3_ub)
        ub = delta_v3_lb
        if lb <= ub:
            A_E_partial = - A_E_partial_int_2_lb
            B_E_partial = - B_E_partial_int_2_lb
            C_E_partial = - C_E_partial_int_2_lb + A_E_partial_int_2_C_q_v3
            D_E_partial = - D_E_partial_int_2_lb + B_E_partial_int_2_C_q_v3
            E_E_partial = - E_E_partial_int_2_lb + C_E_partial_int_2_C_q_v3

            roots = np.roots([4 * A_E_partial, 3 * B_E_partial, 2 * (C_E_partial + A_U_taker), D_E_partial + B_U_taker])
            roots = roots[np.isreal(roots)].real

            for delta in roots:
                delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
                U = A_E_partial * delta ** 4 + B_E_partial * delta ** 3 + (
                        C_E_partial + A_U_taker) * delta ** 2 + (
                            D_E_partial + B_U_taker) * delta + (E_E_partial + C_U_taker)
                if U > max_U:
                    delta_hat = delta
                    max_U = U

        """ 3.2.10 lb in [q_v3, inf], ub in [q_v3, inf] """
        lb = max(taker_bnd, 0, delta_v3_lb, delta_v3_ub)
        ub = np.inf
        if lb <= ub:
            delta = - B_U_taker / 2 / A_U_taker
            delta = self._range_bounding(x=delta, lb=lb, ub=ub, l_close=False, r_close=True)
            U = A_U_taker * delta ** 2 + B_U_taker * delta + C_U_taker
            if U > max_U:
                max_U = U
                delta_hat = delta

        return max_U, delta_hat

    def optimal_price_level(self, contract: str, ex: str, q: float, d: Direction, beta: float, T: float, tau: float, I: float) -> Tuple[float, float]:
        tick_size = self._get_qty_tick_size_gcd(contract=contract)
        int_I = int(round(I / tick_size))
        int_q = int(round(q / tick_size))
        return self._optimal_price_level(contract=contract, ex=ex, q=int_q, d=d, beta=beta, T=T, tau=tau, I=int_I)

    def _optimal_quantity_without_partial_fill(self, contract: str, ex: str, d: Direction, delta: float, beta: float, T: float, tau: float, I: int) -> Tuple[float, float]:
        xex = "xex"
        tilde_p = self.get_depth_metrics(contract=contract, ex=xex, metric="tilde_p")
        SEx_tilde_p = self.get_depth_metrics(contract=contract, ex=ex, metric="tilde_p")
        if d == Direction.long:
            s_d = self.get_depth_metrics(contract=contract, ex=xex, metric="s_l")
            s_o = self.get_depth_metrics(contract=contract, ex=xex, metric="s_s")
            a_d = self.get_depth_metrics(contract=contract, ex=xex, metric="a_l")
            b_d = self.get_depth_metrics(contract=contract, ex=xex, metric="b_l")
            a_o = self.get_depth_metrics(contract=contract, ex=xex, metric="a_s")
            b_o = self.get_depth_metrics(contract=contract, ex=xex, metric="b_s")
            SEx_s_d = self.get_depth_metrics(contract=contract, ex=ex, metric="s_l")
            SEx_s_o = self.get_depth_metrics(contract=contract, ex=ex, metric="s_s")
            SEx_a_o = self.get_depth_metrics(contract=contract, ex=ex, metric="a_s")
            SEx_b_o = self.get_depth_metrics(contract=contract, ex=ex, metric="b_s")
        else:
            s_d = self.get_depth_metrics(contract=contract, ex=xex, metric="s_s")
            s_o = self.get_depth_metrics(contract=contract, ex=xex, metric="s_l")
            a_d = self.get_depth_metrics(contract=contract, ex=xex, metric="a_s")
            b_d = self.get_depth_metrics(contract=contract, ex=xex, metric="b_s")
            a_o = self.get_depth_metrics(contract=contract, ex=xex, metric="a_l")
            b_o = self.get_depth_metrics(contract=contract, ex=xex, metric="b_l")
            SEx_s_d = self.get_depth_metrics(contract=contract, ex=ex, metric="s_s")
            SEx_s_o = self.get_depth_metrics(contract=contract, ex=ex, metric="s_l")
            SEx_a_o = self.get_depth_metrics(contract=contract, ex=ex, metric="a_l")
            SEx_b_o = self.get_depth_metrics(contract=contract, ex=ex, metric="b_l")
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)

        max_U = 0
        q_hat = 0

        """ Taker side """
        pnl = self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_tick_size)
        c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=tilde_p, q_tick_size=q_tick_size)
        C_R = -beta * 0.5 * (q_tick_size * tilde_p * sigma) ** 2 * (T - tau)
        a_u = C_R
        I_d = 1 if d == Direction.long else -1
        b_u = pnl - c_taker + C_R * 2 * I * I_d
        if d == Direction.long:
            taker_delta_bnd = tilde_p - s_d - SEx_tilde_p - SEx_s_o
        else:
            taker_delta_bnd = SEx_tilde_p - SEx_s_o - tilde_p - s_d
        if delta <= taker_delta_bnd:
            roots = [0, -b_u/a_u]
            lb = 0
            if d == Direction.long:
                p = tilde_p - s_d - delta
                delta_o = p - SEx_tilde_p - SEx_s_o
            else:
                p = tilde_p + s_d + delta
                delta_o = SEx_tilde_p - SEx_s_o - p
            Q = self._Q_d(delta_d=delta_o, a_d=SEx_a_o, b_d=SEx_b_o)
            ub = Q
            roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
            for q in roots:
                U = a_u * q**2 + b_u * q
                if U > max_U:
                    q_hat = q
                    max_U = U

        """ delta_d > -s_d-s_o """
        if (delta > taker_delta_bnd) and (delta < 0):
            Q = 0
        else:
            Q = self._Q_d(delta_d=delta, a_d=a_d, b_d=b_d)
        lambda_d = self._get_lambda_for_d(contract=contract, ex=xex, d=d) * tau
        a_d_1 = self._a_d_1(k_1=self._k_1, lambda_d=lambda_d)
        b_d_1 = self._b_d_1(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=self._epsilon, g=self._g)
        a_d_2 = self._a_d_2(k_2=self._k_2, lambda_d=lambda_d)
        b_d_2 = self._b_d_2(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=self._epsilon, g=self._g)
        c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=tilde_p, q_tick_size=q_tick_size)
        b_u = pnl - c_maker + C_R * 2* I * I_d

        vtx1 = -b_d_1/a_d_1
        vtx2 = lambda_d + self._epsilon
        vtx3 = -b_d_2/a_d_2

        """ range 1: [0, vtx1] """
        if vtx1 >= 0:  # range exists
            if Q <= vtx1:  # q exists
                roots = [0, -b_u/a_u]
                lb = 0
                ub = vtx1 - Q
                roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
                for q in roots:
                    U = a_u * q**2 + b_u * q
                    if U > max_U:
                        q_hat = q
                        max_U = U

        """ range 2: [vtx1, vtx2] """
        if vtx2 >= 0:  # range exists
            if Q <= vtx2:  # q exists
                lb = max(0., vtx1 - Q)
                ub = vtx2 - Q
                if vtx1 <= 0:  # integrate from 0
                    a_p_1 = - a_d_1/2
                    b_p_1 = - (a_d_1 * Q + b_d_1)
                    c_p_1 = 1 - a_d_1/2 * Q ** 2 - b_d_1 * Q
                else:  # integrate from vtx1
                    a_p_1 = - a_d_1 / 2
                    b_p_1 = - (a_d_1 * Q + b_d_1)
                    c_p_1 = 1 - a_d_1 / 2 * Q ** 2 - b_d_1 * Q + a_d_1/2*(-b_d_1/a_d_1)**2 + b_d_1*(-b_d_1/a_d_1)
                roots = np.roots([4*a_p_1*a_u, 3*(a_p_1*b_u+b_p_1*a_u), 2*(c_p_1*a_u+b_p_1*b_u), c_p_1*b_u])
                roots = roots[np.isreal(roots)].real
                roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
                for q in roots:
                    U = (a_p_1*q**2 + b_p_1*q + c_p_1)*(a_u*q**2 + b_u*q)
                    if U > max_U:
                        q_hat = q
                        max_U = U

        """ range 3: [vtx2, vtx3] """
        if vtx3 >= 0:  # range exists
            if Q <= vtx3:  # q exists
                lb = max(0., vtx2 - Q)
                ub = vtx3 - Q
                if vtx2 >= 0:
                    a_p = - a_d_2 / 2
                    b_p = - (a_d_2 * Q + b_d_2)
                    pr_1 = a_d_1 / 2 * vtx2**2 + b_d_1 * vtx2
                    if vtx1 >= 0:
                        c_p = 1 - pr_1 - a_d_2 / 2 * Q ** 2 - b_d_2 * Q + a_d_2 / 2 * vtx2 ** 2 + b_d_2 * vtx2 + a_d_1 / 2 * vtx1**2 + b_d_1 * vtx1
                    else:  # vtx1 >= 0
                        c_p = 1 - pr_1 - a_d_2 / 2 * Q ** 2 - b_d_2 * Q + a_d_2 / 2 * vtx2 ** 2 + b_d_2 * vtx2
                else:
                    a_p = - a_d_2 / 2
                    b_p = - (a_d_2 * Q + b_d_2)
                    c_p = 1 - a_d_2 / 2 * Q ** 2 - b_d_2 * Q
                roots = np.roots([4 * a_p * a_u, 3 * (a_p * b_u + b_p * a_u), 2 * (c_p * a_u + b_p * b_u), c_p * b_u])
                roots = roots[np.isreal(roots)].real
                roots = [self._range_bounding(x=x, lb=lb, ub=ub, l_close=False, r_close=True) for x in roots]
                for q in roots:
                    U = (a_p * q ** 2 + b_p * q + c_p) * (a_u * q ** 2 + b_u * q)
                    if U > max_U:
                        q_hat = q
                        max_U = U

        return max_U, q_hat

    def optimal_quantity_without_partial_fill(self, contract: str, ex: str, d: Direction, delta: float, beta: float, T: float, tau: float, I: float) -> Tuple[float, float]:
        tick_size = self._get_qty_tick_size_gcd(contract=contract)
        int_I = int(round(I / tick_size))
        U, q = self._optimal_quantity_without_partial_fill(contract=contract, ex=ex, d=d, delta=delta, beta=beta, T=T, tau=tau, I=int_I)
        return U, q * tick_size

    def _optimal_quantity(self, contract: str, ex: str, d: Direction, delta: float, beta: float, T: float, tau: float, I: int) -> Tuple[float, float]:
        xex = "xex"
        tilde_p = self.get_depth_metrics(contract=contract, ex=xex, metric="tilde_p")
        SEx_tilde_p = self.get_depth_metrics(contract=contract, ex=ex, metric="tilde_p")
        s_l = self.get_depth_metrics(contract=contract, ex=xex, metric="s_l")
        s_s = self.get_depth_metrics(contract=contract, ex=xex, metric="s_s")
        a_l = self.get_depth_metrics(contract=contract, ex=xex, metric="a_l")
        b_l = self.get_depth_metrics(contract=contract, ex=xex, metric="b_l")
        a_s = self.get_depth_metrics(contract=contract, ex=xex, metric="a_s")
        b_s = self.get_depth_metrics(contract=contract, ex=xex, metric="b_s")
        SEx_s_l = self.get_depth_metrics(contract=contract, ex=ex, metric="s_l")
        SEx_s_s = self.get_depth_metrics(contract=contract, ex=ex, metric="s_s")
        SEx_a_l = self.get_depth_metrics(contract=contract, ex=ex, metric="a_l")
        SEx_b_l = self.get_depth_metrics(contract=contract, ex=ex, metric="b_l")
        SEx_a_s = self.get_depth_metrics(contract=contract, ex=ex, metric="a_s")
        SEx_b_s = self.get_depth_metrics(contract=contract, ex=ex, metric="b_s")
        sigma = self.get_pred_sigma(contract=contract, ex=xex)
        p_hat = self.get_pred_ref_prc(contract=contract, ex=xex)
        s_o_hat = self.get_pred_half_sprd(contract=contract, ex=xex,
                                          d=Direction.short if d == Direction.long else Direction.long)
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        C_R = -beta * 0.5 * (q_tick_size * tilde_p * sigma) ** 2 * (T - tau)

        if d == Direction.long:
            taker_bnd = (tilde_p - s_l) - (SEx_tilde_p + SEx_s_s)
            I_d = 1
            s_d = s_l
        else:
            taker_bnd = (SEx_tilde_p - SEx_s_l) - (tilde_p + s_s)
            I_d = -1
            s_d = s_s

        pnl = self._pnl(p_hat=p_hat, s_o_hat=s_o_hat, tilde_p=tilde_p, s_d=s_d, delta_d=delta, d=d, q_tick_size=q_tick_size)

        max_U = 0
        q_hat = 0

        c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=tilde_p, q_tick_size=q_tick_size)
        c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=tilde_p, q_tick_size=q_tick_size)

        A_U_taker = C_R
        B_U_taker = 2 * C_R * I * I_d + pnl - c_taker
        A_U_maker = C_R
        B_U_maker = 2 * C_R * I * I_d + pnl - c_maker
        U_Full = lambda x: A_U_maker * x ** 2 + B_U_maker * x

        lambda_d = self._get_lambda_for_d(contract=contract, ex=xex, d=d) * tau
        epsilon = self._epsilon
        a_d_1 = self._a_d_1(k_1=self._k_1, lambda_d=lambda_d)
        b_d_1 = self._b_d_1(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        a_d_2 = self._a_d_2(k_2=self._k_2, lambda_d=lambda_d)
        b_d_2 = self._b_d_2(k_1=self._k_1, k_2=self._k_2, lambda_d=lambda_d, epsilon=epsilon, g=self._g)
        Pr_Full_Int_1 = lambda x: a_d_1 / 2 * x ** 2 + b_d_1 * x
        Pr_Full_Int_2 = lambda x: a_d_2 / 2 * x ** 2 + b_d_2 * x
        a_l = self.get_depth_metrics(contract=contract, ex=xex, metric="a_l")
        a_s = self.get_depth_metrics(contract=contract, ex=xex, metric="a_s")
        b_l = self.get_depth_metrics(contract=contract, ex=xex, metric="b_l")
        b_s = self.get_depth_metrics(contract=contract, ex=xex, metric="b_s")
        a_d = a_l if d == Direction.long else a_s
        b_d = b_l if d == Direction.long else b_s
        q_v1 = - b_d_1 / a_d_1
        q_v2 = lambda_d + epsilon
        q_v3 = - b_d_2 / a_d_2


        if delta <= taker_bnd:
            """ 1. With Taker """

            if d == Direction.long:
                A_Q_o = -SEx_a_s
                B_Q_o = SEx_a_s * taker_bnd + SEx_b_s
                Q_o = A_Q_o * delta + B_Q_o
            else:
                A_Q_o = -SEx_a_l
                B_Q_o = SEx_a_l * taker_bnd + SEx_b_l
                Q_o = A_Q_o * delta + B_Q_o

            """ 1.1 Pure Taker """
            q = -B_U_taker/A_U_taker/2
            if q > Q_o:
                q = Q_o
                U = A_U_taker * q ** 2 + B_U_taker * q
                if U > max_U:
                    q_hat = q
                    max_U = U

            """ 1.2 Taker + Maker """
            A_U_Full = A_U_maker
            B_U_Full = -2 * A_U_maker * Q_o + B_U_maker
            C_U_Full = A_U_maker * Q_o ^ 2 - B_U_maker * Q_o

            U_taker = A_U_taker * Q_o ** 2 + B_U_taker * Q_o

            if delta < 0:
                """ 1.2.1 No Queue """

                E_Partial_Int_1 = lambda x: a_d_1 * A_U_maker / 4 * x ** 4 + (a_d_1 * B_U_maker + b_d_1 * A_U_maker) / 3 * x ** 3 + b_d_1 * B_U_maker / 2 * x ** 2
                E_Partial_Int_2 = lambda x: a_d_2 * A_U_maker / 4 * x ** 4 + (a_d_2 * B_U_maker + b_d_2 * A_U_maker) / 3 * x ** 3 + b_d_2 * B_U_maker / 2 * x ** 2

                """ 1.2.1.1 maker q in (0, q_v1) """
                lb = Q_o
                ub = q_v1 + Q_o
                if lb < ub:
                    q = - B_U_Full / 2 / A_U_Full
                    q = self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                    U_maker = A_U_Full * q ** 2 + B_U_Full * q + C_U_Full
                    U = U_taker + U_maker
                    if U > max_U:
                        max_U = U
                        q_hat = q

                """ 1.2.1.2 maker q in (q_v1, q_v2] """
                b_v1 = max(q_v1, 0)
                lb = b_v1 + Q_o
                ub = q_v2 + Q_o
                if lb < ub:
                    A_E_partial = a_d_1 * A_U_maker / 4
                    B_E_partial = -a_d_1 * A_U_maker * Q_o + (a_d_1 * B_U_maker + b_d_1 * A_U_maker) / 3
                    C_E_partial = 3/2 * a_d_1 * A_U_maker * Q_o ** 2 - (a_d_1 * B_U_maker + b_d_1 * A_U_maker) * Q_o + b_d_1 * B_U_maker / 2
                    D_E_partial = -a_d_1 * A_U_maker * Q_o ** 3 + (a_d_1 * B_U_maker + b_d_1 * A_U_maker) * Q_o ** 2 - b_d_1 * B_U_maker * Q_o
                    E_E_partial = a_d_1 * A_U_maker / 4 * Q_o ** 4 - (a_d_1 * B_U_maker + b_d_1 * A_U_maker) / 3 * Q_o ** 3 + b_d_1 * B_U_maker / 2 * Q_o ** 2 - E_Partial_Int_1(b_v1) + U_Full(b_v1)

                    A_Pr_Full = - a_d_1 / 2
                    B_Pr_Full = a_d_1 * Q_o - b_d_1
                    C_Pr_Full = 1 - a_d_1 / 2 * Q_o ** 2 + b_d_1 * Q_o + Pr_Full_Int_1(b_v1)

                    A_E_full = A_Pr_Full * A_U_Full
                    B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                    C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                    D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                    E_E_full = C_Pr_Full * C_U_Full

                    roots = np.roots([4*(A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                    roots = roots[np.isreal(roots)].real
                    roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True) for q in roots]
                    for q in roots:
                        U_partial = A_E_partial*q**4 + B_E_partial*q**3 + C_E_partial*q**2 + D_E_partial*q + E_E_partial
                        U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                        U_maker = U_partial + U_full
                        U = U_taker + U_maker
                        if U > max_U:
                            max_U = U
                            q_hat = q

                """ 1.2.1.3 maker q in (q_v2, q_v3] """
                b_v2 = max(q_v2, 0)
                lb = b_v2 + Q_o
                ub = q_v3 + Q_o
                if lb < ub:
                    A_E_partial = a_d_2 * A_U_maker / 4
                    B_E_partial = -a_d_2 * A_U_maker * Q_o + (a_d_2 * B_U_maker + b_d_2 * A_U_maker) / 3
                    C_E_partial = 3 / 2 * a_d_2 * A_U_maker * Q_o ** 2 - (a_d_2 * B_U_maker + b_d_2 * A_U_maker) * Q_o + b_d_2 * B_U_maker / 2
                    D_E_partial = -a_d_2 * A_U_maker * Q_o ** 3 + (a_d_2 * B_U_maker + b_d_2 * A_U_maker) * Q_o ** 2 - b_d_2 * B_U_maker * Q_o
                    E_E_partial = a_d_2 * A_U_maker / 4 * Q_o ** 4 - (a_d_2 * B_U_maker + b_d_2 * A_U_maker) / 3 * Q_o ** 3 + b_d_2 * B_U_maker / 2 * Q_o ** 2 + E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + U_Full(b_v1) - E_Partial_Int_2(b_v2)

                    A_Pr_Full = - a_d_2 / 2
                    B_Pr_Full = a_d_2 + Q_o - b_d_2
                    C_Pr_Full = 1 - Pr_Full_Int_1(b_v2) + Pr_Full_Int_1(b_v1) - a_d_2 / 2 * Q_o ** 2 + b_d_2 * Q_o + Pr_Full_Int_2(b_v2)

                    A_E_full = A_Pr_Full * A_U_Full
                    B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                    C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                    D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                    E_E_full = C_Pr_Full * C_U_Full

                    roots = np.roots([4*(A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                    roots = roots[np.isreal(roots)].real
                    roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True) for q in roots]
                    for q in roots:
                        U_partial = A_E_partial*q**4 + B_E_partial*q**3 + C_E_partial*q**2 + D_E_partial*q + E_E_partial
                        U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                        U_maker = U_partial + U_full
                        U = U_taker + U_maker
                        if U > max_U:
                            max_U = U
                            q_hat = q

                """ 1.2.1.4 maker q in (q_v3, inf) """
                b_v3 = max(q_v3, 0)

                U_partial = U_Full(b_v1) + E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + E_Partial_Int_2(b_v3) - E_Partial_Int_2(b_v2)
                U_maker = U_partial
                U = U_taker + U_maker
                q = b_v3 + Q_o
                if U > max_U:
                    max_U = U
                    q_hat = q

            else:  # delta >= 0
                """ 1.2.2 Queued """
                Q_d = self._Q_d(delta_d=delta, a_d=a_d, b_d=b_d)

                E_Partial_Int_1 = lambda x: a_d_1 * A_U_maker / 4 * x ** 4 + (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3 * x ** 3 + (a_d_1 * Q_d + b_d_1) * B_U_maker / 2 * x ** 2
                E_Partial_Int_2 = lambda x: a_d_2 * A_U_maker / 4 * x ** 4 + (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3 * x ** 3 + (a_d_2 * Q_d + b_d_2) * B_U_maker / 2 * x ** 2

                if Q_d <= q_v1:
                    """ 1.2.2.1 maker Q_d in (-inf, q_v1] """

                    """ 1.2.2.1.1 maker q in (0, q_v1) """
                    lb = Q_o
                    ub = q_v1 - Q_d + Q_o
                    if lb < ub:
                        q = - B_U_Full / 2 / A_U_Full
                        q = self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                        U_maker = A_U_Full * q ** 2 + B_U_Full * q + C_U_Full
                        U = U_taker + U_maker
                        if U > max_U:
                            max_U = U
                            q_hat = q

                    """ 1.2.2.1.2 maker q in (q_v1, q_v2] """
                    b_v1 = max(q_v1 - Q_d, 0)
                    lb = b_v1 + Q_o
                    ub = q_v2 - Q_d + Q_o
                    if lb < ub:
                        A_E_partial = a_d_1 * A_U_maker / 4
                        B_E_partial = -a_d_1 * A_U_maker * Q_o + (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3
                        C_E_partial = 3 / 2 * a_d_1 * A_U_maker * Q_o ** 2 - (
                                    a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) * Q_o + (
                                                  a_d_1 * Q_d + b_d_1) * B_U_maker / 2
                        D_E_partial = -a_d_1 * A_U_maker * Q_o ** 3 + (
                                    a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) * Q_o ** 2 - (
                                                  a_d_1 * Q_d + b_d_1) * B_U_maker * Q_o
                        E_E_partial = a_d_1 * A_U_maker / 4 * Q_o ** 4 - (
                                    a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3 * Q_o ** 3 + (
                                                  a_d_1 * Q_d + b_d_1) * B_U_maker / 2 * Q_o ** 2 - E_Partial_Int_1(
                            b_v1) + U_Full(b_v1)

                        A_Pr_Full = - a_d_1 / 2
                        B_Pr_Full = a_d_1 * Q_o - (a_d_1 * Q_d + b_d_1)
                        C_Pr_Full = 1 - a_d_1 / 2 * (-Q_o + Q_d) ** 2 + b_d_1 * Q_o - b_d_1 * Q_d + Pr_Full_Int_1(
                            max(q_v1, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (
                        A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full),
                        D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_taker + U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 1.2.2.1.3 maker q in (q_v2, q_v3] """
                    b_v2 = max(q_v2 - Q_d, 0)
                    lb = b_v2 + Q_o
                    ub = q_v3 - Q_d + Q_o
                    if lb < ub:

                        A_E_partial = a_d_2 * A_U_maker / 4
                        B_E_partial = -a_d_2 * A_U_maker * Q_o + (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3
                        C_E_partial = 3 / 2 * a_d_2 * A_U_maker * Q_o ** 2 - (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) * Q_o + (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker / 2
                        D_E_partial = -a_d_2 * A_U_maker * Q_o ** 3 + (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) * Q_o ** 2 - (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker * Q_o
                        E_E_partial = a_d_2 * A_U_maker / 4 * Q_o ** 4 - (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3 * Q_o ** 3 + (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker / 2 * Q_o ** 2 + E_Partial_Int_1(
                            b_v2) - E_Partial_Int_1(b_v1) + U_Full(b_v1) - E_Partial_Int_2(b_v2)

                        A_Pr_Full = - a_d_2 / 2
                        B_Pr_Full = a_d_2 + Q_o - (a_d_2 * Q_d + b_d_2)
                        C_Pr_Full = 1 - Pr_Full_Int_1(max(q_v2, 0)) + Pr_Full_Int_1(max(q_v1, 0)) - a_d_2 / 2 * (
                                    -Q_o + Q_d) ** 2 - b_d_2 * (-Q_o + Q_d) + Pr_Full_Int_2(max(q_v2, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (
                        A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full),
                        D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_taker + U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 1.2.2.1.4 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)

                    U_partial = U_Full(b_v1) + E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + E_Partial_Int_2(
                        b_v3) - E_Partial_Int_2(b_v2)
                    U_maker = U_partial
                    U = U_taker + U_maker
                    q = b_v3 + Q_o
                    if U > max_U:
                        max_U = U
                        q_hat = q

                elif (Q_d > q_v1) and (Q_d <= q_v2):
                    """ 1.2.2.2 maker Q_d in (q_v1, q_v2] """

                    """ 1.2.2.2.1 maker q in (q_v1, q_v2] """
                    b_v1 = max(q_v1 - Q_d, 0)
                    lb = b_v1 + Q_o
                    ub = q_v2 - Q_d + Q_o
                    if lb < ub:
                        A_E_partial = a_d_1 * A_U_maker / 4
                        B_E_partial = -a_d_1 * A_U_maker * Q_o + (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3
                        C_E_partial = 3 / 2 * a_d_1 * A_U_maker * Q_o ** 2 - (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) * Q_o + (a_d_1 * Q_d + b_d_1) * B_U_maker / 2
                        D_E_partial = -a_d_1 * A_U_maker * Q_o ** 3 + (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) * Q_o ** 2 - (a_d_1 * Q_d + b_d_1) * B_U_maker * Q_o
                        E_E_partial = a_d_1 * A_U_maker / 4 * Q_o ** 4 - (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3 * Q_o ** 3 + (a_d_1 * Q_d + b_d_1) * B_U_maker / 2 * Q_o ** 2

                        A_Pr_Full = - a_d_1 / 2
                        B_Pr_Full = a_d_1 * Q_o - (a_d_1 * Q_d + b_d_1)
                        C_Pr_Full = 1 - a_d_1 / 2 * (-Q_o + Q_d) ** 2 + b_d_1 * Q_o - b_d_1 * Q_d + Pr_Full_Int_1(max(q_v1, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_taker + U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 1.2.2.2.2 maker q in (q_v2, q_v3] """
                    b_v2 = max(q_v2 - Q_d, 0)
                    lb = b_v2 + Q_o
                    ub = q_v3 - Q_d + Q_o
                    if lb < ub:

                        A_E_partial = a_d_2 * A_U_maker / 4
                        B_E_partial = -a_d_2 * A_U_maker * Q_o + (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3
                        C_E_partial = 3 / 2 * a_d_2 * A_U_maker * Q_o ** 2 - (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) * Q_o + (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker / 2
                        D_E_partial = -a_d_2 * A_U_maker * Q_o ** 3 + (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) * Q_o ** 2 - (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker * Q_o
                        E_E_partial = a_d_2 * A_U_maker / 4 * Q_o ** 4 - (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3 * Q_o ** 3 + (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker / 2 * Q_o ** 2 + E_Partial_Int_1(
                            b_v2) - E_Partial_Int_2(b_v2)

                        A_Pr_Full = - a_d_2 / 2
                        B_Pr_Full = a_d_2 + Q_o - (a_d_2 * Q_d + b_d_2)
                        C_Pr_Full = 1 - Pr_Full_Int_1(max(q_v2, 0)) + Pr_Full_Int_1(max(q_v1, 0)) - a_d_2 / 2 * (
                                    -Q_o + Q_d) ** 2 - b_d_2 * (-Q_o + Q_d) + Pr_Full_Int_2(max(q_v2, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_taker + U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 1.2.2.2.3 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)
                    U_partial = E_Partial_Int_1(b_v2) + E_Partial_Int_2(b_v3) - E_Partial_Int_2(b_v2)
                    U_maker = U_partial
                    U = U_taker + U_maker
                    q = b_v3 + Q_o
                    if U > max_U:
                        max_U = U
                        q_hat = q

                elif (Q_d > q_v2) and (Q_d <= q_v3):
                    """ 1.2.2.3 maker Q_d in (q_v2, q_v3] """

                    """ 1.2.2.3.1 maker q in (q_v2, q_v3] """
                    b_v2 = max(q_v2 - Q_d, 0)
                    lb = b_v2 + Q_o
                    ub = q_v3 - Q_d + Q_o
                    if lb < ub:

                        A_E_partial = a_d_2 * A_U_maker / 4
                        B_E_partial = -a_d_2 * A_U_maker * Q_o + (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3
                        C_E_partial = 3 / 2 * a_d_2 * A_U_maker * Q_o ** 2 - (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) * Q_o + (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker / 2
                        D_E_partial = -a_d_2 * A_U_maker * Q_o ** 3 + (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) * Q_o ** 2 - (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker * Q_o
                        E_E_partial = a_d_2 * A_U_maker / 4 * Q_o ** 4 - (
                                    a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3 * Q_o ** 3 + (
                                                  a_d_2 * Q_d + b_d_2) * B_U_maker / 2 * Q_o ** 2

                        A_Pr_Full = - a_d_2 / 2
                        B_Pr_Full = a_d_2 + Q_o - (a_d_2 * Q_d + b_d_2)
                        C_Pr_Full = 1 - Pr_Full_Int_1(max(q_v2, 0)) + Pr_Full_Int_1(max(q_v1, 0)) - a_d_2 / 2 * (
                                    -Q_o + Q_d) ** 2 - b_d_2 * (-Q_o + Q_d) + Pr_Full_Int_2(max(q_v2, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_taker + U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 1.2.2.3.2 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)
                    U_partial = E_Partial_Int_2(b_v3)
                    U_maker = U_partial
                    U = U_taker + U_maker
                    q = b_v3 + Q_o
                    if U > max_U:
                        max_U = U
                        q_hat = q

                elif Q_d > q_v3:
                    """ 1.2.2.4 maker Q_d in (q_v3, inf) """

                    """ 1.2.2.4.1 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)
                    U = U_taker
                    q = Q_o
                    if U > max_U:
                        max_U = U
                        q_hat = q

        else:
            """ 2. Maker """
            A_U_Full = A_U_maker
            B_U_Full = B_U_maker
            C_U_Full = 0

            if delta < 0:
                """ 2.1 No Queue """

                E_Partial_Int_1 = lambda x: a_d_1 * A_U_maker / 4 * x ** 4 + (a_d_1 * B_U_maker + b_d_1 * A_U_maker) / 3 * x ** 3 + b_d_1 * B_U_maker / 2 * x ** 2
                E_Partial_Int_2 = lambda x: a_d_2 * A_U_maker / 4 * x ** 4 + (a_d_2 * B_U_maker + b_d_2 * A_U_maker) / 3 * x ** 3 + b_d_2 * B_U_maker / 2 * x ** 2

                """ 2.1.1 maker q in (0, q_v1) """
                lb = 0
                ub = q_v1
                if lb < ub:
                    q = - B_U_maker / 2 / A_U_maker
                    q = self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                    U_maker = A_U_maker * q ** 2 + B_U_maker * q
                    U = U_maker
                    if U > max_U:
                        max_U = U
                        q_hat = q

                """ 2.1.2 maker q in (q_v1, q_v2] """
                b_v1 = max(q_v1, 0)
                lb = b_v1
                ub = q_v2
                if lb < ub:

                    A_E_partial = a_d_1 * A_U_maker / 4
                    B_E_partial = (a_d_1 * B_U_maker + b_d_1 * A_U_maker) / 3
                    C_E_partial = b_d_1 * B_U_maker / 2
                    D_E_partial = 0
                    E_E_partial = - E_Partial_Int_1(b_v1) + U_Full(b_v1)

                    A_Pr_Full = - a_d_1 / 2
                    B_Pr_Full = - b_d_1
                    C_Pr_Full = 1 + Pr_Full_Int_1(b_v1)

                    A_E_full = A_Pr_Full * A_U_Full
                    B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                    C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                    D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                    E_E_full = C_Pr_Full * C_U_Full

                    roots = np.roots([4*(A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                    roots = roots[np.isreal(roots)].real
                    roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True) for q in roots]
                    for q in roots:
                        U_partial = A_E_partial*q**4 + B_E_partial*q**3 + C_E_partial*q**2 + D_E_partial*q + E_E_partial
                        U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                        U_maker = U_partial + U_full
                        U = U_maker
                        if U > max_U:
                            max_U = U
                            q_hat = q

                """ 2.1.3 maker q in (q_v2, q_v3] """
                b_v2 = max(q_v2, 0)
                lb = b_v2
                ub = q_v3
                if lb < ub:

                    A_E_partial = a_d_2 * A_U_maker / 4
                    B_E_partial = (a_d_2 * B_U_maker + b_d_2 * A_U_maker) / 3
                    C_E_partial = b_d_2 * B_U_maker / 2
                    D_E_partial = 0
                    E_E_partial = E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + U_Full(b_v1) - E_Partial_Int_2(b_v2)

                    A_Pr_Full = - a_d_2 / 2
                    B_Pr_Full = - b_d_2
                    C_Pr_Full = 1 - Pr_Full_Int_1(b_v2) + Pr_Full_Int_1(b_v1) + Pr_Full_Int_2(b_v2)

                    A_E_full = A_Pr_Full * A_U_Full
                    B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                    C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                    D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                    E_E_full = C_Pr_Full * C_U_Full

                    roots = np.roots([4*(A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                    roots = roots[np.isreal(roots)].real
                    roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True) for q in roots]
                    for q in roots:
                        U_partial = A_E_partial*q**4 + B_E_partial*q**3 + C_E_partial*q**2 + D_E_partial*q + E_E_partial
                        U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                        U_maker = U_partial + U_full
                        U = U_maker
                        if U > max_U:
                            max_U = U
                            q_hat = q

                """ 2.1.4 maker q in (q_v3, inf) """
                b_v3 = max(q_v3, 0)

                U_partial = U_Full(b_v1) + E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + E_Partial_Int_2(b_v3) - E_Partial_Int_2(b_v2)
                U_maker = U_partial
                U = U_maker
                q = b_v3
                if U > max_U:
                    max_U = U
                    q_hat = q

            else:  # delta >= 0
                """ 2.2 Queued """
                Q_d = self._Q_d(delta_d=delta, a_d=a_d, b_d=b_d)

                E_Partial_Int_1 = lambda x: a_d_1 * A_U_maker / 4 * x ** 4 + (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3 * x ** 3 + (a_d_1 * Q_d + b_d_1) * B_U_maker / 2 * x ** 2
                E_Partial_Int_2 = lambda x: a_d_2 * A_U_maker / 4 * x ** 4 + (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3 * x ** 3 + (a_d_2 * Q_d + b_d_2) * B_U_maker / 2 * x ** 2

                if Q_d <= q_v1:
                    """ 2.2.1 maker Q_d in (-inf, q_v1] """

                    """ 2.2.1.1 maker q in (0, q_v1) """
                    lb = 0
                    ub = q_v1 - Q_d
                    if lb < ub:
                        q = - B_U_Full / 2 / A_U_Full
                        q = self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                        U_maker = A_U_Full * q ** 2 + B_U_Full * q + C_U_Full
                        U = U_maker
                        if U > max_U:
                            max_U = U
                            q_hat = q

                    """ 2.2.1.2 maker q in (q_v1, q_v2] """
                    b_v1 = max(q_v1 - Q_d, 0)
                    lb = b_v1
                    ub = q_v2 - Q_d
                    if lb < ub:
                        A_E_partial = a_d_1 * A_U_maker / 4
                        B_E_partial = (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3
                        C_E_partial = (a_d_1 * Q_d + b_d_1) * B_U_maker / 2
                        D_E_partial = 0
                        E_E_partial = - E_Partial_Int_1(b_v1) + U_Full(b_v1)

                        A_Pr_Full = - a_d_1 / 2
                        B_Pr_Full = - (a_d_1 * Q_d + b_d_1)
                        C_Pr_Full = 1 - a_d_1 / 2 * Q_d ** 2 - b_d_1 * Q_d + Pr_Full_Int_1(max(q_v1, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (
                        A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full),
                        D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 2.2.1.3 maker q in (q_v2, q_v3] """
                    b_v2 = max(q_v2 - Q_d, 0)
                    lb = b_v2
                    ub = q_v3 - Q_d
                    if lb < ub:

                        A_E_partial = a_d_2 * A_U_maker / 4
                        B_E_partial = (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3
                        C_E_partial = (a_d_2 * Q_d + b_d_2) * B_U_maker / 2
                        D_E_partial = 0
                        E_E_partial = E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + U_Full(b_v1) - E_Partial_Int_2(b_v2)

                        A_Pr_Full = - a_d_2 / 2
                        B_Pr_Full = - (a_d_2 * Q_d + b_d_2)
                        C_Pr_Full = 1 - Pr_Full_Int_1(max(q_v2, 0)) + Pr_Full_Int_1(max(q_v1, 0)) - a_d_2 / 2 * Q_d ** 2 - b_d_2 * Q_d + Pr_Full_Int_2(max(q_v2, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (
                        A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full),
                        D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 2.2.1.4 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)

                    U_partial = U_Full(b_v1) + E_Partial_Int_1(b_v2) - E_Partial_Int_1(b_v1) + E_Partial_Int_2(b_v3) - E_Partial_Int_2(b_v2)
                    U_maker = U_partial
                    U = U_maker
                    q = b_v3
                    if U > max_U:
                        max_U = U
                        q_hat = q

                elif (Q_d > q_v1) and (Q_d <= q_v2):
                    """ 2.2.2 maker Q_d in (q_v1, q_v2] """

                    """ 2.2.2.1 maker q in (q_v1, q_v2] """
                    b_v1 = max(q_v1 - Q_d, 0)
                    lb = b_v1
                    ub = q_v2 - Q_d
                    if lb < ub:

                        A_E_partial = a_d_1 * A_U_maker / 4
                        B_E_partial = (a_d_1 * B_U_maker + (a_d_1 * Q_d + b_d_1) * A_U_maker) / 3
                        C_E_partial = (a_d_1 * Q_d + b_d_1) * B_U_maker / 2
                        D_E_partial = 0
                        E_E_partial = 0

                        A_Pr_Full = - a_d_1 / 2
                        B_Pr_Full = - (a_d_1 * Q_d + b_d_1)
                        C_Pr_Full = 1 - a_d_1 / 2 * Q_d ** 2 - b_d_1 * Q_d + Pr_Full_Int_1(max(q_v1, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 2.2.2.2 maker q in (q_v2, q_v3] """
                    b_v2 = max(q_v2 - Q_d, 0)
                    lb = b_v2
                    ub = q_v3 - Q_d
                    if lb < ub:

                        A_E_partial = a_d_2 * A_U_maker / 4
                        B_E_partial = (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3
                        C_E_partial = (a_d_2 * Q_d + b_d_2) * B_U_maker / 2
                        D_E_partial = 0
                        E_E_partial = E_Partial_Int_1(b_v2) - E_Partial_Int_2(b_v2)

                        A_Pr_Full = - a_d_2 / 2
                        B_Pr_Full = - (a_d_2 * Q_d + b_d_2)
                        C_Pr_Full = 1 - Pr_Full_Int_1(max(q_v2, 0)) + Pr_Full_Int_1(max(q_v1, 0)) - a_d_2 / 2 * Q_d ** 2 - b_d_2 * Q_d + Pr_Full_Int_2(max(q_v2, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 2.2.2.3 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)
                    U_partial = E_Partial_Int_1(b_v2) + E_Partial_Int_2(b_v3) - E_Partial_Int_2(b_v2)
                    U_maker = U_partial
                    U = U_maker
                    q = b_v3
                    if U > max_U:
                        max_U = U
                        q_hat = q

                elif (Q_d > q_v2) and (Q_d <= q_v3):
                    """ 2.2.3 maker Q_d in (q_v2, q_v3] """

                    """ 2.2.3.1 maker q in (q_v2, q_v3] """
                    b_v2 = max(q_v2 - Q_d, 0)
                    lb = b_v2
                    ub = q_v3 - Q_d
                    if lb < ub:

                        A_E_partial = a_d_2 * A_U_maker / 4
                        B_E_partial = (a_d_2 * B_U_maker + (a_d_2 * Q_d + b_d_2) * A_U_maker) / 3
                        C_E_partial = (a_d_2 * Q_d + b_d_2) * B_U_maker / 2
                        D_E_partial = 0
                        E_E_partial = 0

                        A_Pr_Full = - a_d_2 / 2
                        B_Pr_Full = - (a_d_2 * Q_d + b_d_2)
                        C_Pr_Full = 1 - Pr_Full_Int_1(max(q_v2, 0)) + Pr_Full_Int_1(max(q_v1, 0)) - a_d_2 / 2 * Q_d ** 2 - b_d_2 * Q_d + Pr_Full_Int_2(max(q_v2, 0))

                        A_E_full = A_Pr_Full * A_U_Full
                        B_E_full = A_Pr_Full * B_U_Full + B_Pr_Full * A_U_Full
                        C_E_full = A_Pr_Full * C_U_Full + B_Pr_Full * B_U_Full + C_Pr_Full * A_U_Full
                        D_E_full = B_Pr_Full * C_U_Full + C_Pr_Full * B_U_Full
                        E_E_full = C_Pr_Full * C_U_Full

                        roots = np.roots([4 * (A_E_partial + A_E_full), 3 * (B_E_partial + B_E_full), 2 * (C_E_partial + C_E_full), D_E_partial + D_E_full])
                        roots = roots[np.isreal(roots)].real
                        roots = [self._range_bounding(x=q, lb=lb, ub=ub, l_close=False, r_close=True)
                                 for q in roots]
                        for q in roots:
                            U_partial = A_E_partial * q ** 4 + B_E_partial * q ** 3 + C_E_partial * q ** 2 + D_E_partial * q + E_E_partial
                            U_full = A_E_full * q ** 4 + B_E_full * q ** 3 + C_E_full * q ** 2 + D_E_full * q + E_E_full
                            U_maker = U_partial + U_full
                            U = U_maker
                            if U > max_U:
                                max_U = U
                                q_hat = q

                    """ 2.2.3.2 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)
                    U_partial = E_Partial_Int_2(b_v3)
                    U_maker = U_partial
                    U = U_maker
                    q = b_v3
                    if U > max_U:
                        max_U = U
                        q_hat = q

                elif Q_d > q_v3:
                    """ 2.2.4 maker Q_d in (q_v3, inf) """

                    """ 2.2.4.1 maker q in (q_v3, inf) """
                    b_v3 = max(q_v3 - Q_d, 0)
                    U = 0
                    pass

        return max_U, q_hat

    def optimal_quantity(self, contract: str, ex: str, d: Direction, delta: float, beta: float, T: float, tau: float, I: float) -> Tuple[float, float]:
        tick_size = self._get_qty_tick_size_gcd(contract=contract)
        int_I = int(round(I / tick_size))
        U, q = self._optimal_quantity(contract=contract, ex=ex, d=d, delta=delta, beta=beta, T=T, tau=tau, I=int_I)
        return U, q * tick_size