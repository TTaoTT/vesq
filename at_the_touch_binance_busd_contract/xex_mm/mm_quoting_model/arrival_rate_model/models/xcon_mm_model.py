import logging
import math
import nlopt

from typing import Dict, List, Tuple

import numpy as np
from scipy.stats import norm

from xex_mm.utils.base import Level, Depth, ReferencePriceCalculator
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.utils.newtons_method import VectorizedLinearNewtonsMethod

from cython_xcon_mm_model import CyXConMMModel


class DeltaObjectiveFuncForNlopt:
    def __init__(self,
                 q_arr: np.ndarray, q_tick_arr: np.ndarray,
                 tilde_p_arr: np.ndarray, s_d_arr: np.ndarray, p_hat_arr: np.ndarray, s_o_hat_arr: np.ndarray,
                 c_maker_arr: np.ndarray, c_taker_arr: np.ndarray,
                 one_d_arr: np.ndarray, I_arr: np.ndarray,
                 tilde_cov_arr: np.ndarray, beta_arr: np.ndarray, taker_bnd_arr: np.ndarray,
                 lambda_arr: np.ndarray, epsilon_arr: np.ndarray,
                 a_d_arr: np.ndarray, b_d_arr: np.ndarray,
                 debug: bool = False):
        """
        :param q_arr: The quantity adjustment to add to inventory I.
        """

        self._q_arr = q_arr
        self._q_tick_arr = q_tick_arr
        self._tilde_p_arr = tilde_p_arr
        self._s_d_arr = s_d_arr
        self._p_hat_arr = p_hat_arr
        self._s_o_hat_arr = s_o_hat_arr
        self._c_maker_arr = c_maker_arr
        self._c_taker_arr = c_taker_arr
        self._one_d_array = one_d_arr
        self._I_arr = I_arr
        self._tilde_cov_arr = tilde_cov_arr
        self._beta_arr = beta_arr
        self._taker_bnd_arr = taker_bnd_arr
        self._lambda_arr = lambda_arr
        self._epsilon_arr = epsilon_arr
        self._a_d_arr = a_d_arr
        self._b_d_arr = b_d_arr
        self._debug = debug

        self._diag_one_d_arr = np.diag(self._one_d_array)
        self._n = len(self._q_arr)
        self._one_arr = np.ones(self._n)
        self._dr_arr = self._diag_one_d_arr.dot(self._tilde_cov_arr).dot(self._I_arr + self._diag_one_d_arr.dot(self._q_arr + self._one_arr / 2))
        self._beta_dR_arr = np.diag(self._beta_arr).dot(self._dr_arr)
        self._amt_entry_arr = np.diag(self._q_tick_arr).dot(self._one_arr)
        self._exit_prc_mat = np.diag(self._p_hat_arr) - self._diag_one_d_arr.dot(np.diag(self._s_o_hat_arr))

        self._eval_count = 0

    def eval_delta(self, delta_arr: np.ndarray) -> Tuple:
        # U
        p_entry_arr = self._tilde_p_arr - self._diag_one_d_arr.dot(self._s_d_arr) - self._diag_one_d_arr.dot(delta_arr)
        q_token_arr = np.linalg.inv(np.diag(p_entry_arr)).dot(self._amt_entry_arr)
        amt_exit_arr = self._exit_prc_mat.dot(q_token_arr)
        maker_filter = delta_arr > self._taker_bnd_arr
        taker_filter = ~maker_filter
        one_maker_arr = maker_filter.astype(np.int)
        ont_taker_arr = taker_filter.astype(np.int)
        fee_arr = np.diag(one_maker_arr).dot(self._c_maker_arr) + np.diag(ont_taker_arr).dot(self._c_taker_arr)
        pnl_arr = self._diag_one_d_arr.dot(amt_exit_arr - self._amt_entry_arr) - np.diag(fee_arr).dot(q_token_arr)
        U_arr = pnl_arr - self._beta_dR_arr
        _q_arr = self._a_d_arr * delta_arr + self._b_d_arr + self._q_arr
        _mu_arr = self._lambda_arr - self._epsilon_arr
        _sigma_arr = np.sqrt(self._lambda_arr)
        _q_z_arr = (_q_arr - _mu_arr) / _sigma_arr
        pr_arr = 1 - norm.cdf(_q_z_arr)
        taker_filter = delta_arr <= self._taker_bnd_arr
        U_arr[~taker_filter] *= pr_arr[~taker_filter]
        # dU
        dpnl = (self._diag_one_d_arr.dot(self._exit_prc_mat) - np.diag(fee_arr)).dot(np.diag(self._amt_entry_arr)).dot(np.diag(1/p_entry_arr)).dot(self._diag_one_d_arr).dot(1 / p_entry_arr)
        dU_arr = - norm.pdf(_q_z_arr) * self._a_d_arr * U_arr + pr_arr.dot(dpnl)
        dU_arr[taker_filter] = dpnl[taker_filter]
        return U_arr, dU_arr, pnl_arr, self._dr_arr, pr_arr

    def __call__(self, x, grad):
        self._eval_count += 1
        U_arr, dU_arr, pnl_arr, dR_arr, pr_arr = self.eval_delta(delta_arr=x)
        U = np.ones(U_arr.shape).dot(U_arr)

        if self._debug:
            logging.info(f"eval_count={self._eval_count},x={x},U_arr={U_arr}, U={U},dU={dU_arr}, pnl={pnl_arr}, dr_arr={dR_arr}, pr_arr={pr_arr}")

        if grad.size > 0:
            grad[:] = dU_arr  # TODO: check this
        return U


class XConMmModel:
    """
    Cross contract & exchange MM model.
    """

    def __init__(self,
                 ref_prc_mdl_map: Dict,
                 half_sprd_mdl_map: Dict,
                 cov_mdl_map: Dict,
                 arrival_rate_mdl_map: Dict,
                 fee_rate_map: Dict,
                 q_grp_size: float,
                 debug: bool = False,
                 ):

        self._ref_prc_model_map = ref_prc_mdl_map  # contract, ex
        self._half_sprd_model_map = half_sprd_mdl_map  # contract, ex, side
        self._cov_model_map = cov_mdl_map  # ((contract1, ex1), (contract2, ex2))
        self._arrival_rate_model_map = arrival_rate_mdl_map  # contract, ex, side
        self._fee_rate_map = fee_rate_map  # contract, ex, maker_taker
        self._q_grp_size = q_grp_size
        self._debug = debug

        self._pred_ref_prc_map = {}  # contract, ex
        self._pred_half_sprd_map = {}  # contract, ex, side
        self._pred_cov_map = {}  # ((contract1, ex1), (contract2, ex2))
        self._pred_arrival_rate_map = {}  # contract, ex, side
        self._pred_arrival_rate_mkt_depth_decay_coef_map = {}  # contract, ex, side
        self._pred_arrival_rate_qty_decay_coef_map = {}  # contract, ex, side

        self._depth_metrics = {}  # contract, ex, metric

        self._epsilon = -0.5
        self._min_cov = 1e-12

    ############################################ Params Control #################################################

    def _update_ex_contract_d_preds(self, contract: str, ex: str, d: Direction, xs: np.ndarray, mdl_map: Dict, pred_map: Dict):
        mdl = mdl_map[contract][ex][d]
        if contract not in pred_map:
            pred_map[contract] = {}
        if ex not in pred_map[contract]:
            pred_map[contract][ex] = {}
        pred_map[contract][ex][d] = mdl.predict(xs=xs)

    def _update_ex_contract_preds(self, contract: str, ex: str, xs: np.ndarray, mdl_map: Dict, pred_map: Dict):
        mdl = mdl_map[contract][ex]
        if contract not in pred_map:
            pred_map[contract] = {}
        if ex not in pred_map[contract]:
            pred_map[contract][ex] = {}
        pred_map[contract][ex] = mdl.predict(xs=xs)

    def update_pred_arrival_rate(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_model_map,
                                         pred_map=self._pred_arrival_rate_map)

    def get_pred_arrival_rate(self, contract: str, ex: str, d: Direction) -> float:
        return self._pred_arrival_rate_map[contract][ex][d]

    def update_pred_ref_prc(self, contract: str, ex: str, xs: np.ndarray):
        self._update_ex_contract_preds(contract=contract, ex=ex, xs=xs,
                                       mdl_map=self._ref_prc_model_map,
                                       pred_map=self._pred_ref_prc_map)

    def get_pred_ref_prc(self, contract: str, ex: str, d: Direction) -> float:
        return self._pred_ref_prc_map[contract][ex][d]

    def update_pred_half_spread(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._half_sprd_model_map,
                                         pred_map=self._pred_half_sprd_map)

    def get_pred_half_sprd(self, contract: str, ex: str, d: Direction):
        return self._pred_half_sprd_map[contract][ex][d]

    @staticmethod
    def get_cov_key(contract1: str, ex1: str, contract2: str, ex2: str):
        key1 = (contract1, ex1)
        key2 = (contract2, ex2)
        f_make_str_key = lambda key1, key2: f"(({key1[0]},{key1[1]}),({key2[0]},{key2[1]}))"
        if key1 <= key2:
            return f_make_str_key(key1, key2)
        return f_make_str_key(key2, key1)

    def update_pred_cov(self, contract1: str, ex1: str, contract2: str, ex2: str, xs: np.ndarray):
        cov_key = self.get_cov_key(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2)
        mdl = self._cov_model_map[cov_key]
        val = mdl.predict(xs)
        self._pred_cov_map[cov_key] = val

    def get_pred_cov(self, contract1: str, ex1: str, contract2: str, ex2: str):
        cov_key = self.get_cov_key(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2)
        cov = self._pred_cov_map[cov_key]
        corr_sign = np.sign(cov)
        return corr_sign * max(self._min_cov, cov)

    def _get_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker):
        return self._fee_rate_map[contract][ex][maker_taker]

    def _get_lambda_for_d(self, contract: str, ex: str, d: Direction) -> float:
        d = Direction.long if d == Direction.short else Direction.short
        return self.get_pred_arrival_rate(contract=contract, ex=ex, d=d) / self._q_grp_size  # U

    def _calib_Q_func(self, side: List[Level]) -> Tuple[float, float]:
        if len(side) == 0:
            return 0, 0
        if len(side) == 1:
            return 0, side[0].qty
        Q = 0
        Qs = []
        ps = []
        for lvl in side:
            Q += lvl.qty
            Qs.append(Q)
            ps.append(lvl.prc)
        X = np.abs(np.array(ps) - ps[0])
        y = np.array(Qs) - Qs[0]
        beta = 1 / X.dot(X) * X.dot(y)
        return beta, Qs[0]

    def update_depth_metrics(self, contract: str, ex: str, depth: Depth, amt_bnd: float):
        """
        :param depth: USDT (CCY2) view.
        """
        if contract not in self._depth_metrics:
            self._depth_metrics[contract] = {}
        if ex not in self._depth_metrics[contract]:
            self._depth_metrics[contract][ex] = {}
        guarded_depth: Depth = depth.get_at_touch_guarded_view(amt_bnd=amt_bnd)
        tilde_p = self._depth_to_ref_prc(depth=guarded_depth, amt_bnd=amt_bnd)
        best_ask = guarded_depth.asks[0].prc if len(guarded_depth.asks) > 0 else math.inf
        best_bid = guarded_depth.bids[0].prc if len(guarded_depth.bids) > 0 else math.inf
        s_l = tilde_p - best_bid
        s_s = best_ask - tilde_p
        int_depth = guarded_depth.get_qty_grp_view(q_grp_size=self._q_grp_size)
        a_l, b_l = self._calib_Q_func(side=int_depth.bids)
        a_s, b_s = self._calib_Q_func(side=int_depth.asks)
        _map = dict(
            tilde_p = tilde_p,
            s_l = s_l,
            s_s = s_s,
            a_l = a_l,
            b_l = b_l,
            a_s = a_s,
            b_s = b_s,
        )
        self._depth_metrics[contract][ex] = _map

    def get_depth_metrics(self, contract: str, ex: str, metric: str):
        return self._depth_metrics[contract][ex][metric]

    ############################################ Computation Methods #################################################
    def _depth_to_ref_prc(self, depth: Depth, amt_bnd: float):
        return ReferencePriceCalculator().calc_from_depth(depth=depth, amt_bnd=amt_bnd)

    def _get_entries(self, contracts: List[str], exs: List[str]):
        entries = []
        long_filter = []
        for contract in contracts:
            for ex in exs:
                for d in [Direction.long, Direction.short]:
                    entries.append((contract, ex, d))
                    long_filter.append(d == Direction.long)
        long_filter = np.array(long_filter)
        return entries, long_filter

    def _get_arrays(self, entries: List[Tuple], taus: np.ndarray):
        xex = "xex"
        q_grp_size_arr = []
        lambda_arr = []
        p_hat_arr = []
        tilde_p_arr = []
        s_l_arr = []
        s_s_arr = []
        SEx_tilde_p_arr = []
        SEx_s_l_arr = []
        SEx_s_s_arr = []
        s_o_hat_arr = []
        c_maker_arr = []
        c_taker_arr = []
        cov_mat = []
        a_d_arr = []
        b_d_arr = []
        for contract, ex, d in entries:
            q_grp_size_arr.append(self._q_grp_size)
            lambda_arr.append(self._get_lambda_for_d(contract=contract, ex=ex, d=d))
            p_hat_arr.append(self.get_pred_ref_prc(contract=contract, ex=ex, d=d))
            tilde_p_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="tilde_p"))
            s_l_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="s_l"))
            s_s_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="s_s"))
            SEx_tilde_p_arr.append(self.get_depth_metrics(contract=contract, ex=ex, metric="tilde_p"))
            SEx_s_l_arr.append(self.get_depth_metrics(contract=contract, ex=ex, metric="s_l"))
            SEx_s_s_arr.append(self.get_depth_metrics(contract=contract, ex=ex, metric="s_s"))
            neg_d = Direction.short if d == Direction.long else Direction.long
            s_o_hat_arr.append(self.get_pred_half_sprd(contract=contract, ex=ex,d=neg_d))
            c_maker_arr.append(self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker))
            c_taker_arr.append(self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker))
            cov_row = []
            for contract2, ex2, d2 in entries:
                cov = self.get_pred_cov(contract1=contract, ex1=ex, contract2=contract2, ex2=ex2)
                cov_row.append(cov if d == d2 else -cov)
            cov_mat.append(cov_row)
            a_d_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="a_l" if d == Direction.long else "a_s"))
            b_d_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="b_l" if d == Direction.long else "b_s"))
        n = len(lambda_arr)
        epsilon_arr = np.array([self._epsilon] * n)
        one_d_arr =  np.array([1, -1] * int(n/2))
        q_grp_size_arr = np.array(q_grp_size_arr)
        lambda_arr = np.array(lambda_arr) * taus
        p_hat_arr = np.array(p_hat_arr)
        tilde_p_arr = np.array(tilde_p_arr)
        s_l_arr = np.array(s_l_arr)
        s_s_arr = np.array(s_s_arr)
        SEx_tilde_p_arr = np.array(SEx_tilde_p_arr)
        SEx_s_l_arr = np.array(SEx_s_l_arr)
        SEx_s_s_arr = np.array(SEx_s_s_arr)
        s_o_hat_arr = np.array(s_o_hat_arr)
        c_maker_arr = np.array(c_maker_arr)
        c_taker_arr = np.array(c_taker_arr)
        cov_mat = np.array(cov_mat)
        a_d_arr = np.array(a_d_arr)
        b_d_arr = np.array(b_d_arr)
        return epsilon_arr, one_d_arr, q_grp_size_arr, lambda_arr, p_hat_arr, tilde_p_arr, s_l_arr, s_s_arr, \
               SEx_tilde_p_arr, SEx_s_l_arr, SEx_s_s_arr, s_o_hat_arr, c_maker_arr, c_taker_arr, cov_mat, a_d_arr, b_d_arr

    def _optimal_price_level(self, contracts: List[str], exs: List[str], qs: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray) -> Tuple[float, np.ndarray]:

        """
        1 quantity placed at I + q
        """

        """ Collect params """

        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)

        epsilon_arr, one_d_arr, q_tick_size_arr, lambda_arr, p_hat_arr, tilde_p_arr, s_l_arr, s_s_arr, \
        SEx_tilde_p_arr, SEx_s_l_arr, SEx_s_s_arr, s_o_hat_arr, c_maker_arr, c_taker_arr, cov_arr, a_d_arr, b_d_arr \
            = self._get_arrays(entries=entries, taus=taus)

        short_filter = ~long_filter

        s_d_arr = np.copy(s_l_arr)
        s_d_arr[short_filter] = s_s_arr[short_filter]

        long_taker_bnd = (tilde_p_arr - s_l_arr) - (SEx_tilde_p_arr + SEx_s_s_arr)
        short_taker_bnd = (SEx_tilde_p_arr - SEx_s_l_arr) - (tilde_p_arr + s_s_arr)
        taker_bnd_arr = long_taker_bnd
        taker_bnd_arr[short_filter] = short_taker_bnd[short_filter]

        diag_q_tick = np.diag(q_tick_size_arr)
        diag_sqrt_T_tau = np.diag(np.sqrt(Ts - taus))

        tilde_cov_arr = diag_q_tick.dot(diag_sqrt_T_tau).dot(cov_arr).dot(diag_sqrt_T_tau).dot(diag_q_tick)

        n = len(entries)
        opt = nlopt.opt(nlopt.LD_SLSQP, n)
        obj_func = DeltaObjectiveFuncForNlopt(
            q_arr=qs,
            q_tick_arr=q_tick_size_arr,
            tilde_p_arr=tilde_p_arr,
            s_d_arr=s_d_arr,
            p_hat_arr=p_hat_arr,
            s_o_hat_arr=s_o_hat_arr,
            c_maker_arr=c_maker_arr,
            c_taker_arr=c_taker_arr,
            one_d_arr=one_d_arr,
            I_arr=Is,
            tilde_cov_arr=tilde_cov_arr,
            beta_arr=betas,
            taker_bnd_arr=taker_bnd_arr,
            lambda_arr=lambda_arr,
            epsilon_arr=epsilon_arr,
            a_d_arr=a_d_arr,
            b_d_arr=b_d_arr,
            debug=self._debug,
        )
        opt.set_max_objective(obj_func)
        opt.set_ftol_rel(1e-6)
        opt.set_ftol_abs(1e-2)
        opt.set_xtol_rel(1e-6)
        opt.set_xtol_abs(1e-2)
        opt.set_maxeval(30)
        # opt.set_maxtime()
        # nlopt.srand(0)
        delta_0_arr = (lambda_arr + epsilon_arr - b_d_arr - qs) / a_d_arr
        # opt.verbose = 1
        delta_hat_arr = opt.optimize(delta_0_arr)
        maxf = opt.last_optimum_value()
        max_U_arr, dU_arr, pnl_arr, dR_arr, pr_arr = obj_func.eval_delta(delta_arr=delta_hat_arr)
        assert abs(maxf - np.sum(max_U_arr)) < 1e-6
        bad_util = max_U_arr < 1e-6
        max_U_arr[bad_util] = 0
        delta_hat_arr[bad_util] = np.nan
        max_U = np.sum(max_U_arr)

        result_code = opt.last_optimize_result()

        if result_code < 0:
            logging.warning(f"Delta solving failed with code: {result_code}, check \n "
                            f"https://nlopt.readthedocs.io/en/latest/NLopt_Reference/#error-codes-negative-return-values")
            return np.nan, np.ones(n) * np.nan

        if result_code == 5:
            logging.warning(f"WARNING: Delta solving premature return due to max eval reached: {opt.get_maxeval()}.\n"
                            f"If you see this too often, check for systematic issues.")

        elif result_code == 6:
            logging.warning(f"Delta solving premature return due to max time reached: {opt.get_maxtime()}.\n"
                            f"If you see this too often, check for systematic issues.")

        if self._debug:
            # https://nlopt.readthedocs.io/en/latest/NLopt_Reference/#error-codes-negative-return-values
            logging.info(f"delta_hat_arr={delta_hat_arr}, maxf={maxf}, max_U={max_U}, max_U_arr={max_U_arr}, dU_arr={dU_arr}, result_code={result_code}")

        return maxf, delta_hat_arr

    def optimal_price_level(self, contracts: List[str], exs: List[str], qs: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray) -> Tuple[float, np.ndarray]:
        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)
        q_grp_size_arr = np.array([self._q_grp_size] * len(entries))
        int_I_arr = np.round(Is / q_grp_size_arr).astype(np.int)
        int_q_arr = np.round(qs / q_grp_size_arr).astype(np.int)
        return self._optimal_price_level(contracts=contracts, exs=exs, qs=int_q_arr, betas=betas, Ts=Ts, taus=taus, Is=int_I_arr)

    def _optimal_quantity(self, contracts: List[str], exs: List[str], deltas: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray, q0_arr: np.ndarray) -> Tuple[float, np.ndarray]:

        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)

        epsilon_arr, one_d_arr, q_tick_size_arr, lambda_arr, p_hat_arr, tilde_p_arr, s_l_arr, s_s_arr, \
        SEx_tilde_p_arr, SEx_s_l_arr, SEx_s_s_arr, s_o_hat_arr, c_maker_arr, c_taker_arr, cov_arr, a_d_arr, b_d_arr \
            = self._get_arrays(entries=entries, taus=taus)

        short_filter = ~long_filter

        s_d_arr = np.copy(s_l_arr)
        s_d_arr[short_filter] = s_s_arr[short_filter]

        long_taker_bnd = (tilde_p_arr - s_l_arr) - (SEx_tilde_p_arr + SEx_s_s_arr)
        short_taker_bnd = (SEx_tilde_p_arr - SEx_s_l_arr) - (tilde_p_arr + s_s_arr)
        taker_bnd_arr = long_taker_bnd
        taker_bnd_arr[short_filter] = short_taker_bnd[short_filter]

        diag_q_tick = np.diag(q_tick_size_arr)
        diag_sqrt_T_tau = np.diag(np.sqrt(Ts - taus))
        diag_one_d_arr = np.diag(one_d_arr)

        tilde_cov_arr = diag_q_tick.dot(diag_sqrt_T_tau).dot(cov_arr).dot(diag_sqrt_T_tau).dot(diag_q_tick)

        n = len(deltas)
        diag_beta_arr = np.diag(betas)

        no_entry_filter = np.isnan(deltas)
        p_entry_arr = tilde_p_arr - diag_one_d_arr.dot(s_d_arr) - one_d_arr * deltas
        one_arr = np.ones(n)
        one_arr[no_entry_filter] = 0
        amt_entry_arr = diag_q_tick.dot(one_arr)
        q_token_arr = amt_entry_arr / p_entry_arr
        q_token_arr[no_entry_filter] = 0
        exit_prc_mat = np.diag(p_hat_arr) - diag_one_d_arr.dot(np.diag(s_o_hat_arr))
        amt_exit_arr = exit_prc_mat.dot(q_token_arr)
        maker_filter = deltas > taker_bnd_arr
        taker_filter = ~maker_filter
        one_maker_arr = maker_filter.astype(np.int)
        one_taker_arr = taker_filter.astype(np.int)
        fee_arr = np.diag(one_maker_arr).dot(c_maker_arr) + np.diag(one_taker_arr).dot(c_taker_arr)
        pnl_arr = diag_one_d_arr.dot(amt_exit_arr - amt_entry_arr) - np.diag(fee_arr).dot(q_token_arr)

        diag_one_arr = np.diag(one_arr)
        A_U_mat = - diag_beta_arr.dot(diag_one_arr).dot(diag_one_d_arr).dot(tilde_cov_arr).dot(diag_one_d_arr)
        B_U_arr = pnl_arr - diag_beta_arr.dot(diag_one_arr).dot(diag_one_d_arr).dot(tilde_cov_arr).dot(Is + 0.5 * diag_one_d_arr.dot(one_arr))

        o = VectorizedLinearNewtonsMethod(tol=1e-6, max_iter=10, debug=self._debug)
        o.set_A_b(A=A_U_mat, b=B_U_arr)
        q0_arr[no_entry_filter] = 0
        q_hat_arr, resid, n_iter = o.eval(x=q0_arr)

        if n_iter >= o.max_iter:
            logging.warning(f"Quantity solving hit max iter: {o.max_iter}")

        if self._debug:
            logging.info(f"q_hat_arr={q_hat_arr}, resid={resid}, sum_resid={np.sum(resid)}, n_iter={n_iter}")

        dR_arr = diag_one_d_arr.dot(tilde_cov_arr).dot(Is + diag_one_d_arr.dot(q_hat_arr + 0.5 * one_arr))

        U_arr = A_U_mat.dot(q_hat_arr) + B_U_arr
        _q_arr = a_d_arr * deltas + b_d_arr + q_hat_arr
        _mu_arr = lambda_arr - epsilon_arr
        _sigma_arr = np.sqrt(lambda_arr)
        _q_z_arr = (_q_arr - _mu_arr) / _sigma_arr
        pr_arr = 1 - norm.cdf(_q_z_arr)

        U_arr[~taker_filter] *= pr_arr[~taker_filter]

        if np.any(np.isnan(U_arr)):
            raise RuntimeError(f"NaN in U_arr={U_arr}!")

        if self._debug:
            logging.info(f"pnl={pnl_arr}, dR={dR_arr}, pr={pr_arr}, U_arr={U_arr}")

        max_U = np.sum(U_arr)

        return max_U, q_hat_arr

    def optimal_quantity(self, contracts: List[str], exs: List[str], deltas: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray, q0_arr: np.ndarray) -> Tuple[float, np.ndarray]:
        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)
        q_grp_size_arr = np.array([self._q_grp_size] * len(entries))
        int_I_arr = np.round(Is / q_grp_size_arr).astype(np.int)
        int_q0_arr = np.round(q0_arr / q_grp_size_arr).astype(np.int)
        U, q = self._optimal_quantity(contracts=contracts, exs=exs, deltas=deltas, betas=betas, Ts=Ts, taus=taus, Is=int_I_arr, q0_arr=int_q0_arr)
        return U, q * q_grp_size_arr


class PyXConMmModel:
    """
    Cross contract & exchange MM model.
    """

    def __init__(self,
                 ref_prc_mdl_map: Dict,
                 half_sprd_mdl_map: Dict,
                 cov_mdl_map: Dict,
                 arrival_rate_mdl_map: Dict,
                 fee_rate_map: Dict,
                 q_grp_size_map: Dict,
                 debug: bool = False,
                 ):

        self._cy_xcon_mm_model = CyXConMMModel()
        self._set_fee_rate_map(fee_rate_map=fee_rate_map)
        self._set_q_grp_size_map(q_grp_size_map=q_grp_size_map)

        self._ref_prc_mdl_map = ref_prc_mdl_map  # contract, ex
        self._half_sprd_mdl_map = half_sprd_mdl_map  # contract, ex, side
        self._cov_mdl_map = cov_mdl_map  # contract, ex
        self._arrival_rate_mdl_map = arrival_rate_mdl_map  # contract, ex, side
        self._debug = debug

        self._epsilon = -0.5
        self._min_cov = 1e-12

    ############################################ Params Control #################################################
    def update_pred_arrival_rate(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        mdl = self._arrival_rate_mdl_map[contract][ex][d]
        val = mdl.predict(xs)
        self._cy_xcon_mm_model.update_pred_arrival_rate(contract=contract.encode(), ex=ex.encode(), d=d, val=val)

    def get_pred_arrival_rate(self, contract: str, ex: str, d: Direction) -> float:
        return self._cy_xcon_mm_model.get_pred_arrival_rate(contract=contract.encode(), ex=ex.encode(), d=d)

    def update_pred_ref_prc(self, contract: str, ex: str, xs: np.ndarray):
        mdl = self._ref_prc_mdl_map[contract][ex]
        val = mdl.predict(xs)
        self._cy_xcon_mm_model.update_pred_ref_prc(contract=contract.encode(), ex=ex.encode(), val=val)

    def get_pred_ref_prc(self, contract: str, ex: str) -> float:
        return self._cy_xcon_mm_model.get_pred_ref_prc(contract=contract.encode(), ex=ex.encode())

    def update_pred_half_sprd(self, contract: str, ex: str, d: int, xs: np.ndarray):
        mdl = self._half_sprd_mdl_map[contract][ex][d]
        val = mdl.predict(xs)
        self._cy_xcon_mm_model.update_pred_half_sprd(contract=contract.encode(), ex=ex.encode(), d=d, val=val)

    def get_pred_half_sprd(self, contract: str, ex: str, d: int):
        return self._cy_xcon_mm_model.get_pred_half_sprd(contract=contract.encode(), ex=ex.encode(), d=d)

    @staticmethod
    def get_cov_key(contract1: str, ex1: str, contract2: str, ex2: str):
        return CyXConMMModel.get_cov_key(contract1=contract1.encode(), ex1=ex1.encode(), contract2=contract2.encode(), ex2=ex2.encode())

    def update_pred_cov(self, contract1: str, ex1: str, contract2: str, ex2: str, xs: np.ndarray):
        cov_key = self.get_cov_key(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2)
        mdl = self._cov_mdl_map[cov_key]
        val = mdl.predict(xs)
        self._cy_xcon_mm_model.update_pred_cov(contract1=contract1.encode(), ex1=ex1.encode(), contract2=contract2.encode(), ex2=ex2.encode(), val=val)

    def get_pred_cov(self, contract1: str, ex1: str, contract2: str, ex2: str):
        return self._cy_xcon_mm_model.get_pred_cov(contract1=contract1.encode(), ex1=ex1.encode(), contract2=contract2.encode(), ex2=ex2.encode())

    def _set_fee_rate_map(self, fee_rate_map: Dict):
        for contract, d1 in fee_rate_map.items():
            for ex, d2 in d1.items():
                for maker_taker, val in d2.items():
                    self.update_fee_rate(contract=contract, ex=ex, maker_taker=maker_taker, val=val)

    def update_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker, val: float):
        self._cy_xcon_mm_model.update_fee_rate(contract=contract.encode(), ex=ex.encode(), maker_taker=maker_taker, val=val)

    def get_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker):
        return self._cy_xcon_mm_model.get_fee_rate(contract=contract.encode(), ex=ex.encode(), maker_taker=maker_taker)

    def _set_q_grp_size_map(self, q_grp_size_map: Dict):
        for contract, val in q_grp_size_map.items():
            self.update_q_grp_size(contract=contract, val=val)

    def update_q_grp_size(self, contract: str, val: float):
        self._cy_xcon_mm_model.update_q_grp_size(contract=contract.encode(), val=val)

    def get_q_grp_size(self, contract: str):
        return self._cy_xcon_mm_model.get_q_grp_size(contract=contract.encode())

    def _depth_to_ref_prc(self, depth: Depth, amt_bnd: float):
        return ReferencePriceCalculator().calc_from_depth(depth=depth, amt_bnd=amt_bnd)

    def _calib_Q_func(self, side: List[Level]) -> Tuple[float, float]:
        if len(side) == 0:
            return 0, 0
        if len(side) == 1:
            return 0, side[0].qty
        Q = 0
        Qs = []
        ps = []
        for lvl in side:
            Q += lvl.qty
            Qs.append(Q)
            ps.append(lvl.prc)
        X = np.abs(np.array(ps) - ps[0])
        y = np.array(Qs) - Qs[0]
        beta = 1 / X.dot(X) * X.dot(y)
        return beta, Qs[0]

    def update_depth_metrics(self, contract: str, ex: str, depth: Depth, amt_bnd: float):
        """
        :param depth: USDT (CCY2) view.
        """
        guarded_depth: Depth = depth.get_at_touch_guarded_view(amt_bnd=amt_bnd)
        tilde_p = self._depth_to_ref_prc(depth=guarded_depth, amt_bnd=amt_bnd)
        best_ask = guarded_depth.asks[0].prc if len(guarded_depth.asks) > 0 else math.inf
        best_bid = guarded_depth.bids[0].prc if len(guarded_depth.bids) > 0 else math.inf
        s_l = tilde_p - best_bid
        s_s = best_ask - tilde_p
        int_depth = guarded_depth.get_qty_grp_view(q_grp_size=self.get_q_grp_size(contract=contract))
        a_l, b_l = self._calib_Q_func(side=int_depth.bids)
        a_s, b_s = self._calib_Q_func(side=int_depth.asks)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="tilde_p".encode(), val=tilde_p)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="s_l".encode(), val=s_l)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="s_s".encode(), val=s_s)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="a_l".encode(), val=a_l)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="b_l".encode(), val=b_l)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="a_s".encode(), val=a_s)
        self._cy_xcon_mm_model.update_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric="b_s".encode(), val=b_s)

    def get_depth_metrics(self, contract: str, ex: str, metric: str):
        return self._cy_xcon_mm_model.get_depth_metrics(contract=contract.encode(), ex=ex.encode(), metric=metric.encode())

    ############################################ Computation Methods #################################################

    def _get_lambda_for_d(self, contract: str, ex: str, d: Direction) -> float:
        d = Direction.long if d == Direction.short else Direction.short
        return self.get_pred_arrival_rate(contract=contract, ex=ex, d=d) / self.get_q_grp_size(contract=contract)  # U

    def _get_entries(self, contracts: List[str], exs: List[str]):
        entries = []
        long_filter = []
        for contract in contracts:
            for ex in exs:
                for d in [Direction.long, Direction.short]:
                    entries.append((contract, ex, d))
                    long_filter.append(d == Direction.long)
        long_filter = np.array(long_filter)
        return entries, long_filter

    def _get_arrays(self, entries: List[Tuple], taus: np.ndarray):
        xex = "xex"
        q_grp_size_arr = []
        lambda_arr = []
        p_hat_arr = []
        tilde_p_arr = []
        s_l_arr = []
        s_s_arr = []
        SEx_tilde_p_arr = []
        SEx_s_l_arr = []
        SEx_s_s_arr = []
        s_o_hat_arr = []
        c_maker_arr = []
        c_taker_arr = []
        cov_mat = []
        a_d_arr = []
        b_d_arr = []
        for contract, ex, d in entries:
            q_grp_size_arr.append(self.get_q_grp_size(contract=contract))
            lambda_arr.append(self._get_lambda_for_d(contract=contract, ex=ex, d=d))
            p_hat_arr.append(self.get_pred_ref_prc(contract=contract, ex=ex))
            tilde_p_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="tilde_p"))
            s_l_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="s_l"))
            s_s_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="s_s"))
            SEx_tilde_p_arr.append(self.get_depth_metrics(contract=contract, ex=ex, metric="tilde_p"))
            SEx_s_l_arr.append(self.get_depth_metrics(contract=contract, ex=ex, metric="s_l"))
            SEx_s_s_arr.append(self.get_depth_metrics(contract=contract, ex=ex, metric="s_s"))
            neg_d = Direction.short if d == Direction.long else Direction.long
            s_o_hat_arr.append(self.get_pred_half_sprd(contract=contract, ex=ex,d=neg_d))
            c_maker_arr.append(self.get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker))
            c_taker_arr.append(self.get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker))
            cov_row = []
            for contract2, ex2, d2 in entries:
                cov = self.get_pred_cov(contract1=contract, ex1=ex, contract2=contract2, ex2=ex2)
                cov_row.append(cov if d == d2 else -cov)
            cov_mat.append(cov_row)
            a_d_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="a_l" if d == Direction.long else "a_s"))
            b_d_arr.append(self.get_depth_metrics(contract=contract, ex=xex, metric="b_l" if d == Direction.long else "b_s"))
        n = len(lambda_arr)
        epsilon_arr = np.array([self._epsilon] * n)
        one_d_arr =  np.array([1, -1] * int(n/2))
        q_grp_size_arr = np.array(q_grp_size_arr)
        lambda_arr = np.array(lambda_arr) * taus
        p_hat_arr = np.array(p_hat_arr)
        tilde_p_arr = np.array(tilde_p_arr)
        s_l_arr = np.array(s_l_arr)
        s_s_arr = np.array(s_s_arr)
        SEx_tilde_p_arr = np.array(SEx_tilde_p_arr)
        SEx_s_l_arr = np.array(SEx_s_l_arr)
        SEx_s_s_arr = np.array(SEx_s_s_arr)
        s_o_hat_arr = np.array(s_o_hat_arr)
        c_maker_arr = np.array(c_maker_arr)
        c_taker_arr = np.array(c_taker_arr)
        cov_mat = np.array(cov_mat)
        a_d_arr = np.array(a_d_arr)
        b_d_arr = np.array(b_d_arr)
        return epsilon_arr, one_d_arr, q_grp_size_arr, lambda_arr, p_hat_arr, tilde_p_arr, s_l_arr, s_s_arr, \
               SEx_tilde_p_arr, SEx_s_l_arr, SEx_s_s_arr, s_o_hat_arr, c_maker_arr, c_taker_arr, cov_mat, a_d_arr, b_d_arr

    def _optimal_price_level(self, contracts: List[str], exs: List[str], qs: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray) -> Tuple[float, np.ndarray]:

        """
        1 quantity placed at I + q
        """

        """ Collect params """

        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)

        epsilon_arr, one_d_arr, q_grp_size_arr, lambda_arr, p_hat_arr, tilde_p_arr, s_l_arr, s_s_arr, \
        SEx_tilde_p_arr, SEx_s_l_arr, SEx_s_s_arr, s_o_hat_arr, c_maker_arr, c_taker_arr, cov_arr, a_d_arr, b_d_arr \
            = self._get_arrays(entries=entries, taus=taus)

        short_filter = ~long_filter

        s_d_arr = np.copy(s_l_arr)
        s_d_arr[short_filter] = s_s_arr[short_filter]

        long_taker_bnd = (tilde_p_arr - s_l_arr) - (SEx_tilde_p_arr + SEx_s_s_arr)
        short_taker_bnd = (SEx_tilde_p_arr - SEx_s_l_arr) - (tilde_p_arr + s_s_arr)
        taker_bnd_arr = long_taker_bnd
        taker_bnd_arr[short_filter] = short_taker_bnd[short_filter]

        diag_q_tick = np.diag(q_grp_size_arr)
        diag_sqrt_T_tau = np.diag(np.sqrt(Ts - taus))

        tilde_cov_arr = diag_q_tick.dot(diag_sqrt_T_tau).dot(cov_arr).dot(diag_sqrt_T_tau).dot(diag_q_tick)

        n = len(entries)
        opt = nlopt.opt(nlopt.LD_SLSQP, n)
        obj_func = DeltaObjectiveFuncForNlopt(
            q_arr=qs,
            q_tick_arr=q_grp_size_arr,
            tilde_p_arr=tilde_p_arr,
            s_d_arr=s_d_arr,
            p_hat_arr=p_hat_arr,
            s_o_hat_arr=s_o_hat_arr,
            c_maker_arr=c_maker_arr,
            c_taker_arr=c_taker_arr,
            one_d_arr=one_d_arr,
            I_arr=Is,
            tilde_cov_arr=tilde_cov_arr,
            beta_arr=betas,
            taker_bnd_arr=taker_bnd_arr,
            lambda_arr=lambda_arr,
            epsilon_arr=epsilon_arr,
            a_d_arr=a_d_arr,
            b_d_arr=b_d_arr,
            debug=self._debug,
        )
        opt.set_max_objective(obj_func)
        opt.set_ftol_rel(1e-6)
        opt.set_ftol_abs(1e-2)
        opt.set_xtol_rel(1e-6)
        opt.set_xtol_abs(1e-2)
        opt.set_maxeval(30)
        # opt.set_maxtime()
        # nlopt.srand(0)
        delta_0_arr = (lambda_arr + epsilon_arr - b_d_arr - qs) / a_d_arr
        # opt.verbose = 1
        delta_hat_arr = opt.optimize(delta_0_arr)
        maxf = opt.last_optimum_value()
        max_U_arr, dU_arr, pnl_arr, dR_arr, pr_arr = obj_func.eval_delta(delta_arr=delta_hat_arr)
        assert abs(maxf - np.sum(max_U_arr)) < 1e-6
        bad_util = max_U_arr < 1e-6
        max_U_arr[bad_util] = 0
        delta_hat_arr[bad_util] = np.nan
        max_U = np.sum(max_U_arr)

        result_code = opt.last_optimize_result()

        if result_code < 0:
            logging.warning(f"Delta solving failed with code: {result_code}, check \n "
                            f"https://nlopt.readthedocs.io/en/latest/NLopt_Reference/#error-codes-negative-return-values")
            return np.nan, np.ones(n) * np.nan

        if result_code == 5:
            logging.warning(f"WARNING: Delta solving premature return due to max eval reached: {opt.get_maxeval()}.\n"
                            f"If you see this too often, check for systematic issues.")

        elif result_code == 6:
            logging.warning(f"Delta solving premature return due to max time reached: {opt.get_maxtime()}.\n"
                            f"If you see this too often, check for systematic issues.")

        if self._debug:
            # https://nlopt.readthedocs.io/en/latest/NLopt_Reference/#error-codes-negative-return-values
            logging.info(f"delta_hat_arr={delta_hat_arr}, maxf={maxf}, max_U={max_U}, max_U_arr={max_U_arr}, dU_arr={dU_arr}, result_code={result_code}")

        return maxf, delta_hat_arr

    def optimal_price_level(self, contracts: List[str], exs: List[str], qs: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray) -> Tuple[float, np.ndarray]:
        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)
        q_grp_size_arr = np.array([self._q_grp_size_map] * len(entries))
        int_I_arr = np.round(Is / q_grp_size_arr).astype(np.int)
        int_q_arr = np.round(qs / q_grp_size_arr).astype(np.int)
        return self._optimal_price_level(contracts=contracts, exs=exs, qs=int_q_arr, betas=betas, Ts=Ts, taus=taus, Is=int_I_arr)

    def _optimal_quantity(self, contracts: List[str], exs: List[str], deltas: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray, q0_arr: np.ndarray) -> Tuple[float, np.ndarray]:

        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)

        epsilon_arr, one_d_arr, q_tick_size_arr, lambda_arr, p_hat_arr, tilde_p_arr, s_l_arr, s_s_arr, \
        SEx_tilde_p_arr, SEx_s_l_arr, SEx_s_s_arr, s_o_hat_arr, c_maker_arr, c_taker_arr, cov_arr, a_d_arr, b_d_arr \
            = self._get_arrays(entries=entries, taus=taus)

        short_filter = ~long_filter

        s_d_arr = np.copy(s_l_arr)
        s_d_arr[short_filter] = s_s_arr[short_filter]

        long_taker_bnd = (tilde_p_arr - s_l_arr) - (SEx_tilde_p_arr + SEx_s_s_arr)
        short_taker_bnd = (SEx_tilde_p_arr - SEx_s_l_arr) - (tilde_p_arr + s_s_arr)
        taker_bnd_arr = long_taker_bnd
        taker_bnd_arr[short_filter] = short_taker_bnd[short_filter]

        diag_q_tick = np.diag(q_tick_size_arr)
        diag_sqrt_T_tau = np.diag(np.sqrt(Ts - taus))
        diag_one_d_arr = np.diag(one_d_arr)

        tilde_cov_arr = diag_q_tick.dot(diag_sqrt_T_tau).dot(cov_arr).dot(diag_sqrt_T_tau).dot(diag_q_tick)

        n = len(deltas)
        diag_beta_arr = np.diag(betas)

        no_entry_filter = np.isnan(deltas)
        p_entry_arr = tilde_p_arr - diag_one_d_arr.dot(s_d_arr) - one_d_arr * deltas
        one_arr = np.ones(n)
        one_arr[no_entry_filter] = 0
        amt_entry_arr = diag_q_tick.dot(one_arr)
        q_token_arr = amt_entry_arr / p_entry_arr
        q_token_arr[no_entry_filter] = 0
        exit_prc_mat = np.diag(p_hat_arr) - diag_one_d_arr.dot(np.diag(s_o_hat_arr))
        amt_exit_arr = exit_prc_mat.dot(q_token_arr)
        maker_filter = deltas > taker_bnd_arr
        taker_filter = ~maker_filter
        one_maker_arr = maker_filter.astype(np.int)
        one_taker_arr = taker_filter.astype(np.int)
        fee_arr = np.diag(one_maker_arr).dot(c_maker_arr) + np.diag(one_taker_arr).dot(c_taker_arr)
        pnl_arr = diag_one_d_arr.dot(amt_exit_arr - amt_entry_arr) - np.diag(fee_arr).dot(q_token_arr)

        diag_one_arr = np.diag(one_arr)
        A_U_mat = - diag_beta_arr.dot(diag_one_arr).dot(diag_one_d_arr).dot(tilde_cov_arr).dot(diag_one_d_arr)
        B_U_arr = pnl_arr - diag_beta_arr.dot(diag_one_arr).dot(diag_one_d_arr).dot(tilde_cov_arr).dot(Is + 0.5 * diag_one_d_arr.dot(one_arr))

        o = VectorizedLinearNewtonsMethod(tol=1e-6, max_iter=10, debug=self._debug)
        o.set_A_b(A=A_U_mat, b=B_U_arr)
        q0_arr[no_entry_filter] = 0
        q_hat_arr, resid, n_iter = o.eval(x=q0_arr)

        if n_iter >= o.max_iter:
            logging.warning(f"Quantity solving hit max iter: {o.max_iter}")

        if self._debug:
            logging.info(f"q_hat_arr={q_hat_arr}, resid={resid}, sum_resid={np.sum(resid)}, n_iter={n_iter}")

        dR_arr = diag_one_d_arr.dot(tilde_cov_arr).dot(Is + diag_one_d_arr.dot(q_hat_arr + 0.5 * one_arr))

        U_arr = A_U_mat.dot(q_hat_arr) + B_U_arr
        _q_arr = a_d_arr * deltas + b_d_arr + q_hat_arr
        _mu_arr = lambda_arr - epsilon_arr
        _sigma_arr = np.sqrt(lambda_arr)
        _q_z_arr = (_q_arr - _mu_arr) / _sigma_arr
        pr_arr = 1 - norm.cdf(_q_z_arr)

        U_arr[~taker_filter] *= pr_arr[~taker_filter]

        if np.any(np.isnan(U_arr)):
            raise RuntimeError(f"NaN in U_arr={U_arr}!")

        if self._debug:
            logging.info(f"pnl={pnl_arr}, dR={dR_arr}, pr={pr_arr}, U_arr={U_arr}")

        max_U = np.sum(U_arr)

        return max_U, q_hat_arr

    def optimal_quantity(self, contracts: List[str], exs: List[str], deltas: np.ndarray, betas: np.ndarray,
            Ts: np.ndarray, taus: np.ndarray, Is: np.ndarray, q0_arr: np.ndarray) -> Tuple[float, np.ndarray]:
        entries, long_filter = self._get_entries(contracts=contracts, exs=exs)
        q_grp_size_arr = np.array([self._q_grp_size_map] * len(entries))
        int_I_arr = np.round(Is / q_grp_size_arr).astype(np.int)
        int_q0_arr = np.round(q0_arr / q_grp_size_arr).astype(np.int)
        U, q = self._optimal_quantity(contracts=contracts, exs=exs, deltas=deltas, betas=betas, Ts=Ts, taus=taus, Is=int_I_arr, q0_arr=int_q0_arr)
        return U, q * q_grp_size_arr