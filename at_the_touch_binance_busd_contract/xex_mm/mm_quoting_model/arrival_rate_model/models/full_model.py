import math
from collections import deque
from typing import Dict, List, Tuple, Union

import numpy as np
import pandas as pd
from xex_mm.mm_quoting_model.arrival_rate_model.utils.fast_norm import fast_norm_cdf_vec, fast_norm_cdf
from scipy.stats import poisson
from xex_mm.utils.base import Depth, ReferencePriceCalculator
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.utils.methods import tick_size_gcd
from xex_mm.xex_depth.xex_depth import XExDepth


class SingleArrivalRateXExMmModel:
    """
    Quantity is in units of coin.
    """

    def __init__(self,
                 ref_prc_mdl_map: Dict,
                 half_sprd_mdl_map: Dict,
                 sigma_mdl_map: Dict,
                 arrival_rate_mdl_map: Dict,
                 arrival_rate_mkt_depth_decay_coef_mdl_map: Dict,
                 arrival_rate_qty_decay_coef_mdl_map: Dict,
                 fee_rate_map: Dict,
                 qty_tick_size_map: Dict,
                 ):

        self._ref_prc_model_map = ref_prc_mdl_map  # contract, ex
        self._half_sprd_model_map = half_sprd_mdl_map  # contract, ex, side
        self._sigma_model_map = sigma_mdl_map  # contract, ex
        self._arrival_rate_model_map = arrival_rate_mdl_map  # contract, ex, side
        self._arrival_rate_mkt_depth_decay_coef_mdl_map = arrival_rate_mkt_depth_decay_coef_mdl_map  # contract, ex, side
        self._arrival_rate_qty_decay_coef_mdl_map = arrival_rate_qty_decay_coef_mdl_map  # contract, ex, side
        self._fee_rate_map = fee_rate_map  # contract, ex, maker_taker
        self._qty_tick_size_map = qty_tick_size_map  # contract, ex

        self._pred_ref_prc_map = {}  # contract, ex
        self._pred_half_sprd_map = {}  # contract, ex, side
        self._pred_sigma_map = {}  # contract, ex
        self._pred_arrival_rate_map = {}  # contract, ex, side
        self._pred_arrival_rate_mkt_depth_decay_coef_map = {}  # contract, ex, side
        self._pred_arrival_rate_qty_decay_coef_map = {}  # contract, ex, side
        self._qty_tick_size_gcd_map = {}  # contract
        self._update_qty_tick_size_gcd_map()

        self._numeric_tol = 1e-10
        self._xex = "xex"


    ############################################ Params Control #################################################

    def _update_ex_contract_d_preds(self, contract: str, ex: str, d: Direction, xs: np.ndarray, mdl_map: Dict, pred_map: Dict):
        mdl = mdl_map[contract][ex][d]
        if contract not in pred_map:
            pred_map[contract] = {}
        if ex not in pred_map[contract]:
            pred_map[contract][ex] = {}
        pred_map[contract][ex][d] = mdl.predict(xs=xs)

    def _update_ex_contract_preds(self, contract: str, ex: str, xs: np.ndarray, mdl_map: Dict, pred_map: Dict):
        mdl = mdl_map[contract][ex]
        if contract not in pred_map:
            pred_map[contract] = {}
        pred_map[contract][ex] = mdl.predict(xs=xs)

    def update_pred_arrival_rate(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_model_map,
                                         pred_map=self._pred_arrival_rate_map)

    def get_pred_arrival_rate(self, contract: str, ex: str, d: Direction) -> float:
        return self._pred_arrival_rate_map[contract][ex][d]

    def update_pred_ref_prc(self, contract: str, ex: str, xs: np.ndarray):
        self._update_ex_contract_preds(contract=contract, ex=ex, xs=xs,
                                       mdl_map=self._ref_prc_model_map,
                                       pred_map=self._pred_ref_prc_map)

    def get_pred_ref_prc(self, contract: str, ex: str) -> float:
        return self._pred_ref_prc_map[contract][ex]

    def update_pred_half_spread(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._half_sprd_model_map,
                                         pred_map=self._pred_half_sprd_map)

    def get_pred_half_sprd(self, contract: str, ex: str, d: Direction):
        return self._pred_half_sprd_map[contract][ex][d]

    def update_pred_sigma(self, contract: str, ex: str, xs: np.ndarray):
        self._update_ex_contract_preds(contract=contract, ex=ex, xs=xs,
                                       mdl_map=self._sigma_model_map,
                                       pred_map=self._pred_sigma_map)

    def get_pred_sigma(self, contract: str, ex: str):
        return self._pred_sigma_map[contract][ex]

    def update_pred_arrival_rate_mkt_depth_decay_coef(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_mkt_depth_decay_coef_mdl_map,
                                         pred_map=self._pred_arrival_rate_mkt_depth_decay_coef_map)

    def get_pred_arrival_rate_mkt_depth_decay_coef(self, contract: str, ex: str, d: Direction):
        return self._pred_arrival_rate_mkt_depth_decay_coef_map[contract][ex][d]

    def update_pred_arrival_rate_qty_decay_coef(self, contract: str, ex: str, d: Direction, xs: np.ndarray):
        self._update_ex_contract_d_preds(contract=contract, ex=ex, d=d, xs=xs,
                                         mdl_map=self._arrival_rate_qty_decay_coef_mdl_map,
                                         pred_map=self._pred_arrival_rate_qty_decay_coef_map)

    def get_pred_arrival_rate_qty_decay_coef(self, contract: str, ex: str, d: Direction):
        return self._pred_arrival_rate_qty_decay_coef_map[contract][ex][d]

    def _get_fee_rate(self, contract: str, ex: str, maker_taker: MakerTaker):
        return self._fee_rate_map[contract][ex][maker_taker]

    def _update_qty_tick_size_gcd_map(self):
        for contract, xex_tick_size_map in self._qty_tick_size_map.items():
            self._qty_tick_size_gcd_map[contract] = tick_size_gcd(tick_sizes=list(xex_tick_size_map.values()))

    def _get_qty_tick_size_gcd(self, contract: str) -> float:
        return self._qty_tick_size_gcd_map[contract]

    ############################################ Computation Methods #################################################

    def _depth_to_ref_prc(self, depth: Depth):
        return ReferencePriceCalculator().calc_from_depth(depth=depth)

    def _get_arrival_rate_mkt_depth_decay_multiplier(self, contract: str, ex: str, d: Direction, p: float, ref_prc: float):
        decay_coef = self.get_pred_arrival_rate_mkt_depth_decay_coef(contract=contract, ex=ex, d=d)
        if d == Direction.long:  # bid side
            mkt_depth = np.log(ref_prc/p)
            return math.exp(-mkt_depth*decay_coef)
        if d == Direction.short:  # ask side
            mkt_depth = np.log(p/ref_prc)
            return math.exp(-mkt_depth*decay_coef)
        raise RuntimeError()

    def _get_arrival_rate_qty_decay_multiplier(self, contract: str, ex: str, d: Direction, q: int):
        decay_coef = self.get_pred_arrival_rate_qty_decay_coef(contract=contract, ex=ex, d=d)
        return math.exp(-q*decay_coef)

    def _get_lambda_d(self, contract: str, ex: str, d: Direction, p: float, q: int, ref_prc: float):
        assert ex == "xex"  # loosen this when we start using SEx arrival rates
        neg_d = Direction.long if d == Direction.short else Direction.short
        A = self.get_pred_arrival_rate(contract=contract, ex=ex, d=neg_d) / self._get_qty_tick_size_gcd(contract=contract)
        # mkt depth decay
        mkt_depth_decay_mult = self._get_arrival_rate_mkt_depth_decay_multiplier(
            contract=contract, ex=ex, d=d, p=p, ref_prc=ref_prc)
        A *= mkt_depth_decay_mult
        # qty decay
        qty_decay_mult = self._get_arrival_rate_qty_decay_multiplier(
            contract=contract, ex=ex, d=d, q=q)
        A *= qty_decay_mult
        return A

    def _fill_probability(self, contract: str, ex: str, d: Direction, p: float, q: int, Q: int, tau: float, ref_prc: float) -> float:
        lambda_d = self._get_lambda_d(contract=contract, ex=self._xex, d=d, p=p, q=q, ref_prc=ref_prc)
        return 1 - poisson.cdf(q + Q - 1, lambda_d * tau)

    def _pnl(self, contract: str, ex: str, p: float, q: int, d: Direction) -> float:
        """
        Carry time period is pinned to pred_ref_prc time span t to T.
        Pred_sprd pinned to pred_ref_prc end time T.
        """
        Id = self._get_I_d(d=d)
        pred_ref_prc = self.get_pred_ref_prc(contract=contract, ex=self._xex)
        pred_half_sprd = self.get_pred_half_sprd(contract=contract, ex=self._xex, d=d)
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        return q * q_tick_size * Id * (pred_ref_prc - Id * pred_half_sprd - p)

    def _get_unit_c_maker(self, contract: str, ex: str, p: float):
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        return p * self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.maker) * q_tick_size

    def _expected_maker_pnl(self, contract: str, ex: str, p: float, q: int, d: Direction, XEx_Q: int, tau: float, ref_prc: float) -> float:
        """ XEx Q and arrival rate. """
        unit_c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=p)
        lambda_d = self._get_lambda_d(contract=contract, ex=self._xex, d=d, p=p, q=q, ref_prc=ref_prc)
        val = 0
        unit_pnl = self._pnl(contract=contract, ex=ex, p=p, q=1, d=d)

        ########### Iterative SLOW ###########
        # for i in range(1, q):
        #     pnl = unit_pnl * i
        #     pr = poisson.pmf(XEx_Q + i, lambda_d * tau)
        #     val += pr * (pnl - c_maker * i)
        ########## Vectorized FAST ##########
        q_arr = np.array(range(1, q))
        pr_arr = poisson.pmf(XEx_Q + q_arr, lambda_d * tau)
        pnl_arr = unit_pnl * q_arr
        val_arr = pr_arr * (pnl_arr - unit_c_maker * q_arr)
        val += np.sum(val_arr)

        pnl = unit_pnl * q
        pr = (1 - poisson.cdf(XEx_Q + q - 1, lambda_d * tau))
        val += pr * (pnl - unit_c_maker * q)
        return val

    def _approx_expected_maker_pnl(self, contract: str, ex: str, p: float, q: int, d: Direction, XEx_Q: int, tau: float, ref_prc: float) -> float:
        """ XEx Q and arrival rate. """
        unit_c_maker = self._get_unit_c_maker(contract=contract, ex=ex, p=p)
        lambda_d = self._get_lambda_d(contract=contract, ex=self._xex, d=d, p=p, q=q, ref_prc=ref_prc)
        val = 0
        unit_pnl = self._pnl(contract=contract, ex=ex, p=p, q=1, d=d)

        q_arr = np.array(range(1, q))
        mu = lambda_d * tau
        loc = mu - 0.2
        scale = math.sqrt(mu)
        z_arr = (XEx_Q + q_arr - loc) / scale
        u_pr_arr = fast_norm_cdf_vec(z_arr + 0.5 / scale)
        d_pr_arr = fast_norm_cdf_vec(z_arr - 0.5 / scale)
        pr_arr = u_pr_arr - d_pr_arr
        pnl_arr = unit_pnl * q_arr
        val_arr = pr_arr * (pnl_arr - unit_c_maker * q_arr)
        val += np.sum(val_arr)

        pnl = unit_pnl * q
        z = (XEx_Q + q - 1 - loc) / scale
        pr = (1 - fast_norm_cdf(z))
        val += pr * (pnl - unit_c_maker * q)
        return val

    def _get_unit_c_taker(self, contract: str, ex: str, p: float):
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        return p * self._get_fee_rate(contract=contract, ex=ex, maker_taker=MakerTaker.taker) * q_tick_size

    def _taker_pnl(self, contract: str, ex: str, p: float, q: int, d: Direction):
        if q == 0:
            return 0
        unit_c_taker = self._get_unit_c_taker(contract=contract, ex=ex, p=p)
        pnl = self._pnl(contract=contract, ex=ex, p=p, q=q, d=d)
        taker_pnl = pnl - unit_c_taker * q
        return taker_pnl

    def __prc_geq(self, lhs: float, rhs: float) -> bool:
        return lhs > rhs - self._numeric_tol

    def __prc_leq(self, lhs: float, rhs: float) -> bool:
        return lhs < rhs + self._numeric_tol

    def __prc_eq(self, lhs: float, rhs: float) -> bool:
        return abs(lhs - rhs) < self._numeric_tol

    def _get_XEx_maker_Qs(self, int_qty_XEx_depth: Depth, ps: List[float], d: Direction) -> List[int]:
        p_q = deque(ps)  # ascending
        if d == Direction.long:
            bid_q = deque(int_qty_XEx_depth.bids)  # descending
            Q = 0
            data = []
            while len(p_q) > 0:
                if (len(bid_q) > 0) and (self.__prc_geq(bid_q[0].prc, p_q[-1])):
                    bid = bid_q.popleft()
                    Q += bid.qty
                    continue
                # bid_q[0].prc < p_q[-1]
                p_q.pop()
                data.append(Q)
            return data[::-1]

        if d == Direction.short:
            ask_q = deque(int_qty_XEx_depth.asks)  # ascending
            Q = 0
            data = []
            while len(p_q) > 0:
                if (len(ask_q) > 0) and (self.__prc_leq(ask_q[0].prc, p_q[0])):
                    ask = ask_q.popleft()
                    Q += ask.qty
                    continue
                # ask_q[0].prc < p_q[0]
                p_q.popleft()
                data.append(Q)
            return data

        raise RuntimeError()

    def _get_SEx_taker_qs_for_d(self, int_qty_SEx_depth: Depth, ps: List[float], qs: List[int], d: Direction) -> List[List[Tuple[int, int, float]]]:
        p_q = deque(ps)  # ascending
        q_q = deque(qs)  # ascending
        settled_q_map = {}

        if d == Direction.long:
            ask_q = deque(int_qty_SEx_depth.asks)  # ascending
            Q = 0
            _dummy_amt = 0
            data = []
            while len(p_q) > 0:
                if (len(ask_q) > 0) and self.__prc_leq(ask_q[0].prc, p_q[0]):
                    ask = ask_q.popleft()
                    Q += ask.qty
                    _dummy_amt += (ask.prc * ask.qty)  # q_tick_size is not necessary, as amt is not used directly
                    while (len(q_q) > 0) and (Q >= q_q[0]):
                        q = q_q.popleft()
                        avg_fill_p = (_dummy_amt - (Q - q) * ask.prc) / q
                        settled_q_map[q] = avg_fill_p
                    continue
                # ask_q[0].prc > p_q[0]
                p_q.popleft()
                row = []
                for q in qs: # ascending
                    if q in settled_q_map:
                        row.append((q, 0, settled_q_map[q]))
                        continue
                    row.append((Q, q - Q, np.nan if Q == 0 else _dummy_amt / Q))
                data.append(row)
            return data

        if d == Direction.short:
            bid_q = deque(int_qty_SEx_depth.bids)  # ascending
            Q = 0
            _dummy_amt = 0
            data = []
            while len(p_q) > 0:
                if (len(bid_q) > 0) and self.__prc_geq(bid_q[0].prc, p_q[-1]):
                    bid = bid_q.popleft()
                    Q += bid.qty
                    _dummy_amt += (bid.prc * bid.qty)
                    while (len(q_q) > 0) and (Q >= q_q[0]):
                        q = q_q.popleft()
                        avg_fill_p = (_dummy_amt - (Q - q) * bid.prc) / q
                        settled_q_map[q] = avg_fill_p
                    continue
                # bid_q[0].prc < p_q[-1]
                p_q.pop()
                row = []
                for q in qs:  # ascending
                    if q in settled_q_map:
                        row.append((q, 0, settled_q_map[q]))
                        continue
                    row.append((Q, q - Q, np.nan if Q == 0 else _dummy_amt / Q))
                data.append(row)
            return data[::-1]

        raise RuntimeError()

    def _get_I_d(self, d: Direction):
        if d == Direction.long:
            return 1
        if d == Direction.short:
            return -1
        raise RuntimeError()

    def _R(self, contract: str, ex: str, q: Union[int, np.ndarray], d: Direction, I: int, T: float, tau: float):
        Id = self._get_I_d(d)
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        tilde_p = self.get_pred_ref_prc(contract=contract, ex=self._xex)
        pred_sigma = self.get_pred_sigma(contract=contract, ex=self._xex)
        val = 0.5 * ((I + q * Id) * q_tick_size * tilde_p * pred_sigma)**2 * (T - tau)
        return val

    def _I_sgn(self, I: int, d: Direction):
        if I == 0:
            return 1
        q_d = 1 if d == Direction.long else -1
        I_d = 1 if I > 0 else -1
        return I_d * q_d

    def _dR1_d(self, contract: str, ex: str, T: float, tau: float, I: int, d: Direction):
        sgn = self._I_sgn(I=I, d=d)
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        tilde_p = self.get_pred_ref_prc(contract=contract, ex=self._xex)
        sigma = self.get_pred_sigma(contract=contract, ex=self._xex)
        return sgn * q_tick_size * 0.5 * tilde_p * sigma ** 2 * (T - tau)

    def _expected_maker_R(self, contract: str, ex: str, p: float, q: int, d: Direction, XEx_Q: int,
                          I: int, T: float, tau: float, ref_prc: float):
        lambda_d = self._get_lambda_d(contract=contract, ex=self._xex, d=d, p=p, q=q, ref_prc=ref_prc)
        val = 0
        R = self._R(contract=contract, ex=ex, q=0, d=d, I=I, T=T, tau=tau)
        mu = lambda_d * tau
        val1 = poisson.cdf(XEx_Q, mu) * R
        val += val1

        ########## Iterative SLOW #########
        # for i in range(1, q):
        #     R = self._R(contract=contract, ex=ex, q=i, d=d, I=I, T=T, tau=tau)
        #     val += poisson.pmf(XEx_Q + i, mu) * R
        ########## Vectorized FAST ##########
        q_arr = np.array(range(1, q))
        R_arr = self._R(contract=contract, ex=ex, q=q_arr, d=d, I=I, T=T, tau=tau)
        pr_arr = poisson.pmf(XEx_Q + q_arr, mu)
        val_arr = pr_arr * R_arr
        val2 = np.sum(val_arr)
        val += val2

        R = self._R(contract=contract, ex=ex, q=q, d=d, I=I, T=T, tau=tau)
        pr = poisson.cdf(XEx_Q + q - 1, mu)
        val3 = (1 - pr) * R
        val += val3
        return val

    def _expected_maker_dR(self, contract: str, ex: str, p: float, q: int, d: Direction, XEx_Q: int,
                          I: int, T: float, tau: float, ref_prc: float):
        lambda_d = self._get_lambda_d(contract=contract, ex=self._xex, d=d, p=p, q=q, ref_prc=ref_prc)
        val = 0
        mu = lambda_d * tau

        ########## Iterative SLOW #########
        # for i in range(1, q):
        #     R = self._R(contract=contract, ex=ex, q=i, d=d, I=I, T=T, tau=tau)
        #     val += poisson.pmf(XEx_Q + i, mu) * R
        ########## Vectorized FAST ##########
        q_arr = np.array(range(1, q))
        dR1 = self._dR1_d(contract=contract, ex=ex, d=d, I=I, T=T, tau=tau)
        pr_arr = poisson.pmf(XEx_Q + q_arr, mu)
        val_arr = pr_arr * dR1 * q_arr
        val2 = np.sum(val_arr)
        val += val2

        dR1 = self._dR1_d(contract=contract, ex=ex, d=d, I=I, T=T, tau=tau)
        pr = poisson.cdf(XEx_Q + q - 1, mu)
        val3 = (1 - pr) * dR1 * q
        val += val3
        return val

    def _approx_expected_maker_R(self, contract: str, ex: str, p: float, q: int, d: Direction, XEx_Q: int,
                          I: int, T: float, tau: float, ref_prc: float):
        lambda_d = self._get_lambda_d(contract=contract, ex=self._xex, d=d, p=p, q=q, ref_prc=ref_prc)
        val = 0
        R = self._R(contract=contract, ex=ex, q=0, d=d, I=I, T=T, tau=tau)
        mu = lambda_d * tau
        loc = mu - 0.2
        scale = math.sqrt(mu)
        z = (XEx_Q - loc) / scale
        pr1 = fast_norm_cdf(z)
        val1 = pr1 * R
        val += val1

        q_arr = np.array(range(1, q))
        R_arr = self._R(contract=contract, ex=ex, q=q_arr, d=d, I=I, T=T, tau=tau)
        z_arr = (XEx_Q + q_arr - loc) / scale
        u_p_arr = fast_norm_cdf_vec(z_arr + 0.5 / scale)
        d_p_arr = fast_norm_cdf_vec(z_arr - 0.5 / scale)
        pr_arr = u_p_arr - d_p_arr
        # pr2 = np.sum(pr_arr)
        val_arr = pr_arr * R_arr
        val2 = np.sum(val_arr)
        val += val2

        R = self._R(contract=contract, ex=ex, q=q, d=d, I=I, T=T, tau=tau)
        z = (XEx_Q + q - 1 - loc) / scale
        pr = fast_norm_cdf(z)
        pr3 = (1 - pr)
        val3 = pr3 * R
        val += val3
        return val

    def _taker_R(self, contract: str, ex: str, p: float, q: int, d: Direction, I: int, T: float, tau: float):
        return self._R(contract=contract, ex=ex, q=q, d=d, I=I, T=T, tau=tau)

    def SEx_utility_values(self, contract: str, ex: str, ps: List[float], qs: List[int], I: int, T: float, tau: float, beta: float, int_qty_SEx_depth: Depth, int_qty_XEx_depth: Depth) -> pd.DataFrame:
        ps.sort()  # ascending
        qs.sort()  # ascending
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        data = []
        for d in [Direction.long, Direction.short]:
            SEx_tkr_qs = self._get_SEx_taker_qs_for_d(int_qty_SEx_depth=int_qty_SEx_depth, ps=ps, qs=qs, d=d)
            XEx_mkr_qs = self._get_XEx_maker_Qs(int_qty_XEx_depth=int_qty_XEx_depth, ps=ps, d=d)
            for p, SEx_tkr_qs, XEx_mkr_Q in zip(ps, SEx_tkr_qs, XEx_mkr_qs):
                for q, (SEx_tkr_fill_q, SEx_tkr_unfill_q, avg_tkr_fill_p) in zip(qs, SEx_tkr_qs):
                    tkr_pnl =  self._taker_pnl(contract=contract, ex=ex, p=avg_tkr_fill_p, q=SEx_tkr_fill_q, d=d)
                    if SEx_tkr_unfill_q == 0:
                        exp_pnl = tkr_pnl
                        base_R = self._R(contract=contract, ex=ex, q=0, d=d, I=I, T=T, tau=tau)
                        exp_R = self._taker_R(contract=contract, ex=ex, p=p, q=q, d=d, I=I, T=T, tau=tau)
                        exp_dR = exp_R - base_R
                        util_val = exp_pnl - beta * exp_dR
                        data.append(dict(d=d, p=p, q=q * q_tick_size,
                                         tkr_pnl=tkr_pnl, exp_mkr_pnl=0, exp_pnl=exp_pnl,
                                         base_R=base_R, exp_R=exp_R, exp_dR=exp_dR, util_val=util_val))
                        continue
                    assert SEx_tkr_unfill_q > 0
                    assert SEx_tkr_fill_q >= 0
                    if SEx_tkr_fill_q > 0:
                        shadow_SEx_depth = int_qty_SEx_depth.get_shadow_depth(q=SEx_tkr_fill_q, d=d)
                        shadow_XEx_depth = int_qty_XEx_depth - int_qty_SEx_depth + shadow_SEx_depth
                        ref_prc = self._depth_to_ref_prc(depth=shadow_XEx_depth)
                    else:
                        ref_prc = self._depth_to_ref_prc(depth=int_qty_XEx_depth)
                    exp_mkr_pnl = self._expected_maker_pnl(
                        contract=contract, ex=ex, p=p, q=SEx_tkr_unfill_q, d=d, XEx_Q=XEx_mkr_Q, tau=tau, ref_prc=ref_prc)
                    exp_pnl = tkr_pnl + exp_mkr_pnl
                    base_R = self._R(contract=contract, ex=ex, q=0, d=d, I=I, T=T, tau=tau)
                    Id = self._get_I_d(d=d)
                    I_post_tkr = I + Id * SEx_tkr_fill_q
                    exp_R = self._expected_maker_R(
                        contract=contract, ex=ex, p=p, q=SEx_tkr_unfill_q, d=d, XEx_Q=XEx_mkr_Q, I=I_post_tkr, T=T, tau=tau, ref_prc=ref_prc)
                    exp_dR = exp_R - base_R
                    util_val = exp_pnl - beta * exp_dR
                    data.append(dict(d=d, p=p, q=q * q_tick_size,
                                     tkr_pnl=tkr_pnl, exp_mkr_pnl=exp_mkr_pnl, exp_pnl=exp_pnl,
                                     base_R=base_R, exp_R=exp_R, exp_dR=exp_dR, util_val=util_val))
                    continue
        return pd.DataFrame(data)

    def approx_SEx_utility_values(self, contract: str, ex: str, ps: List[float], qs: List[int], I: int, T: float, tau: float, beta: float, int_qty_SEx_depth: Depth, int_qty_XEx_depth: Depth) -> pd.DataFrame:
        ps.sort()  # ascending
        qs.sort()  # ascending
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        data = []
        for d in [Direction.long, Direction.short]:
            SEx_tkr_qs = self._get_SEx_taker_qs_for_d(int_qty_SEx_depth=int_qty_SEx_depth, ps=ps, qs=qs, d=d)
            XEx_mkr_qs = self._get_XEx_maker_Qs(int_qty_XEx_depth=int_qty_XEx_depth, ps=ps, d=d)
            for p, SEx_tkr_qs, XEx_mkr_Q in zip(ps, SEx_tkr_qs, XEx_mkr_qs):
                for q, (SEx_tkr_fill_q, SEx_tkr_unfill_q, avg_tkr_fill_p) in zip(qs, SEx_tkr_qs):
                    tkr_pnl = self._taker_pnl(contract=contract, ex=ex, p=avg_tkr_fill_p, q=SEx_tkr_fill_q, d=d)
                    if SEx_tkr_unfill_q == 0:
                        exp_pnl = tkr_pnl
                        base_R = self._R(contract=contract, ex=ex, q=0, d=d, I=I, T=T, tau=tau)
                        exp_R = self._taker_R(contract=contract, ex=ex, p=p, q=q, d=d, I=I, T=T, tau=tau)
                        exp_dR = exp_R - base_R
                        util_val = exp_pnl - beta * exp_dR
                        data.append(dict(d=d, p=p, q=q,
                                         tkr_pnl=tkr_pnl, exp_mkr_pnl=0, exp_pnl=exp_pnl,
                                         base_R=base_R, exp_R=exp_R, exp_dR=exp_dR, util_val=util_val))
                        continue
                    assert SEx_tkr_unfill_q > 0
                    assert SEx_tkr_fill_q >= 0
                    if SEx_tkr_fill_q > 0:
                        shadow_SEx_depth = int_qty_SEx_depth.get_shadow_depth(q=SEx_tkr_fill_q, d=d)
                        shadow_XEx_depth = int_qty_XEx_depth - int_qty_SEx_depth + shadow_SEx_depth
                        ref_prc = self._depth_to_ref_prc(depth=shadow_XEx_depth)
                    else:
                        ref_prc = self._depth_to_ref_prc(depth=int_qty_XEx_depth)
                    exp_mkr_pnl = self._approx_expected_maker_pnl(
                        contract=contract, ex=ex, p=p, q=SEx_tkr_unfill_q, d=d, XEx_Q=XEx_mkr_Q, tau=tau, ref_prc=ref_prc)
                    exp_pnl = tkr_pnl + exp_mkr_pnl
                    base_R = self._R(contract=contract, ex=ex, q=0, d=d, I=I, T=T, tau=tau)
                    Id = self._get_I_d(d=d)
                    I_post_tkr = I + Id * SEx_tkr_fill_q
                    exp_R = self._approx_expected_maker_R(
                        contract=contract, ex=ex, p=p, q=q, d=d, XEx_Q=XEx_mkr_Q, I=I_post_tkr, T=T, tau=tau, ref_prc=ref_prc)
                    exp_dR = exp_R - base_R
                    util_val = exp_pnl - beta * exp_dR
                    data.append(dict(d=d, p=p, q=q * q_tick_size,
                                     tkr_pnl=tkr_pnl, exp_mkr_pnl=exp_mkr_pnl, exp_pnl=exp_pnl,
                                     base_R=base_R, exp_R=exp_R, exp_dR=exp_dR, util_val=util_val))
                    continue
        return pd.DataFrame(data)

    def utility_values(self, contract: str, ps: List[float], qs: List[float], I: float, T: float, tau: float, beta: float, XEx_depth: XExDepth) -> pd.DataFrame:
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        int_qs = [int(round(q/q_tick_size)) for q in qs]
        int_I = int(round(I/q_tick_size))
        int_qty_XEx_depth = XEx_depth.get_xex_depth().get_qty_grp_view(q_grp_size=q_tick_size)
        shared_params = dict(contract=contract, ps=ps, qs=int_qs, I=int_I, T=T, tau=tau, beta=beta,
                             int_qty_XEx_depth=int_qty_XEx_depth)
        dfs = []
        for ex in XEx_depth.get_ex_names():
            int_qty_SEx_depth = XEx_depth.get_depth_for_ex(ex).get_qty_grp_view(q_grp_size=q_tick_size)
            df = self.SEx_utility_values(
                ex=ex, int_qty_SEx_depth=int_qty_SEx_depth,
                **shared_params)
            df["ex"] = ex
            dfs.append(df)
        df = pd.concat(dfs, axis=0)
        return df

    def approx_utility_values(self, contract: str, ps: List[float], qs: List[float], I: float, T: float, tau: float, beta: float, XEx_depth: XExDepth) -> pd.DataFrame:
        q_tick_size = self._get_qty_tick_size_gcd(contract=contract)
        int_qs = [int(round(q/q_tick_size)) for q in qs]
        int_I = int(round(I/q_tick_size))
        int_qty_XEx_depth = XEx_depth.get_xex_depth().get_qty_grp_view(q_grp_size=q_tick_size)
        shared_params = dict(contract=contract, ps=ps, qs=int_qs, I=int_I, T=T, tau=tau, beta=beta,
                             int_qty_XEx_depth=int_qty_XEx_depth)
        dfs = []
        for ex in XEx_depth.get_ex_names():
            int_qty_SEx_depth = XEx_depth.get_depth_for_ex(ex).get_qty_grp_view(q_grp_size=q_tick_size)
            df = self.approx_SEx_utility_values(
                ex=ex, int_qty_SEx_depth=int_qty_SEx_depth,
                **shared_params)
            df["ex"] = ex
            dfs.append(df)
        df = pd.concat(dfs, axis=0)
        return df
