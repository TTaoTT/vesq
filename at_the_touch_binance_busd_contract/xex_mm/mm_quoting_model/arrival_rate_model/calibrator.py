from functools import partial

import numpy as np
import pandas as pd
from sklearn import linear_model

from datetime import datetime, timedelta

from xex_mm.utils.calc_node import ModelCalcNode

from concurrent.futures.process import ProcessPoolExecutor

from xex_mm.utils.parallel import FunctorDistributionEngine


class LinearModelCalibrator(ModelCalcNode):
    def _get_name(self) -> str:
        return f"{self.__class__.__name__}({self._x_nodes},{self._y_nodes}).v{1}"

    def _calc(self, start_datetime: str, end_datetime:str):
        x_dfs = [x_node.get_chunks(start_datetime=start_datetime, end_datetime=end_datetime) for x_node in self._x_nodes]
        y_dfs = [y_node.get_chunks(start_datetime=start_datetime, end_datetime=end_datetime) for y_node in self._y_nodes]
        x_cols = sum([list(x_df.columns) for x_df in x_dfs], [])
        y_cols = sum([list(y_df.columns) for y_df in y_dfs], [])
        data = pd.concat(x_dfs + y_dfs, axis=1).ffill().loc[y_dfs[0].index,:]
        lms = []
        for y_col in y_cols:
            lm = linear_model.LinearRegression()
            lm.fit(X=data[x_cols], y=data[y_col])
            lms.append(lm)
        return lms
    
    def _train_ics(self, start_datetime: str, end_datetime:str):
        models = self.get(start_datetime=start_datetime,end_datetime=end_datetime)
        x_dfs = [x_node.get_chunks(start_datetime=start_datetime, end_datetime=end_datetime) for x_node in self._x_nodes]
        y_dfs = [y_node.get_chunks(start_datetime=start_datetime, end_datetime=end_datetime) for y_node in self._y_nodes]
        x_cols = sum([list(x_df.columns) for x_df in x_dfs], [])
        y_cols = sum([list(y_df.columns) for y_df in y_dfs], [])
        self.__y_cols = y_cols
        data = pd.concat(x_dfs + y_dfs, axis=1).ffill().loc[y_dfs[0].index,:]
        ics = []
        for i, y_col in enumerate(y_cols):
            lm = models[i]
            ics.append(data[y_col].corr(pd.Series(lm.predict(X=data[x_cols]))))
        return ics

    def _validation(self, cur_datetime: str, train_hours: int, validation_hours: int):
        train_start = (datetime.strptime(cur_datetime, '%Y%m%d%H') - timedelta(hours=train_hours)).strftime('%Y%m%d%H')
        validation_end = (datetime.strptime(cur_datetime, '%Y%m%d%H') + timedelta(hours=validation_hours)).strftime('%Y%m%d%H')
        models = self.get(start_datetime=train_start, end_datetime=cur_datetime)
        train_ics = self._train_ics(start_datetime=train_start, end_datetime=cur_datetime)
        x_dfs = [x_node.get_chunks(start_datetime=cur_datetime, end_datetime=validation_end) for x_node in self._x_nodes]
        y_dfs = [y_node.get_chunks(start_datetime=cur_datetime, end_datetime=validation_end) for y_node in self._y_nodes]
        x_cols = sum([list(x_df.columns) for x_df in x_dfs], [])
        y_cols = sum([list(y_df.columns) for y_df in y_dfs], [])
        data = pd.concat(x_dfs + y_dfs, axis=1).ffill().loc[y_dfs[0].index,:]
        validation_ics = []
        for i, y_col in enumerate(y_cols):
            lm = models[i]
            validation_ics.append(data[y_col].corr(pd.Series(lm.predict(X=data[x_cols]))))
        return train_ics, validation_ics
    
    def evaluation(self, start_datetime: str, end_datetime: str, train_hours: int, validation_hours: int):
        def validation_helper(data):
            return self._validation(data[0],data[1],data[2])
        cur_datetime = (datetime.strptime(start_datetime, '%Y%m%d%H') + timedelta(hours=train_hours))
        stop_datetime = (datetime.strptime(end_datetime, '%Y%m%d%H') - timedelta(hours=validation_hours))
        time_period = []
        ics = dict()
        while cur_datetime <= stop_datetime:
            time_period.append([cur_datetime.strftime('%Y%m%d%H'),train_hours,validation_hours])
            cur_datetime += timedelta(hours=1)
        pool = ProcessPoolExecutor()
        results = pool.map(validation_helper, time_period)
        pool.shutdown(wait=True)
        for result in results:
            for i, y_col in enumerate(self.__y_cols):
                if not ics.get(y_col):
                    ics[y_col] = dict()
                    ics[y_col]["train"] = []
                    ics[y_col]["validation"] = []
                ics[y_col]["train"].append(result[0][i])
                ics[y_col]["validation"].append(result[1][i])
        return ics


def run_ar_correlation():
    from xex_mm.signals.factors.past_arrival_rate import SinglePastArrivalRateNode
    from xex_mm.signals.labels.forward_arrival_rate import SingleForwardArrivalRateNode
    import matplotlib.pyplot as plt

    ar_t = 100
    fwd_t = 200

    x_node = SinglePastArrivalRateNode(ex="binance", contract="btc_usdt", t=ar_t)
    y_node = SingleForwardArrivalRateNode(contract="btc_usdt", ex="binance", ar_t=ar_t, fwd_t=fwd_t)

    start_ts = pd.Timestamp("20220721 11")
    end_ts = pd.Timestamp("20220722 11")
    ts = start_ts
    datetimes = []
    while ts <= end_ts:
        datetimes.append(ts.strftime("%Y%m%d%H"))
        ts += pd.Timedelta("1h")

    n_procs = 20
    fde = FunctorDistributionEngine(n_procs=n_procs)
    x_ftors = [partial(x_node.get, datetime=datetime, refresh=False) for datetime in datetimes]
    logging.info(f"Running x functors...")
    x_dfs = fde.run(x_ftors)
    for x_df in x_dfs:
        assert x_df.index.is_unique
    logging.info(f"Running y functors...")
    y_ftors = [partial(y_node.get, datetime=datetime, refresh=False) for datetime in datetimes]
    y_dfs = fde.run(y_ftors)
    for y_df in y_dfs:
        assert y_df.index.is_unique

    x_df = pd.concat(x_dfs, axis=0)
    assert x_df.index.is_unique
    y_df = pd.concat(y_dfs, axis=0)
    assert y_df.index.is_unique

    _y_df = pd.DataFrame(data=x_df.values, index=x_df.index - fwd_t, columns=x_df.columns)
    assert y_df.shape == _y_df.shape
    assert np.all(y_df.index == _y_df.index)
    assert not np.any((y_df - x_df.values).abs() > 1e-6)
    assert not np.any((y_df - _y_df.values).abs() > 1e-6)

    assert x_df.index[0] > y_df.index[0]
    assert x_df.index[-1] > y_df.index[-1]
    df = pd.concat([x_df, y_df], axis=1).ffill().loc[x_df.index[x_df.index <= y_df.index[-1]], :]
    print(f"{df.describe()}")

    plt.scatter(df["lar"], df["fwd_lar"])
    plt.xlabel("lar")
    plt.ylabel("fwd_lar")
    plt.title("lar")
    plt.show()

    plt.scatter(df["sar"], df["fwd_sar"])
    plt.xlabel("sar")
    plt.ylabel("fwd_sar")
    plt.title("sar")
    plt.show()


def run_ar_ret_correlation():
    from xex_mm.signals.factors.past_arrival_rate import SinglePastArrivalRateReturnNode
    from xex_mm.signals.labels.forward_arrival_rate import SingleForwardArrivalRateReturnNode
    import matplotlib.pyplot as plt

    ar_t = 100
    fwd_t = 2 * ar_t

    x_node = SinglePastArrivalRateReturnNode(ex="binance", contract="btc_usdt", ar_t=ar_t, fwd_t=fwd_t)
    y_node = SingleForwardArrivalRateReturnNode(contract="btc_usdt", ex="binance", ar_t=ar_t, fwd_t=fwd_t)

    start_ts = pd.Timestamp("20220721 11")
    end_ts = pd.Timestamp("20220722 11")
    ts = start_ts
    datetimes = []
    while ts <= end_ts:
        datetimes.append(ts.strftime("%Y%m%d%H"))
        ts += pd.Timedelta("1h")

    n_procs = 20
    fde = FunctorDistributionEngine(n_procs=n_procs)
    x_ftors = [partial(x_node.get, datetime=datetime, refresh=False) for datetime in datetimes]
    logging.info(f"Running x functors...")
    x_dfs = fde.run(x_ftors)
    for x_df in x_dfs:
        assert x_df.index.is_unique
    logging.info(f"Running y functors...")
    y_ftors = [partial(y_node.get, datetime=datetime, refresh=False) for datetime in datetimes]
    y_dfs = fde.run(y_ftors)
    for y_df in y_dfs:
        assert y_df.index.is_unique

    x_df = pd.concat(x_dfs, axis=0)
    assert x_df.index.is_unique
    y_df = pd.concat(y_dfs, axis=0)
    assert y_df.index.is_unique

    _y_df = pd.DataFrame(data=x_df.values, index=x_df.index - fwd_t, columns=x_df.columns)
    assert y_df.shape == _y_df.shape
    assert np.all(y_df.index == _y_df.index)
    assert not np.any((y_df - x_df.values).abs() > 1e-6)
    assert not np.any((y_df - _y_df.values).abs() > 1e-6)

    assert x_df.index[0] > y_df.index[0]
    assert x_df.index[-1] > y_df.index[-1]
    df = pd.concat([x_df, y_df], axis=1).ffill().loc[x_df.index[x_df.index <= y_df.index[-1]], :]
    print(f"{df.describe()}")

    plt.scatter(df["lar_ret"], df["fwd_lar_ret"])
    plt.xlabel("lar_ret")
    plt.ylabel("fwd_lar_ret")
    plt.title("lar_ret")
    plt.show()

    plt.scatter(df["sar_ret"], df["fwd_sar_ret"])
    plt.xlabel("sar_ret")
    plt.ylabel("fwd_sar_ret")
    plt.title("sar_ret")
    plt.show()


def run_test():
    from xex_mm.signals.factors.past_arrival_rate import SinglePastArrivalRateNode
    from xex_mm.signals.labels.forward_arrival_rate import SingleForwardArrivalRateNode

    ex = "binance"
    contract = "btc_usdt"
    ar_t = 100
    fwd_t = 200

    Xs = [SinglePastArrivalRateNode(contract=contract, ex=ex, t=ar_t)]
    ys = [SingleForwardArrivalRateNode(contract=contract, ex=ex, ar_t=ar_t, fwd_t=fwd_t)]
    o = LinearModelCalibrator(x_nodes=Xs, y_nodes=ys)
    df = o.evaluation(start_datetime="2022072108", end_datetime="2022072112", train_hours=2, validation_hours=1)
    print(df)


if __name__ == '__main__':
    import logging
    logging.getLogger().setLevel(logging.INFO)
    run_ar_correlation()
    # run_ar_ret_correlation()