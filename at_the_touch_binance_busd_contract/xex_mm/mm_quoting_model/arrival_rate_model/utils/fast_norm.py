
import math
import numpy as np


def fast_norm_cdf_vec(x):
    return 0.5 + np.sign(x) * 0.5 * np.sqrt(1 - np.exp(-5 / 8 * x ** 2))
    # return 0.5 - 1.136 * np.tanh(-0.2965 * x) + 2.47 * np.tanh(0.5416 * x) - 3.013 * np.tanh(0.4134 - x)


def fast_norm_cdf(x):
    if x >= 0:
        return 0.5 + 0.5 * math.sqrt(1 - math.exp(-5/8*x**2))
    return 1 - fast_norm_cdf(-x)
    # return 0.5 - 1.136 * math.tanh(-0.2965 * x) + 2.47 * math.tanh(0.5416 * x) - 3.013 * math.tanh(0.4134 - x)


def fast_norm_cdf_inv(p):
    if p >= 0.5:
        return math.sqrt(-8/5 * math.log(1 - (2*p - 1) ** 2))
    return -math.sqrt(-8/5 * math.log(1 - (1 - 2*p) ** 2))


if __name__ == '__main__':
    print(fast_norm_cdf(fast_norm_cdf_inv(0.2)))