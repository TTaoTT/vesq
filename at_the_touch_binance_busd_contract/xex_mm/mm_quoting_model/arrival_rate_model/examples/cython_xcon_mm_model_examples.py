import datetime

import pandas as pd
from typing import Dict

import numpy as np

from xex_mm.utils.base import Depth
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.mm_quoting_model.arrival_rate_model.models.xcon_mm_model import PyXConMmModel


class DummyRefPriceModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local ref_prc


class DummySpreadModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local spread


class DummyCovModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local covariance


class DummyArrivalRateModel:
    """ USDT (CCY2) numeraire """
    def predict(self, xs: np.ndarray):
        return xs[0]  # local arrival rate


class DummyArrivalRateMktDepthDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


class DummyArrivalRateQtyDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


BETA = 100
SIGMA = 0.00001


class XConModelScenarioTest:
    def __init__(self):
        self.contracts = ["contract"]
        self.exs_to_trade = ["mexc", "binance"]
        self.nominal_exs = self.exs_to_trade + ["xex"]
        self._xex_depth_map: Dict[str, XExDepth] = {}
        self._initialize_depths()
        self._setup_mm_model()

    def _initialize_depths(self):
        xex_depth = XExDepth()
        contract = "contract"
        ccy1 = "dummy_ccy1"
        ccy2 = "dummy_ccy2"

        depth_dict = {
            "asks": [[10000, 0.01], [10100, 0.01], [10200, 0.01]],
            "bids": [[9800, 0.01], [9700, 0.01], [9600, 0.01]],
            "resp_ts": 1,
            "server_ts": 1,
        }
        ex_name = "mexc"
        xex_depth.update_depth_for_ex(depth=Depth.from_dict(contract=contract, ccy1=ccy1, ccy2=ccy2, ex=ex_name, depth=depth_dict))

        depth_dict = {
            "asks": [[9900, 0.010], [10000, 0.010], [10100, 0.010]],
            "bids": [[9700, 0.010], [9600, 0.010], [9500, 0.010]],
            "resp_ts": 2,
            "server_ts": 2,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(depth=Depth.from_dict(contract=contract, ccy1=ccy1, ccy2=ccy2, ex=ex_name, depth=depth_dict))

        depth_dict = {
            "asks": [[9600, 0.010], [9700, 0.010], [9800, 0.010]],
            "bids": [[9500, 0.010], [9400, 0.020], [9300, 0.010]],
            "resp_ts": 3,
            "server_ts": 3,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(depth=Depth.from_dict(contract=contract, ccy1=ccy1, ccy2=ccy2, ex=ex_name, depth=depth_dict))

        print(xex_depth)
        self._xex_depth_map["contract"] = xex_depth

    def _setup_mm_model(self):
        self._q_grp_size_map = dict(
            contract=10
        )
        self._mm_mdl = PyXConMmModel(
            ref_prc_mdl_map=dict(
                contract={ex: DummyRefPriceModel() for ex in self.nominal_exs}
            ),
            half_sprd_mdl_map=dict(
                contract={ex: {
                        Direction.long: DummySpreadModel(),
                        Direction.short: DummySpreadModel(),
                    } for ex in self.nominal_exs}
            ),
            cov_mdl_map={
                PyXConMmModel.get_cov_key(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2): DummyCovModel()
                for i, contract1 in enumerate(self.contracts) for j, ex1 in enumerate(self.nominal_exs)
                for contract2 in self.contracts[i:] for ex2 in self.nominal_exs[j:]
            },
            arrival_rate_mdl_map=dict(
                contract={ex: {
                        Direction.long: DummyArrivalRateModel(),
                        Direction.short: DummyArrivalRateModel(),
                    } for ex in self.nominal_exs}
            ),
            fee_rate_map=dict(
                contract=dict(
                    mexc={
                        MakerTaker.maker: -0.0001,
                        MakerTaker.taker: 0.0001,
                    },
                    binance={
                        MakerTaker.maker: -0.0002,
                        MakerTaker.taker: 0.0003,
                    },
                ),
            ),
            q_grp_size_map=self._q_grp_size_map,
            debug=True,
        )

    def _setup_base_scenario(self):
        for contract in self.contracts:
            xex_depth_obj = self._xex_depth_map[contract]
            for ex in self.nominal_exs:
                self._mm_mdl.update_pred_arrival_rate(
                    contract=contract, ex=ex, d=Direction.long, xs=np.array([10]))  # USDT (CCY2) numéraire
                self._mm_mdl.update_pred_arrival_rate(
                    contract=contract, ex=ex, d=Direction.short, xs=np.array([10]))  # USDT (CCY2) numéraire
                self._mm_mdl.update_pred_half_spread(
                    contract=contract, ex=ex, d=Direction.long, xs=np.array([100]))
                self._mm_mdl.update_pred_half_spread(
                    contract=contract, ex=ex, d=Direction.short, xs=np.array([100]))
                ref_prc = 9700
                self._mm_mdl.update_pred_ref_prc(
                    contract=contract, ex=ex, d=Direction.long, xs=np.array([ref_prc]))
                self._mm_mdl.update_pred_ref_prc(
                    contract=contract, ex=ex, d=Direction.short, xs=np.array([ref_prc]))
                self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
                    contract=contract, ex=ex, d=Direction.long, xs=np.array([0.1]))
                self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
                    contract=contract, ex=ex, d=Direction.short, xs=np.array([0.1]))
                self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
                    contract=contract, ex=ex, d=Direction.long, xs=np.array([0.025]))
                self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
                    contract=contract, ex=ex, d=Direction.short, xs=np.array([0.025]))
                if ex == "xex":
                    depth = xex_depth_obj.get_xex_depth()
                else:
                    depth = xex_depth_obj.get_depth_for_ex(ex=ex)
                self._mm_mdl.update_depth_metrics(contract=contract, ex=ex, depth=depth.get_ccy2_view(),
                                                  amt_bnd=self._mm_mdl._q_grp_size_map)

        """ Cov Metrics """
        entries, long_filter = self._mm_mdl._get_entries(contracts=self.contracts, exs=self.exs_to_trade)
        n = len(entries)
        for i, (contract, ex, d) in enumerate(entries):
            xs = np.zeros(n)
            var = SIGMA**2
            xs[i] = var
            if d == Direction.long:
                xs[i+1] = -var
            if d == Direction.short:
                xs[i-1] = -var
            self._mm_mdl.update_pred_cov(contract=contract, ex=ex, d=d, xs=xs)

    def test_update_pred_arrival_rate(self):
        for contract in self.contracts:
            for ex in self.nominal_exs:
                for d in [Direction.long, Direction.short]:
                    x = np.random.randn()
                    self._mm_mdl.update_pred_arrival_rate(contract=contract, ex=ex, d=d, xs=np.array([x]))
                    x_out = self._mm_mdl.get_pred_arrival_rate(contract=contract, ex=ex, d=d)
                    assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"

    def test_update_pred_ref_prc(self):
        for contract in self.contracts:
            for ex in self.nominal_exs:
                x = np.random.randn()
                self._mm_mdl.update_pred_ref_prc(contract=contract, ex=ex, xs=np.array([x]))
                x_out = self._mm_mdl.get_pred_ref_prc(contract=contract, ex=ex)
                assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"

    def test_update_pred_half_sprd(self):
        for contract in self.contracts:
            for ex in self.nominal_exs:
                for d in [Direction.long, Direction.short]:
                    x = np.random.randn()
                    self._mm_mdl.update_pred_half_sprd(contract=contract, ex=ex, d=d, xs=np.array([x]))
                    x_out = self._mm_mdl.get_pred_half_sprd(contract=contract, ex=ex, d=d)
                    assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"

    def test_update_pred_cov(self):
        for i, contract1 in enumerate(self.contracts):
            for j, ex1 in enumerate(self.nominal_exs):
                for contract2 in self.contracts[i:]:
                    for ex2 in self.nominal_exs[j:]:
                        x = np.random.randn()
                        self._mm_mdl.update_pred_cov(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2, xs=np.array([x]))
                        x_out = self._mm_mdl.get_pred_cov(contract1=contract1, ex1=ex1, contract2=contract2, ex2=ex2)
                        assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"

    def test_update_fee_rate(self):
        for contract in self.contracts:
            for ex in self.nominal_exs:
                for mt in [MakerTaker.maker, MakerTaker.taker]:
                    x = np.random.randn()
                    self._mm_mdl.update_fee_rate(contract=contract, ex=ex, maker_taker=mt, val=x)
                    x_out = self._mm_mdl.get_fee_rate(contract=contract, ex=ex, maker_taker=mt)
                    assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"

    def test_update_q_grp_size(self):
        for contract in self.contracts:
            x = self._q_grp_size_map[contract]
            x_out = self._mm_mdl.get_q_grp_size(contract=contract)
            assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"
            self._mm_mdl.update_q_grp_size(contract, x)
            try:
                failed = False
                self._mm_mdl.update_q_grp_size(contract, x + 1)
            except:
                logging.info(f"Failed as expected.")
                failed = True
            finally:
                assert failed

    def test_update_depth_metrics(self):
        for contract in self.contracts:
            xex_depth = self._xex_depth_map[contract]
            for ex in xex_depth.get_ex_names():
                depth = xex_depth.get_depth_for_ex(ex=ex)
                self._mm_mdl.update_depth_metrics(contract=contract, ex=ex, depth=depth, amt_bnd=1)
                metrics = ["tilde_p", "s_l", "s_s", "a_l", "b_l", "a_s", "b_s"]
                metrics_dict = {}
                for m in metrics:
                    x_out = self._mm_mdl.get_depth_metrics(contract=contract, ex=ex, metric=m)
                    metrics_dict[m] = x_out
                    # assert abs(x_out - x) < 1e-6, f"x_out={x_out}, x={x}"
                print(metrics_dict)

    def run(self):
        self._setup_base_scenario()

        """ USDT (CCY2) numeraire """
        q = 100  # USDT (CCY2) numéraire
        T = 1000
        tau = 100
        """ USDT (CCY2) numeraire """
        I = 0  # USDT (CCY2) numéraire

        entries, long_filter = self._mm_mdl._get_entries(contracts=self.contracts, exs=self.exs_to_trade)
        tilde_p_arr = []
        s_l_arr = []
        s_s_arr = []
        q_arr = []
        beta_arr = []
        T_arr = []
        tau_arr = []
        I_arr = []
        for i, (contract, ex, d) in enumerate(entries):
            tilde_p_arr.append(self._mm_mdl.get_depth_metrics(contract=contract, ex=ex, metric="tilde_p"))
            s_l_arr.append(self._mm_mdl.get_depth_metrics(contract=contract, ex=ex, metric="s_l"))
            s_s_arr.append(self._mm_mdl.get_depth_metrics(contract=contract, ex=ex, metric="s_s"))
            q_arr.append(q)
            beta_arr.append(BETA * 10)
            T_arr.append(T)
            tau_arr.append(tau)
            I_arr.append(I)
        tilde_p_arr = np.array(tilde_p_arr)
        s_l_arr = np.array(s_l_arr)
        s_s_arr = np.array(s_s_arr)
        q_arr = np.array(q_arr)
        beta_arr = np.array(beta_arr)
        T_arr = np.array(T_arr)
        tau_arr = np.array(tau_arr)
        I_arr = np.array(I_arr)

        """ Compute delta """

        max_U_delta, delta_hat_arr = self._mm_mdl.optimal_price_level(contracts=self.contracts, exs=self.exs_to_trade, qs=q_arr, betas=beta_arr, Ts=T_arr, taus=tau_arr, Is=I_arr)
        p_hat = np.copy(tilde_p_arr)
        p_hat[long_filter] -= (s_l_arr[long_filter] + delta_hat_arr[long_filter])
        p_hat[~long_filter] += (s_s_arr[~long_filter] + delta_hat_arr[~long_filter])

        """ Compute q """
        max_U_q, q_hat_arr = self._mm_mdl.optimal_quantity(contracts=self.contracts, exs=self.exs_to_trade, deltas=delta_hat_arr, betas=beta_arr, Ts=T_arr, taus=tau_arr, Is=I_arr, q0_arr=q_arr)

        data = []
        for (contract, ex, d), p_hat, q_hat in zip(entries, p_hat, q_hat_arr):
            data.append((contract, ex, d, p_hat, q_hat))
        columns = ["contract", "ex", "d", "p_hat", "q_hat"]
        data = pd.DataFrame(data=data, columns=columns)

        if self._mm_mdl._debug:
            logging.info(f"max_U_delta={max_U_delta}, max_U_q={max_U_q}")

        return data

    def timed_run(self, run_func):
        n = 100
        start_time = datetime.datetime.now()
        for i in range(n):
            data = run_func()
        end_time = datetime.datetime.now()
        ellapsed = end_time - start_time
        print(f"ellapsed: {ellapsed}, per iter: {ellapsed / n}, n: {n}")


if __name__ == '__main__':
    import logging
    logging.getLogger().setLevel(logging.INFO)
    test_obj = XConModelScenarioTest()

    test_obj.test_update_pred_arrival_rate()
    test_obj.test_update_pred_ref_prc()
    test_obj.test_update_pred_half_sprd()
    test_obj.test_update_pred_cov()
    test_obj.test_update_fee_rate()
    test_obj.test_update_q_grp_size()
    test_obj.test_update_depth_metrics()

    # print(test_obj.run())
    # test_obj.timed_run(test_obj.run)
