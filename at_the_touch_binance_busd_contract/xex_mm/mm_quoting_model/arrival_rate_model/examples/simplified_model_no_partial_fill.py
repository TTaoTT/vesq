import datetime
import pandas as pd
from typing import Dict

import numpy as np

from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model_no_partial_fill import SingleArrivalRateXExMmModelNoPartialFill
from xex_mm.utils.base import Depth
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.xex_depth.xex_depth import XExDepth


class DummyRefPriceModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local ref_prc


class DummySpreadModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local spread


class DummySigmaModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local volatility


class DummyArrivalRateModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local arrival rate


class DummyArrivalRateMktDepthDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


class DummyArrivalRateQtyDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


BETA = 1e-2
SIGMA = 0.0001


class SingleArrivalRateMmSimplifiedModelNoParitalFillScenarioTest:
    def __init__(self):
        self.contract = "contract"
        self.I = 0
        self.T = 1000
        self.tau = 100
        self._xex_depth_map: Dict[str, XExDepth] = {}
        self._initialize_depths()
        self._setup_mm_model()

    def _initialize_depths(self):
        xex_depth = XExDepth()
        ccy1 = "dummy_ccy1"
        ccy2 = "dummy_ccy2"

        depth_dict = {
            "asks": [[10000, 0.01], [10100, 0.01], [10200, 0.01]],
            "bids": [[9800, 0.01], [9700, 0.01], [9600, 0.01]],
            "resp_ts": 1,
            "server_ts": 1,
        }
        ex_name = "mexc"
        xex_depth.update_depth_for_ex(depth=Depth.from_dict(depth=depth_dict, contract=self.contract, ccy1=ccy1, ccy2=ccy2, ex=ex_name))

        depth_dict = {
            "asks": [[9900, 0.010], [10000, 0.010], [10100, 0.010]],
            "bids": [[9700, 0.010], [9600, 0.010], [9500, 0.010]],
            "resp_ts": 2,
            "server_ts": 2,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(depth=Depth.from_dict(depth=depth_dict, contract=self.contract, ccy1=ccy1, ccy2=ccy2, ex=ex_name))

        depth_dict = {
            "asks": [[9600, 0.010], [9700, 0.010], [9800, 0.010]],
            "bids": [[9500, 0.010], [9400, 0.020], [9300, 0.010]],
            "resp_ts": 3,
            "server_ts": 3,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(depth=Depth.from_dict(depth=depth_dict, contract=self.contract, ccy1=ccy1, ccy2=ccy2, ex=ex_name))

        print(xex_depth)
        self._xex_depth_map["contract"] = xex_depth

    def _setup_mm_model(self):
        self._mm_mdl = SingleArrivalRateXExMmModelNoPartialFill(
            ref_prc_mdl_map=dict(
                contract=dict(
                    xex=DummyRefPriceModel(),
                )
            ),
            half_sprd_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummySpreadModel(),
                        Direction.short: DummySpreadModel(),
                    }
                )
            ),
            sigma_mdl_map=dict(
                contract=dict(
                    xex=DummySigmaModel(),
                )
            ),
            arrival_rate_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateModel(),
                        Direction.short: DummyArrivalRateModel(),
                    },
                ),
            ),
            fee_rate_map=dict(
                contract=dict(
                    mexc={
                        MakerTaker.maker: -0.0001,
                        MakerTaker.taker: 0.0001,
                    },
                    binance={
                        MakerTaker.maker: -0.0002,
                        MakerTaker.taker: 0.0003,
                    },
                ),
            ),
            arrival_rate_mkt_depth_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateMktDepthDecayCoefModel(),
                        Direction.short: DummyArrivalRateMktDepthDecayCoefModel(),
                    }
                )
            ),
            arrival_rate_qty_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateQtyDecayCoefModel(),
                        Direction.short: DummyArrivalRateQtyDecayCoefModel(),
                    }
                )
            ),
            q_grp_size_ccy2_map=dict(
                contract=10,  # usdt numeraire
            ),
        )

    def _setup_base(self):
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.001]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.001]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([10]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))
        amt_bnd = self._mm_mdl.get_q_grp_size_ccy2(contract=self.contract)  # used interchangably here, but doesn't have to be
        self._mm_mdl.update_depth(
            contract=self.contract, ex="xex", depth=self._xex_depth_map["contract"].get_xex_depth(), amt_bnd=amt_bnd)
        for ex in ["mexc", "binance"]:
            self._mm_mdl.update_depth(
                contract=self.contract, ex=ex,
                depth=self._xex_depth_map["contract"].get_depth_for_ex(ex=ex), amt_bnd=amt_bnd)
        self.I = 1000 * self._mm_mdl._get_q_grp_size_ccy1(contract=self.contract)

    def run_base(self):
        self._setup_base()

        self._mm_mdl.update_ar_return_space_lower_bound(contract=self.contract, val=0.001)
        self._mm_mdl.update_ar_return_space_upper_bound(contract=self.contract, val=0.03)

        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat, taker_maker_flag = self._mm_mdl.optimal_price_level_q(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                T=self.T, tau=self.tau, beta=BETA)
                U_q, q_hat = self._mm_mdl.optimal_quantity(contract=self.contract, ex=ex, d=d, I=self.I, delta=delta_hat,
                                                       T=self.T, tau=self.tau, beta=BETA)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append((ex, d, U_p, p_hat, U_q, q_hat))
        return data

    def run_optimal_qty2(self):
        self._setup_base()

        self._mm_mdl.update_ar_return_space_lower_bound(contract=self.contract, val=0.001)
        self._mm_mdl.update_ar_return_space_upper_bound(contract=self.contract, val=0.03)

        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat, taker_maker_flag = self._mm_mdl.optimal_price_level_q(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                T=self.T, tau=self.tau, beta=BETA)
                U_q, q_hat = self._mm_mdl.optimal_quantity2(contract=self.contract, ex=ex, d=d, I=self.I, delta=delta_hat,
                                                            T=self.T, tau=self.tau, q_beta=BETA)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append((ex, d, U_p, p_hat, U_q, q_hat))
        return data

    def _setup_skewed_arrival_rate(self):
        self.I = 0
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.075]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([10]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))
        amt_bnd = self._mm_mdl.get_q_grp_size_ccy2(contract=self.contract)
        self._mm_mdl.update_depth(
            contract=self.contract, ex="xex", depth=self._xex_depth_map["contract"].get_xex_depth(), amt_bnd=amt_bnd)
        for ex in ["mexc", "binance"]:
            self._mm_mdl.update_depth(
                contract=self.contract, ex=ex,
                depth=self._xex_depth_map["contract"].get_depth_for_ex(ex=ex), amt_bnd=amt_bnd)

    def run_q1_skewed_arrival_rate(self):
        self._setup_skewed_arrival_rate()

        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta = self._mm_mdl.optimal_price_level_q1(
                    contract=self.contract,
                    ex=ex,
                    d=d,
                    I=self.I, T=self.T, tau=self.tau, beta=BETA,
                    floor = -np.inf, ceil = np.inf
                )
                U_q, q = self._mm_mdl.optimal_quantity(
                    contract=self.contract,
                    ex=ex,
                    delta=delta,
                    d=d,
                    I=self.I, T=self.T, tau=self.tau, beta=BETA,
                    floor=-np.inf, ceil=np.inf
                )
                p = tilde_p - s_l - delta if d == Direction.long else tilde_p + s_s + delta
                data.append((ex, d, U_p, p, U_q, q))
        return data

    def run_q_skewed_arrival_rate(self):
        self._setup_skewed_arrival_rate()

        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat, taker_maker_flag = self._mm_mdl.optimal_price_level_q(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                T=self.T, tau=self.tau, beta=BETA,
                                                                    floor = -np.inf, ceil = np.inf)
                U_q, q_hat = self._mm_mdl.optimal_quantity(contract=self.contract, ex=ex, d=d, I=self.I, delta=delta_hat,
                                                       T=self.T, tau=self.tau, beta=BETA,
                                                           floor = -np.inf, ceil = np.inf)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append((ex, d, U_p, p_hat, U_q, q_hat))
        return data

    def _setup_inventory_solvency(self):
        self.I = 10
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([10]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))
        amt_bnd = self._mm_mdl.get_q_grp_size_ccy2(contract=self.contract)
        self._mm_mdl.update_depth(
            contract=self.contract, ex="xex", depth=self._xex_depth_map["contract"].get_xex_depth(), amt_bnd=amt_bnd)
        for ex in ["mexc", "binance"]:
            self._mm_mdl.update_depth(
                contract=self.contract, ex=ex,
                depth=self._xex_depth_map["contract"].get_depth_for_ex(ex=ex), amt_bnd=amt_bnd)

    def run_q_inventory_solvency(self):
        self._setup_inventory_solvency()

        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat, taker_maker_flag = self._mm_mdl.optimal_price_level_q(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                    T=self.T, tau=self.tau, beta=BETA,
                                                                    floor = -np.inf, ceil = np.inf)
                U_q, q_hat = self._mm_mdl.optimal_quantity(contract=self.contract, ex=ex, d=d, I=self.I,
                                                           delta=delta_hat,
                                                           T=self.T, tau=self.tau, beta=BETA,
                                                           floor = -np.inf, ceil = np.inf)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append((ex, d, U_p, p_hat, U_q, q_hat))
        return data

    def timed_run(self, run_func):
        n = 1000
        start_time = datetime.datetime.now()
        for i in range(n):
            data = run_func()
        end_time = datetime.datetime.now()
        ellapsed = end_time - start_time
        print(f"ellapsed: {ellapsed}, per iter: {ellapsed / n}, n: {n}")


if __name__ == '__main__':
    test_obj = SingleArrivalRateMmSimplifiedModelNoParitalFillScenarioTest()
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    for i in range(10000):
        pd.DataFrame(test_obj.run_base())
        pd.DataFrame(test_obj.run_optimal_qty2())
