
import nlopt
from numpy import *

def obj_func(x, grad):
    a = 1.
    b = 1.
    c = 1.
    if grad.size > 0:
        grad[:] = random.randn(grad.size)
    return sum(a * x ** 2 + b * x + c)


if __name__ == '__main__':
    n = 3
    opt = nlopt.opt(nlopt.LD_SLSQP, n)
    opt.set_max_objective(obj_func)
    opt.set_maxeval(100)
    x0 = zeros(n).astype(double)
    print(opt.optimize(x0))

