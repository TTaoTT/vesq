import logging
import os
import traceback
from collections import deque
from pathlib import Path
from typing import Dict, Deque

import pandas as pd
import json

from xex_mm.utils.base import Depth, Trade

DEPTH_LEVEL = 20
DATA_ROOT = f"{Path().home()}/user_storage/TT/calibration_data"

class DataLoader:
    def _row_dict_to_depth(self, row_dict: Dict, contract: str, ccy1: str, ccy2: str, ex: str) -> Depth:
        tmp_dict = dict()
        asks = []
        bids = []
        for i in range(DEPTH_LEVEL):
            asks.append([row_dict[f"ap{i+1}"], row_dict[f"av{i+1}"]])
            bids.append([row_dict[f"bp{i+1}"], row_dict[f"bv{i+1}"]])
        tmp_dict["asks"] = asks
        tmp_dict["bids"] = bids
        tmp_dict["resp_ts"] = row_dict["localtime"]
        tmp_dict["server_ts"] = row_dict["localtime"]
        depth = Depth.from_dict(depth=tmp_dict, contract=contract, ccy1=ccy1, ccy2=ccy2, ex=ex)
        depth.mp = row_dict["mp"]
        return depth

    def get_depth_df(self, ex: str, pair: str, datetime: str) -> pd.DataFrame:
        path = f"{DATA_ROOT}/tmp_depth_{ex}.{pair}_{datetime}.csv"
        try:
            df = pd.read_csv(path)
        except Exception as e:
            traceback.print_stack()
            if os.path.exists(path):
                os.remove(path)
                logging.warning(f"Removed bad file: {path}, please rerun.")
            raise e

        ask_qty = pd.Series()
        ask_amt = pd.Series()
        bid_qty = pd.Series()
        bid_amt = pd.Series()
        for i in range(DEPTH_LEVEL):
            ask_qty += df[f"av{i+1}"]
            bid_qty += df[f"bv{i+1}"]
            ask_amt += df[f"ap{i+1}"] * df[f"av{i+1}"]
            bid_amt += df[f"bp{i+1}"] * df[f"bv{i+1}"]
        ask_p = ask_amt/ask_qty
        bid_p = bid_amt/bid_qty
        df["mp"] = (ask_p * bid_qty + bid_p * ask_qty) / (bid_qty + ask_qty)
        return df.loc[~df.localtime.duplicated(keep="last"), :]

    def get_depth_queue(self, ex: str, pair: str, datetime: str) -> Deque[Depth]:
        """ Load depth df from API """
        df = self.get_depth_df(ex=ex, pair=pair, datetime=datetime)
        q = deque()
        for row_dict in df.to_dict(orient="record"):
            q.append(self._row_dict_to_depth(row_dict=row_dict, contract=pair, ccy1="ccy1", ccy2="ccy2", ex=ex))
        return q

    def __row_dict_to_trade(self, row_dict: Dict) -> Trade:
        ex, contract = row_dict["symbol"].split(".")
        return Trade(
            ex=ex,
            contract=contract,
            price=row_dict["price"],
            quantity=row_dict["qty"],
            tx_time=row_dict["localtime"],
            local_time=row_dict["localtime"],
            mexc_side=row_dict["mexc_side"],
            side=row_dict["side"],
            uid=row_dict["uid"],
        )

    def get_trade_df(self, ex: str, pair: str, datetime: str) -> pd.DataFrame:
        return pd.read_csv(f"{DATA_ROOT}/trade_{ex}.{pair}_{datetime}.csv")

    def get_trade_queue(self, ex: str, pair: str, datetime: str) -> Deque[Trade]:
        """ Load trade df from API """
        df = self.get_trade_df(ex=ex, pair=pair, datetime=datetime)
        q = deque()
        for row_dict in df.to_dict(orient="record"):
            q.append(self.__row_dict_to_trade(row_dict=row_dict))
        return q
    
    def get_mexc_informness_map(self,pair) -> Dict:
        with open(f"{DATA_ROOT}/{pair}_informness_map.json", 'r') as f:
            d = json.load(f)
        return d
        