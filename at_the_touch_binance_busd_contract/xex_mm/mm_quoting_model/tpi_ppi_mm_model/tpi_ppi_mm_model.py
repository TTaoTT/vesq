
"""

2 components:

1. Information(trade and order flow)'s impact on both side quotes (mid price) (predictive/offensive). -> PPI
2. Information(trade and order flow)'s impact on one side quotes (predictive/defensive/baiting). -> TPI

Model:
PPI = Linear Model = beta_q_ppi * q_trd + beta_i * I
Fair price = Last fair price + PPI(Informed-ness/Alpha)
Fair spread = hedge cost = E[dollar hedge delay risk penalty] + takers fee = f(sigma_trd * t_delay) + takers_fee
TPI = beta_q_tpi * q_trd
Ask = Fair Price + TPI + Fair Spread
Bid = Fair Price + TPI - Fair Spread

"""

import math

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class TpiPpiMmModel:
    def __init__(self,
                 ppi_betas: np.ndarray,
                 sprd_t: float,
                 takers_fee_rate: float,
                 ask_tpi_betas: np.ndarray,
                 bid_tpi_betas: np.ndarray,
                 bid_liquidity_replenishment_rate: float,
                 ask_liquidity_replenishment_rate: float,
                 ):
        self.__ppi_betas = ppi_betas
        self.__sprd_t = sprd_t
        self.__takers_fee_rate = takers_fee_rate
        self.__ask_tpi_betas = ask_tpi_betas
        self.__bid_tpi_betas = bid_tpi_betas
        self.__bid_liquidity_replenishment_rate = bid_liquidity_replenishment_rate
        self.__ask_liquidity_replenishment_rate = ask_liquidity_replenishment_rate

        self.__ppi_arr = []
        self.__bid_tpi_arr = []
        self.__ask_tpi_arr = []
        self.__cum_ppi = 0
        self.__cum_bid_tpi = 0
        self.__cum_ask_tpi = 0
        self.__decayed_cum_bid_tpi = 0
        self.__decayed_cum_ask_tpi = 0
        self.__last_decay_time_bid = -1
        self.__last_decay_time_ask = -1

    def informedness_scaled_trade_quantity(self, trd_qty: float, informedness: float) -> float:
        """
        :param trd_qty: Signed trade quantity.
        :param informedness: Signed informedness measure, per USDT profitability.
        """
        return trd_qty * informedness

    def ppi(self, x_arr: np.ndarray) -> float:
        """
        No intercept. There should be no PPI when there are no trades.

        :param x_arr: Input of PPI model.
            Signed quantity * Signed informed-ness
            Signed informed-ness = profitability
        return: PPI mid price adjustment.
        """
        return self.__ppi_betas.dot(x_arr)

    def fair_spread(self, sigma: float, ref_prc: float):
        """
        This is a measure of expected hedging cost, which has two components:
            1. Market/timing risk due to untimely hedging, since in reality hedging is not instantaneous.
            2. Takers fee
        #1 is a function of time (till the hedge is actually executed), this can be a floating measure or a model
        itself, but for now, we model this with a constant delay, which can be set manually given our knowledge of
        average exchange latency and our hedger's operational time lag. Or, it can be calibrated.
        Secondly, it is also a function of price volatility (trade price volatility, since trades usually bounce
        between best bid/asks).

        :param t: Time delay till a hedge is complete, in seconds.
        :param sigma: Annualized volatility of trade price.
        :param takers_fee: Dollar fee.
        return: A measure of fair spread.
        """
        secs_per_y = 365 * 24 * 60 * 60
        return sigma * math.sqrt(self.__sprd_t / secs_per_y) + self.__takers_fee_rate * ref_prc

    def inventory_optim_spread(self):
        raise NotImplementedError()

    def tpi(self, betas: np.ndarray, x_arr: np.ndarray):
        """
        No intercept. There should be no TPI when there are no trades.

        :param betas: Coefficients of TPI model.
        :param x_arr: Input of TPI model.
            Taker quantity
        return: TPI mid price adjustment.
        """
        return betas.dot(x_arr)

    def bid_tpi(self, x_arr: np.ndarray):
        return self.tpi(betas=self.__bid_tpi_betas, x_arr=x_arr)

    def ask_tpi(self, x_arr: np.ndarray):
        return self.tpi(betas=self.__ask_tpi_betas, x_arr=x_arr)

    def ask(self, last_fair_prc: float, ppi: float, fair_sprd: float, tpi: float):
        """
        At-touch strategy

        :param last_fair_prc: Last observable or implied fair price proxy.
        :param ppi: PPI. [-inf, +inf]
        :param fair_sprd: Fair spread. (0, +inf]
        :param tpi: TPI. [0, +inf]
        return: Best ask quote price.
        """
        assert fair_sprd > 0
        assert tpi >= 0
        return last_fair_prc + ppi + fair_sprd + tpi

    def bid(self, last_fair_prc: float, ppi: float, fair_sprd: float, tpi: float):
        """
        At-touch strategy

        :param last_fair_prc: Last observable or implied fair price proxy.
        :param ppi: PPI. [-inf, +inf]
        :param fair_sprd: Fair spread. (0, +inf]
        :param tpi: TPI. [0, +inf]
        return: Best ask quote price.
        """
        assert fair_sprd > 0
        assert tpi >= 0
        return last_fair_prc + ppi - fair_sprd - tpi

    def __add_ppi(self, ppi: float):
        self.__ppi_arr.append(ppi)
        self.__cum_ppi += ppi

    def __decay_ask_tpi(self, ts: int):
        t = (ts - self.__last_decay_time_ask) / 1e9 if self.__last_decay_time_ask != -1 else 0
        self.__decayed_cum_ask_tpi = math.exp(-self.__ask_liquidity_replenishment_rate * t) * self.__decayed_cum_ask_tpi
        self.__last_decay_time_ask = ts

    def __decay_bid_tpi(self, ts: int):
        t = (ts - self.__last_decay_time_bid) / 1e9 if self.__last_decay_time_bid != -1 else 0
        self.__decayed_cum_bid_tpi = math.exp(-self.__bid_liquidity_replenishment_rate * t) * self.__decayed_cum_bid_tpi
        self.__last_decay_time_bid = ts

    def __add_bid_tpi(self, bid_tpi: float):
        self.__bid_tpi_arr.append(bid_tpi)
        self.__cum_bid_tpi += bid_tpi
        self.__decayed_cum_bid_tpi += bid_tpi

    def __add_ask_tpi(self, ask_tpi: float):
        self.__ask_tpi_arr.append(ask_tpi)
        self.__cum_ask_tpi += ask_tpi
        self.__decayed_cum_ask_tpi += ask_tpi

    def get_cum_ppi(self):
        return self.__cum_ppi

    def get_cum_ask_tpi(self):
        return self.__decayed_cum_ask_tpi

    def get_cum_bid_tpi(self):
        return self.__decayed_cum_bid_tpi

    def implied_bid_ask(self,
                        ts: int,
                        last_fair_prc: float, ppi_x_arr: np.ndarray, sigma: float,
                        ask_tpi_x_arr: np.ndarray, bid_tpi_x_arr: np.ndarray):
        """
        Stateful, call sequentially when orderbook in unstable state.

        Fair price calculation should only be triggered during an unstable market state. An unstable market state is
        defined by when market spread is unusually large, which is measured by empirical quantile. For instance, we
        defined a cutoff quantile of 0.8, meaning when market spread is larger than 80% of the historical quantiles we
        observe, the orderbook is deemed unstable, fair price measure (using mid price as a proxy) is unreliable, hence
        needs to trigger fair price calculation.

        :param last_fair_prc: Last reliable fair price proxy observed during a stable market or computed from the last
            fair price calculation.
        """

        ppi = self.ppi(x_arr=ppi_x_arr)
        self.__add_ppi(ppi=ppi)
        ask_tpi = self.ask_tpi(x_arr=ask_tpi_x_arr)
        self.__decay_ask_tpi(ts=ts)
        self.__add_ask_tpi(ask_tpi=ask_tpi)
        bid_tpi = self.bid_tpi(x_arr=bid_tpi_x_arr)
        self.__decay_bid_tpi(ts=ts)
        self.__add_bid_tpi(bid_tpi=bid_tpi)
        cum_ppi = self.get_cum_ppi()
        fair_sprd = self.fair_spread(sigma=sigma, ref_prc=last_fair_prc + cum_ppi)
        cum_ask_tpi = self.get_cum_ask_tpi()
        cum_bid_tpi = self.get_cum_bid_tpi()
        ask = self.ask(last_fair_prc=last_fair_prc, ppi=cum_ppi, fair_sprd=fair_sprd, tpi=cum_ask_tpi)
        bid = self.bid(last_fair_prc=last_fair_prc, ppi=cum_ppi, fair_sprd=fair_sprd, tpi=cum_bid_tpi)
        print(f"ppi={ppi}, ask_tpi={ask_tpi}, bid_tpi={bid_tpi}")
        return bid, ask

    def reset(self):
        """
        Stateful, reset unstable orderbook tracking.
        """
        self.__ppi_arr = []
        self.__bid_tpi_arr = []
        self.__ask_tpi_arr = []
        self.__cum_ppi = 0
        self.__cum_bid_tpi = 0
        self.__cum_ask_tpi = 0


def run_test():
    last_fair_prc=1000
    ppi_betas=np.array([0.02])
    t=1
    sigma=0.7
    takers_fee_rate = 0.0003
    ask_tpi_betas=np.array([0.03])
    bid_tpi_betas = np.array([0.03])

    trd_qty_arr = [10, -10, 10, -10, 10, -10, 10, -10, 10, -10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ts = np.array(range(1, len(trd_qty_arr)+1)) * 1e9
    informedness_arr = [0.] * len(trd_qty_arr)
    bid_liquidity_replenishment_rate = 0.01
    ask_liquidity_replenishment_rate = 0.01

    o = TpiPpiMmModel(
        ppi_betas=ppi_betas,
        sprd_t=t,
        takers_fee_rate=takers_fee_rate,
        ask_tpi_betas=ask_tpi_betas,
        bid_tpi_betas=bid_tpi_betas,
        bid_liquidity_replenishment_rate=bid_liquidity_replenishment_rate,
        ask_liquidity_replenishment_rate=ask_liquidity_replenishment_rate,
    )

    bid_asks = []
    for _ts, trd_qty, inform in zip(ts, trd_qty_arr, informedness_arr):
        bid, ask = o.implied_bid_ask(
            last_fair_prc=last_fair_prc,
            ppi_x_arr=np.array([trd_qty * inform]),
            sigma=sigma,
            ask_tpi_x_arr=np.array([trd_qty if trd_qty > 0 else 0]),
            bid_tpi_x_arr=np.array([-trd_qty if trd_qty < 0 else 0]),
            ts=_ts,
        )
        bid_asks.append((bid, ask))
    bid_asks = pd.DataFrame(bid_asks, columns=["bid", "ask"])
    bid_asks.plot()
    plt.show()


if __name__ == '__main__':
    run_test()
