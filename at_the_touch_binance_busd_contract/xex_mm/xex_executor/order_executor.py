import math
from typing import Dict, List, Tuple

import numpy as np
import pandas as pd

from xex_mm.signals.LocalCalculator import LocalCalculatorBase
from xex_mm.strategy_configs.xex_mm_config import PredRefPrcMode
from xex_mm.calculators.arrival_rate import CalEMAvgArrivalRate
from xex_mm.utils.base import Depth, Trade, BBO, TransOrder
from xex_mm.utils.enums import Direction
from xex_mm.utils.configs import FeeConfig
from xex_mm.xex_depth.xex_depth import XExDepth

from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model_no_partial_fill import SingleArrivalRateXExMmModelNoPartialFill

from xex_mm.signals.SignalCalculator import SignalCalculatorBase
from xex_mm.xex_executor.params import ModelParams


class RefPriceModel:
    def predict(self, xs: np.ndarray):
        return xs[0]
        
class SpreadModel:
    def predict(self, xs: np.ndarray):
        return xs[0]
        
class SigmaModel:
    def predict(self, xs: np.ndarray):
        return xs[0]
        
class ArrivalRateModel:
    def predict(self, xs: np.ndarray):
        return xs[0]
        
class ArrivalRateMktDepthDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]
        
class ArrivalRateQtyDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


class OrderExecutorTest2:
    def __init__(self, iids: List[str], amt_bnd: float,
                 pred_on: bool, art: float, sigmat: float, hst: float, rett: float, refprct: float,
                 latencyt: float, event_count_halflife: int, invent: float, ref_prc_reset_sigma_multiplier: float,
                 betas: List[Tuple[float, float]], q_ubs: List[float], tau_lb: int,
                 pred_ref_prc_mode: PredRefPrcMode,
                 mdl_params: ModelParams, onboard: bool = False) -> None:
        self._xex_depth_map: Dict[str, XExDepth] = {}
        self._inventory_map: Dict[str, float] = {}
        self._iids = iids
        self._init_depth(iids=iids)
        self.amt_bnd = amt_bnd
        self._setup_mm_model()
        self.pred_on = pred_on
        self.signal_calculator_map = dict()
        self._mdl_params = mdl_params
        for trd_sym, rel_sym in zip(mdl_params.trade_symbols, mdl_params.related_symbols):
            self.signal_calculator_map[trd_sym] = SignalCalculatorBase(pred_on=mdl_params.pred_on,
                                                                   trade_symbol=trd_sym,
                                                                   related_symbol=rel_sym)
        self.local_calculator = LocalCalculatorBase(art=art, sigmat=sigmat, hst=hst, rett=rett, refprct=refprct,
                                                  latencyt=latencyt, invent=invent,
                                                  event_count_halflife=event_count_halflife)
        self._ref_prc_reset_sigma_multiplier = ref_prc_reset_sigma_multiplier
        self.inven_bnd = 1000
        self._betas = betas
        self.q_ubs = q_ubs
        self._pred_ref_prc_mode = pred_ref_prc_mode
        self.onboard = onboard
        self.pnl_decomp = []
        self._tau_lb = tau_lb

    def update_signal_model(self, filepath: str):
        for trd_sym, cal in self.signal_calculator_map.items():
            cal.load_ref_prc_model(pickle_address=filepath)

    def _init_depth(self, iids: List[str]):
        for iid in iids:
            _, contract = iid.split(".")
            if self._xex_depth_map.get(contract) is None:
                self._xex_depth_map[contract] = XExDepth(contract=contract)
    
    def _setup_mm_model(self):
        self.mm_mdl = SingleArrivalRateXExMmModelNoPartialFill(fee_rate_map=FeeConfig().to_dict())

    def get_tau(self, ts: int):
        depth_freq, bbo_freq, trade_freq = self.local_calculator.local_update_freq_cal.get_factor(ts=ts)
        if depth_freq is None:
            return
        depth_freq = max(self._tau_lb, depth_freq)
        return depth_freq

    def _update_prediction(self, contract: str, ex: str, ref_prc: float, ts: int):
        tau = self.get_tau(ts=ts)
        if tau is None:
            return False

        xex = "xex"

        if self.pred_on:
            # pred_lar = self.local_calculator.predict_long_arrival_rate(ts=ts)
            # pred_sar = self.local_calculator.predict_short_arrival_rate(ts=ts)

            pred_las, pred_sas = self.local_calculator.pred_trade_arrival_size(ts=ts)
            pred_lat, pred_sat = self.local_calculator.pred_trade_arrival_time(ts=ts)

            pred_lhs = self.local_calculator.predict_long_half_spread(ts=ts)
            pred_shs = self.local_calculator.predict_short_half_spread(ts=ts)
            pred_sigma = self.local_calculator.predict_sigma(ts=ts)
            iid = f"{ex}.{contract}"
            if iid in self.signal_calculator_map:
                pred_ref_prc_ret = self.signal_calculator_map[iid].predict_ref_prc_ret(sec=tau)
                if pred_ref_prc_ret is None:
                    return False
            else:
                pred_ref_prc_ret = self.local_calculator.predict_ref_prc_ret(ts=ts)
            pred_ref_prc = ref_prc * math.exp(pred_ref_prc_ret * tau)
            pred_latency = self.local_calculator.pred_latency(ts=ts)
        else:
            if not self.mm_mdl.has_depth_metrics(contract=contract, ex=xex):
                return False

            pred_sigma = self.local_calculator.curr_ema_sigma(ts=ts)
            if pred_sigma is None:
                return False

            # pred_lar = self.local_calculator.curr_emavg_long_arrival_rate(ts=ts)
            # pred_sar = self.local_calculator.curr_emavg_short_arrival_rate(ts=ts)  # num tokens

            pred_las, pred_sas = self.local_calculator.curr_trade_arrival_size(ts=ts)
            if (pred_las is None) or (pred_sas is None):
                return False

            pred_lat, pred_sat = self.local_calculator.curr_trade_arrival_time(ts=ts)
            if (pred_lat is None) or (pred_sat is None):
                return False

            pred_lhs = self.local_calculator.curr_emavg_long_half_spread(ts=ts)
            if pred_lhs is None:
                return False
            pred_shs = self.local_calculator.curr_emavg_short_half_spread(ts=ts)
            if pred_shs is None:
                return False
            # pred_ref_prc_ret = 0
            # pred_ref_prc = ref_prc
            if self._pred_ref_prc_mode == PredRefPrcMode.neutral:
                pred_ref_prc = ref_prc
                pred_ref_prc_ret = 0
            elif self._pred_ref_prc_mode == PredRefPrcMode.ret:
                pred_ref_prc_ret = self.local_calculator.curr_emavg_ref_prc_ret(ts=ts)
                if pred_ref_prc_ret is None:
                    pred_ref_prc_ret = 0
                pred_ref_prc = ref_prc * math.exp(pred_ref_prc_ret * tau)
            elif self._pred_ref_prc_mode == PredRefPrcMode.prc:
                pred_ref_prc = self.local_calculator.curr_emavg_ref_prc(ts=ts)
                pred_ref_prc_ret = math.log(pred_ref_prc/ref_prc)
                # reset
                reset_bnd = math.sqrt(tau) * pred_sigma * self._ref_prc_reset_sigma_multiplier
                prc_lag = abs(ref_prc - pred_ref_prc)
                if (abs(pred_ref_prc_ret) > reset_bnd) or (prc_lag > reset_bnd):
                    pred_ref_prc = ref_prc
                    pred_ref_prc_ret = 0
                    self.local_calculator.local_ref_prc_cal.await_reset = True
                    raise RuntimeError(f"pred_ref_prc_ret={pred_ref_prc_ret}, prc_lag={prc_lag}, reset_ret_bnd={reset_bnd}, ref_prc_reset_sigma_multiplier={self._ref_prc_reset_sigma_multiplier}, pred_sigma={pred_sigma}")
            elif self._pred_ref_prc_mode == PredRefPrcMode.tar:
                raise NotImplementedError("LAR, SAR deprecated")
                # tilde_p = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
                # s_l = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_l")
                # s_s = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_s")
                # self.mm_mdl.update_pred_arrival_rate(contract=contract, ex=xex, d=Direction.long, pred=pred_lar)
                # self.mm_mdl.update_pred_arrival_rate(contract=contract, ex=xex, d=Direction.short, pred=pred_sar)
                # lar = self.mm_mdl._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=Direction.short, tau=tau)
                # sar = self.mm_mdl._get_lambda_for_quote_d(contract=contract, ex=xex, quote_d=Direction.long, tau=tau)
                # lar_sar_ratio = lar / (lar + sar)
                # if lar_sar_ratio > 0.5:
                #     a = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_s")
                #     b = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_s")
                #     # delta = max(((lar - sar) - b) / a, 0)
                #     delta = max(((lar - sar) - 0) / a, 0)
                #     pred_ref_prc = tilde_p + s_s + delta
                # elif lar_sar_ratio < 0.5:
                #     a = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_l")
                #     b = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_l")
                #     # delta = max(((sar - lar) - b) / a, 0)
                #     delta = max(((sar - lar) - 0) / a, 0)
                #     pred_ref_prc = tilde_p - s_l - delta
                # else:
                #     pred_ref_prc = ref_prc
                # pred_ref_prc_ret = math.log(pred_ref_prc / ref_prc)
            else:
                raise NotImplementedError()
            pred_latency = self.local_calculator.curr_emavg_latency(ts=ts)

        mult = 20
        tilde_p = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        q_grp_size_ccy2 = min(pred_las, pred_sas) * tilde_p / mult

        pred_lat = max(pred_lat, 1)
        pred_sat = max(pred_sat, 1)

        self.mm_mdl.update_tau(contract=contract, ex=xex, tau=tau)
        self.mm_mdl.update_pred_arrival_size_in_tokens(
            contract=contract, ex=xex, d=Direction.long, arrival_size_in_tokens=pred_las)
        self.mm_mdl.update_pred_arrival_size_in_tokens(
            contract=contract, ex=xex, d=Direction.short, arrival_size_in_tokens=pred_sas)
        self.mm_mdl.update_pred_arrival_time(
            contract=contract, ex=xex, d=Direction.long, arrival_time=pred_lat)
        self.mm_mdl.update_pred_arrival_time(
            contract=contract, ex=xex, d=Direction.short, arrival_time=pred_sat)

        self.mm_mdl.update_q_grp_size_ccy2(contract=contract, q_grp_size_ccy2=q_grp_size_ccy2)

        self.mm_mdl.update_pred_sigma(
            contract=contract, ex=xex,
            pred=pred_sigma)
        self.mm_mdl.update_pred_half_spread(
            contract=contract, ex=xex, d=Direction.long,
            pred=pred_lhs)
        self.mm_mdl.update_pred_half_spread(
            contract=contract, ex=xex, d=Direction.short,
            pred=pred_shs)
        self.mm_mdl.update_pred_ref_prc(
            contract=contract, ex=xex,
            pred=pred_ref_prc)
        self.mm_mdl.update_pred_latency(
            contract=contract, ex=xex,
            pred_latency=pred_latency
        )

        return None not in {pred_las, pred_sas, pred_lat, pred_sat, pred_lhs, pred_shs, pred_sigma, pred_ref_prc_ret}

    def _update_depth_for_mm_mdl(self, contract: str, ex: str):

        depth = self._xex_depth_map[contract].get_depth_for_ex(ex=ex)
        self.mm_mdl.update_depth(contract=contract, ex=ex, depth=depth, amt_bnd=self.amt_bnd)

        xex_depth = self._xex_depth_map[contract].get_xex_depth()
        xex = "xex"
        self.mm_mdl.update_depth(contract=contract, ex=xex, depth=xex_depth, amt_bnd=self.amt_bnd)

    def update_depth(self, iid: str, depth: Depth):
        ex, contract = iid.split(".")
        xex_depth_obj = self._xex_depth_map[contract]
        xex_depth_obj.update_depth_for_ex(depth=depth)
        self._update_depth_for_mm_mdl(contract=contract, ex=ex)
        self.local_calculator.update_depth(depth=xex_depth_obj.get_xex_depth())

    def remove_depth(self, iid: str):
        ex, contract = iid.split(".")
        xex_depth_obj = self._xex_depth_map[contract]
        if xex_depth_obj.contains(ex=ex):
            xex_depth_obj.remove_ex(ex=ex)
        self.mm_mdl.remove_depth_metrics(contract=contract, ex=ex)
        xex_depth_obj = self._xex_depth_map[contract]
        if not xex_depth_obj.empty():
            xex_depth = xex_depth_obj.get_xex_depth()
            xex = "xex"
            self.mm_mdl.update_depth(contract=contract, ex=xex, depth=xex_depth, amt_bnd=self.amt_bnd)
            self.local_calculator.update_depth(depth=xex_depth)

    def update_bbo(self, bbo: BBO):

        """ Full update with bbo is slow. """
        contract = bbo.meta.contract
        ex = bbo.meta.ex
        xex_depth_obj = self._xex_depth_map[contract]
        if xex_depth_obj.contains(ex=ex):
            if bbo.valid_bbo:
                xex_depth_obj.update_depth_for_ex_with_bbo(bbo=bbo)
                self._update_depth_for_mm_mdl(contract=contract, ex=ex)
                self.local_calculator.update_bbo(bbo=bbo)

        """ Partial update with bbo is fast. """
        # self._mm_mdl.update_bbo(contract=contract, ex=ex, bbo=bbo)  # Partial, no xex update
        # # TODO: Factor update?
            
    def get_ex_depth(self, iid: str):
        ex_name, contract = iid.split(".")
        return self._xex_depth_map[contract].get_depth_for_ex(ex_name)
        
    def get_depth(self, iid: str):
        _, contract = iid.split(".")
        return self._xex_depth_map[contract].get_xex_depth()
    
    def get_inventory(self, contract: str):
        return self._inventory_map.get(contract, 0)
    
    def update_trade(self, trade: Trade):
        self.local_calculator.update_trade(trade=trade)
        
    def update_inventory(self, contract: str, inventory: float, ts: int):
        self._inventory_map[contract] = inventory
        self.local_calculator.local_inven_sigma_cal.on_inventory(inven=inventory, ts=ts)

    def cal_pnl_decomp(self, ref_prc: float, delta: float, price: float, qty: float, side: int, type: str, ts: int):
        self.pnl_decomp.append(
            {
                "ref_prc":ref_prc,
                "spread_pnl":delta*qty,
                "aggression_pnl":(price - ref_prc - delta) * qty if side == -1 else (ref_prc - price - delta) * qty,
                "inven_chg": qty*side,
                "maker_amt":qty*price if type == "maker" else 0,
                "taker_amt":qty*price if type == "taker" else 0,
                "ts": ts
            }
        )

    def get_pnl_decomp_result(self):
        if len(self.pnl_decomp) == 0:
            return pd.DataFrame()

        df = pd.DataFrame(self.pnl_decomp)
        df["inven"] = df["inven_chg"].cumsum()
        df["maket_pnl"] = df["ref_prc"].diff() * df["inven"].shift()

        return df

    def is_stats_ready(self, contract: str):
        xex = "xex"
        depth_metrics_ready = self.mm_mdl.has_depth_metrics(contract=contract, ex=xex)
        return depth_metrics_ready

    def get_stats(self, contract: str, ts: int) -> Dict:
        xex = "xex"
        stats = {}
        ref_prc = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        stats["ts"] = ts
        stats["inven"] = self.get_inventory(contract=contract)
        if self.local_calculator.local_inven_sigma_cal.emavg_inven_sigma.is_initialized:
            stats["inven_sigma"] = self.local_calculator.local_inven_sigma_cal.get_factor(ts=ts)
        stats["ref_prc"] = ref_prc
        a_l = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_l")
        b_l = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_l")
        a_s = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_s")
        b_s = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_s")
        s_l = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_l")
        s_s = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_s")
        stats["a_l"] = a_l
        stats["b_l"] = b_l
        stats["a_s"] = a_s
        stats["b_s"] = b_s
        stats["s_l"] = s_l
        stats["s_s"] = s_s
        if self.pred_on:
            stats["sigma"] = self.local_calculator.predict_sigma(ts=ts)
            stats["larr"] = self.local_calculator.predict_long_arrival_rate(ts=ts)
            stats["sarr"] = self.local_calculator.predict_short_arrival_rate(ts=ts)
            stats["las"], stats["sas"] = self.local_calculator.pred_trade_arrival_size(ts=ts)
            stats["lat"], stats["sat"] = self.local_calculator.pred_trade_arrival_time(ts=ts)
        else:
            stats["mdl_sigma"] = self.mm_mdl.get_pred_sigma(contract=contract, ex=xex)
            mdl_pred_ref_prc = self.mm_mdl.get_pred_ref_prc(contract=contract, ex=xex)
            if mdl_pred_ref_prc is not None:
                stats["mdl_pred_ref_prc"] = mdl_pred_ref_prc
            mdl_curr_ref_prc = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
            if mdl_pred_ref_prc is not None:
                stats["mdl_ref_prc_ret"] = math.log(mdl_pred_ref_prc / mdl_curr_ref_prc)
            stats["sigma"] = self.local_calculator.curr_ema_sigma(ts=ts)
            stats["impulse_sigma"] = self.local_calculator.curr_impulse_sigma(ts=ts)
            stats["pred_ref_prc"] = self.local_calculator.curr_emavg_ref_prc(ts=ts)
            stats["pred_ref_prc_ret"] = self.local_calculator.curr_emavg_ref_prc_ret(ts=ts)
            stats["larr"] = self.local_calculator.curr_emavg_long_arrival_rate(ts=ts)
            stats["sarr"] = self.local_calculator.curr_emavg_short_arrival_rate(ts=ts)
            par: CalEMAvgArrivalRate = self.local_calculator.calib_map["PastEMAvgArrivalRate"]
            lar, lrr, sar, srr = par.record(ts=ts)
            stats["lar"] = lar
            stats["lrr"] = lrr
            stats["sar"] = sar
            stats["srr"] = srr
            stats["las"], stats["sas"] = self.local_calculator.curr_trade_arrival_size(ts=ts)
            stats["lat"], stats["sat"] = self.local_calculator.curr_trade_arrival_time(ts=ts)
            depth_freq, bbo_freq, trade_freq = self.local_calculator.local_update_freq_cal.get_factor(ts=ts)
            if depth_freq is not None:
                stats["depth_freq"] = depth_freq
            if bbo_freq is not None:
                stats["bbo_freq"] = bbo_freq
            if trade_freq is not None:
                stats["trade_freq"] = trade_freq
            latency = self.local_calculator.curr_emavg_latency(ts=ts)
            if latency is not None:
                stats["emavg_latency"] = latency
        return stats

    def get_orders(self, contract: str, ex: str, ref_prc: float, t: int):
        orders = []

        res = self._update_prediction(contract=contract, ex=ex, ref_prc=ref_prc, ts=t)
        if not res:
            return [], []

        xex = "xex"
        tilde_p = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")
        s_l = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_l")
        s_s = self.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_s")
        sigma = self.mm_mdl.get_pred_sigma(contract=contract, ex=xex)
        cur_risk = self.get_inventory(contract=contract)
        data = []
        available_xex_exs = set(self._xex_depth_map[contract].get_ex_names())
        for iid in self._iids:
            _ex, contract = iid.split(".")
            if _ex not in available_xex_exs:
                continue
            for i, (q_beta, I_beta) in enumerate(self._betas):
                for d in [Direction.long, Direction.short]:
                    _data = dict(
                        contract=contract,
                        ex=_ex,
                        i=i,
                        q_beta=q_beta,
                        I_beta=I_beta,
                        d=d,
                    )
                    q = self.mm_mdl._get_q_grp_size_ccy1(contract=contract) * 1
                    U_p, delta_hat, maker_taker, p_msg = self.mm_mdl.optimal_price_level_q2(
                        contract=contract,
                        ex=_ex, d=d, q=q,
                        I=cur_risk, q_beta=q_beta, I_beta=I_beta,
                    )
                    _data.update(
                        U_p=U_p,
                        delta_hat=delta_hat,
                        maker_taker=maker_taker,
                        p_msg=p_msg,
                    )
                    if math.isnan(delta_hat):
                        data.append(_data)
                        continue
                    p = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                    delta = s_l + delta_hat if d == Direction.long else s_s + delta_hat
                    U_q, q_hat, q_msg = self.mm_mdl.optimal_quantity2(
                        contract=contract,
                        ex=_ex, delta=delta_hat, d=d,
                        I=cur_risk, q_beta=q_beta, I_beta=I_beta,
                    )
                    _data.update(
                        U_q=U_q,
                        q_hat=q_hat,
                        q_msg=q_msg,
                    )
                    if math.isnan(q_hat):
                        data.append(_data)
                        continue
                    order_q = min(q_hat, self.q_ubs[i] / tilde_p)
                    if U_p> -self.mm_mdl._numeric_tol and order_q>0:
                        orders.append(
                            TransOrder(
                                side=d,
                                p=p,
                                q=order_q,
                                iid=".".join((_ex, contract)),
                                floor=i,
                                delta=delta,
                                maker_taker=maker_taker
                                )
                            )
                    data.append(_data)
        return orders, data
