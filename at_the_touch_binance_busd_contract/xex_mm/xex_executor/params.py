from dataclasses import dataclass
from typing import List, Dict


@dataclass
class ModelParams:
    pred_on: bool
    trade_symbol_to_related_symbols_map: Dict
    trade_symbol_to_model_path_map: Dict

    """
    Example:
    
        symbol_map = {
            "binance.btc_usdt_swap":["binance.btc_usdt_swap","binance.btc_usdt"],
            "mxc.btc_usdt_swap":["binance.btc_usdt_swap","mxc.btc_usdt"],
        }
    """

    def __post_init__(self):
        self.related_symbol_to_trade_symbols_map = dict()
        self._trade_symbol_set = set()
        self._related_symbol_set = set()
        for trd_sym, rel_syms in self.trade_symbol_to_related_symbols_map.items():
            self._trade_symbol_set.add(trd_sym)
            for rel_sym in rel_syms:
                self._related_symbol_set.add(rel_sym)
                if rel_sym not in self.related_symbol_to_trade_symbols_map:
                    self.related_symbol_to_trade_symbols_map[rel_sym] = []
                self.related_symbol_to_trade_symbols_map[rel_sym].append(trd_sym)

    def is_trade_symbol(self, iid: str):
        return iid in self._trade_symbol_set

    def is_related_symbol(self, iid: str):
        return iid in self._related_symbol_set

    def to_dict(self):
        dct = self.__dict__
        dct["_trade_symbol_set"] = tuple(self._trade_symbol_set)
        dct["_related_symbol_set"] = tuple(self._related_symbol_set)
        return dct
