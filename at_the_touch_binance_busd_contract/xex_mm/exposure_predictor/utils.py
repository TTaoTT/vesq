import os.path
import pandas as pd
import tzlocal


class DataLoader:
    def __init__(self):
        self.data_folder = "C:\\Users\\aurth\\Downloads"

    def get_trade_data_path(self):
        return os.path.join(self.data_folder, "trade_sample.csv")

    def get_orderbook_data_path(self):
        return os.path.join(self.data_folder, "ob_sample.csv")

    def load_trade_data(self):
        df = pd.read_csv(self.get_trade_data_path())
        df.set_index(df.columns[0], inplace=True)
        df["datetime"] = pd.DatetimeIndex(df["localtime"] * 1000000, tz="UTC").tz_convert(tz=tzlocal.get_localzone())
        return df

    def load_orderbook_data(self):
        df = pd.read_csv(self.get_orderbook_data_path())
        df.set_index(df.columns[0], inplace=True)
        df["datetime"] = pd.DatetimeIndex(df["localtime"] * 1000000, tz="UTC").tz_convert(tz=tzlocal.get_localzone())
        return df


if __name__ == '__main__':
    df = DataLoader().load_orderbook_data()
    print(df.head())
