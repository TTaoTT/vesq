import math
from typing import Tuple

import numpy as np
import pandas as pd
from xex_mm.exposure_predictor.utils import DataLoader

"""
We want to strip all dependency to latency from our own actions.
"""


class ExposurePredictor:
    year_dt = pd.Timedelta("365d")

    def calib_mu_sigma(self, hist_orderbook: pd.DataFrame, hist_trade: pd.DataFrame) -> Tuple[float, float]:
        mid_prc: pd.Series = (hist_orderbook["ap1"] + hist_orderbook["bp1"]) / 2
        mid_prc.index = hist_orderbook["datetime"]
        mid_prc.ffill(inplace=True)
        r = np.log(mid_prc/mid_prc.shift())
        mu = r.mean()
        sigma = r.std()
        dt = mid_prc.index.to_series().diff().mean()
        mu = mu * self.year_dt / dt
        sigma = sigma * math.sqrt(self.year_dt / dt)
        return mu, sigma

    def pred_exposure(self, curr_exposure: float, curr_bid: float, curr_ask: float,
                      last_update_time: pd.Timestamp, curr_time: pd.Timestamp,
                      bid_orders: np.ndarray, ask_orders: np.ndarray,
                      mu: float, sigma: float, sigma_multiplier: float) -> float:
        """
        We only use cur bid1/ask1, since price movement layered on top of volume change is too much uncertainty.

        :param curr_exposure: Contructed by doing our own order matching, free from latency of our own actions.
        :param curr_bid: From orderbook, free from latency of our own actions.
        :param curr_ask: From orderbook, free from latency of our own actions.
        :param last_update_time: Last orderbook time.
        :param curr_time: Current time.
        :param bid_orders: Bid orders that have been submitted and not confirmed canceled.
        :param ask_orders: Ask orders that have been submitted and not confirmed canceled.

        [
            [prc1, qty1],
            [prc2, qty2],
            ...
        ]

        :param mu: Annualized drift.
        :param sigma: Annualized volatility.
        :param sigma_multiplier: Sigma multiplier to identify fill range.
        :return: Predicted exposure.
        """

        mid_price = (curr_bid + curr_ask) / 2
        spread = curr_ask - curr_bid
        dt = (curr_time - last_update_time) / self.year_dt

        upper_bid = mid_price * (1 + mu * dt + sigma_multiplier * sigma * math.sqrt(dt)) - spread / 2
        lower_ask = mid_price * (1 + mu * dt - sigma_multiplier * sigma * math.sqrt(dt)) + spread / 2

        print(f"upper_bid={upper_bid}, lower_ask={lower_ask}")

        short_qty = np.sum(ask_orders[ask_orders[:, 0] <= upper_bid, 1])
        long_qty = np.sum(bid_orders[bid_orders[:, 0] >= lower_ask, 1])

        return curr_exposure + long_qty - short_qty


if __name__ == '__main__':

    dl = DataLoader()

    ep = ExposurePredictor()
    ep.calib_mu_sigma(
        hist_orderbook=dl.load_orderbook_data(),
        hist_trade=dl.load_trade_data(),
    )

    exp = ep.pred_exposure(
        curr_exposure=10,
        curr_bid=1000.00,
        curr_ask=1000.01,
        last_update_time=pd.Timestamp("2022-06-24 09:30:00.000000000"),
        curr_time=pd.Timestamp("2022-06-24 09:30:03.000000000"),
        bid_orders=np.array([
            [999.99, 1],
            [999.98, 1],
            [999.97, 1],
        ]),
        ask_orders=np.array([
            [1000.02, 1],
            [1000.03, 1],
            [1000.04, 1],
        ]),
        mu=100,
        sigma=0.1,
        sigma_multiplier=1,
    )

    print(exp)


