import math
from typing import List, Dict, Tuple

import numpy as np

from atom.model.order import Order as ProdOrder
from atom.model.order import OrderSide

from xex_mm.utils.base import Depth, Order, TransOrder
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.xex_depth.xex_depth import XExDepth


class XexMmDeltaNeutralExecutor:
    def __init__(self, contract: str, profit_margin_return: float):
        super(XexMmDeltaNeutralExecutor, self).__init__()
        self.contract = contract
        self.profit_margin_return = profit_margin_return

        self._xex_depth: XExDepth = ...
        self._prev_order_map: Dict = {}

        self._fee_map = {}

        self._numerical_tol = 1e-10

    def update_fee_rate(self, contract: str, ex: str, takers_fee: float, makers_fee: float):
        self._fee_map[(contract, ex)] = (takers_fee, makers_fee)

    def get_fee_rate(self, contract: str, ex: str):
        return self._fee_map[(contract, ex)]

    def update_xex_depth(self, xex_depth: XExDepth) -> bool:
        assert self.contract == xex_depth.contract
        self._xex_depth = xex_depth
        return True

    def hedge_increment(self, inc_fill_qty_in_tokens: float, prc: float, maker_fee: float, d: int) -> Tuple[float, List[TransOrder]]:
        if self._xex_depth is Ellipsis:
            return inc_fill_qty_in_tokens, []

        if len(self._xex_depth.get_ex_names()) == 0:
            return inc_fill_qty_in_tokens, []

        post_fee_xex_depth: Depth = self._xex_depth.get_post_taker_fee_view().get_xex_depth()

        if d == Direction.long:  # long, hit bid to short hedge
            post_fee_long_prc = prc * (1 + maker_fee)
            post_fee_bid_hedge_prc = post_fee_long_prc * (1 + self.profit_margin_return)
            remaining_qty_in_tokens = inc_fill_qty_in_tokens
            post_fee_bid_quote_prc = np.nan
            for bid_lvl in post_fee_xex_depth.bids:
                bid_prc = bid_lvl.prc
                if bid_prc > post_fee_bid_hedge_prc - self._numerical_tol:
                    post_fee_bid_quote_prc = bid_prc
                    remaining_qty_in_tokens -= bid_lvl.qty
                    if remaining_qty_in_tokens < 0 + self._numerical_tol:
                        break
                    continue
                break
            if np.isnan(post_fee_bid_quote_prc):
                return inc_fill_qty_in_tokens, []

            total_quoted_qty_in_tokens = 0
            remaining_qty_in_tokens = inc_fill_qty_in_tokens
            orders = []
            for hex in self._xex_depth.get_ex_names():
                hex_taker_fee, hex_maker_fee = self._fee_map[(self.contract, hex)]
                hex_iid = f"{hex}.{self.contract}"
                hex_depth = self._xex_depth.get_depth_for_ex(ex=hex)
                quote_qty_in_tokens = 0
                for bid_lvl in hex_depth.bids:
                    if bid_lvl.prc * (1 - hex_taker_fee) > post_fee_bid_quote_prc - self._numerical_tol:
                        quote_qty_in_tokens += bid_lvl.qty
                        remaining_qty_in_tokens -= bid_lvl.qty
                        if remaining_qty_in_tokens < 0:
                            quote_qty_in_tokens += remaining_qty_in_tokens
                            remaining_qty_in_tokens += (-remaining_qty_in_tokens)
                            break
                        continue
                    break
                if quote_qty_in_tokens > 0:
                    trans_order = TransOrder(
                        side=Direction.short,
                        p=post_fee_bid_quote_prc / (1 - hex_taker_fee),
                        q=quote_qty_in_tokens,
                        iid=hex_iid,
                        floor=np.nan,
                        delta=np.nan,
                        life=1,
                        maker_taker=MakerTaker.taker,
                    )
                    total_quoted_qty_in_tokens += trans_order.quantity
                    assert total_quoted_qty_in_tokens < inc_fill_qty_in_tokens + self._numerical_tol
                    orders.append(trans_order)
            if total_quoted_qty_in_tokens > inc_fill_qty_in_tokens + self._numerical_tol:
                raise RuntimeError(f"total_quoted_qty_in_tokens({total_quoted_qty_in_tokens}) > inc_fill_qty({inc_fill_qty_in_tokens})")
            resid_qty_in_tokens = inc_fill_qty_in_tokens - total_quoted_qty_in_tokens
            return resid_qty_in_tokens, orders

        # short, hit ask to long hedge
        post_fee_short_prc = prc * (1 - maker_fee)
        post_fee_ask_hedge_prc = post_fee_short_prc * (1 - self.profit_margin_return)
        remaining_qty_in_tokens = inc_fill_qty_in_tokens
        post_fee_ask_quote_prc = np.nan
        for ask_lvl in post_fee_xex_depth.asks:
            ask_prc = ask_lvl.prc
            if ask_prc < post_fee_ask_hedge_prc + self._numerical_tol:
                post_fee_ask_quote_prc = ask_prc
                remaining_qty_in_tokens -= ask_lvl.qty
                if remaining_qty_in_tokens < 0 + self._numerical_tol:
                    break
                continue
            break
        if math.isnan(post_fee_ask_quote_prc):
            return inc_fill_qty_in_tokens, []

        total_quoted_qty_in_tokens = 0
        remaining_qty_in_tokens = inc_fill_qty_in_tokens
        orders = []
        for hex in self._xex_depth.get_ex_names():
            hex_taker_fee, hex_maker_fee = self._fee_map[(self.contract, hex)]
            hex_iid = f"{hex}.{self.contract}"
            hex_depth = self._xex_depth.get_depth_for_ex(ex=hex)
            quote_qty_in_tokens = 0
            for ask_lvl in hex_depth.asks:
                if ask_lvl.prc * (1 + hex_taker_fee) < post_fee_ask_quote_prc + self._numerical_tol:
                    quote_qty_in_tokens += ask_lvl.qty
                    remaining_qty_in_tokens -= ask_lvl.qty
                    if remaining_qty_in_tokens < 0:
                        quote_qty_in_tokens += remaining_qty_in_tokens
                        remaining_qty_in_tokens += (-remaining_qty_in_tokens)
                        break
                    continue
                break
            if quote_qty_in_tokens > 0:
                trans_order = TransOrder(
                    side=Direction.long,
                    p=post_fee_ask_quote_prc / (1 + hex_taker_fee),
                    q=quote_qty_in_tokens,
                    iid=hex_iid,
                    floor=np.nan,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.taker,
                )
                total_quoted_qty_in_tokens += trans_order.quantity
                assert total_quoted_qty_in_tokens < inc_fill_qty_in_tokens + self._numerical_tol
                orders.append(trans_order)
        if total_quoted_qty_in_tokens > inc_fill_qty_in_tokens + self._numerical_tol:
            raise RuntimeError(f"total_quoted_qty_in_tokens({total_quoted_qty_in_tokens}) > inc_fill_qty({inc_fill_qty_in_tokens})")
        resid_qty_in_tokens = inc_fill_qty_in_tokens - total_quoted_qty_in_tokens
        return resid_qty_in_tokens, orders

    def get_inc_fill_qty_in_tokens(self, order: Order) -> float:
        assert order.filled_qty >= 0
        oid = order.order_id
        if oid in self._prev_order_map:
            prev_order: Order = self._prev_order_map[oid]
            inc_fill_qty_in_tokens = max(order.filled_qty - prev_order.filled_qty, 0)
            return inc_fill_qty_in_tokens
        return order.filled_qty

    def update_order(self, order: Order) -> Tuple[float, List[TransOrder]]:
        oid = order.order_id
        taker_fee, maker_fee = self._fee_map[(order.contract, order.ex)]

        if oid in self._prev_order_map:
            prev_order: Order = self._prev_order_map[oid]
            inc_fill_qty_in_tokens = order.filled_qty - prev_order.filled_qty
            if inc_fill_qty_in_tokens < self._numerical_tol:
                self._prev_order_map[oid] = order
                return inc_fill_qty_in_tokens, []
            resid_qty_in_tokens, orders_to_place = self.hedge_increment(inc_fill_qty_in_tokens=inc_fill_qty_in_tokens, prc=order.avg_fill_prc, maker_fee=maker_fee, d=order.side)
            self._prev_order_map[oid] = order
            return resid_qty_in_tokens, orders_to_place

        inc_fill_qty_in_tokens = order.filled_qty
        if inc_fill_qty_in_tokens < self._numerical_tol:
            self._prev_order_map[oid] = order
            return inc_fill_qty_in_tokens, []
        resid_qty_in_tokens, orders_to_place = self.hedge_increment(inc_fill_qty_in_tokens=inc_fill_qty_in_tokens, prc=order.avg_fill_prc, maker_fee=maker_fee, d=order.side)
        self._prev_order_map[oid] = order
        return resid_qty_in_tokens, orders_to_place

    def remove_order(self, oid: str):
        if oid not in self._prev_order_map:
            return
        self._prev_order_map.pop(oid)


