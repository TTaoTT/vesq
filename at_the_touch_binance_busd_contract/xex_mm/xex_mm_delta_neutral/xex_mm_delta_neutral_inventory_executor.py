import math
from typing import List, Dict, Tuple

import numpy as np

from atom.model.order import Order as ProdOrder
from atom.model.order import OrderSide

from xex_mm.utils.base import Depth, Order, TransOrder
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.xex_depth.xex_depth import XExDepth


class XexMmDeltaNeutralInventoryExecutor:
    def __init__(self, contract: str):
        super(XexMmDeltaNeutralInventoryExecutor, self).__init__()
        self.contract = contract

        self._xex_depth: XExDepth = ...

        self._fee_map = {}
        self._prev_order_map = {}

        self._numerical_tol = 1e-10

    def update_fee_rate(self, contract: str, ex: str, takers_fee: float, makers_fee: float):
        self._fee_map[(contract, ex)] = (takers_fee, makers_fee)

    def update_xex_depth(self, xex_depth: XExDepth):
        self._xex_depth = xex_depth

    def update_order(self, order: Order):
        self._prev_order_map[order.order_id] = order

    def get_inc_fill_qty_in_tokens(self, order: Order) -> float:
        assert order.filled_qty >= 0
        oid = order.order_id
        if oid in self._prev_order_map:
            prev_order: Order = self._prev_order_map[oid]
            inc_fill_qty_in_tokens = max(order.filled_qty - prev_order.filled_qty, 0)
            return inc_fill_qty_in_tokens
        return order.filled_qty

    def hedge_inventory(self, inventory: float) -> Tuple[float, List[TransOrder]]:
        post_fee_xex_depth: Depth = self._xex_depth.get_post_taker_fee_view().get_xex_depth()

        if inventory > 0:
            remaining_inventory = inventory
            bid_prc = np.nan
            for bid_lvl in post_fee_xex_depth.bids:
                bid_prc = bid_lvl.prc
                remaining_inventory -= bid_lvl.qty
                if remaining_inventory < 0 + self._numerical_tol:
                    break
            post_fee_bid_quote_prc = bid_prc
            if np.isnan(post_fee_bid_quote_prc):
                return inventory, []

            total_quoted_qty_in_tokens = 0
            remaining_inventory = inventory
            orders = []
            for hex in self._xex_depth.get_ex_names():
                hex_taker_fee, hex_maker_fee = self._fee_map[(self.contract, hex)]
                hex_depth = self._xex_depth.get_depth_for_ex(ex=hex)
                quote_qty_in_tokens = 0
                for bid_lvl in hex_depth.bids:
                    if bid_lvl.prc * (1 - hex_taker_fee) > post_fee_bid_quote_prc - self._numerical_tol:
                        quote_qty_in_tokens += bid_lvl.qty
                        remaining_inventory -= bid_lvl.qty
                        if remaining_inventory < 0:
                            quote_qty_in_tokens += remaining_inventory
                            remaining_inventory += (-remaining_inventory)
                            break
                        continue
                    break
                if quote_qty_in_tokens > 0:
                    trans_order = TransOrder(
                        side=Direction.short,
                        p=post_fee_bid_quote_prc / (1 - hex_taker_fee),
                        q=quote_qty_in_tokens,
                        iid=f"{hex}.{self.contract}",
                        floor=np.nan,
                        delta=np.nan,
                        life=1,
                        maker_taker=MakerTaker.taker,
                    )
                    if trans_order.side == Direction.long:
                        total_quoted_qty_in_tokens += trans_order.quantity
                    else:
                        total_quoted_qty_in_tokens -= trans_order.quantity
                    resid_inventory = inventory + total_quoted_qty_in_tokens
                    assert abs(resid_inventory) < abs(inventory)
                    orders.append(trans_order)
            resid_inventory = inventory + total_quoted_qty_in_tokens
            return resid_inventory, orders

        remaining_inventory = inventory
        ask_prc = np.nan
        for ask_lvl in post_fee_xex_depth.asks:
            ask_prc = ask_lvl.prc
            remaining_inventory += ask_lvl.qty
            if remaining_inventory > 0 - self._numerical_tol:
                break
            continue
        post_fee_ask_quote_prc = ask_prc
        if math.isnan(post_fee_ask_quote_prc):
            return inventory, []

        total_quoted_qty_in_tokens = 0
        remaining_inventory = inventory
        orders = []
        for hex in self._xex_depth.get_ex_names():
            hex_taker_fee, hex_maker_fee = self._fee_map[(self.contract, hex)]
            hex_depth = self._xex_depth.get_depth_for_ex(ex=hex)
            quote_qty_in_tokens = 0
            for ask_lvl in hex_depth.asks:
                if ask_lvl.prc * (1 + hex_taker_fee) < post_fee_ask_quote_prc + self._numerical_tol:
                    quote_qty_in_tokens += ask_lvl.qty
                    remaining_inventory += ask_lvl.qty
                    if remaining_inventory > 0:
                        quote_qty_in_tokens -= remaining_inventory
                        remaining_inventory -= remaining_inventory
                        break
                    continue
                break
            if quote_qty_in_tokens > 0:
                trans_order = TransOrder(
                    side=Direction.long,
                    p=post_fee_ask_quote_prc / (1 + hex_taker_fee),
                    q=quote_qty_in_tokens,
                    iid=f"{hex}.{self.contract}",
                    floor=np.nan,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.taker,
                )
                if trans_order.side == Direction.long:
                    total_quoted_qty_in_tokens += trans_order.quantity
                else:
                    total_quoted_qty_in_tokens -= trans_order.quantity
                resid_inventory = inventory + total_quoted_qty_in_tokens
                assert abs(resid_inventory) < abs(inventory)
                orders.append(trans_order)
        resid_inventory = inventory + total_quoted_qty_in_tokens
        return resid_inventory, orders


