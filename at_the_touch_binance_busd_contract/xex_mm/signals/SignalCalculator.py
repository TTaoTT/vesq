from xex_mm.signals.factors.factor_calculator import FactorCalculatorBase
from xex_mm.utils.base import Depth, Trade, BBO


class SignalCalculatorBase:
    
    def __init__(self, **kwargs) -> None:
        self.pred_on = kwargs["pred_on"]
        self.factor_calculator_base = FactorCalculatorBase(
            trade_symbol = kwargs["trade_symbol"], 
            related_symbols = kwargs["related_symbols"],
            params = kwargs["params"],
            )
    
    def onInit(self, **kwargs):
        pass
    
    def update_depth(self, depth: Depth):
        """
        Args:
            depth (Depth): list of level class, 
            depth level default to 20.
            ask price 1: depth.asks[0].prc,
            ask quantity 1: depth.asks[0].qty,
            use resp_ts(in ms)
        """
        if self.pred_on:
            self.factor_calculator_base.update_depth(depth)
            
    def update_trade(self, trade: Trade):
        """
        Args:
            trade (Trade): 
            use local_time(in ms)
        """
        if self.pred_on:
            self.factor_calculator_base.update_trade(trade)

    def update_bbo(self, bbo: BBO):
        pass

    def load_ref_prc_model(self, pickle_address):
        self.factor_calculator_base.ref_prc_model.load_model(pickle_address)
    
    def load_ref_prc_params(self, json_files):
        self.factor_calculator_base.ref_prc_model.load_params(json_files)
    
    def predict_ref_prc_ret(self, ms: int):
        """
        Args:
            sec (int): delta time for pred_y
        """
        if self.pred_on:
            return self.factor_calculator_base.predict_ref_prc_ret(ms)
        else:
            return None
