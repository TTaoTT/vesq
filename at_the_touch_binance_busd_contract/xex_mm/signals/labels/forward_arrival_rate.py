import numpy as np
import pandas as pd
from xex_mm.signals.factors.past_arrival_rate import SinglePastArrivalRateNode

from xex_mm.utils.calc_node import DfCalcNode


class SingleForwardArrivalRateNode(DfCalcNode):
    def __init__(self, ex: str, contract: str, ar_t: int, fwd_t: int):
        self._ex = ex
        self._contract = contract
        self._ar_t = ar_t
        self._fwd_t = fwd_t
        self._past_node = SinglePastArrivalRateNode(ex=self._ex, contract=self._contract, t=self._ar_t)

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract},{self._fwd_t},{self._past_node}).v{2}"

    def _calc(self, datetime: str) -> pd.DataFrame:
        df = self._past_node.get(datetime=datetime)
        df.index -= self._fwd_t
        df.rename(columns=dict(lar="fwd_lar", sar="fwd_sar"), inplace=True)
        return df


class SingleForwardArrivalRateReturnNode(DfCalcNode):
    def __init__(self, ex: str, contract: str, ar_t: int, fwd_t: int):
        self._ex = ex
        self._contract = contract
        self._ar_t = ar_t
        self._fwd_t = fwd_t
        self._fwd_node = SingleForwardArrivalRateNode(ex=self._ex, contract=self._contract, ar_t=self._ar_t, fwd_t=self._fwd_t)

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract},{self._fwd_node}).v{2}"

    def _calc(self, datetime: str) -> pd.DataFrame:
        fwd_df = self._fwd_node.get(datetime=datetime)
        past_df = self._fwd_node._past_node.get(datetime=datetime)
        df = pd.concat([fwd_df, past_df], axis=1).ffill().loc[past_df.index[0]:fwd_df.index[-1], :]
        floor = 1e-10
        df["fwd_lar_ret"] = np.log(np.maximum(df["fwd_lar"], floor) / np.maximum(df["lar"].values, floor))
        df["fwd_sar_ret"] = np.log(np.maximum(df["fwd_sar"], floor) / np.maximum(df["sar"].values, floor))
        return df[["fwd_lar_ret", "fwd_sar_ret"]]


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    ex = "binance"
    contract = "btc_usdt"
    node = SingleForwardArrivalRateReturnNode(contract=contract, ex=ex, t=1000)
    df = node.get(datetime="2022072108", refresh=True)
    print(df.describe())
    df.plot()
    plt.show()
