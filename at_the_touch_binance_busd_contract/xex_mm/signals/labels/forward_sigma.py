import logging

import pandas as pd
import numpy as np

from xex_mm.mm_quoting_model.data_loader import DataLoader
from xex_mm.utils.calc_node import DfCalcNode

from typing import List
from xex_mm.utils.base import Level

class FowardSigmaNode(DfCalcNode):
    def __init__(self, ex: str, pair: str, t: float):
        self.__ex = ex
        self.__pair = pair
        self.__t = t
        self.__data_loader = DataLoader()

    def _get_name(self):
        return f"{self.__class__.__name__}({self.__ex},{self.__pair},{self.__t}).v{1}"
        
    def _calc(self, datetime: str) -> pd.DataFrame:

        logging.info(f"Getting depth queue...")
        depth_queue = self.__data_loader.get_depth_queue(ex=self.__ex, pair=self.__pair, datetime=datetime)

        logging.info(f"Starting calculation of forward sigma ...")
        cur_depth = depth_queue.popleft()
        sigma_arr = []
        ts_arr = []
        tmp_arr = []
        cur_ts = cur_depth.resp_ts
        while depth_queue:
            if cur_depth.resp_ts >= cur_ts + self.__t:
                sigma_arr.append(np.std(tmp_arr)/self.__t)
                ts_arr.append(cur_ts)
                cur_ts = cur_depth.resp_ts
            cur_depth = depth_queue.popleft()
            tmp_arr.append(cur_depth.mp)
            
        return pd.DataFrame({"forward_sigma": sigma_arr,
                             "ts": ts_arr}).set_index("ts")
