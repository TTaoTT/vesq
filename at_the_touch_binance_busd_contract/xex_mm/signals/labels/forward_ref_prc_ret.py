import datetime
import logging
import math
from collections import deque
from functools import partial

import pandas as pd
import numpy as np

from hf.CaliDataGen import BEIJIN, generate_orderbook
from xex_mm.mm_quoting_model.data_loader import DataLoader
from xex_mm.utils.calc_node import DfCalcNode

from typing import List, Deque, Tuple
from xex_mm.utils.base import Level, Depth, ReferencePriceCalculator
from xex_mm.utils.parallel import FunctorDistributionEngine


class FowardRefPrcRetNode(DfCalcNode):
    def __init__(self, ex: str, pair: str, t: float):
        self.__ex = ex
        self.__pair = pair
        self.__t = t
        self.__data_loader = DataLoader()

    def _get_name(self):
        return f"{self.__class__.__name__}({self.__ex},{self.__pair},{self.__t}).v{1}"
        
    def _calc(self, datetime: str) -> pd.DataFrame:

        logging.info(f"Getting depth queue...")
        depth_queue = self.__data_loader.get_depth_queue(ex=self.__ex, pair=self.__pair, datetime=datetime)

        logging.info(f"Starting calculation of forward reference price return ...")
        cur_depth = depth_queue.popleft()
        ref_prc_arr = []
        ts_arr = []
        cur_ts = cur_depth.resp_ts
        cur_mp = cur_depth.mp
        while depth_queue:
            if cur_depth.resp_ts >= cur_ts + self.__t:
                ref_prc_arr.append(np.log(cur_depth.mp/cur_mp))
                ts_arr.append(cur_ts)
                cur_ts = cur_depth.resp_ts
                cur_mp = cur_depth.mp
            cur_depth = depth_queue.popleft()
            
        return pd.DataFrame({"forward_ref_prc_ret": ref_prc_arr,
                             "ts": ts_arr}).set_index("ts")


class FowardRefPrcRetNode2(DfCalcNode):
    def __init__(self, ex: str, contract: str, T: int):
        self._ex = ex
        self._contract = contract
        self._T = T
        self._data_loader = DataLoader()
        self._ref_prc_calculator = ReferencePriceCalculator()

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract},{self._T}).v{1}"

    def _calc(self, datetime: str) -> pd.DataFrame:
        logging.info(f"Getting depth queue...")
        depth_queue = self._data_loader.get_depth_queue(ex=self._ex, pair=self._contract, datetime=datetime)

        logging.info(f"Starting calculation of forward reference price return ...")
        _ref_prc_q: Deque[Tuple[int, float]] = deque()
        data = []
        while depth_queue:
            depth = depth_queue.popleft()
            ts = depth.resp_ts
            ref_prc = self._ref_prc_calculator.calc_from_depth(depth=depth, amt_bnd=100)
            if len(_ref_prc_q) <= 1:
                _ref_prc_q.append((ts, ref_prc))
                continue
            if ts - _ref_prc_q[0][0] > self._T:
                _ts, _ref_prc = _ref_prc_q.popleft()
                _fwd_ret = math.log(_ref_prc_q[-1][1] / _ref_prc)
                data.append(dict(ts=_ts, fwd_ret=_fwd_ret))
            _ref_prc_q.append((ts, ref_prc))
        while _ref_prc_q:
            _fwd_ref_prc = _ref_prc_q[-1][1]
            _ts, _ref_prc = _ref_prc_q.popleft()
            while (len(_ref_prc_q) > 0) and (_ref_prc_q[0][0] <= _ts):
                _ts, _ref_prc = _ref_prc_q.popleft()
            _fwd_ret = math.log(_fwd_ref_prc / _ref_prc)
            data.append(dict(ts=_ts, fwd_ret=_fwd_ret))

        return pd.DataFrame(data).set_index("ts")


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import logging
    logging.getLogger().setLevel(logging.INFO)


    contract = "btc_usdt_swap"
    ex = "binance"
    dt = datetime.timedelta(hours=1)
    start_time = BEIJIN.localize(datetime.datetime(2022, 6, 10, 8))
    end_time = BEIJIN.localize(datetime.datetime(2022, 6, 20, 8))
    n_procs = 60

    generate_orderbook(contract, ex, start_time, end_time, dt, n_procs=n_procs)

    start_ts = pd.Timestamp(start_time)
    end_ts = pd.Timestamp(end_time)
    ts = start_ts
    tasks = []
    while ts < end_ts:
        o = FowardRefPrcRetNode2(contract="btc_usdt_swap", ex="binance", T=10)
        tasks.append(partial(o.get, datetime=ts.strftime("%Y%m%d%H"), refresh=False))
        ts += pd.Timedelta("1h")
    dfs = FunctorDistributionEngine(n_procs=n_procs).run(ftors=tasks)
    df = pd.concat(dfs, axis=0)
    df["fwd_ret"].abs().hist(bins=100)
    plt.show()
    print(df.describe(percentiles=np.arange(0.8, 1, 0.01)))