from collections import defaultdict
from typing import Dict, Tuple

from xex_mm.calculators.arrival_size import CalEMAvgArrivalSize, CalImpulseArrivalSize
from xex_mm.calculators.arrival_time import CalEMAvgArrivalTime, CalImpulseArrivalTime
from xex_mm.calculators.depth_shape import CalEMAvgDepthMetrics
from xex_mm.calculators.latency import CalEMAvgLatency
from xex_mm.calculators.order_fill_tracker import ConsecutiveOrderFillTracker
from xex_mm.calculators.ref_prc import CalEMAvgRefPrc
from xex_mm.calculators.ref_prc_return import CalEMAvgRet, CalImpulseRet
from xex_mm.calculators.trade_ret_lambda import TradeReturnLambdaCal
from xex_mm.calculators.trade_stability import CalTradeStability
from xex_mm.calculators.update_frequency import CalUpdateFrequency
from xex_mm.calculators.sigma import CalImpulseSigma, CalEMAvgSigma
from xex_mm.calculators.half_sprd import CalImpulseHalfSpread, CalEMAvgHalfSpread, CalSpreadMultiplier, \
    CalInstantHalfSpread
from xex_mm.utils.base import Depth, Trade, BBO
from xex_mm.utils.inventory import CalEMAvgInventorySigma


class LocalCalculatorBase:
    def __init__(self, art: float, sigmat: float, hst: float, rett: float, refprct: float,
                 latencyt: float, invent: float, dmt: float, quote_defense_t: int,
                 event_count_halflife: int, trl_count_halflife: int, trade_stability_buffer_window: int):
        self._art = art
        self._sigmat = sigmat
        self._hst = hst
        self._rett = rett
        self._refprct = refprct
        self._latencyt = latencyt
        self._invent = invent
        self._dmt = dmt
        self._quote_defense_t = quote_defense_t
        self._event_count_halflife = event_count_halflife
        self._trl_count_halflife = trl_count_halflife
        self._trade_stability_buffer_window = trade_stability_buffer_window
        self._setup_calib()

    def _setup_calib(self):
        self.ema_sigma_cal_map = defaultdict(lambda: CalEMAvgSigma(halflife=self._sigmat))
        self.impulse_sigma_cal_map = defaultdict(lambda: CalImpulseSigma(halflife=self._sigmat))
        self.ema_halfsprd_cal_map = defaultdict(lambda: CalEMAvgHalfSpread(halflife=self._hst))
        self.impulse_halfsprd_cal_map = defaultdict(lambda: CalImpulseHalfSpread(halflife=self._hst))
        self.instant_halfsprd_cal_map = defaultdict(lambda: CalInstantHalfSpread())
        self.ema_ret_cal_map = defaultdict(lambda: CalEMAvgRet(halflife=self._rett))
        self.impulse_ret_cal_map = defaultdict(lambda: CalImpulseRet(halflife=self._rett))
        self.latency_cal_map = defaultdict(lambda: CalEMAvgLatency(halflife=self._latencyt))
        self.ref_prc_cal_map = defaultdict(lambda: CalEMAvgRefPrc(halflife=self._refprct))
        self.arrival_size_cal_map = defaultdict(lambda: CalEMAvgArrivalSize(halflife=self._event_count_halflife))
        self.arrival_time_cal_map = defaultdict(lambda: CalEMAvgArrivalTime(halflife=self._event_count_halflife))
        self.impulse_arrival_size_cal_map = defaultdict(lambda: CalImpulseArrivalSize(halflife=self._event_count_halflife))
        self.impulse_arrival_time_cal_map = defaultdict(lambda: CalImpulseArrivalTime(halflife=self._event_count_halflife))
        self.inven_sigma_cal_map = defaultdict(lambda: CalEMAvgInventorySigma(halflife=self._invent))
        self.update_freq_cal_map = defaultdict(lambda: CalUpdateFrequency(halflife=self._event_count_halflife))
        self.depth_metrics_cal_map = defaultdict(lambda: CalEMAvgDepthMetrics(halflife=self._dmt))
        self.trade_ret_lambda_cal_map = defaultdict(lambda: TradeReturnLambdaCal(halflife=self._trl_count_halflife))
        self.sprd_mult_cal_map = defaultdict(lambda: CalSpreadMultiplier(halflife=self._quote_defense_t))
        self.trade_stability_cal_map = defaultdict(lambda: CalTradeStability(buffer_window=self._trade_stability_buffer_window))
        self.consecutive_fill_tracker_map: Dict[Tuple[str, str], ConsecutiveOrderFillTracker] = defaultdict(lambda: ConsecutiveOrderFillTracker())

    def update_depth(self, depth: Depth) -> bool:
        iid = (depth.meta_data.contract, depth.meta_data.ex)
        self.ema_ret_cal_map[iid].add_depth(depth=depth)
        self.impulse_ret_cal_map[iid].add_depth(depth=depth)
        self.ema_sigma_cal_map[iid].add_depth(depth=depth)
        self.impulse_sigma_cal_map[iid].add_depth(depth=depth)
        self.ema_halfsprd_cal_map[iid].add_depth(depth=depth)
        self.impulse_halfsprd_cal_map[iid].add_depth(depth=depth)
        self.instant_halfsprd_cal_map[iid].add_depth(depth=depth)
        self.latency_cal_map[iid].add_depth(depth=depth)
        self.ref_prc_cal_map[iid].add_depth(depth=depth)
        self.update_freq_cal_map[iid].add_depth(depth=depth)
        self.depth_metrics_cal_map[iid].add_depth(depth=depth)
        return True

    def update_trade(self, trade: Trade) -> bool:
        iid = (trade.contract, trade.ex)
        self.latency_cal_map[iid].add_trade(trade=trade)
        self.arrival_size_cal_map[iid].add_trade(trade=trade)
        self.arrival_time_cal_map[iid].add_trade(trade=trade)
        self.impulse_arrival_size_cal_map[iid].add_trade(trade=trade)
        self.impulse_arrival_time_cal_map[iid].add_trade(trade=trade)
        self.update_freq_cal_map[iid].add_trade(trade=trade)
        self.trade_ret_lambda_cal_map[iid].add_trade(trade=trade)
        self.sprd_mult_cal_map[iid].add_trade(trade=trade)
        self.trade_stability_cal_map[iid].add_trade(trade=trade)
        return True

    def update_bbo(self, bbo: BBO) -> bool:
        iid = (bbo.meta.contract, bbo.meta.ex)
        self.ema_sigma_cal_map[iid].add_bbo(bbo=bbo)
        self.impulse_sigma_cal_map[iid].add_bbo(bbo=bbo)
        self.ema_halfsprd_cal_map[iid].add_bbo(bbo=bbo)
        self.impulse_halfsprd_cal_map[iid].add_bbo(bbo=bbo)
        self.instant_halfsprd_cal_map[iid].add_bbo(bbo=bbo)
        self.ema_ret_cal_map[iid].add_bbo(bbo=bbo)
        self.impulse_ret_cal_map[iid].add_bbo(bbo=bbo)
        self.latency_cal_map[iid].add_bbo(bbo=bbo)
        self.ref_prc_cal_map[iid].add_bbo(bbo=bbo)
        self.update_freq_cal_map[iid].add_bbo(bbo=bbo)
        self.depth_metrics_cal_map[iid].add_bbo(bbo=bbo)
        return True

    """ Predict Function """

    def predict_ref_prc_ret(self, contract: str, ex: str, ts: int):
        return self.curr_emavg_ref_prc_ret(contract=contract, ex=ex, ts=ts)

    def predict_long_half_spread(self, contract: str, ex: str, ts: int):
        return self.curr_impulse_long_half_spread(contract=contract, ex=ex, ts=ts)

    def predict_short_half_spread(self, contract: str, ex: str, ts: int):
        return self.curr_impulse_short_half_spread(contract=contract, ex=ex, ts=ts)

    def predict_sigma(self, contract: str, ex: str, ts: int):
        return self.curr_ema_sigma(contract=contract, ex=ex, ts=ts)

    def pred_latency(self, contract: str, ex: str, ts: int):
        return self.curr_emavg_latency(contract=contract, ex=ex, ts=ts)

    def pred_trade_arrival_size(self, contract: str, ex: str, ts: int):
        return self.curr_trade_arrival_size(contract=contract, ex=ex, ts=ts)

    def pred_trade_arrival_time(self, contract: str, ex: str, ts: int):
        return self.curr_trade_arrival_time(contract=contract, ex=ex, ts=ts)

    """ Current Stats """

    def curr_impulse_ref_prc_ret(self, contract: str, ex: str, ts: int):
        return self.impulse_ret_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_emavg_ref_prc_ret(self, contract: str, ex: str, ts: int):
        return self.ema_ret_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_emavg_ref_prc(self, contract: str, ex: str, ts: int):
        return self.ref_prc_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_emavg_long_half_spread(self, contract: str, ex: str, ts: int):
        return self.ema_halfsprd_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_emavg_short_half_spread(self, contract: str, ex: str, ts: int):
        return self.ema_halfsprd_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_impulse_long_half_spread(self, contract: str, ex: str, ts: int):
        return self.impulse_halfsprd_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_impulse_short_half_spread(self, contract: str, ex: str, ts: int):
        return self.impulse_halfsprd_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_ema_sigma(self, contract: str, ex: str, ts: int):
        return self.ema_sigma_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_impulse_sigma(self, contract: str, ex: str, ts: int):
        return self.impulse_sigma_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_emavg_latency(self, contract: str, ex: str, ts: int):
        return self.latency_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_trade_arrival_size(self, contract: str, ex: str, ts: int):
        return self.arrival_size_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_trade_arrival_time(self, contract: str, ex: str, ts: int):
        return self.arrival_time_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_inven_sigma(self, contract: str, ex: str, ts: int):
        return self.inven_sigma_cal_map[(contract, ex)].get_factor(ts=ts)

    def curr_depth_metrics(self, contract: str, ex: str, ts: int):
        return self.depth_metrics_cal_map[(contract, ex)].get_factor(ts=ts)

