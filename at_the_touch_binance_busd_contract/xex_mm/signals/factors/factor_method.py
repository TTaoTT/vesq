import time
import numpy as np
from xex_mm.utils.base import Depth, Trade

"""  
self.data_source[ms] = dict(
    _cache_depth_ = list(),
    _cache_prc_ = list(),
    _cache_buys_ = list(),
    _cache_sells_ = list(),           
)
"""

class Base():
    def __init__(self) -> None:
        self._factor_value_ = dict()
        self._history_data_ = dict()
    
    def update_depth(self, depth:Depth):
        pass
    
    def update_trade(self, trade:Trade):
        pass
    
class PastRetQuantile(Base):
    
    def __init__(self) -> None:
        super().__init__()  
    
    def update_factor(self, DataClass):
        data_source = DataClass.data_source[5000]
        past_ret = data_source["_cache_prc_"][-1] / data_source["_cache_prc_"][0] - 1
        past_ret_1 = (past_ret > 0.001)*(1) + (past_ret < -0.001)*(-1)
        past_ret_2 = (past_ret > 0.0005)*(1) + (past_ret < -0.0005)*(-1)  
        self._factor_value_ = {
            "past_ret":past_ret,
            "past_ret_1":past_ret_1,
            "past_ret_2":past_ret_2,           
        }      

class RelatedDepthFactor(Base):
    def __init__(self) -> None:
        super().__init__()
        
    def update_factor(self, DataClass, RelatedData):
        data_source = DataClass.data_source[5000]
        related_data= RelatedData.data_source[5000]
        if len(data_source["_cache_prc_"]) > 1 and len(related_data["_cache_prc_"]) > 1:
            rprc1 = related_data["_cache_prc_"][0]
            rprc2 = related_data["_cache_prc_"][-1]
            tprc1 = data_source["_cache_prc_"][0]
            tprc2 = data_source["_cache_prc_"][-1]
            self._factor_value_ = {
                "last_diff": tprc2/tprc1 - rprc2/rprc1,
                "prc_diff": tprc2/rprc2 - 1
        }

class OneSideLevelSpread(Base):
    def __init__(self) -> None:
        super().__init__()
        
    def update_factor(self, DataClass):
        data_source = DataClass.data_source[5000]
        depth = data_source["_cache_depth_"][-1]
        ap1_ap5_spread = depth.asks[4].prc - depth.asks[0].prc
        bp1_bp5_spread = depth.bids[0].prc - depth.bids[4].prc
        self._factor_value_ = {
            "ap1_ap5_spread":ap1_ap5_spread,
            "bp1_bp5_spread":bp1_bp5_spread          
        }        

class StepOrderImbalanceRatio(Base):
    def __init__(self) -> None:
        super().__init__()
        
    def update_factor(self, DataClass):
        data_source = DataClass.data_source[5000]
        depth = data_source["_cache_depth_"][-1]  
        weight = np.array([20-i for i in range(20)])     
        av20 = sum([depth.asks[i].prc * depth.asks[i].qty * weight[i] for i in range(0, 20)])
        bv20 =  sum([depth.bids[i].prc * depth.bids[i].qty * weight[i] for i in range(0, 20)])
        self._factor_value_ = {
            "step_20_oibr":(av20-bv20)/(av20+bv20)          
        }
        
class OriginalVpin(Base):
    def __init__(self) -> None:
        super().__init__()
        
    def update_factor(self, DataClass):
        data_source = DataClass.data_source[5000]
        if len(data_source["_cache_buys_"]) > 1:
            buy_sum =sum([trade.price * trade.quantity for trade in data_source["_cache_buys_"][1:]])
        else:
            buy_sum = 0
        if len(data_source["_cache_sells_"]) > 1: 
            sell_sum =sum([trade.price * trade.quantity for trade in data_source["_cache_sells_"][1:]])   
        else:
            sell_sum = 0
        self._factor_value_ = {"vpin":(buy_sum - sell_sum)/(buy_sum + sell_sum + 1e-7)} 
        
class ArrivalRateEma(Base):
    def __init__(self) -> None:
        super().__init__() 
        self._history_data_["buy_ema"] = 0
        self._history_data_["buy_ms"] = 0
        self._history_data_["sell_ema"] = 0 
        self._history_data_["sell_ms"] = 0
        self.alpha = 1/(1+0.5)                
    
    def update_trade(self, trade:Trade):    
        if trade.side == 1:
            decay = trade.local_time - self._history_data_["buy_ms"]
            if decay == 0:
                self._history_data_["buy_ema"] += trade.quantity
            else:
                self._history_data_["buy_ema"] = self._history_data_["buy_ema"]*((1-self.alpha)**decay) + self.alpha*trade.quantity
            self._history_data_["buy_ms"] = trade.local_time
        else:
            decay = trade.local_time - self._history_data_["sell_ms"]
            if decay == 0:
                self._history_data_["sell_ema"] += trade.quantity
            else:
                self._history_data_["sell_ema"] = self._history_data_["sell_ema"]*((1-self.alpha)**decay) + self.alpha*trade.quantity
            self._history_data_["sell_ms"] = trade.local_time            
        
    def update_factor(self, DataClass):
        buy_ema = self._history_data_["buy_ema"]
        sell_ema = self._history_data_["sell_ema"]
        self._factor_value_ = {
            "buy_ema":buy_ema,
            "sell_ema":sell_ema,
            "arrival_ratio":buy_ema/(buy_ema+sell_ema+1e-7)
        }

class OverVolumeRatio(Base):
    def __init__(self) -> None:
        super().__init__()
        
    def update_factor(self, DataClass):
        data_source = DataClass.data_source[5000]    
        depth = data_source["_cache_depth_"][-1]       
        askp, bidp = depth.asks[0].prc, depth.bids[0].prc
        
        over_ask_buy, over_ask_sell, over_bid_buy, over_bid_sell = 0, 0, 0, 0
        
        if len(data_source["_cache_buys_"]) > 1:
            buys = data_source["_cache_buys_"][1:]
            for trade in buys:
                if trade.price > askp:
                    over_ask_buy += trade.price * trade.quantity
                elif trade.price < bidp:
                    over_bid_buy += trade.price * trade.quantity            
        
        if len(data_source["_cache_sells_"]) > 1:
            sells = data_source["_cache_sells_"][1:]
            for trade in sells:
                if trade.price < bidp:
                    over_bid_sell += trade.price * trade.quantity
                elif trade.price > askp:
                    over_ask_sell += trade.price * trade.quantity
        
        
        self._factor_value_ = {
            "over_hold_ratio":(over_bid_buy-over_ask_sell)/(over_bid_buy+over_ask_sell+1e-7),
            "over_push_ratio":(over_ask_buy-over_bid_sell)/(over_ask_buy+over_bid_sell+1e-7),  
            "over_volume_ratio":(over_ask_buy+over_ask_sell-over_bid_buy-over_bid_sell) \
                            /(over_ask_buy+over_ask_sell+over_bid_buy+over_bid_sell+1e-7)
        }       

class ConsumeQtyRatio(Base):
    def __init__(self) -> None:
        super().__init__()
        
    def update_factor(self, DataClass):
        data_source = DataClass.data_source[5000]    
        depth = data_source["_cache_depth_"][-1]       
        
        if len(data_source["_cache_buys_"]) > 1:
            buy_prices = [trade.price for trade in data_source["_cache_buys_"][1:]]
        else:
            buy_prices = list()
        
        if len(data_source["_cache_sells_"]) > 1:
            sell_prices = [trade.price for trade in data_source["_cache_sells_"][1:]]
        else:
            sell_prices = list()
        
        price = buy_prices
        price.extend(sell_prices)
        
        if len(price) == 0:
            consume_notional_ratio = 0
        else:
            max_price = max(price)
            max_consume = 0
            for item in depth.asks:
                if item.prc < max_price:
                    max_consume += item.prc * item.qty
                else:
                    break        
            
            min_price = min(price)
            min_consume = 0
            for item in depth.bids:
                if item.prc > min_price:
                    min_consume += item.prc * item.qty
                else:
                    break
            consume_notional_ratio = (max_consume-min_consume)/(max_consume+min_consume+1e-7)
        
        self._factor_value_ = {
            "consume_notional_ratio":consume_notional_ratio
        }
        