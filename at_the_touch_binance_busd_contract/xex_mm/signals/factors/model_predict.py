import json
import pandas as pd
import numpy as np
from statsmodels.iolib.smpickle import load_pickle

class RefPrcModel():
    
    def __init__(self) -> None:
        self.model = None
        self.params = None

    def load_model(self, pick_file):
        self.model = load_pickle(pick_file)
        self.model_sec = int(pick_file.split(".")[-2])/1000
        self.x_columns = list(self.model.params.keys())
        self.x_nums = len(self.x_columns)
        # print(self.x_columns)
    
    def load_params(self, json_file):
        pass
        # with open(json_file,"r") as f:
        #     self.params = json.loads(f)
        
    def predict_1sec(self, _dict):
        X = np.array([_dict[name] for name in self.x_columns])
        return np.tanh(self.model.predict(X))[0] / self.model_sec
        
class ArrivalRateModel():
    
    def __init__(self) -> None:
        pass
    
class CancelRateModel():
    
    def __init__(self) -> None:
        pass