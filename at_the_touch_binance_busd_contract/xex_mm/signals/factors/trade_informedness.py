import logging
import math
from typing import Dict

import numpy as np
import pandas as pd

from xex_mm.mm_quoting_model.arrival_rate_model.utils.consts import ExchangeNames
from xex_mm.mm_quoting_model.data_loader import DataLoader
from xex_mm.utils.base import ReferencePriceCalculator, Depth
from xex_mm.utils.enums import Direction
from xex_mm.utils.calc_node import DfCalcNode, CalRealTime
from xex_mm.utils.base import Trade


class TradeInformednessCalculator:
    def __init__(self, contract: str, ema_halflife: float):
        self._contract = contract
        self._ema_halflife = ema_halflife
        self._ema_lambda = 0.5 ** (1 / self._ema_halflife)

        self._qty_ema_map: Dict = {}
        self._ref_prc = np.nan
        self._I_map: Dict = {}
        self._got_trd = False
        self._got_depth = False
        self._alpha_map: Dict = {}
        self._alpha = 0

        self._data_loader = DataLoader()
        self._ref_prc_calculator = ReferencePriceCalculator()
        self._informness_map = self._data_loader.get_mexc_informness_map(pair=self._contract)
        self._default_informedness = 1e-4

    ######################################### STATE MANAGEMENT #######################################

    def _update_qty_ema(self, uid: int, trd: Trade):
        if uid not in self._qty_ema_map:
            self._qty_ema_map[uid] = trd.quantity
            return
        self._qty_ema_map[uid] *= self._ema_lambda
        self._qty_ema_map[uid] += trd.quantity

    def _update_inventory(self, uid: int, trd: Trade):
        if uid not in self._I_map:
            self._I_map[uid] = 0
        self._I_map[uid] += (trd.side * trd.quantity)

    def on_depth(self, depth: Depth):
        self._ref_prc = self._ref_prc_calculator.calc_from_depth(depth=depth)
        self._got_depth = True

    def on_trade(self, trd: Trade):
        self._update_qty_ema(uid=trd.mexc_uid, trd=trd)
        self._update_inventory(uid=trd.mexc_uid, trd=trd)
        self._got_trd = True

    ######################################### STATE MANAGEMENT #######################################

    def _qty_agg(self, uid: int, qty: float):
        # return qty
        return abs(qty) / self._qty_ema_map[uid]

    def _prc_agg(self, prc: float, d: Direction):
        if d == Direction.long:
            return prc / self._ref_prc
        else:
            return self._ref_prc / prc

    def _inv_agg(self, uid: int, qty: float, d: Direction):
        I = self._I_map[uid]
        if I == 0:
            return 1
        I_d = 1 if d == Direction.long else -1
        return abs(I + qty * I_d) / abs(I)

    def _informedness(self, uid: int):
        return self._informness_map.get(uid, self._default_informedness)

    def _d(self, trd: Trade):
        if trd.ex == ExchangeNames.mexc:
            if trd.side == 1:
                return Direction.long
            elif trd.side == -1:
                return Direction.short
            else:
                raise RuntimeError()
        elif trd.ex == ExchangeNames.binance:
            if trd.side == 1:
                return Direction.long
            elif trd.side == -1:
                return Direction.short
            else:
                raise RuntimeError()
        else:
            raise NotImplementedError()

    def update_informedness(self, trd: Trade):
        if not self._got_trd or not self._got_depth:
            return
        qty_agg = self._qty_agg(uid=trd.mexc_uid, qty=trd.quantity)
        d = self._d(trd=trd)
        inv_agg = self._inv_agg(qty=trd.quantity, d=d, uid=trd.mexc_uid)
        prc_agg = self._prc_agg(prc=trd.price, d=d)
        informedness = self._informedness(uid=trd.mexc_uid)
        alpha = trd.side * qty_agg * inv_agg * prc_agg * informedness
        prev_alpha = self._alpha_map.get(trd.mexc_uid, 0)
        self._alpha -= prev_alpha
        self._alpha += alpha
        self._alpha_map[trd.mexc_uid] = alpha

    def get_alpha(self):
        return self._alpha


class PerTickTradeInformednessNode(DfCalcNode):
    def __init__(self, ex: str, contract: str):
        self._ex = ex
        self._contract = contract
        self._data_loader = DataLoader()

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract}).v{1}"

    def _calc(self, datetime: str) -> pd.DataFrame:

        logging.info(f"Getting depth queue...")
        depth_queue = self._data_loader.get_depth_queue(ex=self._ex, pair=self._contract, datetime=datetime)
        logging.info(f"Getting trade queue...")
        trade_queue = self._data_loader.get_trade_queue(ex=self._ex, pair=self._contract, datetime=datetime)
        logging.info(f"Getting mexc informedness...")

        logging.info(f"Starting calculation of trade informness...")
        calculator = TradeInformednessCalculator(contract=self._contract, ema_halflife=10)
        data = []
        _prev_ts = -1
        while depth_queue and trade_queue:
            if depth_queue[0].resp_ts >= trade_queue[0].local_time:
                ts = trade_queue[0].local_time
                trd: Trade = None
                while (len(trade_queue) > 0) and (trade_queue[0].local_time <= ts):
                    trd = trade_queue.popleft()
                    trd = trd.clone()
                    trd.mexc_uid = -1
                    calculator.on_trade(trd=trd)
                    calculator.update_informedness(trd=trd)
                data.append(dict(
                    informedness=calculator.get_alpha(),
                    uid=trd.mexc_uid,
                    ts=ts,
                ))
                assert ts > _prev_ts
                _prev_ts = ts
                continue
            ts = depth_queue[0].resp_ts
            while (len(depth_queue) > 0) and (depth_queue[0].resp_ts <= ts):
                depth = depth_queue.popleft()
                calculator.on_depth(depth=depth)
        return pd.DataFrame(data).set_index("ts")


class CalTradeInformedness(CalRealTime):
    def __init__(self,t: int, pair: str) -> None:
        self.__data_loader = DataLoader()
        self.__informness_map = self.__data_loader.get_mexc_informness_map(pair=pair)
        self.__factor_arr = []
        self.__factor = 0
        self.__ts_arr = []
        self.__t = t
        
    def add_trade(self, trade: Trade):
        if trade.mexc_uid > 0:
            pred = self.__informness_map[str(trade.mexc_uid)] * trade.quantity * trade.side
            self.__factor_arr.append(pred)
            self.__factor += pred
            self.__ts_arr.append(trade.local_time)
        
        while self.__ts_arr and self.__ts_arr[0] < trade.local_time - self.__t:
            self.__ts_arr.pop(0)
            f = self.__factor_arr.pop(0)
            self.__factor -= f
    
    def get_factor(self):
        return self.__factor


class TradeInformednessNode(DfCalcNode):
    def __init__(self, ex: str, pair: str, t: float):
        self.__ex = ex
        self.__pair = pair
        self.__t = t
        self.__data_loader = DataLoader()

    def _get_name(self):
        return f"{self.__class__.__name__}({self.__ex},{self.__pair},{self.__t}).v{1}"

    def _calc(self, datetime: str) -> pd.DataFrame:

        logging.info(f"Getting depth queue...")
        depth_queue = self.__data_loader.get_depth_queue(ex=self.__ex, pair=self.__pair, datetime=datetime)
        logging.info(f"Getting trade queue...")
        trade_queue = self.__data_loader.get_trade_queue(ex=self.__ex, pair=self.__pair, datetime=datetime)
        logging.info(f"Getting mexc informedness...")
        informness_map = self.__data_loader.get_mexc_informness_map(pair=self.__pair)

        logging.info(f"Starting calculation of trade informness...")
        cur_depth = depth_queue.popleft()
        cur_trade = trade_queue.popleft()
        ts_arr = []
        predictor_arr = []
        cur_ts = cur_depth.resp_ts
        last_predictor = 0
        cur_predictor = 0
        while depth_queue and trade_queue:
            if cur_trade.local_time - cur_ts > self.__t:
                uid = cur_trade.mexc_uid
                if uid and informness_map.get(str(uid)):
                    cur_predictor += informness_map[str(uid)] * cur_trade.quantity * cur_trade.side
                ts_arr.append(cur_ts)
                predictor_arr.append(last_predictor)
                while cur_depth.resp_ts <= cur_trade.local_time:
                    cur_depth = depth_queue.popleft()
                while cur_depth.resp_ts > cur_trade.local_time:
                    cur_trade = trade_queue.popleft()
                cur_ts = cur_depth.resp_ts
                last_predictor = cur_predictor
                cur_predictor = 0

            else:
                uid = cur_trade.mexc_uid
                if uid and informness_map.get(str(uid)):
                    cur_predictor += informness_map[str(uid)] * cur_trade.quantity * cur_trade.side
                cur_trade = trade_queue.popleft()

        return pd.DataFrame({"informedness": predictor_arr,
                             "ts": ts_arr}).set_index("ts")


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    df = PerTickTradeInformednessNode(ex="xex", contract="axs_usdt_swap").get(datetime="2022072108", refresh=True)
    print(df)
    df[["informedness"]].plot()
    plt.show()