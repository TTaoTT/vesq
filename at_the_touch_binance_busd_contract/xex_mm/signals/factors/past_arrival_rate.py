
import datetime
from collections import deque
from functools import partial

import numpy as np
import pandas as pd
from typing import Deque

from hf.CaliDataGen import BEIJIN, generate_orderbook
from xex_mm.mm_quoting_model.data_loader import DataLoader
from xex_mm.utils.calc_node import DfCalcNode
from xex_mm.utils.base import Trade

from xex_mm.calculators.arrival_rate import RollingWindowArrivalRateCal, \
    CalImpulseArrivalRate, CalEMAvgArrivalRate
from xex_mm.utils.parallel import FunctorDistributionEngine


class SinglePastArrivalRateNode(DfCalcNode):
    def __init__(self, ex: str, contract: str, t: int):
        self._ex = ex
        self._contract = contract
        self._t = t
        self._data_loader = DataLoader()

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract},{self._t}).v{10}"

    def _get_left_trds(self, trd_q: Deque[Trade]) -> Deque[Trade]:
        left_trade = trd_q.popleft()
        left_trades = deque([left_trade])
        if len(trd_q) == 0:
            return left_trades
        while (len(trd_q) > 0) and (trd_q[0].local_time <= left_trade.local_time):
            trd = trd_q.popleft()
            if trd.local_time < left_trade.local_time:
                raise RuntimeError("Trade flow is not monotonically increasing in local time?")
            left_trades.append(trd)
        return left_trades

    def _calc(self, datetime: str) -> pd.DataFrame:

        logging.info(f"Getting trade queue...")
        trd_q = self._data_loader.get_trade_queue(ex=self._ex, pair=self._contract, datetime=datetime)

        logging.info(f"Starting calculation of past arrival rate...")
        cal = RollingWindowArrivalRateCal(t=self._t)
        _wakeup_q = deque()
        data = []
        while (len(trd_q) > 0) or (len(_wakeup_q) > 0):
            if (len(trd_q) > 0) and (len(_wakeup_q) > 0):
                left_trade_ts = trd_q[0].local_time
                left_wakeup_ts = _wakeup_q[0]
                if left_trade_ts <= left_wakeup_ts:
                    left_trades = self._get_left_trds(trd_q=trd_q)
                    if left_trade_ts == left_wakeup_ts:
                        _wakeup_q.popleft()
                    for trd in left_trades:
                        cal.add_trade(trade=trd)
                    _wakeup_q.append(left_trade_ts + self._t)
                    lar, sar = cal.get_factor()
                    data.append(dict(lar=lar, sar=sar, ts=left_trade_ts))
                else:
                    wakeup_ts = _wakeup_q.popleft()
                    cal.on_wakeup(ts=wakeup_ts)
                    _wakeup_q.append(wakeup_ts + self._t)
                    lar, sar = cal.get_factor()
                    data.append(dict(lar=lar, sar=sar, ts=wakeup_ts))
            elif (len(trd_q) > 0) and (len(_wakeup_q) == 0):
                left_trades = self._get_left_trds(trd_q=trd_q)
                for trd in left_trades:
                    cal.add_trade(trade=trd)
                ts = left_trades[0].local_time
                _wakeup_q.append(ts + self._t)
                lar, sar = cal.get_factor()
                data.append(dict(lar=lar, sar=sar, ts=ts))
            elif len(trd_q) == 0:
                break
                # wakeup_ts = _wakeup_q.popleft()
                # cal.on_wakeup(ts=wakeup_ts)
                # lar, sar = cal.get_factor()
                # data.append(dict(lar=lar, sar=sar, ts=wakeup_ts))
            else:
                raise RuntimeError("Logically impossible to be here.")

        df = pd.DataFrame(data).set_index("ts")
        if not df.index.is_unique:
            raise RuntimeError("Timestamp is not unique?")

        return df


class PastArrivalRateNode(DfCalcNode):
    def __init__(self, ex: str, contract: str, halflife: float):
        self._ex = ex
        self._contract = contract
        self._data_loader = DataLoader()
        self._halflife = halflife
        self._calculator = CalEMAvgArrivalRate(halflife=self._halflife)

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract},{self._calculator}).v7"

    def _calc(self, datetime: str) -> pd.DataFrame:

        logging.info(f"Getting trade queue...")
        trd_q = self._data_loader.get_trade_queue(ex=self._ex, pair=self._contract, datetime=datetime)
        logging.info(f"Getting depth queue...")
        depth_q = self._data_loader.get_depth_queue(ex=self._ex, pair=self._contract, datetime=datetime)

        logging.info(f"Starting calculation of past arrival rate...")

        data = []
        while (len(trd_q) > 0) or (len(depth_q) > 0):
            if len(depth_q) == 0:
                trd = trd_q.popleft()
                self._calculator.add_trade(trade=trd)
                lar, lrr, sar, srr = self._calculator.record(ts=trd.local_time)
                ts = trd.local_time
            elif len(trd_q) == 0:
                depth = depth_q.popleft()
                self._calculator.add_depth(depth=depth)
                lar, lrr, sar, srr = self._calculator.record(ts=depth.resp_ts)
                ts = depth.resp_ts
            elif trd_q[0].local_time < depth_q[0].resp_ts:
                trd = trd_q.popleft()
                self._calculator.add_trade(trade=trd)
                lar, lrr, sar, srr = self._calculator.record(ts=trd.local_time)
                ts = trd.local_time
            elif trd_q[0].local_time > depth_q[0].resp_ts:
                depth = depth_q.popleft()
                self._calculator.add_depth(depth=depth)
                lar, lrr, sar, srr = self._calculator.record(ts=depth.resp_ts)
                ts = depth.resp_ts
            else:  # trd_q[0].local_time == depth_q[0].resp_ts:
                trd = trd_q.popleft()
                depth = depth_q.popleft()
                self._calculator.add_trade(trade=trd)
                self._calculator.add_depth(depth=depth)
                lar, lrr, sar, srr = self._calculator.record(ts=depth.resp_ts)
                ts = trd.local_time
            data.append(dict(
                ts=ts,
                lar=lar,
                sar=sar,
                lrr=lrr,
                srr=srr,
                larr=lar - lrr,
                sarr=sar - srr,
            ))

        df = pd.DataFrame(data).set_index('ts')
        return df


class SinglePastArrivalRateReturnNode(DfCalcNode):
    def __init__(self, ex: str, contract: str, ar_t: int, fwd_t: int):
        self._ex = ex
        self._contract = contract
        self._ar_t = ar_t
        self._fwd_t = fwd_t
        self._past_node = SinglePastArrivalRateNode(ex=self._ex, contract=self._contract, t=self._ar_t)

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._contract},{self._fwd_t},{self._past_node}).v{3}"

    def _calc(self, datetime: str) -> pd.DataFrame:
        df = self._past_node.get(datetime=datetime)
        r_df = df.rename(columns=dict(lar="r_lar", sar="r_sar"))
        l_df = pd.DataFrame(index=df.index + self._fwd_t, data=df.values, columns=[f"l_{c}" for c in df.columns])
        df = pd.concat([l_df, r_df], axis=1).ffill().loc[l_df.index[0]:r_df.index[-1], :]
        floor = 1e-10
        ret_df = np.log(np.maximum(df[["r_lar", "r_sar"]], floor) / np.maximum(df[["l_lar", "l_sar"]].values, floor))
        ret_df.rename(columns=dict(r_lar="lar_ret", r_sar="sar_ret"), inplace=True)
        return ret_df


if __name__ == '__main__':

    import logging
    logging.getLogger().setLevel(logging.INFO)
    # df = SinglePastArrivalRateNode(ex="xex", contract="axs_usdt_swap", t=1).get(datetime="2022072108", refresh=True)
    # print(df.describe())
    import matplotlib.pyplot as plt
    ex = "binance"
    contract = "btc_usdt_swap"

    start_time = BEIJIN.localize(datetime.datetime(2022, 6, 19, 8))
    end_time = BEIJIN.localize(datetime.datetime(2022, 6, 20, 8))
    dt = datetime.timedelta(hours=1)
    n_procs = 60

    generate_orderbook(contract, ex, start_time, end_time, dt, n_procs=n_procs)

    o = PastArrivalRateNode(ex=ex, contract=contract, halflife=100)
    start_ts = pd.Timestamp(start_time)
    end_ts = pd.Timestamp(end_time)
    ts = start_ts
    tasks = []
    while ts < end_ts:
        logging.info(f"ts={ts}")
        tasks.append(partial(o.get, datetime=ts.strftime("%Y%m%d%H"), refresh=True))
        ts += pd.Timedelta("1h")
    dfs = FunctorDistributionEngine(n_procs=n_procs).run(ftors=tasks)
    df = pd.concat(dfs, axis=0)
    # df["lar"].hist(bins=100)
    df.head(10000)[["lar", "sar", "lrr", "srr"]].plot(figsize=(20, 10))
    plt.show()
    print(df.describe(percentiles=np.arange(0.8, 1, 0.01)))
