import numpy as np
import time
from xex_mm.utils.base import Depth, Trade, BBO
from xex_mm.signals.factors.prepare_data import PrePareData
from xex_mm.signals.factors.model_predict import RefPrcModel
from xex_mm.signals.factors.factor_method import (
    PastRetQuantile,RelatedDepthFactor, OneSideLevelSpread, 
    StepOrderImbalanceRatio, OriginalVpin, ArrivalRateEma, 
    ConsumeQtyRatio,
    )

class FactorCalculatorBase:
    
    def __init__(self, trade_symbol:str, related_symbols:list(), params:dict()):
        
        self.trade_symbol = trade_symbol
        self.related_symbols = related_symbols
        self.params = params

        self.init_data_class()
        self.init_factor_map()
        self.init_models()
    
    def init_data_class(self):
        self.prepare_data_class = dict()
        self.prepare_data_class[self.trade_symbol] = PrePareData(self.trade_symbol)
        for related_symbol in self.related_symbols:
            self.prepare_data_class[related_symbol] = PrePareData(related_symbol)
     
    def init_factor_map(self):
        self.factor_map = dict()
        self.factor_map["PastRetQuantile"] = PastRetQuantile()
        self.factor_map["RelatedDepthFactor"] = RelatedDepthFactor()
        self.factor_map["OneSideLevelSpread"] = OneSideLevelSpread()
        self.factor_map["StepOrderImbalanceRatio"] = StepOrderImbalanceRatio()
        self.factor_map["OriginalVpin"] = OriginalVpin()
        self.factor_map["ArrivalRateEma"] = ArrivalRateEma()
        self.factor_map["ConsumeQtyRatio"] = ConsumeQtyRatio()            
    
    def init_models(self):
        self.ref_prc_model = RefPrcModel()
        # self.ref_prc_model.load_model(pick_address)
        
    def update_depth(self, depth:Depth):
        iid = f"""{depth.meta_data.ex}.{depth.meta_data.contract}"""  
        self.prepare_data_class[iid].update_depth(depth) 
        if iid == self.trade_symbol:
            self.factor_map["PastRetQuantile"].update_factor(self.prepare_data_class[self.trade_symbol])
            self.factor_map["RelatedDepthFactor"].update_factor(self.prepare_data_class[self.trade_symbol], self.prepare_data_class[self.related_symbols[0]])
            self.factor_map["OneSideLevelSpread"].update_factor(self.prepare_data_class[self.trade_symbol])
            self.factor_map["StepOrderImbalanceRatio"].update_factor(self.prepare_data_class[self.trade_symbol])
            self.factor_map["ConsumeQtyRatio"].update_factor(self.prepare_data_class[self.trade_symbol])
    
    def update_trade(self, trade:Trade):
        iid = f"""{trade.ex}.{trade.contract}"""
        self.prepare_data_class[iid].update_trade(trade)
        if iid == self.trade_symbol:
            self.factor_map["ArrivalRateEma"].update_trade(trade)
            self.factor_map["ArrivalRateEma"].update_factor(self.prepare_data_class[self.trade_symbol])
            self.factor_map["OriginalVpin"].update_factor(self.prepare_data_class[self.trade_symbol])
    
    def map_all_factor(self):
        _dict = dict()
        for _, _class_ in self.factor_map.items():
            _dict.update(_class_._factor_value_)      
        return _dict 
    
    def predict_ref_prc_ret(self, ms):
        all_factor = self.map_all_factor()
        if len(all_factor) < self.ref_prc_model.x_nums:
            return None
        else:
            p1 = self.ref_prc_model.predict_1sec(all_factor) 
            return ms * p1 / 1000