import datetime
from functools import partial

import pandas as pd
import numpy as np

from hf.CaliDataGen import BEIJIN, generate_orderbook
from xex_mm.calculators.sigma import CalImpulseSigma
from xex_mm.mm_quoting_model.data_loader import DataLoader
from xex_mm.utils.calc_node import DfCalcNode

from xex_mm.utils.parallel import FunctorDistributionEngine


class PastSigmaNode(DfCalcNode):
    def __init__(self, ex: str, pair: str, t: float):
        self._ex = ex
        self._pair = pair
        self._data_loader = DataLoader()
        self._sigma_cal = CalImpulseSigma(halflife=t)

    def _get_name(self):
        return f"{self.__class__.__name__}({self._ex},{self._pair},{self._sigma_cal}).v{1}"

    def _calc(self, datetime: str) -> pd.DataFrame:
        logging.info(f"Getting depth queue...")
        depth_queue = self._data_loader.get_depth_queue(ex=self._ex, pair=self._pair, datetime=datetime)

        logging.info(f"Starting calculation of past sigma ...")
        data = []
        while depth_queue:
            cur_depth = depth_queue.popleft()
            self._sigma_cal.add_depth(depth=cur_depth)
            sigma = self._sigma_cal.get_factor(ts=cur_depth.resp_ts)
            data.append(dict(
                sigma=sigma,
                ts=cur_depth.resp_ts,
            ))

        return pd.DataFrame(data).set_index("ts")


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import logging
    logging.getLogger().setLevel(logging.INFO)

    contract = "btc_usdt_swap"
    ex = "binance"
    dt = datetime.timedelta(hours=1)
    start_time = BEIJIN.localize(datetime.datetime(2022, 6, 19, 8))
    end_time = BEIJIN.localize(datetime.datetime(2022, 6, 20, 8))
    n_procs = 60

    generate_orderbook(contract, ex, start_time, end_time, dt, n_procs=n_procs)

    start_ts = pd.Timestamp(start_time)
    end_ts = pd.Timestamp(end_time)
    ts = start_ts
    tasks = []
    while ts < end_ts:
        o = PastSigmaNode(pair="btc_usdt_swap", ex="binance", t=1000)
        tasks.append(partial(o.get, datetime=ts.strftime("%Y%m%d%H"), refresh=False))
        ts += pd.Timedelta("1h")
    dfs = FunctorDistributionEngine(n_procs=n_procs).run(ftors=tasks)
    df = pd.concat(dfs, axis=0)
    df.sigma.head(1000).plot(figsize=(20, 10))
    # df["half_sprd"].hist(bins=100)
    plt.show()
    print(df.describe(percentiles=np.arange(0.8, 1, 0.01)))