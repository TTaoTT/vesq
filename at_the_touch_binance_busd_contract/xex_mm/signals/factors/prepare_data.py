import time
from xex_mm.utils.base import Depth, Trade


class PrePareData():
    
    def __init__(self, symbol_name:str):
        
        self.symbol_name = symbol_name
        self.current_ts = None
        self.data_source = dict()
        self.pop_source = dict()
        
        _ms_list_ = [5000]  # keep past 5s data
        for ms in _ms_list_:
            self.init_variables(ms)
        self.enough_count, self.enough_thield = 0, 4*len(_ms_list_)
        
    """
    first one it's the latest one x sec agofrom x sec ago till now
    """    
    def init_variables(self, ms:int):
        self.data_source[ms] = dict(
            _cache_depth_ = list(),
            _cache_prc_ = list(),
            _cache_buys_ = list(),
            _cache_sells_ = list(),            
        )
        self.pop_source[ms] = dict(
            _cache_depth_ = list(),
            _cache_buys_ = list(),
            _cache_sells_ = list(),           
        )

    def get_mid_price(self, depth, thield):
        notioanl = 0
        for item in depth.asks:
            notioanl += item.prc * item.qty
            ask = item.prc
            if notioanl > thield:
                break
        for item in depth.bids:
            notioanl += item.prc * item.qty
            bid = item.prc
            if notioanl > thield:
                break   
        return (ask+bid)/2
        
    def update_symbol_depth(self, ms:int, depth:Depth):
        _d = 0
        for past_depth in self.data_source[ms]["_cache_depth_"]:
            if past_depth.resp_ts < depth.resp_ts - ms:
                _d += 1
            else:
                break
        if _d > 0:
            self.pop_source[ms]["_cache_depth_"] = self.data_source[ms]["_cache_depth_"][:_d]
            self.data_source[ms]["_cache_depth_"] = self.data_source[ms]["_cache_depth_"][_d-1:]
            self.data_source[ms]["_cache_prc_"] = self.data_source[ms]["_cache_prc_"][_d-1:] 
            if self.enough_count < self.enough_thield:
                self.enough_count += 1
        else:
            self.pop_source[ms]["_cache_depth_"] = list()
        self.data_source[ms]["_cache_depth_"].append(depth)
        self.data_source[ms]["_cache_prc_"].append(self.get_mid_price(depth, 100))
        
    def update_symbol_trade(self, ms:int, trade:Trade):
        key = "_cache_buys_" if trade.side == 1 else "_cache_sells_"
        _d = 0
        for past_trade in self.data_source[ms][key]:
            if past_trade.local_time < trade.local_time - ms:
                _d += 1
            else:
                break
        if _d > 0:
            self.pop_source[ms][key] = self.data_source[ms][key][:_d-1]
            self.data_source[ms][key] = self.data_source[ms][key][_d-1:]
            if self.enough_count < self.enough_thield:
                self.enough_count += 1
        else:
            self.pop_source[ms][key] = list()
        self.data_source[ms][key].append(trade)
    
    def update_depth(self, depth:Depth):
        self.current_ts = depth.resp_ts           
        if len(depth.asks) < 20 or len(depth.bids) < 20:
            return
        for ms in list(self.data_source.keys()):
            self.update_symbol_depth(ms, depth)
            
    def update_trade(self, trade:Trade):
        self.current_ts = trade.local_time   
        for ms in list(self.data_source.keys()):
            self.update_symbol_trade(ms, trade)
    
    def if_data_enough(self):
        return self.enough_count == self.enough_thield
              

             
        