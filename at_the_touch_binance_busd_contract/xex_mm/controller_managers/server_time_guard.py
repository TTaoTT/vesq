class ServerTimeGuard:
    def __init__(self):
        self._last_tx_time_map = dict()

    def update_server_time(self, iid: str, ts: int):
        self._last_tx_time_map[iid] = ts

    def has_last_server_time(self, iid: str):
        return iid in self._last_tx_time_map

    def get_last_server_time(self, iid: str):
        return self._last_tx_time_map[iid]
