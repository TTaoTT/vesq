import logging

from xex_mm.utils.configs import HeartBeatTolerance, LatencyBoundConfig


class IIdStateManager:
    def __init__(self):
        self._heart_beat_tol_cfg = HeartBeatTolerance()
        self._last_update_time_map = dict()
        self.stale_iids = set()
        self.nonstale_iids = set()

        self._latency_bound_cfg = LatencyBoundConfig()
        self._latency_map = dict()
        self.high_latency_iids = set()
        self.normal_latency_iids = set()

        self._active_state_tracker_map = dict()
        self.active_iids = set()
        self.inactive_iids = set()

        self._n_switches = 2

    ################################# STALE IID DETECTION #############################
    def update_heart_beat_time(self, iid: str, ts: int):
        self._last_update_time_map[iid] = ts

    def get_heart_beat_gap(self, iid: str, ts: int):
        if iid not in self._last_update_time_map:
            return 0
        last_update_ts = self._last_update_time_map[iid]
        assert ts >= last_update_ts
        return ts - last_update_ts

    def is_stale(self, iid: str, ts: int):
        gap = self.get_heart_beat_gap(iid=iid, ts=ts)
        tol = self._heart_beat_tol_cfg.get_heart_beat_tol_by_iid(iid=iid)
        is_stale = gap > tol
        if is_stale:
            logging.warning(f"Iid={iid} is stale, last heart beat is {gap} ms ago.")
        return is_stale

    def is_in_stale_state(self, iid: str):
        if (iid not in self.stale_iids) and (iid not in self.nonstale_iids):
            self.stale_iids.add(iid)
        return iid in self.stale_iids

    def enter_stale_state(self, iid: str):
        if iid in self.nonstale_iids:
            self.nonstale_iids.remove(iid)
        self.stale_iids.add(iid)

        if iid not in self._active_state_tracker_map:
            self._active_state_tracker_map[iid] = 0
        else:
            self._active_state_tracker_map[iid] -= 1
            assert self._active_state_tracker_map[iid] >= 0

        if iid in self.active_iids:
            self.active_iids.remove(iid)
        self.inactive_iids.add(iid)

        logging.warning(f"Enter stale state, iid={iid}")

    def enter_nonstale_state(self, iid: str):
        if iid in self.stale_iids:
            self.stale_iids.remove(iid)
        self.nonstale_iids.add(iid)

        if iid not in self._active_state_tracker_map:
            self._active_state_tracker_map[iid] = 1
        else:
            self._active_state_tracker_map[iid] += 1

        if self.is_active(iid=iid):
            self.active_iids.add(iid)
            if iid in self.inactive_iids:
                self.inactive_iids.remove(iid)

        logging.warning(f"Enter nonstale state, iid={iid}")

    ################################# LARGE LATENCY IID DETECTION #############################
    def is_high_latency(self, iid: str, latency: int):
        high_latency_bnd, normal_latency_bnd = self._latency_bound_cfg.get_latency_bound_by_iid(iid=iid)
        if self.is_in_high_latency_state(iid=iid):
            if latency < normal_latency_bnd:
                return False
            return True
        else:  # not is_in_high_latency_state
            if latency > high_latency_bnd:
                return True
            return False

    def is_in_high_latency_state(self, iid: str):
        if (iid not in self.high_latency_iids) and (iid not in self.normal_latency_iids):
            self.high_latency_iids.add(iid)
        return iid in self.high_latency_iids

    def enter_high_latency_state(self, iid: str):
        self.high_latency_iids.add(iid)

        if iid in self.normal_latency_iids:
            self.normal_latency_iids.remove(iid)
        if iid not in self._active_state_tracker_map:
            self._active_state_tracker_map[iid] = 0
        else:
            self._active_state_tracker_map[iid] -= 1
            assert self._active_state_tracker_map[iid] >= 0

        if iid in self.active_iids:
            self.active_iids.remove(iid)
        self.inactive_iids.add(iid)

        logging.warning(f"Enter high latency state, iid={iid}")

    def enter_normal_latency_state(self, iid: str):
        self.normal_latency_iids.add(iid)

        if iid in self.high_latency_iids:
            self.high_latency_iids.remove(iid)
        if iid not in self._active_state_tracker_map:
            self._active_state_tracker_map[iid] = 1
        else:
            self._active_state_tracker_map[iid] += 1

        if self.is_active(iid=iid):
            self.active_iids.add(iid)
            if iid in self.inactive_iids:
                self.inactive_iids.remove(iid)

        logging.warning(f"Enter normal latency state, iid={iid}")

    ################################# ACTIVE STATE MAINTAINENCE #############################
    def is_active(self, iid: str) -> bool:
        if iid not in self._active_state_tracker_map:
            self._active_state_tracker_map[iid] = 0
        return self._active_state_tracker_map[iid] == self._n_switches
