import numpy as np
import pandas as pd

from xex_mm.utils.data import DataLoader


class SpotPerpCorr:
    def __init__(self):
        self._data_loader = DataLoader()

    def _calc_ret_ser(self, depth_df: pd.DataFrame, tau: pd.Timedelta):
        mp_ser = (depth_df["ap1"] + depth_df["bp1"]) / 2
        mp_ser.name = "mp"
        past_mp_ser = pd.Series(index=mp_ser.index + tau, data=mp_ser.values, name="past_mp")
        start_ts = past_mp_ser.index[0]
        end_ts = mp_ser.index[-1]
        df = pd.concat([
            past_mp_ser.loc[start_ts:end_ts].resample(pd.Timedelta("100ms")).last(),
            mp_ser.loc[start_ts:end_ts].resample(pd.Timedelta("100ms")).last(),
        ], axis=1).ffill()
        r_ser = np.log(df["mp"] / df["past_mp"])
        return r_ser

    def depth_ref_prc_corr(self, spot_contract: str, spot_ex: str, perp_contract: str, perp_ex: str, start_date: str, end_date: str, tau: pd.Timedelta):
        spot_df = self._data_loader.read_depth2(ex=spot_ex, contract=spot_contract, start_str=start_date, end_str=end_date)
        perp_df = self._data_loader.read_depth2(ex=perp_ex, contract=perp_contract, start_str=start_date, end_str=end_date)
        spot_r = self._calc_ret_ser(depth_df=spot_df, tau=tau)
        perp_r = self._calc_ret_ser(depth_df=perp_df, tau=tau)
        r_df: pd.DataFrame = pd.concat([spot_r, perp_r], axis=1)#.ffill()
        return r_df.corr()



if __name__ == '__main__':
    import logging
    logging.getLogger().setLevel(logging.INFO)
    obj = SpotPerpCorr()
    corr_summary = []
    start_date = pd.Timestamp("20220601")
    end_date = pd.Timestamp("20220629")
    date = start_date
    dts = ["1s", "10s"]
    spot_contract = "link_usdt"
    spot_ex = "binance"
    perp_contract = "link_usdt_swap"
    perp_ex = "binance"
    while date <= end_date:
        print(f"date={date}")
        str_start_date = str(date.strftime("%Y%m%d"))
        str_end_date = str((date + pd.Timedelta("1d")).strftime("%Y%m%d"))
        for dt in dts:
            print(f"dt={dt}")
            corr_df = obj.depth_ref_prc_corr(spot_contract=spot_contract, spot_ex=spot_ex,
                                              perp_contract=perp_contract, perp_ex=perp_ex,
                                              start_date=str_start_date, end_date=str_end_date,
                                              tau=pd.Timedelta(dt))
            corr = corr_df.iloc[1, 0]
            print(f"corr={corr}")
            corr_summary.append(dict(
                date=date,
                dt=dt,
                spot_contract=spot_contract,
                spot_ex=spot_ex,
                perp_contract=perp_contract,
                perp_ex=perp_ex,
                corr=corr,
            ))

        date += pd.Timedelta("1d")
    corr_summary = pd.DataFrame(corr_summary)
    print(corr_summary)