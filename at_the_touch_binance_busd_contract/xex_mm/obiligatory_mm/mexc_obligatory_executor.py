from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor


class MexcObligatoryExecutor(ObligatoryExecutor):
    def __init__(self, contract: str):
        super(MexcObligatoryExecutor, self).__init__(ex="mexc", contract=contract)
