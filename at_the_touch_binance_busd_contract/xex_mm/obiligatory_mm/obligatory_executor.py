import math
from typing import List, Tuple, Dict

import numpy as np

from xex_mm.calculators.em import EMAvgDecay
from xex_mm.utils.base import ReferencePriceCalculator, Order, Trade, TransOrder
from xex_mm.utils.enums import Side, Direction, MakerTaker
from xex_mm.utils.calc_node import CalRealTime
from xex_mm.utils.side_and_direction import direction_to_side, flip_direction
from xex_mm.xex_depth.xex_depth import XExDepth


class QuoteLevelHitRateAnomalyCal(CalRealTime):
    def __init__(self, d: Direction, l_halflife: int, s_halflife: int) -> None:
        """
        Parameters
        ----------
        l_halflife: Long (time window, not trade direction) halflife
        s_halflife: Short (time window, not trade direction) halflife
        """
        self.d = d
        if self.d == Direction.long:
            self.side = Side.long
        elif self.d == Direction.short:
            self.side = Side.short
        else:
            raise NotImplementedError(f"Unsupported direction: {self.d}")
        assert l_halflife >= s_halflife
        super(QuoteLevelHitRateAnomalyCal, self).__init__()
        self._l_ar_calc = EMAvgDecay(halflife=l_halflife)
        self._s_ar_calc = EMAvgDecay(halflife=s_halflife)

        self._numeric_tol = 1e-10

    def on_wakeup(self, ts: int):  # q = 0
        self._l_ar_calc.on_wakeup(ts=ts)
        self._s_ar_calc.on_wakeup(ts=ts)

    def add_trade(self, trade: Trade):
        assert self.side == trade.side
        self._l_ar_calc.on_val(val=trade.quantity, ts=trade.local_time)
        self._s_ar_calc.on_val(val=trade.quantity, ts=trade.local_time)

    def get_factor(self, ts: int):
        if not self._s_ar_calc.is_initialized or not self._l_ar_calc.is_initialized:
            return np.nan
        return self._s_ar_calc.get_avg(ts=ts) / max(self._l_ar_calc.get_avg(ts=ts), self._numeric_tol)


class ObligatoryExecutor:
    def __init__(self, ex: str, contract: str):
        self.ex = ex
        self.contract = contract
        self.iid = f"{self.ex}.{self.contract}"

        self._xex_depth: XExDepth = ...
        self._ref_prc_calculator = ReferencePriceCalculator()
        self._ref_prc = ...

        self._requirements: List = []
        self._prev_order_map: Dict = dict()

        self._l_halflife = 1000 * 60 # ms
        self._s_halflife = 1000
        self._hit_anomaly_cal_map: Dict[Tuple[Direction, int], QuoteLevelHitRateAnomalyCal] = dict()
        self._hit_anomaly_tol = 2

    def update_xex_depth(self, xex_depth: XExDepth):
        self._xex_depth = xex_depth
        self._ref_prc = self._ref_prc_calculator.calc_from_depth(depth=self._xex_depth.get_xex_depth(), amt_bnd=0)

    def update_requirements(self, requirements: List[Tuple]):
        self._requirements = requirements
        self._hit_anomaly_cal_map = dict()
        for i in range(len(self._requirements)):
            self._hit_anomaly_cal_map[(Direction.long, i)] = QuoteLevelHitRateAnomalyCal(l_halflife=self._l_halflife,
                                                                                         s_halflife=self._s_halflife,
                                                                                         d=Direction.long)
            self._hit_anomaly_cal_map[(Direction.short, i)] = QuoteLevelHitRateAnomalyCal(l_halflife=self._l_halflife,
                                                                                         s_halflife=self._s_halflife,
                                                                                         d=Direction.short)

    def _get_requirements(self) -> List:
        return self._requirements

    def _get_hit_anomaly_cal(self, d: Direction, i: int) -> QuoteLevelHitRateAnomalyCal:
        return self._hit_anomaly_cal_map[(d, i)]
    
    def _get_hit_anomaly_ratio(self, d: Direction, i: int, ts: int):
        return self._get_hit_anomaly_cal(d=d, i=i).get_factor(ts=ts)

    def get_hit_anomaly_ratios(self, ts: int):
        data = dict()
        for i in range(len(self._requirements)):
            data[f"{Direction.long}.lvl{i}"] = self._get_hit_anomaly_ratio(d=Direction.long, i=i, ts=ts)
            data[f"{Direction.short}.lvl{i}"] = self._get_hit_anomaly_ratio(d=Direction.short, i=i, ts=ts)
        return data

    def gen_quotes(self, ts: int) -> List[TransOrder]:
        self.on_wakeup(ts=ts)
        orders = []
        for i, (ret, q_ccy2) in enumerate(self._get_requirements()):
            ask_prc = self._ref_prc * (1 + ret)
            bid_prc = self._ref_prc * (1 - ret)
            q_ccy1 = q_ccy2 / ask_prc
            l_r = self._get_hit_anomaly_ratio(d=Direction.long, i=i, ts=ts)
            s_r = self._get_hit_anomaly_ratio(d=Direction.short, i=i, ts=ts)
            if math.isnan(l_r) or (l_r < 2):
                orders.append(TransOrder(
                    side=Direction.short,
                    p=ask_prc,
                    q=q_ccy1,
                    iid=self.iid,
                    floor=i,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.maker,
                ))
            if math.isnan(s_r) or (s_r < 2):
                orders.append(TransOrder(
                    side=Direction.long,
                    p=bid_prc,
                    q=q_ccy1,
                    iid=self.iid,
                    floor=i,
                    delta=np.nan,
                    life=1,
                    maker_taker=MakerTaker.maker,
                ))
        return orders

    def get_inc_fill_qty_and_prc_in_tokens(self, order: Order) -> Tuple[float, float]:
        assert order.filled_qty >= 0
        cid = order.client_id
        if cid in self._prev_order_map:
            prev_order: Order = self._prev_order_map[cid]
            inc_fill_qty_in_tokens = max(order.filled_qty - prev_order.filled_qty, 0)
            inc_amt = order.filled_qty * order.avg_fill_prc - prev_order.filled_qty * prev_order.avg_fill_prc
            inc_fill_prc = inc_amt / inc_fill_qty_in_tokens if inc_fill_qty_in_tokens > 0 else np.nan
            return inc_fill_qty_in_tokens, inc_fill_prc
        return order.filled_qty, order.avg_fill_prc

    def _update_order(self, order: Order):
        self._prev_order_map[order.client_id] = order

    def remove_order(self, oid: str):
        self._prev_order_map.pop(oid)

    def on_order(self, order: Order):
        """ Order hit, update arrival rate """
        i = int(order.meta["lvl_idx"])
        qty, prc = self.get_inc_fill_qty_and_prc_in_tokens(order=order)
        if qty > 0:
            trd = Trade(
                ex=order.ex,
                contract=order.contract,
                price=prc,
                quantity=qty,
                tx_time=order.local_time,  # TODO
                local_time=order.local_time,
                side=direction_to_side(d=flip_direction(d=order.side)),
                mexc_side=0,
                uid=0,
            )
            if trd.side == Side.long:
                hit_anomoly_cal = self._get_hit_anomaly_cal(d=Direction.long, i=i)
            elif trd.side == Side.short:
                hit_anomoly_cal = self._get_hit_anomaly_cal(d=Direction.short, i=i)
            else:
                raise NotImplementedError(f"Unsupported side: {trd.side}")
            hit_anomoly_cal.add_trade(trade=trd)
        elif qty < 0:
            raise RuntimeError(f"Incremental filled quantity should always be greater or equal to 0, got: {qty}")
        else:  # qty == 0
            pass
        self._update_order(order=order)

    def on_wakeup(self, ts: int):
        """ No hit, decay arrival rate """
        for i in range(len(self._get_requirements())):
            self._get_hit_anomaly_cal(d=Direction.long, i=i).on_wakeup(ts=ts)
            self._get_hit_anomaly_cal(d=Direction.short, i=i).on_wakeup(ts=ts)



