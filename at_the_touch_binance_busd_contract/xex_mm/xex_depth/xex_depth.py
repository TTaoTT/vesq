import logging
import math
from decimal import Decimal
from typing import Dict, List, Tuple, Union

from xex_mm.calculators.ref_prc import CalEMAvgRelRefPrcSprd2
from xex_mm.utils.base import Depth, BBO, ReferencePriceCalculator, Trade
from xex_mm.utils.enums import Direction, DepthShadowState


class XExDepth:
    def __init__(self, contract: str, ref_prc_sprd_halflife: float = 1e3):
        self._ex_depth_map: Dict[str, Depth] = {}
        self._xex_depth: Depth = ...
        self._ex_24h_trade_vol: Dict[str, Decimal] = {}
        self._ex_fee_rate_map: Dict[str, Tuple[float, float]] = {}
        self.contract: str = contract
        self._exs_set = set()
        self._ref_prc_cal = ReferencePriceCalculator()
        self._emavg_ref_prc_sprd_cal_map = dict()
        self._emavg_ref_prc_sprd_halflife = ref_prc_sprd_halflife
        self._funding_cost_map = dict()
        self._future_funding_cost_compensation = dict()
        self._ref_prc_map = dict()
        self._xex_ref_prc = ...

    def __repr__(self):
        return self._xex_depth.__repr__()

    def get_ex_names(self) -> List[str]:
        return list(self._ex_depth_map.keys())

    def contains(self, ex: str) -> bool:
        return ex in self._ex_depth_map

    def empty(self):
        return not self._exs_set

    def get_ex_depth_map(self):
        return self._ex_depth_map

    def get_emavg_ref_prc_sprd_cal(self, ex: str) -> CalEMAvgRelRefPrcSprd2:
        if ex not in self._emavg_ref_prc_sprd_cal_map:
            iid = f"{ex}.{self.contract}"
            self._emavg_ref_prc_sprd_cal_map[ex] = CalEMAvgRelRefPrcSprd2(
                iid=iid,
                halflife=self._emavg_ref_prc_sprd_halflife
            )
        return self._emavg_ref_prc_sprd_cal_map[ex]

    def update_depth_for_ex_with_bbo(self, bbo: BBO) -> bool:
        if not bbo.valid_bbo:
            return False

        ex = bbo.meta.ex

        if ex in self._ex_depth_map:
            prev_depth = self._ex_depth_map[ex]
            self._xex_depth -= prev_depth
            new_depth = prev_depth.update_depth_with_bbo(bbo=bbo)
            self._xex_depth += new_depth
            # self._xex_depth.meta_data.depth_completeness_state = DepthCompletenessState.bbo_update
            self._ex_depth_map[ex] = new_depth
            return True
        logging.info(f"BBO before depth for contract={bbo.meta.contract}, ex={ex}, skipping.")
        return False
        # depth = bbo.to_depth()
        # self._ex_depth_map[ex] = depth
        # if self._xex_depth is Ellipsis:
        #     self._xex_depth = depth
        # else:
        #     self._xex_depth += depth
        #     self._xex_depth.mp = depth.mp

    def update_depth_for_ex(self, depth: Depth) -> bool:
        ex = depth.meta_data.ex

        if ex in self._ex_depth_map:
            self._xex_depth -= self._ex_depth_map[ex]
            self._xex_depth += depth
            self._xex_depth.mp = depth.mp
            self._ex_depth_map[ex] = depth
            return True
        self._ex_depth_map[ex] = depth
        if self._xex_depth is Ellipsis:
            self._xex_depth = depth.clone()
            self._xex_depth.meta_data.ex = "xex"
        else:
            self._xex_depth += depth
            self._xex_depth.mp = depth.mp
        self._exs_set.add(ex)
        return True

    def update_24h_trade_vol_for_ex(self, ex_name: str, _24h_trade_vol: Decimal):
        self._ex_24h_trade_vol[ex_name] = _24h_trade_vol

    def update_fee_for_ex(self, ex_name: str, maker_fee_rate: float, taker_fee_rate: float):
        self._ex_fee_rate_map[ex_name] = (maker_fee_rate, taker_fee_rate)

    def remove_ex(self, ex: str):
        self._xex_depth -= self.get_depth_for_ex(ex=ex)
        self._ex_depth_map.pop(ex)
        self._exs_set.remove(ex)

    def get_depth_for_ex(self, ex: str):
        if ex in self._ex_depth_map:
            return self._ex_depth_map[ex]
        raise RuntimeError(f"Depth for exchange {ex} not found!")

    def get_24h_trade_vol_for_ex(self, ex_name: str) -> Decimal:
        return self._ex_24h_trade_vol[ex_name]

    def get_xex_depth(self) -> Depth:
        return self._xex_depth

    def get_shadow_depth_SEx(self, ex: str, q: int, d: Direction) -> Depth:
        SEx_depth = self.get_depth_for_ex(ex=ex)
        SEx_shadow_depth = SEx_depth.get_shadow_depth(q=q, d=d)
        depth = self._xex_depth - SEx_depth + SEx_shadow_depth
        depth.meta_data.depth_shadow_state = DepthShadowState.shadow
        return depth

    def get_shadow_depth(self, q: int, d: Direction) -> Depth:
        return self._xex_depth.get_shadow_depth(q=q, d=d)

    def get_post_taker_fee_view(self):
        """ Potentially slow """
        post_taker_fee_xex = self.__class__(contract=self.contract)
        for ex, depth in self.get_ex_depth_map().items():
            assert ex == depth.meta_data.ex
            post_taker_fee_xex.update_depth_for_ex(depth=depth.get_post_taker_fee_view())
        return post_taker_fee_xex

    def get_funding_cost(self, ex: str):
        return self._funding_cost_map.get(ex, 0)

    def get_futures_funding_cost_compensation(self, ex: str):
        return self._future_funding_cost_compensation.get(ex, 0)

    def get_post_funding_cost_view(self):
        """ Potentially slow """
        post_funding_cost_xex = self.__class__(contract=self.contract)
        for ex, depth in self.get_ex_depth_map().items():
            assert ex == depth.meta_data.ex
            post_funding_cost_xex.update_depth_for_ex(depth=depth.get_post_funding_cost_view())
        return post_funding_cost_xex

    def get_post_future_funding_cost_compensation_view(self):
        """ Potentially slow """
        xex_depth = self.__class__(contract=self.contract)
        for ex, depth in self.get_ex_depth_map().items():
            assert ex == depth.meta_data.ex
            xex_depth.update_depth_for_ex(depth=depth.get_post_funding_compensation_view())
        return xex_depth

    def get_post_latency_cost_view(self):
        """ Potentially slow """
        xex_depth = self.__class__(contract=self.contract)
        for ex, depth in self.get_ex_depth_map().items():
            assert ex == depth.meta_data.ex
            xex_depth.update_depth_for_ex(depth=depth.get_post_latency_cost_view())
        return xex_depth

    def get_long_term_prc_spread_view(self, ex: str):
        """ Potentially slow """
        ex_centered_xex = self.__class__(contract=self.contract)
        ts = self._xex_depth.resp_ts
        ex_rel_prc_shift = self.get_emavg_ref_prc_sprd_cal(ex=ex).get_ema_ref_prc_sprd(ts=ts)
        for _ex, depth in self.get_ex_depth_map().items():
            assert _ex == depth.meta_data.ex
            rel_prc_shift = self.get_emavg_ref_prc_sprd_cal(ex=_ex).get_ema_ref_prc_sprd(ts=ts)
            ex_centered_xex.update_depth_for_ex(depth=depth.get_prc_shifted_view(rel_prc_shift=ex_rel_prc_shift - rel_prc_shift))
        return ex_centered_xex

    def get_post_cost_view(self):
        """ Potentially slow """
        xex_depth = self.__class__(contract=self.contract)
        for ex, depth in self.get_ex_depth_map().items():
            assert ex == depth.meta_data.ex
            xex_depth.update_depth_for_ex(depth=depth.get_post_cost_view())
        return xex_depth

    def update_ref_prc(self, ex: str) -> bool:
        """ Call this after bbo or depth update """
        ref_prc = self._ref_prc_cal.calc_from_depth(depth=self.get_depth_for_ex(ex=ex), amt_bnd=0)
        if math.isnan(ref_prc):
            return False
        self._ref_prc_map[ex] = ref_prc

        ref_prc = self._ref_prc_cal.calc_from_depth(depth=self.get_xex_depth(), amt_bnd=0)
        if math.isnan(ref_prc):
            return False
        self._xex_ref_prc = ref_prc
        return True

    def get_xex_ref_prc(self):
        return self._xex_ref_prc

    def get_ref_prc(self, ex: str) -> Union[float, None]:
        return self._ref_prc_map.get(ex)

    def update_emavg_ref_prc_sprd(self, ex: str) -> bool:
        """ Call this after bbo or depth update """
        success = self.update_ref_prc(ex=ex)
        if not success:
            return False
        cal = self.get_emavg_ref_prc_sprd_cal(ex=ex)
        # update order matters
        success = cal.update_ref_prc(ref_prc=self._ref_prc_map[ex])
        if not success:
            return False
        success = cal.update_xex_ref_prc(xex_ref_prc=self._xex_ref_prc)
        if not success:
            return False
        success = cal.update_rel_ref_prc_sprd(ts=self._xex_depth.resp_ts)
        return True

    def get_shifted_xex_ref_prc(self, ex: str) -> Union[float, None]:
        if self._xex_ref_prc is Ellipsis:
            return
        rel_prc_shift = self.get_emavg_ref_prc_sprd_cal(ex=ex).get_ema_ref_prc_sprd(ts=self._xex_depth.resp_ts)
        if rel_prc_shift is None:
            return
        return self._xex_ref_prc * math.exp(rel_prc_shift)

    def get_shifted_xex_view(self, ex: str) -> Union[Depth, None]:
        rel_prc_shift = self.get_emavg_ref_prc_sprd_cal(ex=ex).get_ema_ref_prc_sprd(ts=self._xex_depth.resp_ts)
        if rel_prc_shift is None:
            return
        return self.get_depth_for_ex(ex=ex).get_prc_shifted_view(rel_prc_shift=rel_prc_shift)

"""
Components contributing to xex ref prc diff:

1. Trading fees
2. Funding rates (Future side pays spot side to borrow money to build inventory to arbitrage out the price diff)
3. Latency cost
4. Other long term factors, token underlying changes (rare events would require manual intervention)

XEx ref prc, merge the adjusted bbo/depth for each exchange. Adjusted means bbo/depth removed of: 

1. Takers fee
2. Funding cost 
3. Latency cost
4. EMA spread (catch all)

Any other reason for short term price divergence is arbitrage-able.
"""


class XExBBO(XExDepth):
    def update_bbo(self, bbo: BBO):
        return self.update_depth_for_ex(depth=bbo.to_depth())

    def update_depth_for_ex(self, depth: Depth):
        return super(XExBBO, self).update_depth_for_ex(depth.to_bbo().to_depth())

    def update_depth(self, depth: Depth):
        return self.update_depth_for_ex(depth=depth)

    def update_depth_for_ex_with_trade(self, trade: Trade, halfsprd: float) -> bool:
        ex = trade.ex
        if ex in self._ex_depth_map:
            prev_depth = self._ex_depth_map[ex]
            self._xex_depth -= prev_depth
            new_depth = prev_depth.update_depth_with_trade(trade=trade, halfsprd=halfsprd)
            self._xex_depth += new_depth
            self._ex_depth_map[ex] = new_depth
            return True
        # logging.info(f"Trade before depth for contract={trade.contract}, ex={ex}, skipping.")
        return False

    def update_trade(self, trade: Trade, halfsprd: float) -> bool:
        return self.update_depth_for_ex_with_trade(trade=trade, halfsprd=halfsprd)
