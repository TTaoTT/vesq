import datetime
import time

import numpy as np
from scipy.stats import poisson


def run():
    ns = [1, 10, 100, 1000, 10000]
    mu = 1
    start_time = time.time_ns()
    for i in range(100):
        res = []
        for n in ns:
            res.append(poisson.pmf(n, mu))
    end_time = time.time_ns()
    print(end_time - start_time)
    print(res)
    start_time = time.time_ns()
    for i in range(100):
        res = poisson.pmf(np.array(ns), mu)
    end_time = time.time_ns()
    print(end_time - start_time)
    print(res)


if __name__ == '__main__':
    run()