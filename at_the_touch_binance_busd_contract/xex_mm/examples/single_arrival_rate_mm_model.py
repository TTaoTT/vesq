import datetime
import pandas as pd
from typing import Dict

import numpy as np

from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model import SingleArrivalRateXExMmModelWithPartialFill
from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model_no_partial_fill import SingleArrivalRateXExMmModelNoPartialFill
from xex_mm.mm_quoting_model.arrival_rate_model.models.full_model import SingleArrivalRateXExMmModel
from xex_mm.utils.base import Depth
from xex_mm.utils.enums import Direction, MakerTaker
from xex_mm.xex_depth.xex_depth import XExDepth

from mm_quoting_model.arrival_rate_model.models.xcon_model import XConMmModel


raise DeprecationWarning()


class DummyRefPriceModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local ref_prc


class DummySpreadModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local spread


class DummySigmaModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local volatility


class DummyArrivalRateModel:
    def predict(self, xs: np.ndarray):
        return xs[0]  # local arrival rate


class DummyArrivalRateMktDepthDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


class DummyArrivalRateQtyDecayCoefModel:
    def predict(self, xs: np.ndarray):
        return xs[0]


BETA = 0.0000001
SIGMA = 0.0001


class SingleArrivalRateMmFullModelScenarioTest:
    def __init__(self):
        self.contract = "contract"
        self.I = 0
        self.T = 1000
        self.tau = 100
        self._xex_depth_map: Dict[str, XExDepth] = {}
        self._initialize_depths()
        self._setup_mm_model()

    def _initialize_depths(self):
        xex_depth = XExDepth()

        depth_dict = {
            "asks": [[10000, 0.01], [10100, 0.01], [10200, 0.01]],
            "bids": [[9800, 0.01], [9700, 0.01], [9600, 0.01]],
            "resp_ts": 1,
            "server_ts": 1,
        }
        ex_name = "mexc"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[9900, 0.010], [10000, 0.010], [10100, 0.010]],
            "bids": [[9700, 0.010], [9600, 0.010], [9500, 0.010]],
            "resp_ts": 2,
            "server_ts": 2,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[9600, 0.010], [9700, 0.010], [9800, 0.010]],
            "bids": [[9500, 0.010], [9400, 0.020], [9300, 0.010]],
            "resp_ts": 3,
            "server_ts": 3,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        print(xex_depth)
        self._xex_depth_map["contract"] = xex_depth

    def _setup_mm_model(self):
        self._mm_mdl = SingleArrivalRateXExMmModel(
            ref_prc_mdl_map=dict(
                contract=dict(
                    xex=DummyRefPriceModel(),
                )
            ),
            half_sprd_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummySpreadModel(),
                        Direction.short: DummySpreadModel(),
                    }
                )
            ),
            sigma_mdl_map=dict(
                contract=dict(
                    xex=DummySigmaModel(),
                )
            ),
            arrival_rate_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateModel(),
                        Direction.short: DummyArrivalRateModel(),
                    },
                ),
            ),
            fee_rate_map=dict(
                contract=dict(
                    mexc={
                        MakerTaker.maker: -0.0001,
                        MakerTaker.taker: 0.0001,
                    },
                    binance={
                        MakerTaker.maker: -0.0002,
                        MakerTaker.taker: 0.0003,
                    },
                ),
            ),
            arrival_rate_mkt_depth_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateMktDepthDecayCoefModel(),
                        Direction.short: DummyArrivalRateMktDepthDecayCoefModel(),
                    }
                )
            ),
            arrival_rate_qty_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateQtyDecayCoefModel(),
                        Direction.short: DummyArrivalRateQtyDecayCoefModel(),
                    }
                )
            ),
            qty_tick_size_map=dict(
                contract=dict(
                    mexc=0.01,
                    binance=0.005,
                )
            )
        )

    def _setup_inventory_solvency(self):
        self.I = 10
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([10]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))

    def run_inventory_solvency(self):
        self._setup_inventory_solvency()

        XEx_depth = self._xex_depth_map["contract"]
        start_time = datetime.datetime.now()
        df = self._mm_mdl.utility_values(
            contract=self.contract,
            ps=[x * 10 for x in range(900, 1060)],
            # ps=[9990],
            qs=[0.005],
            # ps = [9800, 10500],
            # qs = [0.015],
            I=self.I, T=self.T, tau=self.tau, beta=BETA,
            XEx_depth=XEx_depth,
        )
        end_time = datetime.datetime.now()
        print(df.sort_values(["util_val", "d", "p"]).tail(500).to_string())
        print(f"ellapsed: {end_time - start_time}")

    def run_approx(self):
        XEx_depth = self._xex_depth_map["contract"]
        start_time = datetime.datetime.now()
        df = self._mm_mdl.approx_utility_values(
            contract=self.contract,
            ps=[x * 100 for x in range(90, 106)],
            qs=[0.01, 0.03, 0.1],
            # ps = [10500],
            # qs = [0.1],
            I=self.I, T=self.T, tau=self.tau, beta=BETA,
            XEx_depth=XEx_depth,
        )
        end_time = datetime.datetime.now()
        print(df.sort_values("util_val").tail(20).to_string())
        print(f"ellapsed: {end_time - start_time}")

    def profile_run(self):
        XEx_depth = self._xex_depth_map["contract"]
        for i in range(1000):
            df = self._mm_mdl.approx_utility_values(
                contract=self.contract,
                ps=list(range(90, 106)),
                qs=[0.01, 0.03, 0.1],
                # ps = [10500],
                # qs = [0.1],
                I=self.I, T=self.T, tau=self.tau, beta=BETA,
                XEx_depth=XEx_depth,
            )

    def run_multi_step(self):
        contract = "contract"
        xex = "xex"
        XEx_depth = self._xex_depth_map[contract]
        n = 100
        start_time = datetime.datetime.now()
        for i in range(n):
            ref_prc = self._mm_mdl.get_pred_ref_prc(contract=contract, ex=xex)
            p_search_df = self._mm_mdl.approx_utility_values(
                contract=contract,
                ps=np.arange(-10, 10, 3) + ref_prc,
                qs=[0.01],
                # ps = [10500],
                # qs = [0.1],
                I=self.I, T=self.T, tau=self.tau, beta=BETA,
                XEx_depth=XEx_depth,
            )
            p_search_df.sort_values("util_val", inplace=True)
            p_search_df2 = self._mm_mdl.approx_utility_values(
                contract=contract,
                ps=np.arange(-3, 3, 1) + p_search_df.iloc[-1]["p"],
                qs=[0.01],
                # ps = [10500],
                # qs = [0.1],
                I=self.I, T=self.T, tau=self.tau, beta=BETA,
                XEx_depth=XEx_depth,
            )
            p_search_df2.sort_values("util_val", inplace=True)
            q_search_df = self._mm_mdl.approx_utility_values(
                contract=contract,
                ps=[p_search_df2.iloc[-1]["p"]],
                qs=[0.01, 0.03, 0.05, 0.1], # * GCD
                # ps = [105],
                # qs = [0.1],
                I=self.I, T=self.T, tau=self.tau, beta=BETA,
                XEx_depth=XEx_depth,
            )
            q_search_df.sort_values("util_val", inplace=True)
        end_time = datetime.datetime.now()
        # print(p_search_df.to_string())
        # print(p_search_df2.to_string())
        # print(q_search_df.to_string())
        ellapsed = end_time - start_time
        print(f"ellapsed: {ellapsed}, per iter: {ellapsed/n}, n iters={n}")


class SingleArrivalRateMmSimplifiedModelNoParitalFillScenarioTest:
    def __init__(self):
        self.contract = "contract"
        self.I = 0
        self.T = 1000
        self.tau = 100
        self._xex_depth_map: Dict[str, XExDepth] = {}
        self._initialize_depths()
        self._setup_mm_model()

    def _initialize_depths(self):
        xex_depth = XExDepth()

        depth_dict = {
            "asks": [[10000, 0.01], [10100, 0.01], [10200, 0.01]],
            "bids": [[9800, 0.01], [9700, 0.01], [9600, 0.01]],
            "resp_ts": 1,
            "server_ts": 1,
        }
        ex_name = "mexc"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[9900, 0.010], [10000, 0.010], [10100, 0.010]],
            "bids": [[9700, 0.010], [9600, 0.010], [9500, 0.010]],
            "resp_ts": 2,
            "server_ts": 2,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[9600, 0.010], [9700, 0.010], [9800, 0.010]],
            "bids": [[9500, 0.010], [9400, 0.020], [9300, 0.010]],
            "resp_ts": 3,
            "server_ts": 3,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        print(xex_depth)
        self._xex_depth_map["contract"] = xex_depth

    def _setup_mm_model(self):
        self._mm_mdl = SingleArrivalRateXExMmModelNoPartialFill(
            ref_prc_mdl_map=dict(
                contract=dict(
                    xex=DummyRefPriceModel(),
                )
            ),
            half_sprd_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummySpreadModel(),
                        Direction.short: DummySpreadModel(),
                    }
                )
            ),
            sigma_mdl_map=dict(
                contract=dict(
                    xex=DummySigmaModel(),
                )
            ),
            arrival_rate_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateModel(),
                        Direction.short: DummyArrivalRateModel(),
                    },
                ),
            ),
            fee_rate_map=dict(
                contract=dict(
                    mexc={
                        MakerTaker.maker: -0.0001,
                        MakerTaker.taker: 0.0001,
                    },
                    binance={
                        MakerTaker.maker: -0.0002,
                        MakerTaker.taker: 0.0003,
                    },
                ),
            ),
            arrival_rate_mkt_depth_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateMktDepthDecayCoefModel(),
                        Direction.short: DummyArrivalRateMktDepthDecayCoefModel(),
                    }
                )
            ),
            arrival_rate_qty_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateQtyDecayCoefModel(),
                        Direction.short: DummyArrivalRateQtyDecayCoefModel(),
                    }
                )
            ),
            qty_tick_size_map=dict(
                contract=dict(
                    mexc=0.01,
                    binance=0.005,
                )
            ),
            prc_tick_size_map=dict(
                contract=dict(
                    mexc=1e-5,
                    binance=1e-6,
                )
            ),
        )

    def _setup_skewed_arrival_rate(self):
        self.I = 0
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.075]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([10]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))
        tick_size = self._mm_mdl._get_qty_tick_size_gcd(contract=self.contract)
        self._mm_mdl.update_depth(
            contract=self.contract, ex="xex", depth=self._xex_depth_map["contract"].get_xex_depth().get_qty_grp_view(
                q_grp_size=tick_size))
        for ex in ["mexc", "binance"]:
            self._mm_mdl.update_depth(
                contract=self.contract, ex=ex,
                depth=self._xex_depth_map["contract"].get_depth_for_ex(ex=ex).get_qty_grp_view(
                    q_grp_size=tick_size))

    def run_q1_skewed_arrival_rate(self):
        self._setup_skewed_arrival_rate()

        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta = self._mm_mdl.optimal_price_level_q1(
                    contract=self.contract,
                    ex=ex,
                    d=d,
                    I=self.I, T=self.T, tau=self.tau, beta=BETA,
                )
                U_q, q = self._mm_mdl.optimal_quantity(
                    contract=self.contract,
                    ex=ex,
                    delta=delta,
                    d=d,
                    I=self.I, T=self.T, tau=self.tau, beta=BETA,
                )
                p = tilde_p - s_l - delta if d == Direction.long else tilde_p + s_s + delta
                data.append((ex, d, U_p, p, U_q, q))
        return data

    def run_q_skewed_arrival_rate(self):
        self._setup_skewed_arrival_rate()

        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat = self._mm_mdl.optimal_price_level_q(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                T=self.T, tau=self.tau, beta=BETA)
                U_q, q_hat = self._mm_mdl.optimal_quantity(contract=self.contract, ex=ex, d=d, I=self.I, delta=delta_hat,
                                                       T=self.T, tau=self.tau, beta=BETA)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append((ex, d, U_p, p_hat, U_q, q_hat))
        return data

    def _setup_inventory_solvency(self):
        self.I = 10
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.00075]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([10]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))
        tick_size = self._mm_mdl._get_qty_tick_size_gcd(contract=self.contract)
        self._mm_mdl.update_depth(
            contract=self.contract, ex="xex", depth=self._xex_depth_map["contract"].get_xex_depth().get_qty_grp_view(
                q_grp_size=tick_size))
        for ex in ["mexc", "binance"]:
            self._mm_mdl.update_depth(
                contract=self.contract, ex=ex,
                depth=self._xex_depth_map["contract"].get_depth_for_ex(ex=ex).get_qty_grp_view(
                    q_grp_size=tick_size))

    def run_q_inventory_solvency(self):
        self._setup_inventory_solvency()

        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metric(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat = self._mm_mdl.optimal_price_level_q(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                    T=self.T, tau=self.tau, beta=BETA)
                U_q, q_hat = self._mm_mdl.optimal_quantity(contract=self.contract, ex=ex, d=d, I=self.I,
                                                           delta=delta_hat,
                                                           T=self.T, tau=self.tau, beta=BETA)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append((ex, d, U_p, p_hat, U_q, q_hat))
        return data

    def timed_run(self, run_func):
        n = 1000
        start_time = datetime.datetime.now()
        for i in range(n):
            data = run_func()
        end_time = datetime.datetime.now()
        ellapsed = end_time - start_time
        print(f"ellapsed: {ellapsed}, per iter: {ellapsed / n}, n: {n}")


class SingleArrivalRateMmSimplifiedModelScenarioTest:
    def __init__(self):
        self.contract = "contract"
        self.I = 0
        self.T = 1000
        self.tau = 100
        self._xex_depth_map: Dict[str, XExDepth] = {}
        self._initialize_depths()
        self._setup_mm_model()

    def _initialize_depths(self):
        xex_depth = XExDepth()

        depth_dict = {
            "asks": [[10000, 0.01], [10100, 0.01], [10200, 0.01]],
            "bids": [[9800, 0.01], [9700, 0.01], [9600, 0.01]],
            "resp_ts": 1,
            "server_ts": 1,
        }
        ex_name = "mexc"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[9900, 0.010], [10000, 0.010], [10100, 0.010]],
            "bids": [[9700, 0.010], [9600, 0.010], [9500, 0.010]],
            "resp_ts": 2,
            "server_ts": 2,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[9600, 0.010], [9700, 0.010], [9800, 0.010]],
            "bids": [[9500, 0.010], [9400, 0.020], [9300, 0.010]],
            "resp_ts": 3,
            "server_ts": 3,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        print(xex_depth)
        self._xex_depth_map["contract"] = xex_depth

    def _setup_mm_model(self):
        self._mm_mdl = SingleArrivalRateXExMmModelWithPartialFill(
            ref_prc_mdl_map=dict(
                contract=dict(
                    xex=DummyRefPriceModel(),
                )
            ),
            half_sprd_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummySpreadModel(),
                        Direction.short: DummySpreadModel(),
                    }
                )
            ),
            sigma_mdl_map=dict(
                contract=dict(
                    xex=DummySigmaModel(),
                )
            ),
            arrival_rate_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateModel(),
                        Direction.short: DummyArrivalRateModel(),
                    },
                ),
            ),
            fee_rate_map=dict(
                contract=dict(
                    mexc={
                        MakerTaker.maker: -0.0001,
                        MakerTaker.taker: 0.0001,
                    },
                    binance={
                        MakerTaker.maker: -0.0002,
                        MakerTaker.taker: 0.0003,
                    },
                ),
            ),
            arrival_rate_mkt_depth_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateMktDepthDecayCoefModel(),
                        Direction.short: DummyArrivalRateMktDepthDecayCoefModel(),
                    }
                )
            ),
            arrival_rate_qty_decay_coef_mdl_map=dict(
                contract=dict(
                    xex={
                        Direction.long: DummyArrivalRateQtyDecayCoefModel(),
                        Direction.short: DummyArrivalRateQtyDecayCoefModel(),
                    }
                )
            ),
            qty_tick_size_map=dict(
                contract=dict(
                    mexc=0.01,
                    binance=0.005,
                )
            ),
            prc_tick_size_map=dict(
                contract=dict(
                    mexc=1e-5,
                    binance=1e-6,
                )
            ),
        )
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.075]))
        self._mm_mdl.update_pred_arrival_rate(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.075]))
        self._mm_mdl.update_pred_sigma(
            contract=self.contract, ex="xex", xs=np.array([SIGMA]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([100]))
        self._mm_mdl.update_pred_half_spread(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([100]))
        self._mm_mdl.update_pred_ref_prc(
            contract=self.contract, ex="xex", xs=np.array([9700]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_mkt_depth_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.1]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.long, xs=np.array([0.025]))
        self._mm_mdl.update_pred_arrival_rate_qty_decay_coef(
            contract=self.contract, ex="xex", d=Direction.short, xs=np.array([0.025]))
        tick_size = self._mm_mdl._get_qty_tick_size_gcd(contract=self.contract)
        self._mm_mdl.update_depth(
            contract=self.contract, ex="xex", depth=self._xex_depth_map["contract"].get_xex_depth().get_qty_grp_view(
                q_grp_size=tick_size))

        for ex in ["mexc", "binance"]:
            self._mm_mdl.update_depth(
                contract=self.contract, ex=ex,
                depth=self._xex_depth_map["contract"].get_depth_for_ex(ex=ex).get_qty_grp_view(
                    q_grp_size=tick_size))

    def run_q_without_partial_fill(self):
        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metrics(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metrics(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metrics(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat = self._mm_mdl.optimal_price_level(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                T=self.T, tau=self.tau, beta=BETA)
                U_q, q_hat = self._mm_mdl.optimal_quantity_without_partial_fill(contract=self.contract, ex=ex, d=d, I=self.I, delta=delta_hat,
                                                       T=self.T, tau=self.tau, beta=BETA)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append(dict(
                    ex=ex,
                    d=d,
                    U_p=U_p,
                    p_hat=p_hat,
                    U_q=U_q,
                    q_hat=q_hat,
                ))
        return data

    def run_q(self):
        q = 0.005
        tilde_p = self._mm_mdl.get_depth_metrics(contract=self.contract, ex="xex", metric="tilde_p")
        s_l = self._mm_mdl.get_depth_metrics(contract=self.contract, ex="xex", metric="s_l")
        s_s = self._mm_mdl.get_depth_metrics(contract=self.contract, ex="xex", metric="s_s")
        data = []
        for ex in [
            "mexc",
            "binance",
        ]:
            for d in [
                Direction.long,
                Direction.short,
            ]:
                U_p, delta_hat = self._mm_mdl.optimal_price_level(contract=self.contract, ex=ex, d=d, I=self.I, q=q,
                                                                T=self.T, tau=self.tau, beta=BETA)
                U_q, q_hat = self._mm_mdl.optimal_quantity(contract=self.contract, ex=ex, d=d, I=self.I, delta=delta_hat,
                                                       T=self.T, tau=self.tau, beta=BETA)
                p_hat = tilde_p - s_l - delta_hat if d == Direction.long else tilde_p + s_s + delta_hat
                data.append(dict(
                    ex=ex,
                    d=d,
                    U_p=U_p,
                    p_hat=p_hat,
                    U_q=U_q,
                    q_hat=q_hat,
                ))
        return data

    def timed_run(self, run_func):
        n = 1000
        start_time = datetime.datetime.now()
        for i in range(n):
            data = run_func()
        end_time = datetime.datetime.now()
        ellapsed = end_time - start_time
        print(f"ellapsed: {ellapsed}, per iter: {ellapsed / n}, n: {n}")


if __name__ == '__main__':
    # test_obj = SingleArrivalRateMmFullModelScenarioTest()
    # print(test_obj.run_inventory_solvency())
    # test_obj = SingleArrivalRateMmSimplifiedModelNoParitalFillScenarioTest()
    # for row in test_obj.run_q_inventory_solvency():
    #     print(row)

    test_obj_2 = SingleArrivalRateMmSimplifiedModelScenarioTest()
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    print(pd.DataFrame(test_obj_2.run_q()).T)
    # print(pd.DataFrame(test_obj_2.run_q_without_partial_fill()).T)
    # test_obj_2.timed_run(test_obj_2.run_q)
    # test_obj_2.timed_run(test_obj_2.run_q_without_partial_fill)

    # test_obj = XConModelScenarioTest()
    # test_obj.run()
