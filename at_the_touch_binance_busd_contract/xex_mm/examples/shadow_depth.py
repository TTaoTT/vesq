from typing import Dict

from xex_mm.utils.base import Depth
from xex_mm.utils.enums import Direction
from xex_mm.xex_depth.xex_depth import XExDepth


class ShadowDepthTest:
    def __init__(self):
        self._xex_depth_map: Dict[str, Depth] = {}
        self._setup_depth()

    def _setup_depth(self):
        xex_depth = XExDepth()

        depth_dict = {
            "asks": [[100, 1], [101, 1], [102, 1]],
            "bids": [[98, 1], [97, 1], [96, 1]],
            "resp_ts": 1,
            "server_ts": 1,
        }
        ex_name = "mexc"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[99, 1], [100, 1], [101, 1]],
            "bids": [[97, 1], [96, 1], [95, 1]],
            "resp_ts": 2,
            "server_ts": 2,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        depth_dict = {
            "asks": [[96, 1], [97, 1], [98, 1]],
            "bids": [[95, 1], [94, 2], [93, 1]],
            "resp_ts": 3,
            "server_ts": 3,
        }
        ex_name = "binance"
        xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

        print(xex_depth)
        self._xex_depth_map["contract"] = xex_depth

    def run(self):
        print(self._xex_depth_map["contract"].get_shadow_depth(q=3, d=Direction.long))


if __name__ == '__main__':
    ShadowDepthTest().run()
