from xex_mm.utils.base import Depth
from xex_mm.utils.enums import Direction
from decimal import Decimal

from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.xex_order_router.xex_order_router import XExRouter


def test_xex_depth():
    xex_depth = XExDepth()

    depth_dict = {
        "asks": [[100, 1], [101, 1], [102, 1]],
        "bids": [[98, 1], [97, 1], [96, 1]],
        "resp_ts": 1,
        "server_ts": 1,
    }
    ex_name = "ex1"
    xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

    depth_dict = {
        "asks": [[99, 1], [100, 1], [101, 1]],
        "bids": [[97, 1], [96, 1], [95, 1]],
        "resp_ts": 2,
        "server_ts": 2,
    }
    ex_name = "ex2"
    xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

    depth_dict = {
        "asks": [[99, 1], [100, 1], [101, 1]],
        "bids": [[97, 1], [96, 2], [95, 1]],
        "resp_ts": 3,
        "server_ts": 3,
    }
    ex_name = "ex2"
    xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))

    print(xex_depth)


def test_xex_router():
    symbol = "ETH/USDT"

    xex_depth = XExDepth()

    depth_dict = {
        "asks": [[100, 1], [101, 1], [102, 1]],
        "bids": [[98, 1], [97, 1], [96, 1]],
        "resp_ts": 1,
        "server_ts": 1,
    }
    ex_name = "ex1"
    xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))
    xex_depth.update_24h_trade_vol_for_ex(ex_name=ex_name, _24h_trade_vol=Decimal(10))

    depth_dict = {
        "asks": [[99, 1], [100, 1], [101, 1]],
        "bids": [[97, 1], [96, 1], [95, 1]],
        "resp_ts": 2,
        "server_ts": 2,
    }
    ex_name = "ex2"
    xex_depth.update_depth_for_ex(ex_name=ex_name, depth=Depth.from_dict(depth=depth_dict))
    xex_depth.update_24h_trade_vol_for_ex(ex_name=ex_name, _24h_trade_vol=Decimal(1))

    xex_router = XExRouter()

    xex_router.update_xex_depth(symbol=symbol, xex_depth=xex_depth)

    dest_ex = xex_router.get_destination_exchange(symbol=symbol, price=Decimal(96), quantity=Decimal(2), direction=Direction.long)

    print(dest_ex)


if __name__ == '__main__':
    test_xex_router()