import pandas as pd

from xex_mm.signals.factors.trade_informedness import TradeInformednessNode2
from xex_mm.signals.labels import FowardRefPrcRetNode2

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    x = TradeInformednessNode2(ex="xex", pair="axs_usdt_swap").get(datetime="2022072108", refresh=False)
    y = FowardRefPrcRetNode2(contract="axs_usdt_swap", ex="xex", T=1000).get(datetime="2022072108", refresh=False)
    data = pd.concat([x, y], axis=1).ffill()
    data[["informedness", "fwd_ret"]].plot()
    plt.show()
    plt.scatter(data["informedness"], data["fwd_ret"])
    plt.show()