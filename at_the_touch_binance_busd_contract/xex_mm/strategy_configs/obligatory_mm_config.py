from dataclasses import dataclass
from typing import List, Tuple


@dataclass
class ObligatoryMmConfig:
    version: str
    contract: str
    ex: str
    hedge_exs: List[str]
    profit_margin_return: float
    cancel_tolerance: float
    requirements: List[Tuple]
