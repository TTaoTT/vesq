from dataclasses import dataclass
from typing import List, Set, Tuple, Union, Dict

import numpy as np

from xex_mm.xex_executor.params import ModelParams


class PredRefPrcMode:
    ret = "ret"
    prc = "prc"
    tar = "tar"
    neutral = "neutral"
    xex = "xex"


@dataclass
class QuoteLevelConfig:
    RETURN_UB: float
    RETURN_STEP_SIZE: float
    QUOTE_QUANTITY: float
    CANCEL_TOL: float


@dataclass
class Config:
    CONTRACT: str
    MM_EXCHANGES: List[str]
    ART: float
    SIGMAT: float
    HST: float
    RETT: float
    REFPRCT: float
    INVENT: float
    DMT: float
    TRL_COUNT: int
    TRADE_STABILITY_BUFFER_WINDOW: int
    REF_PRC_RESET_SIGMA_MULTIPLIER: float
    TAU_LB: int
    T: int
    LATENCYT: float
    QUOTE_DEFENSE_T: int
    EVENT_COUNT_HALFLIFE: int
    BBO_SWITCH: bool
    FOLDER_NAME: str
    BETA: float
    TRADE_DEFENSE_CANCEL_AR_MULTIPLIER: float
    DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER: float
    DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER: float
    PRED_ON: bool
    PRED_REF_PRC_MODE: PredRefPrcMode
    PROFIT_MARGIN_RETURN: float
    MDL_PARAMS: ModelParams
    QUOTE_LVL_CONFIGS: Dict[str, QuoteLevelConfig]
    MAX_INVEN: float
    CONSECUTIVE_FILL_LIMIT: int
    MSG: str

    def to_dict(self):
        dct = self.__dict__.copy()
        dct["MDL_PARAMS"] = self.MDL_PARAMS.to_dict()
        dct["QUOTE_LVL_CONFIGS"] = dict()
        for ex, qlc in self.QUOTE_LVL_CONFIGS.items():
            dct["QUOTE_LVL_CONFIGS"][ex] = qlc.__dict__.copy()
        return dct