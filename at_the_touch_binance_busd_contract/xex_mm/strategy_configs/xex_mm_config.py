from dataclasses import dataclass
from typing import List, Set, Tuple, Union, Dict

import numpy as np

from xex_mm.xex_executor.params import ModelParams


class PredRefPrcMode:
    ret = "ret"
    prc = "prc"
    tar = "tar"
    neutral = "neutral"
    xex = "xex"


@dataclass
class QuoteLevelConfig:
    TARGET_FILL_PROB: float
    QTY_UB: float
    CANCEL_TOL: float


@dataclass
class Config:
    CONTRACT: str
    DYNAMIC_MM_EXCHANGES: List[str]
    SNAP_HEDGE_EXCHANGES: List[str]
    ART: float
    SIGMAT: float
    HST: float
    RETT: float
    REFPRCT: float
    INVENT: float
    DMT: float
    TRL_COUNT: int
    REF_PRC_RESET_SIGMA_MULTIPLIER: float
    TAU_LB: int
    T: int
    LATENCYT: float
    QUOTE_DEFENSE_T: int
    EVENT_COUNT_HALFLIFE: int
    BBO_SWITCH: bool
    FOLDER_NAME: str
    BETA: float
    TRADE_DEFENSE_CANCEL_AR_MULTIPLIER: float
    DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER: float
    DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER: float
    PRED_ON: bool
    PRED_REF_PRC_MODE: PredRefPrcMode
    PROFIT_MARGIN_RETURN: float
    MDL_PARAMS: ModelParams
    QUOTE_LVL_CONFIGS: Dict[str, List[QuoteLevelConfig]]
    QUOTE_FILL_PROB_CUTOFFS: Dict[str, float]
    MSG: str

    def to_dict(self):
        dct = self.__dict__.copy()
        dct["MDL_PARAMS"] = self.MDL_PARAMS.to_dict()
        dct["QUOTE_LVL_CONFIGS"] = dict()
        for ex, qlcs in self.QUOTE_LVL_CONFIGS.items():
            dct["QUOTE_LVL_CONFIGS"][ex] = [qlc.__dict__.copy() for qlc in qlcs]
        return dct