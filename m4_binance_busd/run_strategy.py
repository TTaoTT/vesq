import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_busd.spot.na"
        self.buy_trade_cache = PriceDistributionCache(self.params["cache_size"])
        self.sell_trade_cache = PriceDistributionCache(self.params["cache_size"])

        self.quantile = self.params["quantile"]

        self.price_muli = 1e8
        self.qty_muli = 1e2
        self.recvWindow = self.params["recvWindow"]

        self.order_to_send = Queue()
        self.order_to_cancel = Queue()
        self.retreat_queue = Queue()
        self.trade_ts = time.time()*1e3
        self.bbo_ts = time.time()*1e3
        self.pos_limit = Decimal(5)

        self.order_cache = dict()
        self.ask_order_cache = dict()
        self.bid_order_cache = dict()

        self.canceling_id = set()

        # self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False
        
        self.order_count = 0
        self.order_fail_count = 0

        self.delay = 0
        
        self.last_symbol = None
    
        self.buy_amount = 0
        self.buy_qty = 0
        self.sell_amount = 0
        self.sell_qty = 0

        self.price = 0

    def remove_order(self,cid):
        order = self.order_cache.pop(cid)
        if order.side == OrderSide.Buy:
            self.buy_amount += order.filled_amount * order.avg_filled_price
            self.buy_qty += order.filled_amount
            self.bid_order_cache.pop(cid)
        else:
            self.sell_amount += order.filled_amount * order.avg_filled_price
            self.sell_qty += order.filled_amount
            self.ask_order_cache.pop(cid)

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        self.logger.warning(f"failed order: {err}")
        o = self.order_cache[client_id]
        o.xchg_status = OrderStatus.Failed
        await self.on_order(o)

    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.9*1200
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.9*50
        if cond1 or cond2:
            self.hold_back = True
            self.logger.warning("hold back")
            await asyncio.sleep(1)
            self.hold_back = False

    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol)
        # self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        try:
            symbol_cfg = self.get_symbol_config(self.symbol)
            qty_tick = symbol_cfg["qty_tick_size"]
            price_tick = symbol_cfg['price_tick_size']
            self.half_step_tick = qty_tick/Decimal(2)
            c = 0
            if Decimal(self.price_muli)*price_tick < Decimal(0.5) or Decimal(self.qty_muli)*qty_tick < Decimal(0.5):
                self.logger.warning("price or qty multi not good enough")
                exit()
            while True:
                await asyncio.sleep(1)
                c += 1
                if self.price:
                    self.logger.warning(f"{self.price}")

                    amount = Decimal(self.params["amount"]) / Decimal(self.price)
                    amount = float(int(amount/qty_tick)*qty_tick)
                    
                    self.amount = amount
                    self.amount_decimal = Decimal(amount)
                    break
                if c > 1000:
                    break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise

    async def update_amount(self):
        while True:
            await asyncio.sleep(60*60)
            try:
            
                symbol_cfg = self.get_symbol_config(self.symbol)
                qty_tick = symbol_cfg["qty_tick_size"]
                if self.price:
                    amount = Decimal(self.params["amount"]) / Decimal(self.price)
                    amount = float(int(amount/qty_tick)*qty_tick)
                    
                    self.amount = amount
                    self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")

            
    # async def on_order(self, o):
    #     # self.logger.warning(f"on order here,{o}")
    #     if o["e"] != "ORDER_TRADE_UPDATE":
    #         return
    #     order = o["o"]
    #     client_id = order["c"]
    #     if client_id not in list(self.order_cache): 
    #         return
        
    #     if Decimal(order["z"]) < self.order_cache[client_id].filled_amount:
    #         return
        
    #     status = BinanceHelper.order_status_mapping_rev[order["X"]]
        
    #     self.handle_pos(client_id,order)
    #     self.order_cache[client_id].filled_amount = Decimal(order["z"])
    #     self.order_cache[client_id].avg_filled_price = Decimal(order["ap"])
    #     self.order_cache[client_id].xchg_status = status
        
    #     if status in OrderStatus.fin_status():
    #         if Decimal(order["z"]) > Decimal(0):
    #             self.logger.warning(f"order filled at {Decimal(order['ap'])},side:{self.order_cache[client_id].side}")
    #         self.remove_order(client_id)

    async def on_order(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache): 
            return

        

        if order.xchg_status in OrderStatus.fin_status():
            self.order_cache[client_id].filled_amount = order.filled_amount
            self.order_cache[client_id].avg_filled_price = order.avg_filled_price
            self.remove_order(client_id)

    # async def on_bbo(self, symbol, bbo):
    #     if symbol != self.last_symbol:
    #         self.logger.warning(f"wrong symbol: {symbol}")
    #     self.last_symbol = symbol
        
    #     t_s = bbo["data"]["E"] - self.bbo_ts
        
    #     self.ap = float(bbo["data"]["a"])
    #     self.bp = float(bbo["data"]["b"])
        
    #     self.buy_trade_cache.remove_expired(bbo["data"]["E"])
    #     self.sell_trade_cache.remove_expired(bbo["data"]["E"])

    #     if (not self.bid_order_cache or not self.ask_order_cache) and self.order_to_send.empty() and t_s > 80:
    #         self.bbo_ts = bbo["data"]["E"]
    #         self.s = time.time()
    #         self.order_to_send.put_nowait(1)
    #     if self.order_cache and self.order_to_cancel.empty():
    #         self.order_to_cancel.put_nowait(1)
    
    async def on_public_trade(self, symbol, trade):
        self.price = trade.price
        self.trade_ts = trade.server_ms
        tmp = {
            "t":trade.server_ms,
            "p":int(float(trade.price)*self.price_muli), 
            "q":int(float(trade.quantity)*self.qty_muli)
            }
        if trade.side == OrderSide.Sell:
            self.sell_trade_cache.feed_trade(tmp)
        else:
            self.buy_trade_cache.feed_trade(tmp)

        self.buy_trade_cache.remove_expired(trade.server_ms)
        self.sell_trade_cache.remove_expired(trade.server_ms)

    async def send_order_action(self):
        while True:
            await asyncio.sleep(0.5)
            ask = self.buy_trade_cache.get_ask_price(self.quantile)
            bid = self.sell_trade_cache.get_bid_price(self.quantile)

            if not self.rate_limit and not self.hold_back and self.send:
                if ask and not self.ask_order_cache:
                    self.loop.create_task(self.send_order(Decimal(ask/self.price_muli),self.amount_decimal,OrderSide.Sell))
                if bid and not self.bid_order_cache:
                    self.loop.create_task(self.send_order(Decimal(bid/self.price_muli),self.amount_decimal,OrderSide.Buy))

    async def rate_limit_check(self):
        while True:
            await self.retreat_queue.get()
            if self.rate_limit and self.send:
                self.send = False
                await self.retreat()
                self.send = True
                
    async def exit_logic(self):
        while True:
            await asyncio.sleep(1)
            if time.time()*1e3 - self.trade_ts > 60*1e3:
                self.send = False
                await self.retreat()
                self.logger.warning("no data exit")
                exit()
            
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=None,stop_ts=0.5):
        client_id = ClientIDGenerator.gen_client_id("binance")
        self.order_count += 1
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow

        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = stop_ts
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        if side == OrderSide.Sell:
            self.ask_order_cache[client_id] = order
        else:
            self.bid_order_cache[client_id] = order
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=int(time.time()*1e3),
                recvWindow=recvWindow,
                client_id=client_id
            )
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            self.order_fail_count += 1
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
        except ExchangeTemporaryError as err:
            await self.internal_fail(client_id, err)
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def batch_cancel(self, oid, order):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                await self.cancel_order(order)
                self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):

        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
                
        while True:
            await asyncio.sleep(0.2)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] +order.message["query"]*6)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order(order_new)
                    elif self.order_cache[oid].message["query"]>10:
                        self.send = False
                        await self.retreat()
                        await self.redis_set_cache({})
                        self.logger.warning(f"check order too many times and exiting")
                        exit()
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                if self.sell_qty and self.buy_qty:
                    sell_p = self.sell_amount / self.sell_qty
                    buy_p = self.buy_amount / self.buy_qty
                    await self.redis_set_cache({"exit":None,"spread":(sell_p-buy_p) / (sell_p + buy_p) * 2})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
            await asyncio.sleep(3)
            
    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.update_redis_cache(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            self.update_amount(),
            self.rate_limit_check(),
            self.exit_logic()
            # self.debug_funtion(),
            # self.time_control_function()
            )

    # async def debug_funtion(self):
    #     while True:
    #         await asyncio.sleep(1)
    #         if self.order_cache:
    #             self.logger.warning(f"all order:{self.order_cache}")
    #             self.logger.warning(f"ask order:{self.ask_order_cache}")
    #             self.logger.warning(f"bid order:{self.bid_order_cache}")
    
    # async def time_control_function(self):
    #     await asyncio.sleep(60*30)
    #     await self.retreat()
    #     self.logger.warning("testing over")
    #     exit()
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    