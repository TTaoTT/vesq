# import os
# os.environ["ATOM_PRINT_RESPONSE"] = '1'
from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "preprod")
from strategy_base.base import CommonStrategy
import time
import asyncio
from asyncio.queues import Queue, LifoQueue
import traceback

from atom.model import OrderSide, PartialOrder, OrderStatus, BBODepth, OrderPositionSide, OrderType
from atom.model.depth import Depth as AtomDepth
from atom.model.order import Order as AtomOrder
from atom.exceptions import OrderNotFoundError
from typing import Set, AnyStr, Union, Dict, List, Tuple
from decimal import Decimal
import numpy as np

from collections import deque, defaultdict

import aiohttp

import json

from xex_mm.dmm_strategy.executor import UserProfileTracker

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.contracts = [
            "btc_usdt_swap",
            "eth_usdt_swap",
            "ape_usdt_swap",
            "gmt_usdt_swap",
            "xrp_usdt_swap",
            "matic_usdt_swap",
            "ada_usdt_swap",
            "sol_usdt_swap",
            "gala_usdt_swap",
            "etc_usdt_swap",
            "axs_usdt_swap",
            "doge_usdt_swap",
            "atom_usdt_swap",
            "near_usdt_swap",
            "sand_usdt_swap",
            "bnb_usdt_swap",
        ]
        
        
        with open("blacklist.json") as f:
            data = json.load(f)
            
        self.pair_to_contract_mapper = dict()
            
        self.tracker: Dict[UserProfileTracker] = dict()
        for contract in self.contracts:
            ccy1, ccy2, _ = contract.split("_")
            xchg_pair = (f"{ccy1}{ccy2}").upper()
            self.pair_to_contract_mapper[xchg_pair] = contract
            user_ids = set(data.get(xchg_pair, []))
            self.logger.info(f"{contract} blacklist:{user_ids}")
            self.tracker[xchg_pair] = UserProfileTracker(xchg_pair=xchg_pair, user_ids=user_ids)
            
        self.last_seq = None
        self.break_point = 0
        
        self.counter = 1
    
    async def before_strategy_start(self):
        # for acc in self.account_names:
        #     self.direct_subscribe_order_update(symbol_name=f"lbank.btc_usdt_swap.swap.na", account_name=acc)
        
        self.recv_lbank_user_trade(self.on_user_profile)
    
    async def on_user_profile(self, data: Dict):
        try:
            # self.logger.info(f"getting data:{data}")
            xchg_pair = data.get("InstrumentID")
            if self.tracker.get(xchg_pair) != None:
                if data.get("MemberID") in self.tracker[xchg_pair].user_ids:
                    self.logger.info(f"getting black list data:{data}")
                    self.logger.info(f"before total position:{self.tracker[xchg_pair].total_position}")
                    self.logger.info(f"before single position:{self.tracker[xchg_pair].single_position[data.get('MemberID')]}")
                    
                    self.tracker[xchg_pair].on_user_profile(xchg_pair, user_trade=data)
                    
                    self.logger.info(f"after total position:{self.tracker[xchg_pair].total_position}")
                    self.logger.info(f"after single position:{self.tracker[xchg_pair].single_position[data.get('MemberID')]}")
        except:
            traceback.print_exc()
        
    def recv_lbank_user_trade(self, callback):
            _URL_ = "ws://172.31.4.70:11005/?Token=shadows_trade_token"
            _MESSAGE_ = {"SendTopicAction": {"Action": "1", "LocalNo": 0, "ResumeNo": 0, "TopicID": "13"}}

            async def _recv_():
                async with aiohttp.ClientSession() as session:
                    async with session.ws_connect(_URL_) as conn:
                        self.counter = 1
                        await conn.send_json(_MESSAGE_)
                        while True:
                            message = await conn.receive_json()
                            msg_action = message["action"]
                            if msg_action == "RecvTopicAction":
                                self.logger.info(f"subscribe lbank user trade result: {message['errorMsg']}")
                            elif msg_action == "PushTrade":
                                if self.last_seq is not None and message["seq"] - self.last_seq >= 2:
                                    self.break_point += 1
                                    self.logger.info("message lost")
                                self.last_seq = message["seq"]
                                    
                                for result in message["result"]:
                                    item = result["data"]
                                    await callback(item)

            async def keep_run():
                while True:
                    try:
                        await _recv_()
                    except Exception as err:
                        self.logger.error(f"lbank user trade subscription fail: {err}")
                    await asyncio.sleep(min(1, 0.1 * self.counter))
                    self.counter += 1

            self.loop.create_task(keep_run())
            
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            res = dict()
            for xchg_pair in self.tracker:
                contract = self.pair_to_contract_mapper[xchg_pair]
                res[contract] = self.tracker[xchg_pair].total_position
            await self.cache_redis.handler.set("cache:hf_lbank:blacklist_position",json.dumps(res))
            # for k in self.tracker:
            #     t = self.tracker[k]
            #     self.logger.info(f"{k} total position:{t.total_position}")
            
            res = await self.cache_redis.handler.get("cache:hf_lbank:blacklist_position")
            if res:
                await self.redis_set_cache(json.loads(res))
                
    async def reboot_strategy(self):
        while True:
            await asyncio.sleep(60 * 10)
            if self.break_point > 0:
                self.logger.info("strategy reboot")
                for k in self.tracker:
                    self.tracker[k].reset()
                self.break_point = 0
            

    async def strategy_core(self):
        await asyncio.sleep(3)
        await asyncio.gather(
            self.set_cache(),
            self.reboot_strategy()
            
            )
        
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    
                    
                