import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
    def get_qty(self):
        return self.q_sum
    
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.buy_trade_cache = PriceDistributionCache(self.params["cache_size"])
        self.sell_trade_cache = PriceDistributionCache(self.params["cache_size"])

        self.quantile = self.params["quantile"]
        self.weight = self.params["weight"]

        self.price_muli = 1e7
        self.qty_muli = 1e7
        self.recvWindow = self.params["recvWindow"]

        self.order_to_send = Queue()
        self.order_to_cancel = Queue()
        self.retreat_queue = Queue()
        self.trade_ts = 0
        self.bbo_ts = time.time()*1e3
        self.pos_limit = Decimal(5)

        self.order_cache = dict()
        self.ask_order_cache = dict()
        self.bid_order_cache = dict()

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False
        
        self.order_count = 0
        self.order_fail_count = 0

        self.delay = 0
        
        self.last_symbol = None
        
        self.ap = None
        self.bp = None

    def remove_order(self,cid):
        order = self.order_cache.pop(cid)
        if order.side == OrderSide.Buy:
            self.bid_order_cache.pop(cid)
        else:
            self.ask_order_cache.pop(cid)

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        self.logger.warning(f"failed order: {err}")
        o = self.order_cache[client_id]
        o.xchg_status = OrderStatus.Failed
        await self.on_order_inner(o)

    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.9*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.9*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.9*1200

        if cond1 or cond2 or cond3:
            self.hold_back = True
            self.logger.warning("hold back")
            await asyncio.sleep(0.5)
            self.hold_back = False

    
    def update_config_before_init(self):
        self.use_colo_url = True

    async def before_strategy_start(self):
        self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.pos = position.data["long_qty"] - position.data["short_qty"]
                
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol)
                qty_tick = symbol_cfg["qty_tick_size"]
                price_tick = symbol_cfg['price_tick_size']
                self.half_step_tick = qty_tick/Decimal(2)
                c = 0
                if Decimal(self.price_muli)*price_tick < Decimal(0.5) or Decimal(self.qty_muli)*qty_tick < Decimal(0.5):
                    self.logger.warning("price or qty multi not good enough")
                    exit()
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ap:
                        self.logger.warning(f"{self.ap}")
                        amount = min(balance,Decimal(5000)) * Decimal(0.4)
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/qty_tick)*qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
                        break
                    if c > 1000:
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise

    async def update_amount(self):
        while True:
            await asyncio.sleep(60*60*24)
            try:
                api = self.get_exchange_api("binance")[0]  
                cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
                equity = cur_balance.get('usdt')
                if equity:
                    balance = equity["all"]
                    symbol_cfg = self.get_symbol_config(self.symbol)
                    qty_tick = symbol_cfg["qty_tick_size"]
                    if self.ap:
                        amount = min(balance,Decimal(5000)) * Decimal(0.4)
                        amount = amount / Decimal(self.ap)
                        amount = float(int(amount/qty_tick)*qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")

    def handle_pos_inner(self,client_id,p_order):
        amount_changed = p_order.filled_amount - self.order_cache[client_id].filled_amount
        if not amount_changed:
            return

        if self.order_cache[client_id].side == OrderSide.Buy:
            self.pos += amount_changed
        else:
            self.pos -= amount_changed
            
    def handle_pos(self,client_id,order):
        amount_changed = Decimal(order["z"]) - self.order_cache[client_id].filled_amount
        if not amount_changed:
            return

        if self.order_cache[client_id].side == OrderSide.Buy:
            self.pos += amount_changed
        else:
            self.pos -= amount_changed
            
    async def on_order(self, o):
        # self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        client_id = order["c"]
        if client_id not in list(self.order_cache): 
            return
        
        if Decimal(order["z"]) < self.order_cache[client_id].filled_amount:
            return
        
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        
        self.handle_pos(client_id,order)
        self.order_cache[client_id].filled_amount = Decimal(order["z"])
        self.order_cache[client_id].avg_filled_price = Decimal(order["ap"])
        self.order_cache[client_id].xchg_status = status
        
        if status in OrderStatus.fin_status():
            if Decimal(order["z"]) > Decimal(0):
                self.logger.warning(f"order filled at {Decimal(order['ap'])},side:{self.order_cache[client_id].side}")
            self.remove_order(client_id)

    async def on_order_inner(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache): 
            return
        
        if order.filled_amount < self.order_cache[client_id].filled_amount:
            return

        self.handle_pos_inner(client_id,order)
        self.order_cache[client_id].filled_amount = order.filled_amount
        self.order_cache[client_id].avg_filled_price = order.avg_filled_price
        self.order_cache[client_id].xchg_status = order.xchg_status

        if order.xchg_status in OrderStatus.fin_status():
            if order.filled_amount > Decimal(0):
                self.logger.warning(f"order filled at {order.avg_filled_price},side:{self.order_cache[client_id].side}")
            self.remove_order(client_id)

    async def on_bbo(self, symbol, bbo):
        if symbol != self.last_symbol:
            self.logger.warning(f"wrong symbol: {symbol}")
        self.last_symbol = symbol
        
        t_s = bbo["data"]["E"] - self.bbo_ts
        
        self.ap = float(bbo["data"]["a"])
        self.bp = float(bbo["data"]["b"])
        
        self.buy_trade_cache.remove_expired(bbo["data"]["E"])
        self.sell_trade_cache.remove_expired(bbo["data"]["E"])

        if (not self.bid_order_cache or not self.ask_order_cache) and self.order_to_send.empty():
            self.bbo_ts = bbo["data"]["E"]
            self.s = time.time()
            self.order_to_send.put_nowait(1)
        if self.order_cache and self.order_to_cancel.empty():
            self.order_to_cancel.put_nowait(1)
    
    async def on_public_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["E"]
        self.trade_ts = max(self.trade_ts, trade["data"]["E"])
        tmp = {
            "t":trade["data"]["E"],
            "p":int(float(trade["data"]["p"])*self.price_muli), 
            "q":int(float(trade["data"]["q"])*self.qty_muli)
            }
        if trade["data"]["m"]:
            self.sell_trade_cache.feed_trade(tmp)
        else:
            self.buy_trade_cache.feed_trade(tmp)

        self.buy_trade_cache.remove_expired(self.trade_ts)
        self.sell_trade_cache.remove_expired(self.trade_ts)

    async def send_order_action(self):
        while True:
            await self.order_to_send.get()
            ask = self.buy_trade_cache.get_ask_price(self.quantile)
            bid = self.sell_trade_cache.get_bid_price(self.quantile)
            

            if ask and bid:
                weight = ask - bid
                weight = weight / (ask + bid) * 2

            cur_risk = self.pos/self.amount_decimal
            
            if not self.rate_limit and (not ask or not bid or weight < self.weight or cur_risk > self.pos_limit or cur_risk < -self.pos_limit) and self.send:
                await self.send_balance_order(cur_risk)
            elif not self.rate_limit and not self.hold_back and self.send and self.delay<25:
                self.logger.warning(f"price gap: {weight}")
                weight = weight/0.001
                weight = min(weight, 1.5)
                qty = self.amount * weight
                await self.send_entry_order(Decimal(ask/self.price_muli),Decimal(bid/self.price_muli),Decimal(qty))
            elif not self.rate_limit and not self.hold_back and self.send and self.delay>=25:
                await self.send_balance_order(cur_risk)

    async def rate_limit_check(self):
        while True:
            await self.retreat_queue.get()
            if self.rate_limit and self.send:
                self.send = False
                await self.retreat()
                self.send = True
                
    async def exit_logic(self):
        while True:
            await asyncio.sleep(1)
            if time.time()*1e3 - self.bbo_ts > 60*1e3:
                self.send = False
                await self.retreat()
                self.logger.warning("no data exit")
                exit()
            
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.PostOnly,recvWindow=None,timestamp=None,stop_ts=0.2):
        client_id = ClientIDGenerator.gen_client_id("binance")
        self.order_count += 1
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow
        if timestamp == None:
            timestamp = self.bbo_ts

        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = stop_ts
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache[client_id] = order
        if side == OrderSide.Sell:
            self.ask_order_cache[client_id] = order
        else:
            self.bid_order_cache[client_id] = order
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=timestamp,
                recvWindow=recvWindow,
                client_id=client_id
            )
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            self.order_fail_count += 1
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except ExchangeTemporaryError as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def send_balance_order(self,cur_risk):
        if cur_risk>0 and not self.ask_order_cache:
            self.logger.warning(f"balancing at risk: {cur_risk}")
            await self.send_order(Decimal(self.ap),min(self.amount_decimal,abs(self.pos)),OrderSide.Sell,OrderPositionSide.Open,OrderType.PostOnly,timestamp=self.bbo_ts)
        elif cur_risk<0 and not self.bid_order_cache:
            self.logger.warning(f"balancing at risk: {cur_risk}")
            await self.send_order(Decimal(self.bp),min(self.amount_decimal,abs(self.pos)),OrderSide.Buy,OrderPositionSide.Open,OrderType.PostOnly,timestamp=self.bbo_ts)

    async def send_entry_order(self,ask,bid,qty):
        qty_offset = min(self.amount_decimal,abs(self.pos))
        if not self.bid_order_cache and not self.ask_order_cache:
            if self.pos > Decimal(0):
                await asyncio.gather(
                    self.send_order(ask,qty+qty_offset,OrderSide.Sell),
                    self.send_order(bid,qty,OrderSide.Buy)
                )
            else:
                await asyncio.gather(
                    self.send_order(ask,qty,OrderSide.Sell),
                    self.send_order(bid,qty+qty_offset,OrderSide.Buy)
                )
        elif not self.bid_order_cache:
            if self.pos > Decimal(0):
                await self.send_order(bid,qty,OrderSide.Buy)
            else:
                await self.send_order(bid,qty+qty_offset,OrderSide.Buy)
        elif not self.ask_order_cache:
            if self.pos > Decimal(0):
                await self.send_order(ask,qty+qty_offset,OrderSide.Sell)
            else:
                await self.send_order(ask,qty,OrderSide.Sell)

    async def batch_cancel(self, oid, order):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                await self.cancel_order(order)
                self.canceling_id.remove(oid)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):

        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
                
        while True:
            await self.order_to_cancel.get()
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] +order.message["query"]*6)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order_inner(order_new)
                    elif self.order_cache[oid].message["query"]>10:
                        self.send = False
                        await self.retreat()
                        await self.redis_set_cache({})
                        self.logger.warning(f"check order too many times and exiting")
                        exit()
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def check_position(self):
        while True:
            await asyncio.sleep(60)
            try:
                if not self.rate_limit:
                    api = self.get_exchange_api("binance")[0]
                    position = await api.contract_position(self.get_symbol_config(self.symbol))
                    pos = position.data["long_qty"] - position.data["short_qty"]
                    if abs(pos - self.pos)>self.amount_decimal*self.pos_limit:
                        self.logger.warning(
                        f"""
                        =========update wrong position==========
                        origin: {self.pos}
                        update: {pos}
                        """
                    )
                    self.pos = pos
                    
            except Exception as err:
                self.logger.warning(f'check position err {err}')

    async def retreat(self):
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(Decimal(self.bp*0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
                else:
                    await self.send_order(Decimal(self.ap*1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None,"fail rate":self.order_fail_count/self.order_count if self.order_count else 0})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(1)
        await asyncio.gather(
            self.update_redis_cache(),
            self.check_position(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            self.update_amount(),
            self.rate_limit_check(),
            self.exit_logic()
            # self.debug_funtion(),
            # self.time_control_function()
            )

    # async def debug_funtion(self):
    #     while True:
    #         await asyncio.sleep(1)
    #         if self.order_cache:
    #             self.logger.warning(f"all order:{self.order_cache}")
    #             self.logger.warning(f"ask order:{self.ask_order_cache}")
    #             self.logger.warning(f"bid order:{self.bid_order_cache}")
    
    # async def time_control_function(self):
    #     await asyncio.sleep(60*30)
    #     await self.retreat()
    #     self.logger.warning("testing over")
    #     exit()
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    