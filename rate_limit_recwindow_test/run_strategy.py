import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
import aiohttp
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.rate_limit = False
        self.symbol = "binance.near_usdt_swap.usdt_contract.na"
        self.ts = 0

        self.total_count = 0
        self.expired_count = 0

        self.delay = collections.deque(maxlen=100000)

        self.order_to_send = Queue()

        self.use_raw_stream = True

        self.last_bbo_id = 0
        self.last_trade_id = 0

        self.use_colo_url = True

    async def before_strategy_start(self):
        # self.direct_subscribe_orderbook(symbol_name=self.symbol)
        # self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        # self.direct_subscribe_public_trade(symbol_name=self.symbol)
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        # self.logger.setLevel("WARNING")

    # def update_config_before_init(self):
    #     self.use_colo_url = True

    # async def on_public_trade(self, symbol, trade):
        
    #     trade_id = trade["data"]["t"]
    #     if trade_id - self.last_trade_id == 1 or self.last_trade_id == 0:
    #         self.last_trade_id = trade_id
    #         if time.time()*1e3 - trade["data"]["E"] > 300:
    #             self.logger.warning(f"delay for trade: {time.time()*1e3 - trade['data']['E']}")
    #     elif trade_id - self.last_trade_id < 1:
    #         return
    #     elif trade_id - self.last_trade_id > 1:
    #         self.logger.warning(f"!!!!!!!!!!not in order")
    #         return
    
    async def on_agg_trade(self, symbol, trade):
        self.logger.warning(f"agg_{trade['data']}")

    async def on_bbo(self,symbol,bbo):
        ap = Decimal(bbo["data"]["a"])
        bp = Decimal(bbo["data"]["b"])
        self.mp = (ap + bp) / Decimal(2)
        self.ts= max(self.ts,bbo["data"]["E"])

        if self.order_to_send.empty():
            self.order_to_send.put_nowait(1)
        
        
        
        # bbo_id = bbo["data"]["u"]
        # if bbo_id > self.last_bbo_id:
        #     self.last_bbo_id = bbo_id
        #     if time.time()*1e3 - bbo["data"]["E"] > 300:
        #         self.logger.warning(f"delay for bbo: {time.time()*1e3 - bbo['data']['E']}")

    # async def on_orderbook(self, symbol, orderbook):
    #     ap = orderbook["asks"][0][0]
    #     bp = orderbook["bids"][0][0]
    #     self.mp = (ap + bp) / Decimal(2)
    #     self.ts= max(self.ts,orderbook["server_ts"])

    #     if self.order_to_send.empty():
    #         self.order_to_send.put_nowait(1)
    
    # async def handle_limit(self,headers):
    #     cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.9*2400
    #     cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.9*300
    #     cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.9*1200
    #     self.logger.warning(f"""weight1m:{float(headers['X-MBX-USED-WEIGHT-1M'])}, 
    #                         order10s:{float(headers['X-MBX-ORDER-COUNT-10S'])},
    #                         order1m:{float(headers['X-MBX-ORDER-COUNT-1M'])}
    #                         """)

    #     if cond1 or cond2 or cond3:
    #         self.hold_back = True
    #         await asyncio.sleep(60)
    #         self.logger.warning("rest for 1 min done")

    async def send_order_action(self):
        ts = int(time.time()*1e3)
        try:
            self.total_count += 1
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price = self.mp * Decimal(1.03),
                volume=Decimal(1),
                side=OrderSide.Sell,
                position_side=OrderPositionSide.Open,
                order_type=OrderType.PostOnly,
                timestamp=self.ts,
                recvWindow=10
            )
            # await self.handle_limit(res.headers)
        except RateLimitException as err:
            self.logger.warning(f"rate limit err:{err}")
            self.rate_limit = True
            await asyncio.sleep(60)
            self.logger.warning("rest for 1 min done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            # self.logger.warning(f"outside recWindow:{time.time()*1e3-ts}----{err}")
            self.delay.append(time.time()*1e3-ts)
        except Exception as err:
            self.logger.warning(f"send order err:{err}")
            raise

    async def cancel_order_action(self):
        api = self.get_exchange_api("binance")[0]
        try:
            await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
        except RateLimitException as err:
            self.logger.warning(f"rate limit err:{err}")
            self.rate_limit = True
            await asyncio.sleep(60)
            self.logger.warning("rest for 1 min")
            self.rate_limit = False
        except Exception as err:
            self.logger.warning(f"cancel order err:{err}")

    # async def set_cache(self):
    #     while True:
    #         await asyncio.sleep(1)
    #         await self.redis_set_cache(
    #             {
    #             "min delay time":np.min(self.delay) if self.delay else 0,
    #             "max delay time":np.max(self.delay) if self.delay else 0,
    #             "avg delay time":np.mean(self.delay) if self.delay else 0,
    #             }
    #         )

    async def test_function(self):
        while True:
            await asyncio.sleep(10)
            # if not self.rate_limit:
            #     await self.send_order_action()
            #     await self.cancel_order_action()

    async def strategy_core(self):
        await asyncio.sleep(2)
        await asyncio.gather(
            self.test_function(),
            # self.set_cache()
        )
        # while True:
        #     await asyncio.sleep(10)

                
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()

    