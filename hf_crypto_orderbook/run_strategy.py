import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue
import json

from atom.models.order import *
from atom.models.trade_data import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = f"crypto.btc_usdt.spot"
        self.amount_cache = collections.deque(maxlen=10000)
        
    async def before_strategy_start(self):
        await self._get_symbol_config_from_remote([self.symbol])
        self.loop.create_task(self._subscribe_orderbook_direct_(self.symbol))
        self.subscribe_public_trade([self.symbol])
        
    
        
    async def on_orderbook(self, symbol, orderbook):
        amount = 0
        best_ask = orderbook["asks"][0][0]
        best_bid = orderbook["bids"][0][0]
 
        for i in range(len(orderbook["asks"])):
            if (orderbook["asks"][i][0] - best_ask) / best_ask < Decimal(0.0005):
                amount += orderbook["asks"][i][1] * orderbook["asks"][i][0] 
            if (best_bid - orderbook["asks"][i][0]) / best_bid < Decimal(0.0005):
                amount += orderbook["bids"][i][1] * orderbook["bids"][i][0]
                
        self.amount_cache.append(float(amount))
        
    
    async def set_cache(self):
        while True:
            await asyncio.sleep(10)
            cur_balance = await self.get_balance(self.symbol)
            equity = cur_balance.get("usdt")
            await self.redis_set_cache({"btc":np.median(self.amount_cache),"usdt amount":equity,"total balance":json.dumps(cur_balance)})
            
    async def strategy_core(self):
        
        await self.set_cache()
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    