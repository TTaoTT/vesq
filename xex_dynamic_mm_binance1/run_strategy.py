from dataclasses import dataclass
from itertools import filterfalse
import logging
import asyncio
from decimal import Decimal
import time
import trace
import pandas as pd
from asyncio.queues import Queue, LifoQueue
import os
import numpy as np
import traceback
import math
from typing import Dict, Set, List, AnyStr, Union, Tuple
import ujson as json
from orderedset import OrderedSet

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.order import PartialOrder
from atom.model.depth import Depth as AtomDepth
from strategy_base.base import CommonStrategy
from sortedcontainers import SortedDict
from _operator import neg
from orderedset import OrderedSet

from xex_mm.utils.base import Trade, Depth, BBO, Direction, TransOrder, OrderManager, MakerTaker, ReferencePriceCalculator, Order
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig
from xex_mm.utils.contract_mapping import ContractMapper


from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard
from xex_mm.strategy_configs.xex_mm_config import PredRefPrcMode, Config
from xex_mm.xex_executor.order_executor import OrderExecutorTest2

from xex_mm.mm_quoting_model.arrival_rate_model.models.simplified_model_no_partial_fill import SingleArrivalRateXExMmModelNoPartialFill
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_executor import XexMmDeltaNeutralExecutor

class OrderManager(OrderManager):
    def clear(self):
        self._trans_order_cache = {}
        self._ask_ladder = SortedDict()
        self._bid_ladder = SortedDict(neg)
        self._oid_to_iid_map: Dict[str, str] = {}
        self._iid_to_oids_map: Dict[str, OrderedSet] = {}
        self._order_cache = {}
        

price_precision = {
    "binance.reef_usdt_swap":6,
    "binance.rvn_usdt_swap":5,
    'binance.ape_usdt_swap':3,
    'binance.mkr_usdt_swap':1,
    'binance.gmt_usdt_swap':4,
    'binance.hnt_usdt_swap':3,
    'binance.luna2_usdt_swap': 4,
    'binance.luna2_busd_swap': 4,
    'binance.band_usdt_swap':4,
    'binance.mask_usdt_swap':3,
}

qty_precision = {
    "binance.reef_usdt_swap":1,
    "binance.rvn_usdt_swap":0,
    'binance.ape_usdt_swap':0,
    'binance.mkr_usdt_swap':3,
    'binance.gmt_usdt_swap':0,
    'binance.hnt_usdt_swap':0,
    'binance.luna2_usdt_swap': 0,
    'binance.luna2_busd_swap': 0,
    'binance.band_usdt_swap':1,
    'binance.mask_usdt_swap':0,
}

class FeeStructure:
    def __init__(self, maker: float, taker: float) -> None:
        self.maker = maker
        self.taker = taker
        
class FeeConfig:
    def __init__(self) -> None:
        self.config = {
            # BINANCE
            "binance.reef_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            "binance.rvn_usdt_swap":FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.ape_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.mkr_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.gmt_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.hnt_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.luna2_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.luna2_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.luna2_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.luna2_busd_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.band_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
            'binance.mask_usdt_swap': FeeStructure(maker=-0.00001, taker=0.0003),
        }
    
    def get_fee(self, ex: str, contract: str):
        return self.config[f"{ex}.{contract}"].maker, self.config[f"{ex}.{contract}"].taker
    


def setup_q(ret_quantile_arr):
    q_arr = []
    for i, quantile in enumerate(ret_quantile_arr):
        q_arr.append(1)
    return q_arr

def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class LoadBalancer:
    def __init__(self, max_leverage: float) -> None:
        self._position_map = dict()
        self._balance_map = dict()
        self._account_load_map = dict()
        self._ip_load_map = dict()
        
        self.max_leverage = max_leverage
    
    def setup(self, acc_list: list, trader_list: list):
        acc_list = acc_list.copy()
        trader_list = trader_list.copy()
        for acc in acc_list:
            self._position_map[acc] = 0
            self._balance_map[acc] = 0
            self._account_load_map[acc] = 0
        
        for trader in trader_list:
            self._ip_load_map[trader] = 0
        
    
    def get_acc_list(self):
        return sorted(self._account_load_map, key=self._account_load_map.get)
        
    def get_trader_list(self):
        return sorted(self._ip_load_map, key=self._ip_load_map.get)
    
    def get_trader(self):
        return self.get_trader_list()[0]
    
    def update_balance(self, acc:str, balance:float):
        self._balance_map[acc] = balance
    
    def update_position(self, acc: str, chg_qty: float):
        self._position_map[acc] += chg_qty
    
    def volume_allowed(self, acc: str, side: Direction, prc: float):
        if side == Direction.long:
            return self._balance_map[acc] * self.max_leverage / prc - self._position_map[acc]
        else:
            return self._balance_map[acc] * self.max_leverage / prc + self._position_map[acc]
    
    def update_limit(self, acc: str, trader_name: str, acc_load: float, ip_load: float):
        self._account_load_map[acc] = acc_load
        self._ip_load_map[trader_name] = ip_load
    
    def reduce_count(self, reduce: float):
        self._account_load_map[self.get_acc_list()[-1]] -= reduce
        self._ip_load_map[self.get_trader_list()[-1]] -= reduce
        

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.label_oid = None
        self.start_ts = time.time()
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.beta = int(self.config['strategy']['params']['beta'])
        self.quantity_base = int(self.config['strategy']['params']['quantity_base'])
        max_leverage = int(self.config['strategy']['params']['max_leverage'])
        self.bnd_amt_in_coin = Decimal(self.config['strategy']['params']['bnd_amt_in_coin'])
        
        self.genearte_lt = 0
        self._dynamic_mm_order_manager = OrderManager()
        self._snap_hedge_order_manager = OrderManager()
        self._contract_mapper = ContractMapper()
        self.load_balancer = LoadBalancer(max_leverage=max_leverage)
        # self.prc_per_diff = 0.00005 / 5
        self._ref_prc_cal = ReferencePriceCalculator()
        self.bbo_index =0
        self._n_def_cancel = 0
        self._prev_bbo: BBO = ...

        self.pair: str = ...
        self.symbols: List[str] = ...

        self._prev_depth_metrics: Dict = dict()
        self._ref_prc_map: Dict = dict()
        self._ret_map: Dict = dict()
        
        self._dynamic_mm_exchanges = set()
        self._snap_hedge_exchanges = set()
        self._exchanges = set()
        
        self._order_stats = dict()
        self._order_stats["buy_amt"] = 0
        self._order_stats["buy_qty"] = 0
        self._order_stats["sell_amt"] = 0
        self._order_stats["sell_qty"] = 0
        
        self.canceling_id = set()
        
        self.send = False
        
        self.trd_amt= 0 
        
        self.iid_state_manager = IIdStateManager()
        self.server_time_guard = ServerTimeGuard()
        
        self.ob_queue = LifoQueue()
        self.bbo_queue = LifoQueue()
        
        self.cfg: Config = Config(
            CONTRACT=self.contract,
            DYNAMIC_MM_EXCHANGES=["binance"],
            SNAP_HEDGE_EXCHANGES=[],
            ART=1e3,
            SIGMAT=1e4,
            HST=1e4,
            RETT=1e3,
            REFPRCT=1e4,
            INVENT=1e5,
            REF_PRC_RESET_SIGMA_MULTIPLIER=3,
            TAU_LB=100,
            LATENCYT=1e2,
            EVENT_COUNT_HALFLIFE=100,
            BBO_SWITCH=True,
            FOLDER_NAME="",
            TRADE_DEFENSE_CANCEL_AR_MULTIPLIER=3,
            DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER=3,
            DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER=3,
            BETAS=[self.beta],
            QTY_UB_ARR=[self.quantity_base],
            TOLERANCE_ARR=[1],
            MODEL_FILEPATH="",
            PRED_ON=False,
            PRED_REF_PRC_MODE=PredRefPrcMode.prc,
            PROFIT_MARGIN_RETURN=1e-5,
            MSG="",
        )

        self.symbols = []
        for ex in self.cfg.DYNAMIC_MM_EXCHANGES:
            self.symbols.append(f"{ex}.{self.cfg.CONTRACT}")
        for ex in self.cfg.SNAP_HEDGE_EXCHANGES:
            self.symbols.append(f"{ex}.{self.cfg.CONTRACT}")
            
        self.executor = OrderExecutorTest2(
            iids=self.symbols,
            amt_bnd=0,  # This is handled when building depth
            pred_on=self.cfg.PRED_ON,
            art=self.cfg.ART,
            sigmat=self.cfg.SIGMAT,
            hst=self.cfg.HST,
            rett=self.cfg.RETT,
            refprct=self.cfg.REFPRCT,
            latencyt=self.cfg.LATENCYT,
            event_count_halflife=self.cfg.EVENT_COUNT_HALFLIFE,
            q_ubs=self.cfg.QTY_UB_ARR,
            pred_ref_prc_mode=self.cfg.PRED_REF_PRC_MODE,
            betas=self.cfg.BETAS,
            invent=self.cfg.INVENT,
            ref_prc_reset_sigma_multiplier=self.cfg.REF_PRC_RESET_SIGMA_MULTIPLIER,
            tau_lb=self.cfg.TAU_LB,
        )
        if self.executor.pred_on:
            self.executor.update_signal_model(filepath=self.cfg.MODEL_FILEPATH)
        
        """ Snap Hedge Executor """
        self._delta_neutral_executor = XexMmDeltaNeutralExecutor(contract=self.cfg.CONTRACT,
                                                                 profit_margin_return=self.cfg.PROFIT_MARGIN_RETURN)

        self._dynamic_mm_exchanges = set(self.cfg.DYNAMIC_MM_EXCHANGES)
        self._snap_hedge_exchanges = set(self.cfg.SNAP_HEDGE_EXCHANGES)
        self._exchanges = self._dynamic_mm_exchanges | self._snap_hedge_exchanges

        for ex in self.cfg.SNAP_HEDGE_EXCHANGES + self.cfg.DYNAMIC_MM_EXCHANGES:
            makers_fee, takers_fee = FeeConfig().get_fee(ex=ex, contract=self.cfg.CONTRACT)
            self._delta_neutral_executor.update_fee_rate(contract=self.cfg.CONTRACT, ex=ex,
                                                         makers_fee=makers_fee, takers_fee=takers_fee)
        
        self.precision = dict()
        for iid in self.symbols:
            pp_iid = iid
            if self._contract_mapper.is_proxy_iid(iid=iid):
                pp_iid = self._contract_mapper.proxy_spot_iid_to_spot_iid(proxy_spot_iid=iid)
            self.precision[iid] = price_precision[pp_iid]
        self._inventory_map: Dict[str, float] = {}
    
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
    
    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        
    async def before_strategy_start(self):
        
        for acc in self.account_names:    
            ex, _ = acc.split(".")     
            symbol = f"{ex}.{self.cfg.CONTRACT}.{self.market}.{self.sub_market}"
            self.direct_subscribe_order_update(symbol_name=symbol,account_name=acc)
            await asyncio.sleep(1)
        
        for symbol in self.symbols:
            symbol = f"{symbol}.{self.market}.{self.sub_market}"
            symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
            bnd = self.bnd_amt_in_coin / symbol_cfg["contract_value"]
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_agg_trade(symbol_name=symbol)
            self.direct_subscribe_bbo(symbol_name=symbol)
        
        self.loop.create_task(self.handle_ob_update())
        
        ch1 = "mq:hf_signal:order_response"
        ch2 = "mq:hf_signal:cancel_response"
        self.loop.create_task(self.subscribe_to_channel([ch1],self.process_order_response))
        self.loop.create_task(self.subscribe_to_channel([ch2],self.process_cancel_response))
        
        for acc in self.account_names:
            self._account_config_[acc]["cfg_pos_mode"][symbol] = 1
            
        self.load_balancer.setup(
            acc_list=self.account_names,
            trader_list=["hf2_binance_swap","hf3_binance_swap","hf4_binance_swap"]
        )
        
        counter = 0
        while True:
            await asyncio.sleep(1)
            counter += 1
            exs = []
            for ex in self.cfg.DYNAMIC_MM_EXCHANGES:
                exs.append(ex)
            for ex in self.cfg.SNAP_HEDGE_EXCHANGES:
                exs.append(ex)
            
            has_price = True
            for ex in exs:
                if self._ref_prc_map.get((self.cfg.CONTRACT, ex)) == None:
                    has_price = False
                    
            if has_price:
                await self.rebalance()
                break
    
            if counter > 100:
                self.logger.critical("no price info coming")
                exit()
        
        
        self.logger.critical("start......")
        
        self.send = True
        
    def get_inventory(self, iid: str):
        return self._inventory_map[iid]

    def update_inventory(self, iid: str, chg_qty: float, ts: int):
        if iid not in self._inventory_map:
            self._inventory_map[iid] = 0
        self._inventory_map[iid] += chg_qty
        ex, contract = iid.split(".")
        self.executor.update_inventory(contract=contract, inventory=self._inventory_map[iid], ts=ts)
    
    def update_order_stats(self, porder: PartialOrder, torder: TransOrder):
        chg_qty = float(porder.filled_amount) if torder.side == Direction.long else -float(porder.filled_amount)
        self.load_balancer.update_position(acc=porder.account_name, chg_qty=chg_qty)
        if torder.side == Direction.long:
            self._order_stats["buy_amt"] += porder.avg_filled_price * porder.filled_amount
            self._order_stats["buy_qty"] += porder.filled_amount
        else:
            self._order_stats["sell_amt"] += porder.avg_filled_price * porder.filled_amount
            self._order_stats["sell_qty"] += porder.filled_amount
        self.trd_amt += abs(porder.filled_amount) * porder.avg_filled_price
            
    async def on_order(self, order: PartialOrder):
        order_status = order.xchg_status
        oid = order.client_id
        if not self._dynamic_mm_order_manager.contains_trans_order(oid=oid) and not self._snap_hedge_order_manager.contains_trans_order(oid=oid):
            return
        try:
            """ Dynamic MM  """
            if self._dynamic_mm_order_manager.contains_trans_order(oid=oid):
                if oid == self.label_oid:
                    self.logger.critical(f"handling check order: {oid}")
                o: TransOrder = self._dynamic_mm_order_manager.get_trans_order(oid)
                config = self.get_symbol_config(symbol_identity=o.symbol_id)
                order_obj: Order = Order.from_dict(porder=order, torder=o, contract_value=config["contract_value"])

                """ Update Inventory """
                hedge_trans_orders = []
                if order_obj.filled_qty > 0:
                    qty_chg = self._dynamic_mm_order_manager.get_order_inc_fill_qty_in_tokens(order=order_obj)
                    if qty_chg <= 0 and order_status not in OrderStatus.fin_status():
                        self.logger.critical(f"here is something weird, oid={oid}, qty_chg={qty_chg}")
                        return
                    self.update_inventory(iid=o.iid, chg_qty=order_obj.side * qty_chg, ts=int(time.time()*1e3))
                    xex_iid = f"xex.{order_obj.contract}"
                    self.update_inventory(iid=xex_iid, chg_qty=order_obj.side * qty_chg, ts=int(time.time()*1e3))
                    

                    """ Hedge Order """
                    takers_fee, makers_fee = self._delta_neutral_executor.get_fee_rate(contract=order_obj.contract, ex=order_obj.ex)
                    _, hedge_trans_orders = self._delta_neutral_executor.hedge_increment(
                        inc_fill_qty_in_tokens=qty_chg, prc=order_obj.avg_fill_prc, maker_fee=makers_fee,
                        d=order_obj.side)

                self._dynamic_mm_order_manager.update_order(order=order_obj)

                """ Clean up """
                if order_status in OrderStatus.fin_status():
                    self.update_order_stats(porder=order, torder=o)
                    self._dynamic_mm_order_manager.pop_trans_order(oid=oid)
                    self._dynamic_mm_order_manager.pop_order(oid=oid)

                for o in hedge_trans_orders:
                    self.loop.create_task(self.send_snap_hedge_order(order=o))

                return
            
            """ Snap Hedge """
            if self._snap_hedge_order_manager.contains_trans_order(oid=oid):
                o: TransOrder = self._snap_hedge_order_manager.get_trans_order(oid)
                config = self.get_symbol_config(symbol_identity=o.symbol_id)
                order_obj: Order = Order.from_dict(porder=order, torder=o, contract_value=config["contract_value"])

                """ Update inventory """
                if order_obj.filled_qty > 0:
                    qty_chg = self._snap_hedge_order_manager.get_order_inc_fill_qty_in_tokens(order=order_obj)
                    if qty_chg <= 0:
                        return
                    qty_chg = qty_chg if order_obj.side == Direction.long else -qty_chg
                    self.update_inventory(iid=o.iid, chg_qty=qty_chg, ts=int(time.time()*1e3))

                self._snap_hedge_order_manager.update_order(order=order_obj)

                """ Clean up """
                if order_status in OrderStatus.fin_status():
                    self.update_order_stats(porder=order, torder=o)
                    if self._snap_hedge_order_manager.contains_trans_order(oid=oid):
                        self._snap_hedge_order_manager.pop_trans_order(oid=oid)
                        self._snap_hedge_order_manager.pop_order(oid=oid)

                """ Snap cancel """
                if self._snap_hedge_order_manager.contains_trans_order(oid=oid):
                    o = self._snap_hedge_order_manager.trans_order_at(oid=oid)
                    if not o.cancel:
                        self.loop.create_task(self.handle_cancel_order(oid=oid, order=o))

                return

            """ Got an order that has already finished, this is due to receiving msgs in the wrong order. Skip """
            logging.info(f"Received msg for a finished order {order}, skip.")
            
            
        except Exception as err:
            self.logger.critical(f"handle on_order err: {err}")
            traceback.print_exc()
    
    async def on_agg_trade(self, symbol, trade: PublicTrade):
        try:
            iid = symbol
            ex, contract = iid.split(".")
            t = Trade(
                ex=ex,
                contract=contract,
                price=float(trade.price),
                quantity=float(trade.quantity),
                tx_time=trade.transaction_ms,
                local_time=int(time.time()*1e3),
                side=1 if trade.side == OrderSide.Buy else -1,
                mexc_side=None,
                uid=-1
            )

            """ Latency state """
            self.executor.signal_calculator.local_latency_cal.add_trade(trade=t)
            latency = self.executor.signal_calculator.local_latency_cal.get_factor(ts=t.local_time)
            self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

            self.executor.update_trade(t)

            self.defensive_cancel_orders_on_trade(contract=contract, ex=ex, trd=t)
        except Exception as err:
            traceback.print_exc()
            
    async def internal_fail(self,client_id,acc,err=None):
        try:
            self.logger.critical(f"failed order: {err}, acc:{acc}, client_id={client_id}")
            if self._dynamic_mm_order_manager.contains_trans_order(client_id):
                o = self._dynamic_mm_order_manager.trans_order_at(client_id)
                order = PartialOrder(
                    account_name=o.account_name,
                    xchg_id=None,
                    exchange_pair="",
                    client_id=client_id,
                    xchg_status=OrderStatus.Failed,
                    filled_amount=0,
                    avg_filled_price=0,
                    commission_fee=0,
                    local_ms=int(time.time() * 1e3),
                    server_ms=int(time.time() * 1e3),
                    )
                await self.on_order(order)
            elif self._snap_hedge_order_manager.contains_trans_order(client_id):
                o = self._snap_hedge_order_manager.trans_order_at(client_id)
                order = PartialOrder(
                    account_name=o.account_name,
                    xchg_id=None,
                    exchange_pair="",
                    client_id=client_id,
                    xchg_status=OrderStatus.Failed,
                    filled_amount=0,
                    avg_filled_price=0,
                    commission_fee=0,
                    local_ms=int(time.time() * 1e3),
                    server_ms=int(time.time() * 1e3),
                    )
                await self.on_order(order)
        except:
            pass
            
    def defensive_cancel_orders_on_trade(self, contract: str, ex: str, trd: Trade):

        """ SEx or XEx? That is the question. """

        xex = "xex"

        mm_mdl = self.executor.mm_mdl
        sig_cal = self.executor.signal_calculator
        ts = trd.local_time

        if not mm_mdl.has_depth_metrics(contract=contract, ex=xex):
            return

        depth_freq, bbo_freq, trade_freq = sig_cal.local_update_freq_cal.get_factor(ts=ts)
        if depth_freq is None:
            return

        tau = depth_freq  # order update frequency
        pred_las, pred_sas = sig_cal.pred_trade_arrival_size(ts=ts)
        if (pred_las is None) or (pred_sas is None):
            return
        pred_lat, pred_sat = sig_cal.pred_trade_arrival_time(ts=ts)
        if (pred_lat is None) or (pred_sat is None):
            return
        pred_lat = max(1, pred_lat)
        pred_sat = max(1, pred_sat)
        curr_las, curr_sas = sig_cal.local_impulse_arrival_size_cal.get_factor(ts=ts)
        if (curr_las is None) or (curr_sas is None):
            return
        curr_lat, curr_sat = sig_cal.local_impulse_arrival_time_cal.get_factor(ts=ts)
        if (curr_lat is None) or (curr_sat is None):
            return
        curr_lat = max(1, curr_lat)
        curr_sat = max(1, curr_sat)
        avg_las, avg_sas = sig_cal.local_arrival_size_cal.get_factor(ts=ts)
        if (avg_las is None) or (avg_sas is None):
            return
        avg_lat, avg_sat = sig_cal.local_arrival_time_cal.get_factor(ts=ts)
        if (avg_lat is None) or (avg_sat is None):
            return
        tilde_p = mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")

        ask_prc_touch_line = tilde_p
        bid_prc_touch_line = tilde_p

        if trd.side == Direction.long:
            frequent_event = max(1/pred_lat, 1/curr_lat) > avg_lat * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            large_quantity = max(pred_las, curr_las) > avg_las * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            if frequent_event or large_quantity:
                lar = max(pred_las, curr_las, pred_las * tau / pred_lat, curr_las * tau / curr_lat)
                a_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_s")
                b_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_s")
                s_s = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_s")
                delta = (lar + self.executor.mm_mdl._epsilon - b_s) / max(1e-6, a_s)
                ask_prc_touch_line = max(tilde_p + s_s + delta, 0)
        elif trd.side == Direction.short:
            frequent_event = max(1/pred_sat, 1/curr_sat) > avg_sat * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            large_quantity = max(pred_sas, curr_sas) > avg_sas * self.cfg.TRADE_DEFENSE_CANCEL_AR_MULTIPLIER
            if frequent_event or large_quantity:
                sar = max(pred_sas, curr_sas, pred_sas * tau / pred_sat, curr_sas * tau / curr_sat)
                a_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="a_l")
                b_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="b_l")
                s_l = self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="s_l")
                delta = (sar + self.executor.mm_mdl._epsilon - b_l) / max(1e-6, a_l)
                bid_prc_touch_line = max(tilde_p - s_l - delta, 0)
        else:
            raise NotImplementedError()

        I = 0
        self.defensive_cancel_orders(ask_touch_line=ask_prc_touch_line, bid_touch_line=bid_prc_touch_line, I=I)
        self._n_def_cancel += 1
                
    def _update_latency(self, iid: str, latency: float, allow_rectivate: bool):
        ism = self.iid_state_manager
        is_in_high_latency_state = ism.is_in_high_latency_state(iid=iid)
        is_active = ism.is_active(iid=iid)
        if not is_in_high_latency_state:
            if ism.is_high_latency(iid=iid, latency=latency):
                ism.enter_high_latency_state(iid=iid)
                if is_active:
                    self.deactivate(iid=iid)
        else:  # is_in_high_latency_state
            if not ism.is_high_latency(iid=iid, latency=latency):
                if allow_rectivate:
                    ism.enter_normal_latency_state(iid=iid)
    
    async def on_bbo(self, symbol, bbo):
        try:
            await self.bbo_queue.put((symbol, bbo))
        except:
            traceback.print_exc()
            
    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            await self.ob_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")
            
    async def handle_ob_update(self):
        while True:
            try:
                if self.bbo_queue.empty() and self.ob_queue.empty():
                    await asyncio.sleep(0.001)
                    continue
                elif not self.ob_queue.empty():
                    symbol, orderbook = await self.ob_queue.get()
                    self.ob_queue = LifoQueue()
                    message_type = "ob"
                else:
                    symbol, bbo = await self.bbo_queue.get()
                    self.bbo_queue = LifoQueue()
                    message_type = "bbo"
                    
                if message_type == "ob":
                    s = time.time()*1e3
                    iid = symbol
                    ex, contract = symbol.split(".")
                    ccy = contract.split("_")
                    depth_obj:Depth = Depth.from_dict(
                        depth=orderbook,
                        ccy1=ccy[0],
                        ccy2=ccy[1],
                        ex=ex,
                        contract=contract
                        )
                    ex = depth_obj.meta_data.ex
                    contract = depth_obj.meta_data.contract
                    server_ts = depth_obj.server_ts
                    local_ts = depth_obj.resp_ts

                    """ Latency state """
                    self.executor.signal_calculator.local_latency_cal.add_depth(depth=depth_obj)
                    latency = self.executor.signal_calculator.local_latency_cal.get_factor(ts=depth_obj.resp_ts)
                    self._update_latency(iid=iid, latency=latency, allow_rectivate=True)

                    """ tx time guard """
                    if self.server_time_guard.has_last_server_time(iid=iid):
                        last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                        if server_ts < last_server_time:
                            continue
                    self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

                    ism = self.iid_state_manager
                    """ Activate stale state """
                    is_in_stale_state = ism.is_in_stale_state(iid=iid)
                    if is_in_stale_state:
                        ism.enter_nonstale_state(iid=iid)
                    ism.update_heart_beat_time(iid=iid, ts=int(time.time()*1e3))

                    """ Check stale """
                    for nsiids in ism.nonstale_iids:
                        is_active = ism.is_active(iid=nsiids)
                        if ism.is_stale(iid=nsiids, ts=int(time.time()*1e3)):
                            ism.enter_stale_state(iid=nsiids)
                            if is_active:
                                self.deactivate(iid=nsiids)

                    if not ism.active_iids:
                        continue

                    """ Record prev depth metrics """
                    xex = "xex"
                    ex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    xex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=xex)
                    if ex_has_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=ex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=ex))
                    if xex_has_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=xex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=xex))

                    """ Update depth metrics """
                    self.executor.update_depth(iid, depth_obj)
                    if ism.is_active(iid=iid):
                        xex_depth = self.executor._xex_depth_map[contract]
                        self._delta_neutral_executor.update_xex_depth(xex_depth=xex_depth)

                    """ Compute and record ref prcs and returns """
                    self._update_ref_prc(contract=contract, ex=ex,
                                        ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, metric="tilde_p"))
                    self._update_ref_prc(contract=contract, ex=xex,
                                        ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p"))
                    if ex_has_depth_metrics:
                        _prev_ex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=ex, metric_name="tilde_p")
                        ex_ref_prc = self._get_ref_prc(contract=contract, ex=ex)
                        self._update_ret(contract=contract, ex=ex,
                                        ret=math.log(ex_ref_prc / _prev_ex_ref_prc))
                    if xex_has_depth_metrics:
                        _prev_xex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=xex, metric_name="tilde_p")
                        xex_ref_prc = self._get_ref_prc(contract=contract, ex=xex)
                        self._update_ret(contract=contract, ex=xex,
                                        ret=math.log(xex_ref_prc / _prev_xex_ref_prc))
                    data = self.generate_orders(ref_prc=self._get_ref_prc(contract=contract, ex=xex))
                    # self.cancel_orders_action()
                    if time.time()*1e3 - s > 100:
                        self.logger.critical(f"orderbook handling time: {time.time()*1e3 - s}")
                    
                elif message_type == "bbo":
                    s = time.time()*1e3
                    iid = symbol
                    bbo_obj = BBO.from_dict_prod(iid=symbol, bbo_depth=bbo)

                    if not bbo_obj.valid_bbo:
                        continue

                    contract = bbo_obj.meta.contract
                    ex = bbo_obj.meta.ex
                    server_ts = bbo_obj.server_time
                    local_ts = bbo_obj.local_time

                    ism = self.iid_state_manager
                    ism.update_heart_beat_time(iid=iid, ts=local_ts)

                    """ Latency state """
                    self.executor.signal_calculator.local_latency_cal.add_bbo(bbo=bbo_obj)
                    latency = self.executor.signal_calculator.local_latency_cal.get_factor(ts=bbo_obj.local_time)
                    self._update_latency(iid=iid, latency=latency, allow_rectivate=False)

                    """ Server time guard """
                    if self.server_time_guard.has_last_server_time(iid=iid):
                        last_server_time = self.server_time_guard.get_last_server_time(iid=iid)
                        if server_ts < last_server_time:
                            continue
                    self.server_time_guard.update_server_time(iid=iid, ts=server_ts)

                    """ Check stale """
                    for nsiids in ism.nonstale_iids.copy():
                        is_active = ism.is_active(iid=nsiids)
                        if ism.is_stale(iid=nsiids, ts=int(time.time()*1e3)):
                            ism.enter_stale_state(iid=nsiids)
                            if is_active:
                                self.deactivate(iid=nsiids)

                    if not ism.is_active(iid=iid):
                        continue

                    if self._prev_bbo is not Ellipsis:
                        bid_prc_chged = abs(math.log(bbo_obj.best_bid_prc / self._prev_bbo.best_bid_prc)) > 1e-6
                        ask_prc_chged = abs(math.log(bbo_obj.best_ask_prc / self._prev_bbo.best_ask_prc)) > 1e-6
                        if not ask_prc_chged and not bid_prc_chged:
                            continue

                    if not self.executor._xex_depth_map[contract].contains(ex=ex):
                        continue

                    """ Record prev depth metrics """
                    xex = "xex"
                    ex_had_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    xex_had_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=xex)
                    if ex_had_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=ex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=ex))

                    if xex_had_depth_metrics:
                        self._update_prev_depth_metrics(contract=contract, ex=xex,
                                                        metrics_dict=self.executor.mm_mdl.get_depth_metrics(contract=contract, ex=xex))

                    """ Update depth metrics """
                    self.executor.update_bbo(bbo_obj)
                    if ism.is_active(iid=iid):
                        xex_depth = self.executor._xex_depth_map[contract]
                        self._delta_neutral_executor.update_xex_depth(xex_depth=xex_depth)

                    """ Compute and record ref prcs and returns """
                    ex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    if ex_has_depth_metrics:
                        self._update_ref_prc(contract=contract, ex=ex,
                                            ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=ex, 
                                                                                          metric="tilde_p"))
                    xex_has_depth_metrics = self.executor.mm_mdl.has_depth_metrics(contract=contract, ex=ex)
                    if xex_has_depth_metrics:
                        self._update_ref_prc(contract=contract, ex=xex,
                                            ref_prc=self.executor.mm_mdl.get_depth_metric(contract=contract, ex=xex,
                                                                                          metric="tilde_p"))
                    if ex_had_depth_metrics and ex_has_depth_metrics:
                        _prev_ex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=ex, metric_name="tilde_p")
                        self._update_ret(contract=contract, ex=ex,
                                        ret=math.log(self._get_ref_prc(contract=contract, ex=ex) / _prev_ex_ref_prc))
                    if xex_had_depth_metrics and xex_has_depth_metrics:
                        _prev_xex_ref_prc = self._get_prev_depth_metrics(contract=contract, ex=xex, metric_name="tilde_p")
                        self._update_ret(contract=contract, ex=xex,
                                        ret=math.log(self._get_ref_prc(contract=contract, ex=xex) / _prev_xex_ref_prc))

                    """ Full recalc of all optimal quotes is slow. """
                    # self.generate_orders(self._ref_prc)
                    # self.cancel_orders_action()

                    """ Defensive cancel is fast. """
                    ex, contract = iid.split(".")
                    self.defensive_cancel_orders_on_bbo(contract=contract, ex=ex, bbo=bbo_obj)
                    self._prev_bbo = bbo_obj
                    if time.time()*1e3 - s > 100:
                        self.logger.critical(f"bbo handling time: {time.time()*1e3 - s}")
            except Exception as err:
                traceback.print_exc()
        
                
    def defensive_cancel_orders_on_bbo(self, contract: str, ex: str, bbo: BBO):

        """ SEx or XEx? That is the question. """

        xex = "xex"

        exec = self.executor
        mm_mdl = exec.mm_mdl
        sig_cal = exec.signal_calculator
        ts = bbo.local_time

        if not mm_mdl.has_depth_metrics(contract=contract, ex=ex):
            return

        tau = exec.get_tau(ts=ts)
        if tau is None:
            return

        tilde_p = mm_mdl.get_depth_metric(contract=contract, ex=xex, metric="tilde_p")

        pred_ret = sig_cal.predict_ref_prc_ret(ts=ts)
        if pred_ret is None:
            pred_ret = 0
        pred_long_half_sprd = sig_cal.predict_long_half_spread(ts=ts)
        pred_short_half_sprd = sig_cal.predict_short_half_spread(ts=ts)
        pred_ref_prc = tilde_p * (1 + pred_ret * tau)
        pred_bid_touch_line = pred_ref_prc - pred_long_half_sprd
        pred_ask_touch_line = pred_ref_prc + pred_short_half_sprd

        curr_ret = sig_cal.curr_impulse_ref_prc_ret(ts=ts)
        if curr_ret is None:
            curr_ret = 0
        curr_long_half_sprd = sig_cal.curr_impulse_long_half_spread(ts=ts)
        curr_short_half_sprd = sig_cal.curr_impulse_short_half_spread(ts=ts)
        curr_ref_prc = tilde_p * (1 + curr_ret * tau)
        curr_bid_touch_line = curr_ref_prc - curr_long_half_sprd
        curr_ask_touch_line = curr_ref_prc + curr_short_half_sprd

        abs_ret = max(abs(curr_ret), abs(pred_ret))
        half_sprd = max(pred_long_half_sprd, pred_short_half_sprd, curr_long_half_sprd, curr_short_half_sprd)
        bid_touch_line = min(pred_bid_touch_line, curr_bid_touch_line)
        ask_touch_line = max(pred_ask_touch_line, curr_ask_touch_line)

        avg_sigma = sig_cal.curr_ema_sigma(ts=ts)
        large_abs_ret_bnd = avg_sigma * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_RETURN_MULTIPLIER
        lhs = sig_cal.curr_impulse_long_half_spread(ts=ts)
        shs = sig_cal.curr_emavg_short_half_spread(ts=ts)
        avg_half_sprd = (lhs + shs) / 2
        large_hs_bnd = avg_half_sprd * self.cfg.DEPTH_BBO_DEFENSE_CANCEL_HALF_SPRD_MULTIPLIER

        large_ret = abs_ret > large_abs_ret_bnd
        large_sprd = half_sprd > large_hs_bnd

        if large_sprd or large_ret:
            I = 0
            self.defensive_cancel_orders(ask_touch_line=ask_touch_line, bid_touch_line=bid_touch_line, I=I)
            self._n_def_cancel += 1
    
    def _update_prev_depth_metrics(self, contract: str, ex: str, metrics_dict: Dict):
        self._prev_depth_metrics[(contract, ex)] = metrics_dict

    def _get_prev_depth_metrics(self, contract: str, ex: str, metric_name: str) -> float:
        return self._prev_depth_metrics[(contract, ex)][metric_name]

    def _remove_prev_depth_metrics(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._prev_depth_metrics:
            self._prev_depth_metrics.pop((contract, ex))

    def _update_ref_prc(self, contract: str, ex: str, ref_prc: float):
        self._ref_prc_map[(contract, ex)] = ref_prc

    def _get_ref_prc(self, contract: str, ex: str) -> float:
        return self._ref_prc_map[(contract, ex)]

    def _remove_ref_prc(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._ref_prc_map:
            self._ref_prc_map.pop(key)

    def _update_ret(self, contract: str, ex: str, ret: float):
        self._ret_map[(contract, ex)] = ret

    def _get_ret(self, contract: str, ex: str) -> float:
        return self._ret_map[(contract, ex)]

    def _remove_ret(self, contract: str, ex: str):
        key = (contract, ex)
        if key in self._ret_map:
            self._ret_map.pop(key)

    def _has_ret(self, contract: str, ex: str) -> bool:
        return (contract, ex) in self._ret_map

    def deactivate(self, iid: str):
        ex, contract = iid.split(".")
        self.executor.remove_depth(iid=iid)
        self.cancel_all_orders_for_iid(iid=iid)
        self._remove_prev_depth_metrics(contract=contract, ex=ex)
        # self._remove_ref_prc(contract=contract, ex=ex)
        self._remove_ret(contract=contract, ex=ex)
    
    def generate_orders(self, ref_prc):
        orders, data = self.executor.get_orders(
            contract=self.cfg.CONTRACT,
            ref_prc=ref_prc,
            t=int(time.time()*1e3),
        )
        for order in orders:
            self.loop.create_task(self.send_dynamic_mm_order(order))
        return data
            
    def cancel_orders_action(self):
        for oid in self._dynamic_mm_order_manager.keys():
            if self._dynamic_mm_order_manager.contains_trans_order(oid):
                o = self._dynamic_mm_order_manager.trans_order_at(oid=oid)
                if o.sent_ts < time.time()*1e3 - o.life:
                    if not o.cancel:
                        self.loop.create_task(self.handle_cancel_order(oid=oid, order=o))
                    
    def defensive_cancel_orders(self, ask_touch_line: float, bid_touch_line: float, I: float):
        if I >= 0:  # long
            for int_prc in list(self._dynamic_mm_order_manager._bid_ladder.keys()):
                if self._dynamic_mm_order_manager._bid_ladder.get(int_prc):
                    oid_set = self._dynamic_mm_order_manager._bid_ladder[int_prc]
                    prc =  int_prc / self._dynamic_mm_order_manager.price_multilier
                    if prc > bid_touch_line:
                        for oid in oid_set:
                            order = self._dynamic_mm_order_manager.trans_order_at(oid)
                            if not order.cancel:
                                self.loop.create_task(self.handle_cancel_order(oid=oid, order=order))
                        continue
                    break
        if I <= 0:  # short
            for int_prc in list(self._dynamic_mm_order_manager._bid_ladder.keys()):
                if self._dynamic_mm_order_manager._ask_ladder.get(int_prc):
                    oid_set = self._dynamic_mm_order_manager._ask_ladder[int_prc]
                    prc = int_prc / self._dynamic_mm_order_manager.price_multilier
                    if prc < ask_touch_line:
                        for oid in oid_set:
                            order = self._dynamic_mm_order_manager.trans_order_at(oid)
                            if not order.cancel:
                                self.loop.create_task(self.handle_cancel_order(oid=oid, order=order))
                        continue
                    break

    def _cancel_orders_for_iid(self, iid: str, order_manager: OrderManager):
        for oid in list(order_manager.get_oids_for_iid(iid=iid)):
            o = order_manager.trans_order_at(oid=oid)
            if not o.cancel:
                self.loop.create_task(self.handle_cancel_order(oid=oid, order=o))
    
    def cancel_dynamic_mm_orders_for_iid(self, iid: str):
        self._cancel_orders_for_iid(iid=iid, order_manager=self._dynamic_mm_order_manager)

    def cancel_snap_hedge_orders_for_iid(self, iid: str):
        self._cancel_orders_for_iid(iid=iid, order_manager=self._snap_hedge_order_manager)

    def cancel_all_orders_for_iid(self, iid: str):
        self.cancel_dynamic_mm_orders_for_iid(iid=iid)
        self.cancel_snap_hedge_orders_for_iid(iid=iid)
        
    def get_volume_locked(self, side: Direction, acc: str,order_manager_list: List[OrderManager]):
        vol = 0
        for order_manager in list(order_manager_list):
            for _, o in list(order_manager.trans_order_items()):
                if o.side == side and o.account_name == acc:
                    vol += o.quantity
        return vol
    
    async def send_dynamic_mm_order(self, order: TransOrder):
        if not self.send:
            return
        block = False
        for oid in self._dynamic_mm_order_manager.keys():
            if self._dynamic_mm_order_manager.contains_trans_order(oid=oid):
                o = self._dynamic_mm_order_manager.get_trans_order(oid=oid)
                if o.side == order.side and o.iid == order.iid and o.floor == order.floor:
                    if abs(o.price - order.price) > 10 ** (-self.precision[order.iid]) * self.cfg.TOLERANCE_ARR[order.floor]:
                        if not o.cancel:
                            await self.handle_cancel_order(oid=oid, order=o)
                    else:
                        o.sent_ts = int(time.time()*1e3)
                        block = True
                        continue
        if block:
            return
        await self.base_send_order(order=order, order_manager=self._dynamic_mm_order_manager)
    
    async def send_snap_hedge_order(self, order: TransOrder):
        await self.base_send_order(order=order)
        
                
    async def base_send_order(self, order: TransOrder, order_manager: OrderManager):
                
        iid = order.iid
        ex, _ = iid.split(".")
        
        order.symbol_id = self.get_symbol_config(f"{order.iid}.{self.market}.{self.sub_market}")["id"]
        
        pp_iid = iid
        if self._contract_mapper.is_proxy_iid(iid=iid):
            pp_iid = self._contract_mapper.proxy_spot_iid_to_spot_iid(proxy_spot_iid=iid)
            
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[pp_iid])
        else:
            order.price = round_decimals_up(order.price, price_precision[pp_iid])
            
        qp_iid = iid
        if self._contract_mapper.is_proxy_iid(iid=iid):
            qp_iid = self._contract_mapper.proxy_spot_iid_to_spot_iid(proxy_spot_iid=iid)
        
        order.quantity = round(order.quantity, qty_precision[qp_iid])
        
        order_type = "PostOnly"
        # if order.maker_taker == MakerTaker.maker:
        #     order_type = "PostOnly"
        # if order.maker_taker == MakerTaker.taker:
        #     order_type = "IOC"
        
        if order.quantity == 0:
            return
        symbol = f"{iid}.{self.market}.{self.sub_market}"
        symbol_cfg = self.get_symbol_config(symbol)
        
        total_volume = order.quantity
        trader = self.load_balancer.get_trader()
        for acc in list(self.load_balancer.get_acc_list()):
            volume_allowed = self.load_balancer.volume_allowed(acc=acc, side=order.side, prc=order.price)
            volume_locked = self.get_volume_locked(
                side=order.side, acc=acc, 
                order_manager_list=[
                    self._dynamic_mm_order_manager, 
                    self._snap_hedge_order_manager
                    ]
                )
            volume_allowed -= volume_locked
            if volume_allowed > total_volume:
                if not self.volume_notional_check(symbol=symbol, price=order.price, qty=order.quantity):
                    continue
                cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
                new_order = TransOrder(
                    p=round(float(order.price), price_precision[iid]),
                    q=round(float(order.quantity), qty_precision[iid]),
                    iid=order.iid,
                    floor=order.floor,
                    maker_taker=order.maker_taker,
                    side=order.side
                )
                new_order.sent_ts = int(time.time()*1e3)
                new_order.account_name = acc
                new_order.symbol_id = order.symbol_id
                new_order.client_id = cid
                
                order_manager.add_trans_order(order=new_order, oid=cid)
                
                price = Decimal(new_order.price) + symbol_cfg["price_tick_size"]/Decimal("2")
                volume = Decimal(new_order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2")
                
                price, volume = self.format_price_volume(symbol_name=symbol, price=price, volume=volume)
                try:
                    await self.publish_to_channel(
                            "mq:hf_signal:order_request",
                            {
                                "strategy_name":trader,
                                "symbol":symbol,
                                "price":str(price),
                                "qty":str(volume),
                                "side":1 if new_order.side == Direction.long else -1,
                                "client_id": cid,
                                "acc": acc,
                                "order_type":order_type,
                                "main_sn":self.strategy_name,
                            }
                        )
                    break
                except Exception as err:
                    self.logger.critical(f"{err}")
                    await self.internal_fail(cid,err)
                    
            else:
                if not self.volume_notional_check(symbol=symbol, price=order.price, qty=volume_allowed):
                    continue
                cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
                new_order = TransOrder(
                    p=round(float(order.price), price_precision[iid]),
                    q=round(float(volume_allowed), qty_precision[iid]),
                    iid=order.iid,
                    floor=order.floor,
                    maker_taker=order.maker_taker,
                    side=order.side
                )
                new_order.sent_ts = int(time.time()*1e3)
                new_order.account_name = acc
                new_order.symbol_id = order.symbol_id
                new_order.client_id = cid
                
                order_manager.add_trans_order(order=new_order, oid=cid)
                
                price = Decimal(new_order.price) + symbol_cfg["price_tick_size"]/Decimal("2")
                volume = Decimal(new_order.quantity) + symbol_cfg["qty_tick_size"]/Decimal("2")
                
                price, volume = self.format_price_volume(symbol_name=symbol, price=price, volume=volume)
                try:
                    await self.publish_to_channel(
                            "mq:hf_signal:order_request",
                            {
                                "strategy_name":trader,
                                "symbol":symbol,
                                "price":str(price),
                                "qty":str(volume),
                                "side":1 if new_order.side == Direction.long else -1,
                                "client_id": cid,
                                "acc": acc,
                                "order_type":order_type,
                                "main_sn":self.strategy_name,
                            }
                        )
                    total_volume -= new_order.quantity
                except Exception as err:
                    self.logger.critical(f"{err}")
                    await self.internal_fail(cid,err)
                
        
    async def process_order_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["main_sn"] != self.strategy_name:
            return
        if payload["err"] != None:
            await self.internal_fail(
                client_id=payload["client_id"],
                err=payload["err"],
                acc=payload["account_name"]
                )
            return
        self.load_balancer.update_limit(
            acc=payload["account_name"],
            trader_name=payload["startegy_name"],
            acc_load=payload["uid10s"],
            ip_load=payload["ip1m"]
            )
        
    
    async def handle_cancel_order(self, oid, order: TransOrder):
        try:
            if oid not in self.canceling_id:
                self.canceling_id.add(oid)
                trader = self.load_balancer.get_trader()
                order.cancel = True
                await self.publish_to_channel(
                        "mq:hf_signal:cancel_request",
                        {
                            "strategy_name":trader,
                            "symbol_id": order.symbol_id,
                            "account_name": order.account_name,
                            "client_id": order.client_id,
                            "main_sn":self.strategy_name,
                        }
                    )
                
        except Exception as err:
            self.logger.critical(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)
        
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["main_sn"] != self.strategy_name:
            return
        if self._dynamic_mm_order_manager.contains_trans_order(payload["client_id"]) and payload["res"] == False:
            self._dynamic_mm_order_manager.trans_order_at([payload["client_id"]]).cancel = False
        if self._snap_hedge_order_manager.contains_trans_order(payload["client_id"]) and payload["res"] == False:
            self._snap_hedge_order_manager.trans_order_at([payload["client_id"]]).cancel = False 
        self.canceling_id.remove(payload["client_id"])
        
    async def rolling_cancel_order(self):
        while True:
            await asyncio.sleep(0.01)
            self.cancel_orders_action()
                
    async def rebalance(self):
        # rebalance, use when initializing or emergency exiting.
        try:
            ref_prc = self._get_ref_prc(contract=self.cfg.CONTRACT, ex="xex")
            ccy = self.cfg.CONTRACT.split("_")
            ccy2 = ccy[1]
            self.logger.info(f"current reference price={ref_prc}")
            for acc in self.account_names:
                self.logger.info(f"handle account rebalance: {acc}")
                ex, _ = acc.split(".")
                api = self.get_exchange_api_by_account(acc)
                iid = f"{ex}.{self.cfg.CONTRACT}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    await api.flash_cancel_orders(symbol_cfg)
                except:
                    self.logger.info("no open orders")
                
                for _ in range(3):
                    position_data = (await api.contract_position(symbol_cfg)).data
                    pos = position_data["long_qty"] - position_data["short_qty"]
                    self.logger.info(f"position={pos}")
                    if pos > 0:
                        try:
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(ref_prc*0.99), 
                                volume=pos,
                                side=OrderSide.Sell, 
                                position_side=OrderPositionSide.Close, 
                                order_type=OrderType.IOC,
                                account_name=acc,
                                recvWindow=3000)
                        except:
                            self.logger.warning(f"clean position err")
                            traceback.print_exc()
                    elif pos < 0:
                        try:
                            await self.make_order(
                                symbol_name=symbol,
                                price=Decimal(ref_prc*1.01), 
                                volume=-pos, 
                                side=OrderSide.Buy, 
                                position_side=OrderPositionSide.Close, 
                                order_type=OrderType.IOC,
                                account_name=acc,
                                recvWindow=3000)
                        except:
                            self.logger.warning(f"clean position err")
                            traceback.print_exc()
                    await asyncio.sleep(0.5)
                
                await asyncio.sleep(0.5)
                
                cur_balance = (await api.account_balance(symbol_cfg)).data
                self.load_balancer.update_balance(acc=acc, balance=float(cur_balance[ccy2]["all"]))
                
                position_data = (await api.contract_position(symbol_cfg)).data
                pos = position_data["long_qty"] - position_data["short_qty"]
                self.logger.info(f"""pos={pos} for {acc}""")

                self.logger.info(f"""{ccy2}={float(cur_balance[ccy2]["all"])} for {acc}""")
        except Exception as err:
            traceback.print_exc()

    async def get_order_status_direct(self, oid, order: TransOrder):
        
        if time.time()*1e3 - order.sent_ts > 5000 + order.query * 5000:
            order.query += 1
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=oid)
                new_order:AtomOrder = res.data
                self.logger.critical(f"direct check order:{oid}, status={new_order.xchg_status}")
                if new_order.xchg_status in OrderStatus.fin_status():
                    self.label_oid = oid
                    await self.on_order(new_order)
                else:
                    await self.handle_cancel_order(oid=oid, order=order)
                
            except Exception as err:
                self.logger.critical(f"direct check order error: {err}")
                
            if order.query > 10:
                self.send = False
                await asyncio.sleep(1)
                await self.rebalance()
                await asyncio.sleep(1)
                self.logger.critical(f"check order too many times and reset")
                self._dynamic_mm_order_manager.clear()
                self._snap_hedge_order_manager.clear()
                
                self.send = True
        
    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.check_dynamic_orders(),
                self.check_snap_orders()
            )

            
    async def check_dynamic_orders(self):
        for oid in self._dynamic_mm_order_manager.keys():
            if self._dynamic_mm_order_manager.contains_trans_order(oid):
                o = self._dynamic_mm_order_manager.trans_order_at(oid)
                await self.get_order_status_direct(oid=oid, order=o)

            
            
    async def check_snap_orders(self):
        for oid in self._snap_hedge_order_manager.keys():
            if self._snap_hedge_order_manager.contains_trans_order(oid):
                o = self._snap_hedge_order_manager.trans_order_at(oid)
                await self.get_order_status_direct(oid=oid, order=o)
                 
    
    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    await self.rebalance()
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def get_local_stats(self):
        while True:
            await asyncio.sleep(5)
            try:
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="perp",
                        tag={"sn":f"xex_dynamic_mm_binance_{self.cfg.CONTRACT}"}, 
                        fields={
                            "inventory":float(self.get_inventory(f"{self.cfg.DYNAMIC_MM_EXCHANGES[0]}.{self.cfg.CONTRACT}")) if f"{self.cfg.DYNAMIC_MM_EXCHANGES[0]}.{self.cfg.CONTRACT}" in self._inventory_map else float(0),
                            "ref_prc":float(self._get_ref_prc(contract=self.cfg.CONTRACT, ex=self.cfg.DYNAMIC_MM_EXCHANGES[0])),
                            }
                        )
                    )
                ccy = self.cfg.CONTRACT.split("_")
                ccy2 = ccy[1]
                tot_balance = 0
                for acc in self.account_names:
                    ex, _ = acc.split(".")
                    api = self.get_exchange_api_by_account(acc)
                    iid = f"{ex}.{self.cfg.CONTRACT}"
                    symbol = f"{iid}.{self.market}.{self.sub_market}"
                    symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                    cur_balance = (await api.account_balance(symbol_cfg)).data
                    balance=float(cur_balance[ccy2]["all"])
                    self.load_balancer.update_balance(acc=acc, balance=balance)
                    await asyncio.sleep(0.2)
                    tot_balance += balance
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="perp", 
                            tag={"sn":f"xex_dynamic_mm_binance1_{self.cfg.CONTRACT}"}, 
                            fields={
                                f"{acc}_load": float(self.load_balancer._account_load_map[acc])
                            }
                        )
                    )
                
                for trader in list(self.load_balancer._ip_load_map.keys()):
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="perp", 
                            tag={"sn":f"xex_dynamic_mm_binance1_{self.cfg.CONTRACT}"}, 
                            fields={
                                f"{trader}_load": float(self.load_balancer._ip_load_map[trader])
                            }
                        )
                    )
                
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="perp", 
                        tag={"sn":f"xex_dynamic_mm_binance1_{self.cfg.CONTRACT}"}, 
                        fields={
                            "balance": float(tot_balance)
                            }
                        )
                    )
                
                for _, order in list(self._dynamic_mm_order_manager.trans_order_items()):
                    if order.side == Direction.short:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="perp", 
                                tag={"sn":f"xex_dynamic_mm_binance1_{self.cfg.CONTRACT}"}, 
                                fields={
                                    "cache_sell_price":float(order.price)
                                    }
                                )
                            )
                    else:
                        self.loop.create_task(
                            self.push_influx_data(
                                measurement="perp", 
                                tag={"sn":f"xex_dynamic_mm_binance1_{self.cfg.CONTRACT}"}, 
                                fields={
                                    "cache_buy_price":float(order.price)
                                    }
                                )
                            )
                self.loop.create_task(
                    self.push_influx_data(
                        measurement="perp", 
                        tag={"sn":f"xex_dynamic_mm_binance1_{self.cfg.CONTRACT}"}, 
                        fields={
                            "trade_amount_per_second": float(float(self.trd_amt)/(time.time() - self.start_ts)),
                            }
                        )
                    )
                
                

            except Exception as err:
                self.logger.critical(f"sending info to grafana err: {err}")
                traceback.print_exc()
                
    async def debug(self):
        while True:
            await asyncio.sleep(10)
            for oid, o in list(self._dynamic_mm_order_manager.trans_order_items()):
                if o.cancel is True and time.time()*1e3 - o.sent_ts> 1000:
                    self.logger.warning(f"expired order: p={o.price}, q={o.quantity}, side={o.side}, acc={o.account_name}, id={oid}, time={time.time()*1e3 - o.sent_ts}")
            self.logger.warning(".................")
            
            tot_pos = 0
            for acc in self.account_names:
                self.logger.critical(f"{acc} load={self.load_balancer._account_load_map[acc]}")
                self.logger.critical(f"{acc} pos={self.load_balancer._position_map[acc]}")
                tot_pos += self.load_balancer._position_map[acc]
            self.logger.critical(f"total pos={tot_pos}")
            
            for trader in list(self.load_balancer._ip_load_map.keys()):
                self.logger.critical(f"{trader} load={self.load_balancer._ip_load_map[trader]}")
    
    async def reduce_count(self):
        while True:
            await asyncio.sleep(1)
            self.load_balancer.reduce_count(1)
        
    async def strategy_core(self):
        
        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.get_local_stats(),
                self.reduce_count(),
                self.rolling_cancel_order(),
                # self.debug()
            )
        
            
if __name__ == '__main__':
    logging.getLogger().setLevel("CRITICAL")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()