# import os
# os.environ["ATOM_PRINT_RESPONSE"] = '1'
# from atom.exchange_api.lbank.lbank_http import LbankRestApi
# LbankRestApi.colo_urls["swap"] = "http://172.31.4.70:12001"

import pandas as pd
from strategy_base.utils import sync_git_repo

sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "preprod")
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 20
from atom.helpers import send_ding_talk
from atom.helpers import decrypt_credential
from atom.exchange_api.factory import build_rest_api
from collections import defaultdict, deque
from xex_mm.controller_managers.server_time_guard import ServerTimeGuard
from xex_mm.controller_managers.iid_state_manager import IIdStateManager
from xex_mm.strategy_configs.obligatory_mm_config import ObligatoryMmConfig
from xex_mm.xex_mm_delta_neutral.xex_mm_delta_neutral_inventory_executor import XexMmDeltaNeutralInventoryExecutor
from xex_mm.utils.configs import price_precision, qty_precision, FeeConfig, HeartBeatTolerance, QuantityPrecisionManager, PricePrecisionManager
from xex_mm.obiligatory_mm.obligatory_executor import ObligatoryExecutor
from xex_mm.xex_depth.xex_depth import XExDepth
from xex_mm.strategy_configs.dmm_config import Config
from xex_mm.utils.enums import Side

from xex_mm.dmm_strategy.cancel_module import VolNTickCalculator

from xex_mm.dmm_strategy.executor import DMMExecutor, DMMGuardExecutor
from xex_mm.order_managers.zone_order_manager import ZoneOrderManager

from strategy_base.base import CommonStrategy
from atom.model import OrderSide, PartialOrder, OrderStatus, BBODepth, OrderPositionSide, OrderType, PublicTrade
from atom.model.depth import Depth as AtomDepth
from atom.model.order import Order as AtomOrder
from atom.exceptions import OrderNotFoundError

from atom.helpers import ClientIDGenerator
from asyncio.queues import Queue, LifoQueue
import uuid
from orderedset import OrderedSet
import ujson as json
from typing import Set, AnyStr, Union, Dict, List, Tuple
import math
import traceback
import numpy as np

import time
from decimal import Decimal
import asyncio
import logging


from xex_mm.utils.base import Depth, BBO, Direction, Order, TransOrder, MakerTaker


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.ob_update = False
        self.start_ts = time.time()
        self.oid_to_cid = dict()
        self.mm_cache = defaultdict(lambda: set())
        self.mm_id = 0
        
        self.contract = self.config['strategy']['params']['contract']
        self.market = self.config['strategy']['params']['market']
        self.sub_market = self.config['strategy']['params']['sub_market']
        self.bnd_amt_in_coin = Decimal(
            self.config['strategy']['params']['bnd_amt_in_coin'])
        self.max_open_amount = float(self.config['strategy']['params']['max_open_amount'])
        self.unknown_order_dict = dict()
        self.counter = 0
        self.filled_counter = 0

        self.cfg = Config(
            CONTRACT=self.contract,
            EX="binance",
            MM_EX="lbank",
            TARGET_DEPTH_AMOUNT=self.max_open_amount,
            SIDE_MIN_RATIO=0,
            FRAGMENTATION_RATIO=2,
            FOLDER_NAME=f".",
            MSG=".",
        )

        self._numeric_tol = 1e-10

        self._inventory: float = 0
        self._obl_inventory: float = 0
        self._hedge_inventory: float = 0


        self.canceling_id = set()
        self.quering_id = set()

        self.send = False

        self.trd_amt = 0

        self.taker_book_queue = LifoQueue()
        self.maker_book_queue = LifoQueue()
        self.bbo_queue = LifoQueue()
        self.order_queue = Queue()

        self.cancel_elapsed_q = deque(maxlen=1000)
        self.send_elapsed_q = deque(maxlen=1000)
        
        self.send_order_on_msg = dict()
        self.cancel_order_on_msg = dict()
        self.send_order_on_msg_q = deque(maxlen=1000)
        self.cancel_order_on_msg_q = deque(maxlen=1000)
        
        self.timer1 = deque(maxlen=10000)
        self.timer2 = deque(maxlen=10000)

        self._pt_cache: Dict[str, float] = dict()
        self._qt_cache: Dict[str, float] = dict()

        self._min_confirmed_open_zone: Dict[int, float] = {Side.long: -1, Side.short: -1}
        self._min_confirmed_open_zone_initialized: Dict[int, bool] = {Side.long: False, Side.short: False}
        self._guard_min_confirmed_open_zone: Dict[int, float] = {Side.long: -1, Side.short: -1}
        self._guard_min_confirmed_open_zone_initialized: Dict[int, bool] = {Side.long: False, Side.short: False}
        
        self.tgt_depth_amt = self.cfg.TARGET_DEPTH_AMOUNT
        
        self.recvWindow = 1000

        self.not_found_trans_order = deque()
        self.err_not_found_order = deque(maxlen=100)
        
        self.mp = 0
        
        self.flow_data_ts = deque()
        self.flow_count = 0
        
        self.send_base_on_flow = False
        
        self.ob_counter = 0
        
        self.send_count = 0
        self.cancel_count = 0
        self.query_count = 0
        self.bp_count = 0
        self.send_delay = []
        
        self.mm_pending_open_to_confirmed_open_long_q = deque(maxlen=1000)
        self.mm_pending_open_to_confirmed_open_short_q = deque(maxlen=1000)
        self.guard_pending_open_to_confirmed_open_long_q = deque(maxlen=1000)
        self.guard_pending_open_to_confirmed_open_short_q = deque(maxlen=1000)

    def _build_direct_sub_params_(self, symbol):
        if symbol["exchange_name"] == "binance":
            use_colo_url = False
        else:
            use_colo_url = self.use_colo_url
        return dict(
            exchange_name=symbol["exchange_name"],
            account=None,
            market=symbol["market"],
            private_mode=False,
            use_colo_url=use_colo_url,
            use_raw_stream=self.use_raw_stream
        )
    
    async def _build_rest_api_(self):
        apis_account = dict()
        apis_exchange = defaultdict(list)
        for idx, item in enumerate(self.config['exchanges']):
            account_name = item["account"]["account_name"]
            item['account'].update(json.loads(decrypt_credential(item['account'].pop('epass'))))
            if item['exchange_name'] == "binance":
                use_colo_url = False
            else:
                use_colo_url = self.use_colo_url
            _api = await build_rest_api(
                item['exchange_name'], item['account'], async_init=True, **item['params'], **self.config['system']['logger'],
                use_colo_url=use_colo_url
            )
            apis_account[account_name] = _api
            apis_exchange[item['exchange_name']].append(_api)
            setattr(self, f"exchange_{idx + 1}", _api)
            self.cache_enable_switch[account_name] = item['params'].get('cache_enabled', True)
            account_cfg_margin_mode = (await self.cache_redis.handler.hgetall(f"cache:account_cfg:{account_name}:margin_mode")) or dict()
            account_cfg_pos_mode = (await self.cache_redis.handler.hgetall(f"cache:account_cfg:{account_name}:pos_mode")) or dict()
            account_cfg_leverage = (await self.cache_redis.handler.hgetall(f"cache:account_cfg:{account_name}:leverage")) or dict()
            for k in ("cfg_margin_mode", "cfg_pos_mode", "cfg_leverage"):
                item[k] = locals().get(f"account_{k}", dict())
            self._account_config_[account_name] = item
        return apis_account, apis_exchange
    
    def update_config_before_init(self):
        self.use_colo_url = True
        # self.config['system']['logger']["max_size"] = 1024 * 1024 * 1   # 每个文件最大1M
        # self.config['system']['logger']["backup_count"] = 1 # 1个backup，总计两个文件

    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(
                map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))

    def update_inventory(self, chg_qty: float, key: str, prc: float):
        self.logger.info(f"curr inventory:{self._inventory}, update inventory: {chg_qty}, from:{key}")
        self._inventory += chg_qty
        if key == "obl":
            self._obl_inventory += chg_qty
        elif key == "hedge":
            self._hedge_inventory += chg_qty

        self.trd_amt += abs(chg_qty) * prc

    def direction_map(self, side: Direction):
        if side == Direction.long:
            return OrderSide.Buy
        else:
            return OrderSide.Sell

    
    async def get_max_order_volume(self, exchange_pair):
        api = self.get_exchange_api(exchange_name="lbank")[0]
        response = await api.make_request("swap", "GET", "/cfd/openApi/v1/pub/instrument", query={"productGroup":"SwapU"}, need_sign=False)
        config_list = response.json["data"]
        for config in config_list:
            if config["symbol"] == exchange_pair:
                return Decimal(config["maxOrderVolume"])

    async def before_strategy_start(self):
        self.ex_pair_set = set()
        self.logger.setLevel("CRITICAL")
        maker_symbol = f"{self.cfg.MM_EX}.{self.contract}.{self.market}.{self.sub_market}"
        if self.cfg.MM_EX.startswith("lbank") and self.market == "usdt_contract":
            ccy1, _, _ = self.contract.split("_")
            maker_symbol = f"{self.cfg.MM_EX}.{ccy1}_usdt_swap.swap.na"
        maker_config = self.get_symbol_config(symbol_identity=maker_symbol)
        self.maker_qty_tick = maker_config["qty_tick_size"]
        self.maker_contract_value = maker_config["contract_value"]
        bnd = self.bnd_amt_in_coin / maker_config["contract_value"]
        self.ex_pair_set.add(maker_config["exchange_pair"])
        self.max_order_volume = await self.get_max_order_volume(exchange_pair=maker_config["exchange_pair"])
        
        self.odr_mngr: ZoneOrderManager = ZoneOrderManager(contract=self.contract, ex=self.cfg.MM_EX)
        self.guard_odr_mngr: ZoneOrderManager = ZoneOrderManager(contract=self.contract, ex=self.cfg.MM_EX)

        self.executor: DMMExecutor = DMMExecutor(contract=self.contract, mm_ex=self.cfg.MM_EX,
                                                 max_tgt_depth_amt=self.cfg.TARGET_DEPTH_AMOUNT / 3,
                                                 side_min_ratio=self.cfg.SIDE_MIN_RATIO,
                                                 fragmentation_ratio=self.cfg.FRAGMENTATION_RATIO,
                                                 max_order_volume=float(self.max_order_volume),
                                                 logger=self.logger)
        self.executor.link_order_manager(order_manager=self.odr_mngr)
        self.executor.link_guard_order_manager(guard_order_manager=self.guard_odr_mngr)
        self.guard_executor: DMMGuardExecutor = DMMGuardExecutor(contract=self.contract, mm_ex=self.cfg.MM_EX,
                                                 side_min_ratio=0.8,
                                                 fragmentation_ratio=self.cfg.FRAGMENTATION_RATIO,
                                                 max_order_volume=float(self.max_order_volume),
                                                 halflife=100,  # n events
                                                 logger=self.logger)
        self.guard_executor.on_tgt_depth_amt(tgt_depth_amt=self.cfg.TARGET_DEPTH_AMOUNT * 2 / 3)
        self.guard_executor.link_order_manager(order_manager=self.guard_odr_mngr)
        self.guard_executor.link_mm_order_manager(mm_order_manager=self.odr_mngr)

        self.trd_vol_n_tick_calc = VolNTickCalculator(T=10000, n=100, tick=self._get_pt(ex=self.cfg.MM_EX), rho=1)
        
        self.direct_subscribe_order_update(symbol_name=maker_symbol)
        # self.direct_subscribe_orderbook(
        #     symbol_name=maker_symbol, is_incr_depth=True, depth_min_v=bnd, is_testnet=False)

        self.taker_config = dict()
        for ex in [self.cfg.EX]:
            symbol = f"{ex}.{self.contract}.{self.market}.{self.sub_market}"
            if ex.startswith("lbank") and self.market == "usdt_contract":
                ccy1, _, _ = self.contract.split("_")
                symbol = f"{ex}.{ccy1}_usdt_swap.swap.na"
            taker_config = self.get_symbol_config(symbol_identity=symbol)
            self.ex_pair_set.add(taker_config["exchange_pair"])
            self.taker_config[ex] = taker_config
            bnd = self.bnd_amt_in_coin / taker_config["contract_value"]
            self.direct_subscribe_orderbook(
                symbol_name=symbol, is_incr_depth=True, depth_min_v=bnd)
            self.direct_subscribe_bbo(symbol_name=symbol)
            self.direct_subscribe_agg_trade(symbol_name=symbol)

        self.loop.create_task(self.handle_ws_update())

        await self.rebalance()

        ccy = self.contract.split("_")
        ccy1 = ccy[0]
        base_ccy2 = ccy[1]

        # self.ccy2_amt = 0

        # for acc in self.account_names:
        #     if acc.startswith("binance"):
        #         continue
        #     ex, _ = acc.split(".")
        #     api = self.get_exchange_api_by_account(acc)
        #     iid = f"{ex}.{self.contract}"
        #     symbol = f"{iid}.{self.market}.{self.sub_market}"
        #     if iid.startswith("lbank") and self.market == "usdt_contract":
        #         ccy1, _, _ = self.contract.split("_")
        #         symbol = f"{self.cfg.MM_EX}.{ccy1}_usdt_swap.swap.na"
        #     symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
        #     self._account_config_[acc]["cfg_pos_mode"][symbol] = 1
        #     cur_balance = (await api.account_balance(symbol_cfg)).data
        #     self.logger.warning(f"cur_balance={cur_balance}")
        #     if ex.startswith("lbank"):
        #         ccy2 = "usdt"
        #     else:
        #         ccy2 = base_ccy2
        #     self.ccy2_amt += cur_balance[ccy2]["all"]
            
        self.send = True
        
    
    def _get_pt(self, ex: str):
        if ex not in self._pt_cache:
            self._pt_cache[ex] = PricePrecisionManager().get_price_tick_by_ce(contract=self.contract, ex=ex)
        return self._pt_cache[ex]

    def _get_qt(self, ex: str):
        if ex not in self._qt_cache:
            self._qt_cache[ex] = QuantityPrecisionManager().get_quantity_tick_by_ce(contract=self.contract, ex=ex)
        return self._qt_cache[ex]

    def float_to_int_prc(self, ex: str, prc: float):
        pt = self._get_pt(ex)
        return int(round(prc / pt))

    def int_to_float_prc(self, ex: str, int_prc: float):
        pt = self._get_pt(ex)
        return int_prc * pt

    def on_msg(self, odr_msg: Order):
        if odr_msg.status in OrderStatus.fin_status():
            self.odr_mngr.on_odr_finish(odr_msg)
            self.guard_odr_mngr.on_odr_finish(odr_msg)
            return
        if odr_msg.status in {OrderStatus.New, OrderStatus.PartiallyFilled}:
            window = 10000  # 10s
            t = int(time.time() * 1e3)
            # MM
            if odr_msg.client_id in self.odr_mngr.pending_open_zone:
                if odr_msg.side == Side.long:
                    self.mm_pending_open_to_confirmed_open_long_q.append(odr_msg)
                    while True:
                        if not self.mm_pending_open_to_confirmed_open_long_q:
                            break
                        head: Order = self.mm_pending_open_to_confirmed_open_long_q[0]
                        if t - head.local_time < window:
                            break
                        self.mm_pending_open_to_confirmed_open_long_q.popleft()
                elif odr_msg.side == Side.short:
                    self.mm_pending_open_to_confirmed_open_short_q.append(odr_msg)
                    while True:
                        if not self.mm_pending_open_to_confirmed_open_short_q:
                            break
                        head: Order = self.mm_pending_open_to_confirmed_open_short_q[0]
                        if t - head.local_time < window:
                            break
                        self.mm_pending_open_to_confirmed_open_short_q.popleft()
            # GUARD
            if odr_msg.client_id in self.guard_odr_mngr.pending_open_zone:
                if odr_msg.side == Side.long:
                    self.guard_pending_open_to_confirmed_open_long_q.append(odr_msg)
                    while True:
                        if not self.guard_pending_open_to_confirmed_open_long_q:
                            break
                        head: Order = self.guard_pending_open_to_confirmed_open_long_q[0]
                        if t - head.local_time < window:
                            break
                        self.guard_pending_open_to_confirmed_open_long_q.popleft()
                elif odr_msg.side == Side.short:
                    self.guard_pending_open_to_confirmed_open_short_q.append(odr_msg)
                    while True:
                        if not self.guard_pending_open_to_confirmed_open_short_q:
                            break
                        head: Order = self.guard_pending_open_to_confirmed_open_short_q[0]
                        if t - head.local_time < window:
                            break
                        self.guard_pending_open_to_confirmed_open_short_q.popleft()

            self.odr_mngr.on_odr_confirm_open(odr_msg)
            self.guard_odr_mngr.on_odr_confirm_open(odr_msg)
            return
        raise NotImplementedError()
    
    async def pending_order_to_confirmed_flow(self):
        while True:
            await asyncio.sleep(1)

            if len(self.mm_pending_open_to_confirmed_open_long_q) == 0:
                msg = "mm long pending to confirmed 0 for 10s"
                self.loop.create_task(
                    send_ding_talk(title="LBANK", 
                                    message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: {msg}, strategy:{self.strategy_name}",
                                    token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                    secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                )
            if len(self.mm_pending_open_to_confirmed_open_short_q) == 0:
                msg = "mm short pending to confirmed 0 for 10s"
                self.loop.create_task(
                    send_ding_talk(title="LBANK", 
                                    message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: {msg}, strategy:{self.strategy_name}",
                                    token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                    secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                )
            if len(self.guard_pending_open_to_confirmed_open_long_q) == 0:
                msg = "guard long pending to confirmed 0 for 10s"
                self.loop.create_task(
                    send_ding_talk(title="LBANK", 
                                    message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: {msg}, strategy:{self.strategy_name}",
                                    token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                    secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                )
            if len(self.mm_pending_open_to_confirmed_open_short_q) == 0:
                msg = "guard short pending to confirmed 0 for 10s"
                self.loop.create_task(
                    send_ding_talk(title="LBANK", 
                                    message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: {msg}, strategy:{self.strategy_name}",
                                    token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                    secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                )

                
                
    def schedule_cancel_order(self, cid: str, om: ZoneOrderManager):
        o = om.get_trans_order(cid)
        if o:
            self.loop.create_task(self.handle_cancel_order(oid=cid, order=o, order_manager=om))
            # self.loop.run_until_complete(self.handle_cancel_order(oid=cid, order=o, order_manager=om))

    def on_bbo_(self, bbo: BBO):
        td = self.executor.on_bbo(bbo=bbo)
        for cid in td.orders_to_cancel:
            self.schedule_cancel_order(cid=cid, om=self.odr_mngr)
        td = self.guard_executor.on_bbo(bbo=bbo)
        for cid in td.orders_to_cancel:
            self.schedule_cancel_order(cid=cid, om=self.guard_odr_mngr)

    async def on_agg_trade(self, symbol, trade: PublicTrade):
        self.trd_vol_n_tick_calc.on_trade_price(trade_price=float(trade.price), timestamp=trade.local_ms)

    def update_rel_prc_shift(self):
        pt = self._get_pt(ex=self.cfg.MM_EX)
        try:
            ms_n_ticks = max(self.trd_vol_n_tick_calc.get_vol_n_ticks_quantile(0.3), 0.04)
            trd_tau = self.trd_vol_n_tick_calc.get_avg_tau()

            if not trd_tau or not self.mp:
                avg_rps = 5e-5
                sigma_rps = avg_rps / 2.33  # 1% prob == 0
            else:
                floored_trd_tau = max(500, trd_tau)
                trd_n_ticks = ms_n_ticks * floored_trd_tau
                avg_rps = trd_n_ticks * pt / self.mp
                sigma_rps = avg_rps / 2.33  # 1% prob == 0
                self.logger.critical(f"RPS: ms_n_ticks={ms_n_ticks},trd_tau={trd_tau},trd_n_ticks={trd_n_ticks},avg_rps={avg_rps}")

            rps = max(0, np.random.randn() * sigma_rps + avg_rps)  # floor at 0
            rps = min(rps, 0.003)  # capped at 0.003
        except:
            traceback.print_exc()
            rps = 0.0001

        rps_nticks = rps * self.mp / pt
        self.logger.critical(f"RPS: rps={rps}, rps_nticks={rps_nticks}")

        self.executor.on_rel_prc_shift(rel_prc_shift=rps)

    def on_depth(self, depth: Depth):
        self.update_rel_prc_shift()
        td = self.executor.on_depth(depth=depth)
        for todr in td.orders_to_send:
            if todr.quantity > float(self.max_order_volume):
                todr.quantity = float(self.max_order_volume)
            self.loop.create_task(self.send_order(todr=todr, om=self.odr_mngr))
        for cid in td.orders_to_cancel:
            self.schedule_cancel_order(cid=cid, om=self.odr_mngr)
        guard_tgt_depth_amt = self.cfg.TARGET_DEPTH_AMOUNT - self.executor.tgt_depth_amt
        self.guard_executor.on_min_ret(min_ret=max(self.executor.bid_back_ret, self.executor.ask_back_ret))
        self.guard_executor.on_tgt_depth_amt(tgt_depth_amt=guard_tgt_depth_amt)
        self.logger.critical(f"dmm_tgt_depth_amt={self.executor.tgt_depth_amt},guard_tgt_depth_amt={self.guard_executor.tgt_depth_amt}")
        td = self.guard_executor.on_depth(depth=depth)
        for todr in td.orders_to_send:
            if todr.quantity > float(self.max_order_volume):
                todr.quantity = float(self.max_order_volume)
            self.loop.create_task(self.send_order(todr=todr, om=self.guard_odr_mngr))
        for cid in td.orders_to_cancel:
            self.schedule_cancel_order(cid=cid, om=self.guard_odr_mngr)
            
    async def send_order(self, todr: TransOrder, om: ZoneOrderManager):
        if not self.send:
            return
        self.pop_flow()
        if self.flow_count > 200:
            return
        self.trim_order_before_sending(todr)
        if todr.client_id:
            om.add_trans_order(todr)
            self.flow_data_ts.append(time.time()*1e3)
            self.flow_count += 1
            try:
                resp = await self.base_send_order(order=todr)
                if resp:
                    todr.xchg_id = resp.xchg_id
                else:
                    om.pop_trans_order(todr.client_id)
            except:
                self.logger.critical(f"order update during send err:{traceback.format_exc()}")
                om.pop_trans_order(todr.client_id)
                
    def pop_flow(self):
        while self.flow_data_ts:
            if time.time()*1e3 - self.flow_data_ts[0] > 1000:
                self.flow_data_ts.popleft()
                self.flow_count -= 1
            else:
                break
                
            
    async def on_order(self, order):
        try:
            await self.order_queue.put(order)
        except:
            traceback.print_exc()

    def handle_order(self, order: Union[PartialOrder, AtomOrder]):
        
        if order.client_id in self.send_order_on_msg:
            self.send_order_on_msg_q.append(int(time.time()*1e3) - self.send_order_on_msg[order.client_id])
            self.send_order_on_msg.pop(order.client_id, None)
        
        if order.xchg_status in OrderStatus.fin_status() and order.client_id in self.cancel_order_on_msg:
            if order.xchg_status in {OrderStatus.Canceled}:
                self.cancel_order_on_msg_q.append(int(time.time()*1e3) - self.cancel_order_on_msg[order.client_id])
            self.cancel_order_on_msg.pop(order.client_id, None)
        
        # self.logger.critical(f"order msg:{order.__dict__}")
        
        # if int(time.time()*1e3)-order.server_ms > 1000:
        #     self.logger.critical(f"recieve message delay {int(time.time()*1e3-order.server_ms)}ms, cid={order.client_id}")
        if isinstance(order, PartialOrder):
            if order.exchange_pair not in self.ex_pair_set:
                # self.logger.critical(f"{order.__dict__}")
                return
        elif isinstance(order, AtomOrder):
            symbol_cfg = self.get_symbol_config(order.symbol_id)
            if symbol_cfg["exchange_pair"] not in self.ex_pair_set:
                return
            
        if (not self.odr_mngr.__contains__(cid=order.client_id)) and (not self.guard_odr_mngr.__contains__(cid=order.client_id)):
            return
        
        try:
            s = time.time()
            todr = self.odr_mngr.get_trans_order(cid=order.client_id)
            todr = self.guard_odr_mngr.get_trans_order(cid=order.client_id) or todr
            odr_msg: Order = Order.from_dict(
                    porder=order,
                    torder=todr,
                    contract_value=self.maker_contract_value
                )
            self.on_msg(odr_msg=odr_msg)

            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(
                    f"on order processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle on_order err: {err}")
            traceback.print_exc()
            self.loop.create_task(
                send_ding_talk(title="LBANK", 
                                 message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: handle order err{traceback.format_exc()}, strategy:{self.strategy_name}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )

    async def on_orderbook(self, symbol: str, orderbook: Dict):
        try:
            if symbol.startswith(self.cfg.MM_EX):
                await self.maker_book_queue.put((symbol, orderbook))
            else:
                await self.taker_book_queue.put((symbol, orderbook))
        except:
            traceback.print_exc()
            self.logger.warning(f"{symbol}, {orderbook['asks']}")

    def handle_orderbook(self, symbol: str, orderbook: Dict):
        self.ob_update = True
        try:
            self.ap = orderbook["asks"][0][0]
            self.bp = orderbook["bids"][0][0]
        except:
            pass
        try:
            s = time.time()
            ex, contract = symbol.split(".")
            ccy = contract.split("_")
            depth_obj: Depth = Depth.from_dict(
                depth=orderbook,
                ccy1=ccy[0],
                ccy2=ccy[1],
                ex=ex,
                contract=contract
            )
            # if ex == "binance":
            #     self.ob_counter += 1
            
            # if self.ob_counter % 3 != 0:
            #     return
            self.on_depth(depth=depth_obj)

            om_mm = self.odr_mngr
            for side in [Side.long, Side.short]:
                if not self._min_confirmed_open_zone_initialized[side] and (om_mm.confirm_opened_zone.side_amts[side] > self.executor.tgt_depth_amt):
                    self._min_confirmed_open_zone[side] = om_mm.confirm_opened_zone.side_amts[side]
                    self._min_confirmed_open_zone_initialized[side] = True
                if self._min_confirmed_open_zone_initialized[side]:
                    self._min_confirmed_open_zone[side] = min(self._min_confirmed_open_zone[side], om_mm.confirm_opened_zone.side_amts[side])

            om_guard = self.guard_odr_mngr
            for side in [Side.long, Side.short]:
                if not self._guard_min_confirmed_open_zone_initialized[side] and (om_guard.confirm_opened_zone.side_amts[side] > self.guard_executor.tgt_depth_amt):
                    self._guard_min_confirmed_open_zone[side] = om_guard.confirm_opened_zone.side_amts[side]
                    self._guard_min_confirmed_open_zone_initialized[side] = True
                if self._guard_min_confirmed_open_zone_initialized[side]:
                    self._guard_min_confirmed_open_zone[side] = min(self._guard_min_confirmed_open_zone[side], om_guard.confirm_opened_zone.side_amts[side])
            
            
            for side in [Side.long, Side.short]:
                if self._guard_min_confirmed_open_zone_initialized[side] and self._min_confirmed_open_zone_initialized[side]:
                    opended = om_mm.confirm_opened_zone.side_amts[side] + om_guard.confirm_opened_zone.side_amts[side]
                    if opended < self.max_open_amount * 0.5:
                       self.loop.create_task(
                            send_ding_talk(title="LBANK", 
                                            message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: confirmed open order at low level, confirmed opened amount:{opended}, side:{side},  strategy:{self.strategy_name}",
                                            token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                            secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                        )
                       self.logger.critical(f"warning: confirmed open order at low level, confirmed opened amount:{opended}, side:{side}")
            
            
            if time.time()*1e3 - s*1e3 > 100:
                self.logger.critical(
                    f"orderbook processing inner time is {time.time()*1e3 - s*1e3}ms")
        except Exception as err:
            self.logger.critical(f"handle orderbook err: {err}")
            traceback.print_exc()
            self.loop.create_task(
                send_ding_talk(title="LBANK", 
                                 message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: handle orderbook err{traceback.format_exc()}, strategy:{self.strategy_name}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )
    
    async def on_bbo(self, symbol, bbo):
        try:
            await self.bbo_queue.put((symbol, bbo))
        except:
            traceback.print_exc()

    def handle_bbo(self, symbol: str, bbo: BBODepth):
        if not self.ob_update:
            return
        try:
            bbo_obj: BBO = BBO.from_dict_prod(iid=symbol, bbo_depth=bbo)
            self.on_bbo_(bbo=bbo_obj)
            self.mp = (bbo_obj.best_ask_prc + bbo_obj.best_bid_prc) / 2
        except Exception as err:
            self.logger.critical(f"handle bbo err: {err}")
            traceback.print_exc()
            self.loop.create_task(
                send_ding_talk(title="LBANK", 
                                 message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: handle bbo err{traceback.format_exc()}, strategy:{self.strategy_name}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )

    async def handle_ws_update(self):

        while True:
            try:
                if not self.order_queue.empty():
                    order = await self.order_queue.get()
                    self.handle_order(order)
                    continue
                elif self.maker_book_queue.empty() and self.taker_book_queue.empty() and self.bbo_queue.empty():
                    await asyncio.sleep(0.001)
                elif not self.taker_book_queue.empty():
                    symbol, orderbook = await self.taker_book_queue.get()
                    self.taker_book_queue = LifoQueue()
                    self.handle_orderbook(symbol=symbol, orderbook=orderbook)
                elif not self.bbo_queue.empty():
                    symbol, bbo = await self.bbo_queue.get()
                    self.bbo_queue = LifoQueue()
                    self.handle_bbo(symbol=symbol, bbo=bbo)
                else:
                    symbol, orderbook = await self.maker_book_queue.get()
                    self.maker_book_queue = LifoQueue()
                    self.handle_orderbook(symbol=symbol, orderbook=orderbook)

                await asyncio.sleep(0.001)
            except:
                await asyncio.sleep(0.001)
                traceback.print_exc()

    async def base_send_order(self, order: TransOrder):
        if order.quantity == 0:
            # self.logger.info(f"Trying to send qty=0 order, skip: {order.client_id}")
            return None
        iid = order.iid
        side = self.direction_map(side=order.side)
        symbol = f"{iid}.{self.market}.{self.sub_market}"
        if iid.startswith("lbank") and self.market == "usdt_contract":
            ccy1, _, _ = self.contract.split("_")
            symbol = f"{self.cfg.MM_EX}.{ccy1}_usdt_swap.swap.na"

        if not self.volume_notional_check(symbol=symbol, price=order.price, qty=order.quantity):
            self.logger.info(f"volume_notional_check failed, skip: {order.client_id}, qty={order.quantity}")
            return None

        order_type = OrderType.Limit
        if order.maker_taker == MakerTaker.maker:
            order_type = OrderType.PostOnly
        if order.maker_taker == MakerTaker.taker:
            order_type = OrderType.IOC

        try:
            symbol_cfg = self.get_symbol_config(symbol)
            st = int(time.time() * 1e3)
            self.send_order_on_msg[order.client_id] = st
            self.send_count += 1
            resp: AtomOrder = await self.make_order(
                symbol_name=symbol,
                price=Decimal(order.price) +
                symbol_cfg["price_tick_size"]/Decimal("2"),
                volume=Decimal(order.quantity) +
                symbol_cfg["qty_tick_size"]/Decimal("2"),
                side=side,
                order_type=order_type,
                position_side=OrderPositionSide.Open,
                client_id=order.client_id,
                recvWindow=self.recvWindow,
                cancel_cross=True
            )
            et = int(time.time() * 1e3)
            elapsed = et - st
            self.send_delay.append(elapsed)
            self.send_elapsed_q.append(elapsed)
        except Exception as err:
            self.logger.critical(f"send order failed, {err}")
            self.send_order_on_msg.pop(order.client_id, None)
            return None
        
        try:
            if not resp.xchg_id:
                self.logger.critical(f"response of sending order without xchg id:{resp.__dict__}")
            order.update(resp)
            order.price = round(float(resp.requested_price), price_precision[iid])
            order.quantity = round(
                float(resp.requested_amount), qty_precision[iid])
        except:
            self.logger.critical(f"order update after send err:{traceback.format_exc()}")
            return resp
        return resp

    def trim_order_before_sending(self, order: TransOrder):
        ex, contract = order.iid.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex, market="swap")
        if ex.startswith("lbank") and self.market == "usdt_contract":
            ccy1, _, _ = contract.split("_")
            order.symbol_id = self.get_symbol_config(
                f"{ex}.{ccy1}_usdt_swap.swap.na")["id"]
        else:
            order.symbol_id = self.get_symbol_config(
                f"{order.iid}.{self.market}.{self.sub_market}")["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)

        order.price = round(order.price, price_precision[order.iid])

        order.quantity = round(order.quantity, qty_precision[order.iid])

        return cid
    

    async def cancel_order(self, order: TransOrder, catch_error=True):
        """
        Cancel a order
        order: TransOrder object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(
            order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
        s = int(time.time()*1e3)
        if self.cancel_order_on_msg.get(order.client_id) == None:
            self.cancel_order_on_msg[order.client_id] = s
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(
                    f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            elapsed = int(time.time()*1e3) - s
            self.logger.info(f"cancel takes: {elapsed} ms")
            self.cancel_elapsed_q.append(elapsed)
            return r.data
        except Exception as err:
            self.cancel_order_on_msg.pop(order.client_id, None)
            if not catch_error:
                raise
            else:
                self.logger.critical(
                    f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, redirect False")
                return False

    async def handle_cancel_order(self, oid, order: TransOrder, order_manager: ZoneOrderManager):
        
        async def unit_cancel_order(order: TransOrder):
            try:
                self.cancel_count += 1
                res = await self.cancel_order(order)
                if res is True:
                    order.cancel = True
            except Exception as err:
                self.logger.critical(f'cancel order {oid} err {err}')

        if oid in self.canceling_id:
            return

        self.canceling_id.add(oid)

        st = time.time()*1e3
        order_manager.on_cancel_order(oid)
        self.timer1.append(time.time()*1e3-st)
        while True:
            if order_manager.get_trans_order(cid=oid) != None:
                st2 = time.time()*1e3
                o: TransOrder = order_manager.get_trans_order(cid=oid)
                self.timer2.append(time.time()*1e3-st2)
                if o.cancel:
                    self.canceling_id.remove(oid)
                    return
            else:
                self.canceling_id.remove(oid)
                return
            
            await unit_cancel_order(order)
            await asyncio.sleep(1)
        
    
    async def rebalance(self):
        # rebalance, use when initializing or emergency exiting.
        ccy = self.contract.split("_")
        ccy1 = ccy[0]
        ccy2 = ccy[1]
        if self.market == "spot":
            pass
        elif self.market == "usdt_contract":
            for acc in self.account_names:
                if acc.startswith("binance"):
                    continue
                ex, _ = acc.split(".")
                api = self.get_exchange_api_by_account(acc)
                iid = f"{ex}.{self.contract}"
                symbol = f"{iid}.{self.market}.{self.sub_market}"
                if iid.startswith("lbank"):
                    ccy1, _, _ = self.contract.split("_")
                    symbol = f"{self.cfg.MM_EX}.{ccy1}_usdt_swap.swap.na"
                symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                try:
                    all_opening_orders: List[AtomOrder] = (await api.all_opening_orders(symbol_cfg)).data
                    iid = f"{symbol_cfg['exchange_name']}.{symbol_cfg['pair']}"
                    for order in all_opening_orders:
                        if order.side == OrderSide.Buy:
                            side = Direction.long
                        elif order.side == OrderSide.Sell:
                            side = Direction.short
                        else:
                            self.logger.critical(f"{order.side} not implemented")
                        torder = TransOrder(
                            side=side,
                            p=float(order.requested_price),
                            q=float(order.requested_amount),
                            iid=iid,
                            floor=0,
                            delta=0,
                        )
                        torder.xchg_id = order.xchg_id
                        torder.client_id = order.client_id
                        torder.symbol_id = order.symbol_id
                        torder.sent_ts = int(time.time()*1e3)
                        self.odr_mngr.add_confirmed_open_trans_order(todr=torder)
                except:
                    traceback.print_exc()
                    self.logger.info("no open orders")
                    
                # cur_position = (await api.contract_position(symbol_cfg)).data
                # self.logger.critical(f"current position: {cur_position}")
                # pos = cur_position["long_qty"] - cur_position["short_qty"]

                # self.logger.info(f"""current position={pos}""")
                # self._inventory += float(pos)
                # if ex == self.cfg.MM_EX:
                #     self._obl_inventory += float(pos)
                await asyncio.sleep(1)

    async def get_order_status_direct(self, oid, order: TransOrder, order_manager: ZoneOrderManager):
        async def unit_query_order(order: TransOrder):
            try:
                symbol = self.get_symbol_config(order.symbol_id)
                api = self.get_exchange_api_by_account(
                    order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                st = int(time.time()*1e3)
                self.query_count += 1
                res = await api.order_match_result(
                    symbol=self.get_symbol_config(order.symbol_id),
                    order_id=order.xchg_id,
                    client_id=order.client_id,
                    match_cache=True
                )
                new_order: AtomOrder = res.data
                self.logger.critical(f"query order res;{new_order.__dict__}, takes: {int(time.time()*1e3) - st}")
                if new_order.xchg_status in OrderStatus.fin_status():
                    await self.on_order(new_order)
            except OrderNotFoundError as err:
                order.query += 1
                self.logger.critical(
                    f"order not found: {err}, iid={order.iid}, oid={order.xchg_id}, cid={order.client_id}")
            except Exception as err:
                self.logger.critical(
                    f"direct check order error: {err}, iid={order.iid}, oid={order.xchg_id}, cid={order.client_id}")
                
        window = self.recvWindow * 3

        if time.time()*1e3 - order.sent_ts < window or oid in self.quering_id:
            return

        self.quering_id.add(oid)

        counter = 0
        
        while True:
            self.logger.critical(f"order id={order.xchg_id}, cid={order.client_id}, query times:{order.query}")
            if order_manager.get_trans_order(cid=oid) != None:
                if time.time()*1e3 - order.sent_ts < window:
                    self.quering_id.remove(oid)
                    return
            else:
                self.quering_id.remove(oid)
                return
            if order.xchg_id:
                counter += 1
                await unit_query_order(order)
                if order.query > 0:
                    self.not_found_trans_order.append(order)
                    order_manager.on_odr_not_found(todr=order)
                    self.logger.critical(f"order not found pop, order id={order.xchg_id}, cid={order.client_id}.{order_manager.__contains__(cid=order.client_id)}")
                    self.quering_id.remove(oid)
                    return
            else:
                await asyncio.sleep(0.1)
                continue
            await asyncio.sleep(window / 1000)

            # if counter > 1000:
            #     if order_manager.get_trans_order(cid=oid) != None:
            #         self.logger.critical(f"order query too many times")
            #         try:
            #             await send_ding_talk(title="LBANK", message=f"warning: order query {counter} times, exchange id={order.xchg_id}, strategy:{self.strategy_name}",
            #                                  token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            #         except:
            #             traceback.print_exc()
                        
    async def check_not_found_order(self):
        while True:
            await asyncio.sleep(10)
            while self.not_found_trans_order:
                order: TransOrder = self.not_found_trans_order.popleft()
                try:
                    symbol = self.get_symbol_config(order.symbol_id)
                    api = self.get_exchange_api_by_account(
                        order.account_name) if order.account_name else self.get_exchange_api(symbol['exchange_name'])[0]
                    res: AtomOrder = await api.order_match_result(
                        symbol=self.get_symbol_config(order.symbol_id),
                        order_id=order.xchg_id,
                        client_id=order.client_id,
                        match_cache=True
                    )
                    if res.xchg_status not in OrderStatus.fin_status():
                        self.logger.critical(f"query result:{res.__dict__}")
                        self.logger.critical(f"query trans order:{order.__dict__}")
                        self.err_not_found_order.append(res)
                except OrderNotFoundError as err:
                    pass
                except Exception as err:
                    self.not_found_trans_order.append(order)
                await asyncio.sleep(1)

    async def reset_missing_order_action(self):
        while True:
            await asyncio.sleep(1)
            self.check_orders(order_manager=self.odr_mngr)
            self.check_orders(order_manager=self.guard_odr_mngr)
            await asyncio.sleep(59)

    def check_orders(self, order_manager: ZoneOrderManager):
        for oid in order_manager.pending_cancel_zone.odr_cache.keys():
            if order_manager.get_trans_order(oid):
                o = order_manager.get_trans_order(oid)
                self.loop.create_task(self.get_order_status_direct(
                    oid=oid, order=o, order_manager=order_manager))

    async def update_redis_cache(self):
        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await asyncio.sleep(5)
                    for acc in self.account_names:
                        if acc.startswith("binance"):
                            continue
                        ex, _ = acc.split(".")
                        api = self.get_exchange_api_by_account(acc)
                        iid = f"{ex}.{self.contract}"
                        symbol = f"{iid}.{self.market}.{self.sub_market}"
                        if iid.startswith("lbank"):
                            ccy1, _, _ = self.contract.split("_")
                            symbol = f"{self.cfg.MM_EX}.{ccy1}_usdt_swap.swap.na"
                        symbol_cfg = self.get_symbol_config(symbol_identity=symbol)
                        try:
                            await api.flash_cancel_orders(symbol_cfg)
                        except:
                            traceback.print_exc()
                            self.logger.info("no open orders")
                    await self.redis_set_cache({})
                    self.logger.critical(f"manually exiting")
                    exit()
                oid_len = [int(len(x)) for x in self.mm_cache.values()]
                await self.redis_set_cache(
                    {
                    "exit": None,
                    "send order on msg": len(self.send_order_on_msg),
                    "cancel order on msg": len(self.cancel_order_on_msg),
                    "send_order_on_msg_q":len(self.send_order_on_msg_q),
                    "cancel_order_on_msg_q":len(self.cancel_order_on_msg_q),
                    "mm_id cache": len(self.mm_cache),
                    "id in mm_id max":f"{np.max(oid_len)}" if oid_len else 0, 
                    "err not found order":len(self.err_not_found_order), 
 
                    }
                    )
            except Exception as err:
                self.logger.critical(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()

    async def print_local_stats(self):
        while True:
            await asyncio.sleep(10)
            try:
                percentiles = [0.5, 0.9, 0.95, 0.99, 0.999]
                self.logger.critical(f"{pd.Series(self.send_elapsed_q, name='send_elapsed').describe(percentiles=percentiles)}")
                self.logger.critical(f"{pd.Series(self.cancel_elapsed_q, name='cancel_elapsed').describe(percentiles=percentiles)}")
                self.logger.critical(f"{pd.Series(self.send_order_on_msg_q, name='send_on_msg_elapsed').describe(percentiles=percentiles)}")
                self.logger.critical(f"{pd.Series(self.cancel_order_on_msg_q, name='cancel_on_msg_elapsed').describe(percentiles=percentiles)}")

                self.logger.critical(f"t={int(time.time()*1e3)}")

                om = self.odr_mngr
                self.logger.critical("MM")
                self.logger.critical(f"min_confirmed_open_zone={self._min_confirmed_open_zone},min_confirmed_open_zone_initialized={self._min_confirmed_open_zone_initialized}")
                self.logger.critical(f"pending_open_zone={om.pending_open_zone.zone_amt},confirmed_open_zone={om.confirm_opened_zone.zone_amt},pending_cancel_zone={om.pending_cancel_zone.zone_amt}")
                self.logger.critical(f"pending_open_long={om.pending_open_zone.side_amts[Side.long]},pending_open_short={om.pending_open_zone.side_amts[Side.short]}")
                self.logger.critical(f"confirmed_open_long={om.confirm_opened_zone.side_amts[Side.long]},confirmed_open_short={om.confirm_opened_zone.side_amts[Side.short]}")
                open_long_amt = om.pending_open_zone.side_amts[Side.long] + om.confirm_opened_zone.side_amts[Side.long]
                open_short_amt = om.pending_open_zone.side_amts[Side.short] + om.confirm_opened_zone.side_amts[Side.short]
                self.logger.critical(f"open_long={open_long_amt},open_short={open_short_amt}")

                om = self.guard_odr_mngr
                self.logger.critical("GUARD")
                self.logger.critical(f"min_confirmed_open_zone={self._guard_min_confirmed_open_zone},min_confirmed_open_zone_initialized={self._guard_min_confirmed_open_zone_initialized}")
                self.logger.critical(f"pending_open_zone={om.pending_open_zone.zone_amt},confirmed_open_zone={om.confirm_opened_zone.zone_amt},pending_cancel_zone={om.pending_cancel_zone.zone_amt}")
                self.logger.critical(f"pending_open_long={om.pending_open_zone.side_amts[Side.long]},pending_open_short={om.pending_open_zone.side_amts[Side.short]}")
                self.logger.critical(f"confirmed_open_long={om.confirm_opened_zone.side_amts[Side.long]},confirmed_open_short={om.confirm_opened_zone.side_amts[Side.short]}")
                open_long_amt = om.pending_open_zone.side_amts[Side.long] + om.confirm_opened_zone.side_amts[Side.long]
                open_short_amt = om.pending_open_zone.side_amts[Side.short] + om.confirm_opened_zone.side_amts[Side.short]
                self.logger.critical(f"open_long={open_long_amt},open_short={open_short_amt}")
                
                self.logger.critical(f"mm long 10s pending to confirmed flow: {len(self.mm_pending_open_to_confirmed_open_long_q)}")
                self.logger.critical(f"mm short 10s pending to confirmed flow: {len(self.mm_pending_open_to_confirmed_open_short_q)}")
                self.logger.critical(f"guard long 10s pending to confirmed flow: {len(self.guard_pending_open_to_confirmed_open_long_q)}")
                self.logger.critical(f"guard short 10s pending to confirmed flow: {len(self.guard_pending_open_to_confirmed_open_short_q)}")
                
                # self.logger.critical(f"task len:{len(asyncio.all_tasks())}")
                # task_count = dict()
                # for task in asyncio.all_tasks():
                #     if task_count.get(task._coro.__name__):
                #         task_count[task._coro.__name__] += 1
                #     else:
                #         task_count[task._coro.__name__] = 1
                
                # self.logger.critical(f"task count:{task_count}")
                
                # self.logger.critical(f"{pd.Series(self.timer1, name='timer1').describe(percentiles=percentiles)}")
                # self.logger.critical(f"{pd.Series(self.timer2, name='timer1').describe(percentiles=percentiles)}")

            except Exception as err:
                self.logger.critical(f"print stats err: {err}")
                traceback.print_exc()

    async def send_stats_to_grafana(self):
        while True:
            await asyncio.sleep(60)
            try:
                self.bp_count += 1
                api = self.get_exchange_api(exchange_name="lbank")[0]
                resp = await api.account_balance_all(market="swap", sub_market="na")
                curr_balance = float(resp.data["usdt"]["all"])
                curr_unrealized = float(resp.json["data"]["unrealizedProfit"])
                
                cur_position = (await api.contract_position_all(market="swap", sub_market="na"))
                self.logger.critical(f"position info:{cur_position}") 
                if len(cur_position.data) > 1:
                    self.logger.critical("single account has more than one contract has position!!!")
                    
                for contract in cur_position.data:
                    if contract != self.contract:
                        continue
                    pos = cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"]
                    self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                "current position":  float(self.mp * float(pos)) 
                            }
                        )
                    )
                self.loop.create_task(
                        self.push_influx_data(
                            measurement="tt",
                            tag={"sn": self.strategy_name},
                            fields={
                                "current balance": float(curr_balance + curr_unrealized)
                            }
                        )
                    )
            except:
                pass
    
    async def collect_request_count(self):
        while True:
            await asyncio.sleep(10)
            self.loop.create_task(
                self.push_influx_data(
                    measurement="tt",
                    tag={"sn": self.strategy_name},
                    fields={
                        "send count": self.send_count,
                        "cancel count": self.cancel_count,
                        "send delay": np.mean(self.send_delay) if self.send_delay else 0,
                    }
                )
            )
            if self.send_count < 100:
                self.loop.create_task(
                    send_ding_talk(title="LBANK", 
                                    message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: send order abnormal, current sending frequency: {self.send_count}/10s, strategy:{self.strategy_name}",
                                    token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                    secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
                )
            self.send_count = 0
            self.cancel_count = 0
            self.query_count = 0
            self.bp_count = 0
            self.send_delay = []
            
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                # self.check_unknown_order(),
                self.reset_missing_order_action(),
                self.update_redis_cache(),
                self.print_local_stats(),
                self.check_not_found_order(),
                self.send_stats_to_grafana(),
                self.collect_request_count(),
                self.pending_order_to_confirmed_flow()
            )


if __name__ == '__main__':
    logging.getLogger().setLevel("CRITICAL")
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
