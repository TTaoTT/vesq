import asyncio
from decimal import Decimal
import time 
import numpy as np
import collections
from asyncio.queues import Queue

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
class PairData():
    def __init__(self) -> None:
        self.symbol = ""
        self.sell_trade_cache = None
        self.buy_trade_cache = None
        self.stats = None

SYMBOLS = ["gmt", "ape", "zil", "zrx", "luna", "sol", "near","ogn","knc","doge"]
                
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        
        self.data = dict()
        for symbol in SYMBOLS:
            tmp = f"binance.{symbol}_usdt_swap.usdt_contract.na"
            key = self.get_symbol_config(tmp)["exchange_pair"]
            self.data[key] = PairData()
            self.data[key].symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
            self.data[key].sell_trade_cache = PriceDistributionCache(50)
            self.data[key].buy_trade_cache = PriceDistributionCache(50)
            self.data[key].stats = collections.deque(maxlen=10000)

        self.price_muli = 1e7
        self.qty_muli = 1e7
        self.quantile = 0.9
        self.trade_ts = 0
        
        self.use_raw_stream = True
        
    def update_config_before_init(self):
        self.use_colo_url = True
        
    async def before_strategy_start(self):
        for key in self.data:
            self.direct_subscribe_public_trade(symbol_name=self.data[key].symbol)
        self.logger.setLevel("WARNING")
        
    async def on_public_trade(self, symbol, trade):
        key = trade["data"]["s"]
        self.trade_ts = max(self.trade_ts, trade["data"]["E"])
        tmp = {
            "t":trade["data"]["E"],
            "p":int(float(trade["data"]["p"])*self.price_muli), 
            "q":int(float(trade["data"]["q"])*self.qty_muli)
            }
        if trade["data"]["m"]:
            self.data[key].sell_trade_cache.feed_trade(tmp)
        else:
            self.data[key].buy_trade_cache.feed_trade(tmp)

        self.data[key].buy_trade_cache.remove_expired(self.trade_ts)
        self.data[key].sell_trade_cache.remove_expired(self.trade_ts)
        
    async def main_logic(self):
        while True:
            await asyncio.sleep(0.1)
            ts = time.time()*1e3
            for key in self.data:
                ask = self.data[key].buy_trade_cache.get_ask_price(self.quantile)
                bid = self.data[key].sell_trade_cache.get_bid_price(self.quantile)

                if ask and bid:
                    weight = ask - bid
                    weight = weight / (ask + bid) * 2
                else:
                    weight = 0
                self.data[key].stats.append(weight)
        
    async def set_cache(self):
        while True:
            await asyncio.sleep(30)
            cache = dict()
            cache["name"] = "95_98_99_max"
            for key in self.data:
                q95 = str(round(np.quantile(self.data[key].stats, 0.95)*1e3,2))
                q98 = str(round(np.quantile(self.data[key].stats, 0.98)*1e3,2))
                q99 = str(round(np.quantile(self.data[key].stats, 0.99)*1e3,2))
                m = str(round(np.max(self.data[key].stats)*1e3,2))
                cache[key] = f"{q95}_{q98}_{q99}_{m}"
            await self.redis_set_cache(cache)
        
    async def strategy_core(self):
        await asyncio.sleep(2)
        await asyncio.gather(
            self.main_logic(),
            self.set_cache()
            )
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
        
        
        
    
    