import asyncio
from decimal import Decimal
import time 
import pandas as pd

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

from atom.exchange_api.abc import ApiResponse

from collections import defaultdict

from asyncio.queues import Queue
from sortedcontainers import SortedDict

import traceback

# class BestOfferTracker():
#     def __init__(self) -> None:
#         self.best_ask = ...
#         self.best_bid = ...
#         self.ts = 0
    
#     def on_bbo(self, bbo: BBODepth):
#         # TODO exit when time is expired
#         if bbo.server_ms <= self.ts:
#             return
#         self.ts = bbo.server_ms
#         self.best_ask = Decimal(bbo.ask[0])
#         self.best_bid = Decimal(bbo.bid[0])

#     def on_failed_order(self, side: OrderSide, price: Decimal, ts: int):
#         if ts <= self.ts:
#             return
#         # trigger when post only order failed
#         if side == OrderSide.Buy:
#             if price < self.best_ask:
#                 self.best_ask = price
#                 self.ts = ts
#         elif side == OrderSide.Sell:
#             if price > self.best_bid:
#                 self.best_bid = price
#                 self.ts = ts
                
#     def get_best_price(self):
#         return (self.best_ask, self.best_bid)

class  AlgoOrderLoadMalancer:
    def __init__(self) -> None:
        self.account_list = []
        self.load = dict()
    
    def set_up(self, account_list):
        self.account_list = account_list
        for acc in self.account_list:
            self.load[acc] = 0
        
    def new_stop_order(self, acc):
        self.load[acc] += 1
    
    def finished_stop_order(self, acc):
        self.load[acc] -= 1
    
    def get_available_acc(self):
        for acc in self.load:
            if self.load[acc] < 10:
                return acc
        return None

        
class TradeCache:
    def __init__(self, logger) -> None:
        self.cache = SortedDict()
        self.logger = logger
        
        
    def new_trade(self, trade: PublicTrade):
        if trade.price == Decimal(0):
            self.logger.info(f"wrong trade: {trade}")
        if self.cache.get(trade.transaction_ms) != None:
            price = self.cache[trade.transaction_ms]
            if price != -1:
                if price != trade.price:
                    self.cache[trade.transaction_ms] = -1
        else:
            self.cache[trade.transaction_ms] = trade.price
        
    def expired_trade(self):
        for ts in self.cache:
            if ts + 120000 < time.time()*1e3:
                self.cache.pop(ts)
    
    def compare(self, start, end, price):
        for ts in self.cache:
            if self.cache[ts] == Decimal(0):
                self.logger.info(f"wrong price in trade cache, {ts}, {self.cache[ts]}")
            if start <= ts and end >= ts and self.cache[ts] == price:
                return 1 
        
        return 0

class CustomizedOrderManager:
    def __init__(self, logger, start, end) -> None:
        self.global_cache = dict()
        self.stop_order_buy_cache = defaultdict(dict)
        self.stop_order_sell_cache = defaultdict(dict)
        self.limit_order_cache = defaultdict(dict)
        
        self.shift_start = start
        self.shift_end = end
        self.inventory = 0
        self.max_inventory = 0.03
        
        self.logger = logger
        
        self.stats_filled_count = 0
        self.stats_count = 0
        
        
    
    def on_bbo_change(self, side: str, price: Decimal, tick):
        send_stop_orders = []
        cancel_orders = []
        if side == "ask":
            # handle stop orders
            for old_price in self.stop_order_sell_cache:
                if old_price < price - self.shift_end or old_price > price - self.shift_start:
                    for oid in self.stop_order_sell_cache[old_price]:
                        if self.global_cache[oid].message["cancel"] == False:
                            cancel_orders.append(self.global_cache[oid])
            if self.inventory > -self.max_inventory:
                for i in range(self.shift_start, self.shift_end):
                    if price - Decimal(str(int(i))) * tick not in self.stop_order_sell_cache:    
                        send_stop_orders.append((Decimal("0"), price - Decimal(str(int(i))) * tick, "SELL"))
                        send_stop_orders.append((Decimal("1"), price - Decimal(str(int(i))) * tick, "SELL"))
            # handle limit order
            for limit_price in self.limit_order_cache:
                for oid, order in self.limit_order_cache[limit_price].items():
                    if order.side == OrderSide.Sell and order.requested_price > price:
                        if self.global_cache[oid].message["cancel"] == False:
                            cancel_orders.append(self.global_cache[oid])  
            
        elif side == "bid":
            return [], []
            # handle stop orders
            for old_price in self.stop_order_buy_cache:
                if old_price > price + self.shift_end or old_price < price + self.shift_end:
                    for oid in self.stop_order_buy_cache[old_price]:
                        if self.global_cache[oid].message["cancel"] == False:
                            cancel_orders.append(self.global_cache[oid])
            if self.inventory < self.max_inventory:
                for i in range(self.shift_start, self.shift_end):
                    if price + Decimal(str(int(i))) * tick not in self.stop_order_buy_cache:
                        send_stop_orders.append((Decimal("0"), price + Decimal(str(int(i))) * tick, "BUY"))
                        send_stop_orders.append((Decimal("-1"), price + Decimal(str(int(i))) * tick, "BUY"))
            
            # handle limit order
            for limit_price in self.limit_order_cache:
                for oid, order in self.limit_order_cache[limit_price].items():
                    if order.side == OrderSide.Buy and order.requested_price < price:
                        if self.global_cache[oid].message["cancel"] == False:
                            cancel_orders.append(self.global_cache[oid])
        return send_stop_orders, cancel_orders
    
    def cancel_successful(self, oid):
        if self.global_cache.get(oid):
            order = self.global_cache[oid]
            order.message["cancel"] = True
        
    def send_new_stop_order(self, order:Order):
        # self.logger.info(f"add order to global cid={order.client_id}")
        self.global_cache[order.client_id] = order
        if order.side == OrderSide.Buy:
            # self.logger.info(f"add order to stop buy, price={order.requested_price}, cid={order.client_id}")
            self.stop_order_buy_cache[order.requested_price][order.client_id] = order
        elif order.side == OrderSide.Sell:
            # self.logger.info(f"add order to stop sell, price={order.requested_price}, cid={order.client_id}")
            self.stop_order_sell_cache[order.requested_price][order.client_id] = order
        
    def on_stop_order_update(self, order: PartialOrder, lb: AlgoOrderLoadMalancer, acc: str):
        if order.xchg_status in OrderStatus.fin_status() and order.raw_data["o"]["o"] == "STOP":
            if self.global_cache.get(order.client_id) != None:
                cache_order: Order = self.global_cache[order.client_id]
                if cache_order.side == OrderSide.Buy:
                    res = self.stop_order_buy_cache[cache_order.requested_price].pop(order.client_id)
                    if res:
                        self.logger.info(f"stop order finished: {order.client_id}, {acc}")
                        lb.finished_stop_order(acc)
                    # self.logger.info(f"pop order to stop buy, price={cache_order.requested_price}, cid={order.client_id}")
                    if not self.stop_order_buy_cache.get(cache_order.requested_price):
                        self.stop_order_buy_cache.pop(cache_order.requested_price, None)
                        # self.logger.info(f"pop price level to stop buy, price={cache_order.requested_price}")
                elif cache_order.side == OrderSide.Sell:
                    res = self.stop_order_sell_cache[cache_order.requested_price].pop(order.client_id)
                    if res:
                        self.logger.info(f"stop order finished: {order.client_id}, {acc}")
                        lb.finished_stop_order(acc)
                    # self.logger.info(f"pop order to stop sell, price={cache_order.requested_price}, cid={order.client_id}")
                    if not self.stop_order_sell_cache.get(cache_order.requested_price):
                        self.stop_order_sell_cache.pop(cache_order.requested_price, None)
                        # self.logger.info(f"pop price level to stop sell, price={cache_order.requested_price}")

    def on_limit_order_update(self, order: PartialOrder):
        if order.xchg_status == OrderStatus.New and order.raw_data["o"]["o"] == "LIMIT":
            if self.global_cache.get(order.client_id) != None:
                cache_order: Order = self.global_cache[order.client_id]
                cache_order.message["cancel"] = False
                cache_order.message["start"] = order.raw_data["o"]["T"]
                # self.logger.info(f"add order to limit, price={cache_order.requested_price}, cid={cache_order.client_id}")
                self.limit_order_cache[cache_order.requested_price][order.client_id] = cache_order
                cache_order.xchg_status = order.xchg_status
   
        elif order.xchg_status in OrderStatus.fin_status() and order.raw_data["o"]["o"] == "LIMIT":
            if self.global_cache.get(order.client_id) != None:
                cache_order: Order = self.global_cache[order.client_id]
                
                
                self.inventory += order.filled_amount * Decimal("-1") if cache_order.side == OrderSide.Sell else order.filled_amount * Decimal("1")
                self.limit_order_cache[cache_order.requested_price].pop(order.client_id)
                # self.logger.info(f"pop order to limit, price={cache_order.requested_price}, cid={order.client_id}")
                if not self.limit_order_cache.get(cache_order.requested_price):
                    self.limit_order_cache.pop(cache_order.requested_price, None)
                self.global_cache.pop(order.client_id)
                # self.logger.info(f"pop order to global, price={cache_order.requested_price}, cid={order.client_id}")
    
    
    
    def failed_order(self, oid, price, acc, lb:AlgoOrderLoadMalancer):
        res = self.global_cache.pop(oid, None)
        # if res:
            # self.logger.info(f"pop order to global due to failed cid={oid}")
        res = self.limit_order_cache[price].pop(oid, None)
        # if res:
        #     self.logger.info(f"pop order to limit due to failed cid={oid}")
        res = self.stop_order_sell_cache[price].pop(oid, None)
        if res:
            self.logger.info(f"failed order cid:{oid}, {acc}")
            lb.finished_stop_order(acc)
        # if res:
        #     self.logger.info(f"pop order to stop sell due to failed cid={oid}")
        res = self.stop_order_buy_cache[price].pop(oid, None)
        if res:
            self.logger.info(f"failed order cid:{oid}, {acc}")
            lb.finished_stop_order(acc)
        # if res:
        #     self.logger.info(f"pop order to stop buy due to failed cid={oid}")

            
             
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_busd_swap.usdt_contract.na"
        self.stop_price = ...
        self.order_price = set()
        
        self.best_ask = ...
        self.best_bid = ...  
        
        self.om = CustomizedOrderManager(
            logger=self.logger, 
            start=int(self.config['strategy']['params']['start']), 
            end=int(self.config['strategy']['params']['end'])
            )
        
        self.canceling_id = set()
        
        self.activation_data = []
        
        self.sell_trade = TradeCache(logger=self.logger)
        self.buy_trade = TradeCache(logger=self.logger)
        
        self.lb = AlgoOrderLoadMalancer()
        self.lb.set_up(self.account_names)
        
        self.stats_filled_count = 0
        self.stats_count = 0
        self.send = False
        
        self.start_ts = time.time()*1e3
        self.with_order_time = 0
         
    def update_config_before_init(self):
        self.use_colo_url = True
        
    async def before_strategy_start(self):
        cfg = self.get_symbol_config(symbol_identity=self.symbol)
        self.tick = cfg["price_tick_size"]
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol, account_name=acc)
            self._account_config_[acc]["cfg_pos_mode"][self.symbol] = 1
            await self.set_account_to_uno(acc)
            
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        
        await self.cancel_all_orders()
        await self.close_all_position()
        exit()
        self.send = True
        
        self.logger.setLevel("INFO")
        
    async def on_agg_trade(self, symbol, trade: PublicTrade):
        # if self.stop_price is not Ellipsis:
        #     if trade.price >= self.stop_price:
        #         self.logger.info(f"trade trigger stop price, trade transaction time={trade.transaction_ms}")
        #         self.stop_price = ...
        if trade.side == OrderSide.Buy:
            self.buy_trade.new_trade(trade=trade)
        else:
            self.sell_trade.new_trade(trade=trade)
        
        self.buy_trade.expired_trade()
        self.sell_trade.expired_trade()
    
    async def on_bbo(self, symbol, bbo: BBODepth):
        try:       
            if Decimal(bbo.bid[0]) != self.best_bid:
                send_stop_orders, cancel_orders = self.om.on_bbo_change(side="bid", price=Decimal(bbo.bid[0]), tick=self.tick)
                for price_shift, stop_price, side in send_stop_orders:
                    self.send_order(price_shift=price_shift, stop_price=stop_price, side=side)
                for order in cancel_orders:
                    self.loop.create_task(self.cancel_excution(order))
                self.best_bid = Decimal(bbo.bid[0])
            if Decimal(bbo.ask[0]) != self.best_ask:
                send_stop_orders, cancel_orders = self.om.on_bbo_change(side="ask", price=Decimal(bbo.ask[0]), tick=self.tick)
                for price_shift, stop_price, side in send_stop_orders:
                    self.send_order(price_shift=price_shift, stop_price=stop_price, side=side)
                for order in cancel_orders:
                    self.loop.create_task(self.cancel_excution(order))
                self.best_ask = Decimal(bbo.ask[0])
        except:
            traceback.print_exc()
            
    async def cancel_excution(self, order: Order):
        async def unit_cancel_order(order: Order):
            res = await self.cancel_order(order)
            if res == True:
                self.om.cancel_successful(order.client_id)
        
        cid = order.client_id
        if cid in self.canceling_id:
            return
        
        self.canceling_id.add(cid)
    
        counter = 0
        while True:
            if self.om.global_cache.get(cid) != None:
                o = self.om.global_cache[cid]
                if o.message["cancel"]:
                    self.canceling_id.remove(cid)
                    return
            else:
                self.canceling_id.remove(cid)
                return
            counter += 1
            self.loop.create_task(unit_cancel_order(order))
            await asyncio.sleep(0.1)
            
            if counter > 1000:
                self.logger.warning(f"too many cancel orders, order={order.__dict__}")
                exit()
            
            
    async def cancel_order(self, order: Order, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.data
        except Exception as err:
            if not catch_error:
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False
        
    def send_order(self, price_shift: Decimal, stop_price: Decimal, side: str):
        if not self.send:
            return
        client_id, acc = self.send_stop_order_pretreatment(price_shift=price_shift, stop_price=stop_price, side=side)
        if not acc:
            return
        self.loop.create_task(
            self.send_stop_order_execution(price_shift=price_shift, stop_price=stop_price, side=side, client_id=client_id,acc=acc)
        )
        
    def send_stop_order_pretreatment(self, price_shift: Decimal, stop_price: Decimal, side: str):
        cfg = self.get_symbol_config(symbol_identity=self.symbol)
        tick = cfg["price_tick_size"]
        client_id = ClientIDGenerator.gen_client_id("binance")
        acc = self.lb.get_available_acc()
        if acc == None:
            return None, None
        
        self.logger.info(f"send stop order and add in load balancer cid:{client_id}, {acc}")
        self.lb.new_stop_order(acc)
        price, volume = self.format_price_volume(symbol_name=self.symbol, price=stop_price, volume=Decimal("0.001"))
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=stop_price+price_shift*tick,
            volume=volume,
            side=OrderSide.Buy if side == "BUY" else OrderSide.Sell,
            position_side=OrderPositionSide.Open,
            account_name=acc
        )
        order.message = dict()
        order.message["cancel"] = False
        order.message["start"] = None
        order.message["end"] = None
        self.om.send_new_stop_order(order)
        return client_id, acc
        
    async def send_stop_order_execution(self, price_shift: Decimal, stop_price: Decimal, client_id: str, side: str, acc: str):
        cfg = self.get_symbol_config(symbol_identity=self.symbol)
        tick = cfg["price_tick_size"]
        
        price, volume = self.format_price_volume(symbol_name=self.symbol, price=stop_price, volume=Decimal("0.001"))
        
        api = self.get_exchange_api_by_account(acc)
        
        self.logger.info(f"send stop order cid={client_id}")
        try:
            resp: ApiResponse = await api.make_request(
                market="usdt_contract",
                method="POST",
                endpoint="/fapi/v1/order",
                query=dict(
                    symbol="BTCBUSD",
                    side=side,
                    type="STOP",
                    quantity=volume,
                    price=price + price_shift * tick,
                    newClientOrderId=client_id,
                    stopPrice=price,
                    timeInForce="GTX",
                    newOrderRespType="RESULT",
                    timestamp=str(int(time.time()*1e3))),
                need_sign=True
                )
            self.logger.info(f"send order, order:{resp.json}")
        except RateLimitException:
            self.loop.create_task(self.rate_limit_handle())
            self.om.failed_order(oid=client_id, price=stop_price+price_shift*tick, acc=acc, lb=self.lb)
        except Exception:
            traceback.print_exc()
            self.logger.info(f"failed order account:{acc}")
            self.logger.info(f"algo load balancer:{self.lb.load}")
            self.om.failed_order(oid=client_id, price=stop_price+price_shift*tick, acc=acc, lb=self.lb)
    
    async def rate_limit_handle(self):
        self.send = False
        await asyncio.sleep(30)
        self.send = True
    
    async def compare(self, start, end, price, status, trade_cache: TradeCache, cid):
        await asyncio.sleep(1)
        res = trade_cache.compare(start=start, end=end, price=price)    
        if res == 1:
            self.logger.info(f"!!!!!!! order is comparable, cid={cid}, status={status}")
            self.stats_count += 1
            if status == OrderStatus.Filled:
                self.stats_filled_count += 1
    
    def get_fill_prob(self):
        if self.stats_count > 0:
            return self.stats_filled_count / self.stats_count
        return 0
    
    async def on_order(self, order: PartialOrder):
        self.logger.info(f"order cid={order.client_id}, status={order.xchg_status}, data={order.__dict__}")
        try:
            
            # if order.xchg_status == OrderStatus.New and order.raw_data["o"]["o"] == "LIMIT":
            #     self.activation_data.append(
            #         dict(
            #             price=order.raw_data["o"]["p"],
            #             quantity=order.raw_data["o"]["q"],
            #             transaction_time=order.raw_data["o"]["T"],
            #             type=order.raw_data["o"]["ot"],
            #             side=order.raw_data["o"]["S"]
            #         )
            #     )
            
            if order.client_id not in self.om.global_cache:
                return
            
            if order.xchg_status in OrderStatus.fin_status() and order.raw_data["o"]["o"] == "LIMIT":
                if self.om.global_cache.get(order.client_id) != None:
                    cache_order: Order = self.om.global_cache[order.client_id]
                    if order.extra["side"] == OrderSide.Buy:
                        trade_cache = self.sell_trade
                    else:
                        trade_cache = self.buy_trade
                    self.loop.create_task(
                        self.compare(
                            start=cache_order.message["start"], 
                            end=order.raw_data["o"]["T"], 
                            price=cache_order.requested_price, 
                            status=order.xchg_status,
                            trade_cache=trade_cache,
                            cid=order.client_id
                        )
                    )
                    self.with_order_time += (order.raw_data["o"]["T"] - cache_order.message["start"])
            self.om.on_limit_order_update(order=order)
            self.om.on_stop_order_update(order=order,lb=self.lb, acc=order.account_name)
        except:
            traceback.print_exc()
            
    async def cancel_all_orders(self):
        try:
            cfg = self.get_symbol_config(symbol_identity=self.symbol)
            for acc in self.account_names:
                api = self.get_exchange_api_by_account(acc)
                await api.flash_cancel_orders(cfg)
                await asyncio.sleep(1)
        except:
            pass
        
    async def close_all_position(self):
        try:
            for _ in range(2):
                for acc in self.account_names:
                    api = self.get_exchange_api_by_account(acc)
                    cur_pos = (await api.contract_position(self.get_symbol_config(self.symbol))).data
                    pos = cur_pos["long_qty"] - cur_pos["short_qty"]
                    if pos == Decimal(0):
                        continue
                    await self.make_order(
                        symbol_name=self.symbol,
                        price=self.best_ask,
                        volume=abs(pos),
                        side = OrderSide.Buy if pos < 0 else OrderSide.Sell,
                        position_side=OrderPositionSide.Close,
                        order_type= OrderType.Market,
                        account_name=acc,
                    )
                    await asyncio.sleep(0.5)
        except:
            traceback.print_exc()
        
    async def log_save_to_local(self):
        await asyncio.sleep(60 * 30)
        data = pd.DataFrame(self.activation_data)
        data.to_csv(f"./data/activation_data.csv")
        
    async def set_account_to_uno(self, acc):
        api = self.get_exchange_api_by_account(acc)
        self.logger.info(f"try to set {acc} to uno mode")
        try:
            await api.make_request(
                market="usdt_contract",
                    method="POST",
                    endpoint="/fapi/v1/positionSide/dual",
                    query=dict(
                        dualSidePosition="false",
                    ),
                    need_sign=True
            )
        except Exception as err:
            self.logger.info(f"position mode set err:{err}")
            
    
    
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await self.redis_set_cache(
                {
                    "fill prob":self.get_fill_prob(),
                    "with order percentage": self.with_order_time / (time.time()*1e3 - self.start_ts)
                }
            )
            
            await self.close_all_position()
        # await self.log_save_to_local()
        # await self.cancel_all_orders()
            
        
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()