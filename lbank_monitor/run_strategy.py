from strategy_base.base import CommonStrategy
import asyncio
from atom.helpers import send_ding_talk
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2

import traceback
from decimal import Decimal
from atom.helpers import decrypt_credential
from atom.exchange_api.factory import build_rest_api
import ujson as json
from collections import defaultdict, deque
import time

INITIAL_BALANCE = 4600000 + 3600000 + 600000

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbols = [
            "binance.btc_usdt_swap.usdt_contract.na",
            "binance.eth_usdt_swap.usdt_contract.na",
            "binance.ape_usdt_swap.usdt_contract.na",
            "binance.gmt_usdt_swap.usdt_contract.na",
            "binance.xrp_usdt_swap.usdt_contract.na",
            "binance.matic_usdt_swap.usdt_contract.na",
            "binance.ada_usdt_swap.usdt_contract.na",
            "binance.sol_usdt_swap.usdt_contract.na",
            "binance.gala_usdt_swap.usdt_contract.na",
            "binance.etc_usdt_swap.usdt_contract.na",
            "binance.axs_usdt_swap.usdt_contract.na",
            "binance.doge_usdt_swap.usdt_contract.na",
            "binance.atom_usdt_swap.usdt_contract.na",
            "binance.near_usdt_swap.usdt_contract.na",
            "binance.sand_usdt_swap.usdt_contract.na",
            "binance.apt_usdt_swap.usdt_contract.na",
        ]
        self.mp = dict()
        
        self.max_pnl = -INITIAL_BALANCE
        
        self.refresh_max_pnl = self.config['strategy']['params']['refresh_max_pnl']
        
        
    async def before_strategy_start(self):
        for symbol in self.symbols:
            self.direct_subscribe_orderbook(symbol_name=symbol, is_incr_depth=True)
        
        
    def _build_direct_sub_params_(self, symbol):
        if symbol["exchange_name"] == "binance":
            use_colo_url = False
        else:
            use_colo_url = self.use_colo_url
        return dict(
            exchange_name=symbol["exchange_name"],
            account=None,
            market=symbol["market"],
            private_mode=False,
            use_colo_url=use_colo_url,
            use_raw_stream=self.use_raw_stream
        )
    
    async def _build_rest_api_(self):
        apis_account = dict()
        apis_exchange = defaultdict(list)
        for idx, item in enumerate(self.config['exchanges']):
            account_name = item["account"]["account_name"]
            item['account'].update(json.loads(decrypt_credential(item['account'].pop('epass'))))
            if item['exchange_name'] == "binance":
                use_colo_url = False
            else:
                use_colo_url = self.use_colo_url
            item["params"]["use_colo_url"] = self.use_colo_url
            _api = await build_rest_api(
                item['exchange_name'], item['account'], async_init=True, **item['params'], **self.config['system']['logger']
            )
            apis_account[account_name] = _api
            apis_exchange[item['exchange_name']].append(_api)
            setattr(self, f"exchange_{idx + 1}", _api)
            self.cache_enable_switch[account_name] = item['params'].get('cache_enabled', True)
            account_cfg_margin_mode = (await self.cache_redis.handler.hgetall(f"cache:account_cfg:{account_name}:margin_mode")) or dict()
            account_cfg_pos_mode = (await self.cache_redis.handler.hgetall(f"cache:account_cfg:{account_name}:pos_mode")) or dict()
            account_cfg_leverage = (await self.cache_redis.handler.hgetall(f"cache:account_cfg:{account_name}:leverage")) or dict()
            for k in ("cfg_margin_mode", "cfg_pos_mode", "cfg_leverage"):
                item[k] = locals().get(f"account_{k}", dict())
            self._account_config_[account_name] = item
        return apis_account, apis_exchange
            
    def update_config_before_init(self):
        self.use_colo_url = True
            
    async def on_orderbook(self, symbol, orderbook):
        ex, contract = symbol.split(".")
        try:
            ap = orderbook["asks"][0][0]
            bp = orderbook["bids"][0][0]
            self.mp[contract] = (ap + bp) / 2
        except:
            self.logger.critical(traceback.format_exc())
    
    async def push_influx_data(self, measurement, tag, fields):
        dt = {
            "timestamp": int(time.time() * 1e3),
            "measurement": measurement,
            "tag": tag,
            "fields": fields
        }
        await self.cache_redis.handler.lpush(f"cache:influx_queue:db_strategy_metric", json.dumps(dt))
        
    async def query_position(self):
        pos = dict()
        total_pos = dict()
        unrealized = dict()
        for acc in self.account_names:
            if acc.startswith("binance"):
                continue
            api = self.get_exchange_api_by_account(acc)
            cur_position = (await api.contract_position_all(market="swap", sub_market="na"))
            self.logger.info(f"position info:{cur_position}") 
            for contract in cur_position.data:
                spos = cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"]
                # self.logger.info(f"{acc}.{contract} position: {spos}, unrealized: {cur_position}")
                if self.mp.get(contract) != None:
                    p = self.mp.get(contract)
                    if pos.get(contract) != None:
                        pos[contract] += spos * p
                    else:
                        pos[contract] = spos * p
                else:
                    self.logger.info(f"{contract} no price")
            
            for data in cur_position.json["data"]:
                xchg_pair = data["symbol"]
                if unrealized.get(xchg_pair) != None:
                    unrealized[xchg_pair] += float(data["unrealizedProfit"])
                else:
                    unrealized[xchg_pair] = float(data["unrealizedProfit"])
                    
        binance_position = (await self.get_exchange_api(exchange_name="binance")[0].contract_position_all(market="usdt_contract", sub_market="na"))
        for contract, spos_amt in pos.items():
            if self.mp.get(contract) != None:
                p = self.mp.get(contract)
                if binance_position.data.get(contract) != None:
                    binance_spos = binance_position.data[contract]["long_qty"] - binance_position.data[contract]["short_qty"]
                    total_pos[contract] = spos_amt + binance_spos * p
                else:
                    total_pos[contract] = spos_amt
                
        tot = 0
        for single_pos in total_pos.values():
            tot += abs(single_pos)
        
        if tot > Decimal(3000000):
            self.loop.create_task(
                send_ding_talk(title="LBANK", 
                                message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: total position is {tot} usdt, strategy:{self.strategy_name}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )
        self.logger.info(f"tot pos: {tot}")
            
        tot = 0
        for single_pos in pos.values():
            tot += abs(single_pos)
            
        if tot > Decimal(3000000):
            self.loop.create_task(
                send_ding_talk(title="LBANK", 
                                message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: total lbank position is {tot} usdt, strategy:{self.strategy_name}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )
            
        self.logger.info(f"tot lbank pos: {tot}")
        
        self.logger.info("lbank position")
        for symbol, symbol_pos in sorted(pos.items(), key=lambda x: abs(x[1]), reverse=True):
            self.logger.info(f"lbank {symbol}: {symbol_pos}")
        self.logger.info("............")
        
        self.logger.info("total position")
        for symbol, symbol_pos in sorted(total_pos.items(), key=lambda x: abs(x[1]), reverse=True):
            self.logger.info(f" total {symbol}: {symbol_pos}")
        self.logger.info("............")
        
        self.logger.info("lbank unrealized profit")
        for xchg_pair, pnl in unrealized.items():
            self.logger.info(f"{xchg_pair} lbank unrealized profit:{pnl}")
        self.logger.info("............")
    
    async def query_balance(self):
        tot_balance = 0
        unrealized = 0
        frozen_margin = 0
        lbank_balance = 0
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            if acc.startswith("binance"):
                curr_balance = (await api.account_balance_all(market="usdt_contract", sub_market="na")).data["usdt"]["all"]
            elif acc.startswith("lbank"):
                resp = await api.account_balance_all(market="swap", sub_market="na")
                curr_balance = resp.data["usdt"]["all"]
                curr_unrealized = float(resp.json["data"]["unrealizedProfit"])
                frozen_margin += float(resp.json["data"]["frozenMargin"])
                # self.logger.info(f"curr_unrealized {acc}: {curr_unrealized}")
                unrealized += curr_unrealized
                lbank_balance +=curr_balance
            tot_balance += curr_balance
        
        total_pnl = float(tot_balance) -INITIAL_BALANCE + float(unrealized)
        self.logger.info(f"realized_pnl={float(tot_balance)-INITIAL_BALANCE}")
        self.logger.info(f"unrealized_pnl={float(unrealized)}")
        self.logger.info(f"frozen_margin={float(frozen_margin)}")
        self.logger.info(f"total_pnl={total_pnl}")
        self.logger.info("............")
        
        self.logger.info(f"lbank_realized_pnl={float(lbank_balance)-(INITIAL_BALANCE - 500000)}")
        self.logger.info(f"lbank_total_pnl={float(lbank_balance) -(INITIAL_BALANCE - 500000) + float(unrealized)}")
        self.logger.info("............")
        
        self.max_pnl = max(self.max_pnl, total_pnl)
        self.loop.create_task(self.cache_redis.handler.set("cache:hf_lbank:max_drawdown",json.dumps({"max_drawdown":self.max_pnl - total_pnl})))
        self.loop.create_task(self.cache_redis.handler.set("cache:hf_lbank:max_pnl",json.dumps({"max_pnl":self.max_pnl})))
        self.logger.info(f"current_max_total_pnl={self.max_pnl}")
        self.logger.info(f"drawdown now={self.max_pnl - total_pnl}")
        self.logger.info("............")
        
        if self.max_pnl - total_pnl > 100000:
            self.loop.create_task(
                send_ding_talk(title="LBANK", 
                                message=f"[{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}]warning: drawdown is {self.max_pnl - total_pnl}, strategy:{self.strategy_name}",
                                token="c28dc75c436ae46d9a9798e06f1f8cc8ef146746f2af1dfac7a6588a16bea2b3", 
                                secret="SEC1a8d4a3dba633d0f7b57cff44a42b9f19c43e7da69dd5e65d015d2352d4c7583")
            )
        
        
        self.loop.create_task(
            self.push_influx_data(
                measurement="tt",
                tag={"sn": self.strategy_name},
                fields={
                    "total_pnl": float(tot_balance) -INITIAL_BALANCE + float(unrealized),
                    "lbank_total_pnl": float(lbank_balance) -(INITIAL_BALANCE - 500000) + float(unrealized)
                }
            )
        )
            
    async def strategy_core(self):
        res = await self.cache_redis.handler.get("cache:hf_lbank:max_pnl")
        if res and not self.refresh_max_pnl:
            max_pnl = json.loads(res).get("max_pnl")
            if max_pnl != None:
                self.max_pnl = float(max_pnl)
        self.logger.critical(f"initial max pnl:{self.max_pnl}")
        while True:
            await asyncio.sleep(10)
            
            try:
                await self.query_position()
                await self.query_balance()
            except:
                self.logger.critical(traceback.format_exc())
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    
                    
                