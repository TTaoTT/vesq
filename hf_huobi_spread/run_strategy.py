
from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections

from decimal import Decimal 
import asyncio
import time
import numpy as np

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])

        self.symbol_binance = dict()
        self.symbol_huobi = dict()
        self.ap_binance = dict()
        self.bp_binance = dict()
        self.ap_huobi = dict()
        self.bp_huobi = dict()
        self.ap_cache = dict()
        self.bp_cache = dict()
        self.price = dict()

        for key in self.params["symbol"]:
            symbol = self.params["symbol"][key]
            self.symbol_binance[symbol] = f"binance.{symbol}_usdt_swap.usdt_contract.na"
            self.symbol_huobi[symbol] = f"huobi.{symbol}_usdt_swap.usdt_swap.cross"
            self.ap_binance[symbol] = Decimal(0)
            self.bp_binance[symbol] = Decimal(0)
            self.ap_huobi[symbol] = Decimal(0)
            self.bp_huobi[symbol] = Decimal(0)
            self.ap_cache[symbol] = collections.deque(maxlen=1000)
            self.bp_cache[symbol] = collections.deque(maxlen=1000)

            self.price[symbol] = collections.deque(maxlen=1000)


    async def before_strategy_start(self):
        for key in self.symbol_binance:
            self.direct_subscribe_orderbook(symbol_name=self.symbol_huobi[key])
            await self.direct_subscribe_bbo(symbol_name=self.symbol_binance[key])

    async def on_bbo(self, symbol, bbo):
        s = symbol.split(".")[1].split("_")[0]
        if Decimal(bbo.ask[0]) == self.ap_binance[s] or Decimal(bbo.bid[0]) == self.bp_binance[s]:
            return

        self.ap_binance[s] = Decimal(bbo.ask[0])
        self.bp_binance[s] = Decimal(bbo.bid[0])

        

    async def on_orderbook(self, symbol, orderbook):
        s = symbol.split(".")[1].split("_")[0]
        
        self.ap_huobi[s] = orderbook["asks"][0][0]
        self.bp_huobi[s] = orderbook["bids"][0][0]

        self.bp_cache[s].append(self.bp_binance[s]-self.bp_huobi[s])
        self.ap_cache[s].append(self.ap_huobi[s]-self.ap_binance[s])
        self.price[s].append((self.ap_huobi[s]+self.bp_huobi[s])/Decimal(2))

    async def strategy_core(self):
        await asyncio.sleep(1)
        await asyncio.gather(
            self.set_cache()
        )

    async def set_cache(self):
        while True:
            d = dict()
            for key in self.symbol_binance:
                temp_d = dict()
                temp_d["bp"] = np.median(self.bp_cache[key]) / np.mean(self.price[key])
                temp_d["ap"] = np.median(self.ap_cache[key]) / np.mean(self.price[key])
                d[key] = temp_d
            await self.redis_set_cache(d)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()