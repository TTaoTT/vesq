import asyncio
from decimal import Decimal
import time
from wsgiref.util import request_uri 
import pandas as pd
from asyncio.queues import Queue
import os

from requests import request

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

os.environ["WOO_APPLICATION_IDS"] = "2789fdbe-63af-4d50-af35-62b703ac67c2"

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.qty = Decimal(self.config['strategy']['params']['qty'])
        self.gap = Decimal(self.config['strategy']['params']['gap'])
        self.gap_min = Decimal(self.config['strategy']['params']['gap_min'])
        symbol = self.config['strategy']['params']['symbol']
        self.symbol_crypto = f"crypto.{symbol}_usdt.spot.na"
        self.symbol_woo = f"woo.{symbol}_usdt.spot.na"
        self.maker_order_cache = dict()
        self.taker_order_cache = dict()
        
        self.send_order_buy_queue = Queue()
        self.send_order_sell_queue = Queue()
        self.cancel_order_buy_queue = Queue()
        self.cancel_order_sell_queue = Queue()
        
        self.hedge_queue = Queue()
        self.hedge = 0
        
        self.pos = 0
        
        self.send_order_buy_in_action = False
        self.send_order_sell_in_action = False
        
        self.unknown_order_dict = {}
        
        self.canceling_id= set()
        
        self.buy_amount = 0
        self.sell_amount = 0
        self.buy_qty = 0
        self.sell_qty = 0
        
        self.crypto_best_ask = None
        self.crypto_best_bid = None
        
        
    async def before_strategy_start(self):
        self.direct_subscribe_order_update(symbol_name=self.symbol_crypto)
        self.direct_subscribe_order_update(symbol_name=self.symbol_woo)
        self.direct_subscribe_orderbook(symbol_name=self.symbol_woo)
        self.direct_subscribe_orderbook(symbol_name=self.symbol_crypto)
        
        self.logger.setLevel("WARNING")
        
        crypto_config = self.get_symbol_config(self.symbol_crypto)
        self.crypto_qty_tick = crypto_config["qty_tick_size"]
        self.logger.warning(f"crypto qty tick: {self.crypto_qty_tick}, min_qty_value: {crypto_config['min_quantity_val']}, min_notional_val: {crypto_config['min_notional_val']}")
        
        woo_config = self.get_symbol_config(self.symbol_woo)
        self.woo_qty_tick = woo_config["qty_tick_size"]
        self.logger.warning(f"woo qty tick: {self.woo_qty_tick}, min_qty_value: {woo_config['min_quantity_val']}, min_notional_val: {woo_config['min_notional_val']}")
        
        api = self.get_exchange_api("crypto")[0]
        cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol_crypto))).data
        self.crypto_btc_all = cur_balance["btc"]["all"]
        self.crypto_btc_available = cur_balance["btc"]["available"]
        self.crypto_usdt_all = cur_balance["usdt"]["all"]
        self.crypto_usdt_available = cur_balance["usdt"]["available"]
        self.logger.warning(f"balance: {cur_balance}")
        self.logger.warning(f"{self.crypto_btc_all}_{self.crypto_btc_available}_{self.crypto_usdt_all}_{self.crypto_usdt_available}")
        
    async def on_order(self, order):
        
        if order.xchg_id not in list(self.maker_order_cache) and order.xchg_id not in list(self.taker_order_cache):
            self.unknown_order_dict[order.xchg_id] = order
            # self.logger.warning(f"find unknow order {order.xchg_id}")
            return
        
        if order.xchg_id in list(self.maker_order_cache):
            if order.filled_amount < self.maker_order_cache[order.xchg_id].filled_amount:
                return
            amount_changed = order.filled_amount - self.maker_order_cache[order.xchg_id].filled_amount
            self.maker_order_cache[order.xchg_id].filled_amount = order.filled_amount
            self.maker_order_cache[order.xchg_id].avg_filled_price = order.avg_filled_price
            self.maker_order_cache[order.xchg_id].xchg_status = order.xchg_status
            if amount_changed > self.crypto_qty_tick:
                self.logger.warning(f"order has filled, {amount_changed}, {self.maker_order_cache[order.xchg_id].side}, {order.avg_filled_price}")
                if self.maker_order_cache[order.xchg_id].side == OrderSide.Buy:
                    self.hedge -= amount_changed
                else:
                    self.hedge += amount_changed
                self.hedge_queue.put_nowait(1)
                
            if order.xchg_status in OrderStatus.fin_status():
                if self.maker_order_cache[order.xchg_id].side == OrderSide.Buy:
                    self.pos += order.filled_amount
                    self.buy_amount += order.filled_amount * order.avg_filled_price
                    self.buy_qty += order.filled_amount
                    self.crypto_usdt_available -= order.filled_amount * order.avg_filled_price
                    self.crypto_btc_available += order.filled_amount
                else:
                    self.pos -= order.filled_amount
                    self.sell_amount += order.filled_amount * order.avg_filled_price
                    self.sell_qty += order.filled_amount
                    self.crypto_usdt_available += order.filled_amount * order.avg_filled_price
                    self.crypto_btc_available -= order.filled_amount
                self.maker_order_cache.pop(order.xchg_id)
                if order.xchg_id in self.unknown_order_dict:
                    self.unknown_order_dict.pop(order.xchg_id)
                    
        elif order.xchg_id in list(self.taker_order_cache):
            if order.xchg_status in OrderStatus.fin_status():
                self.logger.warning(f"hedge at: {order.filled_amount}, {self.taker_order_cache[order.xchg_id].side}, {order.avg_filled_price}")
                if self.taker_order_cache[order.xchg_id].side == OrderSide.Buy:
                    self.pos += order.filled_amount
                    self.buy_amount += order.filled_amount * order.avg_filled_price
                    self.buy_qty += order.filled_amount
                else:
                    self.pos -= order.filled_amount
                    self.sell_amount += order.filled_amount * order.avg_filled_price
                    self.sell_qty += order.filled_amount
                self.taker_order_cache.pop(order.xchg_id)
                if order.xchg_id in self.unknown_order_dict:
                    self.unknown_order_dict.pop(order.xchg_id)
            
    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.maker_order_cache or xchg_id in self.taker_order_cache:
                    # self.logger.warning(f"unkonw {xchg_id} order rest back")
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(partial_order) for xchg_id, partial_order in now_know_order.items()]
            )
            
    async def on_orderbook(self, symbol, orderbook):
        if symbol == "crypto.btc_usdt":
            self.crypto_best_ask = orderbook["asks"][0][0]
            self.crypto_best_bid = orderbook["bids"][0][0]
            return
        
        aoid = self.has_open_ask_order()
        boid = self.has_open_bid_order()
        
        a = orderbook["asks"]
        b = orderbook["bids"]
        try:
            if aoid != False:
                q_sum = 0
                for p,q in a:
                    if p < self.ask_price:
                        q_sum += q
                    else:
                        break
                # self.logger.warning(f"ask_{q_sum}_{p}_{self.ask_price}")
                if q_sum < self.qty or p*Decimal(1+self.gap_min) > self.ask_price:
                    self.loop.create_task(self.cancel(self.maker_order_cache[aoid]))
                    self.maker_order_cache[aoid].message["stop_ts"] = 0
                    
            if boid != False:
                q_sum = 0
                for p,q in b:
                    if p > self.bid_price:
                        q_sum += q
                    else:
                        break
                # self.logger.warning(f"bid_{q_sum}_{p}_{self.bid_price}")
                if q_sum < self.qty or p*Decimal(1-self.gap_min) < self.bid_price:
                    self.loop.create_task(self.cancel(self.maker_order_cache[boid]))
                    self.maker_order_cache[boid].message["stop_ts"] = 0
        
            if self.send_order_sell_queue.empty() and not self.send_order_sell_in_action and self.crypto_best_ask:
                if not aoid:
                    q_sum = 0
                    for p,q in a:
                        q_sum += q
                        if q_sum >= self.qty*10 and self.crypto_btc_available > self.qty:
                            self.send_order_sell_queue.put_nowait(p*Decimal(1+self.gap))
                            
            if self.send_order_buy_queue.empty() and not self.send_order_buy_in_action and self.crypto_best_bid:
                if not boid:
                    q_sum = 0
                    for p,q in b:
                        q_sum += q
                        if q_sum >= self.qty*10 and self.crypto_usdt_available > p*Decimal(1-self.gap)*self.qty:
                            self.send_order_buy_queue.put_nowait(p*Decimal(1-self.gap))
        
            best_ask = a[0][0]
            best_bid = b[0][0]
            self.mp = (best_ask + best_bid) / 2
        except Exception as err:
            self.logger.warning(f"handle orderbook err: {err}")
            
    async def send_maker_order_buy_action(self):
        while True:
            p = await self.send_order_buy_queue.get()
            self.send_order_buy_in_action = True
            p = min(p, self.crypto_best_bid)
            if self.has_open_bid_order() == False:
                # self.logger.warning(f"sending buy order at: {p}")
                try:
                    order = await self.make_order(self.symbol_crypto,p,self.qty,OrderSide.Buy,OrderPositionSide.Open,OrderType.PostOnly)
                    order.message = dict()
                    order.message["stop_ts"] = 5
                    self.maker_order_cache[order.xchg_id] = order
                except Exception as err:
                    self.logger.warning(f"send maker order buy err: {err}")
            self.send_order_buy_in_action = False
            
    async def send_maker_order_sell_action(self):
        while True:
            p = await self.send_order_sell_queue.get()
            self.send_order_sell_in_action = True
            p = max(p, self.crypto_best_ask)
            if self.has_open_ask_order() == False:
                # self.logger.warning(f"sending sell order at: {p}")
                try:
                    order = await self.make_order(self.symbol_crypto,p,self.qty,OrderSide.Sell,OrderPositionSide.Open,OrderType.PostOnly)
                    order.message = dict()
                    order.message["stop_ts"] = 5
                    self.maker_order_cache[order.xchg_id] = order
                except Exception as err:
                    self.logger.warning(f"send maker order sell err: {err}")
            self.send_order_sell_in_action = False
    
    async def hedge_position(self):
        while True:
            await self.hedge_queue.get()
            self.logger.warning("hedging now")
            if abs(self.hedge) > self.woo_qty_tick:
                resquest_qty = abs(self.hedge)
                side = OrderSide.Buy if self.hedge > 0 else OrderSide.Sell
                p = self.mp * Decimal(1.04) if self.hedge > 0 else self.mp * Decimal(0.96)
                try:
                    order = await self.make_order(self.symbol_woo,p,resquest_qty,side,OrderPositionSide.Open,OrderType.Limit)
                    order.message = dict()
                    order.message["stop_ts"] = 1
                    self.taker_order_cache[order.xchg_id] = order
                    self.hedge = self.hedge - resquest_qty if side == OrderSide.Buy else self.hedge + resquest_qty
                except ParameterError as err:
                    self.logger.warning(f"hedge position ParameterError err: {err}")
                except Exception as err:
                    self.logger.warning(f"hedge position err: {err}")
                    self.hedge_queue.put_nowait(1)
                    
    async def cancel(self,order):
        if order.xchg_id in self.canceling_id:
            return
        self.canceling_id.add(order.xchg_id)
        try:
            await self.cancel_order(order)
        except Exception as err:
            self.logger.warning(f"cancel order err: {err}")
        self.canceling_id.remove(order.xchg_id)
    
    async def cancel_order_action(self):
        async def cancel_order(order):
            if order.xchg_id in self.canceling_id:
                return
            self.canceling_id.add(order.xchg_id)
            try:
                await self.cancel_order(order)
            except Exception as err:
                self.logger.warning(f"cancel order err: {err}")
            self.canceling_id.remove(order.xchg_id)
            
        while True:
            await asyncio.sleep(0.001)
            for order in list(self.maker_order_cache.values()):
                if time.time()*1e3 - order.create_ms > order.message["stop_ts"]*1e3:
                    self.loop.create_task(cancel_order(order))
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            res = await api.order_match_result(self.get_symbol_config(order.symbol_id),order.xchg_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order
                    
    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + 1)*1e3:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.maker_order_cache.get(oid) is not None:
                        if not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                    if self.taker_order_cache.get(oid) is not None:
                        if not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order(order_new)
                    
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.maker_order_cache.items()]
            )
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.taker_order_cache.items()]
            )
            
    def has_open_ask_order(self):
        for o in list(self.maker_order_cache.values()):
            if o.side == OrderSide.Sell:
                self.ask_price = o.requested_price
                return o.xchg_id
        return False
    
    def has_open_bid_order(self):
        for o in list(self.maker_order_cache.values()):
            if o.side == OrderSide.Buy:
                self.bid_price = o.requested_price
                return o.xchg_id
        return False
    
    async def set_cache(self):
        while True:
            await asyncio.sleep(30)
            await self.redis_set_cache(
                {
                    "gap":self.sell_amount/self.sell_qty-self.buy_amount/self.buy_qty if self.buy_qty and self.sell_qty else None,
                    "vol":self.sell_amount+self.buy_amount
                    }
                )
        
        
    async def strategy_core(self):
        while True:
            await asyncio.sleep(10)
            await asyncio.gather(
                self.check_unknown_order(),
                self.send_maker_order_buy_action(),
                self.send_maker_order_sell_action(),
                self.hedge_position(),
                self.cancel_order_action(),
                self.reset_missing_order_action(),
                self.set_cache()
            )
        
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()