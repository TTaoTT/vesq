from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
import collections

from decimal import Decimal 
import asyncio
from asyncio.queues import Queue
import time
import numpy as np

# v2: order cache using clinet_id, other same as v1_1


class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)

    async def make_order(
            self,
            symbol_name,
            price: Decimal,
            volume: Decimal,
            side: OrderSide,
            position_side: OrderPositionSide,
            order_type: OrderType,
            client_id: str,
            tag='AH', extra_info='', account_name=None, auto_format=True, **kwargs):
        symbol = self.get_symbol_config(symbol_name)
        side, order_type, position_side = OrderSide(side), OrderType(order_type), OrderPositionSide(position_side)
        if auto_format is False:
            p, v = price, volume
        else:
            p, v = self.format_price_volume(symbol_name, price, volume)
        log_string = f"{symbol_name} p={p} v={v} p0={price} v0={volume} s={side.name} t={order_type.name} pos_side={position_side.name} tag={tag} extra_info={extra_info} "
        try:
            response = await self._make_order_(symbol, str(p), str(v), side, position_side, order_type, client_id, tag, extra_info, account_name, **kwargs)
            if tag != "HB-ORDER":
                self.logger.info(f"[make order ok] oid={response.data.xchg_id} elapsed={response.elapsed}ms. params: {log_string}")
            return response.data
        except ExchangeRestApiError as err:
            self.logger.error(f"[make order err] {log_string} {err}")
            raise
        except Exception as _err:
            self.logger.error(f"[make order err] {log_string} {_err!r}")
            raise

    async def _make_order_(self, symbol_config, price: str,
                           volume: str,
                           side: OrderSide,
                           position_side: OrderPositionSide,
                           order_type: OrderType,
                           client_id: str,
                           tag, extra_info, account_name, **kwargs):
        if account_name:
            api = self.get_exchange_api_by_account(account_name)
        else:
            api = self.get_exchange_api(symbol_config['exchange_name'])[0]
        response = await api.make_order(symbol=symbol_config, price=price, quantity=volume, side=side, position_side=position_side, order_type=order_type, client_id=client_id,**kwargs)
        response.data.strategy_name = self.config['strategy']['name']
        response.data.tag = tag
        response.data.extra_info = extra_info
        return response

    async def cancel_order(self, order: Order):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            r = await api.cancel_order(symbol=symbol, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.xchg_id} {r.data}")
            return r.data
        except (OrderAlreadyCompletedError, OrderNotFoundError) as e:
            return True
        except ExchangeTemporaryError as e:
            self.logger.error(f"[cancel order error] [{e}] {order.xchg_id}, redirect False")
            return False
        except ExchangeConnectorException as _err:
            self.logger.error(f"[cancel order error] {_err} {order.xchg_id}, raise unknown api error")
            raise
        except Exception as err:
            self.logger.error(f"[cancel order error] {err!r} {order.xchg_id}, redirect False")
            return False