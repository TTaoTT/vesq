
import traceback
from strategy_base.base import CommonStrategy
from decimal import Decimal
import asyncio
import ujson as json
from typing import Set, AnyStr, Union, Dict, List
import time
from atom.model import *
from atom.model.order import Order as AtomOrder
from atom.model.depth import Depth as AtomDepth
from atom.helpers import ClientIDGenerator

from xex_mm.utils.base import OrderManager, MakerTaker, Direction
from xex_mm.utils.configs import price_precision, qty_precision
from xex_mm.utils.contract_mapping import ContractMapper

import math

class TransOrder:
    def __init__(self, side: Direction, p: float, q: float, iid: str, floor: float, delta: float = 0, life: int = 100,
                 maker_taker=MakerTaker.undefined, sent_ts=None, cancel=False, message=None, query=0,symbol_id=None,
                 account_name=None, tag=None, xchg_id=None, client_id=None, cancel_id=None, query_id=None, symbol=None) -> None:
        self.side = side
        self.price = p
        self.quantity = q
        self.iid = iid
        self.life = life
        self.floor = floor
        self.delta = delta
        
        self.sent_ts = sent_ts
        self.cancel = cancel
        self.message = message
        self.query = query
        self.maker_taker = maker_taker
        self.cancel_id = cancel_id
        self.query_id = query_id
        self.symbol = symbol

        # for on board update
        self.symbol_id = symbol_id
        self.account_name = account_name
        self.tag = tag
        self.xchg_id = xchg_id
        self.client_id = client_id

    def update(self, order: AtomOrder):
        self.symbol_id = order.symbol_id
        self.account_name = order.account_name
        self.tag = order.tag
        self.xchg_id = order.xchg_id
        self.client_id = order.client_id
        
    def generate_symbol(self, contract_mapper: ContractMapper):
        # only for binance spot and usdt_contract for now.
        if self.iid.startswith("binance"):
            if contract_mapper.is_proxy_iid(self.iid):
                symbol = contract_mapper.proxy_spot_iid_to_spot_iid(self.iid)
                self.symbol = f"{symbol}.spot.na"
            else:
                self.symbol = f"{self.iid}.usdt_contract.na"
        else:
            raise RuntimeError()
        
    def to_dict(self):
        return dict(
            side=self.side,
            price=self.price,
            quantity=self.quantity,
            iid=self.iid,
            life=self.life,
            floor=self.floor,
            delta=self.delta,
            sent_ts=self.sent_ts,
            cancel=self.cancel,
            message=self.message,
            query=self.query,
            maker_taker=self.maker_taker,
            symbol_id=self.symbol_id,
            account_name=self.account_name,
            tag=self.tag,
            xchg_id=self.xchg_id,
            client_id=self.client_id,
            cancel_id=self.cancel_id,
            query_id=self.query_id,
            symbol=self.symbol,
        )

    @classmethod
    def from_dict(cls, payload):
        return cls(
            side=payload["side"], 
            p=payload["price"],
            q=payload["quantity"],
            iid=payload["iid"],
            floor=payload["floor"],
            delta=payload["delta"],
            life=payload["life"],
            sent_ts = payload["sent_ts"],
            cancel = payload["cancel"],
            message = payload["message"],
            query = payload["query"],
            maker_taker = payload["maker_taker"],
            symbol_id = payload["symbol_id"],
            account_name = payload["account_name"],
            tag = payload["tag"],
            xchg_id = payload["xchg_id"],
            client_id = payload["client_id"],
            cancel_id = payload["cancel_id"],
            query_id = payload["query_id"],
            symbol = payload["symbol"],
            )
        
    def clone(self):
        return self.__class__(
            side=self.side,
            p=self.price,
            q=self.quantity,
            iid=self.iid,
            life=self.life,
            floor=self.floor,
            delta=self.delta,
            sent_ts=self.sent_ts,
            cancel=self.cancel,
            message=self.message,
            query=self.query,
            maker_taker=self.maker_taker,
            symbol_id=self.symbol_id,
            account_name=self.account_name,
            tag=self.tag,
            xchg_id=self.xchg_id,
            client_id=self.client_id,
            cancel_id=self.cancel_id,
            query_id=self.query_id,
            symbol=self.symbol,
        )


def round_decimals_up(number:float, decimals:int=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor

def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbol = "binance.btc_usdt.spot.na"
        # self.symbol = "binance.btc_busd_swap.usdt_contract.na"

        self._order_manager = OrderManager()
        self.canceling_id = set()
        self.quering_id = set()
        
        self._contract_mapper = ContractMapper()
        
    async def before_strategy_start(self):
        for acc in self.account_names:
            self.direct_subscribe_order_update(symbol_name=self.symbol,account_name=acc)
            self.direct_subscribe_orderbook(symbol_name=self.symbol, is_incr_depth=True, depth_min_v=0)
            
        self.loop.create_task(
            self.subscribe_to_channel(
                ["mq:hf_signal:send_response"],
                self.process_send_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                ["mq:hf_signal:cancel_response"],
                self.process_cancel_response
                )
            )
        self.loop.create_task(
            self.subscribe_to_channel(
                ["mq:hf_signal:query_response"],
                self.process_query_response
                )
            )
        self.send_response = dict()
        self.cancel_response = dict()
        self.query_response = dict()
    
    # base operation, send, cancel, query with load balancer
    async def base_send_order(self, order: TransOrder) -> Union[AtomOrder, None]:
        new_order = order.to_dict()
        trader = "hf_trader_test"
        try:
            self.logger.info(f"sending order, {order.client_id}")
            self.send_response[order.client_id] = asyncio.Future()
            await self.publish_to_channel(
                "mq:hf_signal:send_request", 
                {
                    "order": new_order, 
                    "strategy_name": self.strategy_name,
                    "trader": trader
                    }
                )
            resp, headers = await self.send_response[order.client_id]
            if headers:
                self.logger.info(f"header:{headers}")
            if resp:
                self.logger.info(f"send order res, {resp.__dict__}")
                order.xchg_id = resp.xchg_id
            return resp
        except Exception as err:
            self.logger.critical(f"base send order err:{err}")
            return None
        
    async def handle_cancel_order(self, order: TransOrder, order_manager: OrderManager):
        try:
            async def unit_cancel_order(order: TransOrder):
                trader = "hf_trader_test"
                ex, _,_,_ = order.symbol.split(".")
                cancel_id = ClientIDGenerator.gen_client_id(exchange_name=ex)
                order.cancel_id = cancel_id
                new_order = order.to_dict()
                try:
                    self.logger.info(f"canceling order, {order.client_id}")
                    self.cancel_response[order.cancel_id] = asyncio.Future()
                    await self.publish_to_channel(
                        "mq:hf_signal:cancel_request", 
                        {
                            "order": new_order, 
                            "strategy_name": self.strategy_name,
                            "trader": trader
                            }
                        )
                    res, headers = await self.cancel_response[order.cancel_id]
                    if res == True:
                        order.cancel = True
                except Exception as err:
                    self.logger.critical(f'cancel order {order.xchg_id} err {err}')
            
            cid = order.client_id
            if cid in self.canceling_id:
                return
            
            self.canceling_id.add(cid)
        
            counter = 0
            while True:
                if order_manager.get_trans_order(oid=cid) != None:
                    o = order_manager.trans_order_at(oid=cid)
                    if o.cancel:
                        self.canceling_id.remove(cid)
                        return
                else:
                    self.canceling_id.remove(cid)
                    return
                counter += 1
                self.loop.create_task(unit_cancel_order(order))
                await asyncio.sleep(0.01)
                
                if counter > 1000:
                    if order_manager.get_trans_order(oid=cid) != None:
                        o = order_manager.trans_order_at(oid=cid)
                        self.logger.warning(f"too many cancel orders, cache order={o.__dict__}")
                    self.logger.warning(f"too many cancel orders, order={order.__dict__}")
                    exit()
        except:
            traceback.print_exc()
            
    async def handle_query_order(self, order: TransOrder, order_manager: OrderManager):
        try:
            async def unit_query_order(order: TransOrder):
                trader = "hf_trader_test"
                ex, _, _, _ = order.symbol.split(".")
                query_id = ClientIDGenerator.gen_client_id(exchange_name=ex)
                order.query_id = query_id
                new_order = order.to_dict()
                try:
                    self.logger.info(f"quering order, {order.client_id}")
                    self.query_response[order.query_id] = asyncio.Future()
                    await self.publish_to_channel(
                            "mq:hf_signal:query_request", 
                            {
                                "order": new_order, 
                                "strategy_name": self.strategy_name,
                                "trader": trader
                                }
                            )
                    resp, headers = await self.query_response[order.query_id]
                    if resp:
                        self.logger.info(f"quering result, {resp.__dict__}")
                except:
                    return

                if not resp:
                    return
                if resp.xchg_status in OrderStatus.fin_status():
                    await self.on_order(resp)
                else:
                    await self.handle_cancel_order(order=order, order_manager=order_manager)
            
            cid = order.client_id  
            if time.time()*1e3 - order.sent_ts < 200 or cid in self.quering_id:
                return
            
            self.quering_id.add(cid)
            
            counter = 0
            while True:
                if order_manager.get_trans_order(oid=cid) != None:
                    o =  order_manager.trans_order_at(oid=cid)
                    if time.time()*1e3 - order.sent_ts < 200:
                        self.quering_id.remove(cid)
                        return
                else:
                    self.quering_id.remove(cid)
                    return
                counter += 1
                self.loop.create_task(unit_query_order(order))
                await asyncio.sleep(1)
                
                if counter > 100:
                    if order_manager.get_trans_order(oid=cid) != None:
                        o = order_manager.trans_order_at(oid=cid)
                        self.logger.critical(f"too many query orders, cache order={o.__dict__}")
                    self.logger.critical(f"too many query orders, order={order.__dict__}")
                    exit()
        except:
            traceback.print_exc()
    
    async def process_send_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        future: asyncio.Future = self.send_response[payload["client_id"]]
        if payload["status"] == False:
            self.logger.info(f"send response: {payload}")
            future.set_result((None, payload["headers"]))
        else:
            future.set_result((AtomOrder.from_dict(payload["data"]), payload["headers"]))
        
    async def process_cancel_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        future: asyncio.Future = self.cancel_response[payload["cancel_id"]]
        future.set_result((payload["status"],payload["headers"]))
        
    async def process_query_response(self, ch_name, response):
        payload = json.loads(response)
        if payload["strategy_name"] != self.strategy_name:
            return
        future: asyncio.Future = self.query_response[payload["query_id"]]
        if payload["status"] == False:
            future.set_result((None, payload["headers"]))
        else:
            future.set_result((AtomOrder.from_dict(payload["data"]), payload["headers"]))
            
    @staticmethod
    def orderbook_converter(raw_message: Union[AtomDepth, AnyStr]) -> dict:
        """
        converter of order book message from redis.
            --> return: {'asks': [[Decimal(9417), Decimal(31941)] ....], 'bids': [[]...], 'resp_ts': 1591843843560, 'server_ts': 1591843843560}
        """
        ob = dict()
        if isinstance(raw_message, AtomDepth):
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.asks))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_message.bids))
            ob['resp_ts'] = raw_message.local_ms
            ob['server_ts'] = raw_message.server_ms
        else:
            raw_ob = json.loads(raw_message)
            ob['asks'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['a']))
            ob['bids'] = list(map(lambda x: [float(x[0]), float(x[1])], raw_ob['b']))
            ob['resp_ts'] = raw_ob['lms']
            ob['server_ts'] = raw_ob['sms']
        return ob
           
    async def on_orderbook(self, symbol, orderbook):
        self.ap = orderbook["asks"][0][0]
        
    async def on_order(self, order: PartialOrder):
        self.logger.info(f"on order:{order.__dict__}")
        cid = order.client_id
        if self._order_manager.contains_trans_order(oid=cid):
            """ Clean up """
            if order.xchg_status in OrderStatus.fin_status():
                self._order_manager.pop_trans_order(oid=cid)
                self._order_manager.pop_order(oid=cid)
    
    def trim_order_before_sending(self, order: TransOrder):
        if self._contract_mapper.is_proxy_iid(order.iid):
            symbol = self._contract_mapper.proxy_spot_iid_to_spot_iid(order.iid)
        else:
            symbol = order.iid
        ex, _ = symbol.split(".")
        cid = ClientIDGenerator.gen_client_id(exchange_name=ex)
        order.symbol_id = self.get_symbol_config(order.symbol)["id"]
        order.client_id = cid
        order.sent_ts = int(time.time()*1e3)
        if order.side == Direction.long:
            order.price = round_decimals_down(order.price, price_precision[symbol])
        else:
            order.price = round_decimals_up(order.price, price_precision[symbol])
        
        order.quantity = round(order.quantity, qty_precision[symbol])
        
        return cid
            
    async def main_send_order_logic(self):
        while True:
            order = TransOrder(
                side=Direction.long,
                p=self.ap*(0.97),
                q=0.001,
                iid="binance_spot.btc_usdt_swap",
                floor=1,
                maker_taker=MakerTaker.maker,
                account_name=self.account_names[0]
            )
            order.generate_symbol(self._contract_mapper)
            
            cid = self.trim_order_before_sending(order)
            self._order_manager.add_trans_order(order=order, oid=cid)
            resp = self.loop.create_task(self.base_send_order(order=order))
            if not resp:
                self._order_manager.pop_trans_order(oid=cid)
            await asyncio.sleep(5)
                
    async def main_cancel_order_logic(self):
        while True:
            await asyncio.sleep(0.1)
            for _, order in self._order_manager.trans_order_items():
                self.loop.create_task(self.handle_cancel_order(order=order,order_manager=self._order_manager))

    async def main_query_order_logic(self):
        while True:
            await asyncio.sleep(0.5)
            for _, order in self._order_manager.trans_order_items():
                self.loop.create_task(self.handle_query_order(order=order,order_manager=self._order_manager))
            
    async def strategy_core(self):
        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                self.main_send_order_logic(),
                # self.main_cancel_order_logic(),
                self.main_query_order_logic()
            )
            
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
    