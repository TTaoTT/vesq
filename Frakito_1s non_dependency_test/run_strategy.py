import asyncio
from decimal import Decimal
from this import d
import time 
import numpy as np
import collections
from asyncio.queues import Queue
import pandas as pd

from atom.helpers import ClientIDGenerator
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy
from atom.exchange_api.binance.helper import BinanceHelper

from sortedcontainers import SortedDict


class SpreadCalculatorLite():
    def __init__(self, compound_alpha) -> None:
        self.lam = 1
        self.compound_alpha = compound_alpha
        self.t = 10
    
    def get_h(self,q, keppa):
        w1 = np.exp(-self.compound_alpha*(q**2))
        w2 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.compound_alpha*((q-1)**2))
        w3 = np.exp(self.lam*np.exp(-1)*self.t)*np.exp(-self.compound_alpha*((q+1)**2))
        return 1/keppa * np.log(w1 + w2 + w3)
    
    def get_ask(self,inven, keppa):
        return 1/keppa - self.get_h(inven-1, keppa) + self.get_h(inven, keppa)
    
    def get_bid(self,inven, keppa):
        return 1/keppa - self.get_h(inven+1, keppa) + self.get_h(inven, keppa)

class PriceDistributionCache():
    def __init__(self,cache_size=50) -> None:
        self.cache = []
        self.p_q = SortedDict()
        self.cache_size = cache_size
        self.q_sum = 0
        
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
            
        self.q_sum += trade["q"]
        if self.p_q.__contains__(trade["p"]):
            self.p_q[trade["p"]] += trade["q"]
        else:
            self.p_q[trade["p"]] = trade["q"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            old_trade = self.cache.pop(0)
            if self.p_q[old_trade["p"]]<=old_trade["q"]:
                self.p_q.pop(old_trade["p"])
            else:
                self.p_q[old_trade["p"]] -= old_trade["q"]
            self.q_sum -= old_trade["q"]
        
    def get_ask_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = True
        else:
            reverse = False
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
                
    def get_bid_price(self,quantile):
        if quantile > 0.5:
            quantile = 1 - quantile
            reverse = False
        else:
            reverse = True
            
        q_target = self.q_sum * quantile
        q_tmp = 0
        
        if reverse:
            for p in self.p_q.__reversed__():
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
        else:
            for p in self.p_q:
                q_tmp += self.p_q[p]
                if q_tmp >= q_target:
                    return p
    
    def get_median_price(self):
        return self.get_ask_price(0.5)
    
class VolumeCache():
    def __init__(self,cache_size=30*1e3) -> None:
        # No need to convert to int for this cache
        self._cache = []
        self._cache_size = cache_size
        self._amount_sum = Decimal(0)
        
    def feed_trade(self, trade: dict):
        if not self._cache or trade["t"]>= self._cache[-1]["t"]:
            self._cache.append(trade)
        elif trade["t"]<=self._cache[0]["t"]:
            self._cache = [trade] + self._cache
        else:
            i = len(self._cache) - 1
            while self._cache[i]["t"]>trade["t"]:
                i -= 1
            self._cache = self._cache[:i+1] + [trade] + self._cache[i+1:]
            
        self._amount_sum += trade["q"] * trade["p"]
            
    def remove_expired(self, ts):
        if not self._cache:
            return
        elif self._cache[-1]["t"] > ts:
            ts = self._cache[-1]["t"]
            
        while self._cache and self._cache[0]["t"] < ts-self._cache_size:
            old_trade = self._cache.pop(0)
            self._amount_sum -= old_trade["q"] * old_trade["p"]
    
    def get_amount(self):
        return self._amount_sum
    
class OrderCacheManager():
    def __init__(self) -> None:
        self.order_cache = dict()
        self.exposed_cache = dict()
        self.pos = Decimal(0)
        self.exposed_pos = Decimal(0)
        self.mp = []
        self.mp_ts = []
        self.mp_size = 1000
        self.year_ts = int(365 * 24 * 60 * 60 * 1000)
        self.spread = None
        self.last_update_time = 0
        
    def add_order(self,order):
        self.order_cache[order.client_id] = order
    
    def add_exposed_order(self,order,id):
        self.exposed_cache[id] = order
        
    def add_mp(self, ap,bp, ts):
        self.mp.append((ap+bp)/2)
        self.mp_ts.append(ts)
        self.spread = ap - bp
        self.last_update_time = max(self.last_update_time, ts)
        while self.mp_ts and ts - self.mp_ts[0] > self.mp_size:
            self.mp_ts.pop(0)
            self.mp.pop(0)
        
    def calib_mu_sigma(self):
        if len(self.mp) < 2:
            return None, None
        mu =  np.diff(self.mp).mean()
        sigma = np.diff(self.mp).std()
        dt = Decimal(np.diff(self.mp_ts).mean())
        if dt == 0:
            return None, None
        mu = mu * self.year_ts / dt
        sigma = sigma * np.sqrt(self.year_ts/dt)
        
        return mu, sigma
    
    def pred_exposure(self,curr_time,sigma_multiplier):
        
        if not self.mp or not self.spread:
            return 0
        exposure = 0
        mid_price = self.mp[-1]
        spread = self.spread
        dt = Decimal((int(curr_time) - self.last_update_time) / self.year_ts)
        
        mu, sigma = self.calib_mu_sigma()
        if not mu:
            return self.exposed_pos
        
        upper_bid = mid_price * (1 + mu * dt + sigma_multiplier * sigma * np.sqrt(dt)) - spread / 2
        lower_ask = mid_price * (1 + mu * dt - sigma_multiplier * sigma * np.sqrt(dt)) + spread / 2
        
        for order in list(self.exposed_cache.values()):
            if not order["cancel_ts"] or self.last_update_time < order["cancel_ts"]:
                if order["side"] == OrderSide.Buy and order["price"] >= lower_ask:
                    exposure += order["qty"]
                elif order["side"] == OrderSide.Sell and order["price"] <= upper_bid:
                    exposure -= order["qty"]
        return self.exposed_pos + Decimal(exposure)

    def add_trade(self,trade):
        self.last_update_time = max(self.last_update_time, trade["t"])
        for oid in list(self.exposed_cache.keys()):
            order = self.exposed_cache[oid]
            if order["side"] == OrderSide.Buy:
                if not order["cancel_ts"] and trade["p"] < order["price"] and order["create_ts"] < trade["t"]:
                    self.exposed_pos += order["qty"]
                    self.exposed_cache.pop(oid)
                elif order["cancel_ts"] and trade["p"] < order["price"] and order["create_ts"] < trade["t"] < order["cancel_ts"]:
                    self.exposed_pos += order["qty"]
                    self.exposed_cache.pop(oid)
                elif order["cancel_ts"] and trade["t"] > order["cancel_ts"]:
                    self.exposed_cache.pop(oid)
            else:
                if not order["cancel_ts"] and trade["p"] > order["price"] and order["create_ts"] < trade["t"]:
                    self.exposed_pos -= order["qty"]
                    self.exposed_cache.pop(oid)
                elif order["cancel_ts"] and trade["p"] > order["price"] and order["create_ts"] < trade["t"] < order["cancel_ts"]:
                    self.exposed_pos -= order["qty"]
                    self.exposed_cache.pop(oid)
                elif order["cancel_ts"] and trade["t"] > order["cancel_ts"]:
                    self.exposed_cache.pop(oid)
                    
    def after_cancel(self,cid,res):
        if self.order_cache.get(cid) == None:
            return
        o = self.order_cache[cid]
        if o.message["status"] == 0 and res == True and o.message["sent"]:
            self.order_cache[cid].message["status"] = 1
            self.order_cache[cid].message["filled"] = o.requested_amount
            self.pos = self.pos + o.requested_amount if o.side == OrderSide.Buy else self.pos - o.requested_amount
        elif type(res) == dict and res["status"] == "CANCELED" and o.message["status"] == 0:
            self.order_cache[cid].message["status"] = 1
            amount_changed = Decimal(res["executedQty"])
            self.order_cache[cid].message["filled"] = amount_changed
            self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
        else:
            self.order_cache[cid].message["cancel"] += 1
            
    def after_send_ack(self,cid):
        if self.order_cache.get(cid):
            self.order_cache[cid].message["sent"] = True
            
    def order_finish(self,cid,filled):
        # TODO make sure this is wright
        o = self.order_cache[cid]
        if o.message["status"] == 0:
            self.pos = self.pos + filled if o.side == OrderSide.Buy else self.pos - filled
        else:
            if o.message["filled"] != filled:
                amount_changed = filled - o.message["filled"]
                self.pos = self.pos + amount_changed if o.side == OrderSide.Buy else self.pos - amount_changed
        self.order_cache.pop(cid)
    
    def has_open_order(self):
        for cid in self.order_cache:
            if self.order_cache[cid].message["status"] == 0:
                return True
        return False

    def order_info(self,cid):
        o = self.order_cache[cid]
        res = dict()
        res["trigger_ts"] = o.message["trigger_ts"]
        res["create_ts"] = o.create_ms
        res["xchg_create_ts"] = o.message["xchg_create_ts"]
        res["price"] = o.requested_price
        res["qty"] = o.requested_amount
        res["xchg_end_time"] = None
        res["trigger_cancel_ts"] = o.message["trigger_cancel_ts"]
        
        return res

class QtyManager():
    def __init__(self,cache_size=60*1000*30) -> None:
        self.cache = []
        self.cache_size = cache_size
        self.buy_qty_sum = 0
        self.sell_qty_sum = 0
        self.buy_amount_sum = 0
        self.sell_amount_sum = 0
    
    def feed_trade(self, trade: dict):
        if not self.cache or trade["t"]>= self.cache[-1]["t"]:
            self.cache.append(trade)
        elif trade["t"]<=self.cache[0]["t"]:
            self.cache = [trade] + self.cache
        else:
            i = len(self.cache) - 1
            while self.cache[i]["t"]>trade["t"]:
                i -= 1
            self.cache = self.cache[:i+1] + [trade] + self.cache[i+1:]
        if trade["s"] == "BUY":
            self.buy_qty_sum += trade["q"]
            self.buy_amount_sum += trade["q"] * trade["p"]
        else:
            self.sell_qty_sum += trade["q"]
            self.sell_amount_sum += trade["q"] * trade["p"]
            
    def remove_expired(self, ts):
        if not self.cache:
            return
        elif self.cache[-1]["t"] > ts:
            ts = self.cache[-1]["t"]
            
        while self.cache and self.cache[0]["t"] < ts-self.cache_size:
            trade = self.cache.pop(0)
            if trade["s"] == "BUY":
                self.buy_qty_sum -= trade["q"]
                self.buy_amount_sum -= trade["q"] * trade["p"]
            else:
                self.sell_qty_sum -= trade["q"]
                self.sell_amount_sum -= trade["q"] * trade["p"]
                
    def get_spread(self):
        if not self.sell_qty_sum  or not self.buy_qty_sum:
            return None
        if self.sell_qty_sum + self.buy_qty_sum < 100:
            return None
        sell_price = self.sell_amount_sum / self.sell_qty_sum
        buy_price = self.buy_amount_sum / self.buy_qty_sum 
        return (sell_price - buy_price) / (sell_price + buy_price) * 2
        
class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.params = self.config['strategy']['params']
        symbol = self.params["symbol"]
       
        self.symbol = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.trade_cache = PriceDistributionCache(100)
        self.compound_alpha = self.params["compound_alpha"]
        # self.a_step = Decimal(self.params["a_step"])
        self.a = Decimal(self.params["a"])
        self.b = Decimal(self.params["b"])
        self.sc = SpreadCalculatorLite(self.compound_alpha)
        self.vol_cache = VolumeCache(5*1e3)

        self.price_muli = 1e7
        self.qty_muli = 1e7
        self.recvWindow = 1000

        self.send_order_queue = Queue()
        self.retreat_queue = Queue()
        self.cancel_order_queue = Queue()
        self.trade_ts = 0
        self.bbo_ts = 0
        self.pos_limit = Decimal(5)

        self.order_cache = OrderCacheManager()

        self.canceling_id = set()

        self.use_raw_stream = True

        self.heartbeat_order_enabled = False
        self.order_storage_enabled = False

        self.rate_limit = False
        self.send = True
        self.hold_back = False

        self.delay = 0
        
        self.last_symbol = None
        self.ap = None
        
        self.send_order_in_action = False
        self.rest_delay = 10
        self.new_session = None
        self.end_session = None
        self.end_session1 = None
        self.cancel_ts = 0
        
        self.orders = []
        self.expose = []
        self.pred_expose = []

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True

    async def internal_fail(self,client_id,err=None):
        try:
            self.logger.warning(f"failed order: {err}")
            if self.order_cache.order_cache.get(client_id) == None:
                return
            o = self.order_cache.order_cache[client_id]
            o.xchg_status = OrderStatus.Failed
            await self.on_order_inner(o)
        except:
            pass
        
    async def handle_limit(self,headers):
        # self.logger.warning(f"{headers}")
        cond1 = float(headers["X-MBX-USED-WEIGHT-1M"]) >0.8*2400
        cond2 = float(headers["X-MBX-ORDER-COUNT-10S"]) >0.8*300
        cond3 = float(headers["X-MBX-ORDER-COUNT-1M"]) >0.8*1200

        if cond1 or cond2 or cond3:
            self.logger.warning(f"{float(headers['X-MBX-USED-WEIGHT-1M'])},{float(headers['X-MBX-ORDER-COUNT-10S'])}, {float(headers['X-MBX-ORDER-COUNT-1M'])}")
            self.hold_back = True
            self.logger.warning("hold back")
            api = self.get_exchange_api("binance")[0]
            self.loop.create_task(api.flash_cancel_orders(self.get_symbol_config(self.symbol)))
            await asyncio.sleep(1)
            self.hold_back = False

    async def before_strategy_start(self):
        self.direct_subscribe_agg_trade(symbol_name=self.symbol)
        self.direct_subscribe_bbo(symbol_name=self.symbol)
        self.direct_subscribe_order_update(symbol_name=self.symbol)
        self.logger.setLevel("WARNING")
        await self.redis_set_cache({"exit":None})

        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol))
            self.order_cache.pos = position.data["long_qty"] - position.data["short_qty"]
            self.order_cache.exposed_pos = position.data["long_qty"] - position.data["short_qty"]

            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol)
                qty_tick = symbol_cfg["qty_tick_size"]
                price_tick = symbol_cfg['price_tick_size']
                self.half_step_tick = qty_tick/Decimal(2)
                self.half_price_tick = price_tick/Decimal(2)
                self.price_tick = price_tick
                c = 0
                if Decimal(self.price_muli)*price_tick < Decimal(0.5) or Decimal(self.qty_muli)*qty_tick < Decimal(0.5):
                    self.logger.warning("price or qty multi not good enough")
                    exit()
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ap:
                        self.logger.warning(f"{self.ap}")
                        
                        self.amount = 0.005
                        self.amount_decimal = Decimal('0.005')
                        break
                    if c > 1000:
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
            raise
            
    async def update_amount(self):
        while True:
            await asyncio.sleep(60*60)
            try:
                api = self.get_exchange_api("binance")[0]
                cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol))).data
                equity = cur_balance.get('usdt')
                if equity:
                    balance = equity["all"]
                    if self.ap:
                        amount = min(balance*4,Decimal(self.params["amount"]))
                        amount = amount / self.ap
                        symbol_cfg = self.get_symbol_config(self.symbol)
                        qty_tick = symbol_cfg["qty_tick_size"]
                        amount = float(int(amount/qty_tick)*qty_tick)
                        
                        self.amount = amount
                        self.amount_decimal = Decimal(amount)
            except Exception as err:
                self.logger.warning(f"update amount err:{err}")
            
    async def on_order(self, o):
        # self.logger.warning(f"on order here,{o}")
        if o["e"] != "ORDER_TRADE_UPDATE":
            return
        order = o["o"]
        client_id = order["c"]
        if client_id not in list(self.order_cache.order_cache):
            return
        status = BinanceHelper.order_status_mapping_rev[order["X"]]
        if status == OrderStatus.New:
            self.order_cache.order_cache[client_id].message["xchg_create_ts"] = order["T"]
        if status in OrderStatus.fin_status():
            filled = Decimal(order["z"])
            res = self.order_cache.order_info(client_id)
            res["xchg_end_time"] = order["T"]
            self.orders.append(res)
                # self.logger.warning(f"order filled at {Decimal(order['ap'])},side:{self.order_cache.order_cache[client_id].side}, filled: {order['z']}")
            self.order_cache.order_finish(client_id,filled)
            
            
    async def on_order_inner(self, order):
        client_id = order.client_id
        if client_id not in list(self.order_cache.order_cache): 
            return
        
        if order.xchg_status in OrderStatus.fin_status():
            filled = order.filled_amount
            res = self.order_cache.order_info(client_id)
            self.orders.append(res)
            self.order_cache.order_finish(client_id,filled)
            

    async def on_bbo(self, symbol, bbo):
        if symbol != self.last_symbol:
            self.logger.warning(f"wrong symbol: {symbol}")
        self.last_symbol = symbol
        
        self.ap = Decimal(bbo["data"]["a"])
        self.bp = Decimal(bbo["data"]["b"])
        self.order_cache.add_mp(self.ap,self.bp,bbo["data"]["T"])
            
        if self.new_session and bbo["data"]["T"] % (1000*60*5) < self.new_session:
            self.send = True
            self.order_cache.exposed_cache = dict()
            self.order_cache.exposed_pos = Decimal(0)
            self.logger.warning("new session start")
        self.new_session = bbo["data"]["T"] % (1000*60*5)
        
        if self.end_session and (bbo["data"]["T"] + 1000*10) % (1000*60*5) < self.end_session:
            self.retreat_queue.put_nowait(bbo["data"]["T"])
        self.end_session = (bbo["data"]["T"] + 1000*10) % (1000*60*5)

        if self.end_session1 and (bbo["data"]["T"] + 1000*5) % (1000*60*5) < self.end_session1:
            self.retreat_queue.put_nowait(bbo["data"]["T"])
        self.end_session1 = (bbo["data"]["T"] + 1000*5) % (1000*60*5)
        
        if self.bbo_ts and bbo["data"]["T"] % 1000 < self.bbo_ts:
            self.send_order_queue.put_nowait(bbo["data"]["T"])
        self.bbo_ts = bbo["data"]["T"] % 1000
        
        if bbo["data"]["T"] - self.cancel_ts > 10:
            self.cancel_order_queue.put_nowait(1)
            self.cancel_ts = bbo["data"]["T"]
    
    async def on_agg_trade(self, symbol, trade):
        self.delay = time.time()*1e3 - trade["data"]["T"]
        self.trade_ts = max(self.trade_ts, trade["data"]["T"])
        tmp = {
            "t":trade["data"]["T"],
            "p":int(float(trade["data"]["p"])*self.price_muli), 
            "q":int(float(trade["data"]["q"])*self.qty_muli)
            }

        self.trade_cache.feed_trade(tmp)
        self.trade_cache.remove_expired(self.trade_ts)
        self.vol_cache.remove_expired(self.trade_ts)
        self.vol_cache.feed_trade(
            {
            "t":trade["data"]["T"],
            "p":Decimal(trade["data"]["p"]), 
            "q":Decimal(trade["data"]["q"])
            }
        )
        self.order_cache.add_trade(
            {
            "t":trade["data"]["T"],
            "p":Decimal(trade["data"]["p"]),   
            }
        )

    async def send_order_action(self):
        while True:
            trigger_ts = await self.send_order_queue.get()
            self.send_order_in_action = True
            if not self.send or self.rate_limit:
                continue
            spread = float(self.a * np.sqrt(self.vol_cache.get_amount()) + self.b)
            spread = max(spread,0.0002)
            pred_expo = self.order_cache.pred_exposure(time.time()*1e3,1)
            cur_risk = pred_expo/self.amount_decimal
            
            self.pred_expose.append({"t":int(time.time()*1e3),"pred_exposure":pred_expo})
            self.expose.append({"t":int(time.time()*1e3),"pred_exposure":self.order_cache.exposed_pos})
            
            mp = self.trade_cache.get_median_price()
            if not mp:
                mp = (self.ap + self.bp) / 2
            else:
                mp = Decimal(mp / self.price_muli)
                
            ask = mp*(1+Decimal(self.sc.get_ask(float(cur_risk),1/spread)))
            bid = mp*(1-Decimal(self.sc.get_bid(float(cur_risk),1/spread)))
            
            if not self.hold_back:
                self.send_entry_order(ask,bid,self.amount_decimal,trigger_ts)
            self.send_order_in_action = False
                
    def send_entry_order(self,ask,bid,qty,trigger_ts):
        self.loop.create_task(self.send_order(ask,qty,OrderSide.Sell,trigger_ts=trigger_ts))
        self.loop.create_task(self.send_order(bid,qty,OrderSide.Buy,trigger_ts=trigger_ts))
            
    async def rate_limit_check(self):
        self.logger.warning("retreating now")
        api = self.get_exchange_api("binance")[0]
        while True:
            trigger_ts = await self.retreat_queue.get()
            if self.send:
                self.send = False
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
            except Exception as err:
                self.logger.warning(f"pre retreat err:{err}")
            trigger_ts = await self.retreat_queue.get()
            try:
                if pos > 0:
                    await self.send_order(self.bp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000,trigger_ts=trigger_ts)
                else:
                    await self.send_order(self.ap*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000,trigger_ts=trigger_ts)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                
            self.order_cache.exposed_cache = dict()
            self.order_cache.exposed_pos = Decimal(0)
            
    async def send_order(self,price,qty,side,position_side=OrderPositionSide.Open,order_type=OrderType.Limit,recvWindow=None,timestamp=None,trigger_ts=0):
        client_id = ClientIDGenerator.gen_client_id("binance")
        if not self.volume_notional_check(self.symbol,price,qty):
            return
        qty += self.half_step_tick
        price += self.half_price_tick
        price, qty = self.format_price_volume(self.symbol, price, qty)
        if side == OrderSide.Sell:
            client_id += "s"
        else:
            client_id += "b"

        if recvWindow == None:
            recvWindow = self.recvWindow
        
        order = self.gen_async_order(
            client_id=client_id,
            symbol_name=self.symbol,
            price=price,
            volume=qty,
            side=side,
            position_side=position_side,
            order_type=order_type
        )
        extra_info = dict()
        extra_info["stop_ts"] = 1
        extra_info["cancel"] = 0
        extra_info["query"] =  0
        extra_info["status"] =  0
        extra_info["filled"] =  Decimal(0)
        extra_info["sent"] = False
        extra_info["trigger_ts"] = trigger_ts
        extra_info["xchg_create_ts"] = trigger_ts
        extra_info["trigger_cancel_ts"] = trigger_ts
        order.message = extra_info
        order.create_ms = int(time.time()*1e3)
        self.order_cache.add_order(order)
        exposed_order = {
            "price":order.requested_price,
            "qty":order.requested_amount,
            "side":order.side,
            "create_ts":order.create_ms + self.rest_delay,
            "cancel_ts":None,
        }
        self.order_cache.add_exposed_order(exposed_order, client_id)
        if timestamp == None:
            timestamp = order.create_ms
        try:
            res = await self.raw_make_order(
                symbol_name=self.symbol,
                price=price,
                volume=qty,
                side=side,
                position_side=position_side,
                order_type=order_type,
                timestamp=timestamp,
                recvWindow=recvWindow,
                client_id=client_id
            )
            self.order_cache.after_send_ack(client_id)
            await self.handle_limit(res.headers)
        except RateLimitException as err:
            await self.internal_fail(client_id, err)
            self.rate_limit = True
            if self.retreat_queue.empty() and self.send:
                self.retreat_queue.put_nowait(1)
            await asyncio.sleep(20)
            self.logger.warning("rest for 20s done")
            self.rate_limit = False
        except ApiTimeWindowExpiredError as err:
            await self.internal_fail(client_id, err)
        except InsufficientBalanceError as err:
            await self.internal_fail(client_id, err)
        except ReduceOnlyOrderFail as err:
            await self.internal_fail(client_id, err)
        except ExchangeTemporaryError as err:
            await self.internal_fail(client_id, err)
        except Exception as err:
            if str(err) == "Expected object or value":
                raise
            else:
                await self.internal_fail(client_id,err)

    async def cancel_order(self, order: Order, catch_error=True):
        """
        Cancel a order
        order: Order object
        --> return: True / False
        """
        symbol = self.get_symbol_config(order.symbol_id)
        api = self.get_exchange_api_by_account(order.account_name)
        try:
            if order.xchg_id is not None:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id)
            else:
                r = await api.cancel_order(symbol=symbol, order_id=order.xchg_id, client_id=order.client_id)
            if order.tag != "HB-ORDER":
                self.logger.info(f"[cancel order]: {order.client_id}/{order.xchg_id} {r.data}")
            return r.json
        except Exception as err:
            if not catch_error:
                raise
            if type(err) in (OrderAlreadyCompletedError, OrderNotFoundError):
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect True")
                return True
            elif type(err) == ExchangeTemporaryError:
                self.logger.error(f"[cancel order error] [{err}] {order.client_id}/{order.xchg_id}, redirect False")
                return False
            elif isinstance(err, ExchangeConnectorException):
                self.logger.error(f"[cancel order error] {err} {order.client_id}/{order.xchg_id}, raise unknown api error")
                raise
            else:
                self.logger.error(f"[cancel order error] {err!r} {order.client_id}/{order.xchg_id}, redirect False")
                return False

    async def batch_cancel(self, oid, order):
        try:
            if self.order_cache.order_cache.get(oid) == None:
                return
            if oid not in self.canceling_id and self.order_cache.order_cache[oid].message["cancel"] < 4:
                if self.order_cache.order_cache[oid].message["cancel"] == 0 and self.order_cache.exposed_cache.get(oid):
                    self.order_cache.exposed_cache[oid]["cancel_ts"] = time.time()*1e3 + self.rest_delay
                    self.order_cache.order_cache[oid].message["trigger_cancel_ts"] = int(time.time()*1e3)
                self.canceling_id.add(oid)
                res = await self.cancel_order(order)
                self.canceling_id.remove(oid)
                self.order_cache.after_cancel(oid, res)
        except Exception as err:
            self.logger.warning(f'cancel order {oid} err {err}')
            if oid in self.canceling_id:
                self.canceling_id.remove(oid)

    async def cancel_order_action(self):
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3*0.2:
                try:
                    await self.batch_cancel(oid, order)
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f"cancel order err {err}")
        while True:
            await self.cancel_order_queue.get()
            for oid, order in self.order_cache.order_cache.items():
                self.loop.create_task(batch_cancel(oid, order))
    
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(self.symbol),order.xchg_id, client_id=order.client_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):
        async def batch_check_order(oid,order):
            if not order.message:
                # self.logger.warning(f"order has no message while try to check,{oid}")
                return
            if oid in self.canceling_id:
                # self.logger.warning(f"order in canceling state while try to check,{oid}")
                return
            if not order.message["sent"]:
                # self.logger.warning(f"order not sent yet while try to check,{oid}")
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + order.message["query"]*6 + 1)*1e3 and order.message["cancel"]>0:
                try:
                    self.logger.warning(f"check order when {time.time()*1e3 - order.create_ms},{oid}")
                    order_new = await self.get_order_status_direct(order)
                    
                    if self.order_cache.order_cache.get(oid) is not None:
                        self.order_cache.order_cache[oid].message["query"] += 1
                        if self.order_cache.order_cache[oid].message["query"]>10:
                            self.send = False
                            await self.retreat()
                            await self.redis_set_cache({})
                            self.logger.warning(f"check order too many times and exiting")
                            exit()
                        elif not order_new:
                            return
                        elif order_new.xchg_status in OrderStatus.fin_status():
                            await self.on_order_inner(order_new)
                        else:
                            self.loop.create_task(self.batch_cancel(oid,order_new))
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.order_cache.items()]
            )

    async def retreat(self,trigger_ts=0):
        self.logger.warning("retreating now")
        api = self.get_exchange_api("binance")[0]
        for _ in range(3):
            try:
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol))
                position = await api.contract_position(self.get_symbol_config(self.symbol))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.send_order(self.bp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000,trigger_ts=trigger_ts)
                else:
                    await self.send_order(self.ap*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC,recvWindow=3000,trigger_ts=trigger_ts)
            except Exception as err:
                self.logger.warning(f"retreat err:{err}")
                raise
            await asyncio.sleep(3)

    async def update_redis_cache(self):

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send = False
                    await self.retreat()
                    await self.redis_set_cache({})
                    self.logger.warning(f"manually exiting")
                    exit()
                await self.redis_set_cache({"exit":None})
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            
    async def store_data(self):
        self.store_ts = 0
        while True:
            await asyncio.sleep(1)
            t = time.time()*1e3
            if t % (24*60*60*1000) < self.store_ts:
                data = pd.DataFrame(self.orders)
                data.to_csv(f"./data/{self.symbol}_{int(t)}_Frakito_1s_non_dependency.csv")
                pd.DataFrame(self.pred_expose).to_csv(f"./data/pred_exposure_{self.symbol}_{int(t)}_Frakito_1s_non_dependency.csv")
                pd.DataFrame(self.expose).to_csv(f"./data/exposure_{self.symbol}_{int(t)}_Frakito_1s_non_dependency.csv")
                self.orders = []
                self.pred_expose = []
                self.expose = []
                self.logger.warning(">>>>>>")
            self.store_ts = t % (24*60*60*1000)
            
    async def pos_checker(self):
        while True:
            await asyncio.sleep(10)
            self.logger.warning(f"""
                                actual position: {self.order_cache.pos},
                                implied position: {self.order_cache.exposed_pos}
                                """)
            
    async def strategy_core(self):
        account_name = self.account_names[0]
        self._account_config_[account_name]["cfg_pos_mode"][self.symbol] = 1
        await asyncio.sleep(10)
        await asyncio.gather(
            self.update_redis_cache(),
            self.reset_missing_order_action(),
            self.cancel_order_action(),
            self.send_order_action(),
            # self.update_amount(),
            self.rate_limit_check(),
            self.pos_checker(),
            self.store_data()
            )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
            
    