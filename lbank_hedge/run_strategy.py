

from strategy_base.utils import sync_git_repo
sync_git_repo("gitlab.com/aurthes/xex_mm.git", "xex_mm", "preprod")

import json
from collections import defaultdict
from typing import Dict

from xex_mm.dmm_strategy.hedge_inven_manager import HedgeInvenManager
from xex_mm.utils.base import DefaultDict, Order

from strategy_base.base import CommonStrategy
import asyncio
from atom.model.depth import DepthConfig
DepthConfig.MAX_DEPTH = 2
from atom.model import BBODepth, OrderSide, OrderPositionSide, OrderType, PartialOrder
import traceback
from decimal import Decimal
import simplejson

from xex_mm.dmm_strategy.executor import InventoryHedger, InventoryHedger2
import time

INITIAL_BALANCE = 4600000

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        self.symbols = [
            "binance.btc_usdt_swap.usdt_contract.na",
            "binance.eth_usdt_swap.usdt_contract.na",
            "binance.ape_usdt_swap.usdt_contract.na",
            "binance.gmt_usdt_swap.usdt_contract.na",
            "binance.xrp_usdt_swap.usdt_contract.na",
            "binance.matic_usdt_swap.usdt_contract.na",
            "binance.ada_usdt_swap.usdt_contract.na",
            "binance.sol_usdt_swap.usdt_contract.na",
            "binance.gala_usdt_swap.usdt_contract.na",
            "binance.etc_usdt_swap.usdt_contract.na",
            "binance.axs_usdt_swap.usdt_contract.na",
            "binance.doge_usdt_swap.usdt_contract.na",
            "binance.atom_usdt_swap.usdt_contract.na",
            "binance.near_usdt_swap.usdt_contract.na",
            "binance.sand_usdt_swap.usdt_contract.na",
            "binance.apt_usdt_swap.usdt_contract.na",
        ]
        self.hedge_ex = "binance"
        self.mp = dict()
        self.executor: Dict[str, InventoryHedger] = dict()

        # NEW HEDGER SUIT
        self.inven_manager = HedgeInvenManager(ex=self.hedge_ex, logger=self.logger)
        self.executor2: Dict[str, InventoryHedger2] = dict()
        
        self.max_lose_ccy2 = float(self.config['strategy']['params']['max_lose_ccy2'])

        self.uid_inven_ccy2: Dict[str, float] = defaultdict(lambda: 0)

        self.hedge_account = "binance.mm19"
        self.hedge_account_balance = None
        self.max_binance_leverage = 1

    def get_vol_profile_key(self, contract: str):
        return f"cache:lbank_hedge:{contract}_vol_profile"

    async def before_strategy_start(self):
        for symbol in self.symbols:
            self.direct_subscribe_bbo(symbol_name=symbol)
            ex, contract, _, _ = symbol.split(".")
            self.executor[contract] = InventoryHedger(contract=contract, ex=ex, max_lose_ccy2=self.max_lose_ccy2,
                                                      VaR_quantile=0.99, short_halflife=600, long_halflife=86400,
                                                      hegde_increment_ccy2=10000, min_sharpe=1, logger=self.logger)
            self.executor2[contract] = InventoryHedger2(contract=contract, ex=ex, max_lose_ccy2=self.max_lose_ccy2,
                                                      VaR_quantile=0.99, short_halflife=600, long_halflife=86400,
                                                      hegde_increment_ccy2=1000, min_sharpe=0., logger=self.logger)
            # load cached vol profile
            vol_profile_data, status = await self.redis_get_data(redis_key=self.get_vol_profile_key(contract=contract))
            if not status:
                vol_profile_data = {}

            self.logger.info(f"contract={contract}, vol_profile_data={vol_profile_data}")

            # set cached vol profile
            if vol_profile_data:
                short_ret_var = vol_profile_data.get("short_ret_var")
                if short_ret_var:
                    short_ret_var = float(short_ret_var)
                short_dt = vol_profile_data.get("short_dt")
                if short_dt:
                    short_dt = float(short_dt)
                long_ret_var = vol_profile_data.get("long_ret_var")
                if long_ret_var:
                    long_ret_var = float(long_ret_var)
                long_dt = vol_profile_data.get("long_dt")
                if long_dt:
                    long_dt = float(long_dt)
                self.executor[contract].on_init(short_ret_var=short_ret_var, short_dt=short_dt,
                                                long_ret_var=long_ret_var, long_dt=long_dt)
                self.executor2[contract].on_init(short_ret_var=short_ret_var, short_dt=short_dt,
                                                long_ret_var=long_ret_var, long_dt=long_dt)
    
    async def redis_get_data(self, redis_key):
        r = await self.cache_redis.handler.get(redis_key)
        if r:
            return simplejson.loads(r, use_decimal=True), True
        else:
            self.logger.warning(f"get data from {redis_key} fail")
            return dict(), False

    async def on_order(self, order: PartialOrder):
        try:
            todr = self.inven_manager.pending_finish_transorders.get(order.client_id)
            if not todr:
                return
            symbol = f"binance.{order.contract}.usdt_contract.na"
            cv = self.get_symbol_config(symbol)["contract_value"]
            odr_msg = Order.from_dict(porder=order, torder=todr, contract_value=cv)
            self.inven_manager.on_odr_msg(odr_msg=odr_msg)
        except:
            traceback.print_exc()
    
    async def on_bbo(self, symbol, bbo: BBODepth):
        ex, contract = symbol.split(".")
        try:
            ap = float(bbo.ask[0])
            bp = float(bbo.bid[0])
            self.mp[contract] = (ap + bp) / 2
        except:
            self.logger.critical(traceback.format_exc())
        
    async def query_position(self):
        lbank_pos = dict()
        binance_pos = dict()
        for acc in self.account_names:
            api = self.get_exchange_api_by_account(acc)
            if acc.startswith("binance"):
                cur_position = (await api.contract_position_all(market="usdt_contract", sub_market="na"))
                for contract in cur_position.data:
                    spos = float(cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"])
                    if self.mp.get(contract) != None:
                        self.logger.info(f"{acc}.{contract} position (ccy2): {spos * self.mp.get(contract)}")
                        p = self.mp.get(contract)
                        if binance_pos.get(contract) != None:
                            binance_pos[contract] += spos * p
                        else:
                            binance_pos[contract] = spos * p
                    else:
                        self.logger.info(f"{contract} no price")
            else:
                cur_position = (await api.contract_position_all(market="swap", sub_market="na"))
            # self.logger.info(f"position info:{cur_position}") 
                for contract in cur_position.data:
                    spos = float(cur_position.data[contract]["long_qty"] - cur_position.data[contract]["short_qty"])
                    p = self.mp.get(contract)
                    self.logger.info(f"{acc}.{contract} position (ccy2): {spos * p if p else None}")
                    if self.mp.get(contract) != None:
                        p = self.mp.get(contract)
                        if lbank_pos.get(contract) != None:
                            lbank_pos[contract] += spos * p
                        else:
                            lbank_pos[contract] = spos * p
                    else:
                        self.logger.info(f"{contract} no price")

        max_hedge_amt = self.hedge_account_balance * self.max_binance_leverage
        self.logger.info(f"max_hedge_amt:{max_hedge_amt}")
        for contract in lbank_pos:
            mp = self.mp.get(contract)
            if not mp:
                continue
            
            lbank_spos = lbank_pos[contract]
            if binance_pos.get(contract):
                binance_spos = binance_pos[contract]
            else:
                binance_spos = 0
            
            exec = self.executor.get(contract)
            if not exec:
                continue
            
            amt = self.executor[contract].get_hedge_qty(dmm_ex_inven_ccy2=lbank_spos, hedge_ex_inven_ccy2=binance_spos, uid_inven_ccy2=self.uid_inven_ccy2)
            self.logger.critical(f"Hedge: contract={contract}, amt={amt}")
            self.logger.info(f"{contract} max inven:{self.executor[contract].get_max_inven()}")
            self.logger.info(f"{contract} ret_qtl:{self.executor[contract].get_ret_qtl()}")

            # new hedger
            exec = self.executor2[contract]
            inven_ccy2 = lbank_spos
            contract_hedge_inven_manager = self.inven_manager.contract_hedge_inven_managers[contract]
            blacklist_hedge_amt = contract_hedge_inven_manager.blacklist_hedge_inven_manager.inven_ccy1 * exec.blacklist_hedger.prc
            signal_hedge_amt = contract_hedge_inven_manager.cta_hedge_inven_manager.inven_ccy1 * exec.cta_hedger.prc
            VaR_hedge_amt = contract_hedge_inven_manager.VaR_hedge_inven_manager.inven_ccy1 * exec.VaR_hedger.prc
            exec.on_inven_ccy2(inven_ccy2)
            exec.blacklist_hedger.on_blacklist_hedge_amt(blacklist_hedge_amt=blacklist_hedge_amt)
            exec.cta_hedger.on_signal_hedge_amt(signal_hedge_amt=signal_hedge_amt)
            exec.VaR_hedger.on_VaR_hedge_amt(VaR_hedge_amt=VaR_hedge_amt)
            todrs = exec.get_hedge_transorders()
            self.logger.critical(f"contract={contract}")
            for todr in todrs:
                # self.inven_manager.on_transorder(todr=todr)
                self.logger.critical(f"todr={todr}")
            if amt > 0:
                self.logger.info(f"{contract} should buy: {amt} for hedge, lbank pos:{lbank_spos}, binance pos:{binance_spos}")
                target = binance_spos + amt
                if target < max_hedge_amt:
                    await self.hedge_order(contract=contract, amt=abs(amt), side=OrderSide.Buy)
            elif amt < 0:
                target = binance_spos + amt
                if target > -max_hedge_amt:
                    await self.hedge_order(contract=contract, amt=abs(amt), side=OrderSide.Sell)
                self.logger.info(f"{contract} should sell: {amt} for hedge, lbank pos:{lbank_spos}, binance pos:{binance_spos}")

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if Decimal(qty) < config["min_quantity_val"]:
            return False
        if Decimal(price * qty) < config["min_notional_val"]:
            return False
        return True

    async def hedge_order(self, contract: str, amt: float, side: OrderSide):
        mp = self.mp.get(contract)
        if not mp:
            return
        price = mp * (1 + 0.001) if side == OrderSide.Buy else mp * (1 - 0.001)
        volume = abs(amt / price)
        symbol = f"binance.{contract}.usdt_contract.na"
        if not self.volume_notional_check(symbol=symbol, price=price, qty=volume):
            return
        await self.make_order(
            account_name=self.hedge_account,
            symbol_name=symbol,
            price = Decimal(price),
            volume=Decimal(volume),
            side=side,
            position_side=OrderPositionSide.Open,
            order_type=OrderType.IOC
        )
        
    async def feed_price(self):
        while True:
            await asyncio.sleep(1)
            for contract in self.mp:
                self.executor[contract].on_price(prc=self.mp[contract], ts=int(time.time() * 1e3))
                self.executor2[contract].on_prc(prc=self.mp[contract], ts=int(time.time() * 1e3))
                
    async def main_logic(self):
        while True:
            await asyncio.sleep(10)
            try:
                await self.query_position()
            except:
                self.logger.critical(f"error: {traceback.format_exc()}")

    async def update_wyf_signal(self):
        sharpe_data, status = await self.redis_get_data(f"cache:strategy:wangyife_mm_signal_sharpe_ratio")
        if not status:
            sharpe_data = {}

        for symbol in self.symbols:
            ex, contract, _, _ = symbol.split(".")
            ccy1 = contract.split("_")[0]

            uid = "wyf"
            # sharpe
            pair = f"{ccy1}_usdt"
            sharpe = sharpe_data.get(pair)
            if sharpe is not None:
                sharpe = float(sharpe)
                self.logger.info(f"{pair} sharpe: {sharpe}")
                self.executor[contract].on_player_sharpe(uid=uid, sharpe=sharpe)
                self.executor2[contract].cta_hedger.on_cta_sharpe(name=uid, sharpe=sharpe)
            else:
                self.logger.critical(f"{uid} missing sharpe for {contract}")
            # pos
            signal_data, stauts = await self.redis_get_data(f"cache:strategy:mm_signal_b_{ccy1}usdt")
            self.logger.info(f"{symbol} raw data: {signal_data}")
            for key in signal_data:
                pos = float(signal_data[key]["long_qty"] - signal_data[key]["short_qty"])
                self.logger.info(f"{key} pos: {pos}")
                self.executor[contract].on_player_pos(uid=uid, pos=pos)
                self.executor2[contract].cta_hedger.on_cta_signal(name=uid, pos=pos)

    async def update_max_drawdown(self):
        while True:
            await asyncio.sleep(10)

            data, status = await self.redis_get_data(f"cache:hf_lbank:max_drawdown")
            if not status:
                data = {}

            mdd = data.get("max_drawdown")
            if mdd is not None:
                mdd = float(mdd)
                self.logger.info(f"MaxDrawDown: {mdd}")
                for symbol in self.symbols:
                    ex, contract, _, _ = symbol.split(".")
                    self.executor[contract].on_max_drawdown(max_drawdown=mdd)
                    self.executor2[contract].VaR_hedger.on_max_drawdown(max_drawdown=mdd)
            else:
                self.logger.critical(f"MaxDrawDown missing")

    async def update_blacklist_inven(self):
        while True:
            await asyncio.sleep(10)

            data, status = await self.redis_get_data(f"cache:hf_lbank:blacklist_position")
            if not status:
                data = {}

            for symbol in self.symbols:
                ex, contract, _, _ = symbol.split(".")
                blacklist_inven_ccy1 = data.get(contract)
                if blacklist_inven_ccy1 is not None:
                    blacklist_inven_ccy1 = float(blacklist_inven_ccy1)
                    self.executor2[contract].blacklist_hedger.on_blacklist_inven_ccy1(blacklist_inven_ccy1=blacklist_inven_ccy1)
                    self.logger.critical(f"UpdateBlackListInven: contract={contract},blacklist_inven_ccy1={blacklist_inven_ccy1}")
                else:
                    self.logger.critical(f"UpdateBlackListInven: contract={contract},blacklist_inven_ccy1 missing.")

    async def update_cl_signal(self):
        sharpe_data, status = await self.redis_get_data(f"cache:strategy:save_strategy_sharpe")
        if not status:
            sharpe_data = {}

        self.logger.info(f"sharpe_data={sharpe_data}")

        for symbol in self.symbols:
            ex, contract, _, _ = symbol.split(".")
            ccy1 = contract.split("_")[0]

            uid = "cl"
            # sharpe
            key = f"hedge_volumeProfileStdStrategy_{ccy1}"
            sharpe = sharpe_data.get(key)
            if sharpe is not None:
                sharpe = float(sharpe)
                self.logger.info(f"uid={uid}, key={key}, sharpe={sharpe}")
                self.executor[contract].on_player_sharpe(uid=uid, sharpe=sharpe)
                self.executor2[contract].cta_hedger.on_cta_sharpe(name=uid, sharpe=sharpe)
            else:
                self.logger.critical(f"{uid} missing sharpe for {contract}")
            # pos
            signal_data, stauts = await self.redis_get_data(f"cache:strategy:{key}")
            for key in signal_data:
                pos = float(signal_data[key]["long_qty"] - signal_data[key]["short_qty"])
                self.logger.info(f"uid={uid}, key={key}, pos={pos}")
                self.executor[contract].on_player_pos(uid=uid, pos=pos)
                self.executor2[contract].cta_hedger.on_cta_signal(name=uid, pos=pos)

    async def update_signal(self):
        while True:
            await asyncio.sleep(10)
            await self.update_wyf_signal()
            await self.update_cl_signal()

    async def update_binance_balance(self):
        while True:
            await asyncio.sleep(10)
            api = self.get_exchange_api_by_account(self.hedge_account)
            curr_balance = (await api.account_balance_all(market="usdt_contract", sub_market="na")).data["usdt"]["all"]
            self.hedge_account_balance = curr_balance

    async def write_vol_profile(self):
        while True:
            await asyncio.sleep(10)
            for symbol in self.symbols:
                ex, contract, _, _ = symbol.split(".")
                executor = self.executor.get(contract)
                if not executor:
                    continue
                vol_profile_data = dict(short_ret_var=executor.short_daily_ret_quantile_cal.ema_ret_var.get_avg(),
                                        short_dt=executor.short_daily_ret_quantile_cal.ema_dt.get_avg(),
                                        long_ret_var=executor.long_daily_ret_quantile_cal.ema_ret_var.get_avg(),
                                        long_dt=executor.long_daily_ret_quantile_cal.ema_dt.get_avg())
                await self.cache_redis.handler.set(self.get_vol_profile_key(contract=contract), json.dumps(vol_profile_data))
                self.logger.info(f"Wrote vol profile to redis for {contract}: {vol_profile_data}")
            
    async def strategy_core(self):
        await asyncio.gather(
            self.feed_price(),
            self.main_logic(),
            self.update_signal(),
            self.update_binance_balance(),
            self.update_max_drawdown(),
            self.update_blacklist_inven(),
            self.write_vol_profile(),
        )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()
