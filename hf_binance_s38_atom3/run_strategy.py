import asyncio
from decimal import Decimal
import time 
import numpy as np
import math
import collections
import datetime
import copy
import aiohttp
from asyncio.queues import Queue

from atom.helpers import json, safe_decimal
from atom.exceptions import *
from atom.model import *
from strategy_base.base import CommonStrategy

class MyStrategy(CommonStrategy):
    def __init__(self, loop, **kwargs):
        super().__init__(loop, **kwargs)
        # init params
        # use self.params
        self.trade_cache = list()
        self.trade_cache_buy_4s = list()
        self.trade_cache_sell_4s = list()
        self.trade_cache_buy_4s_large = list()
        self.trade_cache_sell_4s_large = list()

        self.trade_cache_buy_1s = list()
        self.trade_cache_sell_1s = list()
        self.trade_cache_buy_1s_large = list()
        self.trade_cache_sell_1s_large = list()
        self.missing_order_cache = list()
        self.order_cache = dict()
        self.pos = Decimal(0)
        self.long_pos = Decimal(0)
        self.short_pos = Decimal(0)
        self.mp = None
        self.ob_mp = None
        self.spread = Decimal(0)
        self.ap = Decimal(0)
        self.bp = Decimal(0)
        self.trade_buy = 0
        self.trade_sell = 0
        self.heartbeat_order_enabled = False
        self.unknown_order_dict = {}
        self.max_balance = 0
        self.last_trade_ts = 0
        self.ob_update = False
        self.send_order = True
        self.order_storage_enabled = False
        self.last_tick = 0
        self.d_count = 0
        self.long_close_lock = Decimal(0)
        self.short_close_lock = Decimal(0)
        self.qty_switching = False
        self.recent_balance = collections.deque(maxlen=10)
        self.pos_price = Decimal(0)

        def float2decimal(_dict):
            for k, v in _dict.copy().items():
                if type(v) == float:
                    _dict[k] = Decimal(str(v))
            return _dict

        self.params = float2decimal(self.config['strategy']['params'])
        # self.symbol_1 = self.config['strategy']['symbol_1']
        self.params["amount_min"] = Decimal(20)
        self.params["price_gap_min"] = Decimal(0.001)
        self.params["spread_min"] = Decimal(0.001)
        self.params["qty_amp"] = Decimal(2)
        symbol = self.params["symbol"]
        self.symbol_1 = f"binance.{symbol}_usdt_swap.usdt_contract.na"

        self._params =self.params["model_params"]

        self.symbol_trade = f"binance.{symbol}_usdt_swap.usdt_contract.na"
        self.symbol_data = f"binance.{symbol}_usdt_swap.usdt_contract.na"

        self.t_1 = Decimal(0)
        self.t_4 = Decimal(0)

        self.filled_order = {
            "buy_tot":0,
            "buy_amount":0,
            "sell_tot":0,
            "sell_amount":0,
            "amount_usdt":collections.deque(maxlen=48),
            "amount_unit":collections.deque(maxlen=48),
            "buy_sell_diff":collections.deque(maxlen=48),
            "indicator":collections.deque(maxlen=48)
        }

        self.strategy_start_time = int(time.time()*1e3)
        
        self._order_to_store = Queue()
        
        self.tf_id = None
        self.tf = False
        
        self.trade_cache_buy_1s_amount = Decimal(0)
        self.trade_cache_buy_1s_qty = Decimal(0)
        
        self.send_order_queue = Queue()

    async def calculate_stats(self):
        config = self.get_symbol_config(self.symbol_1)
        ts = float(config["price_tick_size"]*2)
        i = 0
        while ts % 10<1:
            ts = ts *10
            i += 1

        while True:
            await asyncio.sleep(1)
            if not self.qty_switching:
                await asyncio.sleep(1800)
                try:
                    buy_avg_price = self.filled_order["buy_tot"]/self.filled_order["buy_amount"] if self.filled_order["buy_amount"] else None
                    sell_avg_price = self.filled_order["sell_tot"] / self.filled_order["sell_amount"] if self.filled_order["sell_amount"] else None
                    price_gap = round(
                            (sell_avg_price-buy_avg_price)/(sell_avg_price+buy_avg_price)*2,5
                        ) if buy_avg_price and sell_avg_price else None
                    amount_usdt = round(self.filled_order["buy_tot"]+self.filled_order["sell_tot"],2)
                    amount_unit = round((self.filled_order["buy_amount"]+self.filled_order["sell_amount"])/float(self.params["base_amount"]),2)

                    self.filled_order["amount_usdt"].append(amount_usdt)
                    self.filled_order["amount_unit"].append(amount_unit)
                    self.filled_order["buy_sell_diff"].append(price_gap)
                    self.filled_order["indicator"].append(round(price_gap*amount_unit,5) if price_gap else None)
                    self.logger.warning(f"""
                    ===================================================             
                    amount_usdt,{[x for x in self.filled_order["amount_usdt"]]}
                    amount_unit, {[x for x in self.filled_order["amount_unit"]]}
                    sell buy gap, {[x for x in self.filled_order["buy_sell_diff"]]}
                    ===================================================
                    """)
                    self.filled_order["buy_tot"] = 0
                    self.filled_order["sell_tot"] = 0
                    self.filled_order["buy_amount"] = 0
                    self.filled_order["sell_amount"] = 0

                    if price_gap is not None:
                        if amount_unit > float(self.params["amount_min"]) and price_gap>float(self.params["price_gap_min"]):
                            if self.params["base_amount"] <= self.base_amount * Decimal(1.2):
                                self.qty_switching = True
                        else:
                            if self.params["base_amount"] > self.base_amount:
                                self.qty_switching = True
                    elif self.params["base_amount"] > self.base_amount:
                        self.qty_switching = True

                except Exception as err:
                    self.logger.warning(f"stats calculation error, {err}")

    async def qty_switching_process(self):

        while True:
            await asyncio.sleep(0.01)
            try:
                if self.qty_switching:
                    if (abs(self.pos)<self.params["base_amount"]*Decimal(10) or self.params["base_amount"] <= self.base_amount*Decimal(1.2)) and len(self.filled_order["amount_unit"]):
                        self.logger.warning(f"switching qty now")
                        if not self.filled_order["amount_unit"][-1] or not self.filled_order["buy_sell_diff"][-1]:
                            self.params["base_amount"] = self.base_amount
                        elif self.filled_order["amount_unit"][-1]> float(self.params["amount_min"]) and self.filled_order["buy_sell_diff"][-1]>float(self.params["price_gap_min"]):
                            self.params["base_amount"] = self.base_amount * self.params["qty_amp"]
                        else:
                            self.params["base_amount"] = self.base_amount
                        await self.redis_set_cache({"base_amount": self.params["base_amount"],})
                        self.qty_switching = False
                        temp = self.params["base_amount"]
                        self.logger.warning(f"base amount:{temp}")
            except Exception as err:
                self.logger.warning(f"qty switching error, {err}")

    async def before_strategy_start(self):
        # subscribe trade, depth, order update
        # self._symbol_config_ 是一个 symbol:symbol_config的字典，symbol是启动策略的时候，在管理系统界面设置的
        self.direct_subscribe_orderbook(symbol_name=self.symbol_1)
        self.direct_subscribe_public_trade(symbol_name=self.symbol_1)
        self.direct_subscribe_order_update(symbol_name=self.symbol_trade)
        self.logger.setLevel("WARNING")
        self.session = aiohttp.ClientSession()
        
        try:
            api = self.get_exchange_api("binance")[0]
            position = await api.contract_position(self.get_symbol_config(self.symbol_1))
            self.pos = position.data["long_qty"] - position.data["short_qty"]
            cur_balance = (await api.account_balance(self.get_symbol_config(self.symbol_1))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol_1)
                qty_tick = symbol_cfg["qty_tick_size"]
                c = 0
                while True:
                    await asyncio.sleep(1)
                    c += 1
                    if self.ob_mp:
                        self.logger.warning(f"{self.ob_mp}")
                        amount = balance / Decimal(self.params["balance_base_ratio"]) / self.ob_mp
                        amount = Decimal(int(amount/qty_tick)*qty_tick)
                        self.params["base_amount"] = amount
                        self.base_amount = amount
                        break
                    if c > 1000:
                        self.logger.warning("breaking")
                        break
                        
        except Exception as err:
            self.logger.warning(f"init balance err:{err}")
        
        self.loop.create_task(self.update_base_amount())
        self.loop.create_task(self.store_order())
            
    async def update_base_amount(self):
        while True:
            await asyncio.sleep(1800)
            api = self.get_exchange_api("binance")[0]
            cur_balance = (await api.account_balance (self.get_symbol_config(self.symbol_1))).data
            equity = cur_balance.get('usdt')
            if equity:
                balance = equity["all"]
                symbol_cfg = self.get_symbol_config(self.symbol_1)
                qty_tick = symbol_cfg["qty_tick_size"]
                amount = balance / Decimal(self.params["balance_base_ratio"])/ self.ob_mp
                amount = Decimal(int(amount/qty_tick)*qty_tick)
                if self.base_amount != amount:
                    self.logger.warning(
                        f"""
                        ===================================
                        origin base amonut: {self.base_amount}
                        current base amount: {amount}
                        ===================================
                        """)
                self.base_amount = amount

    async def on_orderbook(self, symbol, orderbook):
        self.ap = orderbook["asks"][0][0]
        self.bp = orderbook["bids"][0][0]
        self.ob_mp = (self.ap + self.bp) / Decimal(2)
        self.spread = self.ap - self.bp
        self.ob_update = True

    async def on_public_trade(self, symbol, trade: PublicTrade):
        s = symbol.split(".")[1].split("_")[0]
        self.trade_cache.append({"ts":trade.server_ms, "q":trade.quantity*trade.price, "s":trade.side})
        self.last_trade_ts = trade.server_ms
        if s == self.params["symbol"]:
            self.trade_cache_buy_1s_amount += trade.quantity * trade.price
            self.trade_cache_buy_1s_qty += trade.quantity
            if trade.side == OrderSide.Buy:
                self.trade_cache_buy_1s.append(float(trade.price))
            else:
                self.trade_cache_sell_1s.append(float(trade.price))

    async def calculate_t_1(self):
        while True:
            await asyncio.sleep(1)

            if self.trade_cache_buy_1s and self.trade_cache_buy_1s[0]!=0:
                buy_imp = (max(self.trade_cache_buy_1s) - self.trade_cache_buy_1s[0]) / self.trade_cache_buy_1s[0]
            else:
                buy_imp = 0

            if self.trade_cache_sell_1s and self.trade_cache_sell_1s[0]!=0:
                sell_imp = (self.trade_cache_sell_1s[0] - min(self.trade_cache_sell_1s)) / self.trade_cache_sell_1s[0]
            else:
                sell_imp = 0
                
            self.mp = self.trade_cache_buy_1s_amount / self.trade_cache_buy_1s_qty if self.trade_cache_buy_1s_qty else None

            self.trade_cache_buy_1s = []
            self.trade_cache_sell_1s = []
            self.trade_cache_buy_1s_amount = Decimal(0)
            self.trade_cache_buy_1s_qty = Decimal(0)

            self.trade_cache_buy_1s_large.append(buy_imp)
            self.trade_cache_sell_1s_large.append(sell_imp)
            if len(self.trade_cache_buy_1s_large) >= 201:
                self.trade_cache_buy_1s_large.pop(0)
            if len(self.trade_cache_sell_1s_large) >= 201:
                self.trade_cache_sell_1s_large.pop(0)

            t_buy = max(self.trade_cache_buy_1s_large)
            t_sell = max(self.trade_cache_sell_1s_large)
            self.t_1 = Decimal(max(t_buy,t_sell))
            
            self.send_order_queue.put_nowait(1)
        
        
    def handle_pos(self, xchg_id, p_order):
        amount_changed = p_order.filled_amount - self.order_cache[xchg_id].filled_amount
        if not amount_changed:
            return
        
        if not self.qty_switching:
            if self.order_cache[xchg_id].side == OrderSide.Buy:
                self.filled_order["buy_tot"] += float(amount_changed * p_order.avg_filled_price)
                self.filled_order["buy_amount"] += float(amount_changed)
            
            if self.order_cache[xchg_id].side == OrderSide.Sell:
                self.filled_order["sell_tot"] += float(amount_changed * p_order.avg_filled_price)
                self.filled_order["sell_amount"] += float(amount_changed)
                
        if self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            self.pos_price = (self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (self.pos + amount_changed)
            self.pos += amount_changed
        elif self.pos >= Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            if self.pos < amount_changed:
                self.pos_price = p_order.avg_filled_price
            self.pos -= amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Buy:
            if self.pos + amount_changed > Decimal(0):
                self.pos_price = p_order.avg_filled_price
            self.pos += amount_changed
        elif self.pos < Decimal(0) and self.order_cache[xchg_id].side == OrderSide.Sell:
            self.pos_price = (-self.pos*self.pos_price + amount_changed*p_order.avg_filled_price) / (-self.pos + amount_changed)
            self.pos -= amount_changed

    async def on_order(self, order: PartialOrder):
        xchg_id = order.xchg_id
        if xchg_id not in list(self.order_cache): 
            self.unknown_order_dict[xchg_id] = order
            return
        
        if order.filled_amount < self.order_cache[xchg_id].filled_amount:
            return

        self.handle_pos(xchg_id,order)
        self.order_cache[xchg_id].filled_amount = order.filled_amount
        self.order_cache[xchg_id].avg_filled_price = order.avg_filled_price
        self.order_cache[xchg_id].xchg_status = order.xchg_status
        extra_info = self.order_cache[xchg_id].message
        self.order_cache[xchg_id].extra_info = f"{'b' if extra_info['source'] == -1 else 'e'}_{extra_info['stop_ts']}"
        tag_info = int(self.params["base_amount"]/self.base_amount)
        self.order_cache[xchg_id].tag = f"{tag_info}"

        if order.xchg_status in OrderStatus.fin_status():
            if order.filled_amount > 0:
                o = self.order_cache[xchg_id]
                o.message = ""
                self._order_to_store.put_nowait(o)
            self.order_cache.pop(xchg_id)
            if xchg_id in self.unknown_order_dict:
                self.unknown_order_dict.pop(xchg_id)
            if self.tf_id == xchg_id:
                self.tf = False

    async def check_unknown_order(self):
        while True:
            await asyncio.sleep(0.001)
            now_know_order = {}
            for xchg_id, partial_order in self.unknown_order_dict.copy().items():
                if xchg_id in self.order_cache:
                    now_know_order[xchg_id] = partial_order
                    self.unknown_order_dict.pop(xchg_id)
            await asyncio.gather(
                *[self.on_order(partial_order) for xchg_id, partial_order in now_know_order.items()]
            )

    async def store_order(self):
        while True:
            order = await self._order_to_store.get()
            await self.post_order_to_system(order, self.session)
            
    async def strategy_core(self):
        await asyncio.sleep(30)
        await asyncio.gather(
            self.send_order_action(),
            self.cancel_order_action(),
            self.reset_missing_order_action(),
            self.update_redis_cache(),
            self.check_trade_flow(),
            self.check_unknown_order(),
            self.check_drawback(),
            self.calculate_t_1(),
            self.calculate_stats(),
            self.qty_switching_process(),
            self.check_position(),
            self.exit_action()
        )

    def get_amout_structure(self):
        return [Decimal(1), Decimal(2), Decimal(4)]
        
    def get_entry_amount(self, net_loc, base_amount,side,level=2):
        if self.pos > Decimal(10)*self.params["base_amount"] and self.pos_price > self.mp*(self.t_1+Decimal(1)) and side == 1:
            return [i*base_amount*level for i in self.get_amout_structure()][net_loc - 1]
        elif self.pos < -Decimal(10)*self.params["base_amount"] and self.pos_price < self.mp*(Decimal(1)-self.t_1) and side == 2:
            return [i*base_amount*level for i in self.get_amout_structure()][net_loc - 1]
        else:
            return [i*base_amount for i in self.get_amout_structure()][net_loc - 1]

    def get_order_position(self):
        ask = dict()
        bid = dict()
        ask[1] = Decimal(round(self._params["up1_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up1_int"],5))
        ask[2] = Decimal(round(self._params["up2_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up2_int"],5))
        ask[3] = Decimal(round(self._params["up3_coef"]*np.sqrt(float(self.trade_buy)) + self._params["up3_int"],5))
        
        bid[1] = Decimal(round(self._params["down1_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down1_int"],5))
        bid[2] = Decimal(round(self._params["down2_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down2_int"],5))
        bid[3] = Decimal(round(self._params["down3_coef"]*np.sqrt(float(self.trade_sell)) + self._params["down3_int"],5))

        return ask, bid

    def generate_entry_orders(self):
        if abs(self.pos)>Decimal(100)*self.params["base_amount"] or not self.mp or self.qty_switching:
            return []
        
        entry_orders = []
        mp = self.mp
        
        ask, bid = self.get_order_position()

        for i in range(1, 4):
            sell_p = mp *(Decimal(1+ask[i]))
            buy_p = mp *(Decimal(1-bid[i]))
            
            s_amount = self.get_entry_amount(net_loc=i, base_amount=self.params["base_amount"],side=2, level=2)
            b_amount = self.get_entry_amount(net_loc=i, base_amount=self.params["base_amount"],side=1, level=2)

            b_io = [self.symbol_1, buy_p, b_amount, OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,0.9,1]
            s_io = [self.symbol_1, sell_p, s_amount, OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,0.9,1]

            entry_orders.append(b_io)
            entry_orders.append(s_io)

        temp_bound = max(self.t_1, Decimal(0.001))
        if ask[3] < temp_bound:
            entry_orders.append([self.symbol_1, mp *(Decimal(1+temp_bound)), self.params["base_amount"]*Decimal(15), OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
            if temp_bound < Decimal(0.003):
                entry_orders.append([self.symbol_1, mp *(Decimal(1+0.003)), self.params["base_amount"]*Decimal(30), OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])      
        elif ask[3] > temp_bound and ask[3] < Decimal(0.002):
            entry_orders.append([self.symbol_1, mp *(Decimal(1+0.002)), self.params["base_amount"]*Decimal(15), OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
            entry_orders.append([self.symbol_1, mp *(Decimal(1+0.003)), self.params["base_amount"]*Decimal(30), OrderSide.Sell, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
        
        if bid[3] < temp_bound:
            entry_orders.append([self.symbol_1, mp *(Decimal(1-temp_bound)), self.params["base_amount"]*Decimal(15), OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
            if temp_bound < Decimal(0.003):
                entry_orders.append([self.symbol_1, mp *(Decimal(1-0.003)), self.params["base_amount"]*Decimal(30), OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
        elif bid[3] > temp_bound and bid[3] < Decimal(0.002):
            entry_orders.append([self.symbol_1, mp *(Decimal(1-0.002)), self.params["base_amount"]*Decimal(15), OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
            entry_orders.append([self.symbol_1, mp *(Decimal(1-0.003)), self.params["base_amount"]*Decimal(30), OrderSide.Buy, OrderPositionSide.Open, OrderType.PostOnly,0.9,1])
        return entry_orders
        
    def order_generate_logic(self):
        
        req_orders = []

        entry_orders = self.generate_entry_orders()
        req_orders.extend(entry_orders)

        filter_orders = self.request_orders_filter(req_orders)

        return filter_orders


    def request_orders_filter(self,req_orders):
        filter_orders = []
        buy_source = [self.order_cache[key].message["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Buy and self.order_cache[key].message]
        sell_source = [self.order_cache[key].message["source"] for key in list(self.order_cache) if self.order_cache[key].side == OrderSide.Sell and self.order_cache[key].message]

        for per_req in req_orders:
            if per_req[3] == OrderSide.Buy:
                x_source = np.array(buy_source)
            else:
                x_source = np.array(sell_source)
            x_source = np.array(x_source)
            cur_pending_len = sum((x_source == per_req[7]) * 1)
            if cur_pending_len < 1:
                filter_orders.append(per_req)
        return filter_orders

    def volume_notional_check(self, symbol, price, qty):
        config = self.get_symbol_config(symbol)
        if qty < config["min_quantity_val"] * Decimal(1):
            return False
        if price * qty < config["min_notional_val"] * Decimal(1):
            return False
        return True
    
    async def close_position(self,params):
        if not self.volume_notional_check(*params[:3]):
            return
        try:
            order =  await self.make_order(*params)
            if order:
                extra_info = dict()
                extra_info["stop_ts"] = 1
                extra_info["cancel"] = 0
                extra_info["source"] =  0 # entry or balance, 0 for close
                extra_info["query"] =  0
                order.message = extra_info
                self.order_cache[order.xchg_id] = order
                self.order_cache[order.xchg_id].filled_amount = Decimal(0)

        except Exception as err:
            self.logger.warning(f"close position err, {err}")

    async def send_order_action(self):
        # only create order cache here
        async def batch_send_order(params):
            if not self.volume_notional_check(*params[:3]):
                return
            try:
                order =  await self.make_order(*params[:6])
                if order:
                    extra_info = dict()
                    extra_info["stop_ts"] = params[6]
                    extra_info["cancel"] = 0
                    extra_info["source"] =  params[7] # entry or balance
                    extra_info["query"] =  0
                    order.message = extra_info
                    self.order_cache[order.xchg_id] = order
                    self.order_cache[order.xchg_id].filled_amount = Decimal(0)

            except Exception as err:
                if str(err) == "Expected object or value":
                    raise
                else:
                    self.logger.warning(f"send order err, {err},")

        while True:
            await self.send_order_queue.get()
            if self.send_order:
                orders = self.order_generate_logic()
                try:
                    await asyncio.gather(*[batch_send_order(order_params) for order_params in orders])
                except Exception as err:
                    self.logger.warning(f"batch send order err, {err}")

    async def check_position(self):
        while True:
            await asyncio.sleep(60)
            try:
                api = self.get_exchange_api("binance")[0]
                position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos != self.pos:
                    self.logger.warning(
                    f"""
                    =========update wrong position==========
                    origin: {self.pos}
                    update: {pos}
                    """
                )
                    self.pos = pos
                
            except Exception as err:
                self.logger.warning(f'check position err {err}')
    
      
    async def get_order_status_direct(self,order):
        try:
            api = self.get_exchange_api_by_account(order.account_name)
            # self.logger.warning(f"get order {order.xchg_id} from exchange http api")
            res = await api.order_match_result(self.get_symbol_config(order.symbol_id),order.xchg_id)
            _order = res.data
        except Exception as err:
            self.logger.error(f"direct check order error: {err}")
            _order = None
        return _order

    async def reset_missing_order_action(self):

        async def batch_check_order(oid,order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > (order.message["stop_ts"] + 1)*1e3 and order.message["cancel"]>0:
                try:
                    order_new = await self.get_order_status_direct(order)
                    self.order_cache[oid].message["query"] += 1
                    if order_new.xchg_status in OrderStatus.fin_status():
                        await self.on_order(order_new)
                    elif self.order_cache[oid].message["query"]>10:
                        self.order_cache.pop(oid)
                except Exception as err:
                    self.logger.warning(f"check order failed {err}, id:{oid}")

        while True:
            await asyncio.sleep(1)
            await asyncio.gather(
                *[batch_check_order(oid, order) for oid, order in self.order_cache.items()]
            )

    async def update_redis_cache(self):

        async def batch_cancel(oid, order):
            try:
                await self.cancel_order(order)
            except Exception as err:
                self.logger.warning(f'cancel order {oid} err {err}')

        async def check_cache():
            # only update the exit
            try:
                data = await self.redis_get_cache()
                if data.get("exit"):
                    self.send_order = False
                    for i in range(3):
                        await asyncio.gather(
                            *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
                            )
                        api = self.get_exchange_api("binance")[0]
                        position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                        pos = position.data["long_qty"] - position.data["short_qty"]
                        if pos > 0:
                            await self.close_position([self.symbol_1, self.ob_mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                        else:
                            await self.close_position([self.symbol_1, self.ob_mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                        
                        await asyncio.sleep(3)
                    await self.redis_set_cache(
                        {
                        "exit":None, 
                        "current position/base_amount":0, 
                        "max balance": 0,
                        "current position": 0, 
                        "long position": 0, 
                        "short position": 0, 
                        }
                        )
                    
                    self.logger.warning(f"manually exiting")
                    exit()

                # if not self.qty_switching and data.get("base_amount") is not None:
                #     if abs(Decimal(data.get("base_amount")) - self.params["base_amount"]) > Decimal(0.05)*self.params["base_amount"]:
                #         self.params["base_amount"] = Decimal(data.get("base_amount"))
                #         self.base_amount = Decimal(data.get("base_amount"))
                        
            except Exception as err:
                self.logger.warning(f"turn down strategy failed {err}")

        async def update_cache():
            _dict = dict()
            _dict = {
                "exit": None,
                "current position/base_amount": self.pos/self.params["base_amount"],
                "max balance": self.max_balance,
                "current position": self.pos,
                "t_1":self.t_1,
                "24h_vol_usdt":sum(self.filled_order["amount_usdt"]),
                "base_amount": self.params["base_amount"],
                "pos_price":self.pos_price
            }
            try:
                await self.redis_set_cache(_dict)
            except Exception as err:
                self.logger.warning(f"set redis cache failed {err}")

        while True:
            await asyncio.sleep(1)
            await check_cache()
            await update_cache()
    
    async def check_trade_flow(self):

        while True:
            await asyncio.sleep(0.1)
            
            while self.trade_cache and self.trade_cache[0]['ts'] < time.time()*1e3 - 5e3:
                self.trade_cache.pop(0)

            if not self.trade_cache and self.last_trade_ts < time.time()*1e3 - 120e3:
                self.logger.warning("trade data flow missing and lock")
                for i in range(3):
                    api = self.get_exchange_api("binance")[0]
                    try:
                        await api.flash_cancel_orders(self.get_symbol_config(self.symbol_1))
                        position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                        pos = position.data["long_qty"] - position.data["short_qty"]
                        if pos > 0:
                            await self.close_position([self.symbol_1, self.ob_mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                        else:
                            await self.close_position([self.symbol_1, self.ob_mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                    except Exception as err:
                        self.logger.warning(f"check cahce err:{err}")
                    await asyncio.sleep(3)
                await asyncio.sleep(30)

            elif not self.trade_cache:
                self.trade_buy = Decimal(0)
                self.trade_sell = Decimal(0)
            else:
                self.trade_buy = abs(sum([item["q"] for item in self.trade_cache if item["s"]==OrderSide.Buy]))
                self.trade_sell = abs(sum([item["q"] for item in self.trade_cache if item["s"]==OrderSide.Sell]))

    async def check_drawback(self):

        async def clear_position():
            for i in range(3):
                api = self.get_exchange_api("binance")[0]
                await api.flash_cancel_orders(self.get_symbol_config(self.symbol_1))
                position = await api.contract_position(self.get_symbol_config(self.symbol_1))
                pos = position.data["long_qty"] - position.data["short_qty"]
                if pos > 0:
                    await self.close_position([self.symbol_1, self.ob_mp*Decimal(0.99), abs(pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                else:
                    await self.close_position([self.symbol_1, self.ob_mp*Decimal(1.01), abs(pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                await asyncio.sleep(3)
                
        while True:
            await asyncio.sleep(30)
            try:
                api = self.get_exchange_api("binance")[0]
                cur_balance = (await api.account_balance (self.get_symbol_config(self.symbol_1))).data
                equity = cur_balance.get('usdt')
                
                self.recent_balance.append(equity["all"])
                self.max_balance = max(self.max_balance, np.median(self.recent_balance))
                if np.median(self.recent_balance) < self.max_balance*Decimal(0.96):
                    t = time.time() - self.last_tick
                    self.send_order = False

                    await clear_position()
                    await self.redis_set_cache(
                    {
                    "exit":None, 
                    "current position/base_amount":0, 
                    "max balance": 0,
                    "current position": 0, 
                    "long position": 0, 
                    "short position": 0, 
                    "base_amount":self.params["base_amount"]
                    }
                    )
                    self.max_balance = 0
                    self.d_count += 1
                    if self.d_count >= 1:
                        self.logger.warning(f"1 drawdown")
                        exit()
                    self.logger.warning(f"reach max drawback, try to lock for 300s now")
                    await asyncio.sleep(300)
                    if t < 120:
                        await asyncio.sleep(900)
                    self.last_tick = time.time()
                    self.send_order = True
                elif abs(self.pos)>Decimal(120)*self.params["base_amount"]:
                    self.logger.warning(f"reach 120 inven")
                    self.send_order = False
                    await clear_position()
                    await asyncio.sleep(90)
                    self.send_order = True

            except Exception as err:
                self.logger.warning(f"check balance err {err}")

    async def cancel_order_action(self):
        
        async def batch_cancel(oid, order):
            if not order.message:
                return
            if time.time()*1e3 - order.create_ms > order.message["stop_ts"] * 1e3 + order.message["cancel"]*1e3:
                try:
                    await self.cancel_order(order)
                    if self.order_cache.get(oid) is not None:
                        self.order_cache[oid].message["cancel"] += 1
                except Exception as err:
                    self.logger.warning(f'cancel order {oid} err {err}')

        while True:
            await asyncio.sleep(0.1)
            await asyncio.gather(
                *[batch_cancel(oid, order) for oid, order in self.order_cache.items()]
            )

    async def exit_action(self):
        async def exit_position(params):
            if not self.volume_notional_check(*params[:3]):
                return
            
            try:
                order =  await self.make_order(*params)
                if order:
                    extra_info = dict()
                    extra_info["stop_ts"] = 1
                    extra_info["cancel"] = 0
                    extra_info["source"] =  0 # entry or balance, 0 for close
                    extra_info["query"] =  0
                    order.message = extra_info
                    self.order_cache[order.xchg_id] = order
                    self.order_cache[order.xchg_id].filled_amount = Decimal(0)
                    
                    self.tf = True
                    self.tf_id = order.xchg_id

            except Exception as err:
                self.logger.warning(f"take profit err: {err}")

        while True:
            await asyncio.sleep(1)
            close_amount = self.base_amount
            if self.tf == False:
                if self.params["base_amount"]*Decimal(5) <abs(self.pos) <= self.params["base_amount"]*Decimal(15):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1 + 0.002):
                        await exit_position([self.symbol_1, self.pos_price * Decimal(1 + 0.002), abs(self.pos), OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1 - 0.002):
                        await exit_position([self.symbol_1, self.pos_price * Decimal(1 - 0.002), abs(self.pos), OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(15) <abs(self.pos) <= self.params["base_amount"]*Decimal(30):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1 + 0.0005):
                        await exit_position([self.symbol_1, self.pos_price * Decimal(1 + 0.0005), close_amount, OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1 - 0.0005):
                        await exit_position([self.symbol_1, self.pos_price * Decimal(1 - 0.0005), close_amount, OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(30) <abs(self.pos) <= self.params["base_amount"]*Decimal(50):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1):
                        await exit_position([self.symbol_1, self.pos_price, close_amount, OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1):
                        await exit_position([self.symbol_1, self.pos_price, close_amount, OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(50) <abs(self.pos):
                    if self.pos > 0 and self.bp > self.pos_price * Decimal(1 - 0.0005):
                        await exit_position([self.symbol_1, self.pos_price * Decimal(1 - 0.0005), close_amount, OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos < 0 and self.ap < self.pos_price * Decimal(1 + 0.0005):
                        await exit_position([self.symbol_1, self.pos_price * Decimal(1 + 0.0005), close_amount, OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos > 0 and self.bp < self.pos_price * Decimal(1 - 0.002):
                        await exit_position([self.symbol_1, self.ob_mp*Decimal(0.99), close_amount, OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos < 0 and self.ap > self.pos_price * Decimal(1 + 0.002):
                        await exit_position([self.symbol_1, self.ob_mp*Decimal(1.01), close_amount, OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                elif self.params["base_amount"]*Decimal(100) <abs(self.pos):
                    if self.pos > 0:
                        await exit_position([self.symbol_1, self.ob_mp*Decimal(0.99), close_amount, OrderSide.Sell, OrderPositionSide.Close, OrderType.IOC])
                    if self.pos < 0:
                        await exit_position([self.symbol_1, self.ob_mp*Decimal(1.01), close_amount, OrderSide.Buy, OrderPositionSide.Close, OrderType.IOC])
                    

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    s = MyStrategy(loop)
    s.run_forever()